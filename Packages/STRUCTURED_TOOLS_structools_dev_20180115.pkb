CREATE OR REPLACE PACKAGE BODY structoolsapp.structured_tools
AS

   /*****************************************************************************************************************************************/
   /* H6 Changes:                                                                                                                                                                                                                                                           */
	/*		29/11/17 Grazyna Szadkowski	Coded bay strategy 80.                                                                                                                                                                                           */
	/*		27/11/17 Grazyna Szadkowski	Coded changes to cater for pickid suppression.                                                                                                                                                         */
	/*		14/11/17 Grazyna Szadkowski	Coded new reinforcement strategy conditions, for PWOD strategy 68.                                                                                                                         */
	/*		06/11/17 Grazyna Szadkowski	Populate labour_hrs in ST_BAY_STRATEGY_RUN* tables, column LABOUR_HRS (renamed from writeoff_val).                                                               */
   /*		24/10/17 Grazyna Szadkowski	Modified PWOD strategy 68 and coded stay strategies 74-77 and xarms strategies 78-79.                                                                                             */
   /*																	 Modified logic for determination of stays and xarms strategies, ie extrac defects inthe code rather than prepopulate the data for specific defects in 				*/
   /*																	 st_equipment.                                                                                                                                                                                                         */	
   /*		02/10/17 Grazyna Szadkowski	Coded PWOD strategy 72 and bay strategy 73.                                                                                                                                                         */
   /*		27/09/17 Grazyna Szadkowski	Remove references to column writeoff_val, renamed now to labour_hrs. Populate labour_hrs.                                                                                     */
   /*		26/09/17 Grazyna Szadkowski	Created wrapper for procedure reporting.generate_post_del_risk_profile.                                                                                                                    */
   /*    30/08/17 Grazyna Szadkowski   Removed (or provided a wrapper for) procedures that are now part of other packaged (housekeeping, common_procs, reporting, optimiser_cycle)           */
   /*												Modified sev_cde processing, to cater for alpha characters (ie st_eqp_defect will now contain all priorities, injcluding the alpha values.                                             */
   /*												Modified selection of defects for vicinity to get all UPAL defects.                                                                                                                                                   */
   /*                                  Coded stretegy 71 for PWOD.                                                                                                                                                                                                   */
   /* H5 Changes:                                                                                                                           																																												*/
   /*    27/07/16 Grazyna Szadkowski   Create priority_treatment column for bays meeting predefined criteria and poles+eqp from selected    																										*/
   /*                                  priority zones. Updated procedure create_summary_for_opensolver to include counts of priority assets.																															*/
   /*                                  Create treatments for stays and crossarms.                                                           																																								*/
   /*                                  Make changes to accomodate Financial component of risk.                                              																																					*/
   /*                                  Make changes to accommodate updated conductor strategy (strategy id 63).                             																																		*/
   /*                                  Added code for summarising segment values, for optimiser to target segments outaside mtzns.          																															 */
   /*                                  Removed reactive maintenance component when producing zone summary, sniper program.                  																														*/
   /*                                  Commented out code referring to parent runs.                                                         																																							*/
   /*                                  Added 'parallel' hint in selected queries to improve response time.                                  																																					*/
   /*                                  Added code to upgrade event for poles under REPLACE bays.                                            																																				 */
   /*                                  Modified handling of FULL and STANDLAONE cost options for xarms and stays.                           																																		*/
   /*                                  Coded bay strategies 65 and 66 (66 contains test for pf < average pfail).                            																																				*/
   /*                                  Modified risk_profile and incd_forecast procedures to ensure it can run for a specified number       																																	*/
   /*                                  of years.                                                                                            																																													 */
   /*                                  Coded pole strategy 68.                                                                              																																										  */
   /*                                  Coded bay strategy 69.                                                                               																																									     */
   /* H4 Changes:                                                                                                                           																																										      */
   /*    27/06/16 Grazyna Szadkowski   Fixed bug not outputting all noaction risk scores to plsql table (in procedure Initialise, when      																													  */
   /*                                  invoking store_risk_pwod).                                                                           */
   /*    22/06/16 Grazyna Szadkowski   Fixed the sniper procedure to output the asset's actual mtzn, not cons_segment_mtzn.                 */
   /*    13/04/16 Grazyna Szadkowski   Rewritten code for procedures generate_post_del_risk_profile and generate_incd_forecast to improve   */
   /*                                  response time.                                                                                       */
   /*                                  Reverted changes to include committed pickids, in procs incd_forecast, reac_maint and risk_profile,  */
   /*                                  as per email from Brad Smith 06/04.                                                                  */ 
   /*                                  Added 'delete_old_results' parameter to procs generate_opt_eqp_list, generate_incd_forecast,         */
   /*                                  generate_age_stats, generate_bays_seg_delta.                                                         */
   /*    06/04/16 Peter Condon         Removed code for obsolete strategies(strategy sets 1 and 2).                                         */
   /*                                  Created procedures to maintain new tables: st_opt_program, st_opt_parm, st_opt_program_parm_value,   */
   /*                                  st_opt_program_run.                                                                                  */
   /* Created various maintenance procedures, like register_run_request, reset_run_request and delete_run_reques,update_span_unit_cost,     */
   /* and others.                                                                                                                           */
   /* Cater FOR COUNTRY AND METRO unit rates for poles and equipment.                                                                       */
   /* Output conductor length, in addition to span length, to st_strategy_run_summary table.                                                */
   /* Cater for partial treatment, based on risk_red per dollar.                                                                            */
   /* Fixed a bug in setting the SCHEDULED value in st_bay_strategy_run_rb and st_strategy_run_rb, ie use the segment's mtzn if the segment */
   /* is to selected for treatment in the first year.                                                                                       */
   /* Modify the age_stats procedure to output sum of bay lengths in addition to bay counts.                                                */
   /* Remove the code to apply rural markup to rural spans.                                                                                 */
   /* Modified create_st_span to access asset_geography to populate area_type (METRO, RURAL, TOWNSITE).                                     */
   /* Modified to collect feeder category information for poles and equipment. When no data exists, defaulted to 'Short Rural'.             */
   /* Modified to apply sif ratings depending on feeder category (ShortRural, LongRural etc).                                               */
   /* Modified to ensure mtzn 'no return after treatment' parameter works for multi-batch runs (summary_for_opensolver procedure).          */
   /* Coded modified strategies (strategy id 46 - 56).                                                                                      */
   /* Modified selection criteria for segment 'rebuild' - removed the ' OR minimum length' critera, ie left only the min pcnt criteria.     */
   /* Implemented mechanism for promoting or demoting assets depending on what feeder type they belong to, by adjusting                     */
   /* npv_sif_risk_reduction.                                                                                                               */
   /* Use more granular data on risk and pfails for poles and conductors (combined risk and pole only or conductor only for pfail.          */
   /* Changed to use st_carrier_detail table in conjunction with carrier_detail (from arcfm) table to derive conductor carrier model code.  */
   /* Changed to adjust installation year based on the table of st_pickid_committed. SCHEDULED flag is now applicable to individual pickids */
   /* (all qualifications on SCHEDULED value in all procs have been removed).                                                               */
   /* Fixed bug causing corrup sum of risk reduction in st_strategy_run_summary table.                                                      */
   /* Fixed bug causing incorrect readings of pfail in the 2nd 5-yr run.                                                                    */
   /* Coded strategy 57 for bays.                                      .                                                                    */
   /* Use carrier data from field validation to obtain carrier model code (stored in st_carrier_detail table) and bay condition score       */
   /* (stored in new column in st_bay_equipment table).                                                                                     */
   /* Reset defects for pickids that have been delivered in 1st pass or are on the committed pickid list.                                   */  
   /* Changed age_stats procedure to cater for more granularity in life expectancy for DSTRs and PWODs.                                     */  
   /* Reset condition code for bays that have been delivered in 1st pass or are on the committed pickid list.                               */  
   /* Exclude pole REINFORCE when checking for partial treatment threshold.                                                                 */
   /* Added Oracle hints to the sql for reac_maint procedure.                                                                               */
   /*****************************************************************************************************************************************/
   

   v_this_year                   PLS_INTEGER := EXTRACT (YEAR FROM SYSDATE);
   v_year_0                      PLS_INTEGER := EXTRACT (YEAR FROM SYSDATE);
   v_null_end_dt                     CONSTANT DATE := TO_DATE('01-01-3000','dd-mm-yyyy');
   v_prog_excp                   EXCEPTION;
   
   v_request_not_valid     EXCEPTION;

   TYPE number_tab_typ                    IS TABLE OF NUMBER;
   TYPE varchar_tab_typ                    IS TABLE OF VARCHAR2(50);   
    
   v_parm_scen_id_life_exp          PLS_INTEGER;
--   v_parm_scen_id_rural_markup      PLS_INTEGER;
   v_parm_scen_id_seg_min_len          PLS_INTEGER;
   v_parm_scen_id_seg_cons_pcnt     PLS_INTEGER;
--   v_parm_scen_id_seg_rb_years      PLS_INTEGER;
   v_parm_scen_id_sif_weighting     PLS_INTEGER;
    v_parm_scen_id_seg_rr_doll            PLS_INTEGER;        -- single scenario id applies to the whole set, ie all years
    v_parm_scen_id_eqp_rr_doll            PLS_INTEGER;        -- single scenario id applies to the whole set, ie all years
   v_parm_scen_id_fdr_cat_adj       PLS_INTEGER;
   
   TYPE fdr_cat_sif_rec_typ             IS RECORD
   (
    pubsafety_sif          NUMBER
   ,wforce_sif             NUMBER
   ,reliability_sif        NUMBER
   ,env_sif                NUMBER
   ,fin_sif                NUMBER
   ,adj_factor             NUMBER
   );
   
   TYPE fdr_cat_sif_tab_typ            IS TABLE OF fdr_cat_sif_rec_typ INDEX BY VARCHAR2(20);
   v_fdr_cat_sif_tab                   fdr_cat_sif_tab_typ;

   v_cost_key                 VARCHAR2(60);
--   v_rural_markup          NUMBER;
   v_bay_life_exp             PLS_INTEGER;

   TYPE cost_rec_typ          IS RECORD
   (labour_hrs               NUMBER
   ,total_cost                NUMBER
   );
   v_cost_rec              cost_rec_typ;
      
   TYPE cost_tab_typ     IS TABLE OF cost_rec_typ INDEX BY VARCHAR2(60);
   /* table of costs, indexed by a concatenation of various factors, depending on eqp type */   
   v_cost_tab            cost_tab_typ;
   
--   TYPE life_exp_tab_typ   IS TABLE OF INTEGER INDEX BY VARCHAR2(20);
--   v_life_exp_tab          life_exp_tab_typ;
   
--   TYPE fdr_cat_tab_typ    IS TABLE OF NUMBER INDEX BY VARCHAR2(35);
--   v_fdr_cat_adj_tab       fdr_cat_tab_typ;


   v_FAIL_PROB_HIGH_pwod            FLOAT(126);
   v_FAIL_PROB_HIGH_dstr            FLOAT(126);
   v_FAIL_PROB_HIGH_dof             FLOAT(126);
   v_FAIL_PROB_HIGH_recl            FLOAT(126);
   v_FAIL_PROB_HIGH_sect            FLOAT(126);
   v_FAIL_PROB_HIGH_ptsd            FLOAT(126);
   v_FAIL_PROB_HIGH_rgtr            FLOAT(126);

   v_FAIL_PROB_LOW_pwod            FLOAT(126);
   v_FAIL_PROB_LOW_dstr            FLOAT(126);
   v_FAIL_PROB_LOW_dof             FLOAT(126);
   v_FAIL_PROB_LOW_recl            FLOAT(126);
   v_FAIL_PROB_LOW_sect            FLOAT(126);
   v_FAIL_PROB_LOW_ptsd            FLOAT(126);
   v_FAIL_PROB_LOW_rgtr            FLOAT(126);

   v_fail_prob_step_pwod            FLOAT(126);
   v_fail_prob_step_dstr            FLOAT(126);
   v_fail_prob_step_dof             FLOAT(126);
   v_fail_prob_step_recl            FLOAT(126);
   v_fail_prob_step_sect            FLOAT(126);
   v_fail_prob_step_ptsd            FLOAT(126);
   v_fail_prob_step_rgtr            FLOAT(126);
   
   v_parm_val_key             VARCHAR2(60);
   TYPE cond_parm_tab_typ    IS TABLE OF VARCHAR2(200) INDEX BY VARCHAR2(60);
   v_cond_parm_tab         cond_parm_tab_typ;

   TYPE risk_rec_typ IS RECORD (
    pick_id        VARCHAR2(10)
   ,yr0            FLOAT(126)
   ,yr1            FLOAT(126)
   ,yr2            FLOAT(126)
   ,yr3            FLOAT(126)
   ,yr4            FLOAT(126)
   ,yr5            FLOAT(126)
   ,yr6            FLOAT(126)
   ,yr7            FLOAT(126)
   ,yr8            FLOAT(126)
   ,yr9            FLOAT(126)
   ,yr10            FLOAT(126)
   ,yr11            FLOAT(126)
   ,yr12            FLOAT(126)
   ,yr13            FLOAT(126)
   ,yr14            FLOAT(126)
   ,yr15            FLOAT(126)
   ,yr16            FLOAT(126)
   ,yr17            FLOAT(126)
   ,yr18            FLOAT(126)
   ,yr19            FLOAT(126)
   ,yr20            FLOAT(126)
   ,yr21            FLOAT(126)
   ,yr22            FLOAT(126)
   ,yr23            FLOAT(126)
   ,yr24            FLOAT(126)
   ,yr25            FLOAT(126)
   ,yr26            FLOAT(126)
   ,yr27            FLOAT(126)
   ,yr28            FLOAT(126)
   ,yr29            FLOAT(126)
   ,yr30            FLOAT(126)
   ,yr31            FLOAT(126)
   ,yr32            FLOAT(126)
   ,yr33            FLOAT(126)
   ,yr34            FLOAT(126)
   ,yr35            FLOAT(126)
   ,yr36            FLOAT(126)
   ,yr37            FLOAT(126)
   ,yr38            FLOAT(126)
   ,yr39            FLOAT(126)
   ,yr40            FLOAT(126)
   ,yr41            FLOAT(126)
   ,yr42            FLOAT(126)
   ,yr43            FLOAT(126)
   ,yr44            FLOAT(126)
   ,yr45            FLOAT(126)
   ,yr46            FLOAT(126)
   ,yr47            FLOAT(126)
   ,yr48            FLOAT(126)
   ,yr49            FLOAT(126)
   ,yr50            FLOAT(126)
   ,fire_frac       FLOAT(126) 
   ,eshock_frac     FLOAT(126)
   ,wforce_frac     FLOAT(126)
   ,reliability_frac FLOAT(126)
   ,physimp_frac     FLOAT(126)
   ,env_frac         FLOAT(126)
   ,fin_frac         FLOAT(126)
   );
      
   TYPE risk_tab_typ             IS TABLE OF FLOAT(126);

   
   v_risk_pubsafety_sif        FLOAT(126);
   v_risk_wforce_sif           FLOAT(126);
   v_risk_reliability_sif      FLOAT(126);
   v_risk_env_sif              FLOAT(126);
   v_risk_fin_sif              FLOAT(126);

   TYPE pwod_risk_tab_typ        IS TABLE OF risk_tab_typ INDEX BY VARCHAR2(40);
   v_pwod_risk_tab               pwod_risk_tab_typ;        --run time
   v_eqp_risk_tab                pwod_risk_tab_typ;          --run time
   v_pole_part_risk_tab          pwod_risk_tab_typ;

        
   PROCEDURE analyse_table (i_tab_name IN varchar2) IS
   
   BEGIN
      structools.schema_utils.anal_table(i_tab_name);
   
   END analyse_table;
   
   PROCEDURE copy_strategy_run_tmp_to_rb(i_run_id    IN PLS_INTEGER) IS
      
   BEGIN
 
       /* Copy data from strategy_run_rb_tmp to strategy_run_rb */
      INSERT INTO structools.st_bay_strategy_run_rb
      SELECT * FROM structools.st_bay_strategy_run_rb_tmp
      WHERE run_id = i_run_id;

      INSERT INTO structools.st_strategy_run_rb
      SELECT * FROM structools.st_strategy_run_rb_tmp
      WHERE run_id = i_run_id;
      
      dbms_output.put_line (i_run_id||' copied from *_rb_tmp tp *_run_rb');

   END copy_strategy_run_tmp_to_rb;

   PROCEDURE copy_st_strategy_run_rb_to_rb2(i_run_id    IN PLS_INTEGER) IS
      
   BEGIN
 
       /* Copy data from st_strategy_run_rb to st_strategy_run_rb2 */
      INSERT INTO structools.st_strategy_run_rb2
      SELECT * FROM structools.st_strategy_run_rb
      WHERE run_id = i_run_id;
      
      dbms_output.put_line (i_run_id||' copied from st_strategy_run_rb to st_strategy_run_rb2');

   END copy_st_strategy_run_rb_to_rb2;

   PROCEDURE populate_working_arrays IS

      CURSOR cost_csr IS
         SELECT EQUIP_GRP_ID
               ,ACTION           
               ,BUNDLED_IND      
               ,JOB_COMPLEXITY   
               ,PHASES           
               ,treatment_type   
               ,labour_hrs
               ,TOTAL_COST
               ,region
         FROM structools.st_eqp_type_action_cost
         ORDER BY equip_grp_id
                  ,TREATMENT_TYPE
                  ,ACTION
                  ,job_complexity
                  ,bundled_ind
                  ,phases
                  ,region
      ;    

--      CURSOR span_cost_csr(i_rural_markup IN NUMBER) IS
--         SELECT area_type
--               ,lvco_cnt
--               ,hvco_cnt
--               ,hvsp_cnt
--               ,CASE area_type
--                  WHEN 'RURAL'    THEN ROUND(labour_hrs * (1 + i_rural_markup))
--                  ELSE                 labour_hrs
--               END                     AS labour_hrs
--               ,CASE area_type
--                  WHEN 'RURAL'    THEN ROUND(total_cost * (1 + i_rural_markup))
--                  ELSE                 total_cost
--               END                     AS total_cost
--         FROM structools.st_span_action_cost
--      ;    
      CURSOR span_cost_csr IS
         SELECT area_type
               ,lvco_cnt
               ,hvco_cnt
               ,hvsp_cnt
               ,labour_hrs
               ,total_cost
         FROM structools.st_span_action_cost
      ;    

--      CURSOR life_exp_csr IS
--         
--         SELECT parameter_egi
--               ,parameter_wood_type
--               ,parameter_val
--         FROM structools.st_parameter_scenario
--         WHERE PARAMETER_ID = 'LIFE_EXPECTANCY'
--         AND   scenario_id = v_parm_scen_id_life_exp
--      ;    
      
--      CURSOR fdr_cat_adj_csr IS
--         
--         SELECT parameter_fdr_cat         AS fdr_cat
--               ,TO_NUMBER(parameter_val)  AS fdr_cat_adj_factor
--         FROM structools.st_parameter_scenario
--         WHERE parameter_id = 'FDR_CAT_ADJ'
--         AND scenario_id = v_parm_scen_id_fdr_cat_adj
--      ;
      
   
   BEGIN
      /* get cost of actions into w-s table */
      v_cost_tab.DELETE;
      
      FOR c IN cost_csr LOOP
         v_cost_rec.labour_hrs := c.labour_hrs;
         v_cost_rec.total_cost := c.total_cost;
               
         IF c.equip_grp_id = 'PWOD' OR c.equip_grp_id = 'PAUS' THEN
            v_cost_key := TRIM(c.equip_grp_id)||c.bundled_ind||TRIM(c.treatment_type)||TRIM(c.action)||TRIM(c.job_complexity)||TRIM(c.region);
            --dbms_output.put_line('v_cost_key = '||v_cost_key||' COST = '||c.total_cost);
            v_cost_tab(v_cost_key) :=  v_cost_rec;
         ELSIF c.equip_grp_id = 'ASTAY' OR c.equip_grp_id = 'AGOST' OR c.equip_grp_id = 'GSTAY' OR c.equip_grp_id = 'OSTAY' 
         OR c.equip_grp_id = 'HVXM' OR c.equip_grp_id = 'LVXM' THEN
            v_cost_key := TRIM(c.equip_grp_id)||TRIM(c.treatment_type);
            --dbms_output.put_line('v_cost_key = '||v_cost_key||' COST = '||c.total_cost);
            v_cost_tab(v_cost_key) :=  v_cost_rec;
         ELSIF c.equip_grp_id = 'DSTR' THEN
            v_cost_key := TRIM(c.equip_grp_id)||c.bundled_ind||TRIM(c.treatment_type)||TRIM(c.job_complexity)||TRIM(c.region);
            --dbms_output.put_line('v_cost_key = '||v_cost_key||' COST = '||c.total_cost);
            v_cost_tab(v_cost_key) :=  v_cost_rec;
         ELSIF c.equip_grp_id = 'RECL' THEN
            v_cost_key := TRIM(c.equip_grp_id)||c.bundled_ind||TRIM(c.treatment_type)||TRIM(c.phases)||TRIM(c.region);
            v_cost_tab(v_cost_key) :=  v_cost_rec;
         ELSE
            v_cost_key := TRIM(c.equip_grp_id)||c.bundled_ind||TRIM(c.treatment_type)||TRIM(c.region);
            v_cost_tab(v_cost_key) :=  v_cost_rec;
         END IF;
--         dbms_output.put_line(v_cost_key||' '||v_cost_rec.total_cost);
      END LOOP;
          
--     /* get cost of actions on spans into w-s table */
--      SELECT TO_NUMBER(parameter_val)
--      INTO   v_rural_markup      
--      FROM structools.st_parameter_scenario
--      WHERE PARAMETER_ID = 'RURAL_SPAN_COST_MARKUP'
--      AND   scenario_id = v_parm_scen_id_rural_markup
--      ;    

--      FOR c IN span_cost_csr(v_rural_markup) LOOP
      FOR c IN span_cost_csr LOOP
         v_cost_rec.labour_hrs := c.labour_hrs;
         v_cost_rec.total_cost := c.total_cost;
         v_cost_key := (c.area_type)||TO_CHAR(c.lvco_cnt,'00')||TO_CHAR(c.hvco_cnt,'00')||TO_CHAR(c.hvsp_cnt,'00');
         v_cost_tab(v_cost_key) :=  v_cost_rec;
      END LOOP;

      /* get life expectancy for various eqp types into w-s table */
      
      SELECT TO_NUMBER(parameter_val)
      INTO   v_bay_life_exp
      FROM structools.st_parameter_scenario
      WHERE PARAMETER_ID = 'LIFE_EXPECTANCY'
      AND   parameter_egi = 'BAY'
      AND   scenario_id = v_parm_scen_id_life_exp
      ;    

--      v_life_exp_tab.DELETE;
--      FOR c IN life_exp_csr LOOP
--         IF c.parameter_egi = 'PWOD' THEN
--            v_life_exp_tab(c.parameter_egi||'-'||c.parameter_wood_type) := TO_NUMBER(c.parameter_val);
--         ELSE   
--            v_life_exp_tab(c.parameter_egi) := TO_NUMBER(c.parameter_val);
--         END IF;
--      END LOOP;

      /* get the priority adjustment factor for feeder category into w-s table */
--      v_fdr_cat_adj_tab.DELETE;
--      FOR c IN fdr_cat_adj_csr LOOP
--         v_fdr_cat_adj_tab(c.fdr_cat) := c.fdr_cat_adj_factor;
--      END LOOP;
      
   END populate_working_arrays;
               
   /* This procedure obtains sif weightings for all the sif categories for feeder category for parameter scenario.                   */
   /* It populates global package table (assoc array) v_fdr_cat_sif_tab and also a global temp table st_gtt_fdr_cat_adj.             */
   /* The gtt table is only used in create_summary_for_opensolver procedure; other procs use the associative array to obtain values  */
   /* for feeder category, for efficiency reasons.                                                                                   */
   /* Both are populated here to make maintenance easier if a change is required (as it should apply to both, as both should contain */
   /* th same data.                                                                                                                  */
   PROCEDURE get_sif_weightings(i_parm_scen_id_sif_weighting  IN PLS_INTEGER, i_parm_scen_id_fdr_cat_adj  IN PLS_INTEGER) IS
         
      v_risk_pubsafety_sif_pcnt     FLOAT(126);
      v_risk_wforce_sif_pcnt        FLOAT(126); 
      v_risk_reliability_sif_pcnt   FLOAT(126);
      v_risk_env_sif_pcnt           FLOAT(126); 
      v_risk_fin_sif_pcnt           FLOAT(126);
      v_sif_tot_pcnt                FLOAT(126);
      v_parameter_fdr_cat           VARCHAR2(35);
      v_fdr_cat_prev                VARCHAR2(35);
      v_fdr_cat                     VARCHAR2(35);
      v_fdr_cat_adj_factor          NUMBER;
      v_sql                         VARCHAR2(1000);

        CURSOR fdr_cat_sif_and_adj_csr IS
         WITH fdr_cat_sif AS
         (
          SELECT parameter_risk_comp
                 ,TO_NUMBER(parameter_val)    AS fdr_cat_sif_pcnt
               ,parameter_fdr_cat
         FROM structools.st_parameter_scenario
         WHERE PARAMETER_ID = 'SIF_WEIGHTING'
         AND   scenario_id = i_parm_scen_id_sif_weighting
         )
         ,
         fdr_cat_adj AS
         (
         SELECT TO_NUMBER(parameter_val)   AS fdr_cat_adj_factor
                 ,parameter_fdr_cat
         FROM structools.st_parameter_scenario
         WHERE PARAMETER_ID = 'FDR_CAT_ADJ'
         AND   scenario_id = i_parm_scen_id_fdr_cat_adj
         )
         SELECT A.parameter_fdr_cat
               ,A.fdr_cat_adj_factor
               ,b.parameter_risk_comp
               ,b.fdr_cat_sif_pcnt
         FROM  fdr_cat_adj          A  INNER JOIN
               fdr_cat_sif          b
         ON A.parameter_fdr_cat = b.parameter_fdr_cat
         ORDER BY parameter_fdr_cat
                        
      ;    
         
   BEGIN         
   
      DELETE FROM structools.st_gtt_fdr_cat_adj;
      v_fdr_cat_sif_tab.DELETE;
                              
      v_fdr_cat_prev := ' ';
      FOR c IN fdr_cat_sif_and_adj_csr LOOP
         IF c.parameter_fdr_cat != v_fdr_cat_prev AND v_fdr_cat_prev != ' ' THEN
            v_sif_tot_pcnt := v_risk_pubsafety_sif_pcnt + v_risk_wforce_sif_pcnt + v_risk_reliability_sif_pcnt + v_risk_env_sif_pcnt --; --not always 100
                           + v_risk_fin_sif_pcnt;
                             
            v_risk_pubsafety_sif       := 5 * v_risk_pubsafety_sif_pcnt   / v_sif_tot_pcnt;
            v_risk_wforce_sif          := 5 * v_risk_wforce_sif_pcnt      / v_sif_tot_pcnt;
            v_risk_reliability_sif     := 5 * v_risk_reliability_sif_pcnt / v_sif_tot_pcnt;
            v_risk_env_sif             := 5 * v_risk_env_sif_pcnt         / v_sif_tot_pcnt; 
            v_risk_fin_sif             := 5 * v_risk_fin_sif_pcnt         / v_sif_tot_pcnt;

            v_fdr_cat_sif_tab(v_fdr_cat_prev).pubsafety_sif          := v_risk_pubsafety_sif;
            v_fdr_cat_sif_tab(v_fdr_cat_prev).wforce_sif             := v_risk_wforce_sif;
            v_fdr_cat_sif_tab(v_fdr_cat_prev).reliability_sif        := v_risk_reliability_sif;
            v_fdr_cat_sif_tab(v_fdr_cat_prev).env_sif                := v_risk_env_sif;
            v_fdr_cat_sif_tab(v_fdr_cat_prev).fin_sif                := v_risk_fin_sif;
            v_fdr_cat_sif_tab(v_fdr_cat_prev).adj_factor             := v_fdr_cat_adj_factor;          
                        
            INSERT INTO structools.st_gtt_fdr_cat_adj
                VALUES(v_fdr_cat_prev
                       ,v_risk_pubsafety_sif 
                     ,v_risk_wforce_sif  
                     ,v_risk_reliability_sif
                     ,v_risk_env_sif
                     ,v_fdr_cat_adj_factor
                     ,v_risk_fin_sif
                     )
            ;    
         END IF;
         IF c.parameter_risk_comp = 'PUBSAFETY' THEN
             v_risk_pubsafety_sif_pcnt    := c.fdr_cat_sif_pcnt;
         ELSIF c.parameter_risk_comp = 'WFORCE' THEN
             v_risk_wforce_sif_pcnt    := c.fdr_cat_sif_pcnt;
         ELSIF c.parameter_risk_comp = 'RELIABILITY' THEN
             v_risk_reliability_sif_pcnt    := c.fdr_cat_sif_pcnt;
         ELSIF c.parameter_risk_comp = 'ENV' THEN
             v_risk_env_sif_pcnt    := c.fdr_cat_sif_pcnt;
         ELSIF c.parameter_risk_comp = 'FIN' THEN
             v_risk_fin_sif_pcnt    := c.fdr_cat_sif_pcnt;
         END IF;
         v_fdr_cat_adj_factor := c.fdr_cat_adj_factor;
         v_fdr_cat_prev := c.parameter_fdr_cat;
         
      END LOOP;

      /* Process last fdr cat */
      v_sif_tot_pcnt := v_risk_pubsafety_sif_pcnt + v_risk_wforce_sif_pcnt + v_risk_reliability_sif_pcnt + v_risk_env_sif_pcnt --; --not always 100
                     + v_risk_fin_sif_pcnt;
                                                                                                                           
      v_risk_pubsafety_sif       := 5 * v_risk_pubsafety_sif_pcnt   / v_sif_tot_pcnt;
      v_risk_wforce_sif          := 5 * v_risk_wforce_sif_pcnt      / v_sif_tot_pcnt;
      v_risk_reliability_sif     := 5 * v_risk_reliability_sif_pcnt / v_sif_tot_pcnt;
      v_risk_env_sif             := 5 * v_risk_env_sif_pcnt         / v_sif_tot_pcnt; 
      v_risk_fin_sif             := 5 * v_risk_fin_sif_pcnt         / v_sif_tot_pcnt;

      v_fdr_cat_sif_tab(v_fdr_cat_prev).pubsafety_sif          := v_risk_pubsafety_sif;
      v_fdr_cat_sif_tab(v_fdr_cat_prev).wforce_sif             := v_risk_wforce_sif;
      v_fdr_cat_sif_tab(v_fdr_cat_prev).reliability_sif        := v_risk_reliability_sif;
      v_fdr_cat_sif_tab(v_fdr_cat_prev).env_sif                := v_risk_env_sif;
      v_fdr_cat_sif_tab(v_fdr_cat_prev).fin_sif                := v_risk_fin_sif;
      v_fdr_cat_sif_tab(v_fdr_cat_prev).adj_factor             := v_fdr_cat_adj_factor;          

      INSERT INTO structools.st_gtt_fdr_cat_adj
         VALUES(v_fdr_cat_prev
               ,v_risk_pubsafety_sif 
               ,v_risk_wforce_sif  
               ,v_risk_reliability_sif
               ,v_risk_env_sif
               ,v_fdr_cat_adj_factor
               ,v_risk_fin_sif
               )
      ;    

--      dbms_output.put_line('st_gtt_fdr_cat_adj TABLE dump');   
--        FOR c IN (SELECT * FROM structools.st_gtt_fdr_cat_adj ORDER BY fdr_cat) LOOP
--          dbms_output.put_line(c.fdr_cat||' '||c.sif_weight_pubsafety||' '||c.sif_weight_wforce||' '||c.sif_weight_reliability
--                                 ||' '||c.sif_weight_env||' '||c.sif_weight_fin||' '||c.adj_factor);
--      END LOOP;            
--      
--      dbms_output.put_line('v_fdr_cat_sif_tab assoc. array dump');   
--      v_fdr_cat_prev := v_fdr_cat_sif_tab.FIRST;
--      WHILE v_fdr_cat_prev IS NOT NULL LOOP
--          dbms_output.put_line(v_fdr_cat_prev||' '||v_fdr_cat_sif_tab(v_fdr_cat_prev).pubsafety_sif
--                                                ||' '||v_fdr_cat_sif_tab(v_fdr_cat_prev).wforce_sif
--                                                ||' '||v_fdr_cat_sif_tab(v_fdr_cat_prev).reliability_sif
--                                                ||' '||v_fdr_cat_sif_tab(v_fdr_cat_prev).env_sif
--                                                ||' '||v_fdr_cat_sif_tab(v_fdr_cat_prev).fin_sif
--                                                ||' '||v_fdr_cat_sif_tab(v_fdr_cat_prev).adj_factor);
--         v_fdr_cat_prev := v_fdr_cat_sif_tab.NEXT(v_fdr_cat_prev);
--      END LOOP;   
            
   END get_sif_weightings;

   PROCEDURE get_risk_for_year(i_pick_id IN VARCHAR2, i_egi IN VARCHAR2, i_part_cde IN VARCHAR2, i_event IN VARCHAR2, i_age IN INTEGER
                              ,i_exec_year IN INTEGER  --, i_init_age IN INTEGER
                              ,i_variation IN CHAR, i_del_year IN PLS_INTEGER
                              ,i_fdr_cat        IN VARCHAR2
                              ,o_risk OUT FLOAT   --, o_risk_noaction OUT FLOAT
                              ,o_risk_npv OUT FLOAT, o_risk_noaction_npv OUT FLOAT
                              ,o_risk_npv_sif OUT FLOAT, o_risk_noaction_npv_sif OUT FLOAT) AS


      v_risk_key        VARCHAR2(40);
      v_risk_val        FLOAT(126) := NULL;
      v_idx_n           INTEGER;
      v_idx_p           INTEGER;
      v_idx_f           INTEGER;
      v_event           VARCHAR2(30);
      v_event_1         CHAR(1);
      v_risk_rec        risk_rec_typ;
      v_risk_tab        risk_tab_typ := risk_tab_typ();
      v_pick_id         VARCHAR2(30) := SUBSTR(i_pick_id, 2, 29);
      v_age             INTEGER;
      v_del_year        PLS_INTEGER;
      v_fire_frac             FLOAT(126);
      v_eshock_frac           FLOAT(126);
      v_physimp_frac          FLOAT(126);
      v_wforce_frac           FLOAT(126);
      v_reliability_frac      FLOAT(126);
      v_env_frac              FLOAT(126);
      v_fin_frac              FLOAT(126);

      /* private procedures for get_risk_for_year */
      PROCEDURE store_risk_pwod2(i_pick_id IN VARCHAR2, i_risk_rec IN risk_rec_typ, i_action IN VARCHAR2) IS
         
         v_risk_key                    VARCHAR2(40);
         v_risk_tab                    risk_tab_typ := risk_tab_typ();


         v_pick_id                     varchar2(10);
   
      BEGIN

         v_risk_key := i_pick_id||i_action;
            
         v_risk_tab.EXTEND(21);
         v_risk_tab(1) := i_risk_rec.yr0;
         v_risk_tab(2) := i_risk_rec.yr1;
         v_risk_tab(3) := i_risk_rec.yr2;
         v_risk_tab(4) := i_risk_rec.yr3;
         v_risk_tab(5) := i_risk_rec.yr4;
         v_risk_tab(6) := i_risk_rec.yr5;
         v_risk_tab(7) := i_risk_rec.yr6;
         v_risk_tab(8) := i_risk_rec.yr7;
         v_risk_tab(9) := i_risk_rec.yr8;
         v_risk_tab(10) := i_risk_rec.yr9;
         v_risk_tab(11) := i_risk_rec.yr10;         
         v_risk_tab(12) := i_risk_rec.yr11;
         v_risk_tab(13) := i_risk_rec.yr12;
         v_risk_tab(14) := i_risk_rec.yr13;
         v_risk_tab(15) := i_risk_rec.yr14;
         v_risk_tab(16) := i_risk_rec.yr15;
         v_risk_tab(17) := i_risk_rec.yr16;
         v_risk_tab(18) := i_risk_rec.yr17;
         v_risk_tab(19) := i_risk_rec.yr18;
         v_risk_tab(20) := i_risk_rec.yr19;
         v_risk_tab(21) := i_risk_rec.yr20;
            
         IF i_action = 'NNPV15' THEN
            v_risk_tab.EXTEND(7);
            v_risk_tab(22) := i_risk_rec.fire_frac;
            v_risk_tab(23) := i_risk_rec.eshock_frac;
            v_risk_tab(24) := i_risk_rec.wforce_frac;
            v_risk_tab(25) := i_risk_rec.reliability_frac;
            v_risk_tab(26) := i_risk_rec.physimp_frac;
            v_risk_tab(27) := i_risk_rec.env_frac;
            v_risk_tab(28) := i_risk_rec.fin_frac;
         END IF;
            
         v_pwod_risk_tab(v_risk_key) := v_risk_tab;
         
      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line ('store_risk_pwod2, i_pick_id = '||i_pick_id||' i_action = '||i_action);   
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;
      END store_risk_pwod2;
         
      PROCEDURE  store_risk_eqp2(i_pick_id IN VARCHAR2, i_risk_rec IN risk_rec_typ, i_action IN VARCHAR2) IS
         
         v_risk_key                    VARCHAR2(40);
         v_risk_tab                    risk_tab_typ := risk_tab_typ();
            
      BEGIN

         v_risk_key := i_pick_id||i_action;
            
         v_risk_tab.EXTEND(21);
         v_risk_tab(1) := i_risk_rec.yr0;
         v_risk_tab(2) := i_risk_rec.yr1;
         v_risk_tab(3) := i_risk_rec.yr2;
         v_risk_tab(4) := i_risk_rec.yr3;
         v_risk_tab(5) := i_risk_rec.yr4;
         v_risk_tab(6) := i_risk_rec.yr5;
         v_risk_tab(7) := i_risk_rec.yr6;
         v_risk_tab(8) := i_risk_rec.yr7;
         v_risk_tab(9) := i_risk_rec.yr8;
         v_risk_tab(10) := i_risk_rec.yr9;
         v_risk_tab(11) := i_risk_rec.yr10;         
         v_risk_tab(12) := i_risk_rec.yr11;
         v_risk_tab(13) := i_risk_rec.yr12;
         v_risk_tab(14) := i_risk_rec.yr13;
         v_risk_tab(15) := i_risk_rec.yr14;
         v_risk_tab(16) := i_risk_rec.yr15;
         v_risk_tab(17) := i_risk_rec.yr16;
         v_risk_tab(18) := i_risk_rec.yr17;
         v_risk_tab(19) := i_risk_rec.yr18;
         v_risk_tab(20) := i_risk_rec.yr19;
         v_risk_tab(21) := i_risk_rec.yr20;
            
         IF i_action = 'NNPV15' THEN
            v_risk_tab.EXTEND(7);
            v_risk_tab(22) := i_risk_rec.fire_frac;
            v_risk_tab(23) := i_risk_rec.eshock_frac;
            v_risk_tab(24) := i_risk_rec.wforce_frac;
            v_risk_tab(25) := i_risk_rec.reliability_frac;
            v_risk_tab(26) := i_risk_rec.physimp_frac;
            v_risk_tab(27) := i_risk_rec.env_frac;
            v_risk_tab(28) := i_risk_rec.fin_frac;
         END IF;
            
         v_eqp_risk_tab(v_risk_key) := v_risk_tab;
         
      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line ('store_risk_eqp2, i_pick_id = '||i_pick_id||' i_action = '||i_action);   
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;      
            
      END store_risk_eqp2;
      
   /* mainline for get_risk_for_year */   
   BEGIN
      
      v_risk_pubsafety_sif   := v_fdr_cat_sif_tab(i_fdr_cat).pubsafety_sif;
      v_risk_wforce_sif      := v_fdr_cat_sif_tab(i_fdr_cat).wforce_sif;
      v_risk_reliability_sif := v_fdr_cat_sif_tab(i_fdr_cat).reliability_sif;
      v_risk_env_sif         := v_fdr_cat_sif_tab(i_fdr_cat).env_sif;            
      v_risk_fin_sif         := v_fdr_cat_sif_tab(i_fdr_cat).fin_sif;            
      
      o_risk := NULL;
      o_risk_npv := NULL;
      o_risk_npv_sif := NULL;
      o_risk_noaction_npv := NULL;
      o_risk_noaction_npv_sif := NULL;
         
      IF i_del_year IS NULL THEN
         v_del_year := v_this_year;
      ELSE
         v_del_year := i_del_year;
      END IF;
               
      v_event := i_event;
         
      v_idx_n := i_exec_year - v_del_year + 1;
      IF i_event = 'NOACTION' THEN
         IF i_variation = 'P' THEN
            v_event := 'REPLACE';
            v_idx_p := i_age + 1;
         ELSIF i_variation = 'F' THEN
            v_event := 'REINFORCE';
            v_idx_f := i_exec_year - v_this_year + 1;
         END IF;
      ELSIF i_event = 'REPLACE' THEN
         v_idx_p := i_age + 1;
      ELSIF i_event = 'REINFORCE' THEN
         v_idx_f := i_exec_year - v_del_year + 1;
      END IF;
            
      IF v_idx_n > 21 THEN
         v_idx_n := 21;
      END IF;
      IF v_idx_p > 21 THEN
         v_idx_p := 21;
      END IF;
      IF v_idx_f > 21 THEN
         v_idx_f := 21;
      END IF;
            
      IF v_event = 'NOACTION' THEN
         v_event_1 := 'N';
      ELSIF v_event = 'REPLACE' THEN
         v_event_1 := 'P';
      ELSIF v_event = 'REINFORCE' THEN
         v_event_1 := 'F';
      END IF;
            
      v_risk_key := TRIM(i_pick_id)||v_event_1;
      
      IF i_part_cde = ' ' THEN
         BEGIN 
            IF v_event_1 = 'N' THEN        
               IF i_egi = 'PWOD' THEN       
                  o_risk := v_pwod_risk_tab(v_risk_key)(v_idx_n);
               ELSE  
                  o_risk := v_eqp_risk_tab(v_risk_key)(v_idx_n);
               END IF;
            ELSIF v_event_1 = 'F' THEN
               IF NOT v_pwod_risk_tab.EXISTS(i_pick_id||'F') THEN
                  SELECT   A.*
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL 
                       ,NULL 
                  INTO   v_risk_rec
                  FROM   structools.st_risk_pwod_reinf  A
                  WHERE  pick_id = v_pick_id;
                  store_risk_pwod2(i_pick_id, v_risk_rec, 'F');
               END IF;
               o_risk := v_pwod_risk_tab(i_pick_id||'F')(v_idx_f);
            ELSE     /* v_event_1 = 'P' */
               IF i_egi = 'PWOD' THEN
                  IF NOT v_pwod_risk_tab.EXISTS(i_pick_id||'P') THEN
                     SELECT   A.*
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL 
                             ,NULL 
                     INTO   v_risk_rec
                     FROM   structools.st_risk_pwod_replace A
                     WHERE  pick_id = v_pick_id;
                     store_risk_pwod2(i_pick_id, v_risk_rec, 'P');
                  END IF;
               ELSIF NOT v_eqp_risk_tab.EXISTS(i_pick_id||'P') THEN
                  IF i_egi = 'DSTR' THEN
                     SELECT   A.*
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL 
                             ,NULL 
                     INTO   v_risk_rec
                     FROM   structools.st_risk_dstr_replace A
                     WHERE  pick_id = v_pick_id;
                  ELSIF i_egi = 'DOF' THEN
                     SELECT   A.*
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL 
                             ,NULL 
                     INTO   v_risk_rec
                     FROM   structools.st_risk_dof_replace A
                     WHERE  pick_id = v_pick_id;
                  ELSIF i_egi = 'RECL' OR i_egi = 'LBS' THEN
                     SELECT   A.*
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL 
                             ,NULL 
                     INTO   v_risk_rec
                     FROM   structools.st_risk_recl_replace A
                     WHERE  pick_id = v_pick_id;
                  ELSIF i_egi = 'SECT' THEN
                     SELECT   A.*
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL 
                             ,NULL 
                     INTO   v_risk_rec
                     FROM   structools.st_risk_sect_replace A
                     WHERE  pick_id = v_pick_id;
                  ELSIF i_egi = 'PTSD' THEN
                     SELECT   A.*
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL 
                             ,NULL 
                     INTO   v_risk_rec
                     FROM   structools.st_risk_ptsd_replace A
                     WHERE  pick_id = v_pick_id;
                  ELSIF i_egi = 'RGTR' THEN
                     SELECT   A.*
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL 
                             ,NULL 
                     INTO   v_risk_rec
                     FROM   structools.st_risk_rgtr_replace A
                     WHERE  pick_id = v_pick_id;
                  END IF;
                  store_risk_eqp2(i_pick_id, v_risk_rec, 'P');
               END IF;
               IF i_egi = 'PWOD' THEN
                  o_risk := v_pwod_risk_tab(i_pick_id||'P')(v_idx_p);
               ELSE
                  o_risk := v_eqp_risk_tab(i_pick_id||'P')(v_idx_p);
               END IF;
            END IF;
            IF i_event = 'REPLACE' OR i_event = 'REINFORCE' THEN     /* get noaaction_npv15 risk */
               IF i_egi = 'PWOD' THEN
                  IF NOT v_pwod_risk_tab.EXISTS(i_pick_id||'NNPV15') THEN
                     SELECT   A.*
                     INTO   v_risk_rec
                     FROM   structools.st_risk_pwod_noaction_npv15 A
                     WHERE  pick_id = v_pick_id;
                     store_risk_pwod2(i_pick_id, v_risk_rec, 'NNPV15');
                  END IF;
               ELSIF NOT v_eqp_risk_tab.EXISTS(i_pick_id||'NNPV15') THEN
                  IF i_egi = 'DSTR' THEN
                     SELECT   A.*
                     INTO   v_risk_rec
                     FROM   structools.st_risk_dstr_noaction_npv15 A
                     WHERE  pick_id = v_pick_id;
                  ELSIF i_egi = 'DOF' THEN
                     SELECT   A.*
                     INTO   v_risk_rec
                     FROM   structools.st_risk_dof_noaction_npv15 A
                     WHERE  pick_id = v_pick_id;
                  ELSIF i_egi = 'RECL' OR i_egi = 'LBS' THEN
                     SELECT   A.*
                     INTO   v_risk_rec
                     FROM   structools.st_risk_recl_noaction_npv15 A
                     WHERE  pick_id = v_pick_id;
                  ELSIF i_egi = 'SECT' THEN
                     SELECT   A.*
                     INTO   v_risk_rec
                     FROM   structools.st_risk_sect_noaction_npv15 A
                     WHERE  pick_id = v_pick_id;
                  ELSIF i_egi = 'PTSD' THEN
                     SELECT   A.*
                     INTO   v_risk_rec
                     FROM   structools.st_risk_ptsd_noaction_npv15 A
                     WHERE  pick_id = v_pick_id;
                  ELSIF i_egi = 'RGTR' THEN
                     SELECT   A.*
                     INTO   v_risk_rec
                     FROM   structools.st_risk_rgtr_noaction_npv15 A
                     WHERE  pick_id = v_pick_id;
                  END IF;
                  store_risk_eqp2(i_pick_id, v_risk_rec, 'NNPV15');
               END IF;
               IF i_egi = 'PWOD' THEN
                  o_risk_noaction_npv := v_pwod_risk_tab(i_pick_id||'NNPV15')(v_idx_n);
                  v_fire_frac          := v_pwod_risk_tab(i_pick_id||'NNPV15')(22);
                  v_eshock_frac        := v_pwod_risk_tab(i_pick_id||'NNPV15')(23);
                  v_wforce_frac        := v_pwod_risk_tab(i_pick_id||'NNPV15')(24);
                  v_reliability_frac   := v_pwod_risk_tab(i_pick_id||'NNPV15')(25);
                  v_physimp_frac       := v_pwod_risk_tab(i_pick_id||'NNPV15')(26);
                  v_env_frac           := v_pwod_risk_tab(i_pick_id||'NNPV15')(27);
                  v_fin_frac           := v_pwod_risk_tab(i_pick_id||'NNPV15')(28);
               ELSE
                  o_risk_noaction_npv := v_eqp_risk_tab(i_pick_id||'NNPV15')(v_idx_n);
                  v_fire_frac          := v_eqp_risk_tab(i_pick_id||'NNPV15')(22);
                  v_eshock_frac        := v_eqp_risk_tab(i_pick_id||'NNPV15')(23);
                  v_wforce_frac        := v_eqp_risk_tab(i_pick_id||'NNPV15')(24);
                  v_reliability_frac   := v_eqp_risk_tab(i_pick_id||'NNPV15')(25);
                  v_physimp_frac       := v_eqp_risk_tab(i_pick_id||'NNPV15')(26);
                  v_env_frac           := v_eqp_risk_tab(i_pick_id||'NNPV15')(27);
                  v_fin_frac           := v_eqp_risk_tab(i_pick_id||'NNPV15')(28);
               END IF;
                  
               /* get post-action npv15 risk */
               IF i_event = 'REINFORCE' THEN   
                  IF NOT v_pwod_risk_tab.EXISTS(i_pick_id||'FNPV15') THEN
                     SELECT   A.*
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL
                             ,NULL 
                             ,NULL 
                     INTO   v_risk_rec
                     FROM   structools.st_risk_pwod_reinf_npv15 A
                     WHERE  pick_id = v_pick_id;
                     store_risk_pwod2(i_pick_id, v_risk_rec, 'FNPV15');
                  END IF;
                  o_risk_npv := v_pwod_risk_tab(i_pick_id||'FNPV15')(v_idx_f);
               ELSE /* i_event = REPLACE */
                  IF i_egi = 'PWOD' THEN
                     IF NOT v_pwod_risk_tab.EXISTS(i_pick_id||'PNPV15')THEN
                        SELECT   A.*
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL 
                                ,NULL 
                        INTO   v_risk_rec
                        FROM   structools.st_risk_pwod_replace_npv15 A
                        WHERE  pick_id = v_pick_id;
                        store_risk_pwod2(i_pick_id, v_risk_rec, 'PNPV15');
                     END IF;
                  ELSIF NOT v_eqp_risk_tab.EXISTS(i_pick_id||'PNPV15')THEN
                     IF i_egi = 'DSTR' THEN
                        SELECT   A.*
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL 
                                ,NULL 
                        INTO   v_risk_rec
                        FROM   structools.st_risk_dstr_replace_npv15 A
                        WHERE  pick_id = v_pick_id;
                     ELSIF i_egi = 'DOF' THEN
                        SELECT   A.*
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL 
                                ,NULL 
                        INTO   v_risk_rec
                        FROM   structools.st_risk_dof_replace_npv15 A
                        WHERE  pick_id = v_pick_id;
                     ELSIF i_egi = 'RECL' OR i_egi = 'LBS' THEN
                        SELECT   A.*
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL 
                                ,NULL 
                        INTO   v_risk_rec
                        FROM   structools.st_risk_recl_replace_npv15 A
                        WHERE  pick_id = v_pick_id;
                     ELSIF i_egi = 'SECT' THEN
                        SELECT   A.*
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL 
                                ,NULL 
                        INTO   v_risk_rec
                        FROM   structools.st_risk_sect_replace_npv15 A
                        WHERE  pick_id = v_pick_id;
                     ELSIF i_egi = 'PTSD' THEN
                        SELECT   A.*
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL 
                                ,NULL 
                        INTO   v_risk_rec
                        FROM   structools.st_risk_ptsd_replace_npv15 A
                        WHERE  pick_id = v_pick_id;
                     ELSIF i_egi = 'RGTR' THEN
                        SELECT   A.*
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL
                                ,NULL 
                                ,NULL 
                        INTO   v_risk_rec
                        FROM   structools.st_risk_rgtr_replace_npv15 A
                        WHERE  pick_id = v_pick_id;
                     END IF;
                     store_risk_eqp2(i_pick_id, v_risk_rec, 'PNPV15');
                  END IF;
                  IF i_egi = 'PWOD' THEN
                     o_risk_npv := v_pwod_risk_tab(i_pick_id||'PNPV15')(v_idx_p);
                  ELSE
                     o_risk_npv := v_eqp_risk_tab(i_pick_id||'PNPV15')(v_idx_p);
                  END IF;
               END IF;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      ELSE /* i_part_cde != ' '; ie it is a stay or a crossarm */
         BEGIN 
            IF v_event_1 = 'N' THEN               
               v_risk_key := TRIM(i_pick_id)||i_part_cde||v_event_1;
               o_risk := v_pole_part_risk_tab(v_risk_key)(v_idx_n);
            ELSE  
               v_risk_key := TRIM(i_pick_id)||i_part_cde||'P';
               o_risk := v_pole_part_risk_tab(v_risk_key)(v_idx_p);
            END IF;
            IF i_event = 'REPLACE' THEN
               v_risk_key := i_pick_id||i_part_cde||'NNPV15';
               o_risk_noaction_npv := v_pole_part_risk_tab(v_risk_key)(v_idx_n);

               v_fire_frac             := v_pole_part_risk_tab(v_risk_key)(22);
               v_eshock_frac           := v_pole_part_risk_tab(v_risk_key)(23);
               v_wforce_frac           := v_pole_part_risk_tab(v_risk_key)(24);
               v_reliability_frac      := v_pole_part_risk_tab(v_risk_key)(25);
               v_physimp_frac          := v_pole_part_risk_tab(v_risk_key)(26);
               v_env_frac              := v_pole_part_risk_tab(v_risk_key)(27);
               v_fin_frac              := v_pole_part_risk_tab(v_risk_key)(28);
                  
               v_risk_key := i_pick_id||i_part_cde||'PNPV15';
               o_risk_npv := v_pole_part_risk_tab(v_risk_key)(v_idx_p);
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      IF i_event = 'REPLACE' OR i_event = 'REINFORCE' THEN
         o_risk_npv_sif := o_risk_npv * (v_fire_frac + v_eshock_frac + v_physimp_frac) 
                                                                         * v_risk_pubsafety_sif
                         + o_risk_npv * v_wforce_frac       * v_risk_wforce_sif
                         + o_risk_npv * v_reliability_frac  * v_risk_reliability_sif
                         + o_risk_npv * v_env_frac          * v_risk_env_sif
                         + o_risk_npv * v_fin_frac          * v_risk_fin_sif;
         o_risk_noaction_npv_sif := o_risk_noaction_npv * (v_fire_frac + v_eshock_frac + v_physimp_frac) 
                                                                              * v_risk_pubsafety_sif
                         + o_risk_noaction_npv * v_wforce_frac       * v_risk_wforce_sif
                         + o_risk_noaction_npv * v_reliability_frac  * v_risk_reliability_sif
                         + o_risk_noaction_npv * v_env_frac          * v_risk_env_sif
                         + o_risk_noaction_npv * v_fin_frac          * v_risk_fin_sif;
      END IF;      

   EXCEPTION
      WHEN OTHERS THEN
        dbms_output.put_line(i_pick_id||' '||i_egi||' i age = '||i_age --||' i_init_age = '||i_init_age
                              ||' i_event = '||i_event||' v_event = '||v_event||' i_exec_year = '||i_exec_year||' v_idx_p = '||v_idx_p
                              ||' v_idx_n = '||v_idx_n);
        dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
        dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END get_risk_for_year;
   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx     
   PROCEDURE execute_run_request(i_run_id  IN  PLS_INTEGER) IS
                                
                                 
      /*****************************************************************/
      /* Global declarations for execute_run_request procedure               */
      /*****************************************************************/

      v_sql             VARCHAR2(1000);
      v_name_used_excp EXCEPTION;
      PRAGMA EXCEPTION_INIT (v_name_used_excp,-955);
         
      v_already_indexed_excp EXCEPTION;
      PRAGMA EXCEPTION_INIT (v_already_indexed_excp,-1408);
      v_dummy_int          PLS_INTEGER;    
      v_run_ts             DATE;
      v_egi_vc2              varCHAR2(5);
      v_rem                PLS_INTEGER;
      v_pz_repl_len_m      PLS_INTEGER;
      v_initial_bay_sel_len_km   PLS_INTEGER;
      v_initial_bay_sel_len_m    PLS_INTEGER;
      v_tot_pz_repl_len_km       PLS_INTEGER;
      v_tot_pz_repl_len_m        PLS_INTEGER;
      v_dummy_pick_id            VARCHAR2(10);
      v_dummy_calendar_year      PLS_INTEGER;
      v_dummy_cnt                PLS_INTEGER;
      
--      TYPE mtzn_sched_tab_typ    IS TABLE OF CHAR(1) INDEX BY VARCHAR2(50);          --pickid init
--      v_mtzn_sched_tab           mtzn_sched_tab_typ; 
      
      v_risk_key_tmp          VARCHAR2(40);
      TYPE egi_tab_typ   IS TABLE OF CHAR(1) INDEX BY VARCHAR2(30);
      v_egi_tab            egi_tab_typ;
      
      v_first_year               PLS_INTEGER;
      v_last_year                PLS_INTEGER;
      v_run_years                PLS_INTEGER;
      v_run_years_char           CHAR(2);
      v_run_years_sum            PLS_INTEGER;         
      v_run_years_sum_char       CHAR(2);             
      v_exec_year                PLS_INTEGER;
      v_treatment_type           VARCHAR2(20); --FULL, PARTIAL
      v_upgrade_poles_under_cond CHAR(1);       -- Y or N
      v_suppress_pickids 					CHAR(1);       -- Y or N
      v_suppress_pickids_prog_id 			INTEGER;
      v_cost_treatment_type      VARCHAR2(20); --FULL, STANDALONE
      v_run_id                   PLS_INTEGER;
      v_risk_key                 VARCHAR2(40);
--      v_parent_run_id            PLS_INTEGER;
--      v_parent_run_id_first      PLS_INTEGER;
--      v_parent_run_id_last       PLS_INTEGER;
--      v_parent_run_program_id    PLS_INTEGER;
      --v_mtzn_committed_version_id   PLS_INTEGER;       --pickid init
            
      TYPE cond_tab_typ         IS TABLE OF VARCHAR2(50) INDEX BY VARCHAR2(9);
      v_cond_tab              cond_tab_typ;
      
      v_fsrtg                    VARCHAR2(200);
      
      TYPE  fsrtg_tab_typ        IS TABLE OF CHAR(1) INDEX BY VARCHAR2(30);
      v_fsrtg_tab                fsrtg_tab_typ;

      
      CURSOR eqp_csr IS
         
--         WITH vic AS
--         (
--         SELECT DISTINCT aa.prnt_plnt_no
--         FROM
--         (
--         SELECT DISTINCT A.prnt_plnt_no 
--                        ,b.pick_id
--                        ,b.event
--         FROM   structools.st_equipment               A  LEFT OUTER JOIN
--                structools.st_pickid_committed        b
--         ON     A.pick_id = b.pick_id   
--         AND    A.part_cde = b.part_cde             
--         WHERE  A.equip_grp_id IN ('PWOD','PAUS')
--         AND A.part_cde = ' '
--         --AND prnt_plnt_no = 'LS8190794'         -- graz delete
--         ) aa
--         WHERE aa.event IS NULL OR aa.event NOT IN ('SUPP', 'PUP')
--         )
--         
         WITH vic AS
         (
         SELECT DISTINCT A.prnt_plnt_no 
         FROM   structools.st_equipment        A        
         WHERE  A.equip_grp_id IN ('PWOD','PAUS')
         AND A.part_cde = ' '
         )
         ,
         COMMITTED AS
         (
         SELECT pick_id                --commited poles and eqp
               ,' ' AS  part_cde
               ,event
               ,commit_yr   AS del_year
         FROM structools.st_pickid_committed
         WHERE part_cde = ' '
         AND pick_id NOT LIKE 'B%'
         UNION
         SELECT A.pick_id              -- xarms and stays by the virtue of being on top of replaced poles
               ,b.part_cde
               ,A.event
               ,A.commit_yr AS del_year
         FROM structools.st_pickid_committed       A  INNER JOIN
               structools.st_equipment             b
         ON A.pick_id = b.pick_id
         WHERE A.part_cde = ' ' AND b.part_defective = 'Y'
         AND b.part_cde !=  ' ' 
         AND A.pick_id LIKE 'S%'
         AND A.event = 'REPLACE'
         UNION
         SELECT A.pick_id              -- xarms and stays scheduled for replace im their own right
               ,'HVXM'            AS part_cde
               ,A.event
               ,A.commit_yr AS del_year
         FROM structools.st_pickid_committed       A  
         WHERE A.part_cde = 'XARM'
         AND A.pick_id LIKE 'S%'
         AND A.event = 'REPLACE'
         UNION
         SELECT A.pick_id              -- xarms and stays scheduled for replace im their own right
               ,'LVXM'            AS part_cde
               ,A.event
               ,A.commit_yr AS del_year
         FROM structools.st_pickid_committed       A  
         WHERE A.part_cde = 'XARM'
         AND A.pick_id LIKE 'S%'
         AND A.event = 'REPLACE'
         UNION
         SELECT A.pick_id              -- xarms and stays scheduled for replace im their own right
               ,'ASTAY'            AS part_cde
               ,A.event
               ,A.commit_yr AS del_year
         FROM structools.st_pickid_committed       A  
         WHERE A.part_cde = 'STAY'
         AND A.pick_id LIKE 'S%'
         AND A.event = 'REPLACE'
         UNION
         SELECT A.pick_id              -- xarms and stays scheduled for replace im their own right
               ,'GSTAY'            AS part_cde
               ,A.event
               ,A.commit_yr AS del_year
         FROM structools.st_pickid_committed       A  
         WHERE A.part_cde = 'STAY'
         AND A.pick_id LIKE 'S%'
         AND A.event = 'REPLACE'
         UNION
         SELECT A.pick_id              -- xarms and stays scheduled for replace im their own right
               ,'OSTAY'            AS part_cde
               ,A.event
               ,A.commit_yr AS del_year
         FROM structools.st_pickid_committed       A  
         WHERE A.part_cde = 'STAY'
         AND A.pick_id LIKE 'S%'
         AND A.event = 'REPLACE'
         UNION
         SELECT A.pick_id              -- xarms and stays scheduled for replace im their own right
               ,'AGOST'            AS part_cde
               ,A.event
               ,A.commit_yr AS del_year
         FROM structools.st_pickid_committed       A  
         WHERE A.part_cde = 'STAY'
         AND A.pick_id LIKE 'S%'
         AND A.event = 'REPLACE'
         )
--         ,
--         actioned_or_committed_eqp_tmp AS        --pickid init
--         (
----         SELECT * 
----         FROM  actioned
----         UNION
--         SELECT * FROM COMMITTED
--         ) 
         ,
         actioned_eqp_tmp AS
         (  
         SELECT pick_id                               
               ,part_cde
               ,MAX(RPAD(event,9)||del_year)                AS max_event_seq 
         --FROM structools.st_del_program_pickid            --pickid init
         FROM COMMITTED
--         WHERE base_run_id BETWEEN v_parent_run_id_first AND v_parent_run_id_last    --pickid init
--         AND program_id = v_parent_run_program_id
--         AND event != 'REPAIR'
         GROUP BY pick_id, part_cde
         )
         ,
         actioned_eqp AS
         (
         SELECT A.pick_id
               ,A.part_cde
               ,RTRIM(SUBSTR(A.max_event_seq,1,9))      AS event
               ,SUBSTR(A.max_event_seq,10,4)            AS del_year
               ,CASE                                        --pickid init
                  WHEN b.event IS NULL THEN  'N'
                  ELSE                       'Y'
               END                                       AS scheduled
         FROM actioned_eqp_tmp      A  LEFT OUTER JOIN      --pickid init
--              structools.st_pickid_committed b
              COMMITTED b
         ON A.pick_id = b.pick_id
         AND A.part_cde = b.part_cde                                    
         )               
         ,
         suppressed_eqp AS
         (
			SELECT pick_id
         				,'Y'					AS suppressed
         FROM structools.st_pickid_suppressed_init
         WHERE equip_grp_id != 'BAY'
         UNION
         SELECT pick_id
         			,'P'						AS suppressed				
         FROM structools.st_pickid_suppressed
         WHERE equip_grp_id != 'BAY'
         AND scenario_id = v_suppress_pickids_prog_id
         )
         SELECT vic.prnt_plnt_no    AS vic_id
               ,A.pick_id
               ,A.part_cde
               ,A.part_defective
               ,A.equip_grp_id
               ,A.instln_year
               ,CASE
                  WHEN b.del_year IS NULL    THEN  A.reinf_cde
                  WHEN b.event = 'REPLACE'   THEN  NULL
                  WHEN b.event = 'REINFORCE' THEN  'F'
                END                     AS reinf_cde
               ,CASE
                  WHEN A.equip_grp_id = 'PWOD' AND b.event = 'REPLACE'   THEN  'S'
                  ELSE                                                         A.wood_type  
                END                     AS wood_type
               ,A.recl_phases   
               ,CASE
                  WHEN A.equip_grp_id = 'PWOD' AND b.event = 'REPLACE'   THEN  999
                  ELSE                                                         A.pole_diameter_mm   
                END                     AS pole_diameter_mm
               ,A.pole_complexity 
               ,A.dstr_size       
               ,CASE A.equip_grp_id
                  WHEN 'PAUS' THEN 1
                  WHEN 'PWOD' THEN 2
                  WHEN 'RECL' THEN 3
                  WHEN 'CAPB' THEN 4
                  WHEN 'REAC' THEN 5
                  WHEN 'LBS'  THEN 6
                  WHEN 'RGTR' THEN 7
                  WHEN 'SECT' THEN 8
                  WHEN 'DSTR' THEN 9
                  WHEN 'PTSD' THEN 10
                  WHEN 'DOF'  THEN 11
               END                  AS eqp_seq_no
              ,A.maint_zone_nam 
              ,A.manuf
              ,A.stock_cde
              ,A.ctl_typ
              ,A.dev_typ
              ,A.manuf_and_mdl
              ,A.expulsion_type
              ,b.del_year
              ,b.event        AS del_event
              ,NULL                       AS non_del_pick_id 
              ,NULL                          AS non_del_event 
              ,NULL                          AS non_del_event_reason 
              ,NULL                          AS non_del_capex
              ,NULL                          AS non_del_failure_prob 
              ,A.fire_risk_zone_cls||A.max_safety_zone_rtg  AS fsrtg
              ,CASE A.region_nam
                  WHEN 'METRO'               THEN  'METRO'
                  ELSE                             'COUNTRY'
              END                            AS region
              ,CASE 
                  WHEN UPPER(d.scnrrr_fdr_cat_nam) = 'NONE'            THEN 'SHORTRURAL'
                  WHEN d.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                  ELSE                                                            UPPER(d.scnrrr_fdr_cat_nam)
              END                                                            AS fdr_cat
              ,NVL(b.scheduled, 'N')                        AS scheduled                            --pickid init
              ,A.cust_ds_cnt
              ,A.labour_hrs
              ,A.cca_treated
              ,E.pick_id														AS pickid_suppressed
              ,E.suppressed
         FROM  vic                     INNER JOIN
               structools.st_equipment A
         ON    A.prnt_plnt_no = vic.prnt_plnt_no
                                       LEFT OUTER JOIN
               actioned_eqp         b
         ON A.pick_id = B.pick_id 
         AND A.part_cde = b.part_cde               
                                                 LEFT OUTER JOIN
                  structools.st_hv_feeder    d
             ON A.feeder_id =        d.hv_fdr_id                                                                         
                                                 LEFT OUTER JOIN
                  suppressed_eqp	E
             ON A.pick_id =        E.pick_id                                                                         
         WHERE  (equip_grp_id != 'PAUS' OR A.part_cde != ' ')
         AND part_defective IS NULL OR part_defective = 'Y'
         --WHERE vic.prnt_plnt_no = 'LS276436' --graz
         --WHERE vic.prnt_plnt_no = 'LS1525'
         --WHERE A.maint_zone_nam in ('RTN/Z005','BUN/Z92','BYF/Z014','MUL/Z022')  -- graz
         --WHERE A.maint_zone_nam in ('RTN/Z005')  -- graz
         ORDER BY vic.prnt_plnt_no, eqp_seq_no, pick_id
      ;
      
      v_eqp_all_rec           eqp_csr%ROWTYPE;

      TYPE eqp_year_tab_typ IS TABLE OF structools.st_strategy_run%ROWTYPE; -- INDEX BY VARCHAR2(9); 
      v_eqp_year_tab        eqp_year_tab_typ := eqp_year_tab_typ();
      v_eqp_last_year_tab   eqp_year_tab_typ := eqp_year_tab_typ();
      
      TYPE eqp_rec_typ   IS RECORD 
      (pick_id       VARCHAR2(9)
      ,egi           VARCHAR2(30)
      ,instln_year   INTEGER
      ,risk          FLOAT(126)
      ,risk_noaction FLOAT(126)
      ,age           INTEGER
--      ,init_age      INTEGER
      ,calendar_year INTEGER
      ,reinf_cde     VARCHAR2(100)
      ,wood_type     CHAR(1)
      ,recl_phases   CHAR(1)
      ,pole_diameter_mm NUMBER
      ,pole_complexity CHAR(6)
      ,dstr_size       CHAR(6)
      ,egi_requested   BOOLEAN
      ,manuf          VARCHAR2(100)
      ,stock_cde      VARCHAR2(100)
      ,ctl_typ        VARCHAR2(100)
      ,dev_typ        VARCHAR2(100)
      ,manuf_and_mdl  VARCHAR2(100)
      ,expulsion_type VARCHAR2(100)
      ,scheduled      CHAR(1)
      ,del_event      VARCHAR2(10)
      ,del_year       PLS_INTEGER
      ,non_del_pick_id   VARCHAR2(9)
      ,non_del_event   VARCHAR2(10)
      ,non_del_event_reason   VARCHAR2(50)
      ,non_del_capex   INTEGER
      ,non_del_failure_prob CHAR(1)
      ,fsrtg                VARCHAR2(30)
      ,fire_risk_zone      CHAR(1)
      ,pub_saf_zone        VARCHAR2(10)
      ,region              varchar2(10)
      ,fdr_cat                    VARCHAR2(35)
      ,part_cde            VARCHAR2(10)
      --,part_defect_hier    PLS_INTEGER
      ,part_defective			CHAR(1)
      ,cust_ds_cnt         PLS_INTEGER
      ,labour_hrs				PLS_INTEGER
      ,cca_treated			CHAR(1)
      ,pickid_suppressed	CHAR(1)
      );
      
      
      TYPE eqp_tab_typ IS TABLE OF eqp_rec_typ;
      v_eqp_tab       eqp_tab_typ := eqp_tab_typ();
      
      TYPE v_risk_yr20_tab_typ  IS TABLE OF risk_rec_typ;
      v_risk_yr20_tab   v_risk_yr20_tab_typ := v_risk_yr20_tab_typ();
      v_risk_yr20_tab_empty   v_risk_yr20_tab_typ := v_risk_yr20_tab_typ();

      v_bay_risk_tab                pwod_risk_tab_typ;
      
      TYPE pick_id_defect_rec_typ      IS RECORD
      (part_cde            VARCHAR2(20)
      ,sev_cde             char(1)
      );
      
      TYPE pick_id_defects_tab_typ  IS TABLE OF pick_id_defect_rec_typ INDEX BY VARCHAR2(4);
      v_pick_id_defects_tab         pick_id_defects_tab_typ;
      
      TYPE stay_xarm_defects_tab_typ IS TABLE OF CHAR(1)  INDEX BY VARCHAR2(4);
      v_stay_xarm_defects_tab         stay_xarm_defects_tab_typ;
      
      TYPE vic_defects_tab_typ      IS TABLE OF pick_id_defects_tab_typ INDEX BY VARCHAR2(10);
      v_vic_defects_tab             vic_defects_tab_typ;
      
      TYPE vic_stay_xarm_defects_tab_typ      IS TABLE OF stay_xarm_defects_tab_typ INDEX BY VARCHAR2(15);
      v_vic_stay_xarm_defects_tab             vic_stay_xarm_defects_tab_typ;
      
      v_vic_id_prev              VARCHAR2(30);
      
      j                          PLS_INTEGER;
      v_vic_cnt                  PLS_INTEGER;
      v_strategy_bay             PLS_INTEGER;
      v_strategy_pwod            PLS_INTEGER;
      v_strategy_paus            PLS_INTEGER;
      v_strategy_dstr            PLS_INTEGER;
      v_strategy_dof             PLS_INTEGER;
      v_strategy_recl            PLS_INTEGER;
      v_strategy_sect            PLS_INTEGER;
      v_strategy_ptsd            PLS_INTEGER;
      v_strategy_capb            PLS_INTEGER;
      v_strategy_reac            PLS_INTEGER;
      v_strategy_lbs             PLS_INTEGER;
      v_strategy_rgtr            PLS_INTEGER;
      v_strategy_agost           PLS_INTEGER;
      v_strategy_astay           PLS_INTEGER;
      v_strategy_gstay           PLS_INTEGER;
      v_strategy_ostay           PLS_INTEGER;
      v_strategy_hvxm            PLS_INTEGER;
      v_strategy_lvxm            PLS_INTEGER;
      
      
      
      v_mtzn_prev                VARCHAR2(100);

      v_span                     VARCHAR2(20);
      v_cond_key                 VARCHAR2(9);
      v_bay_strategy             BOOLEAN;
      v_pole_eqp_strategy        BOOLEAN;
      TYPE bay_stc_tab_typ       IS TABLE OF CHAR(1) INDEX BY VARCHAR2(15);
      v_vic_stc_id               VARCHAR2(9);   
      
      TYPE failure_prob_tab_typ    IS TABLE OF FLOAT(126) INDEX BY VARCHAR2(10);
      v_failure_replace_prob_tab            failure_prob_tab_typ;
      v_failure_noaction_prob_tab           failure_prob_tab_typ;
      v_failure_reinforce_prob_tab          failure_prob_tab_typ;
 
      TYPE mtzn_priority_tab_typ       IS TABLE OF char(1) INDEX BY varchar2(20);
      v_mtzn_priority_tab              mtzn_priority_tab_typ;
      
      v_risk_rec_pole_part          risk_rec_typ;
      v_risk_pick_id                VARCHAR2(10);

      /*****************************************************************/
      /* Private functions for execute_run_request procedure           */
      /*****************************************************************/

      /* load the applicable combos of fire rating and safety zone into an assoc array */
      PROCEDURE load_fsrtg_table(i_egi IN VARCHAR2, i_fsrtg IN VARCHAR2) IS
      
         v_fsrtg        VARCHAR2(200) := i_fsrtg;
         v_len          PLS_INTEGER;
         v_pos          PLS_INTEGER;
         v_fsrtg_val    VARCHAR2(30);
         v_egi          CHAR(4)        := i_egi;

      BEGIN
      
         v_pos := 0;
         v_len := LENGTH(v_fsrtg);
         LOOP
            IF v_len > 0 THEN
               v_fsrtg := SUBSTR(v_fsrtg, v_pos + 1, v_len);
               IF (INSTR(v_fsrtg, ';')) > 0 THEN
                  v_pos := INSTR(v_fsrtg, ';');
                  v_fsrtg_val := SUBSTR(v_fsrtg, 1, v_pos - 1);  
                  v_fsrtg_tab(v_egi||v_fsrtg_val) := ' ';        -- only interested in existence
                  v_len := v_len - v_pos;
               ELSE
                  v_fsrtg_tab(v_egi||v_fsrtg) := ' ';        -- only interested in existence
                  v_len := 0;
               END IF;
            ELSE
               EXIT;
            END IF;

         END LOOP;    

      END load_fsrtg_table;

      /* this procedure currently stores NOACTION risk for all bays */
      PROCEDURE store_risk_bay(i_action IN VARCHAR2) IS
      
         v_risk_tab                    risk_tab_typ := risk_tab_typ();
         v_risk_key                    VARCHAR2(40);
         
      BEGIN
         FOR i IN 1 .. v_risk_yr20_tab.COUNT LOOP
            v_risk_tab.DELETE;
            v_risk_tab.EXTEND(21);
            
            v_risk_tab(1) := v_risk_yr20_tab(i).yr0;
            v_risk_tab(2) := v_risk_yr20_tab(i).yr1;
            v_risk_tab(3) := v_risk_yr20_tab(i).yr2;
            v_risk_tab(4) := v_risk_yr20_tab(i).yr3;
            v_risk_tab(5) := v_risk_yr20_tab(i).yr4;
            v_risk_tab(6) := v_risk_yr20_tab(i).yr5;
            v_risk_tab(7) := v_risk_yr20_tab(i).yr6;
            v_risk_tab(8) := v_risk_yr20_tab(i).yr7;
            v_risk_tab(9) := v_risk_yr20_tab(i).yr8;
            v_risk_tab(10) := v_risk_yr20_tab(i).yr9;
            v_risk_tab(11) := v_risk_yr20_tab(i).yr10;         
            v_risk_tab(12) := v_risk_yr20_tab(i).yr11;
            v_risk_tab(13) := v_risk_yr20_tab(i).yr12;
            v_risk_tab(14) := v_risk_yr20_tab(i).yr13;
            v_risk_tab(15) := v_risk_yr20_tab(i).yr14;
            v_risk_tab(16) := v_risk_yr20_tab(i).yr15;
            v_risk_tab(17) := v_risk_yr20_tab(i).yr16;
            v_risk_tab(18) := v_risk_yr20_tab(i).yr17;
            v_risk_tab(19) := v_risk_yr20_tab(i).yr18;
            v_risk_tab(20) := v_risk_yr20_tab(i).yr19;
            v_risk_tab(21) := v_risk_yr20_tab(i).yr20;
            v_risk_key := 'B'||TRIM(v_risk_yr20_tab(i).pick_id)||i_action;
            v_bay_risk_tab(v_risk_key) := v_risk_tab;
         END LOOP;
         
      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line ('store_risk_bay, i_action = '||i_action);   
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;      
         
      END store_risk_bay;

      /* this procedure currently stores REPLACE, NOACTIONNPV15 and REPLACENPV15 risk for all bays in a single span. The entries are deleted */
      /* after the span gets processed.                                                                                                      */
      PROCEDURE store_risk_bay2(i_pick_id IN VARCHAR2, i_risk_rec IN risk_rec_typ, i_action IN VARCHAR2) IS
      
         v_risk_key                    VARCHAR2(40);
         v_risk_tab                    risk_tab_typ := risk_tab_typ();
         
      BEGIN

         v_risk_key := i_pick_id||i_action;
         
         v_risk_tab.EXTEND(21);
         v_risk_tab(1) := i_risk_rec.yr0;
         v_risk_tab(2) := i_risk_rec.yr1;
         v_risk_tab(3) := i_risk_rec.yr2;
         v_risk_tab(4) := i_risk_rec.yr3;
         v_risk_tab(5) := i_risk_rec.yr4;
         v_risk_tab(6) := i_risk_rec.yr5;
         v_risk_tab(7) := i_risk_rec.yr6;
         v_risk_tab(8) := i_risk_rec.yr7;
         v_risk_tab(9) := i_risk_rec.yr8;
         v_risk_tab(10) := i_risk_rec.yr9;
         v_risk_tab(11) := i_risk_rec.yr10;         
         v_risk_tab(12) := i_risk_rec.yr11;
         v_risk_tab(13) := i_risk_rec.yr12;
         v_risk_tab(14) := i_risk_rec.yr13;
         v_risk_tab(15) := i_risk_rec.yr14;
         v_risk_tab(16) := i_risk_rec.yr15;
         v_risk_tab(17) := i_risk_rec.yr16;
         v_risk_tab(18) := i_risk_rec.yr17;
         v_risk_tab(19) := i_risk_rec.yr18;
         v_risk_tab(20) := i_risk_rec.yr19;
         v_risk_tab(21) := i_risk_rec.yr20;
         
         IF i_action = 'NNPV15' THEN
            v_risk_tab.EXTEND(7);
            v_risk_tab(22) := i_risk_rec.fire_frac;
            v_risk_tab(23) := i_risk_rec.eshock_frac;
            v_risk_tab(24) := i_risk_rec.wforce_frac;
            v_risk_tab(25) := i_risk_rec.reliability_frac;
            v_risk_tab(26) := i_risk_rec.physimp_frac;
            v_risk_tab(27) := i_risk_rec.env_frac;
            v_risk_tab(28) := i_risk_rec.fin_frac;
         END IF;
         
         v_bay_risk_tab(v_risk_key) := v_risk_tab;
         
      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line ('store_risk_bay2, i_pick_id = '||i_pick_id||' i_action = '||i_action);   
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;
      END store_risk_bay2;


      PROCEDURE get_bay_risk_for_year(i_pick_id VARCHAR2, i_event VARCHAR2, i_age INTEGER
                                 --,i_init_age INTEGER
                                 , i_variation CHAR, i_exec_year IN INTEGER, i_del_year IN INTEGER
                                 ,i_fdr_cat IN VARCHAR2
                                 ,o_risk OUT FLOAT, o_risk_npv OUT FLOAT,  o_risk_noaction_npv OUT FLOAT 
                                 ,o_risk_npv_sif OUT FLOAT ,o_risk_noaction_npv_sif OUT FLOAT) AS


         v_idx_n              INTEGER;
         v_idx_p              INTEGER;
         v_event              VARCHAR2(30);
         v_event_1            CHAR(1);
         v_pick_id            VARCHAR2(10) := SUBSTR(i_pick_id, 2, LENGTH(i_pick_id) - 1);
         v_risk_rec           risk_rec_typ;
         v_risk_tab           risk_tab_typ := risk_tab_typ();
         v_del_year           PLS_INTEGER;
         v_fire_frac          FLOAT(126);
         v_eshock_frac        FLOAT(126);
         v_wforce_frac        FLOAT(126);
         v_reliability_frac   FLOAT(126);
         v_physimp_frac       FLOAT(126);
         v_env_frac           FLOAT(126);     
         v_fin_frac           FLOAT(126);
    
      BEGIN

         o_risk := NULL;
         o_risk_npv := NULL;
         o_risk_npv_sif := NULL;
         o_risk_noaction_npv := NULL;
         o_risk_noaction_npv_sif := NULL;

         v_risk_pubsafety_sif   := v_fdr_cat_sif_tab(i_fdr_cat).pubsafety_sif;
         v_risk_wforce_sif      := v_fdr_cat_sif_tab(i_fdr_cat).wforce_sif;
         v_risk_reliability_sif := v_fdr_cat_sif_tab(i_fdr_cat).reliability_sif;
         v_risk_env_sif         := v_fdr_cat_sif_tab(i_fdr_cat).env_sif;            
         v_risk_fin_sif         := v_fdr_cat_sif_tab(i_fdr_cat).fin_sif;
      
         IF i_del_year IS NOT NULL THEN
            v_del_year := i_del_year;
         ELSE
            v_del_year := v_this_year;
         END IF;
         v_idx_n := i_exec_year - v_del_year + 1;
         v_event := i_event;
         IF v_event = 'NOACTION' THEN
            IF i_variation = 'P' THEN
               v_event := 'REPLACE';
               v_idx_p := i_age + 1;
            END IF;
         ELSIF v_event = 'REPLACE' THEN
            v_idx_p := i_age + 1;
         END IF;
               
         IF v_idx_n > 21 THEN
            v_idx_n := 21;
         END IF;
         IF v_idx_p > 21 THEN
            v_idx_p := 21;
         END IF;
         
         IF v_event = 'NOACTION' THEN
            v_event_1 := 'N';
         ELSIF v_event = 'REPLACE' THEN
            v_event_1 := 'P';
         END IF;

         BEGIN
            IF v_event_1 = 'N' THEN
               o_risk := v_bay_risk_tab(i_pick_id||'N')(v_idx_n);
            ELSE   
               IF v_bay_risk_tab.EXISTS(i_pick_id||'P') THEN  --AND v_bay_risk_tab(i_pick_id||'P').COUNT > 0 THEN
                  o_risk := v_bay_risk_tab(i_pick_id||'P')(v_idx_p);
               ELSE
                  SELECT   A.*
                          ,NULL
                          ,NULL      
                          ,NULL      
                          ,NULL      
                          ,NULL      
                          ,NULL       
                          ,NULL       
                  INTO  v_risk_rec
                  FROM  structools.st_risk_bay_replace   A 
                  WHERE pick_id = v_pick_id;
                  store_risk_bay2(i_pick_id, v_risk_rec, 'P');
                  o_risk := v_bay_risk_tab(i_pick_id||'P')(v_idx_p);
               END IF;
            END IF;
            IF i_event = 'REPLACE' THEN
               IF v_bay_risk_tab.EXISTS(i_pick_id||'PNPV15') THEN --AND v_bay_risk_tab(i_pick_id||'PNPV15').COUNT > 0 THEN
                  o_risk_npv := v_bay_risk_tab(i_pick_id||'PNPV15')(v_idx_p);
               ELSE
                  SELECT   A.*
                          ,NULL
                          ,NULL      
                          ,NULL      
                          ,NULL      
                          ,NULL      
                          ,NULL       
                          ,NULL       
                  INTO  v_risk_rec
                  FROM  structools.st_risk_bay_replace_npv15   A 
                  WHERE pick_id = v_pick_id;
                  store_risk_bay2(i_pick_id, v_risk_rec, 'PNPV15');
                  o_risk_npv := v_bay_risk_tab(i_pick_id||'PNPV15')(v_idx_p);
               END IF;
               IF v_bay_risk_tab.EXISTS(i_pick_id||'NNPV15') THEN --AND v_bay_risk_tab(i_pick_id||'NNPV15').COUNT > 0 THEN
                  o_risk_noaction_npv := v_bay_risk_tab(i_pick_id||'NNPV15')(v_idx_n);
               ELSE
                  SELECT   *
                  INTO  v_risk_rec
                  FROM  structools.st_risk_bay_noaction_npv15
                  WHERE pick_id = v_pick_id; 
                  store_risk_bay2(i_pick_id, v_risk_rec, 'NNPV15');
                  o_risk_noaction_npv := v_bay_risk_tab(i_pick_id||'NNPV15')(v_idx_n);
               END IF;
               v_fire_frac          := v_bay_risk_tab(i_pick_id||'NNPV15')(22);
               v_eshock_frac        := v_bay_risk_tab(i_pick_id||'NNPV15')(23);
               v_wforce_frac        := v_bay_risk_tab(i_pick_id||'NNPV15')(24);
               v_reliability_frac   := v_bay_risk_tab(i_pick_id||'NNPV15')(25);
               v_physimp_frac       := v_bay_risk_tab(i_pick_id||'NNPV15')(26);
               v_env_frac           := v_bay_risk_tab(i_pick_id||'NNPV15')(27);
               v_fin_frac           := v_bay_risk_tab(i_pick_id||'NNPV15')(28);
               o_risk_npv_sif := o_risk_npv * (v_fire_frac + v_eshock_frac + v_physimp_frac) * v_risk_pubsafety_sif
                           + o_risk_npv * v_wforce_frac * v_risk_wforce_sif
                           + o_risk_npv * v_reliability_frac * v_risk_reliability_sif
                           + o_risk_npv * v_env_frac * v_risk_env_sif
                           + o_risk_npv * v_fin_frac * v_risk_fin_sif;
               
               o_risk_noaction_npv_sif := o_risk_noaction_npv * (v_fire_frac + v_eshock_frac + v_physimp_frac) * v_risk_pubsafety_sif
                           + o_risk_noaction_npv * v_wforce_frac * v_risk_wforce_sif
                           + o_risk_noaction_npv * v_reliability_frac * v_risk_reliability_sif
                           + o_risk_noaction_npv * v_env_frac * v_risk_env_sif
                           + o_risk_noaction_npv * v_fin_frac * v_risk_fin_sif;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
         
      EXCEPTION
         WHEN OTHERS THEN
           dbms_output.put_line('TEST-05 '||i_pick_id||' i_event = '||i_event||' i_age = '||i_age--||' i_init_age = '||i_init_age
                              ||' i_variation = '||i_variation||' i_exec_year = '||i_exec_year||' i_del_year = '||i_del_year
                              ||' i_fdr_cat = '||i_fdr_cat
                              ||' v_del_year = '||v_del_year
                              ||' v_idx_n = '||v_idx_n||' v_idx_p = '||v_idx_p
                              ||' v_event = '||v_event||' v_event_1 = '||v_event_1);
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);            
            RAISE;

      END get_bay_risk_for_year;

      PROCEDURE check_failure_prob(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS

         v_failure_prob_key       VARCHAR2(10);
         v_yr_idx_char           CHAR(2);
         v_yr_idx                PLS_INTEGER;
         v_idx_type               CHAR(1);
         v_step_idx              PLS_INTEGER;
         v_last_fail_prob        CHAR(1);

      BEGIN

         /* Check probability of failure */
         v_yr_idx := i_exec_year - v_this_year;
         v_idx_type := 'N';   --for Noaction

         IF v_eqp_tab(i).del_event = 'REPLACE' THEN      --del_event is only populated if the pickid had an event in one of the prev logical runs
            v_idx_type := 'P';                           --or if the pickid is one of the committed_pickids
            v_yr_idx := v_eqp_year_tab(x).age;           --ie. if it is not one of the committed pickids, del_event will always be NULL 
         ELSIF v_eqp_tab(i).del_event = 'REINFORCE' THEN --for a single run or the first run in a series of runs  
            v_idx_type := 'F';       
            v_yr_idx := i_exec_year - v_year_0;                   
         END IF;
 
         v_yr_idx_char := LTRIM(TO_CHAR((v_yr_idx), '00'));
         v_failure_prob_key := v_yr_idx_char||v_eqp_tab(i).pick_id;

         v_eqp_year_tab(x).failure_prob := 'N';
         IF i_exec_year > v_first_year THEN
             v_eqp_year_tab(x).failure_prob := coalesce(v_eqp_last_year_tab(x).failure_prob, 'N');
         END IF;
         
         v_step_idx := i_exec_year - v_first_year;
         
         IF v_idx_type = 'N' THEN
            IF v_failure_noaction_prob_tab.EXISTS(v_failure_prob_key) THEN
               IF v_eqp_tab(i).egi = 'PWOD' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_pwod
                  OR v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_pwod - ((v_step_idx) * v_fail_prob_step_pwod) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'DSTR' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_dstr
                  OR v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_dstr - ((v_step_idx) * v_fail_prob_step_dstr) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'DOF' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_dof
                  OR v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_dof - ((v_step_idx) * v_fail_prob_step_dof) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'RECL' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_recl
                  OR v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_recl - ((v_step_idx) * v_fail_prob_step_recl) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'SECT' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_sect
                  OR v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_sect - ((v_step_idx) * v_fail_prob_step_sect) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'PTSD' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_ptsd
                  OR v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_ptsd - ((v_step_idx) * v_fail_prob_step_ptsd) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'RGTR' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_rgtr
                  OR v_failure_noaction_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_rgtr - ((v_step_idx) * v_fail_prob_step_rgtr) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               END IF;
            END IF;
         ELSIF v_idx_type = 'F' THEN
            IF v_failure_reinforce_prob_tab.EXISTS(v_failure_prob_key) THEN
               IF v_eqp_tab(i).egi = 'PWOD' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_reinforce_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_pwod
                  OR v_failure_reinforce_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_pwod - ((v_step_idx) * v_fail_prob_step_pwod) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               END IF;
            END IF;   
         ELSIF v_idx_type = 'P' THEN
            IF v_failure_replace_prob_tab.EXISTS(v_failure_prob_key) THEN
               IF v_eqp_tab(i).egi = 'PWOD' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_pwod
                  OR v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_pwod - ((v_step_idx) * v_fail_prob_step_pwod) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'DSTR' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_dstr
                  OR v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_dstr - ((v_step_idx) * v_fail_prob_step_dstr) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'DOF' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_dof
                  OR v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_dof - ((v_step_idx) * v_fail_prob_step_dof) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'RECL' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_recl
                  OR v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_recl - ((v_step_idx) * v_fail_prob_step_recl) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'SECT' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_sect
                  OR v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_sect - ((v_step_idx) * v_fail_prob_step_sect) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'PTSD' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_ptsd
                  OR v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_ptsd - ((v_step_idx) * v_fail_prob_step_ptsd) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'RGTR' THEN
                  IF v_step_idx = (v_last_year - v_first_year) AND v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_LOW_rgtr
                  OR v_failure_replace_prob_tab(v_failure_prob_key) > v_FAIL_PROB_HIGH_rgtr - ((v_step_idx) * v_fail_prob_step_rgtr) THEN
                     v_eqp_year_tab(x).failure_prob := 'Y';
                  END IF;
               END IF;
            END IF;
         END IF;   
               
         IF v_eqp_year_tab(x).failure_prob = 'Y' THEN        
            IF v_eqp_year_tab(x).event IS NULL THEN
               v_eqp_year_tab(x).event := 'REPLACE';
               IF v_eqp_tab(i).egi = 'RECL' THEN
                  v_cost_key := TRIM(v_eqp_tab(i).egi)||'N'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).recl_phases)||TRIM(v_eqp_tab(i).region);
               ELSIF v_eqp_tab(i).egi = 'DSTR' THEN
                  v_cost_key := TRIM(v_eqp_tab(i).egi)||'N'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).dstr_size)||TRIM(v_eqp_tab(i).region);
               ELSIF v_eqp_tab(i).egi = 'PWOD' THEN   
                  v_cost_key := TRIM(v_eqp_tab(i).egi)||'N'||TRIM(v_cost_treatment_type)||'REPLACE'||TRIM(v_eqp_tab(i).pole_complexity)||TRIM(v_eqp_tab(i).region);
               ELSIF v_eqp_tab(i).egi = 'LBS' OR v_eqp_tab(i).egi = 'CAPB' OR v_eqp_tab(i).egi = 'DOF' 
               OR v_eqp_tab(i).egi = 'PTSD' OR v_eqp_tab(i).egi = 'REAC' OR v_eqp_tab(i).egi = 'RGTR' OR v_eqp_tab(i).egi = 'SECT' THEN
                  v_cost_key := TRIM(v_eqp_tab(i).egi)||'N'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
               END IF;
               v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
            END IF;
         END IF;
         
      END check_failure_prob;

      PROCEDURE store_risk_pwod(i_action IN CHAR) IS
      
         v_risk_tab                    risk_tab_typ := risk_tab_typ();
         v_risk_key                    VARCHAR2(40);
         
      BEGIN
         FOR i IN 1 .. v_risk_yr20_tab.COUNT LOOP
            v_risk_tab.DELETE;
            v_risk_tab.EXTEND(21);
            
            v_risk_tab(1) := v_risk_yr20_tab(i).yr0;
            v_risk_tab(2) := v_risk_yr20_tab(i).yr1;
            v_risk_tab(3) := v_risk_yr20_tab(i).yr2;
            v_risk_tab(4) := v_risk_yr20_tab(i).yr3;
            v_risk_tab(5) := v_risk_yr20_tab(i).yr4;
            v_risk_tab(6) := v_risk_yr20_tab(i).yr5;
            v_risk_tab(7) := v_risk_yr20_tab(i).yr6;
            v_risk_tab(8) := v_risk_yr20_tab(i).yr7;
            v_risk_tab(9) := v_risk_yr20_tab(i).yr8;
            v_risk_tab(10) := v_risk_yr20_tab(i).yr9;
            v_risk_tab(11) := v_risk_yr20_tab(i).yr10;         
            v_risk_tab(12) := v_risk_yr20_tab(i).yr11;
            v_risk_tab(13) := v_risk_yr20_tab(i).yr12;
            v_risk_tab(14) := v_risk_yr20_tab(i).yr13;
            v_risk_tab(15) := v_risk_yr20_tab(i).yr14;
            v_risk_tab(16) := v_risk_yr20_tab(i).yr15;
            v_risk_tab(17) := v_risk_yr20_tab(i).yr16;
            v_risk_tab(18) := v_risk_yr20_tab(i).yr17;
            v_risk_tab(19) := v_risk_yr20_tab(i).yr18;
            v_risk_tab(20) := v_risk_yr20_tab(i).yr19;
            v_risk_tab(21) := v_risk_yr20_tab(i).yr20;
            v_risk_key := 'S'||TRIM(v_risk_yr20_tab(i).pick_id)||i_action;
            v_pwod_risk_tab(v_risk_key) := v_risk_tab;
         END LOOP;
         
      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line ('store_risk_pwod, i_action = '||i_action);   
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;      
         
      END store_risk_pwod;
      
      PROCEDURE store_risk_eqp(i_action IN CHAR) IS
      
         v_risk_tab                    risk_tab_typ := risk_tab_typ();
         v_risk_key                    VARCHAR2(40);
         
      BEGIN
         FOR i IN 1 .. v_risk_yr20_tab.COUNT LOOP
            v_risk_tab.DELETE;
            v_risk_tab.EXTEND(21);
            
            v_risk_tab(1) := v_risk_yr20_tab(i).yr0;
            v_risk_tab(2) := v_risk_yr20_tab(i).yr1;
            v_risk_tab(3) := v_risk_yr20_tab(i).yr2;
            v_risk_tab(4) := v_risk_yr20_tab(i).yr3;
            v_risk_tab(5) := v_risk_yr20_tab(i).yr4;
            v_risk_tab(6) := v_risk_yr20_tab(i).yr5;
            v_risk_tab(7) := v_risk_yr20_tab(i).yr6;
            v_risk_tab(8) := v_risk_yr20_tab(i).yr7;
            v_risk_tab(9) := v_risk_yr20_tab(i).yr8;
            v_risk_tab(10) := v_risk_yr20_tab(i).yr9;
            v_risk_tab(11) := v_risk_yr20_tab(i).yr10;         
            v_risk_tab(12) := v_risk_yr20_tab(i).yr11;
            v_risk_tab(13) := v_risk_yr20_tab(i).yr12;
            v_risk_tab(14) := v_risk_yr20_tab(i).yr13;
            v_risk_tab(15) := v_risk_yr20_tab(i).yr14;
            v_risk_tab(16) := v_risk_yr20_tab(i).yr15;
            v_risk_tab(17) := v_risk_yr20_tab(i).yr16;
            v_risk_tab(18) := v_risk_yr20_tab(i).yr17;
            v_risk_tab(19) := v_risk_yr20_tab(i).yr18;
            v_risk_tab(20) := v_risk_yr20_tab(i).yr19;
            v_risk_tab(21) := v_risk_yr20_tab(i).yr20;
            v_risk_key := 'N'||TRIM(v_risk_yr20_tab(i).pick_id)||i_action;
            v_eqp_risk_tab(v_risk_key) := v_risk_tab;
            
         END LOOP;
         
      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line ('store_risk_eqp, i_action = '||i_action);   
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;      
         
      END store_risk_eqp;
      
      PROCEDURE store_risk_pole_part(i_part_cde IN VARCHAR2, i_action IN VARCHAR2) IS
      
         v_risk_tab                    risk_tab_typ := risk_tab_typ();
         v_risk_key                    VARCHAR2(40);
         
      BEGIN
      
         v_risk_tab.DELETE;
         v_risk_tab.EXTEND(28);
            
         v_risk_tab(1) := v_risk_rec_pole_part.yr0;
         v_risk_tab(2) := v_risk_rec_pole_part.yr1;
         v_risk_tab(3) := v_risk_rec_pole_part.yr2;
         v_risk_tab(4) := v_risk_rec_pole_part.yr3;
         v_risk_tab(5) := v_risk_rec_pole_part.yr4;
         v_risk_tab(6) := v_risk_rec_pole_part.yr5;
         v_risk_tab(7) := v_risk_rec_pole_part.yr6;
         v_risk_tab(8) := v_risk_rec_pole_part.yr7;
         v_risk_tab(9) := v_risk_rec_pole_part.yr8;
         v_risk_tab(10) := v_risk_rec_pole_part.yr9;
         v_risk_tab(11) := v_risk_rec_pole_part.yr10;
         v_risk_tab(12) := v_risk_rec_pole_part.yr11;
         v_risk_tab(13) := v_risk_rec_pole_part.yr12;
         v_risk_tab(14) := v_risk_rec_pole_part.yr13;
         v_risk_tab(15) := v_risk_rec_pole_part.yr14;
         v_risk_tab(16) := v_risk_rec_pole_part.yr15;
         v_risk_tab(17) := v_risk_rec_pole_part.yr16;
         v_risk_tab(18) := v_risk_rec_pole_part.yr17;
         v_risk_tab(19) := v_risk_rec_pole_part.yr18;
         v_risk_tab(20) := v_risk_rec_pole_part.yr19;
         v_risk_tab(21) := v_risk_rec_pole_part.yr20;
         
--         IF i_part_cde = 'HVXM' OR i_part_cde = 'LVXM' THEN
--            v_risk_tab(1) := v_risk_tab(1) + v_risk_rec_pole_part_insul.yr0;
--            v_risk_tab(2) := v_risk_tab(2) + v_risk_rec_pole_part_insul.yr1;
--            v_risk_tab(3) := v_risk_tab(3) + v_risk_rec_pole_part_insul.yr2;
--            v_risk_tab(4) := v_risk_tab(4) + v_risk_rec_pole_part_insul.yr3;
--            v_risk_tab(5) := v_risk_tab(5) + v_risk_rec_pole_part_insul.yr4;
--            v_risk_tab(6) := v_risk_tab(6) + v_risk_rec_pole_part_insul.yr5;
--            v_risk_tab(7) := v_risk_tab(7) + v_risk_rec_pole_part_insul.yr6;
--            v_risk_tab(8) := v_risk_tab(8) + v_risk_rec_pole_part_insul.yr7;
--            v_risk_tab(9) := v_risk_tab(9) + v_risk_rec_pole_part_insul.yr8;
--            v_risk_tab(10) := v_risk_tab(10) + v_risk_rec_pole_part_insul.yr9;
--            v_risk_tab(11) := v_risk_tab(11) + v_risk_rec_pole_part_insul.yr10;
--            v_risk_tab(12) := v_risk_tab(12) + v_risk_rec_pole_part_insul.yr11;
--            v_risk_tab(13) := v_risk_tab(13) + v_risk_rec_pole_part_insul.yr12;
--            v_risk_tab(14) := v_risk_tab(14) + v_risk_rec_pole_part_insul.yr13;
--            v_risk_tab(15) := v_risk_tab(15) + v_risk_rec_pole_part_insul.yr14;
--            v_risk_tab(16) := v_risk_tab(16) + v_risk_rec_pole_part_insul.yr15;
--            v_risk_tab(17) := v_risk_tab(17) + v_risk_rec_pole_part_insul.yr16;
--            v_risk_tab(18) := v_risk_tab(18) + v_risk_rec_pole_part_insul.yr17;
--            v_risk_tab(19) := v_risk_tab(19) + v_risk_rec_pole_part_insul.yr18;
--            v_risk_tab(20) := v_risk_tab(20) + v_risk_rec_pole_part_insul.yr19;
--            v_risk_tab(21) := v_risk_tab(21) + v_risk_rec_pole_part_insul.yr20;
--         END IF;
--
         IF i_action = 'NNPV15' THEN
            v_risk_tab(22) := v_risk_rec_pole_part.fire_frac;
            v_risk_tab(23) := v_risk_rec_pole_part.eshock_frac;
            v_risk_tab(24) := v_risk_rec_pole_part.wforce_frac;
            v_risk_tab(25) := v_risk_rec_pole_part.reliability_frac;
            v_risk_tab(26) := v_risk_rec_pole_part.physimp_frac;
            v_risk_tab(27) := v_risk_rec_pole_part.env_frac;
            v_risk_tab(28) := v_risk_rec_pole_part.fin_frac;
         END IF;
         

         v_risk_key := 'S'||TRIM(v_risk_rec_pole_part.pick_id)||i_part_cde||i_action;
         v_pole_part_risk_tab(v_risk_key) := v_risk_tab;
         
      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line ('store_risk_pole_part. i_action = '||i_action);   
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;      
         
      END store_risk_pole_part
      ;                     
--      PROCEDURE store_risk_eqp(i_action IN CHAR) IS
--      
--         v_risk_tab                    risk_tab_typ := risk_tab_typ();
--         v_risk_key                    VARCHAR2(40);
--         
--      BEGIN
--         FOR i IN 1 .. v_risk_yr20_tab.COUNT LOOP
--            v_risk_tab.DELETE;
--            v_risk_tab.EXTEND(21);
--            
--            v_risk_tab(1) := v_risk_yr20_tab(i).yr0;
--            v_risk_tab(2) := v_risk_yr20_tab(i).yr1;
--            v_risk_tab(3) := v_risk_yr20_tab(i).yr2;
--            v_risk_tab(4) := v_risk_yr20_tab(i).yr3;
--            v_risk_tab(5) := v_risk_yr20_tab(i).yr4;
--            v_risk_tab(6) := v_risk_yr20_tab(i).yr5;
--            v_risk_tab(7) := v_risk_yr20_tab(i).yr6;
--            v_risk_tab(8) := v_risk_yr20_tab(i).yr7;
--            v_risk_tab(9) := v_risk_yr20_tab(i).yr8;
--            v_risk_tab(10) := v_risk_yr20_tab(i).yr9;
--            v_risk_tab(11) := v_risk_yr20_tab(i).yr10;         
--            v_risk_tab(12) := v_risk_yr20_tab(i).yr11;
--            v_risk_tab(13) := v_risk_yr20_tab(i).yr12;
--            v_risk_tab(14) := v_risk_yr20_tab(i).yr13;
--            v_risk_tab(15) := v_risk_yr20_tab(i).yr14;
--            v_risk_tab(16) := v_risk_yr20_tab(i).yr15;
--            v_risk_tab(17) := v_risk_yr20_tab(i).yr16;
--            v_risk_tab(18) := v_risk_yr20_tab(i).yr17;
--            v_risk_tab(19) := v_risk_yr20_tab(i).yr18;
--            v_risk_tab(20) := v_risk_yr20_tab(i).yr19;
--            v_risk_tab(21) := v_risk_yr20_tab(i).yr20;
--            v_risk_key := 'N'||TRIM(v_risk_yr20_tab(i).pick_id)||i_action;
--            v_eqp_risk_tab(v_risk_key) := v_risk_tab;
--         END LOOP;
--         
--      EXCEPTION
--         WHEN OTHERS THEN
--            dbms_output.put_line ('store_risk_pwod, i_action = '||i_action);   
--            RAISE;      
--         
--      END store_risk_eqp;

      
--      PROCEDURE store_risk_bay(i_pick_id IN VARCHAR2) IS
--      
--         v_pick_id      VARCHAR2(30) := SUBSTR(i_pick_id,2,LENGTH(i_pick_id) -1);
--      
--      BEGIN
--      
--         v_bay_risk_tab_n.DELETE;
--         v_bay_risk_tab_r.DELETE;
--         v_bay_risk_tab_n.EXTEND(21);
--         v_bay_risk_tab_r.EXTEND(21);
--         
--         BEGIN
--            SELECT A.risk_indx_val_y0 AS risk_indx_val_y0_n
--                  ,A.risk_indx_val_y1 AS risk_indx_val_y1_n
--                  ,A.risk_indx_val_y2 AS risk_indx_val_y2_n
--                  ,A.risk_indx_val_y3 AS risk_indx_val_y3_n
--                  ,A.risk_indx_val_y4 AS risk_indx_val_y4_n
--                  ,A.risk_indx_val_y5 AS risk_indx_val_y5_n
--                  ,A.risk_indx_val_y6 AS risk_indx_val_y6_n
--                  ,A.risk_indx_val_y7 AS risk_indx_val_y7_n
--                  ,A.risk_indx_val_y8 AS risk_indx_val_y8_n
--                  ,A.risk_indx_val_y9 AS risk_indx_val_y9_n
--                  ,A.risk_indx_valFbay_y10 AS risk_indx_val_y10_n
--                  ,A.risk_indx_val_y11 AS risk_indx_val_y11_n
--                  ,A.risk_indx_val_y12 AS risk_indx_val_y12_n
--                  ,A.risk_indx_val_y13 AS risk_indx_val_y13_n
--                  ,A.risk_indx_val_y14 AS risk_indx_val_y14_n
--                  ,A.risk_indx_val_y15 AS risk_indx_val_y15_n
--                  ,A.risk_indx_val_y16 AS risk_indx_val_y16_n
--                  ,A.risk_indx_val_y17 AS risk_indx_val_y17_n
--                  ,A.risk_indx_val_y18 AS risk_indx_val_y18_n
--                  ,A.risk_indx_val_y19 AS risk_indx_val_y19_n
--                  ,A.risk_indx_val_y20 AS risk_indx_val_y20_n
--                  ,b.risk_indx_val_y0 AS risk_indx_val_y0_r
--                  ,b.risk_indx_val_y1 AS risk_indx_val_y1_r
--                  ,b.risk_indx_val_y2 AS risk_indx_val_y2_r
--                  ,b.risk_indx_val_y3 AS risk_indx_val_y3_r
--                  ,b.risk_indx_val_y4 AS risk_indx_val_y4_r
--                  ,b.risk_indx_val_y5 AS risk_indx_val_y5_r
--                  ,b.risk_indx_val_y6 AS risk_indx_val_y6_r
--                  ,b.risk_indx_val_y7 AS risk_indx_val_y7_r
--                  ,b.risk_indx_val_y8 AS risk_indx_val_y8_r
--                  ,b.risk_indx_val_y9 AS risk_indx_val_y9_r
--                  ,b.risk_indx_val_y10 AS risk_indx_val_y10_r
--                  ,b.risk_indx_val_y11 AS risk_indx_val_y11_r
--                  ,b.risk_indx_val_y12 AS risk_indx_val_y12_r
--                  ,b.risk_indx_val_y13 AS risk_indx_val_y13_r
--                  ,b.risk_indx_val_y14 AS risk_indx_val_y14_r
--                  ,b.risk_indx_val_y15 AS risk_indx_val_y15_r
--                  ,b.risk_indx_val_y16 AS risk_indx_val_y16_r
--                  ,b.risk_indx_val_y17 AS risk_indx_val_y17_r
--                  ,b.risk_indx_val_y18 AS risk_indx_val_y18_r
--                  ,b.risk_indx_val_y19 AS risk_indx_val_y19_r
--                  ,b.risk_indx_val_y20 AS risk_indx_val_y20_r
--            INTO   v_bay_risk_tab_n(1)
--                  ,v_bay_risk_tab_n(2)
--                  ,v_bay_risk_tab_n(3)
--                  ,v_bay_risk_tab_n(4)
--                  ,v_bay_risk_tab_n(5)
--                  ,v_bay_risk_tab_n(6)
--                  ,v_bay_risk_tab_n(7)
--                  ,v_bay_risk_tab_n(8)
--                  ,v_bay_risk_tab_n(9)
--                  ,v_bay_risk_tab_n(10)
--                  ,v_bay_risk_tab_n(11)
--                  ,v_bay_risk_tab_n(12)
--                  ,v_bay_risk_tab_n(13)
--                  ,v_bay_risk_tab_n(14)
--                  ,v_bay_risk_tab_n(15)
--                  ,v_bay_risk_tab_n(16)
--                  ,v_bay_risk_tab_n(17)
--                  ,v_bay_risk_tab_n(18)
--                  ,v_bay_risk_tab_n(19)
--                  ,v_bay_risk_tab_n(20)
--                  ,v_bay_risk_tab_n(21)
--                  ,v_bay_risk_tab_r(1)
--                  ,v_bay_risk_tab_r(2)
--                  ,v_bay_risk_tab_r(3)
--                  ,v_bay_risk_tab_r(4)
--                  ,v_bay_risk_tab_r(5)
--                  ,v_bay_risk_tab_r(6)
--                  ,v_bay_risk_tab_r(7)
--                  ,v_bay_risk_tab_r(8)
--                  ,v_bay_risk_tab_r(9)
--                  ,v_bay_risk_tab_r(10)
--                  ,v_bay_risk_tab_r(11)
--                  ,v_bay_risk_tab_r(12)
--                  ,v_bay_risk_tab_r(13)
--                  ,v_bay_risk_tab_r(14)
--                  ,v_bay_risk_tab_r(15)
--                  ,v_bay_risk_tab_r(16)
--                  ,v_bay_risk_tab_r(17)
--                  ,v_bay_risk_tab_r(18)
--                  ,v_bay_risk_tab_r(19)
--                  ,v_bay_risk_tab_r(20)
--                  ,v_bay_risk_tab_r(21)
--            FROM structools.st_risk_bay_noaction   A  INNER JOIN
--                 structools.st_risk_bay_replace    b
--            ON A.pick_id = b.pick_id
--            WHERE A.pick_id = v_pick_id; --i_bay_csr_rec.pick_id;                  
--         EXCEPTION
--            WHEN NO_DATA_FOUND THEN
--               NULL;
--         END;    
--            
--      END store_risk_bay;

      PROCEDURE set_vicinity_defect_flags IS

         CURSOR pick_id_defect_csr(i_pick_id VARCHAR2) IS
            SELECT --defect_action
                   part_cde
                  ,defect_cde
                  ,min(sev_cde) AS sev_cde
            FROM structools.st_eqp_defect
            WHERE pick_id = i_pick_id
            AND part_cde NOT IN ('HVXM','LVXM','AGOST','ASTAY','GSTAY','OSTAY')
            AND 
            (
            equip_grp_id = 'PWOD' AND defect_cde = 'UPAL'
            OR equip_grp_id = 'PWOD' AND defect_cde != 'UPAL' AND sev_cde <= '3'
            OR  equip_grp_id IN ('RECL','CAPB','REAC','LBS','RGTR','SECT','DSTR','PTSD','DOF') AND sev_cde <= '3'
            )
            GROUP BY part_cde, defect_cde
         ;
         
         CURSOR stay_xarm_defect_csr(i_pick_id VARCHAR2) IS
            SELECT --defect_action
                   part_cde
                  ,defect_cde
                  ,min(sev_cde) AS sev_cde
            FROM structools.st_eqp_defect
            WHERE pick_id = i_pick_id
            AND part_cde IN ('HVXM','LVXM','AGOST','ASTAY','GSTAY','OSTAY')
            AND equip_grp_id IN ( 'PWOD','PAUS')
            AND sev_cde <= '3'
            GROUP BY part_cde, defect_cde
            ORDER BY part_cde, defect_cde
         ;

         --v_pick_id            VARCHAR2(10);

      BEGIN
         /* get defects for all pickids for a vicinity into an assoc array */ 
         
--         v_pick_id := v_vic_defects_tab.FIRST;
--         
--         WHILE v_pick_id IS NOT NULL LOOP
--            --dbms_output.put_line('set_vic_flags. v_pick_id = '||v_pick_id); -- graz delete
--            v_vic_defects_tab(v_pick_id).DELETE;
--            v_pick_id := v_vic_defects_tab.NEXT(v_pick_id);
--         END LOOP;
         
         v_vic_defects_tab.DELETE;
         v_vic_stay_xarm_defects_tab.DELETE;
         --dbms_output.put_line('set_vic_flags. v_vic_defects_tab.COUNT = '||v_vic_defects_tab.COUNT); -- graz delete

         FOR i IN 1 .. v_eqp_tab.COUNT LOOP   /* for each asset in the vicinity */
            v_pick_id_defects_tab.DELETE;
            v_stay_xarm_defects_tab.DELETE;
            IF v_eqp_tab(i).del_year IS NOT NULL THEN --AND v_eqp_tab(i).del_event = 'REPLACE' THEN
               NULL;       --means defects, if existed, have been cleared by the previous pass
            ELSE
               IF (v_eqp_tab(i).egi = 'PWOD' OR v_eqp_tab(i).egi = 'PAUS')  AND v_eqp_tab(i).part_cde = ' '
               OR (v_eqp_tab(i).egi != 'PWOD' AND v_eqp_tab(i).egi != 'PAUS') THEN 
                  FOR c IN pick_id_defect_csr(v_eqp_tab(i).pick_id) LOOP
                     BEGIN
                        IF (v_eqp_tab(i).egi = 'PWOD' OR v_eqp_tab(i).egi = 'PAUS')
                        AND (c.part_cde = 'WPOLE' OR c.part_cde = 'REINF')
                        OR v_eqp_tab(i).egi = 'RECL' AND c.part_cde = 'RECL'
                        OR v_eqp_tab(i).egi = 'CAPB' AND c.part_cde = 'CAPB'
                        OR v_eqp_tab(i).egi = 'REAC' AND c.part_cde = 'REACTR'
                        OR v_eqp_tab(i).egi = 'LBS' AND c.part_cde = 'LBS'
                        OR v_eqp_tab(i).egi = 'RGTR' AND c.part_cde = 'RGTR'
                        OR v_eqp_tab(i).egi = 'SECT' AND c.part_cde = 'SECTR'
                        OR v_eqp_tab(i).egi = 'DSTR' AND c.part_cde = 'PMTX'
                        OR v_eqp_tab(i).egi = 'PTSD' AND c.part_cde = 'PTSD'
                        OR v_eqp_tab(i).egi = 'DOF' AND (c.part_cde = 'DOF' OR  c.part_cde = 'BARREL' AND c.defect_cde = 'UNSE') THEN
                           v_pick_id_defects_tab(c.defect_cde).sev_cde := c.sev_cde;
                           v_pick_id_defects_tab(c.defect_cde).part_cde := c.part_cde;
                        END IF;
                     EXCEPTION
                        WHEN OTHERS THEN
                           dbms_output.put_line ('set_vicinity_defect_flags. pick_id = '||v_eqp_tab(i).pick_id);
                           dbms_output.put_line ('set_vicinity_defect_flags. c.defect_cde = '||c.defect_cde);
                           dbms_output.put_line ('set_vicinity_defect_flags. c.sev_cde = '||c.sev_cde);
                           dbms_output.put_line ('set_vicinity_defect_flags. c.part_cde = '||c.part_cde);
                           RAISE;
                     END;
                  END LOOP;
                  IF v_pick_id_defects_tab.COUNT > 0 THEN
                     v_vic_defects_tab(v_eqp_tab(i).pick_id) := v_pick_id_defects_tab;
                  END IF;
               END IF;
               --IF (v_eqp_tab(i).egi = 'PWOD' OR v_eqp_tab(i).egi = 'PAUS') AND v_eqp_tab(i).part_cde > ' '  THEN -- ie a xarm or stay
               IF  v_eqp_tab(i).part_cde > ' '  THEN -- ie a xarm or stay
                  FOR c IN stay_xarm_defect_csr(v_eqp_tab(i).pick_id) LOOP
                  	IF c.part_cde = v_eqp_tab(i).part_cde THEN
                     	v_stay_xarm_defects_tab(c.defect_cde) := c.sev_cde;
                     --v_stay_xarm_defects_tab(c.defect_cde).part_cde := c.part_cde;
                  	END IF;
                  END LOOP;
                  IF v_stay_xarm_defects_tab.COUNT > 0 THEN
                     v_vic_stay_xarm_defects_tab(v_eqp_tab(i).pick_id||v_eqp_tab(i).part_cde) := v_stay_xarm_defects_tab;
                  END IF;
               END IF;
               
            END IF;
         END LOOP;

      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;      
         
      END set_vicinity_defect_flags;
      
      /************************************************************************************************/
      /* Process one vicinity for single year                                                         */
      /************************************************************************************************/
      PROCEDURE process_vicinity_year(i_vic_id  IN VARCHAR2
                                    , i_exec_year  IN PLS_INTEGER
                                    , i_mtzn       IN VARCHAR2 ) IS

         v_age                   PLS_INTEGER;
         v_pole_being_replaced   BOOLEAN := FALSE;
         --v_conductor_being_replaced BOOLEAN := FALSE;
         v_eqp_being_replaced    BOOLEAN := FALSE;
         v_eqp_being_replaced_defect    BOOLEAN := FALSE;
         v_DSTR_being_replaced   BOOLEAN := FALSE;
         v_RECL_being_replaced   BOOLEAN := FALSE;
         v_LBS_being_replaced    BOOLEAN := FALSE;
         v_PTSD_being_replaced   BOOLEAN := FALSE;
         v_CAPB_being_replaced   BOOLEAN := FALSE;
         v_RGTR_being_replaced   BOOLEAN := FALSE;
         v_othr_eqp_being_replaced    BOOLEAN := FALSE;
         x                       PLS_INTEGER := 0;
         v_action_eqp            BOOLEAN  := FALSE;
         v_action_eqp_pole       BOOLEAN  := FALSE;
         v_risk                  FLOAT(126);
         v_risk_sif              FLOAT(126);
         v_risk_noaction         FLOAT(126);
         v_risk_npv              FLOAT(126);
         v_risk_noaction_npv     FLOAT(126);
         v_risk_npv_sif          FLOAT(126);
         v_risk_noaction_npv_sif FLOAT(126);
--         v_conductor_repl        CHAR(1) := 'N';
--         v_key                   VARCHAR2(15);
         v_variation             CHAR(1);
         v_bundled               char(1);
         v_priority_ind          PLS_INTEGER;
         v_mtzn_priority         BOOLEAN;
         v_insert							BOOLEAN;

         PROCEDURE process_pwod_24_35(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
                                                                                      
         BEGIN

            IF v_strategy_pwod = 24
            OR v_strategy_pwod = 35 AND v_fsrtg_tab.EXISTS(v_eqp_tab(i).fsrtg) THEN
               IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                  v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
                  v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
                  v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
--                  v_eqp_year_tab(x).opex := 0;
                  v_eqp_year_tab(x).capex := v_eqp_tab(i).non_del_capex;
               ELSE
                  IF v_eqp_tab(i).wood_type = 'H' THEN
                     v_parm_val_key := 'PWOD'||'-'||'001'||'-'||'AGE';
                     IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                        v_cost_key := 'PWOD'||'N'||TRIM(v_cost_treatment_type)||'REPLACE'||TRIM(v_eqp_tab(i).pole_complexity)||TRIM(v_eqp_tab(i).region);
--                        v_eqp_year_tab(x).opex := 0;
                        v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := 'PWOD'||'-'||'001';
                        v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     ELSIF v_eqp_tab(i).reinf_cde IS NULL THEN
                        v_parm_val_key := 'PWOD'||'-'||'003'||'-'||'AGE';
                        IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                           v_cost_key := 'PWOD'||'N'||TRIM(v_cost_treatment_type)||'REINFORCE'||TRIM(v_eqp_tab(i).region);  
--                           v_eqp_year_tab(x).opex := 0;
                           v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                           v_eqp_year_tab(x).event := 'REINFORCE';
                           v_cond_key := 'PWOD'||'-'||'003';
                           v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                        END IF;
                     END IF;
                  ELSE     /* made of softwood */
                     v_parm_val_key := 'PWOD'||'-'||'002'||'-'||'AGE';
                     IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                        v_cost_key := 'PWOD'||'N'||TRIM(v_cost_treatment_type)||'REPLACE'||TRIM(v_eqp_tab(i).pole_complexity)||TRIM(v_eqp_tab(i).region);  
--                        v_eqp_year_tab(x).opex := 0;
                        v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := 'PWOD'||'-'||'002';
                        v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     END IF;
                  END IF;
               END IF;
            END IF;
                                   
         END process_pwod_24_35;               
                     
         PROCEDURE process_stay_pole_24_35(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
                                                                                       
         BEGIN

            IF v_strategy_pwod = 24
            OR v_strategy_pwod = 35 AND v_fsrtg_tab.EXISTS(v_eqp_tab(i).fsrtg) THEN
               IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                  v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
                  v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
                  v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
--                  v_eqp_year_tab(x).opex := 0;
                  v_eqp_year_tab(x).capex := v_eqp_tab(i).non_del_capex;
               ELSE
                  IF v_eqp_tab(i).wood_type = 'H' THEN
                     v_parm_val_key := 'PWOD'||'-'||'001'||'-'||'AGE';
                     IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                        v_cost_key := 'PWOD'||'N'||TRIM(v_cost_treatment_type)||'REPLACE'||TRIM(v_eqp_tab(i).pole_complexity)||TRIM(v_eqp_tab(i).region);  
--                        v_eqp_year_tab(x).opex := 0;
                        v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := 'PWOD'||'-'||'001';
                        v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     ELSIF v_eqp_tab(i).reinf_cde IS NULL THEN
                        v_parm_val_key := 'PWOD'||'-'||'003'||'-'||'AGE';
                        IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                           v_cost_key := 'PWOD'||'N'||TRIM(v_cost_treatment_type)||'REINFORCE'||TRIM(v_eqp_tab(i).region);  
--                           v_eqp_year_tab(x).opex := 0;
                           v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                           v_eqp_year_tab(x).event := 'REINFORCE';
                           v_cond_key := 'PWOD'||'-'||'003';
                           v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                        END IF;
                     END IF;
                  ELSE     /* made of softwood */
                     v_parm_val_key := 'PWOD'||'-'||'002'||'-'||'AGE';
                     IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                        v_cost_key := 'PWOD'||'N'||TRIM(v_cost_treatment_type)||'REPLACE'||TRIM(v_eqp_tab(i).pole_complexity)||TRIM(v_eqp_tab(i).region);  
--                        v_eqp_year_tab(x).opex := 0;
                        v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := 'PWOD'||'-'||'002';
                        v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     END IF;
                  END IF;
               END IF;
            END IF;
                  
         END process_stay_pole_24_35;               
                     
         PROCEDURE process_dstr_25_36(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS

            v_replace            BOOLEAN := FALSE;
               
         BEGIN
            IF v_strategy_dstr = 25
            OR v_strategy_dstr = 36 AND v_fsrtg_tab.EXISTS(v_eqp_tab(i).fsrtg) THEN
               IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                  v_replace := TRUE;
               ELSE
                  v_parm_val_key := 'DSTR'||'-'||'001'||'-'||'AGE';
                  IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                     v_cond_key := 'DSTR'||'-'||'001';
                     v_replace := TRUE;
                  END IF;
               END IF;                                                                                                                                          

               IF v_replace THEN
--                  v_eqp_year_tab(x).opex := 0;
                  v_eqp_year_tab(x).event := 'REPLACE';
                  IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                     v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
                     v_eqp_year_tab(x).capex        := v_eqp_tab(i).non_del_capex;
                     v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
                  ELSE
                     v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     IF v_pole_being_replaced THEN
                        v_cost_key := 'DSTR'||'Y'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).dstr_size)||TRIM(v_eqp_tab(i).region);
                     ELSE 
                        v_cost_key := 'DSTR'||'N'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).dstr_size)||TRIM(v_eqp_tab(i).region);
                     END IF;
                     v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                  END IF;
               END IF;
            END IF;
                     
         END process_dstr_25_36;               

         PROCEDURE process_dof_26_37(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS

            v_replace            BOOLEAN := FALSE;
               
         BEGIN
            IF v_strategy_dof = 26
            OR v_strategy_dof = 37 AND v_fsrtg_tab.EXISTS(v_eqp_tab(i).fsrtg) THEN
               IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                  v_replace := TRUE;
               ELSE
                  v_parm_val_key := 'DOF'||'-'||'001'||'-'||'AGE';
                  IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                     v_cond_key := 'DOF'||'-'||'001';
                     v_replace := TRUE;
                  END IF;
               END IF;                                                                                                                                          

               IF v_replace THEN
--                  v_eqp_year_tab(x).opex := 0;
                  v_eqp_year_tab(x).event := 'REPLACE';
                  IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                     v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
                     v_eqp_year_tab(x).capex        := v_eqp_tab(i).non_del_capex;
                     v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
                  ELSE
                     v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     IF v_pole_being_replaced THEN
                        v_cost_key := 'DOF'||'Y'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     ELSE  
                        v_cost_key := 'DOF'||'N'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     END IF;
                     v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                  END IF;
               END IF;
            END IF;
                     
         END process_dof_26_37;               

         PROCEDURE process_recl_27_38(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
                                         
            v_replace         BOOLEAN := FALSE; 

         BEGIN

            IF v_strategy_recl = 27
            OR v_strategy_recl = 38 AND v_fsrtg_tab.EXISTS(v_eqp_tab(i).fsrtg) THEN
               IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                  v_replace := TRUE;
               ELSE
                  v_parm_val_key := 'RECL'||'-'||'001'||'-'||'AGE';
                  IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN 
                     v_replace := TRUE;
                     v_cond_key := 'RECL'||'-'||'001';
                  END IF;
               END IF;

               IF v_replace THEN
--                  v_eqp_year_tab(x).opex := 0;
                  v_eqp_year_tab(x).event := 'REPLACE';
                  IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                     v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
                     v_eqp_year_tab(x).capex        := v_eqp_tab(i).non_del_capex;
                     v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
                  ELSE
                     v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     IF v_pole_being_replaced THEN
                        v_cost_key := 'RECL'||'Y'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).recl_phases)||TRIM(v_eqp_tab(i).region);  
                     ELSE
                        v_cost_key := 'RECL'||'N'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).recl_phases)||TRIM(v_eqp_tab(i).region);
                     END IF;
                     v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                  END IF;
               END IF;
            END IF;
                     
         END process_recl_27_38;               
                     
         PROCEDURE process_sect_28_39(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
                                        
            v_replace         BOOLEAN := FALSE; 

         BEGIN

            IF v_strategy_sect = 28
            OR v_strategy_sect = 39 AND v_fsrtg_tab.EXISTS(v_eqp_tab(i).fsrtg) THEN
               IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                  v_replace := TRUE;
               ELSE
                  v_parm_val_key := 'SECT'||'-'||'001'||'-'||'AGE';
                  IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                     v_replace := TRUE;
                     v_cond_key := 'SECT'||'-'||'001';
                  END IF;
               END IF;
               
               IF v_replace THEN
--                  v_eqp_year_tab(x).opex := 0;
                  v_eqp_year_tab(x).event := 'REPLACE';
                  IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                     v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
                     v_eqp_year_tab(x).capex        := v_eqp_tab(i).non_del_capex;
                     v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
                  ELSE
                     v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     IF v_pole_being_replaced THEN
                        v_cost_key := 'SECT'||'Y'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);  
                     ELSE
                        v_cost_key := 'SECT'||'N'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     END IF;
                     v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                  END IF;
               END IF;
            END IF;
                     
         END process_sect_28_39;               
                     
         PROCEDURE process_ptsd_29_40(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
                                       
            v_replace         BOOLEAN := FALSE; 

         BEGIN

            IF v_strategy_ptsd = 29
            OR v_strategy_ptsd = 40 AND v_fsrtg_tab.EXISTS(v_eqp_tab(i).fsrtg) THEN
               IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                  v_replace := TRUE;
               ELSE
                  v_parm_val_key := 'PTSD'||'-'||'001'||'-'||'AGE';
                  IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN 
                     v_replace := TRUE;
                     v_cond_key := 'PTSD'||'-'||'001';
                  END IF;
               END IF;

               IF v_replace THEN
--                  v_eqp_year_tab(x).opex := 0;
                  v_eqp_year_tab(x).event := 'REPLACE';
                  IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                     v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
                     v_eqp_year_tab(x).capex        := v_eqp_tab(i).non_del_capex;
                     v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
                  ELSE
                     v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     IF v_pole_being_replaced THEN
                        v_cost_key := 'PTSD'||'Y'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     ELSE  
                        v_cost_key := 'PTSD'||'N'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     END IF;
                     v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                  END IF;
               END IF;
            END IF;
                     
         END process_ptsd_29_40;               
                     
         PROCEDURE process_capb_30_41(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
                                       
            v_replace         BOOLEAN := FALSE; 

         BEGIN

            IF v_strategy_capb = 30
            OR v_strategy_capb = 41 AND v_fsrtg_tab.EXISTS(v_eqp_tab(i).fsrtg) THEN
               IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                  v_replace := TRUE;
               ELSE
                  v_parm_val_key := 'CAPB'||'-'||'001'||'-'||'AGE';
                  IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN 
                     v_replace := TRUE;
                     v_cond_key := 'CAPB'||'-'||'001';
                  END IF;
               END IF;

               IF v_replace THEN
--                  v_eqp_year_tab(x).opex := 0;
                  v_eqp_year_tab(x).event := 'REPLACE';
                  IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                     v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
                     v_eqp_year_tab(x).capex        := v_eqp_tab(i).non_del_capex;
                     v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
                  ELSE
                     v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     IF v_pole_being_replaced THEN
                        v_cost_key := 'CAPB'||'Y'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     ELSE  
                        v_cost_key := 'CAPB'||'N'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     END IF;
                     v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                  END IF;
               END IF;
            END IF;
                     
         END process_capb_30_41;               
                     
         PROCEDURE process_reac_31_42(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
                                       
            v_replace         BOOLEAN := FALSE; 

         BEGIN

            IF v_strategy_reac = 31
            OR v_strategy_reac = 42 AND v_fsrtg_tab.EXISTS(v_eqp_tab(i).fsrtg) THEN
               IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                  v_replace := TRUE;
               ELSE
                  v_parm_val_key := 'REAC'||'-'||'001'||'-'||'AGE';
                  IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN 
                     v_replace := TRUE;
                     v_cond_key := 'REAC'||'-'||'001';
                  END IF;
               END IF;

               IF v_replace THEN
--                  v_eqp_year_tab(x).opex := 0;
                  v_eqp_year_tab(x).event := 'REPLACE';
                  IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                     v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
                     v_eqp_year_tab(x).capex        := v_eqp_tab(i).non_del_capex;
                     v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
                  ELSE
                     v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     IF v_pole_being_replaced THEN
                        v_cost_key := 'REAC'||'Y'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     ELSE  
                        v_cost_key := 'REAC'||'N'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     END IF;
                     v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                  END IF;
               END IF;
            END IF;
                     
         END process_reac_31_42;               
                     
         PROCEDURE process_lbs_32_43(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
                                       
            v_replace         BOOLEAN := FALSE; 

         BEGIN

            IF v_strategy_lbs = 32
            OR v_strategy_lbs = 43 AND v_fsrtg_tab.EXISTS(v_eqp_tab(i).fsrtg) THEN
               IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                  v_replace := TRUE;
               ELSE
                  v_parm_val_key := 'LBS'||'-'||'001'||'-'||'AGE';
                  IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN 
                     v_replace := TRUE;
                     v_cond_key := 'LBS'||'-'||'001';
                  END IF;
               END IF;

               IF v_replace THEN
--                  v_eqp_year_tab(x).opex := 0;
                  v_eqp_year_tab(x).event := 'REPLACE';
                  IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                     v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
                     v_eqp_year_tab(x).capex        := v_eqp_tab(i).non_del_capex;
                     v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
                  ELSE
                     v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     IF v_pole_being_replaced THEN
                        v_cost_key := 'LBS'||'Y'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     ELSE  
                        v_cost_key := 'LBS'||'N'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     END IF;
                     v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                  END IF;
               END IF;
            END IF;
                     
         END process_lbs_32_43;               
                     
         PROCEDURE process_rgtr_33_44(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
                                       
            v_replace         BOOLEAN := FALSE; 

         BEGIN

            IF v_strategy_rgtr = 33
            OR v_strategy_rgtr = 44 AND v_fsrtg_tab.EXISTS(v_eqp_tab(i).fsrtg) THEN
               IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                  v_replace := TRUE;
               ELSE
                  v_parm_val_key := 'RGTR'||'-'||'001'||'-'||'AGE';
                  IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN 
                     v_replace := TRUE;
                     v_cond_key := 'RGTR'||'-'||'001';
                  END IF;
               END IF;

               IF v_replace THEN
--                  v_eqp_year_tab(x).opex := 0;
                  v_eqp_year_tab(x).event := 'REPLACE';
                  IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
                     v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
                     v_eqp_year_tab(x).capex        := v_eqp_tab(i).non_del_capex;
                     v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
                  ELSE
                     v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
                     IF v_pole_being_replaced THEN
                        v_cost_key := 'RGTR'||'Y'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     ELSE  
                        v_cost_key := 'RGTR'||'N'||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     END IF;
                     v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                  END IF;
               END IF;
            END IF;
                     
         END process_rgtr_33_44;               
                     

         /****************************************************************************************************************/
         /* Strategies 46 - 55 and 58 - 60 and 67 and 68 and 71                                                                        */
         /****************************************************************************************************************/
         PROCEDURE process_pwod_46(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
         
            v_pick_id            VARCHAR2(10);
                                                                                      
         BEGIN
         
            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
--               v_eqp_year_tab(x).opex := 0;
--               v_eqp_year_tab(x).capex := v_eqp_tab(i).non_del_capex;
            ELSE 
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  ELSIF v_pick_id_defects_tab.EXISTS('UNSS') AND v_eqp_tab(i).reinf_cde IS NOT NULL THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'002';
                  ELSIF v_pick_id_defects_tab.EXISTS('KDET') AND v_pick_id_defects_tab('KDET').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'003';
                  ELSIF v_pick_id_defects_tab.EXISTS('TROT') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'004';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPUS') AND v_pick_id_defects_tab('SPUS').sev_cde <= '2'
                  OR v_pick_id_defects_tab.EXISTS('SPTP') AND v_pick_id_defects_tab('SPTP').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'005';
                  ELSIF v_pick_id_defects_tab.EXISTS('UNSF') OR v_pick_id_defects_tab.EXISTS('UDLS') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'006';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPOU') AND v_pick_id_defects_tab('SPOU').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'007';
                  ELSIF v_pick_id_defects_tab.EXISTS('BAGL') OR v_pick_id_defects_tab.EXISTS('BNHV')
                  OR v_pick_id_defects_tab.EXISTS('BNLV') OR v_pick_id_defects_tab.EXISTS('BURN')
                  OR v_pick_id_defects_tab.EXISTS('FF01') OR v_pick_id_defects_tab.EXISTS('FF02') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'008';
                  ELSIF v_pick_id_defects_tab.EXISTS('UPAL') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'009';
                  ELSIF v_pick_id_defects_tab.EXISTS('REUN') OR v_pick_id_defects_tab.EXISTS('BOLT')
                  OR v_pick_id_defects_tab.EXISTS('REIN') OR v_pick_id_defects_tab.EXISTS('REUG')
                  OR v_pick_id_defects_tab.EXISTS('REWE') THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'010';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPGD') AND v_pick_id_defects_tab('SPGD').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'011';
                  ELSIF v_pick_id_defects_tab.EXISTS('TMPR') THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'012';
                  ELSIF v_pick_id_defects_tab.EXISTS('UNSS') AND v_eqp_tab(i).reinf_cde IS NULL THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'013';
                  END IF;
               END IF;
               
               IF v_cond_key IS NULL THEN /* ie. pole has no defects or none of the defects specified above */
                  v_parm_val_key := v_egi_vc2||'-'||'015'||'-'||'AGE';
                  IF v_eqp_tab(i).reinf_cde = 'LR' THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'014';
                  ELSIF NOT v_vic_defects_tab.EXISTS(v_pick_id)
                  AND v_eqp_tab(i).reinf_cde IS NULL 
                  AND v_eqp_tab(i).wood_type = 'H' AND v_eqp_year_tab(x).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'015';
                  END IF;
               END IF;

               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
            
               check_failure_prob(i, x, i_exec_year);
            END IF;

            /* capex for strategies >= 46 is now calculated in process_vicinity_year, after knowing what bundled value to use */                
--            /* adjust selected values, following the action */ 
--            IF v_eqp_year_tab(x).event = 'REPLACE' THEN
--               v_eqp_year_tab(x).opex := 0;
--               v_cost_key := TRIM(v_eqp_tab(i).egi)||'N'||TRIM(v_cost_treatment_type)||'REPLACE'||TRIM(v_eqp_tab(i).pole_complexity)||TRIM(v_eqp_tab(i).region);
--               v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
--            ELSIF v_eqp_year_tab(x).event = 'REINFORCE' THEN
--                v_cost_key := TRIM(v_eqp_tab(i).egi)||'N'||TRIM(v_cost_treatment_type)||'REINFORCE'||TRIM(v_eqp_tab(i).region);   
--                v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost; 
--                v_eqp_year_tab(x).opex := 0;                                            
--            END IF;                                                                                                                                          


         END process_pwod_46;               
                     
         PROCEDURE process_stay_pole_46(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
                                                                                      
            v_pick_id            VARCHAR2(10);
                                                                                      
         BEGIN

            v_pick_id := v_eqp_tab(i).pick_id;
         
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
--               v_eqp_year_tab(x).opex := 0;
--               v_eqp_year_tab(x).capex := v_eqp_tab(i).non_del_capex;
            ELSE 
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  ELSIF v_pick_id_defects_tab.EXISTS('UNSS') AND v_eqp_tab(i).reinf_cde IS NOT NULL THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'002';
                  ELSIF v_pick_id_defects_tab.EXISTS('KDET') AND v_pick_id_defects_tab('KDET').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'003';
                  ELSIF v_pick_id_defects_tab.EXISTS('TROT') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'004';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPUS') AND v_pick_id_defects_tab('SPUS').sev_cde <= '2'
                  OR v_pick_id_defects_tab.EXISTS('SPTP') AND v_pick_id_defects_tab('SPTP').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'005';
                  ELSIF v_pick_id_defects_tab.EXISTS('UNSF') OR v_pick_id_defects_tab.EXISTS('UDLS') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'006';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPOU') AND v_pick_id_defects_tab('SPOU').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'007';
                     v_cond_key := v_egi_vc2||'-'||'007';
                  ELSIF v_pick_id_defects_tab.EXISTS('BAGL') OR v_pick_id_defects_tab.EXISTS('BNHV')
                  OR v_pick_id_defects_tab.EXISTS('BNLV') OR v_pick_id_defects_tab.EXISTS('BURN')
                  OR v_pick_id_defects_tab.EXISTS('FF01') OR v_pick_id_defects_tab.EXISTS('FF02') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'008';
                  ELSIF v_pick_id_defects_tab.EXISTS('UPAL') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'009';
                  ELSIF v_pick_id_defects_tab.EXISTS('REUN') OR v_pick_id_defects_tab.EXISTS('BOLT')
                  OR v_pick_id_defects_tab.EXISTS('REIN') OR v_pick_id_defects_tab.EXISTS('REUG')
                  OR v_pick_id_defects_tab.EXISTS('REWE') THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'010';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPGD') AND v_pick_id_defects_tab('SPGD').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'011';
                  ELSIF v_pick_id_defects_tab.EXISTS('TMPR') THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'012';
                  ELSIF v_pick_id_defects_tab.EXISTS('UNSS') AND v_eqp_tab(i).reinf_cde IS NULL THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'013';
                  END IF;
               END IF;
               
               IF v_cond_key IS NULL THEN /* ie. pole has no defects or none of the specified above defects */
                  v_parm_val_key := v_egi_vc2||'-'||'015'||'-'||'AGE';
                  IF v_eqp_tab(i).reinf_cde = 'LR' THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'014';
                  ELSIF NOT v_vic_defects_tab.EXISTS(v_pick_id)
                  AND v_eqp_tab(i).reinf_cde IS NULL 
                  AND v_eqp_tab(i).wood_type = 'H' AND v_eqp_year_tab(x).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'015';
                  END IF;                  
               END IF;
            
               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
            
               check_failure_prob(i, x, i_exec_year);
            END IF;

            /* capex for strategies >= 46 is now calculated in process_vicinity_year, after knowing what bundled value to use */                
--            /* adjust selected values, following the action */ 
--            IF v_eqp_year_tab(x).event = 'REPLACE' THEN
--               v_eqp_year_tab(x).opex := 0;
--               v_cost_key := TRIM(v_eqp_tab(i).egi)||'N'||TRIM(v_cost_treatment_type)||'REPLACE'||TRIM(v_eqp_tab(i).pole_complexity)||TRIM(v_eqp_tab(i).region);
--               v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
--            ELSIF v_eqp_year_tab(x).event = 'REINFORCE' THEN
--                v_cost_key := TRIM(v_eqp_tab(i).egi)||'N'||TRIM(v_cost_treatment_type)||'REINFORCE'||TRIM(v_eqp_tab(i).region);   
--                v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost; 
--                v_eqp_year_tab(x).opex := 0;                                            
--            END IF;                                                                                                                                          


         END process_stay_pole_46;               
                     
         PROCEDURE process_dstr_47(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS

            v_pick_id            VARCHAR2(10);
                                                                                      
         BEGIN

            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE 
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE')
                  OR v_pick_id_defects_tab.EXISTS('UNCO')
                  OR v_pick_id_defects_tab.EXISTS('UNMD')
                  OR v_pick_id_defects_tab.EXISTS('LEOI') AND v_pick_id_defects_tab.EXISTS('CORR')
                  OR v_pick_id_defects_tab.EXISTS('HOSP') AND v_pick_id_defects_tab.EXISTS('BUDM')
                  OR v_pick_id_defects_tab.EXISTS('DAMG') AND v_pick_id_defects_tab.EXISTS('UNIT')
                  OR v_pick_id_defects_tab.EXISTS('TPSH') AND v_pick_id_defects_tab.EXISTS('UNIT')
                  OR v_pick_id_defects_tab.EXISTS('DAMG') AND v_pick_id_defects_tab.EXISTS('TPSH') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  END IF;
               END IF;                  
            
               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
               check_failure_prob(i, x, i_exec_year);
            END IF;
                     
         END process_dstr_47; 
                       
         PROCEDURE process_dof_48(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS

            v_pick_id            VARCHAR2(10);
                                                                                      
         BEGIN

            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE 
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE') AND v_pick_id_defects_tab('UNSE').part_cde = 'DOF'
                  OR v_pick_id_defects_tab.EXISTS('MDEF')
                  OR v_pick_id_defects_tab.EXISTS('HOSP') AND v_pick_id_defects_tab.EXISTS('CORR')
                  OR v_pick_id_defects_tab.EXISTS('STUN') AND v_pick_id_defects_tab.EXISTS('IADP')
                  OR v_pick_id_defects_tab.EXISTS('DIRT') AND v_pick_id_defects_tab.EXISTS('DAMG') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  ELSIF v_pick_id_defects_tab.EXISTS('UNSE') AND v_pick_id_defects_tab('UNSE').part_cde = 'BARREL'
                  AND v_eqp_tab(i).expulsion_type != 'Fire Safe Fuse' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'002';
                  END IF;
               END IF;
               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
               check_failure_prob(i, x, i_exec_year);
            END IF;
                     
         END process_dof_48; 
                       
         PROCEDURE process_recl_49(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS

            v_pick_id            VARCHAR2(10);
                                                                                      
         BEGIN

            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE')
                  OR v_pick_id_defects_tab.EXISTS('UNCO')
                  OR v_pick_id_defects_tab.EXISTS('RXCL')
                  OR v_pick_id_defects_tab.EXISTS('RXOP')
                  OR v_pick_id_defects_tab.EXISTS('LGAS')
                  OR v_pick_id_defects_tab.EXISTS('LEOI') AND v_pick_id_defects_tab.EXISTS('CORR')
                  OR v_pick_id_defects_tab.EXISTS('BUDM') AND v_pick_id_defects_tab.EXISTS('SDUN') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  END IF;
               END IF;
               IF v_cond_key IS NULL THEN
                  IF v_eqp_tab(i).recl_phases = '1' AND v_eqp_tab(i).ctl_typ = 'HYDRAULIC' 
                  AND (v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'002';
                  END IF;
               END IF;
               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
               check_failure_prob(i, x, i_exec_year);
            END IF;
                     
         END process_recl_49; 
                       
         PROCEDURE process_sect_50(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS

            v_pick_id            VARCHAR2(10);
                                                                                      
         BEGIN

            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE')
                  OR v_pick_id_defects_tab.EXISTS('UNCO') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  END IF;
               END IF;            
               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
               check_failure_prob(i, x, i_exec_year);
            END IF;
                     
         END process_sect_50; 
                       
         PROCEDURE process_ptsd_51(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS

            v_pick_id            VARCHAR2(10);
                                                                                      
         BEGIN

            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE')
                  OR v_pick_id_defects_tab.EXISTS('UNCO')
                  OR v_pick_id_defects_tab.EXISTS('HOSP') AND v_pick_id_defects_tab.EXISTS('INVB')
                  OR v_pick_id_defects_tab.EXISTS('SBFT') AND v_pick_id_defects_tab.EXISTS('RTSM')
                  OR v_pick_id_defects_tab.EXISTS('INVB') AND v_pick_id_defects_tab.EXISTS('HDAM') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  END IF;
               END IF;            

               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
               check_failure_prob(i, x, i_exec_year);
            END IF;
                     
         END process_ptsd_51; 
                       
         PROCEDURE process_capb_52(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS

            v_pick_id            VARCHAR2(10);
                                                                                      
         BEGIN

            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  END IF;
               END IF;

               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
            END IF;
                     
         END process_capb_52; 
                       
         PROCEDURE process_lbs_54(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS

            v_pick_id            VARCHAR2(10);
                                                                                      
         BEGIN
            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  END IF;
               END IF;

               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
               check_failure_prob(i, x, i_exec_year);
            END IF;
                     
         END process_lbs_54; 
                       
         PROCEDURE process_rgtr_55(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS

            v_pick_id            VARCHAR2(10);
                                                                                      
         BEGIN
            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE')
                  OR v_pick_id_defects_tab.EXISTS('UNCO')
                  OR v_pick_id_defects_tab.EXISTS('CORR')
                  OR v_pick_id_defects_tab.EXISTS('LEOI')
                  OR v_pick_id_defects_tab.EXISTS('UDLS')
                  OR v_pick_id_defects_tab.EXISTS('BUDM')
                  OR v_pick_id_defects_tab.EXISTS('LOIL')
                  OR v_pick_id_defects_tab.EXISTS('TCCF') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  END IF;
               END IF;

               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
               check_failure_prob(i, x, i_exec_year);
            END IF;
                     
         END process_rgtr_55; 
                       
         PROCEDURE process_pwod_68(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
                                                                                      
            v_pick_id            VARCHAR2(10);
            v_pwod_has_reinf_cond	CHAR(1);
                                                                                      
         BEGIN
         
            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE 
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE')
                  OR v_pick_id_defects_tab.EXISTS('UNSS')
                  OR v_pick_id_defects_tab.EXISTS('UNSF')
                  OR v_pick_id_defects_tab.EXISTS('WANT')
                  OR v_pick_id_defects_tab.EXISTS('FF01')
                  OR v_pick_id_defects_tab.EXISTS('FF02') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  ELSIF v_pick_id_defects_tab.EXISTS('TROT') THEN
                  	IF v_pick_id_defects_tab('TROT').sev_cde <= '2' THEN
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := v_egi_vc2||'-'||'002';
                     ELSIF  v_pick_id_defects_tab('TROT').sev_cde = '3' THEN
                     	IF v_eqp_tab(i).cust_ds_cnt > 1 
                        OR v_eqp_tab(i).fire_risk_zone = 'M' OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                        OR v_eqp_tab(i).fire_risk_zone = 'L' AND (v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH') THEN 
                           v_eqp_year_tab(x).event := 'REPLACE';
                           v_cond_key := v_egi_vc2||'-'||'002';
                        END IF;
                     END IF;
                  ELSIF v_pick_id_defects_tab.EXISTS('SPTP') AND v_pick_id_defects_tab('SPTP').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('SPOU') AND v_pick_id_defects_tab('SPOU').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('SPUS') AND v_pick_id_defects_tab('SPUS').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('KDET') AND v_pick_id_defects_tab('KDET').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('BAGL') AND v_pick_id_defects_tab('BAGL').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('BNHV') AND v_pick_id_defects_tab('BNHV').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('BNLV') AND v_pick_id_defects_tab('BNLV').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('BURN') AND v_pick_id_defects_tab('BURN').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('UDLS') AND v_pick_id_defects_tab('UDLS').sev_cde = '1' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'004';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPTP') AND v_pick_id_defects_tab('SPTP').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('SPOU') AND v_pick_id_defects_tab('SPOU').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('SPUS') AND v_pick_id_defects_tab('SPUS').sev_cde = '2' THEN
                     IF v_eqp_tab(i).cust_ds_cnt > 300 
                     OR v_eqp_tab(i).fire_risk_zone = 'X'
                     OR v_eqp_tab(i).fire_risk_zone = 'H' AND (v_eqp_tab(i).pub_saf_zone = 'MEDIUM' OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH')
                     OR ((v_eqp_tab(i).fire_risk_zone = 'M' OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X') AND v_eqp_tab(i).pub_saf_zone = 'VERY HIGH') THEN
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := v_egi_vc2||'-'||'005';
                     END IF;
                  ELSIF v_pick_id_defects_tab.EXISTS('KDET') THEN 
                  	IF v_pick_id_defects_tab('KDET').sev_cde = '2' THEN
                        IF v_eqp_tab(i).cust_ds_cnt > 1 
                        OR v_eqp_tab(i).fire_risk_zone = 'M' OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                        OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                           v_eqp_year_tab(x).event := 'REPLACE';
                           v_cond_key := v_egi_vc2||'-'||'006';
                        END IF;
                  	ELSIF v_pick_id_defects_tab('KDET').sev_cde = '3' THEN
                        IF v_eqp_tab(i).cust_ds_cnt > 1 
                        OR (v_eqp_tab(i).fire_risk_zone = 'M' OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X')
                        OR (v_eqp_tab(i).pub_saf_zone = 'VERY HIGH') THEN
                           v_eqp_year_tab(x).event := 'REPLACE';
                           v_cond_key := v_egi_vc2||'-'||'007';
                        END IF;
                     END IF;
                  ELSIF v_pick_id_defects_tab.EXISTS('BAGL') AND v_pick_id_defects_tab('BAGL').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('BNHV') AND v_pick_id_defects_tab('BNHV').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('BNLV') AND v_pick_id_defects_tab('BNLV').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('BURN') AND v_pick_id_defects_tab('BURN').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('UDLS') AND v_pick_id_defects_tab('UDLS').sev_cde = '2' THEN
                     IF v_eqp_tab(i).cust_ds_cnt > 80
                     OR v_eqp_tab(i).fire_risk_zone = 'M' OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X' THEN
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := v_egi_vc2||'-'||'008';
                     END IF;
                  END IF;
               END IF;

               IF v_cond_key IS NULL THEN  -- no REPLACE
                  
                  v_pwod_has_reinf_cond := 'N';
                  IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                     IF v_pick_id_defects_tab.EXISTS('REIN') AND v_pick_id_defects_tab('REIN').part_cde = 'WPOLE'
                     OR v_pick_id_defects_tab.EXISTS('SPGD') AND v_pick_id_defects_tab('SPGD').part_cde = 'WPOLE' AND v_pick_id_defects_tab('SPGD').sev_cde < '3'
                     OR v_pick_id_defects_tab.EXISTS('BOLT') AND v_pick_id_defects_tab('BOLT').part_cde = 'REINF'
                     OR v_pick_id_defects_tab.EXISTS('REUG') AND v_pick_id_defects_tab('REUG').part_cde = 'REINF'
                     OR v_pick_id_defects_tab.EXISTS('REUN') AND v_pick_id_defects_tab('REUN').part_cde = 'REINF'
                     OR v_pick_id_defects_tab.EXISTS('REWE') AND v_pick_id_defects_tab('REWE').part_cde = 'REINF'
                     OR v_pick_id_defects_tab.EXISTS('TMPR') AND v_pick_id_defects_tab('TMPR').part_cde = 'REINF'
                     OR v_pick_id_defects_tab.EXISTS('REBO') AND v_pick_id_defects_tab('REBO').part_cde = 'REINF' THEN
                        v_pwod_has_reinf_cond := 'Y';
                     END IF;
                  END IF;
                     
                  IF v_eqp_year_tab(x).age >= 25
                  AND v_eqp_tab(i).reinf_cde IS NULL
                  AND v_eqp_tab(i).cca_treated = 'N'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'009';
                  ELSIF v_eqp_year_tab(x).age >= 25
                  AND v_eqp_tab(i).reinf_cde IS NULL
                  AND v_eqp_tab(i).cca_treated = 'N'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'N' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'010';
                  ELSIF v_eqp_year_tab(x).age >= 25
                  AND v_eqp_tab(i).reinf_cde IS NULL
                  AND v_eqp_tab(i).cca_treated = 'Y'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'011';
                  ELSIF v_eqp_year_tab(x).age >= 25
                  AND v_eqp_tab(i).reinf_cde IS NULL
                  AND v_eqp_tab(i).cca_treated = 'Y'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'N' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'012';
                  ELSIF v_eqp_year_tab(x).age < 25
                  AND v_eqp_tab(i).reinf_cde IS NULL
                  AND v_eqp_tab(i).cca_treated = 'N'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'013';
                  ELSIF v_eqp_year_tab(x).age < 25
                  AND v_eqp_tab(i).reinf_cde IS NULL
                  AND v_eqp_tab(i).cca_treated = 'Y'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'014';
                  ELSIF v_eqp_tab(i).reinf_cde = 'LR'
                  AND v_eqp_tab(i).cca_treated = 'N'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'015';
                  ELSIF v_eqp_tab(i).reinf_cde = 'LR'
                  AND v_eqp_tab(i).cca_treated = 'Y'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'016';
                  ELSIF v_eqp_tab(i).wood_type = 'S'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'017';
                  END IF;
               END IF;

               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;

               check_failure_prob(i, x, i_exec_year);

           	END IF;

         END process_pwod_68;               
                     
         PROCEDURE process_stay_pole_68(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
         
            v_pick_id            VARCHAR2(10);
            v_pwod_has_reinf_cond	CHAR(1);
                                                                                      
         BEGIN
         
            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE 
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE')
                  OR v_pick_id_defects_tab.EXISTS('UNSS')
                  OR v_pick_id_defects_tab.EXISTS('UNSF')   THEN
                     IF v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                     OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := v_egi_vc2||'-'||'018';
                     END IF;
                  ELSIF v_pick_id_defects_tab.EXISTS('TROT') AND v_pick_id_defects_tab('TROT').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('KDET') AND v_pick_id_defects_tab('KDET').sev_cde = '1' THEN
                     IF v_eqp_tab(i).cust_ds_cnt > 80
                     OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                     OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := v_egi_vc2||'-'||'019';
                     END IF;
                  ELSIF v_pick_id_defects_tab.EXISTS('TROT') AND v_pick_id_defects_tab('TROT').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('KDET') AND v_pick_id_defects_tab('KDET').sev_cde = '2' THEN
                     IF v_eqp_tab(i).cust_ds_cnt > 300
                     OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                     OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := v_egi_vc2||'-'||'020';
                     END IF;
                  ELSIF v_pick_id_defects_tab.EXISTS('TROT') AND v_pick_id_defects_tab('TROT').sev_cde = '3'
                  OR v_pick_id_defects_tab.EXISTS('KDET') AND v_pick_id_defects_tab('KDET').sev_cde = '3' THEN
                     IF v_eqp_tab(i).cust_ds_cnt > 800
                     OR ((v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X' OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH')
                     AND  v_eqp_tab(i).cust_ds_cnt > 300) THEN
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := v_egi_vc2||'-'||'021';
                     END IF;
                  ELSIF v_pick_id_defects_tab.EXISTS('SPTP') AND v_pick_id_defects_tab('SPTP').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('SPOU') AND v_pick_id_defects_tab('SPOU').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('SPUS') AND v_pick_id_defects_tab('SPUS').sev_cde = '1' THEN
                     IF v_eqp_tab(i).cust_ds_cnt > 1 THEN
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := v_egi_vc2||'-'||'022';
                     END IF;
                  ELSIF v_pick_id_defects_tab.EXISTS('SPTP') AND v_pick_id_defects_tab('SPTP').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('SPOU') AND v_pick_id_defects_tab('SPOU').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('SPUS') AND v_pick_id_defects_tab('SPUS').sev_cde = '2' THEN
                     IF v_eqp_tab(i).cust_ds_cnt > 3000
                     OR (v_eqp_tab(i).cust_ds_cnt > 800 
                     AND (v_eqp_tab(i).fire_risk_zone = 'X' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH')) THEN
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := v_egi_vc2||'-'||'023';
                     END IF;
                  ELSIF v_pick_id_defects_tab.EXISTS('BAGL') AND v_pick_id_defects_tab('BAGL').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('BNHV') AND v_pick_id_defects_tab('BNHV').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('BNLV') AND v_pick_id_defects_tab('BNLV').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('BURN') AND v_pick_id_defects_tab('BURN').sev_cde = '1'
                  OR v_pick_id_defects_tab.EXISTS('UDLS') AND v_pick_id_defects_tab('UDLS').sev_cde = '1' THEN
                     IF v_eqp_tab(i).cust_ds_cnt > 80
                     OR v_eqp_tab(i).fire_risk_zone = 'M' OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                     OR v_eqp_tab(i).pub_saf_zone = 'MEDIUM' OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := v_egi_vc2||'-'||'024';
                     END IF;
                  ELSIF v_pick_id_defects_tab.EXISTS('BAGL') AND v_pick_id_defects_tab('BAGL').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('BNHV') AND v_pick_id_defects_tab('BNHV').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('BNLV') AND v_pick_id_defects_tab('BNLV').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('BURN') AND v_pick_id_defects_tab('BURN').sev_cde = '2'
                  OR v_pick_id_defects_tab.EXISTS('UDLS') AND v_pick_id_defects_tab('UDLS').sev_cde = '2' THEN
                     IF v_eqp_tab(i).cust_ds_cnt > 3000
                     OR (v_eqp_tab(i).cust_ds_cnt > 800 
                     AND (v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X' OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH')) THEN
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := v_egi_vc2||'-'||'025';
                     END IF;
                  ELSIF v_pick_id_defects_tab.EXISTS('WANT')
                  OR v_pick_id_defects_tab.EXISTS('FF01')
                  OR v_pick_id_defects_tab.EXISTS('FF02') THEN
                     IF v_eqp_tab(i).cust_ds_cnt > 300
                     OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                     OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        v_eqp_year_tab(x).event := 'REPLACE';
                        v_cond_key := v_egi_vc2||'-'||'026';
                     END IF;
                  END IF;
               END IF;

               IF v_cond_key IS NULL THEN  -- no REPLACE
                  
                  v_pwod_has_reinf_cond := 'N';
                  IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                     IF v_pick_id_defects_tab.EXISTS('REIN') AND v_pick_id_defects_tab('REIN').part_cde = 'WPOLE'
                     OR v_pick_id_defects_tab.EXISTS('SPGD') AND v_pick_id_defects_tab('SPGD').part_cde = 'WPOLE' AND v_pick_id_defects_tab('SPGD').sev_cde < '3'
                     OR v_pick_id_defects_tab.EXISTS('BOLT') AND v_pick_id_defects_tab('BOLT').part_cde = 'REINF'
                     OR v_pick_id_defects_tab.EXISTS('REUG') AND v_pick_id_defects_tab('REUG').part_cde = 'REINF'
                     OR v_pick_id_defects_tab.EXISTS('REUN') AND v_pick_id_defects_tab('REUN').part_cde = 'REINF'
                     OR v_pick_id_defects_tab.EXISTS('REWE') AND v_pick_id_defects_tab('REWE').part_cde = 'REINF'
                     OR v_pick_id_defects_tab.EXISTS('TMPR') AND v_pick_id_defects_tab('TMPR').part_cde = 'REINF'
                     OR v_pick_id_defects_tab.EXISTS('REBO') AND v_pick_id_defects_tab('REBO').part_cde = 'REINF' THEN
                        v_pwod_has_reinf_cond := 'Y';
                     END IF;
                  END IF;
                     
                  IF v_eqp_year_tab(x).age >= 25
                  AND v_eqp_tab(i).reinf_cde IS NULL
                  AND v_eqp_tab(i).cca_treated = 'N'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'027';
                  ELSIF v_eqp_year_tab(x).age >= 25
                  AND v_eqp_tab(i).reinf_cde IS NULL
                  AND v_eqp_tab(i).cca_treated = 'N'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'N' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'028';
                  ELSIF v_eqp_year_tab(x).age >= 25
                  AND v_eqp_tab(i).reinf_cde IS NULL
                  AND v_eqp_tab(i).cca_treated = 'Y'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'029';
                  ELSIF v_eqp_year_tab(x).age >= 25
                  AND v_eqp_tab(i).reinf_cde IS NULL
                  AND v_eqp_tab(i).cca_treated = 'Y'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'N' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'030';
                  ELSIF v_eqp_year_tab(x).age < 25
                  AND v_eqp_tab(i).reinf_cde IS NULL
                  AND v_eqp_tab(i).cca_treated = 'N'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'031';
                  ELSIF v_eqp_year_tab(x).age < 25
                  AND v_eqp_tab(i).reinf_cde IS NULL
                  AND v_eqp_tab(i).cca_treated = 'Y'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'032';
                  ELSIF v_eqp_tab(i).reinf_cde = 'LR'
                  AND v_eqp_tab(i).cca_treated = 'N'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'033';
                  ELSIF v_eqp_tab(i).reinf_cde = 'LR'
                  AND v_eqp_tab(i).cca_treated = 'Y'
                  AND v_eqp_tab(i).wood_type = 'H'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'034';
                  ELSIF v_eqp_tab(i).wood_type = 'S'
                  AND v_pwod_has_reinf_cond = 'Y' 	THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'035';
                  END IF;
               END IF;

               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;

               check_failure_prob(i, x, i_exec_year);

            END IF;

         END process_stay_pole_68;               
                     
         PROCEDURE process_pwod_71(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
         
            v_pick_id            VARCHAR2(10);
                                                                                      
         BEGIN
         
            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE 
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UPAL') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  ELSIF v_pick_id_defects_tab.EXISTS('UNSS') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'002';
                  ELSIF v_pick_id_defects_tab.EXISTS('KDET') AND v_pick_id_defects_tab('KDET').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'003';
                  ELSIF v_pick_id_defects_tab.EXISTS('TROT') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'004';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPUS') AND v_pick_id_defects_tab('SPUS').sev_cde <= '2'
                  OR v_pick_id_defects_tab.EXISTS('SPTP') AND v_pick_id_defects_tab('SPTP').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'005';
                  ELSIF v_pick_id_defects_tab.EXISTS('UNSF') OR v_pick_id_defects_tab.EXISTS('UDLS') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'006';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPOU') AND v_pick_id_defects_tab('SPOU').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'007';
                  ELSIF v_pick_id_defects_tab.EXISTS('BAGL') OR v_pick_id_defects_tab.EXISTS('BNHV')
                  OR v_pick_id_defects_tab.EXISTS('BNLV') OR v_pick_id_defects_tab.EXISTS('BURN')
                  OR v_pick_id_defects_tab.EXISTS('FF01') OR v_pick_id_defects_tab.EXISTS('FF02') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'008';
                  ELSIF v_pick_id_defects_tab.EXISTS('UNSE') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'009';
                  ELSIF v_pick_id_defects_tab.EXISTS('REUN') OR v_pick_id_defects_tab.EXISTS('BOLT')
                  OR v_pick_id_defects_tab.EXISTS('REIN') OR v_pick_id_defects_tab.EXISTS('REUG')
                  OR v_pick_id_defects_tab.EXISTS('REWE') THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'010';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPGD') AND v_pick_id_defects_tab('SPGD').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'011';
                  ELSIF v_pick_id_defects_tab.EXISTS('TMPR') THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'012';
                  END IF;
               END IF;
               
               IF v_cond_key IS NULL THEN /* ie. pole has no defects of the type specified above */
                  v_parm_val_key := v_egi_vc2||'-'||'014'||'-'||'AGE';
                  IF v_eqp_tab(i).reinf_cde = 'LR' THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'013';
                  ELSIF NOT v_vic_defects_tab.EXISTS(v_pick_id)
                  AND v_eqp_tab(i).reinf_cde IS NULL 
                  AND v_eqp_tab(i).wood_type = 'H' AND v_eqp_year_tab(x).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'014';
                  END IF;
               END IF;

               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
            
               check_failure_prob(i, x, i_exec_year);
            END IF;

         END process_pwod_71;               
                     
         PROCEDURE process_pwod_72(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
         
            v_pick_id            VARCHAR2(10);
            v_safety_frac		FLOAT;
 				v_idx_n					PLS_INTEGER;
            v_risk_key			VARCHAR2(40);
            v_safety_na			FLOAT;
         BEGIN
         
            v_pick_id := v_eqp_tab(i).pick_id;
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            v_cond_key := NULL;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE 
               IF v_vic_defects_tab.EXISTS(v_pick_id) THEN
                  v_pick_id_defects_tab := v_vic_defects_tab(v_pick_id);
                  IF v_pick_id_defects_tab.EXISTS('UNSE') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'001';
                  ELSIF v_pick_id_defects_tab.EXISTS('UNSS') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'002';
                  ELSIF v_pick_id_defects_tab.EXISTS('KDET') AND v_pick_id_defects_tab('KDET').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'003';
                  ELSIF v_pick_id_defects_tab.EXISTS('TROT') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'004';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPUS') AND v_pick_id_defects_tab('SPUS').sev_cde <= '2'
                  OR v_pick_id_defects_tab.EXISTS('SPTP') AND v_pick_id_defects_tab('SPTP').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'005';
                  ELSIF v_pick_id_defects_tab.EXISTS('UNSF') OR v_pick_id_defects_tab.EXISTS('UDLS') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'006';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPOU') AND v_pick_id_defects_tab('SPOU').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'007';
                  ELSIF v_pick_id_defects_tab.EXISTS('BAGL') OR v_pick_id_defects_tab.EXISTS('BNHV')
                  OR v_pick_id_defects_tab.EXISTS('BNLV') OR v_pick_id_defects_tab.EXISTS('BURN')
                  OR v_pick_id_defects_tab.EXISTS('FF01') OR v_pick_id_defects_tab.EXISTS('FF02') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'008';
                  ELSIF v_pick_id_defects_tab.EXISTS('UPAL') THEN
                     v_eqp_year_tab(x).event := 'REPLACE';
                     v_cond_key := v_egi_vc2||'-'||'009';
                  ELSIF v_pick_id_defects_tab.EXISTS('REUN') OR v_pick_id_defects_tab.EXISTS('BOLT')
                  OR v_pick_id_defects_tab.EXISTS('REIN') OR v_pick_id_defects_tab.EXISTS('REUG')
                  OR v_pick_id_defects_tab.EXISTS('REWE') THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'010';
                  ELSIF v_pick_id_defects_tab.EXISTS('SPGD') AND v_pick_id_defects_tab('SPGD').sev_cde <= '2' THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'011';
                  ELSIF v_pick_id_defects_tab.EXISTS('TMPR') THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'012';
                  END IF;
               END IF;
               
               IF v_cond_key IS NULL THEN /* ie. pole has no defects of the type specified above */
                  v_parm_val_key := v_egi_vc2||'-'||'014'||'-'||'AGE';
                  IF v_eqp_tab(i).reinf_cde = 'LR' THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'013';
                  ELSIF NOT v_vic_defects_tab.EXISTS(v_pick_id)
                  AND v_eqp_tab(i).reinf_cde IS NULL 
                  AND v_eqp_tab(i).wood_type = 'H' AND v_eqp_year_tab(x).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                     v_eqp_year_tab(x).event := 'REINFORCE';
                     v_cond_key := v_egi_vc2||'-'||'014';
                  ELSE
                  	v_parm_val_key := v_egi_vc2||'-'||'015'||'-'||'AGE_TOPUP';
                     IF v_eqp_year_tab(x).age > TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                     	v_parm_val_key := v_egi_vc2||'-'||'015'||'-'||'YEAR_TOPUP';
                     	IF v_eqp_year_tab(x).calendar_year >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                  			BEGIN
                              SELECT fire_frac + eshock_frac
                              INTO v_safety_frac
                              FROM structools.st_risk_fractions
                              WHERE pick_id = v_pick_id AND model_name = 'PWOD';
                           EXCEPTION
                           	WHEN NO_DATA_FOUND THEN
                              	v_safety_frac := NULL;
                           END;
                           IF v_safety_frac IS NOT NULL THEN
                           	v_parm_val_key := v_egi_vc2||'-'||'015'||'-'||'SAFETY_NA_TOPUP';
                              v_idx_n := v_eqp_year_tab(x).calendar_year - v_this_year + 1;
                           	v_risk_key := trim(v_pick_id)||'N';
                              v_safety_na := v_pwod_risk_tab(v_risk_key)(v_idx_n) * v_safety_frac;
                              IF v_safety_na > TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
                                 v_eqp_year_tab(x).event := 'REPLACE';
                                 v_cond_key := v_egi_vc2||'-'||'015';
                              END IF;
                           END IF;
                        END IF;
                     END IF;
                  END IF;
               END IF;

               IF v_cond_key IS NOT NULL THEN
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
            
            END IF;

         END process_pwod_72;               
                     
         PROCEDURE process_stay_xarm_58_62_67(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
         
         	v_pick_id            VARCHAR2(15);

         BEGIN
         
            v_egi_vc2 := TRIM(v_eqp_tab(i).part_cde);
            v_cond_key := NULL;
            v_pick_id := trim(v_eqp_tab(i).pick_id)||v_egi_vc2;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE 
            	IF v_egi_vc2 = 'LVXM' OR v_egi_vc2 = 'HVXM' THEN
               	IF v_vic_stay_xarm_defects_tab.EXISTS(v_pick_id) THEN
	                  v_stay_xarm_defects_tab := v_vic_stay_xarm_defects_tab(v_pick_id);
                  	IF v_stay_xarm_defects_tab.EXISTS('SPLI') AND v_stay_xarm_defects_tab('SPLI') = '1' THEN
                        v_cond_key := v_egi_vc2||'-'||'001';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('UNSE') AND v_stay_xarm_defects_tab('UNSE') = '2' THEN
                        v_cond_key := v_egi_vc2||'-'||'002';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('WANT') AND v_stay_xarm_defects_tab('WANT') = '2' THEN
                        v_cond_key := v_egi_vc2||'-'||'003';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('CORR') AND v_stay_xarm_defects_tab('CORR') = '1' THEN
                        v_cond_key := v_egi_vc2||'-'||'004';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('SPLI') AND v_stay_xarm_defects_tab('SPLI') = '2' THEN
                        v_cond_key := v_egi_vc2||'-'||'005';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('BURN') AND v_stay_xarm_defects_tab('BURN') = '2' THEN
                        v_cond_key := v_egi_vc2||'-'||'006';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('CORR') AND v_stay_xarm_defects_tab('CORR') = '2' THEN
                        v_cond_key := v_egi_vc2||'-'||'007';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('SPLI') AND v_stay_xarm_defects_tab('SPLI') = '3' THEN
                        v_cond_key := v_egi_vc2||'-'||'008';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('BURN') AND v_stay_xarm_defects_tab('BURN') = '3' THEN
                        v_cond_key := v_egi_vc2||'-'||'009';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('CORR') AND v_stay_xarm_defects_tab('CORR') = '3' THEN
                        v_cond_key := v_egi_vc2||'-'||'010';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('SXAM') AND (v_stay_xarm_defects_tab('SXAM') = '4' OR v_stay_xarm_defects_tab('SXAM') = '5') THEN
                        v_cond_key := v_egi_vc2||'-'||'011';
                     END IF;
                  END IF;
               ELSIF v_egi_vc2 = 'ASTAY' OR v_egi_vc2 = 'GSTAY' OR v_egi_vc2 = 'OSTAY' OR v_egi_vc2 = 'AGOST' THEN
               	IF v_vic_stay_xarm_defects_tab.EXISTS(v_pick_id) THEN
	                  v_stay_xarm_defects_tab := v_vic_stay_xarm_defects_tab(v_pick_id);
                  	IF v_stay_xarm_defects_tab.EXISTS('STMS') AND v_stay_xarm_defects_tab('STMS') = '1' THEN
                        v_cond_key := v_egi_vc2||'-'||'001';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('UNSE') AND v_stay_xarm_defects_tab('UNSE') = '2' THEN
                        v_cond_key := v_egi_vc2||'-'||'002';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('CORR') AND v_stay_xarm_defects_tab('CORR') = '1' THEN
                        v_cond_key := v_egi_vc2||'-'||'003';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('STMS') AND v_stay_xarm_defects_tab('STMS') = '2' THEN
                        v_cond_key := v_egi_vc2||'-'||'004';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('SIMS') AND v_stay_xarm_defects_tab('SIMS') = '2' THEN
                        v_cond_key := v_egi_vc2||'-'||'005';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('CORR') AND v_stay_xarm_defects_tab('CORR') = '2' THEN
                        v_cond_key := v_egi_vc2||'-'||'006';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('SWNC') AND v_stay_xarm_defects_tab('SWNC') = '2' THEN
                        v_cond_key := v_egi_vc2||'-'||'007';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('STMS') AND v_stay_xarm_defects_tab('STMS') = '3' THEN
                        v_cond_key := v_egi_vc2||'-'||'008';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('CORR') AND v_stay_xarm_defects_tab('CORR') = '3' THEN
                        v_cond_key := v_egi_vc2||'-'||'009';
                     END IF;
                  END IF;
               END IF;
               IF v_cond_key IS NOT NULL THEN
               	v_eqp_year_tab(x).event := 'REPLACE';
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
            END IF;
            
            
         END process_stay_xarm_58_62_67;               
                     
         PROCEDURE process_stay_xarm_74_79(i IN PLS_INTEGER, x IN PLS_INTEGER, i_exec_year IN PLS_INTEGER) IS
         
         	v_pick_id            VARCHAR2(15);

         BEGIN

            v_egi_vc2 := TRIM(v_eqp_tab(i).part_cde);
            v_cond_key := NULL;
            v_pick_id := trim(v_eqp_tab(i).pick_id)||v_egi_vc2;

            IF v_eqp_tab(i).non_del_pick_id IS NOT NULL THEN
               v_eqp_year_tab(x).event        := v_eqp_tab(i).non_del_event;
               v_eqp_year_tab(x).event_reason := v_eqp_tab(i).non_del_event_reason;
               v_eqp_year_tab(x).failure_prob := v_eqp_tab(i).non_del_failure_prob;
            ELSE 
               IF v_egi_vc2 = 'ASTAY' OR v_egi_vc2 = 'GSTAY' OR v_egi_vc2 = 'OSTAY' OR v_egi_vc2 = 'AGOST' THEN
               	IF v_vic_stay_xarm_defects_tab.EXISTS(v_pick_id) THEN
	                  v_stay_xarm_defects_tab := v_vic_stay_xarm_defects_tab(v_pick_id);
                  	IF v_stay_xarm_defects_tab.EXISTS('UNSE') AND v_stay_xarm_defects_tab('UNSE') = '1' THEN
                     	IF v_eqp_tab(i).fire_risk_zone = 'M' OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                        OR v_eqp_tab(i).pub_saf_zone = 'MEDIUM' OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        	v_cond_key := v_egi_vc2||'-'||'001';
                        END IF;
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('UNSE') AND v_stay_xarm_defects_tab('UNSE') = '2' THEN
                     	IF v_eqp_tab(i).fire_risk_zone = 'M' OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                        OR v_eqp_tab(i).pub_saf_zone = 'MEDIUM' OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        	v_cond_key := v_egi_vc2||'-'||'002';
                        END IF;
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('CORR') AND v_stay_xarm_defects_tab('CORR') = '1' THEN
                     	IF v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                        OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        	v_cond_key := v_egi_vc2||'-'||'003';
                        END IF;
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('CORR') AND v_stay_xarm_defects_tab('CORR') = '2' THEN
                     	IF v_eqp_tab(i).cust_ds_cnt > 1
                     	OR v_eqp_tab(i).fire_risk_zone = 'M' OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                        OR v_eqp_tab(i).pub_saf_zone = 'MEDIUM' OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        	v_cond_key := v_egi_vc2||'-'||'004';
                        END IF;
                     END IF;
                  END IF;
            	ELSIF v_egi_vc2 = 'LVXM' OR v_egi_vc2 = 'HVXM' THEN
                	IF v_vic_stay_xarm_defects_tab.EXISTS(v_pick_id) THEN
	                  v_stay_xarm_defects_tab := v_vic_stay_xarm_defects_tab(v_pick_id);
                  	IF v_stay_xarm_defects_tab.EXISTS('SPLI') AND v_stay_xarm_defects_tab('SPLI') = '1' AND v_eqp_tab(i).cust_ds_cnt > 1 THEN
                        v_cond_key := v_egi_vc2||'-'||'001';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('UNSE') AND v_stay_xarm_defects_tab('UNSE') = '1' AND v_eqp_tab(i).cust_ds_cnt > 1 THEN
                        v_cond_key := v_egi_vc2||'-'||'002';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('UNSE') AND v_stay_xarm_defects_tab('UNSE') = '2' AND v_eqp_tab(i).cust_ds_cnt > 1 THEN
                        v_cond_key := v_egi_vc2||'-'||'003';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('WANT') THEN
                        v_cond_key := v_egi_vc2||'-'||'004';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('CORR') AND v_stay_xarm_defects_tab('CORR') = '1' THEN
                     	IF v_eqp_tab(i).fire_risk_zone = 'M' OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                        OR v_eqp_tab(i).pub_saf_zone = 'MEDIUM' OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        	v_cond_key := v_egi_vc2||'-'||'005';
                        END IF;
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('SPLI') AND v_stay_xarm_defects_tab('SPLI') = '2' AND v_eqp_tab(i).cust_ds_cnt > 1 THEN
                        v_cond_key := v_egi_vc2||'-'||'006';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('BURN') AND v_stay_xarm_defects_tab('BURN') = '1' AND v_eqp_tab(i).cust_ds_cnt > 1 THEN
                        v_cond_key := v_egi_vc2||'-'||'007';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('CORR') AND v_stay_xarm_defects_tab('CORR') = '2' AND v_eqp_tab(i).cust_ds_cnt > 1 THEN
                     	IF v_eqp_tab(i).fire_risk_zone = 'M' OR v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                        OR v_eqp_tab(i).pub_saf_zone = 'MEDIUM' OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        	v_cond_key := v_egi_vc2||'-'||'008';
                        END IF;
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('SPLI') AND v_stay_xarm_defects_tab('SPLI') = '3' AND v_eqp_tab(i).cust_ds_cnt > 800 THEN
                     	IF v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                        OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        	v_cond_key := v_egi_vc2||'-'||'009';
                        END IF;
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('SPLI') AND v_stay_xarm_defects_tab('SPLI') = '3' AND v_eqp_tab(i).cust_ds_cnt > 3000 THEN
                     	v_cond_key := v_egi_vc2||'-'||'010';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('BURN') AND v_stay_xarm_defects_tab('BURN') = '2' AND v_eqp_tab(i).cust_ds_cnt > 1 THEN
                        v_cond_key := v_egi_vc2||'-'||'011';
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('LOOS') AND v_stay_xarm_defects_tab('LOOS') = '1' THEN
                     	IF v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                        OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        	v_cond_key := v_egi_vc2||'-'||'012';
                        END IF;
                  	ELSIF v_stay_xarm_defects_tab.EXISTS('LOOS') AND v_stay_xarm_defects_tab('LOOS') = '2' THEN
                     	IF v_eqp_tab(i).fire_risk_zone = 'H' OR v_eqp_tab(i).fire_risk_zone = 'X'
                        OR v_eqp_tab(i).pub_saf_zone = 'HIGH' OR v_eqp_tab(i).pub_saf_zone = 'VERY HIGH' THEN
                        	v_cond_key := v_egi_vc2||'-'||'013';
                        END IF;
                     END IF;
                  END IF;
               END IF;
               IF v_cond_key IS NOT NULL THEN
               	v_eqp_year_tab(x).event := 'REPLACE';
                  v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
            END IF;
            
         END process_stay_xarm_74_79;               
                     
         PROCEDURE optimise_pwod_46(i IN PLS_INTEGER, x IN PLS_INTEGER) IS

            v_repl               BOOLEAN := FALSE;
            v_event_rsn          VARCHAR2(30);
            v_egi_vc2              CHAR(4);

         BEGIN
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            
            IF v_eqp_tab(i).reinf_cde = 'LR' THEN
               v_eqp_year_tab(x).event := 'REPLACE';
               v_cond_key := v_egi_vc2||'-'||'017';
               v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
--               v_eqp_year_tab(x).opex := 0;
--               v_cost_key := TRIM(v_eqp_tab(i).egi)||'Y'||TRIM(v_cost_treatment_type)||'REPLACE'||TRIM(v_eqp_tab(i).pole_complexity)||TRIM(v_eqp_tab(i).region);
--               v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
            ELSE
               v_parm_val_key := v_egi_vc2||'-'||'018'||'-'||'AGE';
               IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
               v_eqp_year_tab(x).event := 'REPLACE';
               v_cond_key := v_egi_vc2||'-'||'018';
               v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
--               v_eqp_year_tab(x).opex := 0;
--               v_cost_key := TRIM(v_eqp_tab(i).egi)||'Y'||TRIM(v_cost_treatment_type)||'REPLACE'||TRIM(v_eqp_tab(i).pole_complexity)||TRIM(v_eqp_tab(i).region);
--               v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
               END IF;
            END IF;                                                                                                                                          
       
         END optimise_pwod_46;

         PROCEDURE optimise_pwod_71(i IN PLS_INTEGER, x IN PLS_INTEGER) IS

            v_repl               BOOLEAN := FALSE;
            v_event_rsn          VARCHAR2(30);
            v_egi_vc2              CHAR(4);

         BEGIN
            v_egi_vc2 := TRIM(v_eqp_tab(i).egi);
            
            IF v_eqp_tab(i).reinf_cde = 'LR' THEN
               v_eqp_year_tab(x).event := 'REPLACE';
               v_cond_key := v_egi_vc2||'-'||'016';
               v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
            ELSE
               v_parm_val_key := v_egi_vc2||'-'||'017'||'-'||'AGE';
               IF v_eqp_tab(i).age >= TO_NUMBER(v_cond_parm_tab(v_parm_val_key)) THEN
               v_eqp_year_tab(x).event := 'REPLACE';
               v_cond_key := v_egi_vc2||'-'||'017';
               v_eqp_year_tab(x).event_reason := v_cond_tab(v_cond_key);
               END IF;
            END IF;                                                                                                                                          
       
         END optimise_pwod_71;

         
      BEGIN /* process_vicinity_year */
      
--         IF v_bay_strategy THEN  /* flag all eqp covered by conductor replacement, poles and pole-top-eqp, to be replaced or not */ 
--            v_key := TO_CHAR(i_exec_year)||TRIM(i_vic_id); 
--            IF v_bay_stc_tab.EXISTS(v_key) THEN
--               v_conductor_repl := 'Y';
--            END IF;
--         END IF;

         v_eqp_last_year_tab := v_eqp_year_tab;
         v_eqp_year_tab.DELETE;

         FOR i IN 1 .. v_eqp_tab.COUNT LOOP   /* for each asset in the vicinity */
            v_action_eqp := FALSE; 
            v_age := v_eqp_tab(i).age + 1;
            v_eqp_tab(i).age := v_age;
            IF v_eqp_tab(i).egi_requested THEN
               x := x + 1;
               v_eqp_year_tab.EXTEND;
               v_eqp_year_tab(x).age := v_age;
               v_eqp_year_tab(x).calendar_year := i_exec_year;
               v_eqp_year_tab(x).pick_id := v_eqp_tab(i).pick_id;
               v_eqp_year_tab(x).pickid_suppressed := v_eqp_tab(i).pickid_suppressed;
               v_eqp_year_tab(x).equip_grp_id := TRIM(v_eqp_tab(i).egi);
               v_eqp_year_tab(x).failure_prob := 'N';
               IF v_eqp_tab(i).del_event = 'REPLACE' THEN        --del_event is only populated if the pickid had an event in one of the prev logical runs
                  v_variation := 'P';                                    -- ie. it does not apply to a single run or the first run in a series of runs
               ELSIF v_eqp_tab(i).del_event = 'REINFORCE' THEN    -- and therefore v_variation does not apply either in those cases.
                  v_variation := 'F';                                    
               ELSE
                  v_variation := NULL;
               END IF;

               --get_risk_for_year(v_eqp_tab(i).pick_id, TRIM(v_eqp_tab(i).egi), v_eqp_tab(i).part_cde,'NOACTION', v_age, i_exec_year, v_eqp_tab(i).init_age
               get_risk_for_year(v_eqp_tab(i).pick_id, TRIM(v_eqp_tab(i).egi), v_eqp_tab(i).part_cde,'NOACTION', v_age, i_exec_year
                              ,v_variation, v_eqp_tab(i).del_year, v_eqp_tab(i).fdr_cat
                              ,v_risk, v_risk_npv, v_risk_noaction_npv, v_risk_npv_sif, v_risk_noaction_npv_sif);
               v_eqp_year_tab(x).risk := v_risk;
               v_eqp_year_tab(x).risk_noaction := v_risk;
               
               v_action_eqp := TRUE;
               IF v_action_eqp AND (v_eqp_tab(i).egi = 'PWOD' OR v_eqp_tab(i).egi = 'PAUS') THEN
                  IF v_eqp_tab(i).part_cde = ' ' THEN 
                     v_action_eqp_pole := TRUE;
                  END IF;
               END IF;

               /* Do not test against strategies if the pickid is one of the committed pickids; ie never crete an event for them. */
               IF v_eqp_tab(i).scheduled = 'Y' THEN
                  NULL;
               ELSE    
                  IF v_eqp_tab(i).egi = 'PWOD' AND v_eqp_tab(i).part_cde = ' ' THEN
                     IF v_eqp_tab(i).pole_complexity = 'STAY' THEN
                        IF (v_strategy_pwod = 24 OR v_strategy_pwod = 35) THEN
                           process_stay_pole_24_35(i, x, i_exec_year);
                        ELSIF v_strategy_pwod = 46 THEN
                           process_stay_pole_46(i, x, i_exec_year);
                        ELSIF v_strategy_pwod = 68 THEN
                           process_stay_pole_68(i, x, i_exec_year);
                        ELSIF v_strategy_pwod = 71 THEN
                           process_PWOD_71(i, x, i_exec_year);
                        ELSIF v_strategy_pwod = 72 THEN
                           process_PWOD_72(i, x, i_exec_year);
                        END IF;
                     ELSE  
                        IF v_strategy_pwod = 24 OR v_strategy_pwod = 35 THEN
                           process_PWOD_24_35(i, x, i_exec_year);
                        ELSIF v_strategy_pwod = 46 THEN
                           process_PWOD_46(i, x, i_exec_year);
                        ELSIF v_strategy_pwod = 68 THEN
                           process_PWOD_68(i, x, i_exec_year);
                        ELSIF v_strategy_pwod = 71 THEN
                           process_PWOD_71(i, x, i_exec_year);
                        ELSIF v_strategy_pwod = 72 THEN
                           process_PWOD_72(i, x, i_exec_year);
                        END IF;
                     END IF;
                     IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                        v_pole_being_replaced := TRUE;
                     END IF;
                  ELSIF v_eqp_tab(i).part_cde = 'ASTAY' AND v_strategy_astay = 58
                  OR    v_eqp_tab(i).part_cde = 'GSTAY' AND v_strategy_gstay = 59
                  OR    v_eqp_tab(i).part_cde = 'OSTAY' AND v_strategy_ostay = 60
                  OR    v_eqp_tab(i).part_cde = 'AGOST' AND v_strategy_agost = 67
                  OR    v_eqp_tab(i).part_cde = 'HVXM' AND v_strategy_hvxm = 61
                  OR    v_eqp_tab(i).part_cde = 'LVXM' AND v_strategy_lvxm = 62  THEN
                     process_stay_xarm_58_62_67(i, x, i_exec_year);
                  ELSIF v_eqp_tab(i).part_cde = 'ASTAY' AND v_strategy_astay = 74
                  OR    v_eqp_tab(i).part_cde = 'GSTAY' AND v_strategy_gstay = 75
                  OR    v_eqp_tab(i).part_cde = 'OSTAY' AND v_strategy_ostay = 76
                  OR    v_eqp_tab(i).part_cde = 'AGOST' AND v_strategy_agost = 77
                  OR    v_eqp_tab(i).part_cde = 'HVXM' AND v_strategy_hvxm = 78
                  OR    v_eqp_tab(i).part_cde = 'LVXM' AND v_strategy_lvxm = 79  THEN
                     process_stay_xarm_74_79(i, x, i_exec_year);
                  ELSIF v_eqp_tab(i).egi = 'PAUS' AND v_eqp_tab(i).part_cde = ' ' THEN
                     IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                        v_pole_being_replaced := TRUE;
                     END IF;
                  ELSIF v_eqp_tab(i).egi = 'RECL' THEN
                     IF (v_strategy_recl = 27 OR v_strategy_recl = 38) THEN
                        process_RECL_27_38(i, x, i_exec_year);
                     ELSIF v_strategy_recl = 49 THEN
                        process_RECL_49(i, x, i_exec_year);
                     END IF;
                     IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                        v_eqp_being_replaced := TRUE;
                        v_RECL_being_replaced := TRUE;
                     END IF;
                  ELSIF v_eqp_tab(i).egi = 'CAPB' THEN
                     IF (v_strategy_capb = 30 OR v_strategy_capb = 42) THEN
                        process_CAPB_30_41(i, x, i_exec_year);
                     ELSIF v_strategy_capb = 52 THEN
                        process_CAPB_52(i, x, i_exec_year);
                     END IF;
                     IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                        v_eqp_being_replaced := TRUE;
                        v_capb_being_replaced := TRUE;
                     END IF;
                  ELSIF v_eqp_tab(i).egi = 'REAC' THEN
                     IF (v_strategy_reac = 31 OR v_strategy_reac = 42) THEN
                        process_REAC_31_42(i, x, i_exec_year);
                     END IF;
                     IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                        v_eqp_being_replaced := TRUE;
                     END IF;
                  ELSIF v_eqp_tab(i).egi = 'LBS' THEN
                     IF (v_strategy_lbs = 32 OR v_strategy_lbs = 43) THEN
                        process_LBS_32_43(i, x, i_exec_year);
                     ELSIF v_strategy_lbs = 54 THEN
                        process_LBS_54(i, x, i_exec_year);
                     END IF;
                     IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                        v_eqp_being_replaced := TRUE;
                        v_LBS_being_replaced := TRUE;
                     END IF;
                  ELSIF v_eqp_tab(i).egi = 'RGTR' THEN
                     IF (v_strategy_rgtr = 33 OR v_strategy_rgtr = 44) THEN
                        process_RGTR_33_44(i, x, i_exec_year);
                     ELSIF v_strategy_rgtr = 55 THEN
                        process_RGTR_55(i, x, i_exec_year);
                     END IF;
                     IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                        v_eqp_being_replaced := TRUE;
                        v_rgtr_being_replaced := TRUE;
                     END IF;
                  ELSIF v_eqp_tab(i).egi = 'SECT' THEN
                     IF (v_strategy_sect = 28 OR v_strategy_sect = 39) THEN
                        process_SECT_28_39(i, x, i_exec_year);
                     ELSIF v_strategy_sect = 50 THEN
                        process_SECT_50(i, x, i_exec_year);
                     END IF;
                     IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                        v_eqp_being_replaced := TRUE;
                     END IF;
                  ELSIF v_eqp_tab(i).egi = 'DSTR' THEN
                     IF (v_strategy_dstr = 25 OR v_strategy_dstr = 36) THEN
                        process_DSTR_25_36(i, x, i_exec_year);
                     ELSIF (v_strategy_dstr = 47) THEN
                        process_DSTR_47(i, x, i_exec_year);
                     END IF;   
                     IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                        v_eqp_being_replaced := TRUE;
                        v_DSTR_being_replaced := TRUE;
                     END IF;
                  ELSIF v_eqp_tab(i).egi = 'PTSD' THEN
                     IF (v_strategy_ptsd = 29 OR v_strategy_ptsd = 40) THEN   
                        process_PTSD_29_40(i, x, i_exec_year);
                     ELSIF v_strategy_ptsd = 51 THEN   
                        process_PTSD_51(i, x, i_exec_year);
                     END IF;
                     IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                        v_eqp_being_replaced := TRUE;
                        v_ptsd_being_replaced := TRUE;
                     END IF;
                  ELSIF v_eqp_tab(i).egi = 'DOF' THEN
                     IF (v_strategy_dof = 26 OR v_strategy_dof = 37) THEN
                        process_DOF_26_37(i, x, i_exec_year);
                     ELSIF v_strategy_dof = 48 THEN
                        process_DOF_48(i, x, i_exec_year);
                     END IF;
                     IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                        v_eqp_being_replaced := TRUE;
                     END IF;
                  END IF;
                  IF v_eqp_being_replaced AND INSTR(v_eqp_year_tab(x).event_reason, 'DEFECT') != 0 AND NOT v_eqp_being_replaced_defect THEN
                     v_eqp_being_replaced_defect := TRUE;
                  END IF;
               END IF;
            END IF;
         END LOOP;
      
--         IF v_conductor_repl = 'Y' THEN  
--            v_conductor_being_replaced := TRUE;
--         END IF; 
         
         /* This optimise logic could have been included in the main process_xxxx procedures. This is because all */
         /* dependency conditions are known due to the sequencing of vicinity equipment.                          */
         /* Therefore, I do not have any optimise_ procs for strategies 24 - 33, as all should be handled in       */
         /* process_xxxx_xx procs.                                                                                */  
         IF (v_pole_being_replaced --OR v_conductor_being_replaced 
         OR v_dstr_being_replaced  OR v_recl_being_replaced OR v_lbs_being_replaced 
         OR v_capb_being_replaced OR v_ptsd_being_replaced OR v_rgtr_being_replaced)
         AND v_action_eqp THEN 
            x := 0;
            FOR i IN 1 .. v_eqp_tab.COUNT LOOP   /* for each asset in the vicinity */
               IF v_eqp_tab(i).egi_requested THEN
                  x := x + 1;
               END IF;
               IF v_eqp_tab(i).egi = 'RECL' THEN
                  IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                     v_eqp_being_replaced := TRUE;
                     v_recl_being_replaced := TRUE;
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'CAPB' THEN
                  IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                     v_eqp_being_replaced := TRUE;
                     v_capb_being_replaced := TRUE;
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'REAC' THEN
                  IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                     v_eqp_being_replaced := TRUE;
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'LBS' THEN
                  IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                     v_eqp_being_replaced := TRUE;
                     v_lbs_being_replaced := TRUE;
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'RGTR' THEN
                  IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                     v_eqp_being_replaced := TRUE;
                     v_rgtr_being_replaced := TRUE;
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'SECT' THEN
                  IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                     v_eqp_being_replaced := TRUE;
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'DSTR' THEN
                  IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                     v_eqp_being_replaced := TRUE;
                     v_dstr_being_replaced := TRUE;
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'PTSD' THEN
                  IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                     v_eqp_being_replaced := TRUE;
                     v_ptsd_being_replaced := TRUE;
                  END IF;
               ELSIF v_eqp_tab(i).egi = 'DOF' THEN
                  IF v_eqp_year_tab(x).event = 'REPLACE' THEN
                     v_eqp_being_replaced := TRUE;
                  END IF;
               END IF;
            END LOOP;
         END IF;

         x := 0;
         IF v_eqp_being_replaced_defect AND NOT v_pole_being_replaced AND v_action_eqp_pole AND v_action_eqp THEN 
            FOR i IN 1 .. v_eqp_tab.COUNT LOOP
               IF v_eqp_tab(i).egi_requested THEN
                  x := x + 1;
               END IF;
               IF v_eqp_tab(i).scheduled != 'Y' THEN
                  IF v_eqp_tab(i).egi = 'PWOD' AND v_eqp_tab(i).pole_complexity != 'STAY' AND v_eqp_tab(i).part_cde = ' ' THEN
                     IF v_strategy_pwod = 46 THEN
                        optimise_pwod_46(i, x);
                     ELSIF v_strategy_pwod = 71 THEN
                        optimise_pwod_71(i, x);
                     END IF;
                  END IF;
               END IF;   
            END LOOP;
         END IF;
         
         
         x := 0;
         FOR i IN 1 .. v_eqp_tab.COUNT LOOP
            IF v_eqp_tab(i).egi_requested THEN
               x := x + 1;
               v_eqp_year_tab(x).risk_npv15 := NULL;
               v_eqp_year_tab(x).risk_noaction_npv15 := NULL;
               v_eqp_year_tab(x).risk_npv15_sif := NULL;
               v_eqp_year_tab(x).risk_noaction_npv15_sif := NULL;
               v_eqp_year_tab(x).labour_hrs := NULL;
               IF v_eqp_tab(i).egi = 'PWOD' AND v_eqp_tab(i).part_cde = ' ' AND v_strategy_pwod = 13
               OR v_eqp_tab(i).egi = 'DSTR' AND v_strategy_dstr = 14
               OR v_eqp_tab(i).egi = 'DOF' AND v_strategy_dof = 15
               OR v_eqp_tab(i).egi = 'RECL' AND v_strategy_recl = 16
               OR v_eqp_tab(i).egi = 'SECT' AND v_strategy_sect = 17
               OR v_eqp_tab(i).egi = 'PTSD' AND v_strategy_ptsd = 18
               OR v_eqp_tab(i).egi = 'LBS' AND v_strategy_lbs = 22
               OR v_eqp_tab(i).egi = 'RGTR' AND v_strategy_rgtr = 23 THEN
                  check_failure_prob(i, x, i_exec_year);
               END IF;
--               v_eqp_year_tab(x).opex := 0;
               /* Determine capex using correct unit rates, now that we know if both pole and eqp are being replaced or just pole or just eqp */
               IF v_eqp_year_tab(x).event = 'REPLACE' THEN
--               	v_eqp_year_tab(x).labour_hrs := 0;
                  IF v_eqp_tab(i).egi = 'PWOD' AND v_eqp_tab(i).part_cde = ' ' AND (v_strategy_pwod = 46 OR v_strategy_pwod = 68 OR v_strategy_pwod = 71 OR v_strategy_pwod = 72) THEN
                     IF v_eqp_being_replaced AND v_eqp_tab(i).pole_complexity != 'STAY' THEN
                        v_bundled := 'Y';
                     ELSE
                        v_bundled := 'N';
                     END IF;
                     v_cost_key := TRIM(v_eqp_tab(i).egi)||v_bundled||TRIM(v_cost_treatment_type)||'REPLACE'||TRIM(v_eqp_tab(i).pole_complexity)||TRIM(v_eqp_tab(i).region);
                     v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                     IF v_eqp_tab(i).egi = 'PWOD' AND v_eqp_tab(i).part_cde = ' ' THEN
                     	v_eqp_year_tab(x).labour_hrs := v_eqp_tab(i).labour_hrs;
                     END IF;
                  END IF;
                  /* all stays and xarms in v_eqp_tab table qualify for replacement */
                  IF v_eqp_tab(i).part_cde = 'ASTAY' OR v_eqp_tab(i).part_cde = 'GSTAY' OR v_eqp_tab(i).part_cde = 'OSTAY' OR v_eqp_tab(i).part_cde = 'AGOST' 
                  OR v_eqp_tab(i).part_cde = 'HVXM' OR v_eqp_tab(i).part_cde = 'LVXM' THEN
                  	IF v_eqp_tab(i).scheduled = 'Y'
                     OR  v_eqp_year_tab(x).event IS NOT NULL THEN 
                        v_cost_key := v_eqp_tab(i).part_cde||TRIM(v_cost_treatment_type);
                        v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                     END IF;
                  END IF;
                  IF v_eqp_tab(i).egi = 'DSTR' AND v_strategy_dstr = 47
                  OR v_eqp_tab(i).egi = 'DOF' AND v_strategy_dof = 48
                  OR v_eqp_tab(i).egi = 'RECL' AND v_strategy_recl = 49
                  OR v_eqp_tab(i).egi = 'SECT' AND v_strategy_sect = 50
                  OR v_eqp_tab(i).egi = 'PTSD' AND v_strategy_ptsd = 51
                  OR v_eqp_tab(i).egi = 'CAPB' AND v_strategy_capb = 52
                  OR v_eqp_tab(i).egi = 'REAC' AND v_strategy_reac = 53
                  OR v_eqp_tab(i).egi = 'LBS' AND v_strategy_lbs = 54
                  OR v_eqp_tab(i).egi = 'RGTR' AND v_strategy_rgtr = 55 THEN
                     IF v_pole_being_replaced THEN
                        v_bundled := 'Y';
                     ELSE
                        v_bundled := 'N';
                     END IF;
                     IF v_eqp_tab(i).egi = 'DSTR' THEN
                        v_cost_key := TRIM(v_eqp_tab(i).egi)||v_bundled||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).dstr_size)||TRIM(v_eqp_tab(i).region);
                     ELSIF v_eqp_tab(i).egi = 'RECL' THEN
                        v_cost_key := TRIM(v_eqp_tab(i).egi)||v_bundled||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).recl_phases)||TRIM(v_eqp_tab(i).region);  
                     ELSE
                        v_cost_key := TRIM(v_eqp_tab(i).egi)||v_bundled||TRIM(v_cost_treatment_type)||TRIM(v_eqp_tab(i).region);
                     END IF;
                     v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                  END IF;
               ELSIF v_eqp_year_tab(x).event = 'REINFORCE' THEN
               	--v_eqp_year_tab(x).labour_hrs := 0;
                  IF v_eqp_tab(i).egi = 'PWOD' AND v_eqp_tab(i).part_cde = ' ' AND (v_strategy_pwod = 46 OR v_strategy_pwod = 68  OR v_strategy_pwod = 71 OR v_strategy_pwod = 72) THEN
                     v_cost_key := TRIM(v_eqp_tab(i).egi)||'N'||TRIM(v_cost_treatment_type)||'REINFORCE'||TRIM(v_eqp_tab(i).region);
                     v_eqp_year_tab(x).capex := v_cost_tab(v_cost_key).total_cost;
                  END IF;
               END IF;
                  
               IF v_eqp_year_tab(x).event = 'REPLACE' THEN
--                  get_risk_for_year(v_eqp_year_tab(x).pick_id, TRIM(v_eqp_year_tab(x).equip_grp_id), v_eqp_tab(i).part_cde, 'REPLACE', 0
--                                   ,i_exec_year, v_eqp_tab(i).init_age, NULL, v_eqp_tab(i).del_year, v_eqp_tab(i).fdr_cat
--                                   ,v_risk, v_risk_npv, v_risk_noaction_npv
--                                   ,v_risk_npv_sif, v_risk_noaction_npv_sif); 
                  get_risk_for_year(v_eqp_year_tab(x).pick_id, TRIM(v_eqp_year_tab(x).equip_grp_id), v_eqp_tab(i).part_cde, 'REPLACE', 0
                                   ,i_exec_year, NULL, v_eqp_tab(i).del_year, v_eqp_tab(i).fdr_cat
                                   ,v_risk, v_risk_npv, v_risk_noaction_npv
                                   ,v_risk_npv_sif, v_risk_noaction_npv_sif); 
                  v_eqp_year_tab(x).risk := v_risk;       
                  v_eqp_year_tab(x).risk_npv15 := v_risk_npv;
                  v_eqp_year_tab(x).risk_noaction_npv15 := v_risk_noaction_npv;                                                                                     
                  v_eqp_year_tab(x).risk_npv15_sif := v_risk_npv_sif;
                  v_eqp_year_tab(x).risk_noaction_npv15_sif := v_risk_noaction_npv_sif;                                                                                     
               ELSIF v_eqp_year_tab(x).event = 'REINFORCE' THEN    
--                  get_risk_for_year(v_eqp_year_tab(x).pick_id, v_eqp_year_tab(x).equip_grp_id, v_eqp_tab(i).part_cde, 'REINFORCE', v_eqp_year_tab(x).age
--                                 ,i_exec_year, v_eqp_tab(i).init_age, NULL, v_eqp_tab(i).del_year, v_eqp_tab(i).fdr_cat
--                                 ,v_risk, v_risk_npv, v_risk_noaction_npv
--                                 ,v_risk_npv_sif, v_risk_noaction_npv_sif);                                                             
                  get_risk_for_year(v_eqp_year_tab(x).pick_id, v_eqp_year_tab(x).equip_grp_id, v_eqp_tab(i).part_cde, 'REINFORCE', v_eqp_year_tab(x).age
                                 ,i_exec_year, NULL, v_eqp_tab(i).del_year, v_eqp_tab(i).fdr_cat
                                 ,v_risk, v_risk_npv, v_risk_noaction_npv
                                 ,v_risk_npv_sif, v_risk_noaction_npv_sif);                                                             
                  v_eqp_year_tab(x).risk := v_risk;
                  v_eqp_year_tab(x).risk_npv15 := v_risk_npv;
                  v_eqp_year_tab(x).risk_noaction_npv15 := v_risk_noaction_npv;                                                                                     
                  v_eqp_year_tab(x).risk_npv15_sif := v_risk_npv_sif;
                  v_eqp_year_tab(x).risk_noaction_npv15_sif := v_risk_noaction_npv_sif;                                                                                     
               END IF;
               IF v_eqp_year_tab(x).risk < v_eqp_year_tab(x).risk_noaction THEN
                  v_eqp_year_tab(x).risk_reduction := v_eqp_year_tab(x).risk_noaction - v_eqp_year_tab(x).risk;
               ELSE
                  v_eqp_year_tab(x).risk_reduction := 0;
               END IF;
               IF v_eqp_year_tab(x).risk_npv15 < v_eqp_year_tab(x).risk_noaction_npv15 THEN
                  v_eqp_year_tab(x).risk_reduction_npv15 := v_eqp_year_tab(x).risk_noaction_npv15 - v_eqp_year_tab(x).risk_npv15;
               ELSE
                  v_eqp_year_tab(x).risk_reduction_npv15 := 0;
               END IF;
               IF v_eqp_year_tab(x).risk_npv15_sif < v_eqp_year_tab(x).risk_noaction_npv15_sif THEN
                  v_eqp_year_tab(x).risk_reduction_npv15_sif := v_eqp_year_tab(x).risk_noaction_npv15_sif - v_eqp_year_tab(x).risk_npv15_sif;
               ELSE
                  v_eqp_year_tab(x).risk_reduction_npv15_sif := 0;
               END IF;
               v_eqp_year_tab(x).rr_npv15_sif_fdr_cat_adj := v_eqp_year_tab(x).risk_reduction_npv15_sif * v_fdr_cat_sif_tab(v_eqp_tab(i).fdr_cat).adj_factor;
            END IF;
         END LOOP;

--         x := 0;                                 --pickid init
--         FOR i IN 1 .. v_eqp_tab.COUNT LOOP
--            IF v_eqp_tab(i).egi_requested THEN
--               x := x + 1;
--               IF (i_exec_year - v_year_0) = 1 THEN
--                  IF v_eqp_year_tab(x).EVENT IS NOT NULL THEN
--                     IF v_mtzn_sched_tab.EXISTS(i_mtzn) THEN
--                        v_eqp_tab(i).scheduled := 'Y';
--                     END IF;
--                  END IF;
--               END IF;
--            END IF;
--         END LOOP;



         /* insert rows for the year for all assets in vicinity */    
         IF v_mtzn_priority_tab.EXISTS(i_mtzn) THEN
            v_mtzn_priority := TRUE;
         ELSE
            v_mtzn_priority := FALSE;
         END IF;

         x := 0;
         FOR i IN 1 .. v_eqp_tab.COUNT LOOP
            IF v_eqp_tab(i).egi_requested THEN
               x := x + 1;
               IF v_mtzn_priority AND v_eqp_year_tab(x).EVENT IS NOT NULL THEN
                  v_priority_ind := 1;
               ELSE
                  v_priority_ind := NULL;
               END IF;
               v_insert := FALSE;
               IF v_eqp_tab(i).part_cde = 'ASTAY' OR v_eqp_tab(i).part_cde = 'GSTAY' OR v_eqp_tab(i).part_cde = 'OSTAY' OR v_eqp_tab(i).part_cde = 'AGOST' 
               OR v_eqp_tab(i).part_cde = 'HVXM' OR v_eqp_tab(i).part_cde = 'LVXM' THEN
                  IF v_eqp_tab(i).scheduled = 'Y' OR  v_eqp_year_tab(x).event IS NOT NULL THEN
                  	v_insert := TRUE;
                  END IF;
               ELSE
               	v_insert := TRUE;
               END IF;
               IF v_insert THEN
                  INSERT      
                  INTO structools.st_strategy_run
                  			(
                           RUN_ID
                           ,VIC_ID
                           ,PICK_ID
                           ,EQUIP_GRP_ID
                           ,EVENT
                           ,RISK
                           ,RISK_NOACTION
--                           ,REMAINING_VAL
                           ,LABOUR_HRS
                           ,CAPEX
--                           ,OPEX
                           ,AGE
                           ,CALENDAR_YEAR
--                           ,VARIATION
                           ,EVENT_REASON
                           ,RISK_REDUCTION
                           ,MAINT_ZONE_NAM
--                           ,CONDUCTOR_STRATEGY
                           ,RISK_NPV15
                           ,RISK_NOACTION_NPV15
                           ,RISK_REDUCTION_NPV15
                           ,RISK_NPV15_SIF
                           ,RISK_NOACTION_NPV15_SIF
                           ,RISK_REDUCTION_NPV15_SIF
                           ,FAILURE_PROB
                           ,SCHEDULED
                           ,RR_NPV15_SIF_FDR_CAT_ADJ
                           ,PRIORITY_IND
                           ,PART_CDE
                           ,PICKID_SUPPRESSED
                           )
                           VALUES  (v_run_id
                                   ,i_vic_id 
                                   ,v_eqp_year_tab(x).PICK_ID          
                                   ,TRIM(v_eqp_year_tab(x).EQUIP_GRP_ID)     
                                   ,v_eqp_year_tab(x).EVENT            
                                   ,v_eqp_year_tab(x).RISK             
                                   ,v_eqp_year_tab(x).RISK_NOACTION   
--                                   ,NULL                    --v_eqp_year_tab(x).REMAINING_VAL
                                   ,v_eqp_year_tab(x).labour_hrs
                                   ,v_eqp_year_tab(x).CAPEX            
--                                   ,v_eqp_year_tab(x).OPEX             
                                   ,v_eqp_year_tab(x).AGE              
                                   ,v_eqp_year_tab(x).CALENDAR_YEAR    
--                                   ,NULL  --variation
                                   ,v_eqp_year_tab(x).EVENT_REASON     
                                   ,v_eqp_year_tab(x).RISK_REDUCTION   
                                   ,i_mtzn
--                                   ,NULL  --v_conductor_repl
                                   ,v_eqp_year_tab(x).risk_npv15
                                   ,v_eqp_year_tab(x).risk_noaction_npv15
                                   ,v_eqp_year_tab(x).risk_reduction_npv15
                                   ,v_eqp_year_tab(x).risk_npv15_sif
                                   ,v_eqp_year_tab(x).risk_noaction_npv15_sif
                                   ,v_eqp_year_tab(x).risk_reduction_npv15_sif
                                   ,v_eqp_year_tab(x).failure_prob                
                                   ,v_eqp_tab(i).scheduled
                                   ,v_eqp_year_tab(x).rr_npv15_sif_fdr_cat_adj
                                   ,v_priority_ind
                                   ,v_eqp_tab(i).part_cde
                                   ,v_eqp_year_tab(x).pickid_suppressed
                                   );
            	END IF;
            END IF;
         END LOOP;
         
         /* Allocate the current year values to the 'PREVIOUS' values for the vicinity (only the values changing from one year to the next). */
         /* Set a flag if something has been replaced for the year for the vicinity and store the replaced thing in a table indexed by pick_id*/ 
         x := 0;

         FOR i IN 1 .. v_eqp_tab.COUNT LOOP
            IF v_eqp_tab(i).egi_requested THEN
               x := x + 1;
               v_eqp_tab(i).risk                 := v_eqp_year_tab(x).risk;
               v_eqp_tab(i).risk_noaction     := v_eqp_year_tab(x).risk_noaction;
               v_eqp_tab(i).age                     := v_eqp_year_tab(x).age;
               v_eqp_tab(i).calendar_year     := v_eqp_year_tab(x).calendar_year;
            END IF;
         END LOOP;

      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;
         
      END process_vicinity_year;

      /************************************************************************************************/
      /* Process single vicinity for the requested time frame.                                        */
      /************************************************************************************************/
      PROCEDURE process_vicinity(i_vic_id IN VARCHAR2  --, i_trtmnt_delay_yrs IN INTEGER
                                 ,i_mtzn  IN VARCHAR2) IS

        
      BEGIN
               
         set_vicinity_defect_flags;
         
         v_exec_year := v_year_0 + 1;
         WHILE v_exec_year <= v_last_year LOOP
            process_vicinity_year(i_vic_id, v_exec_year, i_mtzn);
            v_exec_year := v_exec_year + 1;
         END LOOP;

      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;

      END process_vicinity;
      
      FUNCTION get_bay_value_for_year(i_age           INTEGER
                                     ,i_area_type     CHAR 
                                     ,i_lvco_cnt      INTEGER
                                     ,i_hvco_cnt      INTEGER
                                     ,i_hvsp_cnt      INTEGER
                                     ,i_span_len      NUMBER) RETURN INTEGER AS
                                       
         v_cost               NUMBER;
--         v_life_exp           PLS_INTEGER;
         v_value              INTEGER := 0;
         
      BEGIN
      
--         v_life_exp := v_life_exp_tab('BAY');
            
         IF i_age < v_bay_life_exp THEN
            v_cost_key := TRIM(i_area_type)||TO_CHAR(i_lvco_cnt,'00')||TO_CHAR(i_hvco_cnt,'00')||TO_CHAR(i_hvsp_cnt,'00');
            v_cost     := v_cost_tab(v_cost_key).total_cost * i_span_len / 1000; -- cost of span per km * span_length_in_km;
            v_value    := v_cost 
                        - (v_cost / v_bay_life_exp) * i_age;
         END IF;
         
         RETURN v_value;
      EXCEPTION
         WHEN OTHERS THEN 
            dbms_output.put_line('get bay VALUE FOR YEAR');
            dbms_output.put_line('i_area_type = '||i_area_type||' i_lvco_cnt = '||i_lvco_cnt||' i_hvco_cnt = '||i_hvco_cnt
                                    ||' i_hvsp_cnt = '||i_hvsp_cnt);
         RAISE;
      
      END get_bay_value_for_year;

      PROCEDURE process_bays IS
         
         v_stc1_prev             VARCHAR2(10);
         v_stc2_prev             VARCHAR2(10);
         v_span_bay_cnt          PLS_INTEGER;
         v_span_cnt              PLS_INTEGER;
         v_rem                   PLS_INTEGER;
         v_thin_cu_diam          NUMBER(3,1);
         v_thin_cu_age           PLS_INTEGER;
         v_cu_age                PLS_INTEGER;
         v_steel_age             PLS_INTEGER;
         v_aac_age               PLS_INTEGER;
         v_acsr_age              PLS_INTEGER;
         v_thick_cu_diam         NUMBER(3,1);
         v_thick_cu_age          PLS_INTEGER;
         v_aaac_age              PLS_INTEGER;
         v_hendrix_age           PLS_INTEGER;
         v_lvabc_age             PLS_INTEGER;
         v_bay_age               PLS_INTEGER;
         v_span_replace          BOOLEAN;
         v_cond_score1           PLS_INTEGER;
         v_cond_score2           PLS_INTEGER;
         v_diam1                 NUMBER(3,1);
         v_diam2                 NUMBER(3,1);
         v_diam3                 NUMBER(3,1);
         v_diam4                 NUMBER(3,1);
         v_diam5                 NUMBER(3,1);
         v_diam6                 NUMBER(3,1);
         v_diam7                 NUMBER(3,1);
         v_diam8                 NUMBER(3,1);
         v_diam9                 NUMBER(3,1);
         v_diam10                NUMBER(3,1);
         v_age1                  PLS_INTEGER;
         v_age2                  PLS_INTEGER;
         v_age3                  PLS_INTEGER;
         v_age4                  PLS_INTEGER;
         v_age_topup           PLS_INTEGER;
         v_year_topup           PLS_INTEGER;
         v_safety_na_topup   FLOAT;
         v_dist_coast_km1        PLS_INTEGER;
         v_dist_coast_km2        PLS_INTEGER;
         v_dist_coast_km3        PLS_INTEGER;
         v_dist_coast_km4        PLS_INTEGER;
         v_dist_coast_km5        PLS_INTEGER;
         v_dist_coast_km6        PLS_INTEGER;
         v_pfail                 FLOAT(126);
         v_chng_yr               PLS_INTEGER;
         

      CURSOR bay_20_34_45_56_57_csr IS

         WITH carrier_model AS         --carrier model field val
         (
         SELECT DISTINCT NVL(A.pick_id, b.conductor_pick_id) AS pick_id
               ,CASE
                  WHEN b.conductor_pick_id IS NOT NULL   THEN  TO_CHAR(b.carrier_model_code)
                  ELSE                                         TO_CHAR(A.carrier_model_code)
               END                     AS carrier_model_code
         FROM structoolsw.carrier_detail            A     FULL OUTER JOIN
              structools.st_carrier_detail         b
         ON A.pick_id = b.conductor_pick_id
         )
         ,
         --WITH bays AS
         bays AS
         (         
         SELECT A.pick_id
               ,A.stc1
               ,A.stc2
               ,A.instln_year             AS instl_yr
               ,A.bay_equip_cde 
               ,b.carrier_model_code
               ,A.segment_id
               ,CASE
                    WHEN A.segment_id IS NOT NULL AND c.mtzn    IS NULL              THEN  'UNKNOWN'
                    WHEN A.segment_id IS NOT NULL AND c.mtzn    IS NOT NULL       THEN  c.mtzn
                    ELSE                                                                                                    NULL
               END                                  AS segment_mtzn
               ,A.fire_risk_zone_cls||A.max_safety_zone_rtg AS fsrtg
              ,CASE 
                  WHEN UPPER(d.scnrrr_fdr_cat_nam) = 'NONE'            THEN 'SHORTRURAL'
                  WHEN d.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                  ELSE                                                            UPPER(d.scnrrr_fdr_cat_nam)
              END                                                            AS fdr_cat
              ,A.condition_code             --strat 57
         FROM  structools.st_bay_equipment A    INNER JOIN
               carrier_model  b
               --structools.carrier_detail  b               carrier model field val
         ON 'C'||A.conductor_pick_id = b.pick_id
                                                                  LEFT OUTER JOIN
               structools.st_eqp_mtzn c
         ON A.segment_id = c.pick_id      
                                                                  LEFT OUTER JOIN
               structools.st_hv_feeder        d
         ON A.feeder_id = d.hv_fdr_id                          
         --WHERE A.pick_id = 'B990401'     --graz      
         )
         ,
         def_bays AS
         (
         SELECT DISTINCT 
                pick_id
               ,defect_action
         FROM   structools.st_bay_defect
         WHERE  defect_action = 'REPLACE'
         )
         ,
         actioned_or_committed_bays_tmp AS        --pickid init
         (
--         SELECT pick_id
--               ,event
--               ,del_year
--         FROM structools.st_del_program_pickid
--         WHERE base_run_id BETWEEN v_parent_run_id_first AND v_parent_run_id_last
--         AND program_id = v_parent_run_program_id
--         AND event != 'REPAIR'
--         AND pick_id LIKE 'B%'          --added this
--         UNION ALL
         SELECT pick_id
               ,event
               ,commit_yr   AS del_year
         FROM structools.st_pickid_committed
         WHERE pick_id LIKE 'B%'
         )
         ,
         actioned_bays_tmp AS
         (
         SELECT pick_id
               ,MAX(RPAD(event,9)||del_year)                AS max_event_seq 
         --FROM structools.st_del_program_pickid               --pickid init
         FROM actioned_or_committed_bays_tmp
--         WHERE base_run_id BETWEEN v_parent_run_id_first AND v_parent_run_id_last --pickid init
--         AND program_id = v_parent_run_program_id
--         AND event != 'REPAIR'
--         AND pick_id LIKE 'B%'          --added this
         GROUP BY pick_id
         )
         ,           
         actioned_bays AS
         (
         SELECT A.pick_id
               ,RTRIM(SUBSTR(A.max_event_seq,1,9))      AS event
               ,SUBSTR(A.max_event_seq,10,4)            AS del_yr
               ,CASE                                        --pickid init
                  WHEN b.event IS NULL THEN  'N'
                  ELSE                       'Y'
                END                                       AS scheduled
          FROM actioned_bays_tmp
                                    A  LEFT OUTER JOIN      --pickid init
              structools.st_pickid_committed b
         ON A.pick_id = b.pick_id                                    
         )               
--         ,
--         non_del_bays AS
--         (
--         SELECT DISTINCT pick_id
--               ,event_reason
--         FROM structools.st_bay_strategy_run
--         WHERE run_id = v_parent_run_id_last
--         AND event > ' '
--         AND calendar_year = v_year_0       /* last year of the parent run */
--         AND pick_id NOT IN (SELECT DISTINCT pick_id FROM actioned_bays)
--         )      
			,      
         suppressed_bays AS
         (
         SELECT pick_id
         			  ,'Y'				AS suppressed			
         FROM structools.st_pickid_suppressed_init
         WHERE equip_grp_id = 'BAY'
         UNION
         SELECT pick_id
         			,'P'				AS suppressed	
         FROM structools.st_pickid_suppressed
         WHERE equip_grp_id = 'BAY'
         AND scenario_id = v_suppress_pickids_prog_id
         )
         ,
         bays_all AS
         (
         SELECT A.pick_id
               ,A.instl_yr
               ,A.carrier_model_code
               ,A.bay_equip_cde 
               ,A.stc1
               ,A.stc2
               ,b.mtzn
               ,ROUND(b.span_len)                     AS span_len
               ,b.lvco_cnt
               ,b.hvco_cnt 
               ,b.hvsp_cnt
               ,b.area_type
               ,c.material_cde
               ,c.wire_diam
               ,c.material_cde_common
               ,c.cond_diam
               ,CASE
                  WHEN G.del_yr IS NOT NULL        THEN  NULL
                  ELSE                                   f.defect_action
               END                        AS defect_action
--               ,f.defect_action
               ,A.segment_id
               ,A.segment_mtzn
               ,G.del_yr
               ,G.event
               --,h.pick_id                 AS non_del_pick_id
               ,NULL                      AS non_del_pick_id
               --,h.event_reason            AS non_del_event_reason
               ,NULL                      AS non_del_event_reason
               ,A.fsrtg
               ,A.fdr_cat
               ,NVL(G.scheduled, 'N')     AS scheduled         --pickid init
               ,CASE
                  WHEN G.del_yr IS NOT NULL        THEN  NULL
                  ELSE                                   A.condition_code
               END                        AS condition_code
               ,NULL                      AS high_risk_area
               ,NULL                      AS bay_forced_pick_id
               ,NULL                      AS dist_coast_km
               ,i.pick_id                 AS pickid_suppressed
               ,i.suppressed
               ,NULL                      AS defect_action2
         FROM bays                           A  INNER JOIN
              structools.st_span                b
         ON   A.stc1 = b.stc1
         AND  A.stc2 = b.stc2
                                                LEFT OUTER JOIN
              structools.st_carr_mdl_material   c
         ON   A.carrier_model_code = TO_CHAR(c.carrier_mdl_cde)                                             
                                                LEFT OUTER JOIN
              def_bays                       f
         ON A.pick_id = f.pick_id
                                                LEFT OUTER JOIN
              actioned_bays                  G
         ON A.pick_id = G.pick_id                                                                                                      
                                                LEFT OUTER JOIN
              suppressed_bays					i
         ON A.pick_id = i.pick_id                                                                                                      
--                                                LEFT OUTER JOIN
--              non_del_bays              h
--         ON A.pick_id = h.pick_id                      
         )
         SELECT * FROM bays_all
         ORDER BY stc1, stc2, bay_equip_cde, pick_id
      ;

      CURSOR bay_63_csr IS

         WITH carrier_model AS         --carrier model field val
         (
         SELECT DISTINCT NVL(A.pick_id, b.conductor_pick_id) AS pick_id
               ,CASE
                  WHEN b.conductor_pick_id IS NOT NULL   THEN  TO_CHAR(b.carrier_model_code)
                  ELSE                                         TO_CHAR(A.carrier_model_code)
               END                     AS carrier_model_code
         FROM structoolsw.carrier_detail            A     FULL OUTER JOIN
              structools.st_carrier_detail         b
         ON A.pick_id = b.conductor_pick_id
         )
         ,
         bays AS
         (         
         SELECT A.pick_id
               ,A.stc1
               ,A.stc2
               ,A.instln_year             AS instl_yr
               ,A.bay_equip_cde 
               ,b.carrier_model_code
               ,A.segment_id
               ,CASE
                    WHEN A.segment_id IS NOT NULL AND c.mtzn    IS NULL              THEN  'UNKNOWN'
                    WHEN A.segment_id IS NOT NULL AND c.mtzn    IS NOT NULL       THEN  c.mtzn
                    ELSE                                                                                                    NULL
               END                                  AS segment_mtzn
              ,CASE 
                  WHEN UPPER(d.scnrrr_fdr_cat_nam) = 'NONE'            THEN 'SHORTRURAL'
                  WHEN d.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                  ELSE                                                            UPPER(d.scnrrr_fdr_cat_nam)
              END                                                            AS fdr_cat
              ,CASE
                  WHEN A.fire_risk_zone_cls  = 'X'          THEN  'Y'
                  WHEN A.fire_risk_zone_cls  = 'H'          THEN  'Y'
                  WHEN A.max_safety_zone_rtg = 'VERY_HIGH'  THEN  'Y'
                  WHEN A.max_safety_zone_rtg = 'HIGH'       THEN  'Y'
                  WHEN A.max_safety_zone_rtg = 'MEDIUM'     THEN  'Y'
                  ELSE                                            'N'
              END    AS high_risk_area
              ,dist_coast_km
         FROM  structools.st_bay_equipment A    INNER JOIN
               carrier_model  b
         ON 'C'||A.conductor_pick_id = b.pick_id
                                                                  LEFT OUTER JOIN
               structools.st_eqp_mtzn c
         ON A.segment_id = c.pick_id      
                                                                  LEFT OUTER JOIN
               structools.st_hv_feeder        d
         ON A.feeder_id = d.hv_fdr_id
         --WHERE A.pick_id = 'B990401'     --graz      
         )
         ,
         actioned_or_committed_bays_tmp AS        --pickid init
         (
--         SELECT pick_id
--               ,event
--               ,del_year
--         FROM structools.st_del_program_pickid
--         WHERE base_run_id BETWEEN v_parent_run_id_first AND v_parent_run_id_last
--         AND program_id = v_parent_run_program_id
--         AND event != 'REPAIR'
--         AND pick_id LIKE 'B%'          --added this
--         UNION ALL
         SELECT pick_id
               ,event
               ,commit_yr   AS del_year
         FROM structools.st_pickid_committed
         WHERE pick_id LIKE 'B%'
         )
         ,
         actioned_bays_tmp AS
         (
         SELECT pick_id
               ,MAX(RPAD(event,9)||del_year)                AS max_event_seq 
         --FROM structools.st_del_program_pickid               --pickid init
         FROM actioned_or_committed_bays_tmp
--         WHERE base_run_id BETWEEN v_parent_run_id_first AND v_parent_run_id_last --pickid init
--         AND program_id = v_parent_run_program_id
--         AND event != 'REPAIR'
--         AND pick_id LIKE 'B%'          --added this
         GROUP BY pick_id
         )
         ,           
         actioned_bays AS
         (
         SELECT A.pick_id
               ,RTRIM(SUBSTR(A.max_event_seq,1,9))      AS event
               ,SUBSTR(A.max_event_seq,10,4)            AS del_yr
               ,CASE                                        --pickid init
                  WHEN b.event IS NULL THEN  'N'
                  ELSE                       'Y'
                END                                       AS scheduled
          FROM actioned_bays_tmp
                                    A  LEFT OUTER JOIN      --pickid init
              structools.st_pickid_committed b
         ON A.pick_id = b.pick_id                                    
         )               
--         ,
--         non_del_bays AS
--         (
--         SELECT DISTINCT pick_id
--               ,event_reason
--         FROM structools.st_bay_strategy_run
--         WHERE run_id = v_parent_run_id_last
--         AND event > ' '
--         AND calendar_year = v_year_0       /* last year of the parent run */
--         AND pick_id NOT IN (SELECT DISTINCT pick_id FROM actioned_bays)
--         )      
			,      
         suppressed_bays AS
         (
         SELECT pick_id
         			,'Y'				AS suppressed	
         FROM structools.st_pickid_suppressed_init
         WHERE equip_grp_id = 'BAY'
         UNION
         SELECT pick_id
         			  ,'P'				AS suppressed	
         FROM structools.st_pickid_suppressed
         WHERE equip_grp_id = 'BAY'
         AND scenario_id = v_suppress_pickids_prog_id
         )
         ,
         bays_all AS
         (
         SELECT A.pick_id
               ,A.instl_yr
               ,A.carrier_model_code
               ,A.bay_equip_cde 
               ,A.stc1
               ,A.stc2
               ,b.mtzn
               ,ROUND(b.span_len)                     AS span_len
               ,b.lvco_cnt
               ,b.hvco_cnt 
               ,b.hvsp_cnt
               ,b.area_type
               ,c.material_cde
               ,c.wire_diam
               ,c.material_cde_common
               ,c.cond_diam
               ,NULL                      AS defect_action
               ,A.segment_id
               ,A.segment_mtzn
               ,G.del_yr
               ,G.event                   
               --,h.pick_id                 AS non_del_pick_id
               ,NULL                      AS non_del_pick_id
               --,h.event_reason            AS non_del_event_reason
               ,NULL                      AS non_del_event_reason
               ,NULL                      AS fsrtg
               ,A.fdr_cat
               ,NVL(G.scheduled, 'N')     AS scheduled         --pickid init
--               ,CASE
--                  WHEN G.del_yr IS NOT NULL        THEN  NULL
--                  ELSE                                   A.condition_code
--               END                        AS condition_code
               ,NULL                         AS condition_code
               ,A.high_risk_area
               ,i.pick_id                    AS bay_forced_pick_id
               ,A.dist_coast_km
               ,j.pick_id							AS pickid_suppressed
               ,j.suppressed
               ,NULL									AS defect_action2
         FROM bays                           A  INNER JOIN
              structools.st_span                b
         ON   A.stc1 = b.stc1
         AND  A.stc2 = b.stc2
                                                LEFT OUTER JOIN
              structools.st_carr_mdl_material   c
         ON   A.carrier_model_code = TO_CHAR(c.carrier_mdl_cde)                                             
                                                LEFT OUTER JOIN
              actioned_bays                  G
         ON A.pick_id = G.pick_id                                                                                                      
--                                                LEFT OUTER JOIN
--              non_del_bays              h
--         ON A.pick_id = h.pick_id                                                                                                      
                                                LEFT OUTER JOIN
              structools.st_bay_enforce_replace   i
         ON A.pick_id = i.pick_id       
                                                LEFT OUTER JOIN
              suppressed_bays									j
         ON A.pick_id = j.pick_id       
         )
         SELECT * FROM bays_all
         ORDER BY stc1, stc2, bay_equip_cde, pick_id
      ;

      CURSOR bay_64_csr IS

         WITH carrier_model AS         --carrier model field val
         (
         SELECT DISTINCT NVL(A.pick_id, b.conductor_pick_id) AS cond_pick_id
               ,CASE
                  WHEN b.conductor_pick_id IS NOT NULL   THEN  TO_CHAR(b.carrier_model_code)
                  ELSE                                         TO_CHAR(A.carrier_model_code)
               END                     AS carrier_model_code
         FROM structoolsw.carrier_detail            A     FULL OUTER JOIN
              structools.st_carrier_detail         b
         ON A.pick_id = b.conductor_pick_id
         )
         ,
         bays AS
         (         
         SELECT A.pick_id                 AS bay_pick_id
               ,A.stc1                    AS bay_stc1
               ,A.stc2                    AS bay_stc2
               ,A.instln_year             AS instl_yr
               ,A.bay_equip_cde 
               ,b.carrier_model_code
               ,A.segment_id
               ,CASE
                    WHEN A.segment_id IS NOT NULL AND c.mtzn    IS NULL              THEN  'UNKNOWN'
                    WHEN A.segment_id IS NOT NULL AND c.mtzn    IS NOT NULL       THEN  c.mtzn
                    ELSE                                                                                                    NULL
               END                                  AS segment_mtzn
              ,CASE 
                  WHEN UPPER(d.scnrrr_fdr_cat_nam) = 'NONE'            THEN 'SHORTRURAL'
                  WHEN d.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                  ELSE                                                            UPPER(d.scnrrr_fdr_cat_nam)
              END                                                            AS fdr_cat
              ,CASE
                  WHEN A.fire_risk_zone_cls  = 'X'          THEN  'Y'
                  WHEN A.fire_risk_zone_cls  = 'H'          THEN  'Y'
                  WHEN A.fire_risk_zone_cls  = 'M'          THEN  'Y'
                  WHEN A.max_safety_zone_rtg = 'VERY_HIGH'  THEN  'Y'
                  WHEN A.max_safety_zone_rtg = 'HIGH'       THEN  'Y'
                  WHEN A.max_safety_zone_rtg = 'MEDIUM'     THEN  'Y'
                  WHEN A.max_safety_zone_rtg = 'LOW'        THEN  'Y'
                  ELSE                                            'N'
              END    AS high_risk_area
              ,dist_coast_km
         FROM  structools.st_bay_equipment A    INNER JOIN
               carrier_model  b
         ON 'C'||A.conductor_pick_id = b.cond_pick_id
                                                                  LEFT OUTER JOIN
               structools.st_eqp_mtzn c
         ON A.segment_id = c.pick_id      
                                                                  LEFT OUTER JOIN
               structools.st_hv_feeder        d
         ON A.feeder_id = d.hv_fdr_id
         --WHERE A.pick_id = 'B990401'     --graz      
         )
         ,
         actioned_or_committed_bays_tmp AS        --pickid init
         (
--         SELECT pick_id
--               ,event
--               ,del_year
--         FROM structools.st_del_program_pickid
--         WHERE base_run_id BETWEEN v_parent_run_id_first AND v_parent_run_id_last
--         AND program_id = v_parent_run_program_id
--         AND event != 'REPAIR'
--         AND pick_id LIKE 'B%'          --added this
--         UNION ALL
         SELECT pick_id
               ,event
               ,commit_yr   AS del_year
         FROM structools.st_pickid_committed
         WHERE pick_id LIKE 'B%'
         )
         ,
         actioned_bays_tmp AS
         (
         SELECT pick_id
               ,MAX(RPAD(event,9)||del_year)                AS max_event_seq 
         --FROM structools.st_del_program_pickid               --pickid init
         FROM actioned_or_committed_bays_tmp
--         WHERE base_run_id BETWEEN v_parent_run_id_first AND v_parent_run_id_last --pickid init
--         AND program_id = v_parent_run_program_id
--         AND event != 'REPAIR'
--         AND pick_id LIKE 'B%'          --added this
         GROUP BY pick_id
         )
         ,           
         actioned_bays AS
         (
         SELECT A.pick_id
               ,RTRIM(SUBSTR(A.max_event_seq,1,9))      AS event
               ,SUBSTR(A.max_event_seq,10,4)            AS del_yr
               ,CASE                                        --pickid init
                  WHEN b.event IS NULL THEN  'N'
                  ELSE                       'Y'
                END                                       AS scheduled
          FROM actioned_bays_tmp
                                    A  LEFT OUTER JOIN      --pickid init
              structools.st_pickid_committed b
         ON A.pick_id = b.pick_id                                    
         )               
			,      
         suppressed_bays AS
         (
         SELECT pick_id
                    ,'Y'				AS suppressed
         FROM structools.st_pickid_suppressed_init
         WHERE equip_grp_id = 'BAY'
         UNION
         SELECT pick_id
                    ,'P'				AS suppressed
         FROM structools.st_pickid_suppressed
         WHERE equip_grp_id = 'BAY'
         AND scenario_id = v_suppress_pickids_prog_id
         )
         ,
         def_bays1 AS
         (
         SELECT DISTINCT 
                c.pick_id
         FROM   structools.st_bay_defect        A  INNER JOIN
                structools.st_bay_equipment     b
         ON A.pick_id = b.pick_id                
                                                   INNER JOIN     --expand the defective bays to all bays on the same conductor
                structools.st_bay_equipment     c
         ON b.conductor_pick_id = c.conductor_pick_id
         WHERE  b.instln_year < EXTRACT (YEAR FROM A.defect_crtd_date)
         AND (A.defect_cde IN ('COUS','EXTR') AND A.sev_cde IN ('1','2','3')
         OR A.defect_cde = 'EXCJ')
         )
         ,
         def_bays2 AS
         (
         SELECT DISTINCT 
                c.pick_id
         FROM   structools.st_bay_defect        A  INNER JOIN
                structools.st_bay_equipment     b
         ON A.pick_id = b.pick_id                
                                                   INNER JOIN     --expand the defective bays to all bays on the same conductor
                structools.st_bay_equipment     c
         ON b.conductor_pick_id = c.conductor_pick_id
         WHERE  b.installation_date < A.defect_crtd_date
         AND A.defect_desc IN ('Broken Strands 1 -3 COBS'
         											,'Conductor Abraded 1-3 COAB'
                                          ,'Conductor Annealed 1-3 COAN'
                                          ,'Conductor Burnt 1-3 COBU' 
                                          ,'Conductor Excessive Joins 5 EXCJ'
                                          ,'DBAYHV-COBD Conductor Damged 1-4 COBD' 
														,'DBAYHV-COBD Conductor Damged 1,3 COBD'
                                          ,'DBAYHV-COUS Conductor Unserviceable 2 CO'
                                          ,'DBAYHV-EXCJ Excessive Joins, U/S 2 EXCJ'
                                          ,'DBAYHV-UNSE Unserviceable 1-3 UNSE' 
														,'DBAYLV-COBD Conductor Damged 1-4 COLV'
                                          ,'DBAYLV-COBD Conductor Damged 1,3 COLV' 
														,'DBAYLV-COUS Conductor Unserviceable 2 CO'
                                          ,'DBAYLV-EXCJ Excessive Joins, U/S 2 EXCJ'
                                          ,'Continuous Corrosion 1,2,4 COCC'
                                          ,'Spot Corrosion 1,2,4 COSC'
                                          ,'DBAYHV-EXTR Extreme Rust, U/S 3 EXTR' 
														,'DBAYLV-EXTR Extreme Rust, U/S 3 EXTR'
                                          ,'Don''t Use This Defect EXTR'
         											)
			AND A.sev_cde IN ('1','2')                                          
         )
         ,
         bays_all AS
         (
         SELECT A.bay_pick_id
               ,A.instl_yr
               ,A.carrier_model_code
               ,A.bay_equip_cde 
               ,A.bay_stc1
               ,A.bay_stc2
               ,b.mtzn
               ,ROUND(b.span_len)                     AS span_len
               ,b.lvco_cnt
               ,b.hvco_cnt 
               ,b.hvsp_cnt
               ,b.area_type
               ,c.material_cde
               ,c.wire_diam
               ,c.material_cde_common
               ,c.cond_diam
               ,CASE
                  WHEN G.del_yr IS NOT NULL        THEN  NULL
                  WHEN j.pick_id IS NULL           THEN  NULL
                  ELSE                                   'REPLACE'
               END                        AS defect_action
               ,A.segment_id
               ,A.segment_mtzn
               ,G.del_yr
               ,G.event                   AS actioned_event
               ,NULL                      AS non_del_pick_id
               ,NULL                      AS non_del_event_reason
               ,NULL                      AS fsrtg
               ,A.fdr_cat
               ,NVL(G.scheduled, 'N')     AS scheduled         --pickid init
               ,NULL                        AS condition_code
               ,A.high_risk_area
               ,i.pick_id                    AS bay_forced_pick_id
               ,A.dist_coast_km
               ,K.pick_id							AS pickid_suppressed
               ,K.suppressed
               ,CASE
                  WHEN G.del_yr IS NOT NULL        THEN  NULL
                  WHEN l.pick_id IS NULL           THEN  NULL
                  ELSE                                   'REPLACE_DC'  -- DC for Damage and/or corrosion
               END                        AS defect_action2
         FROM bays                           A  INNER JOIN
              structools.st_span                b
         ON   A.bay_stc1 = b.stc1
         AND  A.bay_stc2 = b.stc2
                                                LEFT OUTER JOIN
              structools.st_carr_mdl_material   c
         ON   A.carrier_model_code = TO_CHAR(c.carrier_mdl_cde)                                             
                                                LEFT OUTER JOIN
              actioned_bays                  G
         ON A.bay_pick_id = G.pick_id                                                                                                      
                                                LEFT OUTER JOIN
              structools.st_bay_enforce_replace   i
         ON A.bay_pick_id = i.pick_id                                                                                                      
                                                LEFT OUTER JOIN
              def_bays1                    j
         ON A.bay_pick_id = j.pick_id     
                                                LEFT OUTER JOIN
              suppressed_bays                    K
         ON A.bay_pick_id = K.pick_id     
                                                LEFT OUTER JOIN
              def_bays2                    l
         ON A.bay_pick_id = l.pick_id     
         )
         SELECT * FROM bays_all
         ORDER BY bay_stc1, bay_stc2, bay_equip_cde, bay_pick_id
      ;
         v_bay_all_rec           bay_20_34_45_56_57_csr%ROWTYPE;
            
         TYPE span_tab_typ       IS TABLE OF bay_20_34_45_56_57_csr%ROWTYPE;
         v_span_tab              span_tab_typ := span_tab_typ();

         TYPE span_repl_yr_rsn_tab_typ  IS TABLE OF VARCHAR2(50);
         TYPE priority_tab_typ   IS TABLE OF INTEGER;
         TYPE risk_na_tab_typ  IS TABLE OF FLOAT(126);

         TYPE span_repl_yr_rsn_rec_typ  IS RECORD
         (bay_pick_id            VARCHAR2(10)
         ,repl_rsn               span_repl_yr_rsn_tab_typ
         ,priority               	priority_tab_typ
         ,risk_na						  risk_na_tab_typ
         );            
         TYPE span_repl_tab_typ  IS TABLE OF span_repl_yr_rsn_rec_typ;            
         v_span_repl_tab         span_repl_tab_typ := span_repl_tab_typ();
            
            
     
         PROCEDURE process_span(i_span_tab       IN span_tab_typ) IS
            
            v_span_replaced         BOOLEAN := FALSE;
            v_years                 PLS_INTEGER := v_last_year - v_year_0;
            v_span_repl_rec         span_repl_yr_rsn_rec_typ;
            v_exec_year             PLS_INTEGER;
            v_priority_tab          priority_tab_typ := priority_tab_typ();
            v_risk_na_tab        		risk_na_tab_typ := risk_na_tab_typ();
            v_repl_rsn_tab          span_repl_yr_rsn_tab_typ := span_repl_yr_rsn_tab_typ();
--            v_init_age              PLS_INTEGER;
            v_risk_reduction        FLOAT(126)    := 0;
            v_risk                  FLOAT(126)    := NULL;
            v_risk_npv              FLOAT(126)    := NULL;
            v_risk_noaction         FLOAT(126)    := NULL;
            v_risk_noaction_npv     FLOAT(126)    := NULL;
            v_risk_reduction_npv    FLOAT(126)    := 0;
            v_risk_npv_sif            FLOAT(126)    := NULL;
            v_risk_noaction_npv_sif   FLOAT(126)    := NULL;
            v_risk_reduction_npv_sif  FLOAT(126)    := 0;
            v_age                      PLS_INTEGER;
            v_capex                    PLS_INTEGER;
            v_labour_hrs             PLS_INTEGER;
            v_event                    VARCHAR2(10);
            v_key                      VARCHAR2(15);
            v_cond_key                 VARCHAR2(8);
            v_cond_key1                VARCHAR2(8);
            v_cond_key2                VARCHAR2(8);
            v_cons_segment_id          VARCHAR2(30);
            v_cons_segment_mtzn        VARCHAR2(30);
            --v_scheduled                CHAR(1);           --pickid init
            v_span_repl_year           PLS_INTEGER;
            v_variation                CHAR(1);
            v_event_reason             VARCHAR2(50);
            v_instln_yr                PLS_INTEGER;
            v_rr_sif_fdr_cat_adj       FLOAT(126);
            v_risk_pick_id             VARCHAR2(10);
            v_bay_pfail                FLOAT(126);
            v_bay_repl_top_up           BOOLEAN;
            v_safety_frac					float;
            v_safety_na						float;
            v_pickid_suppressed		CHAR(1);   
               
         BEGIN

            v_span := TRIM(i_span_tab(1).stc1)||TRIM(i_span_tab(1).stc2);
               
            v_cons_segment_id := i_span_tab(1).segment_id;  -- HV segment if there is any in the span, otherwise the first LV segment
                                                            -- by the virtue of data ordered by bay_equip_typ within a span.             
            v_cons_segment_mtzn := i_span_tab(1).segment_mtzn;

            v_egi_vc2 := 'BAY';
            v_span_repl_tab.DELETE;
            v_span_repl_tab.EXTEND(i_span_tab.COUNT);

            FOR i IN 1 .. i_span_tab.COUNT LOOP
               v_repl_rsn_tab.DELETE;
               v_repl_rsn_tab.EXTEND(v_years);
               v_priority_tab.DELETE;
               v_priority_tab.EXTEND(v_years);
               v_risk_na_tab.DELETE;
               v_risk_na_tab.EXTEND(v_years);
               v_span_repl_tab(i).repl_rsn := span_repl_yr_rsn_tab_typ();
               v_span_repl_tab(i).priority := priority_tab_typ();
               v_span_repl_tab(i).risk_na := risk_na_tab_typ();
               v_span_repl_tab(i).repl_rsn.DELETE;
               v_span_repl_tab(i).repl_rsn.EXTEND(v_years);
               v_span_repl_tab(i).priority.DELETE;
               v_span_repl_tab(i).priority.EXTEND(v_years);
               v_span_repl_tab(i).risk_na.DELETE;
               v_span_repl_tab(i).risk_na.EXTEND(v_years);
               v_bay_repl_top_up := FALSE;
                  
               IF i_span_tab(i).del_yr IS NOT NULL THEN
                  v_instln_yr := i_span_tab(i).del_yr;
                  v_variation := 'P';
               ELSE
                  v_instln_yr := i_span_tab(i).instl_yr;
                  v_variation := NULL;
               END IF;
               /* if the bay is one of the committed bays, do not create an event */
               IF i_span_tab(i).scheduled = 'Y' THEN
                  NULL;
               ELSE
                  FOR j IN 1 .. v_years LOOP
                     v_exec_year := j + v_year_0;
                     v_cond_key  := NULL;
                     v_cond_key1 := NULL;
                     v_cond_key2 := NULL;
                     v_age := v_exec_year - v_instln_yr;
                     get_bay_risk_for_year(i_span_tab(i).pick_id, 'NOACTION', v_age, v_variation
                                       ,v_year_0 + j, i_span_tab(i).del_yr, i_span_tab(i).fdr_cat, v_risk_noaction, v_risk_npv, v_risk_noaction_npv
                                       ,v_risk_npv_sif, v_risk_noaction_npv_sif);
                     v_risk_na_tab(j) := v_risk_noaction;
                     IF i_span_tab(i).non_del_pick_id IS NOT NULL THEN
                        v_repl_rsn_tab(j) := i_span_tab(i).non_del_event_reason;
                     ELSE
                        IF v_strategy_bay = 20 THEN
                           IF i_span_tab(i).material_cde_common = 'COPPER' 
                           AND i_span_tab(i).cond_diam <= v_thin_cu_diam
                           AND (v_exec_year - v_instln_yr) >= v_thin_cu_age THEN
                              v_cond_key := v_egi_vc2||'-'||'001';
                           ELSIF i_span_tab(i).material_cde_common = 'STEEL' 
                           AND (v_exec_year - v_instln_yr) >= v_steel_age THEN
                              v_cond_key := v_egi_vc2||'-'||'002';
                           ELSIF i_span_tab(i).material_cde_common = 'AAC'
                           AND (v_exec_year - v_instln_yr) >= v_aac_age THEN
                              v_cond_key := v_egi_vc2||'-'||'003';
                           ELSIF i_span_tab(i).material_cde_common = 'COPPER'
                           AND i_span_tab(i).cond_diam > v_thick_cu_diam
                           AND (v_exec_year - v_instln_yr) >= v_thick_cu_age THEN
                              v_cond_key := v_egi_vc2||'-'||'004';
                           ELSIF i_span_tab(i).material_cde_common = 'ACSR' THEN
                              IF (v_exec_year - v_instln_yr) >= v_acsr_age THEN
                                 v_cond_key := v_egi_vc2||'-'||'005';
                              END IF;
                           ELSIF i_span_tab(i).material_cde_common = 'AAAC'
                           AND (v_exec_year - v_instln_yr) >= v_aaac_age THEN
                              v_cond_key := v_egi_vc2||'-'||'006';
                           ELSIF i_span_tab(i).material_cde_common = 'HENDRIX'  
                           AND (v_exec_year - v_instln_yr) >= v_hendrix_age THEN
                              v_cond_key := v_egi_vc2||'-'||'007';
                           ELSIF i_span_tab(i).material_cde_common = 'LVABC'
                           AND (v_exec_year - v_instln_yr) >= v_lvabc_age THEN
                              v_cond_key := v_egi_vc2||'-'||'008';
                           ELSIF i_span_tab(i).material_cde_common = 'HVABC' THEN  
                              v_cond_key := v_egi_vc2||'-'||'009';
                           ELSIF i_span_tab(i).defect_action = 'REPLACE' THEN
                              v_cond_key := v_egi_vc2||'-'||'010';
                           END IF;
                           IF v_cond_key IS NOT NULL THEN
                              v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                           END IF;
                        ELSIF v_strategy_bay = 34 THEN 
                           IF (v_exec_year - v_instln_yr) >= v_bay_age THEN
                              v_cond_key := v_egi_vc2||'-'||'001';
                           END IF;
                           IF v_cond_key IS NOT NULL THEN
                              v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                           END IF;
                        ELSIF v_strategy_bay = 45 THEN 
                           IF (v_exec_year - v_instln_yr) >= v_bay_age THEN
                              IF v_fsrtg_tab.EXISTS('BAY '||i_span_tab(i).fsrtg) THEN
                                 v_cond_key := v_egi_vc2||'-'||'001';
                              END IF; 
                           END IF;
                           IF v_cond_key IS NOT NULL THEN
                              v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                           END IF;
                        ELSIF v_strategy_bay = 56 THEN
                           IF i_span_tab(i).material_cde_common = 'COPPER' 
                           AND i_span_tab(i).cond_diam <= v_thin_cu_diam
                           AND (v_exec_year - v_instln_yr) >= v_thin_cu_age THEN
                              v_cond_key := v_egi_vc2||'-'||'001';
                           ELSIF i_span_tab(i).material_cde_common = 'STEEL' 
                           AND (v_exec_year - v_instln_yr) >= v_steel_age THEN
                              v_cond_key := v_egi_vc2||'-'||'002';
                           ELSIF i_span_tab(i).material_cde_common = 'AAC'
                           AND (v_exec_year - v_instln_yr) >= v_aac_age THEN
                              v_cond_key := v_egi_vc2||'-'||'003';
--                           ELSIF i_span_tab(i).material_cde_common = 'ACSR' THEN
--                              IF (v_exec_year - v_instln_yr) >= v_acsr_age THEN
--                                 v_cond_key := v_egi_vc2||'-'||'004';
--                              END IF;
                           ELSIF i_span_tab(i).material_cde_common = 'HVABC' THEN  
                              v_cond_key := v_egi_vc2||'-'||'005';
                           ELSIF i_span_tab(i).defect_action = 'REPLACE' THEN
                              v_cond_key := v_egi_vc2||'-'||'006';
                           END IF;
                           IF v_cond_key IS NOT NULL THEN
                              v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                           END IF;
                        ELSIF v_strategy_bay = 57 THEN         --strat 57
                           IF i_span_tab(i).material_cde_common = 'COPPER' 
                           AND i_span_tab(i).cond_diam <= v_thin_cu_diam
                           AND (v_exec_year - v_instln_yr) >= v_thin_cu_age THEN
                              v_cond_key1 := v_egi_vc2||'-'||'002';
                           ELSIF i_span_tab(i).material_cde_common = 'STEEL' 
                           AND (v_exec_year - v_instln_yr) >= v_steel_age THEN
                              v_cond_key1 := v_egi_vc2||'-'||'003';
                           ELSIF i_span_tab(i).material_cde_common = 'AAC'
                           AND (v_exec_year - v_instln_yr) >= v_aac_age THEN
                              v_cond_key1 := v_egi_vc2||'-'||'004';
--                           ELSIF i_span_tab(i).material_cde_common = 'ACSR' THEN
--                              IF (v_exec_year - v_instln_yr) >= v_acsr_age THEN
--                                 v_cond_key1 := v_egi_vc2||'-'||'005';
--                              END IF;
                           ELSIF i_span_tab(i).del_yr IS NULL AND i_span_tab(i).material_cde_common = 'HVABC' THEN  
                                 v_cond_key1 := v_egi_vc2||'-'||'006';
                           ELSIF i_span_tab(i).defect_action = 'REPLACE' THEN
                              v_cond_key1 := v_egi_vc2||'-'||'007';
                           END IF;
                           IF i_span_tab(i).condition_code IS NOT NULL THEN
                              IF j = 1 AND i_span_tab(i).condition_code >= v_cond_score1
                              OR j > 1 AND i_span_tab(i).condition_code >= v_cond_score2 THEN
                                 v_cond_key1 := NULL;
                              ELSE                                 
                                 v_cond_key2 := v_egi_vc2||'-'||'001';
                              END IF;
                           END IF;
                           IF v_cond_key1 IS NOT NULL AND v_cond_key2 IS NOT NULL THEN
                              v_repl_rsn_tab(j) := TRIM(v_cond_tab(v_cond_key1))||' AND '||TRIM(v_cond_tab(v_cond_key2));
                           ELSIF v_cond_key1 IS NOT NULL AND v_cond_key2 IS NULL THEN
                              v_repl_rsn_tab(j) := v_cond_tab(v_cond_key1);
                           ELSIF v_cond_key1 IS NULL AND v_cond_key2 IS NOT NULL THEN
                              v_repl_rsn_tab(j) := v_cond_tab(v_cond_key2);
                           END IF;
                           v_priority_tab(j) := NULL;
                           IF v_repl_rsn_tab(j) IS NOT NULL THEN --ie bay is selected for replacement
                              IF i_span_tab(i).material_cde_common = 'COPPER' 
                              AND i_span_tab(i).cond_diam <= v_thin_cu_diam
                              AND (v_exec_year - v_instln_yr) >= 40
                              OR
                              i_span_tab(i).condition_code <= 40 THEN
                                 v_priority_tab(j) := 1;
                              END IF;
                           END IF;  
                        ELSIF v_strategy_bay = 63 THEN
                           IF i_span_tab(i).bay_forced_pick_id IS NOT NULL AND i_span_tab(i).del_yr IS NULL THEN
                              v_cond_key := v_egi_vc2||'-'||'001'; 
                           ELSIF i_span_tab(i).material_cde = 'SCGZ'
                           AND i_span_tab(i).cond_diam < v_diam1
                           AND TO_NUMBER(i_span_tab(i).dist_coast_km) <= v_dist_coast_km1 
                           AND i_span_tab(i).del_yr IS NULL THEN
                              v_cond_key := v_egi_vc2||'-'||'002';
                           ELSIF i_span_tab(i).material_cde = 'SCGZ'
                           AND i_span_tab(i).cond_diam >= v_diam2 
                           AND TO_NUMBER(i_span_tab(i).dist_coast_km) <= v_dist_coast_km2
                           AND (v_exec_year - v_instln_yr) > v_age1                       
                           AND i_span_tab(i).high_risk_area = 'Y'                         THEN
                              v_cond_key := v_egi_vc2||'-'||'003';
                           ELSIF i_span_tab(i).material_cde_common = 'COPPER'
                           AND i_span_tab(i).cond_diam <= v_diam3              
                           AND i_span_tab(i).del_yr IS NULL                               THEN
                              v_cond_key := v_egi_vc2||'-'||'004';
                           ELSIF i_span_tab(i).material_cde_common = 'COPPER'
                           AND i_span_tab(i).cond_diam > v_diam4 AND i_span_tab(i).cond_diam < v_diam5 
                           AND TO_NUMBER(i_span_tab(i).dist_coast_km) <= v_dist_coast_km3 
                           AND i_span_tab(i).del_yr IS NULL                               THEN
                              v_cond_key := v_egi_vc2||'-'||'005';
                           ELSIF i_span_tab(i).material_cde_common = 'COPPER'
                           AND i_span_tab(i).cond_diam > v_diam6 AND i_span_tab(i).cond_diam < v_diam7 
                           AND i_span_tab(i).high_risk_area = 'Y'                         
                           AND i_span_tab(i).del_yr IS NULL                               THEN
                              v_cond_key := v_egi_vc2||'-'||'006';
                           ELSIF i_span_tab(i).material_cde_common = 'COPPER'
                           AND i_span_tab(i).cond_diam > v_diam8 AND i_span_tab(i).cond_diam < v_diam9 
                           AND TO_NUMBER(i_span_tab(i).dist_coast_km) <= v_dist_coast_km4
                           AND (v_exec_year - v_instln_yr) > v_age2 
                           AND i_span_tab(i).high_risk_area = 'Y'                         THEN
                              v_cond_key := v_egi_vc2||'-'||'007';
                           END IF;
                           IF v_cond_key IS NOT NULL THEN
                              v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                           END IF;
                        ELSIF v_strategy_bay = 64 OR v_strategy_bay = 66 THEN
                           IF i_span_tab(i).bay_forced_pick_id IS NOT NULL AND i_span_tab(i).del_yr IS NULL THEN
                              v_cond_key := v_egi_vc2||'-'||'001'; 
                           ELSIF i_span_tab(i).material_cde = 'SCGZ'
                           AND i_span_tab(i).cond_diam < v_diam1
                           AND TO_NUMBER(i_span_tab(i).dist_coast_km) <= v_dist_coast_km1 
                           AND i_span_tab(i).del_yr IS NULL THEN
                              v_cond_key := v_egi_vc2||'-'||'002';
                           ELSIF i_span_tab(i).material_cde = 'SCGZ'
                           AND i_span_tab(i).cond_diam >= v_diam2 
                           AND TO_NUMBER(i_span_tab(i).dist_coast_km) <= v_dist_coast_km2
                           AND (v_exec_year - v_instln_yr) > v_age1                       
                           AND i_span_tab(i).high_risk_area = 'Y'                         THEN
                              v_cond_key := v_egi_vc2||'-'||'003';
                           ELSIF i_span_tab(i).material_cde_common = 'COPPER'
                           AND i_span_tab(i).cond_diam <= v_diam3              
                           AND i_span_tab(i).del_yr IS NULL                               THEN
                              v_cond_key := v_egi_vc2||'-'||'004';
                           ELSIF i_span_tab(i).material_cde_common = 'COPPER'
                           AND i_span_tab(i).cond_diam > v_diam4 AND i_span_tab(i).cond_diam < v_diam5 
                           AND TO_NUMBER(i_span_tab(i).dist_coast_km) <= v_dist_coast_km3 
                           AND i_span_tab(i).del_yr IS NULL                               THEN
                              v_cond_key := v_egi_vc2||'-'||'005';
                           ELSIF i_span_tab(i).material_cde_common = 'COPPER'
                           AND i_span_tab(i).cond_diam > v_diam6 AND i_span_tab(i).cond_diam < v_diam7 
                           AND i_span_tab(i).high_risk_area = 'Y'                         
                           AND i_span_tab(i).del_yr IS NULL                               THEN
                              v_cond_key := v_egi_vc2||'-'||'006';
                           ELSIF i_span_tab(i).material_cde_common = 'COPPER'
                           AND i_span_tab(i).cond_diam > v_diam8 AND i_span_tab(i).cond_diam < v_diam9 
                           AND TO_NUMBER(i_span_tab(i).dist_coast_km) <= v_dist_coast_km4
                           AND (v_exec_year - v_instln_yr) >= v_age2 
                           AND i_span_tab(i).high_risk_area = 'Y'                         THEN
                              v_cond_key := v_egi_vc2||'-'||'007';
                           ELSIF i_span_tab(i).defect_action = 'REPLACE' THEN
                              v_cond_key := v_egi_vc2||'-'||'008';
                           END IF;
                           IF v_cond_key IS NOT NULL THEN
                              v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                           END IF;
                        ELSIF v_strategy_bay = 65 OR v_strategy_bay = 69 OR v_strategy_bay = 70  OR v_strategy_bay = 73  OR v_strategy_bay = 80 THEN
                           IF i_span_tab(i).bay_forced_pick_id IS NOT NULL AND i_span_tab(i).del_yr IS NULL THEN
                              v_cond_key := v_egi_vc2||'-'||'001'; 
                           ELSIF i_span_tab(i).material_cde = 'SCGZ'
                           AND i_span_tab(i).cond_diam < v_diam1
                           AND TO_NUMBER(i_span_tab(i).dist_coast_km) <= v_dist_coast_km1 
                           AND i_span_tab(i).del_yr IS NULL THEN
                              v_cond_key := v_egi_vc2||'-'||'002';
                           ELSIF i_span_tab(i).material_cde = 'SCGZ'
                           AND i_span_tab(i).cond_diam >= v_diam2 
                           AND TO_NUMBER(i_span_tab(i).dist_coast_km) <= v_dist_coast_km2
                           AND (v_exec_year - v_instln_yr) > v_age1                       THEN                       
--                           AND i_span_tab(i).high_risk_area = 'Y'                         THEN
                              v_cond_key := v_egi_vc2||'-'||'003';
                           ELSIF i_span_tab(i).material_cde_common = 'COPPER'
                           AND i_span_tab(i).cond_diam <= v_diam3              
                           AND i_span_tab(i).del_yr IS NULL                               THEN
                              v_cond_key := v_egi_vc2||'-'||'004';
                           ELSIF i_span_tab(i).material_cde_common = 'COPPER'
                           AND i_span_tab(i).cond_diam > v_diam4 AND i_span_tab(i).cond_diam < v_diam5 
                           AND i_span_tab(i).del_yr IS NULL                               THEN
                              v_cond_key := v_egi_vc2||'-'||'005';
                           ELSIF i_span_tab(i).material_cde_common = 'COPPER'
                           AND i_span_tab(i).cond_diam > v_diam6 AND i_span_tab(i).cond_diam < v_diam7 
                           AND TO_NUMBER(i_span_tab(i).dist_coast_km) <= v_dist_coast_km3
                           AND (v_exec_year - v_instln_yr) >= v_age2 THEN
                              v_cond_key := v_egi_vc2||'-'||'006';
                           ELSIF i_span_tab(i).defect_action = 'REPLACE' THEN
                              v_cond_key := v_egi_vc2||'-'||'007';
                           END IF;
                           IF v_cond_key IS NOT NULL THEN
                              v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                           END IF;
                           IF v_strategy_bay = 69 AND v_cond_key IS NULL THEN
                              IF v_bay_repl_top_up THEN
                                 v_cond_key := v_egi_vc2||'-'||'008';
                                 v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                              ELSE
                                 IF j >= v_chng_yr AND (v_exec_year - v_instln_yr) >= v_age3 THEN
                                    v_risk_pick_id := SUBSTR(i_span_tab(i).pick_id,2,10);
                                    BEGIN
                                       SELECT
                                          CASE j
                                             WHEN 1   THEN  fail_prob_y1
                                             WHEN 2   THEN  GREATEST(fail_prob_y1, fail_prob_y2)
                                             WHEN 3   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3)
                                             WHEN 4   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4)
                                             WHEN 5   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5)
                                             WHEN 6   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6)
                                             WHEN 7   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 
                                                                     fail_prob_y7)
                                             WHEN 8   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 
                                                                     fail_prob_y7, fail_prob_y8)
                                             WHEN 9   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9)
                                             WHEN 10  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10)
                                             WHEN 11  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11)
                                             WHEN 12  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11,fail_prob_y12)
                                             WHEN 13  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11,fail_prob_y12, fail_prob_y13)
                                             WHEN 14  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14)
                                             WHEN 15  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15)
                                             WHEN 16  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16)
                                             WHEN 17  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16, fail_prob_y17)
                                             WHEN 18  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16, fail_prob_y17, fail_prob_y18)
                                             WHEN 19  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16, fail_prob_y17, fail_prob_y18, fail_prob_y19)
                                             WHEN 20  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16, fail_prob_y17, fail_prob_y18, fail_prob_y19, fail_prob_y20)
                                          END                  AS bay_pfail  
                                       INTO v_bay_pfail 
                                       FROM structools.st_risk_cond_noaction_fail
                                       WHERE pick_id = v_risk_pick_id;
                                       IF v_bay_pfail > v_pfail THEN 
                                          v_cond_key := v_egi_vc2||'-'||'008';
                                          v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                                          v_bay_repl_top_up := TRUE;  -- flag the bay as replaced, to avoid the db call for subsequent years.
                                       END IF;
                                    EXCEPTION
                                       WHEN NO_DATA_FOUND THEN
                                          NULL;
                                    END;
                                 END IF;
                              END IF;
                          ELSIF v_strategy_bay = 70 AND v_cond_key IS NULL THEN
                              IF v_bay_repl_top_up THEN
                                 IF i_span_tab(i).material_cde = 'SCGZ' THEN
                                    v_cond_key := v_egi_vc2||'-'||'008';
                                    v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                                 ELSE
                                    v_cond_key := v_egi_vc2||'-'||'009';
                                    v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                                 END IF;
                              ELSE
                                 IF j >= v_chng_yr THEN
                                    v_risk_pick_id := SUBSTR(i_span_tab(i).pick_id,2,10);
                                    BEGIN
                                       SELECT
                                          CASE j
                                             WHEN 1   THEN  fail_prob_y1
                                             WHEN 2   THEN  GREATEST(fail_prob_y1, fail_prob_y2)
                                             WHEN 3   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3)
                                             WHEN 4   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4)
                                             WHEN 5   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5)
                                             WHEN 6   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6)
                                             WHEN 7   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 
                                                                     fail_prob_y7)
                                             WHEN 8   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 
                                                                     fail_prob_y7, fail_prob_y8)
                                             WHEN 9   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9)
                                             WHEN 10  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10)
                                             WHEN 11  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11)
                                             WHEN 12  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11,fail_prob_y12)
                                             WHEN 13  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11,fail_prob_y12, fail_prob_y13)
                                             WHEN 14  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14)
                                             WHEN 15  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15)
                                             WHEN 16  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16)
                                             WHEN 17  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16, fail_prob_y17)
                                             WHEN 18  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16, fail_prob_y17, fail_prob_y18)
                                             WHEN 19  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16, fail_prob_y17, fail_prob_y18, fail_prob_y19)
                                             WHEN 20  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16, fail_prob_y17, fail_prob_y18, fail_prob_y19, fail_prob_y20)
                                          END                  AS bay_pfail  
                                       INTO v_bay_pfail 
                                       FROM structools.st_risk_cond_noaction_fail
                                       WHERE pick_id = v_risk_pick_id;
                                       IF i_span_tab(i).material_cde = 'SCGZ' AND v_bay_pfail > v_pfail THEN 
                                          v_cond_key := v_egi_vc2||'-'||'008';
                                          v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                                          v_bay_repl_top_up := TRUE;  -- flag the bay as replaced, to avoid the db call for subsequent years.
                                       END IF;
                                       IF i_span_tab(i).material_cde_common = 'COPPER' THEN 
                                          v_cond_key := v_egi_vc2||'-'||'009';
                                          v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                                          v_bay_repl_top_up := TRUE;  -- flag the bay as replaced, to avoid the db call for subsequent years.
                                       END IF;
                                    EXCEPTION
                                       WHEN NO_DATA_FOUND THEN
                                          NULL;
                                    END;
                                 END IF;
                              END IF;
                           ELSIF v_strategy_bay = 73 AND v_cond_key IS NULL THEN
                              IF v_bay_repl_top_up THEN
                                 v_cond_key := v_egi_vc2||'-'||'008';
                                 v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                              ELSE
                                 IF v_exec_year >= v_year_topup
                                 AND (v_exec_year - v_instln_yr) > v_age_topup THEN
                                    BEGIN
                                       SELECT fire_frac + eshock_frac
                                       INTO v_safety_frac
                                       FROM structools.st_risk_fractions
                                       WHERE pick_id = i_span_tab(i).pick_id AND model_name = 'BAY';
                                    EXCEPTION
                                       WHEN no_data_found THEN
                                          v_safety_frac := NULL;
                                    END;
                                    IF v_safety_frac IS NOT NULL THEN
                                       v_safety_na := v_risk_na_tab(j) * v_safety_frac;
                                       IF v_safety_na > v_safety_na_topup THEN
                                          v_cond_key := v_egi_vc2||'-'||'008';
                                          v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                                          v_bay_repl_top_up := TRUE;  -- flag the bay as replaced, to avoid the db call for subsequent years.
                                       END IF;
                                    END IF;
                                 END IF;
                              END IF;
                          ELSIF v_strategy_bay = 80 AND v_cond_key IS NULL THEN
                           	IF i_span_tab(i).defect_action2 = 'REPLACE_DC' THEN	--Damage/Corrosion defect
                              	v_cond_key := v_egi_vc2||'-'||'008';
                                 v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                              ELSIF v_bay_repl_top_up THEN
                                 IF i_span_tab(i).material_cde = 'SCGZ' THEN
                                    v_cond_key := v_egi_vc2||'-'||'009';
                                    v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                                 ELSE
                                    v_cond_key := v_egi_vc2||'-'||'010';
                                    v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                                 END IF;
                              ELSE
                                 IF j >= v_chng_yr THEN
                                    v_risk_pick_id := SUBSTR(i_span_tab(i).pick_id,2,10);
                                    BEGIN
                                       SELECT
                                          CASE j
                                             WHEN 1   THEN  fail_prob_y1
                                             WHEN 2   THEN  GREATEST(fail_prob_y1, fail_prob_y2)
                                             WHEN 3   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3)
                                             WHEN 4   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4)
                                             WHEN 5   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5)
                                             WHEN 6   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6)
                                             WHEN 7   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 
                                                                     fail_prob_y7)
                                             WHEN 8   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 
                                                                     fail_prob_y7, fail_prob_y8)
                                             WHEN 9   THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9)
                                             WHEN 10  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10)
                                             WHEN 11  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11)
                                             WHEN 12  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11,fail_prob_y12)
                                             WHEN 13  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11,fail_prob_y12, fail_prob_y13)
                                             WHEN 14  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14)
                                             WHEN 15  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15)
                                             WHEN 16  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16)
                                             WHEN 17  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16, fail_prob_y17)
                                             WHEN 18  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16, fail_prob_y17, fail_prob_y18)
                                             WHEN 19  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16, fail_prob_y17, fail_prob_y18, fail_prob_y19)
                                             WHEN 20  THEN  GREATEST(fail_prob_y1, fail_prob_y2, fail_prob_y3, fail_prob_y4, fail_prob_y5, fail_prob_y6, 		--20 yrs
                                                                     fail_prob_y7, fail_prob_y8, fail_prob_y9, fail_prob_y10, fail_prob_y11, fail_prob_y12, fail_prob_y13, 
                                                                     fail_prob_y14, fail_prob_y15, fail_prob_y16, fail_prob_y17, fail_prob_y18, fail_prob_y19, fail_prob_y20)
                                          END                  AS bay_pfail  
                                       INTO v_bay_pfail 
                                       FROM structools.st_risk_cond_noaction_fail
                                       WHERE pick_id = v_risk_pick_id;
                                       IF i_span_tab(i).material_cde = 'SCGZ' AND v_bay_pfail > v_pfail THEN 
                                          v_cond_key := v_egi_vc2||'-'||'009';
                                          v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                                          v_bay_repl_top_up := TRUE;  -- flag the bay as replaced, to avoid the db call for subsequent years.
                                       END IF;
                                       IF i_span_tab(i).material_cde_common = 'COPPER' THEN 
                                          v_cond_key := v_egi_vc2||'-'||'010';
                                          v_repl_rsn_tab(j) := v_cond_tab(v_cond_key);
                                          v_bay_repl_top_up := TRUE;  -- flag the bay as replaced, to avoid the db call for subsequent years.
                                       END IF;
                                    EXCEPTION
                                       WHEN NO_DATA_FOUND THEN
                                          NULL;
                                    END;
                                 END IF;
                              END IF;
                           END IF;
								END  IF;
                     END IF;
                  END LOOP;
                  v_span_repl_rec.bay_pick_id := i_span_tab(i).pick_id;
                  v_span_repl_rec.repl_rsn    := v_repl_rsn_tab;
                  v_span_repl_rec.priority    := v_priority_tab;
                  v_span_repl_rec.risk_na    := v_risk_na_tab;
                  v_span_repl_tab(i) := v_span_repl_rec;
               END IF;
            END LOOP;

            /* For each year, check if any bay is replaced for a reason and if so, also replace other bays for that year */
            v_span_repl_year := 0;
            FOR j IN 1 .. v_years LOOP       
               v_span_replaced := FALSE;     
               FOR i IN 1 .. v_span_repl_tab.COUNT LOOP
                  IF v_span_repl_tab(i).repl_rsn(j) IS NOT NULL THEN
                     v_span_replaced := TRUE;
                     IF v_span_repl_year = 0 THEN   
                        v_span_repl_year := j;
                     END IF;
                     EXIT;
                  END IF;
               END LOOP;
               IF v_span_replaced THEN
                  FOR i IN 1 .. v_span_repl_tab.COUNT LOOP
                     IF v_span_repl_tab(i).repl_rsn(j) IS NULL THEN
                        v_span_repl_tab(i).repl_rsn(j) := 'SPAN';
                     END IF;
                  END LOOP;
               END IF;
            END LOOP;
            

            /* Insert a row for each bay+year in the span */  
            FOR i IN 1 .. i_span_tab.COUNT LOOP
--               store_risk_bay(i_span_tab(i).pick_id);
               --v_init_age := v_year_0 - i_span_tab(i).instl_yr;
               IF i_span_tab(i).del_yr IS NOT NULL THEN
                  v_instln_yr := i_span_tab(i).del_yr;
               ELSE
                  v_instln_yr := i_span_tab(i).instl_yr;
               END IF;
               v_age := v_year_0 - v_instln_yr;
               v_capex := NULL;
               v_labour_hrs := NULL;
               v_event := NULL;
               v_event_reason := NULL;
               IF i_span_tab(i).pickid_suppressed IS NOT NULL THEN
               	v_pickid_suppressed :=i_span_tab(i).suppressed;
               ELSE
               	v_pickid_suppressed := 'N';
					END IF;                  
                 
               v_span_repl_rec := v_span_repl_tab(i);
               v_risk_na_tab :=  v_span_repl_rec.risk_na;
               
               FOR j IN 1 .. v_years LOOP
                  v_risk_reduction := 0;
                  v_risk_reduction_npv := 0;
                  v_risk_reduction_npv_sif := 0;
                  v_age := v_age + 1;
--                  get_bay_risk_for_year(i_span_tab(i).pick_id, 'NOACTION', v_age, v_variation
--                                    ,v_year_0 + j, i_span_tab(i).del_yr, i_span_tab(i).fdr_cat, v_risk_noaction, v_risk_npv, v_risk_noaction_npv
--                                    ,v_risk_npv_sif, v_risk_noaction_npv_sif);
						v_risk_noaction := v_risk_na_tab(j);                                   
                  IF v_span_repl_year = 0 THEN
                     v_risk := v_risk_noaction;
                  ELSIF j < v_span_repl_year THEN
                     v_risk := v_risk_noaction;
                  ELSIF j = v_span_repl_year THEN 
                     v_event := 'REPLACE';
                     v_event_reason := v_span_repl_tab(i).repl_rsn(j);
                     get_bay_risk_for_year(i_span_tab(i).pick_id, 'REPLACE', 0, NULL
                                          ,v_year_0 + j, i_span_tab(i).del_yr, i_span_tab(i).fdr_cat, v_risk, v_risk_npv, v_risk_noaction_npv
                                          ,v_risk_npv_sif, v_risk_noaction_npv_sif);
                     v_capex := get_bay_value_for_year(0,i_span_tab(i).area_type,i_span_tab(i).lvco_cnt,i_span_tab(i).hvco_cnt
                              ,i_span_tab(i).hvsp_cnt,i_span_tab(i).span_len);
							v_cost_key := TRIM(i_span_tab(i).area_type)||TO_CHAR(i_span_tab(i).lvco_cnt,'00')||TO_CHAR(i_span_tab(i).hvco_cnt,'00')||TO_CHAR(i_span_tab(i).hvsp_cnt,'00');
            			v_labour_hrs := v_cost_tab(v_cost_key).labour_hrs * i_span_tab(i).span_len / 1000;                            
                  ELSIF j > v_span_repl_year THEN
                     v_event_reason := v_span_repl_tab(i).repl_rsn(j);
                     get_bay_risk_for_year(i_span_tab(i).pick_id, 'REPLACE', 0, NULL
                                       ,v_year_0 + j, i_span_tab(i).del_yr, i_span_tab(i).fdr_cat, v_risk, v_risk_npv, v_risk_noaction_npv
                                       ,v_risk_npv_sif, v_risk_noaction_npv_sif);
                  END IF;
                        
                           
                  IF v_risk < v_risk_noaction THEN
                     v_risk_reduction := v_risk_noaction - v_risk;
                  END IF;
                              
                  IF v_risk_npv < v_risk_noaction_npv THEN
                     v_risk_reduction_npv := v_risk_noaction_npv - v_risk_npv;
                  END IF;

                  IF v_risk_npv_sif < v_risk_noaction_npv_sif THEN
                     v_risk_reduction_npv_sif := v_risk_noaction_npv_sif - v_risk_npv_sif;
                  END IF;
                  IF v_risk < v_risk_noaction THEN
                     v_risk_reduction := v_risk_noaction - v_risk;
                  END IF;
                              
                  IF v_risk_npv < v_risk_noaction_npv THEN
                     v_risk_reduction_npv := v_risk_noaction_npv - v_risk_npv;
                  END IF;

                  IF v_risk_npv_sif < v_risk_noaction_npv_sif THEN
                     v_risk_reduction_npv_sif := v_risk_noaction_npv_sif - v_risk_npv_sif;
                  END IF;
                  v_rr_sif_fdr_cat_adj := v_risk_reduction_npv_sif * v_fdr_cat_sif_tab(i_span_tab(i).fdr_cat).adj_factor;
                        
--                  IF j = 1 THEN               --pickid init
--                     --IF v_event IS NOT NULL AND v_mtzn_sched_tab.EXISTS(i_span_tab(i).mtzn) THEN
--                     IF v_event IS NOT NULL AND v_mtzn_sched_tab.EXISTS(v_cons_segment_mtzn) THEN
--                        v_scheduled := 'Y';
--                     ELSE
--                        v_scheduled := 'N';
--                     END IF;
--                  END IF;

                  /* insert row for bay+year */ 
                  INSERT 
                  INTO structools.st_bay_strategy_run
                  			(
                           RUN_ID
                           ,VIC_ID
                           ,PICK_ID
                           ,EQUIP_GRP_ID
                           ,EVENT
                           ,RISK
                           ,RISK_NOACTION
--                           ,REMAINING_VAL
                           ,LABOUR_HRS
                           ,CAPEX
--                           ,OPEX
                           ,AGE
                           ,CALENDAR_YEAR
                           ,EVENT_REASON
                           ,RISK_REDUCTION
--                           ,PROT_ZONE_ID
--                           ,PROT_ZONE_LEN
                           ,SPAN_LEN
                           ,HVCO_CNT
                           ,LVCO_CNT
                           ,HVSP_CNT
                           ,MAINT_ZONE_NAM
                           ,RISK_NPV15
                           ,RISK_NOACTION_NPV15
                           ,RISK_REDUCTION_NPV15
                           ,RISK_NPV15_SIF
                           ,RISK_NOACTION_NPV15_SIF
                           ,RISK_REDUCTION_NPV15_SIF
                           ,SEGMENT_ID
                           ,COND_EQP_TYP
                           ,CONS_SEGMENT_ID
                           ,SCHEDULED
                           ,CONS_SEGMENT_MTZN
                           ,RR_NPV15_SIF_FDR_CAT_ADJ
                           ,PRIORITY_IND
                           ,PICKID_SUPPRESSED
                           )
                           VALUES  (v_run_id
                                   ,v_span 
                                   ,i_span_tab(i).pick_id          
                                   ,'BAY'     
                                   ,v_event            
                                   ,v_risk
                                   ,v_risk_noaction   
--                                   ,NULL		--remaining_val
                                   ,v_labour_hrs                 
                                   ,v_capex      
--                                   ,0             
                                   ,v_age              
                                   ,v_year_0 + j    
                                   ,v_event_reason   
                                   ,v_risk_reduction   
--                                   ,NULL        --prot_zone_id
--                                   ,NULL        --prot_zone_len
                                   ,i_span_tab(i).span_len
                                   ,i_span_tab(i).hvco_cnt
                                   ,i_span_tab(i).lvco_cnt
                                   ,i_span_tab(i).hvsp_cnt
                                   ,i_span_tab(i).mtzn
                                   ,v_risk_npv
                                   ,v_risk_noaction_npv
                                   ,v_risk_reduction_npv
                                   ,v_risk_npv_sif
                                   ,v_risk_noaction_npv_sif
                                   ,v_risk_reduction_npv_sif
                                   ,i_span_tab(i).segment_id
                                   ,i_span_tab(i).bay_equip_cde
                                   ,v_cons_segment_id
                                   --,v_scheduled           --pickid init
                                   ,i_span_tab(i).scheduled
                                   ,v_cons_segment_mtzn
                                   ,v_rr_sif_fdr_cat_adj
                                   ,v_span_repl_tab(i).priority(j)
                                   ,v_pickid_suppressed
                                   )
                  ;
--                  IF v_event = 'REPLACE' THEN
--                     v_key := TO_CHAR(v_year_0 + j)||'L'||TRIM(i_span_tab(1).stc1);
--                     IF NOT v_bay_stc_tab.EXISTS(v_key) THEN
--                        v_bay_stc_tab(v_key) := ' ';
--                     END IF;
--                        
--                     v_key := TO_CHAR(v_year_0 + j)||'L'||TRIM(i_span_tab(1).stc2);
--                     IF NOT v_bay_stc_tab.EXISTS(v_key) THEN
--                        v_bay_stc_tab(v_key) := ' ';
--                     END IF;
--                  END IF;                       
               END LOOP;
            END LOOP;
            
            FOR i IN 1 .. i_span_tab.COUNT LOOP
               IF v_bay_risk_tab.EXISTS(i_span_tab(i).pick_id||'P') THEN
                  v_bay_risk_tab.DELETE(i_span_tab(i).pick_id||'P');
               END IF;
               IF v_bay_risk_tab.EXISTS(i_span_tab(i).pick_id||'NNPV15') THEN
                  v_bay_risk_tab.DELETE(i_span_tab(i).pick_id||'NNPV15');
               END IF;
               IF v_bay_risk_tab.EXISTS(i_span_tab(i).pick_id||'PNPV15') THEN
                  v_bay_risk_tab.DELETE(i_span_tab(i).pick_id||'PNPV15');
               END IF;
            END LOOP;

         EXCEPTION
            WHEN OTHERS THEN
               dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
               dbms_output.put_line('i_span_tab cnt = '||i_span_tab.COUNT||' span = '||i_span_tab(1).stc1||','||i_span_tab(1).stc2);
               FOR i IN 1 .. i_span_tab.COUNT LOOP
                  dbms_output.put_line('pickid = '||i_span_tab(i).pick_id);
                  dbms_output.put_line('scheduled = '||i_span_tab(i).scheduled);
                  dbms_output.put_line('del_yr = '||i_span_tab(i).del_yr);
                  dbms_output.put_line('material_cde_common = '||i_span_tab(i).material_cde_common);
                  dbms_output.put_line('condition_code = '||i_span_tab(i).condition_code);
                  dbms_output.put_line('v_cond_key1 = '||v_cond_key1);
                  dbms_output.put_line('v_cond_key2 = '||v_cond_key2);
                  dbms_output.put_line('v_cond_key = '||v_cond_key);
               END LOOP;
               dbms_output.put_line('v_span_repl_tab cnt = '||v_span_repl_tab.COUNT);
               dbms_output.put_line('v_repl_rsntab cnt = '||v_span_repl_tab.COUNT);
               FOR i IN 1 .. v_span_repl_tab.COUNT LOOP
                  dbms_output.put_line('pickid = '||v_span_repl_tab(i).bay_pick_id);
                  v_repl_rsn_tab := v_span_repl_tab(i).repl_rsn;
                  dbms_output.put_line('v_span_repl_tab repl_rsn_tab COUNT = '||v_repl_rsn_tab.COUNT);
               END LOOP;
               
               dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
               RAISE;

         END process_span;

         PROCEDURE check_bay_failure_prob(i_run_id IN PLS_INTEGER) IS
            
            v_pf_avg          float(126);
            
            CURSOR null_event_csr (i_year_idx IN PLS_INTEGER)   IS
               WITH null_bays AS
               (
               SELECT A.pick_id
                     ,A.risk_noaction
                     ,CASE i_year_idx
                        WHEN 1   THEN  b.fail_prob_y1
                        WHEN 2   THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2)
                        WHEN 3   THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3)
                        WHEN 4   THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4)
                        WHEN 5   THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5)
                        WHEN 6   THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6)
                        WHEN 7   THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 
                                                b.fail_prob_y7)
                        WHEN 8   THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 
                                                b.fail_prob_y7, b.fail_prob_y8)
                        WHEN 9   THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 
                                                b.fail_prob_y7, b.fail_prob_y8, b.fail_prob_y9)
                        WHEN 10  THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 
                                                b.fail_prob_y7, b.fail_prob_y8, b.fail_prob_y9, b.fail_prob_y10)
                        WHEN 11  THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 		--20 yrs
                                                b.fail_prob_y7, b.fail_prob_y8, b.fail_prob_y9, b.fail_prob_y10, b.fail_prob_y11)
                        WHEN 12  THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 		--20 yrs
                                                b.fail_prob_y7, b.fail_prob_y8, b.fail_prob_y9, b.fail_prob_y10, b.fail_prob_y11,b.fail_prob_y12)
                        WHEN 13  THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 		--20 yrs
                                                b.fail_prob_y7, b.fail_prob_y8, b.fail_prob_y9, b.fail_prob_y10, b.fail_prob_y11,b.fail_prob_y12, b.fail_prob_y13)
                        WHEN 14  THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 		--20 yrs
                                                b.fail_prob_y7, b.fail_prob_y8, b.fail_prob_y9, b.fail_prob_y10, b.fail_prob_y11, b.fail_prob_y12, b.fail_prob_y13, 
                                                b.fail_prob_y14)
                        WHEN 15  THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 		--20 yrs
                                                b.fail_prob_y7, b.fail_prob_y8, b.fail_prob_y9, b.fail_prob_y10, b.fail_prob_y11, b.fail_prob_y12, b.fail_prob_y13, 
                                                b.fail_prob_y14, b.fail_prob_y15)
                        WHEN 16  THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 		--20 yrs
                                                b.fail_prob_y7, b.fail_prob_y8, b.fail_prob_y9, b.fail_prob_y10, b.fail_prob_y11, b.fail_prob_y12, b.fail_prob_y13, 
                                                b.fail_prob_y14, b.fail_prob_y15, b.fail_prob_y16)
                        WHEN 17  THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 		--20 yrs
                                                b.fail_prob_y7, b.fail_prob_y8, b.fail_prob_y9, b.fail_prob_y10, b.fail_prob_y11, b.fail_prob_y12, b.fail_prob_y13, 
                                                b.fail_prob_y14, b.fail_prob_y15, b.fail_prob_y16, b.fail_prob_y17)
                        WHEN 18  THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 		--20 yrs
                                                b.fail_prob_y7, b.fail_prob_y8, b.fail_prob_y9, b.fail_prob_y10, b.fail_prob_y11, b.fail_prob_y12, b.fail_prob_y13, 
                                                b.fail_prob_y14, b.fail_prob_y15, b.fail_prob_y16, b.fail_prob_y17, b.fail_prob_y18)
                        WHEN 19  THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 		--20 yrs
                                                b.fail_prob_y7, b.fail_prob_y8, b.fail_prob_y9, b.fail_prob_y10, b.fail_prob_y11, b.fail_prob_y12, b.fail_prob_y13, 
                                                b.fail_prob_y14, b.fail_prob_y15, b.fail_prob_y16, b.fail_prob_y17, b.fail_prob_y18, b.fail_prob_y19)
                        WHEN 20  THEN  GREATEST(b.fail_prob_y1, b.fail_prob_y2, b.fail_prob_y3, b.fail_prob_y4, b.fail_prob_y5, b.fail_prob_y6, 		--20 yrs
                                                b.fail_prob_y7, b.fail_prob_y8, b.fail_prob_y9, b.fail_prob_y10, b.fail_prob_y11, b.fail_prob_y12, b.fail_prob_y13, 
                                                b.fail_prob_y14, b.fail_prob_y15, b.fail_prob_y16, b.fail_prob_y17, b.fail_prob_y18, b.fail_prob_y19, b.fail_prob_y20)
                     END                  AS bay_pfail   
               FROM  structools.st_bay_strategy_run     A  INNER JOIN
                     structools.st_risk_cond_noaction_fail  b
               ON    A.pick_id = 'B'||b.pick_id
               WHERE A.run_id = i_run_id
               AND   A.calendar_year = v_this_year + i_year_idx
               AND   A.event IS NULL 
               AND   A.scheduled = 'N'
               )
               SELECT A.pick_id
                     ,A.risk_noaction
                     ,c.area_type 
                     ,NVL(c.lvco_cnt, 0) AS lvco_cnt
                     ,NVL(c.hvco_cnt, 0) AS hvco_cnt
                     ,NVL(c.hvsp_cnt, 0) AS hvsp_cnt
                     ,ROUND(c.span_len)                     AS span_len
                     ,CASE 
                        WHEN UPPER(d.scnrrr_fdr_cat_nam) = 'NONE'             THEN  'SHORTRURAL'
                        WHEN d.scnrrr_fdr_cat_nam IS NULL                     THEN  'SHORTRURAL'
                        ELSE                                                         UPPER(d.scnrrr_fdr_cat_nam)
                     END                                                            AS fdr_cat
               FROM  null_bays                          A  INNER JOIN
                     structools.st_bay_equipment        b
               ON    A.pick_id = b.pick_id
                                                            INNER JOIN
                     structools.st_span                     c
               ON    b.stc1 = c.stc1
               AND   b.stc2 = c.stc2    
                                                            LEFT OUTER JOIN
                     structools.st_hv_feeder    d
               ON    b.feeder_id =        d.hv_fdr_id            
               WHERE  A.bay_pfail > v_pf_avg
               ORDER BY A.pick_id
               ; 
            
            TYPE  v_bay_event_pf_upgrade_tab_typ IS TABLE OF null_event_csr%ROWTYPE;
            v_bay_event_pf_upgrade_tab           v_bay_event_pf_upgrade_tab_typ := v_bay_event_pf_upgrade_tab_typ();               

            v_risk                     float(126);
            v_risk_reduction           float(126);
            v_risk_npv15               float(126);
            v_risk_noaction_npv15      float(26);
            v_risk_reduction_npv15     float(126);
            v_risk_npv15_sif           float(126);
            v_risk_noaction_npv15_sif  float(126); 
            v_risk_reduction_npv15_sif float(126);
            v_rr_npv15_sif_fdr_cat_adj float(126);
            v_capex                    PLS_INTEGER;
            v_labour_hrs             PLS_INTEGER;
            v_first_year               PLS_INTEGER;
            v_last_year                PLS_INTEGER;
            v_run_years                PLS_INTEGER;
            v_idx                      PLS_INTEGER;
         
         BEGIN

            dbms_output.put_line(TO_CHAR(SYSDATE,'hh24:mi:ss')||' Start OF check_bay_failure_prob');

            SELECT      run_years
                       ,first_year
            INTO       v_run_years
                      ,v_first_year
            FROM       structools.st_run_request    
            WHERE      run_id = i_run_id 
            ;

            v_last_year     := v_first_year - 1 + v_run_years; 


            --get_parameter_scenarios(i_run_id);
            structoolsapp.common_procs.get_parameter_scenarios(i_run_id, v_parm_scen_id_life_exp,v_parm_scen_id_seg_min_len, v_parm_scen_id_seg_cons_pcnt, v_parm_scen_id_sif_weighting
      																												,v_parm_scen_id_seg_rr_doll, v_parm_scen_id_eqp_rr_doll, v_parm_scen_id_fdr_cat_adj);

               
            populate_working_arrays;
               
            get_sif_weightings(v_parm_scen_id_sif_weighting, v_parm_scen_id_fdr_cat_adj);

         
            SELECT --COUNT(*) AS cnt
                  --,SUM(b.fail_prob_y0) AS sum_pf
                  AVG(b.fail_prob_y0) AS avg_pf
                  --,MAX(b.fail_prob_y0) AS max_pf
                  --,MIN(b.fail_prob_y0) AS min_pf
            INTO  v_pf_avg
            FROM structools.st_bay_strategy_run      A  INNER JOIN
                 structools.st_risk_cond_noaction_fail  b
            ON A.pick_id = 'B'||b.pick_id
            WHERE run_id = i_run_id
            AND calendar_year = v_first_year
            AND event = 'REPLACE'
            ;
            
            FOR i_yr IN v_first_year .. v_last_year LOOP
               v_idx := i_yr - v_this_year;
               OPEN null_event_csr(v_idx);
               FETCH null_event_csr BULK COLLECT INTO v_bay_event_pf_upgrade_tab;
               CLOSE null_event_csr;
               
               FOR i IN 1 .. v_bay_event_pf_upgrade_tab.COUNT LOOP
                  get_bay_risk_for_year(v_bay_event_pf_upgrade_tab(i).pick_id, 'REPLACE', 0, NULL
                                          ,i_yr, NULL, v_bay_event_pf_upgrade_tab(i).fdr_cat, v_risk, v_risk_npv15, v_risk_noaction_npv15
                                          ,v_risk_npv15_sif, v_risk_noaction_npv15_sif);
                  IF v_risk < v_bay_event_pf_upgrade_tab(i).risk_noaction THEN
                     v_risk_reduction := v_bay_event_pf_upgrade_tab(i).risk_noaction - v_risk;
                  ELSE
                     v_risk_reduction := 0;
                  END IF;
                  IF v_risk_npv15 < v_risk_noaction_npv15 THEN
                     v_risk_reduction_npv15 := v_risk_noaction_npv15 - v_risk_npv15;
                  ELSE
                     v_risk_reduction_npv15 := 0;
                  END IF;
                  IF v_risk_npv15_sif < v_risk_noaction_npv15_sif THEN
                     v_risk_reduction_npv15_sif := v_risk_noaction_npv15_sif - v_risk_npv15_sif;
                  ELSE
                     v_risk_reduction_npv15_sif := 0;
                  END IF;
                  v_rr_npv15_sif_fdr_cat_adj := v_risk_reduction_npv15_sif * v_fdr_cat_sif_tab(v_bay_event_pf_upgrade_tab(i).fdr_cat).adj_factor;
                  v_capex := get_bay_value_for_year(0,v_bay_event_pf_upgrade_tab(i).area_type,v_bay_event_pf_upgrade_tab(i).lvco_cnt
                                                   ,v_bay_event_pf_upgrade_tab(i).hvco_cnt,v_bay_event_pf_upgrade_tab(i).hvsp_cnt
                                                   ,v_bay_event_pf_upgrade_tab(i).span_len);
                  v_cost_key := TRIM(v_bay_event_pf_upgrade_tab(i).area_type)||TO_CHAR(v_bay_event_pf_upgrade_tab(i).lvco_cnt,'00')||TO_CHAR(v_bay_event_pf_upgrade_tab(i).hvco_cnt,'00')
                  							||TO_CHAR(v_bay_event_pf_upgrade_tab(i).hvsp_cnt,'00');
                  v_labour_hrs := v_cost_tab(v_cost_key).labour_hrs * v_bay_event_pf_upgrade_tab(i).span_len / 1000;                             

                  UPDATE   structools.st_bay_strategy_run
                     SET    risk = v_risk
                           ,risk_reduction = v_risk_reduction
                           ,risk_npv15 = v_risk_npv15
                           ,risk_noaction_npv15 = v_risk_noaction_npv15
                           ,risk_reduction_npv15 = v_risk_reduction_npv15
                           ,risk_npv15_sif = v_risk_npv15_sif
                           ,risk_noaction_npv15_sif = v_risk_noaction_npv15_sif
                           ,risk_reduction_npv15_sif = v_risk_reduction_npv15_sif
                           ,rr_npv15_sif_fdr_cat_adj = v_rr_npv15_sif_fdr_cat_adj
                           ,event = 'REPLACE'
                           ,event_reason = 'NRMT PF'
                           ,capex = v_capex
                           ,labour_hrs = v_labour_hrs
                  WHERE run_id = i_run_id
                  AND calendar_year = i_yr
                  AND pick_id = v_bay_event_pf_upgrade_tab(i).pick_id
                  ;
               END LOOP;
            END LOOP;
            
            dbms_output.put_line(TO_CHAR(SYSDATE,'hh24:mi:ss')||' END OF check_bay_failure_prob');
         
         EXCEPTION
            WHEN OTHERS THEN
               dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
               RAISE;

            
         END check_bay_failure_prob;
        
         PROCEDURE update_null_segment_mtzn IS
           
            CURSOR null_seg_mtzn IS
               
               WITH null_seg_mtzn AS
               (
               SELECT  cons_segment_id
                                 ,maint_zone_nam
                                 ,COUNT(*) AS cnt
               FROM structools.st_bay_strategy_run
               WHERE run_id = v_run_id
               AND cons_segment_id > ' '
               AND cons_segment_mtzn = 'UNKNOWN'      
               GROUP BY cons_segment_id
                                 ,maint_zone_nam
               )
               ,
               max_mtzn_cnt AS
               (
               SELECT cons_segment_id
                      ,MAX(cnt)  AS max_cnt
               FROM null_seg_mtzn
               GROUP BY cons_segment_id
               ) 
               SELECT A.cons_segment_id
                         ,MIN(A.maint_zone_nam)     AS cons_segment_mtzn
               FROM      null_seg_mtzn         A               INNER JOIN
                            max_mtzn_cnt         b
               ON A.cons_segment_id = b.cons_segment_id
               AND A.cnt = b.max_cnt  
               GROUP BY A.cons_segment_id
            ; 
           
         BEGIN
                    
            FOR c IN null_seg_mtzn LOOP
                UPDATE structools.st_bay_strategy_run
                SET         cons_segment_mtzn = c.cons_segment_mtzn
                WHERE run_id = v_run_id
                AND      cons_segment_id = c.cons_segment_id
                ;
            END LOOP;
           
         END update_null_segment_mtzn;

      /* Mainline for process_bays */
      BEGIN

         structools.schema_utils.disable_table_indexes('ST_BAY_STRATEGY_RUN');

         IF v_strategy_bay = 20 THEN
            v_parm_val_key := 'BAY'||'-'||'001'||'-'||'DIAM';
            v_thin_cu_diam := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'001'||'-'||'AGE';
            v_thin_cu_age  := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'BAY'||'-'||'002'||'-'||'AGE';
            v_steel_age   := TO_NUMBER(v_cond_parm_tab(v_parm_val_key)); 
            v_parm_val_key := 'BAY'||'-'||'003'||'-'||'AGE';
            v_aac_age      := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'004'||'-'||'DIAM';
            v_thick_cu_diam := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'004'||'-'||'AGE';
            v_thick_cu_age := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'005'||'-'||'AGE';
            v_acsr_age     := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'006'||'-'||'AGE';
            v_aaac_age     := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'007'||'-'||'AGE';
            v_hendrix_age  := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'008'||'-'||'AGE';
            v_lvabc_age    := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
         ELSIF v_strategy_bay = 34 THEN   
            v_parm_val_key := 'BAY'||'-'||'001'||'-'||'AGE';
            v_bay_age       := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
         ELSIF v_strategy_bay = 45 THEN   
            v_parm_val_key := 'BAY'||'-'||'001'||'-'||'AGE';
            v_bay_age       := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'BAY'||'-'||'001'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('BAY', v_fsrtg);
         ELSIF v_strategy_bay = 56 THEN  
            v_parm_val_key := 'BAY'||'-'||'001'||'-'||'DIAM';
            v_thin_cu_diam := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'001'||'-'||'AGE';
            v_thin_cu_age  := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'BAY'||'-'||'002'||'-'||'AGE';
            v_steel_age   := TO_NUMBER(v_cond_parm_tab(v_parm_val_key)); 
            v_parm_val_key := 'BAY'||'-'||'003'||'-'||'AGE';
            v_aac_age      := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
--            v_parm_val_key := 'BAY '||'-'||'004'||'-'||'AGE';
--            v_acsr_age     := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
         ELSIF v_strategy_bay = 57 THEN   --strat 57  
            v_parm_val_key := 'BAY'||'-'||'001'||'-'||'COND_SCORE1';
            v_cond_score1  := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'001'||'-'||'COND_SCORE2';
            v_cond_score2  := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'002'||'-'||'DIAM';
            v_thin_cu_diam := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'002'||'-'||'AGE';
            v_thin_cu_age  := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'BAY'||'-'||'003'||'-'||'AGE';
            v_steel_age   := TO_NUMBER(v_cond_parm_tab(v_parm_val_key)); 
            v_parm_val_key := 'BAY'||'-'||'004'||'-'||'AGE';
            v_aac_age      := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
--            v_parm_val_key := 'BAY '||'-'||'005'||'-'||'AGE';
--            v_acsr_age     := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
         ELSIF v_strategy_bay = 63 THEN
            v_parm_val_key := 'BAY'||'-'||'002'||'-'||'DIAM1';
            v_diam1 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'002'||'-'||'DIST_COAST_KM1';
            v_dist_coast_km1 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));

            v_parm_val_key := 'BAY'||'-'||'003'||'-'||'DIAM2';
            v_diam2 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'003'||'-'||'DIST_COAST_KM2';
            v_dist_coast_km2 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'BAY'||'-'||'003'||'-'||'AGE1';
            v_age1 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));

            v_parm_val_key := 'BAY'||'-'||'004'||'-'||'DIAM3';
            v_diam3 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
                      
            v_parm_val_key := 'BAY'||'-'||'005'||'-'||'DIAM4';
            v_diam4 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'005'||'-'||'DIAM5';
            v_diam5 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'005'||'-'||'DIST_COAST_KM3';
            v_dist_coast_km3 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
                      
            v_parm_val_key := 'BAY'||'-'||'006'||'-'||'DIAM6';
            v_diam6 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'006'||'-'||'DIAM7';
            v_diam7 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
                     
            v_parm_val_key := 'BAY'||'-'||'007'||'-'||'DIAM8';
            v_diam8 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'007'||'-'||'DIAM9';
            v_diam9 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'007'||'-'||'DIST_COAST_KM4';
            v_dist_coast_km4 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'BAY'||'-'||'007'||'-'||'AGE2';
            v_age2 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
         ELSIF v_strategy_bay = 64 OR v_strategy_bay = 66 THEN
            v_parm_val_key := 'BAY'||'-'||'002'||'-'||'DIAM1';
            v_diam1 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'002'||'-'||'DIST_COAST_KM1';
            v_dist_coast_km1 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));

            v_parm_val_key := 'BAY'||'-'||'003'||'-'||'DIAM2';
            v_diam2 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'003'||'-'||'DIST_COAST_KM2';
            v_dist_coast_km2 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'BAY'||'-'||'003'||'-'||'AGE1';
            v_age1 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));

            v_parm_val_key := 'BAY'||'-'||'004'||'-'||'DIAM3';
            v_diam3 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
                      
            v_parm_val_key := 'BAY'||'-'||'005'||'-'||'DIAM4';
            v_diam4 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'005'||'-'||'DIAM5';
            v_diam5 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'005'||'-'||'DIST_COAST_KM3';
            v_dist_coast_km3 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
                      
            v_parm_val_key := 'BAY'||'-'||'006'||'-'||'DIAM6';
            v_diam6 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'006'||'-'||'DIAM7';
            v_diam7 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
                     
            v_parm_val_key := 'BAY'||'-'||'007'||'-'||'DIAM8';
            v_diam8 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'007'||'-'||'DIAM9';
            v_diam9 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'007'||'-'||'DIST_COAST_KM4';
            v_dist_coast_km4 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'BAY'||'-'||'007'||'-'||'AGE2';
            v_age2 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
         ELSIF v_strategy_bay = 65 OR v_strategy_bay = 69  OR v_strategy_bay = 70  OR v_strategy_bay = 73  OR v_strategy_bay = 80 THEN
            v_parm_val_key := 'BAY'||'-'||'002'||'-'||'DIAM1';
            v_diam1 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'002'||'-'||'DIST_COAST_KM1';
            v_dist_coast_km1 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));

            v_parm_val_key := 'BAY'||'-'||'003'||'-'||'DIAM2';
            v_diam2 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'003'||'-'||'DIST_COAST_KM2';
            v_dist_coast_km2 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'BAY'||'-'||'003'||'-'||'AGE1';
            v_age1 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));

            v_parm_val_key := 'BAY'||'-'||'004'||'-'||'DIAM3';
            v_diam3 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
                      
            v_parm_val_key := 'BAY'||'-'||'005'||'-'||'DIAM4';
            v_diam4 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'005'||'-'||'DIAM5';
            v_diam5 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
                      
            v_parm_val_key := 'BAY'||'-'||'006'||'-'||'DIAM6';
            v_diam6 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            v_parm_val_key := 'BAY'||'-'||'006'||'-'||'DIAM7';
            v_diam7 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'BAY'||'-'||'006'||'-'||'DIST_COAST_KM3';
            v_dist_coast_km3 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'BAY'||'-'||'006'||'-'||'AGE2';
            v_age2 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            
            IF v_strategy_bay = 69 THEN
               v_parm_val_key := 'BAY'||'-'||'008'||'-'||'PFAIL';
               v_pfail := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
               v_parm_val_key := 'BAY'||'-'||'008'||'-'||'AGE3';
               v_age3 := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
               v_parm_val_key := 'BAY'||'-'||'008'||'-'||'CHNG_YR';
               v_chng_yr := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            ELSIF v_strategy_bay = 70 THEN
               v_parm_val_key := 'BAY'||'-'||'008'||'-'||'PFAIL';
               v_pfail := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
               v_parm_val_key := 'BAY'||'-'||'008'||'-'||'CHNG_YR';
               v_chng_yr := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            ELSIF v_strategy_bay = 73 THEN
               v_parm_val_key := 'BAY'||'-'||'008'||'-'||'AGE_TOPUP';
               v_age_topup := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
               v_parm_val_key := 'BAY'||'-'||'008'||'-'||'YEAR_TOPUP';
               v_year_topup := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
               v_parm_val_key := 'BAY'||'-'||'008'||'-'||'SAFETY_NA_TOPUP';
               v_safety_na_topup := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            ELSIF v_strategy_bay = 80 THEN
               v_parm_val_key := 'BAY'||'-'||'009'||'-'||'PFAIL';
               v_pfail := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
               v_parm_val_key := 'BAY'||'-'||'009'||'-'||'CHNG_YR';
               v_chng_yr := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));         
            END IF;
            
         END IF;     
            
         v_span_bay_cnt := 0;
         v_stc1_prev := ' ';
         v_stc2_prev := ' ';
         v_span_cnt := 0;

         IF v_strategy_bay = 63 THEN
            OPEN bay_63_csr;
         ELSIF v_strategy_bay = 64 OR v_strategy_bay = 65  OR v_strategy_bay = 66  OR v_strategy_bay = 69 OR v_strategy_bay = 70 OR v_strategy_bay = 73 OR v_strategy_bay = 80 THEN
            OPEN bay_64_csr;
         ELSE            
            OPEN bay_20_34_45_56_57_csr;
         END IF;

         LOOP
            IF v_strategy_bay = 63 THEN
               FETCH bay_63_csr INTO v_bay_all_rec;
               EXIT WHEN bay_63_csr%NOTFOUND; -- OR v_span_cnt > 30000;  --graz
            ELSIF v_strategy_bay = 64 OR v_strategy_bay = 65 OR v_strategy_bay = 66  OR v_strategy_bay = 69 OR v_strategy_bay = 70 OR v_strategy_bay = 73 OR v_strategy_bay = 80 THEN
               FETCH bay_64_csr INTO v_bay_all_rec;
               EXIT WHEN bay_64_csr%NOTFOUND; -- OR v_span_cnt > 30000;  --graz
            ELSE                        
               FETCH bay_20_34_45_56_57_csr INTO v_bay_all_rec;
               EXIT WHEN bay_20_34_45_56_57_csr%NOTFOUND; -- OR v_span_cnt > 30000;  --graz
            END IF;
                  
            IF (v_bay_all_rec.stc1 != v_stc1_prev
            OR v_bay_all_rec.stc2 != v_stc2_prev) THEN
               IF v_stc1_prev != ' ' THEN
                  process_span(v_span_tab);
                  v_span_tab.DELETE;
                  v_span_bay_cnt := 0;
                  v_span_cnt := v_span_cnt + 1;
               END IF;
            END IF;
            v_span_tab.EXTEND(1);
            v_span_bay_cnt := v_span_bay_cnt + 1;
            v_span_tab(v_span_bay_cnt) := v_bay_all_rec;
   --            IF v_span_cnt > 20 THEN    --graz
   --               EXIT;
   --            END IF;
            v_stc1_prev := v_bay_all_rec.stc1;
            v_stc2_prev := v_bay_all_rec.stc2;
         END LOOP;

         process_span(v_span_tab);
            
         dbms_output.put_line('processed ALL'||v_span_cnt||' spans '||TO_CHAR(SYSDATE,'hh24:mi:ss'));

         IF v_strategy_bay = 63 THEN
            CLOSE bay_63_csr;
         ELSIF v_strategy_bay = 64 OR v_strategy_bay = 65 OR v_strategy_bay = 66  OR v_strategy_bay = 69 OR v_strategy_bay = 70 OR v_strategy_bay = 73 OR v_strategy_bay = 80 THEN
            CLOSE bay_64_csr;
         ELSE
            CLOSE bay_20_34_45_56_57_csr;
         END IF;

         v_bay_risk_tab.DELETE;
      
         /* recreate indexes for st_bay_strategy_run at this point; otherwise the subsequent update_null_segment_mtzn will take very long */
         structools.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN');  -- this commits db updates
         analyse_table('ST_BAY_STRATEGY_RUN');
         
         IF v_strategy_bay = 66 THEN
            check_bay_failure_prob(i_run_id);
         END IF;
               	
         update_null_segment_mtzn;

         v_bay_risk_tab.DELETE;
      
      END process_bays;

      /*******************************************************************************/
      /* Load what can be loaded from db into internal tables, initialise values etc */
      /*******************************************************************************/ 
      PROCEDURE initialise IS
      
         j                       PLS_INTEGER;
         v_sif_tot_pcnt                FLOAT(126);
         v_failure_prob_cutoff_pwod    FLOAT;
         v_failure_prob_cutoff_dstr    FLOAT;
         v_failure_prob_cutoff_dof     FLOAT;
         v_failure_prob_cutoff_recl    FLOAT;
         v_failure_prob_cutoff_sect    FLOAT;
         v_failure_prob_cutoff_ptsd    FLOAT;
         v_pick_id_yr            CHAR(11);
         K                       PLS_INTEGER;


         CURSOR risk_pwod_yr20_noaction_csr IS
            SELECT   A.* 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
            FROM   structools.st_risk_pwod_noaction   A
         ; 
         CURSOR risk_dstr_yr20_noaction_csr IS
            SELECT   A.* 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
            FROM   structools.st_risk_dstr_noaction   A
         ; 
         CURSOR risk_dof_yr20_noaction_csr IS
            SELECT   A.* 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
            FROM   structools.st_risk_dof_noaction   A
         ; 
         CURSOR risk_recl_yr20_noaction_csr IS
            SELECT   A.* 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
            FROM   structools.st_risk_recl_noaction   A
         ; 
         CURSOR risk_sect_yr20_noaction_csr IS
            SELECT   A.* 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
            FROM   structools.st_risk_sect_noaction   A
         ; 
         CURSOR risk_ptsd_yr20_noaction_csr IS
            SELECT   A.* 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
            FROM   structools.st_risk_ptsd_noaction   A
         ; 
         CURSOR risk_rgtr_yr20_noaction_csr IS
            SELECT   A.* 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
            FROM   structools.st_risk_rgtr_noaction   A
         ; 
         CURSOR risk_bay_yr20_noaction_csr IS
            SELECT   A.* 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
                    ,NULL 
            FROM   structools.st_risk_bay_noaction   A
         ; 

         CURSOR failure_noaction_prob_csr IS    /* set to work for a 10 year horizon */  --20 yrs
            WITH unpivoted_pwod AS
            (
            SELECT pick_id, yr, fail_prob
            FROM  structools.st_risk_pole_noaction_fail
            UNPIVOT (fail_prob FOR yr IN (fail_prob_y0 AS '00', fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03',fail_prob_y4 AS '04', fail_prob_y5 AS '05'
            									,fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09', fail_prob_y10 AS '10'
                                       ,fail_prob_y11 AS '11',fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14', fail_prob_y15 AS '15'
                                       ,fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19',fail_prob_y20 AS '20'))
            )
            ,
            unpivoted_dstr AS
            (
            SELECT pick_id, yr, fail_prob
            FROM  structools.st_risk_dstr_noaction_fail
            UNPIVOT (fail_prob FOR yr IN (fail_prob_y0 AS '00', fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03',fail_prob_y4 AS '04', fail_prob_y5 AS '05'
            									,fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09', fail_prob_y10 AS '10'
                                       ,fail_prob_y11 AS '11',fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14', fail_prob_y15 AS '15'
                                       ,fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19',fail_prob_y20 AS '20'))
            )
            ,
            unpivoted_dof AS
            (
            SELECT pick_id, yr, fail_prob
            FROM  structools.st_risk_dof_noaction_fail
            UNPIVOT (fail_prob FOR yr IN (fail_prob_y0 AS '00', fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03',fail_prob_y4 AS '04', fail_prob_y5 AS '05'
            									,fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09', fail_prob_y10 AS '10'
                                       ,fail_prob_y11 AS '11',fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14', fail_prob_y15 AS '15'
                                       ,fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19',fail_prob_y20 AS '20'))
            )
            ,
            unpivoted_recl AS
            (
            SELECT pick_id, yr, fail_prob
            FROM  structools.st_risk_recl_noaction_fail
            UNPIVOT (fail_prob FOR yr IN (fail_prob_y0 AS '00', fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03',fail_prob_y4 AS '04', fail_prob_y5 AS '05'
            									,fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09', fail_prob_y10 AS '10'
                                       ,fail_prob_y11 AS '11',fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14', fail_prob_y15 AS '15'
                                       ,fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19',fail_prob_y20 AS '20'))
            )
            ,
            unpivoted_sect AS
            (
            SELECT pick_id, yr, fail_prob
            FROM  structools.st_risk_sect_noaction_fail
            UNPIVOT (fail_prob FOR yr IN (fail_prob_y0 AS '00', fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03',fail_prob_y4 AS '04', fail_prob_y5 AS '05'
            									,fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09', fail_prob_y10 AS '10'
                                       ,fail_prob_y11 AS '11',fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14', fail_prob_y15 AS '15'
                                       ,fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19',fail_prob_y20 AS '20'))
            )
            ,
            unpivoted_ptsd AS
            (
            SELECT pick_id, yr, fail_prob
            FROM  structools.st_risk_ptsd_noaction_fail
            UNPIVOT (fail_prob FOR yr IN (fail_prob_y0 AS '00', fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03',fail_prob_y4 AS '04', fail_prob_y5 AS '05'
            									,fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09', fail_prob_y10 AS '10'
                                       ,fail_prob_y11 AS '11',fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14', fail_prob_y15 AS '15'
                                       ,fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19',fail_prob_y20 AS '20'))
            )
            ,
            unpivoted_rgtr AS
            (
            SELECT pick_id, yr, fail_prob
            FROM  structools.st_risk_rgtr_noaction_fail
            UNPIVOT (fail_prob FOR yr IN (fail_prob_y0 AS '00', fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03',fail_prob_y4 AS '04', fail_prob_y5 AS '05'
            									,fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09', fail_prob_y10 AS '10'
                                       ,fail_prob_y11 AS '11',fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14', fail_prob_y15 AS '15'
                                       ,fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19',fail_prob_y20 AS '20'))
            )
            ,
            first_year_pwod AS
            (
--            SELECT pick_id
--                  ,min_year
--            FROM                  
--               (
               SELECT 'S'||pick_id              AS pick_id
                     ,MIN(yr)                   AS min_year
               FROM unpivoted_pwod
               WHERE fail_prob > least(v_FAIL_PROB_LOW_pwod, v_FAIL_PROB_HIGH_pwod)
               GROUP BY pick_id
--               )
--            WHERE min_year <= v_run_years_sum_char
            )
            ,
            first_year_dstr AS
            (
--            SELECT pick_id
--                  ,min_year
--            FROM                  
--               (
               SELECT 'N'||pick_id              AS pick_id
                     ,MIN(yr)                   AS min_year
               FROM unpivoted_dstr
               WHERE fail_prob > least(v_FAIL_PROB_LOW_dstr, v_FAIL_PROB_HIGH_dstr)
               GROUP BY pick_id
--               )
--            WHERE min_year <= v_run_years_sum_char
            )
            ,
            first_year_dof AS
            (
--            SELECT pick_id
--                  ,min_year
--            FROM                  
--               (
               SELECT 'N'||pick_id              AS pick_id
                     ,MIN(yr)                   AS min_year
               FROM unpivoted_dof
               WHERE fail_prob > least(v_FAIL_PROB_LOW_dof, v_FAIL_PROB_HIGH_dof)
               GROUP BY pick_id
--               )
--            WHERE min_year <= v_run_years_sum_char
            )
            ,
            first_year_recl AS
            (
--            SELECT pick_id
--                  ,min_year
--            FROM                  
--               (
               SELECT 'N'||pick_id              AS pick_id
                     ,MIN(yr)                   AS min_year
               FROM unpivoted_recl
               WHERE fail_prob > least(v_FAIL_PROB_LOW_recl, v_FAIL_PROB_HIGH_recl)
               GROUP BY pick_id
--               )
--            WHERE min_year <= v_run_years_sum_char
            )
            ,
            first_year_sect AS
            (
--            SELECT pick_id
--                  ,min_year
--            FROM                  
--               (
               SELECT 'N'||pick_id              AS pick_id
                     ,MIN(yr)                   AS min_year
               FROM unpivoted_sect
               WHERE fail_prob > least(v_FAIL_PROB_LOW_sect, v_FAIL_PROB_HIGH_sect)
               GROUP BY pick_id
--               )
--            WHERE min_year <= v_run_years_sum_char
            )
            ,
            first_year_ptsd AS
            (
--            SELECT pick_id
--                  ,min_year
--            FROM                  
--               (
               SELECT 'N'||pick_id              AS pick_id
                     ,MIN(yr)                   AS min_year
               FROM unpivoted_ptsd
               WHERE fail_prob > least(v_FAIL_PROB_LOW_ptsd, v_FAIL_PROB_HIGH_ptsd)
               GROUP BY pick_id
--               )
--            WHERE min_year <= v_run_years_sum_char
            )
            ,
            first_year_rgtr AS
            (
--            SELECT pick_id
--                  ,min_year
--            FROM                  
--               (
               SELECT 'N'||pick_id              AS pick_id
                     ,MIN(yr)                   AS min_year
               FROM unpivoted_rgtr
               WHERE fail_prob > least(v_FAIL_PROB_LOW_rgtr, v_FAIL_PROB_HIGH_rgtr)
               GROUP BY pick_id
--               )
--            WHERE min_year <= v_run_years_sum_char
            )
            SELECT A.pick_id
                  --,A.min_year
                  ,b.yr
                  ,b.fail_prob
            FROM first_year_pwod       A  INNER JOIN                  
                 unpivoted_pwod        b
            ON A.pick_id = 'S'||b.Pick_id
            WHERE b.yr BETWEEN A.min_year AND v_run_years_sum_char    
            AND b.yr != '00'
            UNION ALL
            SELECT A.pick_id
                  --,A.min_year
                  ,b.yr
                  ,b.fail_prob
            FROM first_year_dstr       A  INNER JOIN                  
                 unpivoted_dstr        b
            ON A.pick_id = 'N'||b.Pick_id
            WHERE b.yr BETWEEN A.min_year AND v_run_years_sum_char
            AND b.yr != '00'
            UNION ALL
            SELECT A.pick_id
                  --,A.min_year
                  ,b.yr
                  ,b.fail_prob
            FROM first_year_dof       A  INNER JOIN                  
                 unpivoted_dof        b
            ON A.pick_id = 'N'||b.Pick_id
            WHERE b.yr BETWEEN A.min_year AND v_run_years_sum_char
            AND b.yr != '00'
            UNION ALL
            SELECT A.pick_id
                  --,A.min_year
                  ,b.yr
                  ,b.fail_prob
            FROM first_year_recl       A  INNER JOIN                  
                 unpivoted_recl        b
            ON A.pick_id = 'N'||b.Pick_id
            WHERE b.yr BETWEEN A.min_year AND v_run_years_sum_char
            AND b.yr != '00'
            UNION ALL
            SELECT A.pick_id
                  --,A.min_year
                  ,b.yr
                  ,b.fail_prob
            FROM first_year_sect       A  INNER JOIN                  
                 unpivoted_sect        b
            ON A.pick_id = 'N'||b.Pick_id
            WHERE b.yr BETWEEN A.min_year AND v_run_years_sum_char
            AND b.yr != '00'
            UNION ALL
            SELECT A.pick_id
                  --,A.min_year
                  ,b.yr
                  ,b.fail_prob
            FROM first_year_ptsd       A  INNER JOIN                  
                 unpivoted_ptsd        b
            ON A.pick_id = 'N'||b.Pick_id
            WHERE b.yr BETWEEN A.min_year AND v_run_years_sum_char
            AND b.yr != '00'
            UNION ALL
            SELECT A.pick_id
                  --,A.min_year
                  ,b.yr
                  ,b.fail_prob
            FROM first_year_rgtr       A  INNER JOIN                  
                 unpivoted_rgtr        b
            ON A.pick_id = 'N'||b.Pick_id
            WHERE b.yr BETWEEN A.min_year AND v_run_years_sum_char
            AND b.yr != '00'
         ;

--         CURSOR mtzn_sched_csr IS             --pickid init
--            SELECT mtzn
--            FROM   structools.st_mtzn_committed
--            WHERE commit_yr = v_year_0
--            AND mtzn_committed_version_id = v_mtzn_committed_version_id;

         CURSOR mtzn_priority_csr IS
            SELECT mtzn
            FROM structools.st_mtzn_priority
         ;
      
      /* Mainline of Initialise */
      BEGIN

         v_cost_treatment_type := 'FULL';  -- We only have unit rates for FULL and STANDALONE treatment, no PARTIAL at this stage.
     
         --get_parameter_scenarios(i_run_id);
         structoolsapp.common_procs.get_parameter_scenarios(i_run_id, v_parm_scen_id_life_exp,v_parm_scen_id_seg_min_len, v_parm_scen_id_seg_cons_pcnt, v_parm_scen_id_sif_weighting
      																												,v_parm_scen_id_seg_rr_doll, v_parm_scen_id_eqp_rr_doll, v_parm_scen_id_fdr_cat_adj);

         
         populate_working_arrays;
         
         /* get risk values for poles into memory (associative array, indexed by pickid+action) */  
        
         OPEN risk_pwod_yr20_noaction_csr;
         LOOP 
            FETCH risk_pwod_yr20_noaction_csr BULK COLLECT INTO v_risk_yr20_tab LIMIT 100;
            --EXIT WHEN risk_pwod_yr50_noaction_csr%NOTFOUND;
            EXIT WHEN v_risk_yr20_tab.COUNT = 0;
            store_risk_pwod('N');
            v_risk_yr20_tab.DELETE;
         END LOOP;
         CLOSE risk_pwod_yr20_noaction_csr;

         OPEN risk_dstr_yr20_noaction_csr;
         LOOP 
            FETCH risk_dstr_yr20_noaction_csr BULK COLLECT INTO v_risk_yr20_tab LIMIT 100;
            --EXIT WHEN risk_dstr_yr20_noaction_csr%NOTFOUND;
            EXIT WHEN v_risk_yr20_tab.COUNT = 0;
            store_risk_eqp('N');
            v_risk_yr20_tab.DELETE;
         END LOOP;
         CLOSE risk_dstr_yr20_noaction_csr;

         OPEN risk_dof_yr20_noaction_csr;
         LOOP 
            FETCH risk_dof_yr20_noaction_csr BULK COLLECT INTO v_risk_yr20_tab LIMIT 100;
            --EXIT WHEN risk_dof_yr20_noaction_csr%NOTFOUND;
            EXIT WHEN v_risk_yr20_tab.COUNT = 0;
            store_risk_eqp('N');
            v_risk_yr20_tab.DELETE;
         END LOOP;
         CLOSE risk_dof_yr20_noaction_csr;
         
         OPEN risk_recl_yr20_noaction_csr;
         LOOP 
            FETCH risk_recl_yr20_noaction_csr BULK COLLECT INTO v_risk_yr20_tab LIMIT 100;
            --EXIT WHEN risk_recl_yr20_noaction_csr%NOTFOUND;
            EXIT WHEN v_risk_yr20_tab.COUNT = 0;
            store_risk_eqp('N');
            v_risk_yr20_tab.DELETE;
         END LOOP;
         CLOSE risk_recl_yr20_noaction_csr;

         OPEN risk_sect_yr20_noaction_csr;
         LOOP 
            FETCH risk_sect_yr20_noaction_csr BULK COLLECT INTO v_risk_yr20_tab LIMIT 100;
            --EXIT WHEN risk_sect_yr20_noaction_csr%NOTFOUND;
            EXIT WHEN v_risk_yr20_tab.COUNT = 0;
            store_risk_eqp('N');
            v_risk_yr20_tab.DELETE;
         END LOOP;
         CLOSE risk_sect_yr20_noaction_csr;

         OPEN risk_ptsd_yr20_noaction_csr;
         LOOP 
            FETCH risk_ptsd_yr20_noaction_csr BULK COLLECT INTO v_risk_yr20_tab LIMIT 100;
            --EXIT WHEN risk_ptsd_yr20_noaction_csr%NOTFOUND;
            EXIT WHEN v_risk_yr20_tab.COUNT = 0;
            store_risk_eqp('N');
            v_risk_yr20_tab.DELETE;
         END LOOP;
         CLOSE risk_ptsd_yr20_noaction_csr;

         OPEN risk_rgtr_yr20_noaction_csr;
         LOOP 
            FETCH risk_rgtr_yr20_noaction_csr BULK COLLECT INTO v_risk_yr20_tab LIMIT 100;
            --EXIT WHEN risk_rgtr_yr20_noaction_csr%NOTFOUND;
            EXIT WHEN v_risk_yr20_tab.COUNT = 0;
            store_risk_eqp('N');
            v_risk_yr20_tab.DELETE;
         END LOOP;
         CLOSE risk_rgtr_yr20_noaction_csr;

         OPEN risk_bay_yr20_noaction_csr;
         LOOP 
            FETCH risk_bay_yr20_noaction_csr BULK COLLECT INTO v_risk_yr20_tab LIMIT 100;
            --EXIT WHEN risk_rgtr_yr20_noaction_csr%NOTFOUND;
            EXIT WHEN v_risk_yr20_tab.COUNT = 0;
            store_risk_bay('N');
            v_risk_yr20_tab.DELETE;
         END LOOP;
         CLOSE risk_bay_yr20_noaction_csr;

         v_risk_yr20_tab := v_risk_yr20_tab_empty;
         
         get_sif_weightings(v_parm_scen_id_sif_weighting, v_parm_scen_id_fdr_cat_adj);
       
         /* get prob of failure for all eqp, where the probability > p_failure_prob_cutoff_xxxx, and load into an associative table, indexed */
         /* by pickid and year */
         
         IF v_strategy_pwod = 13 THEN
            v_parm_val_key := 'PWOD'||'-'||'014'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_pwod := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'PWOD'||'-'||'014'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_pwod := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_pwod := 0;
            ELSE
               v_fail_prob_step_pwod := (v_FAIL_PROB_HIGH_pwod - v_FAIL_PROB_LOW_pwod) / (v_run_years - 1);
            END IF;
         ELSIF v_strategy_pwod = 35 THEN
            v_parm_val_key := 'PWOD'||'-'||'001'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('PWOD', v_fsrtg);
            v_parm_val_key := 'PWOD'||'-'||'002'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('PWOD', v_fsrtg);
            v_parm_val_key := 'PWOD'||'-'||'003'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('PWOD', v_fsrtg);
         ELSIF v_strategy_pwod = 46 THEN
            v_parm_val_key := 'PWOD'||'-'||'016'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_pwod := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'PWOD'||'-'||'016'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_pwod := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_pwod := 0;
            ELSE
               v_fail_prob_step_pwod := (v_FAIL_PROB_HIGH_pwod - v_FAIL_PROB_LOW_pwod) / (v_run_years - 1);
            END IF;
         ELSIF v_strategy_pwod = 68 THEN
            v_parm_val_key := 'PWOD'||'-'||'029'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_pwod := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'PWOD'||'-'||'029'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_pwod := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_pwod := 0;
            ELSE
               v_fail_prob_step_pwod := (v_FAIL_PROB_HIGH_pwod - v_FAIL_PROB_LOW_pwod) / (v_run_years - 1);
            END IF;
         ELSIF v_strategy_pwod = 71 THEN
            v_parm_val_key := 'PWOD'||'-'||'015'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_pwod := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'PWOD'||'-'||'015'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_pwod := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_pwod := 0;
            ELSE
               v_fail_prob_step_pwod := (v_FAIL_PROB_HIGH_pwod - v_FAIL_PROB_LOW_pwod) / (v_run_years - 1);
            END IF;
         END IF;
         IF v_strategy_dstr = 14 THEN
            v_parm_val_key := 'DSTR'||'-'||'010'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_dstr := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'DSTR'||'-'||'010'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_dstr := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_dstr := 0;
            ELSE
               v_fail_prob_step_dstr := (v_FAIL_PROB_HIGH_dstr - v_FAIL_PROB_LOW_dstr) / (v_run_years - 1);
            END IF;
         ELSIF v_strategy_dstr = 36 THEN
            v_parm_val_key := 'DSTR'||'-'||'001'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('DSTR', v_fsrtg);
         ELSIF v_strategy_dstr = 47 THEN
            v_parm_val_key := 'DSTR'||'-'||'002'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_dstr := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'DSTR'||'-'||'002'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_dstr := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_dstr := 0;
            ELSE
               v_fail_prob_step_dstr := (v_FAIL_PROB_HIGH_dstr - v_FAIL_PROB_LOW_dstr) / (v_run_years - 1);
            END IF;
         END IF;
         IF v_strategy_dof = 15 THEN
            v_parm_val_key := 'DOF'||'-'||'009'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_dof  := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'DOF'||'-'||'009'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_dof  := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_dof := 0;
            ELSE
               v_fail_prob_step_dof := (v_FAIL_PROB_HIGH_dof - v_FAIL_PROB_LOW_dof) / (v_run_years - 1);
            END IF;
         ELSIF v_strategy_dof = 37 THEN
            v_parm_val_key := 'DOF'||'-'||'001'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('DOF', v_fsrtg);
         ELSIF v_strategy_dof = 48 THEN
            v_parm_val_key := 'DOF'||'-'||'003'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_dof  := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'DOF'||'-'||'003'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_dof  := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_dof := 0;
            ELSE
               v_fail_prob_step_dof := (v_FAIL_PROB_HIGH_dof - v_FAIL_PROB_LOW_dof) / (v_run_years - 1);
            END IF;
         END IF;
         IF v_strategy_recl = 16 THEN
            v_parm_val_key := 'RECL'||'-'||'008'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_recl := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'RECL'||'-'||'008'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_recl := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_recl := 0;
            ELSE
               v_fail_prob_step_recl := (v_FAIL_PROB_HIGH_recl - v_FAIL_PROB_LOW_recl) / (v_run_years - 1);
            END IF;
         ELSIF v_strategy_recl = 38 THEN
            v_parm_val_key := 'RECL'||'-'||'001'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('RECL', v_fsrtg);
         ELSIF v_strategy_recl = 49 THEN
            v_parm_val_key := 'RECL'||'-'||'003'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_recl := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'RECL'||'-'||'003'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_recl := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_recl := 0;
            ELSE
               v_fail_prob_step_recl := (v_FAIL_PROB_HIGH_recl - v_FAIL_PROB_LOW_recl) / (v_run_years - 1);
            END IF;
         END IF;
         IF v_strategy_sect = 17 THEN
            v_parm_val_key := 'SECT'||'-'||'006'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_sect := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'SECT'||'-'||'006'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_sect := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_sect := 0;
            ELSE
               v_fail_prob_step_sect := (v_FAIL_PROB_HIGH_sect - v_FAIL_PROB_LOW_sect) / (v_run_years - 1);
            END IF;
         ELSIF v_strategy_sect = 39 THEN
            v_parm_val_key := 'SECT'||'-'||'001'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('SECT', v_fsrtg);
         ELSIF v_strategy_sect = 50 THEN
            v_parm_val_key := 'SECT'||'-'||'002'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_sect := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'SECT'||'-'||'002'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_sect := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_sect := 0;
            ELSE
               v_fail_prob_step_sect := (v_FAIL_PROB_HIGH_sect - v_FAIL_PROB_LOW_sect) / (v_run_years - 1);
            END IF;
         END IF;
         IF v_strategy_ptsd = 18 THEN
            v_parm_val_key := 'PTSD'||'-'||'010'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_ptsd := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'PTSD'||'-'||'010'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_ptsd := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_ptsd := 0;
            ELSE
               v_fail_prob_step_ptsd := (v_FAIL_PROB_HIGH_ptsd - v_FAIL_PROB_LOW_ptsd) / (v_run_years - 1);
            END IF;
         ELSIF v_strategy_ptsd = 40 THEN
            v_parm_val_key := 'PTSD'||'-'||'001'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('PTSD', v_fsrtg);
         ELSIF v_strategy_ptsd = 51 THEN
            v_parm_val_key := 'PTSD'||'-'||'002'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_ptsd := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'PTSD'||'-'||'002'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_ptsd := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_ptsd := 0;
            ELSE
               v_fail_prob_step_ptsd := (v_FAIL_PROB_HIGH_ptsd - v_FAIL_PROB_LOW_ptsd) / (v_run_years - 1);
            END IF;
         END IF;
         IF v_strategy_capb = 41 THEN
            v_parm_val_key := 'CAPB'||'-'||'001'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('CAPB', v_fsrtg);
         END IF;
         IF v_strategy_reac = 42 THEN
            v_parm_val_key := 'REAC'||'-'||'001'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('REAC', v_fsrtg);
         END IF;
         IF v_strategy_lbs = 43 THEN
            v_parm_val_key := 'LBS'||'-'||'001'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('LBS', v_fsrtg);
         END IF;
         IF v_strategy_rgtr = 23 THEN
            v_parm_val_key := 'RGTR'||'-'||'006'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_rgtr := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'RGTR'||'-'||'006'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_rgtr := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_rgtr := 0;
            ELSE
               v_fail_prob_step_rgtr := (v_FAIL_PROB_HIGH_rgtr - v_FAIL_PROB_LOW_rgtr) / (v_run_years - 1);
            END IF;
         ELSIF v_strategy_rgtr = 44 THEN
            v_parm_val_key := 'RGTR'||'-'||'001'||'-'||'FSRTG';
            v_fsrtg    := v_cond_parm_tab(v_parm_val_key);
            /* load the applicable combos of fire rating and safety zone into an assoc array */
            load_fsrtg_table('RGTR', v_fsrtg);
         ELSIF v_strategy_rgtr = 55 THEN
            v_parm_val_key := 'RGTR'||'-'||'002'||'-'||'FAIL_PROB_HIGH';
            v_FAIL_PROB_HIGH_rgtr := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            v_parm_val_key := 'RGTR'||'-'||'002'||'-'||'FAIL_PROB_LOW';
            v_FAIL_PROB_LOW_rgtr := TO_NUMBER(v_cond_parm_tab(v_parm_val_key));
            IF v_run_years = 1 THEN
               v_fail_prob_step_rgtr := 0;
            ELSE
               v_fail_prob_step_rgtr := (v_FAIL_PROB_HIGH_rgtr - v_FAIL_PROB_LOW_rgtr) / (v_run_years - 1);
            END IF;
         END IF;
         
         v_failure_noaction_prob_tab.DELETE;
         FOR c IN failure_noaction_prob_csr LOOP
            v_failure_noaction_prob_tab(c.yr||c.pick_id) := c.fail_prob;
         END LOOP;
         
         /* get and store all MTZN scheduled for this year */
--         FOR c IN mtzn_sched_csr LOOP               --pickid init
--            v_mtzn_sched_tab(c.mtzn) := ' ';
--         END LOOP;

         /* get and store all MTZN from mtzn_priority table */
         FOR c IN mtzn_priority_csr LOOP
            v_mtzn_priority_tab(c.mtzn) := ' ';
         END LOOP;
            
         DELETE FROM structools.st_temp_bay_repl;
         COMMIT;
                   
      END initialise;
      
      PROCEDURE validate_run_request (i_run_id IN INTEGER) IS
         
         v_request_not_valid     EXCEPTION;
         v_run_ts                DATE;
         v_strategy_set_id       PLS_INTEGER;
         v_wrong_strategy_cnt    PLS_INTEGER;
      
      BEGIN

         SELECT      run_years
                     ,treatment_type
                     ,run_id
                     ,first_year
                     ,run_ts
                     ,strategy_set_id
                     ,upgrade_poles_under_cond
                     ,suppress_pickids
                     ,suppress_pickids_ver
            INTO       v_run_years
                      ,v_treatment_type
                      ,v_run_id
                      ,v_first_year
                      ,v_run_ts
                      ,v_strategy_set_id
                      ,v_upgrade_poles_under_cond
                      ,v_suppress_pickids
                      ,v_suppress_pickids_prog_id
            FROM       structools.st_run_request    
            WHERE      run_id = i_run_id 
            ;

         dbms_output.put_line('processing request = '|| i_run_id||' '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
         dbms_output.put_line('v_run_id = '||v_run_id);
         dbms_output.put_line('v_treatment_type = '||v_treatment_type);
         dbms_output.put_line('v_first_year := '||v_first_year);
         dbms_output.put_line('v_run_years := '||v_run_years);
         
         IF v_run_ts IS NOT NULL THEN
            dbms_output.put_line('Run request '||i_run_id||' has run_ts NOT NULL');
            RAISE v_request_not_valid;
         END IF;
            
         v_run_years_char := LTRIM(TO_CHAR(v_run_years, '00'));
         v_last_year     := v_first_year - 1 + v_run_years; 

         IF v_run_id IS NULL OR v_run_years IS NULL OR v_treatment_type IS NULL OR v_upgrade_poles_under_cond IS NULL OR v_suppress_pickids IS NULL
         --OR v_mtzn_committed_version_id IS NULL         --pickid init 
         OR v_first_year IS NULL 
         OR v_run_id != i_run_id THEN
            dbms_output.put_line('Run request '||i_run_id||' has missing PARAMETERS IN st_run_request');
            RAISE v_request_not_valid;
         END IF;
         
         IF v_treatment_type != 'FULL' AND v_treatment_type != 'PARTIAL' THEN
            dbms_output.put_line('Run request '||i_run_id||' treatment TYPE IS NOT valid');
            RAISE v_request_not_valid;
         END IF;
         
         IF v_upgrade_poles_under_cond != 'Y' AND v_upgrade_poles_under_cond != 'N' THEN
            dbms_output.put_line('Run request '||i_run_id||' upgrade_poles_under_cond IS NOT valid');
            RAISE v_request_not_valid;
         END IF;
         
         IF v_suppress_pickids != 'Y' AND v_suppress_pickids != 'N' THEN
            dbms_output.put_line('Run request '||i_run_id||' suppress_pickids IS NOT valid');
            RAISE v_request_not_valid;
         END IF;
         
         IF v_suppress_pickids = 'Y' AND v_suppress_pickids_prog_id IS NULL THEN
            dbms_output.put_line('Run request '||i_run_id||' suppress_pickids_ver must NOT be NULL');
            RAISE v_request_not_valid;
         END IF;
         
         IF v_suppress_pickids_prog_id IS NULL THEN
         	v_suppress_pickids_prog_id := '-1';
			END IF;            
         
         IF v_first_year <= v_this_year THEN
            dbms_output.put_line('Run request '||i_run_id||' - first_year must be > '||v_this_year);
            RAISE v_request_not_valid;
         END IF;
            
--         IF v_first_year = v_this_year + 1 THEN
--         7   IF v_parent_run_program_id IS NOT NULL OR v_parent_run_id IS NOT NULL THEN
--               dbms_output.put_line('Run request '||i_run_id||' - program_id AND parent_run_id must be NULL FOR INITIAL run');
--               RAISE v_request_not_valid;
--            END IF;
--         END IF;
            
--         IF v_first_year > v_this_year + 1 THEN
--            IF v_parent_run_program_id IS NULL OR v_parent_run_id IS NULL THEN
--               dbms_output.put_line('Run request '||i_run_id||' - program_id AND parent_run_id must NOT be NULL FOR run continuation');
--               RAISE v_request_not_valid;
--            END IF;
--         END IF;
         
--         IF v_parent_run_id IS NOT NULL THEN
--            BEGIN
--               SELECT DISTINCT run_years 
--               INTO v_parent_run_yrs 
--               FROM structools.st_run_request
--               WHERE run_id BETWEEN v_parent_run_id AND v_run_id - 1;
--               IF v_parent_run_yrs != v_run_years THEN 
--                  dbms_output.put_line('Run request '||i_run_id||' - this run AND ALL PARENT runs must ALL have same run_years');
--                  RAISE v_request_not_valid;
--               END IF;
--               SELECT SUM(run_years)         
--               INTO v_run_years_sum 
--               FROM structools.st_run_request
--               WHERE run_id BETWEEN v_parent_run_id AND v_run_id;
--            EXCEPTION
--               WHEN TOO_MANY_ROWS THEN
--                  dbms_output.put_line('Run request '||i_run_id||' - PARENT runs must ALL have same run_years');
--                  RAISE v_request_not_valid;
--            END;
--            BEGIN
--               SELECT run_id
--               INTO   v_dummy_int
--               FROM structools.st_run_request
--               WHERE run_id = v_run_id - 1
--               AND first_year + run_years = v_first_year;
--            EXCEPTION
--               WHEN NO_DATA_FOUND THEN
--                  dbms_output.put_line('Run request '||i_run_id||' - first_year must be CONSISTENT WITH PREVIOUS run');
--                  RAISE v_request_not_valid;
--            END;
--            BEGIN
--               SELECT DISTINCT parent_run_id 
--               INTO v_parent_run_id2
--               FROM structools.st_run_request
--               WHERE run_id BETWEEN v_parent_run_id + 1 AND v_run_id; -- the first run will not have a parent
--            EXCEPTION
--               WHEN TOO_MANY_ROWS THEN
--                  dbms_output.put_line('Run request '||i_run_id||' - ALL related runs must have same PARENT run_id');
--                  RAISE v_request_not_valid;
--            END;
--            BEGIN
--               SELECT DISTINCT parent_run_program_id 
--               INTO v_parent_run_program_id2
--               FROM structools.st_run_request
--               WHERE run_id BETWEEN v_parent_run_id + 1 AND v_run_id; -- the first run will not have a parent
--            EXCEPTION
--               WHEN TOO_MANY_ROWS THEN
--                  dbms_output.put_line('Run request '||i_run_id||' - ALL related runs must have same PARENT program_id');
--                  RAISE v_request_not_valid;
--            END;
--         ELSE            
            v_run_years_sum := v_run_years;               
--         END IF;
         dbms_output.put_line('v_run_years_sum := '||v_run_years_sum);
         v_run_years_sum_char := LTRIM(TO_CHAR(v_run_years_sum, '00'));
         
         IF v_strategy_set_id IS NULL THEN
            dbms_output.put_line('Run request strategy_set_id must NOT be NULL');
            RAISE v_request_not_valid;
         END IF;
         
         /* Check that run strategies all belong to the strategy set */
         WITH strategy_set_strategies AS
         (
         SELECT strategy_id
         FROM   structools.st_strategy
         WHERE strategy_set_id = v_strategy_set_id
         )
         SELECT COUNT(*)
            INTO v_wrong_strategy_cnt
         FROM
         (
         SELECT A.strategy_id AS strategy_1
               ,b.strategy_id AS strategy_2
         FROM   structools.st_run_request_strategy A  LEFT OUTER JOIN
                strategy_set_strategies         b
         ON A.strategy_id = b.strategy_id    
         WHERE A.run_id = i_run_id
         ) aa
         WHERE aa.strategy_2 IS NULL
         ; 
         IF v_wrong_strategy_cnt > 0 THEN  
            dbms_output.put_line('Run request strategy must be part OF strategy set '||v_strategy_set_id);
            RAISE v_request_not_valid;
         END IF;
         
--         IF v_parent_run_id IS NULL THEN
--            v_parent_run_id_first := -1;
--            v_parent_run_id_last := -1;
--         ELSE
--            v_parent_run_id_first := v_parent_run_id;
--            v_parent_run_id_last  := v_run_id - 1;
--         END IF;
--         
--         IF v_parent_run_program_id IS NULL THEN
--            v_parent_run_program_id := -1;
--         END IF;
         
         IF v_first_year > v_this_year + 1 THEN
            v_year_0 := v_first_year - 1;
         ELSE
            v_year_0 := v_this_year;
         END IF;
         
         /* get strategies for the request and store in a associative array, index by egi, including applicable parameters for the scenario */
         
--         dbms_output.put_line ('Strategies AND parameter VALUES');      
         
         v_cond_parm_tab.DELETE;   
         v_bay_strategy := FALSE;
         
         FOR i IN (
            WITH parms AS 
            (
            SELECT b.strategy_id
                  ,b.strategy_condition_seq
                  ,b.strategy_condition_parm_name
                  ,b.strategy_condition_parm_val
            FROM  structools.st_run_request_strategy    A INNER JOIN
                  structools.st_strategy_scenario       b
            ON    A.strategy_id = b.strategy_id
            AND   A.scenario_id = b.scenario_id   
                                             INNER JOIN
                  structools.st_strategy_condition_parm    c
            ON    b.strategy_id = c.strategy_id
            AND   b.strategy_condition_seq = c.strategy_condition_seq
            AND   b.strategy_condition_parm_name = c.strategy_condition_parm_name
            WHERE A.run_id = i_run_id
            )
            SELECT A.strategy_id
                  ,b.equip_grp_id
                  ,A.scenario_id
                  ,c.strategy_condition_seq
                  ,c.strategy_condition_descr
                  ,c.event_reason
                  ,parms.strategy_condition_parm_name
                  ,parms.strategy_condition_parm_val
            FROM  structools.st_run_request_strategy A  INNER JOIN
                  structools.st_strategy             b
            ON    A.strategy_id = b.strategy_id            
                                             INNER JOIN
                  structools.st_strategy_condition         c
            ON    A.strategy_id = c.strategy_id
                                             INNER JOIN
                  structools.st_run_request                d
            ON    A.run_id = d.run_id
                                             LEFT OUTER JOIN
                  parms
            ON    c.strategy_id = parms.strategy_id                                       
            AND   c.strategy_condition_seq = parms.strategy_condition_seq
            WHERE d.run_id = i_run_id
            AND   b.strategy_set_id = v_strategy_set_id
            ORDER BY A.strategy_id, c.strategy_condition_seq) 
         LOOP
            IF i.equip_grp_id = 'BAY' THEN
               v_egi_vc2 := TRIM(i.equip_grp_id);
               v_bay_strategy := TRUE;
               v_strategy_bay := i.strategy_id;
               IF i.event_reason IS NOT NULL THEN
                  v_cond_key := v_egi_vc2||'-'||LTRIM(TO_CHAR(i.strategy_condition_seq,'000'));
                  v_cond_tab(v_cond_key) := i.event_reason;
               END IF;
               IF i.strategy_condition_parm_name IS NOT NULL THEN 
                  IF i.strategy_condition_parm_val IS NULL THEN
                     dbms_output.put_line ('VALUE NOT provided FOR parameter '||i.strategy_condition_parm_name
                                                ||' FOR strategy FOR '||i.equip_grp_id||' FOR condition "'
                                                ||i.strategy_condition_descr);
                     RAISE v_request_not_valid;
                  ELSE
                     /* save parameters in table v_cond_parm_tab, indexed by concatenation of egi+cond_seq+cond_name */
                     v_parm_val_key := v_egi_vc2||'-'||LTRIM(TO_CHAR(i.strategy_condition_seq,'000'))||'-'||TRIM(i.strategy_condition_parm_name);
                     v_cond_parm_tab(v_parm_val_key) := i.strategy_condition_parm_val;
                  END IF;
               END IF;
            ELSE
               v_pole_eqp_strategy := TRUE;
               IF i.equip_grp_id = 'PWOD' THEN
                  v_strategy_pwod := i.strategy_id;
               ELSIF i.equip_grp_id = 'AGOST' THEN
                  v_strategy_agost := i.strategy_id;
               ELSIF i.equip_grp_id = 'ASTAY' THEN
                  v_strategy_astay := i.strategy_id;
               ELSIF i.equip_grp_id = 'GSTAY' THEN
                  v_strategy_gstay := i.strategy_id;
               ELSIF i.equip_grp_id = 'OSTAY' THEN
                  v_strategy_ostay := i.strategy_id;
               ELSIF i.equip_grp_id = 'HVXM' THEN
                  v_strategy_hvxm := i.strategy_id;
               ELSIF i.equip_grp_id = 'LVXM' THEN
                  v_strategy_lvxm := i.strategy_id;
               ELSIF i.equip_grp_id = 'PAUS' THEN
                  v_strategy_paus := i.strategy_id;
               ELSIF i.equip_grp_id = 'CAPB' THEN
                  v_strategy_capb := i.strategy_id;
               ELSIF i.equip_grp_id = 'REAC' THEN
                  v_strategy_reac := i.strategy_id;
               ELSIF i.equip_grp_id = 'DOF' THEN
                  v_strategy_dof := i.strategy_id;
               ELSIF i.equip_grp_id = 'DSTR' THEN
                  v_strategy_dstr := i.strategy_id;
               ELSIF i.equip_grp_id = 'LBS' THEN
                  v_strategy_lbs := i.strategy_id;
               ELSIF i.equip_grp_id = 'PTSD' THEN
                  v_strategy_ptsd := i.strategy_id;
               ELSIF i.equip_grp_id = 'RECL' THEN
                  v_strategy_recl := i.strategy_id;
               ELSIF i.equip_grp_id = 'RGTR' THEN
                  v_strategy_rgtr := i.strategy_id;
               ELSIF i.equip_grp_id = 'SECT' THEN
                  v_strategy_sect := i.strategy_id;
               END IF;
               v_egi_tab(i.equip_grp_id) := ' ';
               v_egi_vc2 := TRIM(i.equip_grp_id);
               IF i.event_reason IS NOT NULL THEN
                  v_cond_key := v_egi_vc2||'-'||LTRIM(TO_CHAR(i.strategy_condition_seq,'000'));
                  v_cond_tab(v_cond_key) := i.event_reason;
               END IF;
               IF i.strategy_condition_parm_name IS NOT NULL THEN 
                  IF i.strategy_condition_parm_val IS NOT NULL THEN
                     /* save parameters in table v_cond_parm_tab, indexed by concatenation of egi+cond_seq+cond_name */
                     v_parm_val_key := v_egi_vc2||'-'||LTRIM(TO_CHAR(i.strategy_condition_seq,'000'))||'-'||i.strategy_condition_parm_name;
                     v_cond_parm_tab(v_parm_val_key) := i.strategy_condition_parm_val;
                  END IF;
               END IF;
            END IF;
            
            dbms_output.put_line (i.strategy_id||','||i.equip_grp_id||','||i.scenario_id||','||i.strategy_condition_seq
                                     ||','||i.strategy_condition_descr||','||i.event_reason||','||i.strategy_condition_parm_name
                                     ||','||i.strategy_condition_parm_val);
                     
         END LOOP;
         
--         --graz
--         dbms_output.put_line('v_cond_par_tab display');
--         v_parm_val_key := v_cond_parm_tab.FIRST;
--         WHILE v_parm_val_key IS NOT NULL LOOP
--            dbms_output.put_line(v_parm_val_key);
--            v_parm_val_key := v_cond_parm_tab.NEXT(v_parm_val_key);
--         END LOOP;
--
--         --graz
--         dbms_output.put_line('v_cond_tab display');
--         v_cond_key := v_cond_tab.FIRST;
--         WHILE v_cond_key IS NOT NULL LOOP
--            dbms_output.put_line(v_cond_key||' '||v_cond_tab(v_cond_key));
--            v_cond_key := v_cond_tab.NEXT(v_cond_key);
--         END LOOP;

         IF v_bay_strategy THEN
            BEGIN
               v_dummy_int := NULL;
               SELECT run_id
               INTO        v_dummy_int
               FROM structools.st_bay_strategy_run
               WHERE run_id = v_run_id 
               AND ROWNUM = 1;   
               dbms_output.put_line('Run ID '||i_run_id||' has DATA already IN st_bay_strategy_run');
               RAISE v_request_not_valid;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  NULL;
            END;
         END IF;
         
         IF v_pole_eqp_strategy THEN
            BEGIN
               v_dummy_int := NULL;
               SELECT run_id
               INTO        v_dummy_int
               FROM structools.st_strategy_run
               WHERE run_id = v_run_id
               AND ROWNUM = 1;
               dbms_output.put_line('Run ID '||i_run_id||' has DATA already IN st_strategy_run');
               RAISE v_request_not_valid;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  NULL;
            END;
         END IF;
         
      
      EXCEPTION
         WHEN v_request_not_valid THEN
            RAISE;
                                           
      END validate_run_request;
      
   /*****************************************************************/
   /* Mainline of execute_run_request procedure                     */
   /*****************************************************************/
      
   BEGIN

      UPDATE structools.st_run_request
         set start_ts = SYSDATE
      WHERE run_id = i_run_id;
         
      v_run_ts := SYSDATE;
      dbms_output.put_line('Start '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));
      
      v_bay_strategy      := FALSE;
      v_pole_eqp_strategy := FALSE;
      
      validate_run_request(i_run_id);  -- sets v_bay_strategy if a strategy for bays has been requested by this run.

      initialise;
      
      /* If a bay strategy is included in this run request, process it first, so that we can collect all affected poles in a w-s table */

      IF v_bay_strategy THEN
         dbms_output.put_line('Start processing carriers'||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));
         process_bays;
      END IF;

      COMMIT;
            
      /* Now process vicinities */
      
      IF v_pole_eqp_strategy THEN
         structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN');

         dbms_output.put_line('processing poles AND equipment '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));
         j := 0;
         v_vic_id_prev := ' ';
         v_mtzn_prev := ' ';
         
         OPEN eqp_csr;
         
         v_vic_cnt := 0;
         v_eqp_tab.DELETE;
         
         v_pole_part_risk_tab.DELETE;
         
         LOOP
         
            FETCH eqp_csr INTO v_eqp_all_rec;
            EXIT WHEN eqp_csr%NOTFOUND; -- OR v_vic_id_prev > 'LS100153'; -- OR v_vic_cnt >= 1;  
            IF v_eqp_all_rec.vic_id != v_vic_id_prev AND v_vic_id_prev != ' ' THEN
               process_vicinity(v_vic_id_prev, v_mtzn_prev);
               FOR i IN 1 .. v_eqp_tab.COUNT LOOP
                  IF v_eqp_tab(i).egi = 'PWOD' AND v_eqp_tab(i).part_cde = ' ' THEN
                     v_risk_key := v_eqp_tab(i).pick_id||'N';
                     IF v_pwod_risk_tab.EXISTS(v_risk_key) THEN
                        v_pwod_risk_tab.DELETE(v_risk_key);
                     END IF;
                     v_risk_key := v_eqp_tab(i).pick_id||'NNPV15';
                     IF v_pwod_risk_tab.EXISTS(v_risk_key) THEN
                        v_pwod_risk_tab.DELETE(v_risk_key);
                     END IF;
                     v_risk_key := v_eqp_tab(i).pick_id||'P';
                     IF v_pwod_risk_tab.EXISTS(v_risk_key) THEN
                        v_pwod_risk_tab.DELETE(v_risk_key);
                     END IF;
                     v_risk_key := v_eqp_tab(i).pick_id||'PNPV15';
                     IF v_pwod_risk_tab.EXISTS(v_risk_key) THEN
                        v_pwod_risk_tab.DELETE(v_risk_key);
                     END IF;
                     v_risk_key := v_eqp_tab(i).pick_id||'F';
                     IF v_pwod_risk_tab.EXISTS(v_risk_key) THEN
                        v_pwod_risk_tab.DELETE(v_risk_key);
                     END IF;
                     v_risk_key := v_eqp_tab(i).pick_id||'FNPV15';
                     IF v_pwod_risk_tab.EXISTS(v_risk_key) THEN
                        v_pwod_risk_tab.DELETE(v_risk_key);
                     END IF;
                  ELSE
                     v_risk_key := v_eqp_tab(i).pick_id||'N';
                     IF v_eqp_risk_tab.EXISTS(v_risk_key) THEN
                        v_eqp_risk_tab.DELETE(v_risk_key);
                     END IF;
                     v_risk_key := v_eqp_tab(i).pick_id||'NNPV15';
                     IF v_eqp_risk_tab.EXISTS(v_risk_key) THEN
                        v_eqp_risk_tab.DELETE(v_risk_key);
                     END IF;
                     v_risk_key := v_eqp_tab(i).pick_id||'P';
                     IF v_eqp_risk_tab.EXISTS(v_risk_key) THEN
                        v_eqp_risk_tab.DELETE(v_risk_key);
                     END IF;
                     v_risk_key := v_eqp_tab(i).pick_id||'PNPV15';
                     IF v_eqp_risk_tab.EXISTS(v_risk_key) THEN
                        v_eqp_risk_tab.DELETE(v_risk_key);
                     END IF;
                  END IF;
               END LOOP; 
               v_eqp_tab.DELETE;
               v_pole_part_risk_tab.DELETE;
               j := 0;
               v_vic_cnt := v_vic_cnt + 1;
            END IF;
            v_vic_id_prev := v_eqp_all_rec.vic_id;
            v_mtzn_prev := v_eqp_all_rec.maint_zone_nam;
            v_eqp_tab.EXTEND;
            j := j + 1;
            v_eqp_tab(j).pick_id  := v_eqp_all_rec.pick_id;
            v_eqp_tab(j).egi      := TRIM(v_eqp_all_rec.equip_grp_id);
            v_eqp_tab(j).instln_year := v_eqp_all_rec.instln_year;
            v_eqp_tab(j).reinf_cde     := v_eqp_all_rec.reinf_cde;
            v_eqp_tab(j).wood_type     := v_eqp_all_rec.wood_type;
            v_eqp_tab(j).recl_phases   := v_eqp_all_rec.recl_phases;
            v_eqp_tab(j).pole_diameter_mm := v_eqp_all_rec.pole_diameter_mm;
            v_eqp_tab(j).pole_complexity := v_eqp_all_rec.pole_complexity;
            v_eqp_tab(j).dstr_size       := v_eqp_all_rec.dstr_size;
            v_eqp_tab(j).del_year        := v_eqp_all_rec.del_year;
            IF v_eqp_all_rec.del_event = 'REPLACE' THEN
               v_eqp_tab(j).age           := v_year_0 - v_eqp_all_rec.del_year;
--               v_eqp_tab(j).init_age      := v_year_0 - v_eqp_all_rec.del_year;
            ELSE
               v_eqp_tab(j).age           := v_year_0 - v_eqp_all_rec.instln_year;
--               v_eqp_tab(j).init_age      := v_this_year - v_eqp_all_rec.instln_year;
            END IF;
            v_eqp_tab(j).calendar_year := v_year_0;
            v_eqp_tab(j).manuf         := v_eqp_all_rec.manuf;
            v_eqp_tab(j).stock_cde     := v_eqp_all_rec.stock_cde;
            v_eqp_tab(j).ctl_typ       := v_eqp_all_rec.ctl_typ;
            v_eqp_tab(j).dev_typ       := v_eqp_all_rec.dev_typ;
            v_eqp_tab(j).manuf_and_mdl := v_eqp_all_rec.manuf_and_mdl;
            v_eqp_tab(j).expulsion_type := v_eqp_all_rec.expulsion_type;
            v_eqp_tab(j).del_event      := v_eqp_all_rec.del_event;
            v_eqp_tab(j).non_del_pick_id        := v_eqp_all_rec.non_del_pick_id;
            v_eqp_tab(j).non_del_event          := v_eqp_all_rec.non_del_event;
            v_eqp_tab(j).non_del_event_reason   := v_eqp_all_rec.non_del_event_reason;
            v_eqp_tab(j).non_del_capex          := v_eqp_all_rec.non_del_capex;
            v_eqp_tab(j).non_del_failure_prob   := v_eqp_all_rec.non_del_failure_prob;
            v_eqp_tab(j).fsrtg                  := v_eqp_all_rec.fsrtg;
            v_eqp_tab(j).fire_risk_zone         := SUBSTR(v_eqp_all_rec.fsrtg,1,1);
            v_eqp_tab(j).pub_saf_zone           := SUBSTR(v_eqp_all_rec.fsrtg,2,10);
            v_eqp_tab(j).region                 := v_eqp_all_rec.region;
            v_eqp_tab(j).fdr_cat                := v_eqp_all_rec.fdr_cat;
            --v_eqp_tab(j).scheduled              := 'N';         --pickid init
            v_eqp_tab(j).scheduled              := v_eqp_all_rec.scheduled;
            v_eqp_tab(j).part_cde                := v_eqp_all_rec.part_cde;
            --v_eqp_tab(j).part_defect_hier        := v_eqp_all_rec.part_defect_hier;
            v_eqp_tab(j).part_defective        := v_eqp_all_rec.part_defective;
            v_eqp_tab(j).cust_ds_cnt             := v_eqp_all_rec.cust_ds_cnt;
            v_eqp_tab(j).egi_requested 			:= FALSE;
            v_eqp_tab(j).labour_hrs 					:= v_eqp_all_rec.labour_hrs;
            v_eqp_tab(j).cca_treated 					:= v_eqp_all_rec.cca_treated;
            IF v_eqp_all_rec.pickid_suppressed IS NOT NULL THEN
            	v_eqp_tab(j).pickid_suppressed  	:= v_eqp_all_rec.suppressed;
            ELSE
            	v_eqp_tab(j).pickid_suppressed  	:= 'N';
				END IF;               
            IF v_eqp_tab(j).part_cde = ' ' THEN
               IF v_egi_tab.EXISTS(TRIM(v_eqp_tab(j).egi)) THEN
                  v_eqp_tab(j).egi_requested := TRUE;
               END IF;
            ELSE
               IF v_egi_tab.EXISTS(TRIM(v_eqp_tab(j).part_cde)) THEN
                  v_eqp_tab(j).egi_requested := TRUE;
               END IF;
            END IF;
            IF v_eqp_tab(j).part_cde != ' ' AND v_eqp_tab(j).egi_requested THEN
               /* obtain risk for no action and replace for all parts, as they are all to be REPLACEd */
               v_risk_pick_id := SUBSTR(v_eqp_tab(j).pick_id, 2,8);
               IF v_eqp_tab(j).part_cde = 'ASTAY' OR v_eqp_tab(j).part_cde = 'GSTAY' OR v_eqp_tab(j).part_cde = 'OSTAY' 
               OR v_eqp_tab(j).part_cde = 'AGOST' THEN
                  BEGIN
                     SELECT A.*
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                     INTO v_risk_rec_pole_part
                     FROM structools.st_risk_stay_noaction A
                     WHERE pick_id = v_risk_pick_id;
                     store_risk_pole_part(v_eqp_tab(j).part_cde,'N');
                     SELECT A.*
                     INTO v_risk_rec_pole_part
                     FROM structools.st_risk_stay_noaction_npv15 A
                     WHERE pick_id = v_risk_pick_id;
                     store_risk_pole_part(v_eqp_tab(j).part_cde,'NNPV15');
                     SELECT A.*
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                     INTO v_risk_rec_pole_part
                     FROM structools.st_risk_stay_replace A
                     WHERE pick_id = v_risk_pick_id;
                     store_risk_pole_part(v_eqp_tab(j).part_cde,'P');
                     SELECT A.*
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                     INTO v_risk_rec_pole_part
                     FROM structools.st_risk_stay_replace_npv15 A
                     WHERE pick_id = v_risk_pick_id;
                     store_risk_pole_part(v_eqp_tab(j).part_cde,'PNPV15');
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        NULL;
                  END;
               ELSIF v_eqp_tab(j).part_cde = 'HVXM' THEN
                  BEGIN
                     SELECT A.*
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                     INTO v_risk_rec_pole_part
                     FROM structools.st_risk_xarmhvi_noaction A
                     WHERE pick_id = v_risk_pick_id;
                     store_risk_pole_part(v_eqp_tab(j).part_cde,'N');
                     SELECT A.*
                     INTO v_risk_rec_pole_part
                     FROM structools.st_risk_xarmhvi_noaction_npv15 A
                     WHERE pick_id = v_risk_pick_id;
                     store_risk_pole_part(v_eqp_tab(j).part_cde,'NNPV15');
                     SELECT A.*
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                     INTO v_risk_rec_pole_part
                     FROM structools.st_risk_xarmhvi_replace A
                     WHERE pick_id = v_risk_pick_id;
                     store_risk_pole_part(v_eqp_tab(j).part_cde,'P');
                     SELECT A.*
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                     INTO v_risk_rec_pole_part
                     FROM structools.st_risk_xarmhvi_replace_npv15 A
                     WHERE pick_id = v_risk_pick_id;
                     store_risk_pole_part(v_eqp_tab(j).part_cde,'PNPV15');
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        NULL;
                  END;
               ELSIF v_eqp_tab(j).part_cde = 'LVXM' THEN
                  BEGIN
                     SELECT A.*
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                     INTO v_risk_rec_pole_part
                     FROM structools.st_risk_xarmlvi_noaction A
                     WHERE pick_id = v_risk_pick_id;
                     store_risk_pole_part(v_eqp_tab(j).part_cde,'N');
                     SELECT A.*
                     INTO v_risk_rec_pole_part
                     FROM structools.st_risk_xarmlvi_noaction_npv15 A
                     WHERE pick_id = v_risk_pick_id;
                     store_risk_pole_part(v_eqp_tab(j).part_cde,'NNPV15');
                     SELECT A.*
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                     INTO v_risk_rec_pole_part
                     FROM structools.st_risk_xarmlvi_replace A
                     WHERE pick_id = v_risk_pick_id;
                     store_risk_pole_part(v_eqp_tab(j).part_cde,'P');
                     SELECT A.*
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                     INTO v_risk_rec_pole_part
                     FROM structools.st_risk_xarmlvi_replace_npv15 A
                     WHERE pick_id = v_risk_pick_id;
                     store_risk_pole_part(v_eqp_tab(j).part_cde,'PNPV15');
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        NULL;
                  END;
               END IF;
            END IF;
         END LOOP;
         
         process_vicinity(v_vic_id_prev, v_mtzn_prev);
         v_eqp_tab.DELETE;

         CLOSE eqp_csr;
         /* recreate indexes for st_strategy_run */
         structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN');
         analyse_table('ST_STRATEGY_RUN');
         
      END IF;
      
      dbms_output.put_line('END '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));
      
      
      UPDATE structools.st_run_request
         SET  run_ts = SYSDATE
         WHERE run_id = i_run_id
      ;       
      
      --ROLLBACK;       --graz         
      COMMIT;
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line('v_vic_id_prev = '||v_vic_id_prev);
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END execute_run_request;

-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
   /**************************************************************************************************************/
   /* This procedure optimises events and timing of them.                                                         */
   /* It calls 3 procedures, which are exposed rather than private procedures, so that they can be invoked        */
   /* separately if required.                                                                                     */     
   /**************************************************************************************************************/ 
   PROCEDURE optimise_original_selection (i_run_id IN PLS_INTEGER) IS

      v_treatment_type              VARCHAR2(20);
      v_upgrade_poles_under_cond    CHAR(1);
      v_dummy_int          PLS_INTEGER;    

   
   BEGIN

      SELECT --first_year
            --,run_years
             treatment_type
            ,upgrade_poles_under_cond
      INTO   --v_first_year
            --,v_run_years
             v_treatment_type
            ,v_upgrade_poles_under_cond
      FROM structools.st_run_request
      WHERE run_id = i_run_id;
      
      optimise_for_segment_rebuild(i_run_id);

      /* Make sure we are not accidentally duplicating data in st_bay_Strategy_run_rb and st_strategy_run_rb tables */
      BEGIN
         v_dummy_int := NULL;
         SELECT run_id
         INTO   v_dummy_int
         FROM structools.st_bay_strategy_run_rb
         WHERE run_id = i_run_id    --since we now always set run_id to run_id. Make sure we are not duplicating data.
         AND ROWNUM = 1;
         dbms_output.put_line('Run ID '||i_run_id||' has DATA already IN st_bay_strategy_run_rb');
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;

      BEGIN         
         SELECT run_id
         INTO   v_dummy_int
         FROM structools.st_strategy_run_rb
         WHERE run_id = i_run_id
         AND ROWNUM = 1;
         dbms_output.put_line('Run ID '||i_run_id||' has DATA already IN st_strategy_run_rb');
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
      
      IF v_treatment_type = 'PARTIAL' THEN
         adjust_for_partial_treatment(i_run_id);
      ELSE
      	structools.schema_utils.disable_table_indexes('ST_BAY_STRATEGY_RUN_RB');
      	structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN_RB');

         copy_strategy_run_tmp_to_rb(i_run_id);

      	structools.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN_RB');
      	structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN_RB');
         analyse_table('ST_BAY_STRATEGY_RUN_RB');
         analyse_table('ST_STRATEGY_RUN_RB');

         UPDATE structools.st_run_request
            SET  run_ts3 = SYSDATE
            WHERE run_id = i_run_id;
         COMMIT;
      END IF;
      dbms_output.put_line('adjust_for_partial_treatment END '||TO_CHAR(SYSDATE,'hh24:mi:ss'));

      /* Make sure we are not accidentally duplicating data in st_Strategy_run_rb2 table */
      BEGIN         
         SELECT run_id
         INTO   v_dummy_int
         FROM structools.st_strategy_run_rb2
         WHERE run_id = i_run_id
         AND ROWNUM = 1;
         dbms_output.put_line('Run ID '||i_run_id||' has DATA already IN st_strategy_run_rb2');
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;

      IF v_upgrade_poles_under_cond = 'Y' THEN
         upgrade_poles_under_cond(i_run_id);
      ELSE
         structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN_RB2');
         copy_st_strategy_run_rb_to_rb2(i_run_id);
         structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN_RB2');
         analyse_table('ST_STRATEGY_RUN_RB2');

         UPDATE structools.st_run_request
            SET  run_ts4 = SYSDATE
            WHERE run_id = i_run_id;
         COMMIT;
      END IF;
      dbms_output.put_line('v_upgrade_poles_under_cond END '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
      
      dbms_output.put_line('optimise_original_selection END '||TO_CHAR(SYSDATE,'hh24:mi:ss'));

   EXCEPTION

      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END optimise_original_selection;
   
   PROCEDURE optimise_for_segment_rebuild (i_run_id IN PLS_INTEGER) IS

      v_run_years                PLS_INTEGER;
      v_first_year                PLS_INTEGER;
      v_last_year                PLS_INTEGER;
      v_dummy_int                PLS_INTEGER;
      v_seg_id_prev              VARCHAR2(30);
      v_pick_id_prev             VARCHAR2(9);
      v_part_cde_prev            VARCHAR2(10);
      v_cons_segment_id          VARCHAR2(30);
      v_cons_pcnt_thrshld        PLS_INTEGER;
      v_cons_min_len             PLS_INTEGER;
      v_vic_id_prev              VARCHAR2(10);
      v_rebuild                  BOOLEAN;
      K                          PLS_INTEGER;
      v_seg_rebuild_yr           PLS_INTEGER;
      v_cons_seg_mtzn            VARCHAR2(100);

      v_request_not_valid        EXCEPTION;

      CURSOR seg_rebuild_csr IS
   
         WITH lv_seg_len AS
         (
         SELECT aa.cons_segment_id
               ,SUM(aa.span_len) AS seg_len
         FROM
         (   
         SELECT DISTINCT A.cons_segment_id
               ,A.vic_id
               ,A.span_len
         FROM structools.st_bay_strategy_run  A INNER JOIN
              structools.st_bay_equipment      b
         ON A.cons_segment_id = 'N'||ntwk_id      
         WHERE A.run_id = i_run_id
         AND A.calendar_year = v_last_year
         AND A.pickid_suppressed = 'N'
         AND b.swsect_net_typ = 'LV'
         ) aa
         GROUP BY aa.cons_segment_id
         )
         ,
         hv_seg_len AS
         (
         SELECT aa.cons_segment_id
               ,SUM(aa.span_len) AS seg_len
         FROM
         (   
         SELECT DISTINCT A.cons_segment_id
               ,A.vic_id
               ,A.span_len
         FROM structools.st_bay_strategy_run  A INNER JOIN
              structools.st_bay_equipment      b
         ON A.cons_segment_id = b.segment_id      
         WHERE A.run_id = i_run_id
         AND A.calendar_year = v_last_year
         AND A.pickid_suppressed = 'N'
         AND b.swsect_net_typ = 'HV'
         ) aa
         GROUP BY aa.cons_segment_id
         )
         ,
         segment_len AS
         (
         SELECT * FROM lv_seg_len
         UNION ALL
         SELECT * FROM hv_seg_len
         WHERE seg_len > 0         --cover the few cases of bad data, eg bay between pole-1 and pole-1
         )
         ,
         segment_repl_len AS
         (
         SELECT aa.cons_segment_id
               ,SUM(span_len) AS seg_repl_len
         FROM
         (      
         SELECT DISTINCT cons_segment_id
                        ,vic_id        AS span_id
                        ,span_len
         FROM structools.st_bay_strategy_run
         WHERE run_id      = i_run_id
         AND calendar_year = v_last_year  --_seg_rb
         AND event         = 'REPLACE'
         AND cons_segment_id > ' ' --IS NOT NULL
         AND pickid_suppressed = 'N'
--         AND risk_reduction_npv15 > 0
--         AND scheduled = 'N'
         ) aa
         GROUP BY aa.cons_segment_id
         )
         ,
         selected_segments AS
         (
         SELECT aa.cons_segment_id
               ,aa.seg_repl_pcnt
               ,aa.seg_repl_len
               ,aa.seg_len
         FROM      
         (
         SELECT A.cons_segment_id
               ,A.seg_repl_len
               ,b.seg_len
               ,CASE 
                  WHEN seg_len = 0               THEN  0
                  WHEN seg_repl_len = seg_len   THEN  100
                  ELSE ROUND((seg_repl_len / seg_len) * 100)
               END         AS seg_repl_pcnt
         FROM segment_repl_len                   A    INNER JOIN 
              segment_len                        b
         ON A.cons_segment_id = b.cons_segment_id  
         ) aa
         WHERE aa.seg_repl_pcnt >= v_cons_pcnt_thrshld
         --OR   aa.seg_repl_len >= v_cons_min_len
         )
         ,
         selected_bays AS
         (
         SELECT A.pick_id
               ,A.cons_segment_id
               ,A.cons_segment_mtzn
               ,b.seg_repl_len
               ,b.seg_len
               ,MIN(A.calendar_year)  AS bay_repl_yr
         FROM structools.st_bay_strategy_run     A  INNER JOIN
              selected_segments        b
         ON A.cons_segment_id = b.cons_segment_id
         WHERE A.run_id = i_run_id
         AND   A.event > ' ' --IS NOT NULL
         AND A.pickid_suppressed ='N'
--         AND   A.scheduled = 'N'
         --AND calendar_year BETWEEN (v_year_0_seg_rb + 1) AND v_last_year_seg_rb
         GROUP BY A.pick_id, A.cons_segment_id , A.cons_segment_mtzn, b.seg_repl_len, b.seg_len
         )
         ,
         selected_bays_npr AS
         (
         SELECT A.pick_id        
               --,(A.bay_repl_yr - v_year_0_seg_rb) AS repl_yr_idx
               ,(A.bay_repl_yr - v_year_0) AS repl_yr_idx
               ,A.cons_segment_id
               ,A.cons_segment_mtzn
               ,b.risk_reduction_npv15
               ,A.seg_repl_len
               ,A.seg_len
               --,MIN(A.bay_repl_yr - v_year_0_seg_rb) OVER (PARTITION BY A.cons_segment_mtzn) AS seg_repl_idx_min
               ,MIN(A.bay_repl_yr - v_year_0) OVER (PARTITION BY A.cons_segment_mtzn) AS seg_repl_idx_min
         FROM selected_bays         A        INNER JOIN
              structools.st_bay_strategy_run   b
         ON A.pick_id = b.pick_id
         AND A.bay_repl_yr = b.calendar_year
         WHERE b.run_id = i_run_id
         )
         ,
         segment_yr_risk_red AS
         (
         SELECT cons_segment_id 
               ,cons_segment_mtzn
               ,repl_yr_idx
               ,seg_repl_len
               ,seg_len
               ,seg_repl_idx_min
               ,SUM(risk_reduction_npv15) AS seg_yr_risk_red
         FROM selected_bays_npr
         GROUP BY cons_segment_id
                 ,cons_segment_mtzn
                 ,repl_yr_idx
                 ,seg_repl_len
                 ,seg_len
                 ,seg_repl_idx_min
         ) 
         ,
         segment_risk_red_weight AS
         (
         SELECT cons_segment_id 
                     ,cons_segment_mtzn
               ,repl_yr_idx * seg_yr_risk_red AS seg_risk_red_weight
         FROM segment_yr_risk_red
         )                 
         ,
         segment_sum_risk_red AS
         (
         SELECT cons_segment_id 
               ,cons_segment_mtzn
               ,seg_repl_len
               ,seg_len
               ,seg_repl_idx_min
               ,SUM(seg_yr_risk_red)  AS seg_sum_risk_red
         FROM segment_yr_risk_red
         GROUP BY cons_segment_id
                 ,cons_segment_mtzn
                 ,seg_repl_len
                 ,seg_len
                 ,seg_repl_idx_min
         )                 
         ,
         segment_sum_risk_red_weight AS
         (
         SELECT cons_segment_id 
               ,cons_segment_mtzn
               ,SUM(seg_risk_red_weight) AS seg_sum_risk_red_weight
         FROM segment_risk_red_weight
         GROUP BY cons_segment_id, cons_segment_mtzn
         )
         ,
         seg_prev_period AS
         (
         SELECT DISTINCT cons_segment_id
                        ,segment_rebuild_yr
         FROM  structools.st_bay_strategy_run_rb_tmp      
         WHERE run_id = i_run_id   
         AND   segment_rebuild_yr IS NOT NULL
         AND   calendar_year = v_year_0   --_seg_rb             
         )
         SELECT A.cons_segment_id            AS seg_id                 
               ,A.cons_segment_mtzn          AS seg_mtzn
               ,CASE
                  WHEN c.cons_segment_id IS NOT NULL THEN   1
                  WHEN b.seg_sum_risk_red = 0      THEN       ROUND((b.seg_repl_idx_min + v_last_year - v_year_0) / 2)
                  ELSE                                      ROUND(A.seg_sum_risk_red_weight / b.seg_sum_risk_red) 
                END                       AS rebuild_yr
         FROM segment_sum_risk_red_weight    A  INNER JOIN  
              segment_sum_risk_red           b  
         ON A.cons_segment_id = b.cons_segment_id      
                                                LEFT OUTER JOIN
              seg_prev_period                   c
         ON A.cons_segment_id = c.cons_segment_id
         ORDER BY A.cons_segment_id
      ;
      
      TYPE seg_rebuild_rec_typ   IS RECORD 
      (cons_segment_id  VARCHAR2(30)
      ,seg_mtzn         VARCHAR2(50)
      ,rebuild_yr       PLS_INTEGER
      );
      
      TYPE seg_rebuild_tab_typ            IS TABLE OF seg_rebuild_rec_typ INDEX BY VARCHAR2(9);
      v_seg_rebuild_tab                   seg_rebuild_tab_typ;

      TYPE seg_stc_rec_typ   IS RECORD 
      (cons_segment_id  VARCHAR2(30)
      ,seg_mtzn         VARCHAR2(50)
      ,rebuild_yr       PLS_INTEGER
      --,scheduled         CHAR(1)           --pickid init
      );
      v_seg_stc_rec                   seg_stc_rec_typ;
      
      TYPE seg_stc_tab_typ       IS TABLE OF seg_stc_rec_typ INDEX BY VARCHAR2(10);
      v_seg_stc_tab              seg_stc_tab_typ;

      CURSOR bay_strategy_run_csr IS
         SELECT *
         FROM structools.st_bay_strategy_run
         WHERE run_id = i_run_id
         --AND calendar_year BETWEEN (v_year_0_seg_rb + 1) AND v_last_year_seg_rb
         ORDER BY cons_segment_id   --null segments are last by default when ordering in asc sequence.
                 ,pick_id
                 ,calendar_year
      ;
      
      v_bay_strategy_run_rec           bay_strategy_run_csr%ROWTYPE;
      TYPE bay_strategy_run_tab_typ    IS TABLE OF bay_strategy_run_csr%ROWTYPE;
      v_bay_strategy_run_tab           bay_strategy_run_tab_typ := bay_strategy_run_tab_typ();

      CURSOR eqp_strategy_run_csr IS
         SELECT *
         FROM structools.st_strategy_run
         WHERE run_id = i_run_id
         --AND calendar_year BETWEEN (v_year_0_seg_rb + 1) AND v_last_year_seg_rb
         ORDER BY vic_id
                 ,pick_id
                 ,part_cde
                 ,calendar_year
      ;

      v_eqp_strategy_run_rec           eqp_strategy_run_csr%ROWTYPE;
      TYPE eqp_strategy_run_tab_typ    IS TABLE OF eqp_strategy_run_csr%ROWTYPE;
      v_eqp_strategy_run_tab           eqp_strategy_run_tab_typ := eqp_strategy_run_tab_typ();

      /*************************************************************************************************/
      /* Private procedures for optimise_for_segment_rebuild.                                          */    
      /*************************************************************************************************/   
      PROCEDURE process_bay_pick_id_tab(i_rebuild IN BOOLEAN
                                    ,i_segment IN VARCHAR2) IS

         v_seg_rebuild_yr           PLS_INTEGER;
         v_rebuild_idx              PLS_INTEGER;
         v_cons_seg_mtzn            VARCHAR2(50);
         v_idx                      PLS_INTEGER := 0;
         v_len                      PLS_INTEGER := 0;
         v_stc                      VARCHAR2(10);
         v_event                    VARCHAR2(10);
         v_event_reason             VARCHAR2(50);
         v_capex                        PLS_INTEGER;
         v_labour_hrs                 PLS_INTEGER;
         v_risk                           FLOAT(126);
         v_risk_noaction                  FLOAT(126);
         v_risk_reduction                 FLOAT(126);
         v_risk_npv15                          FLOAT(126);
         v_risk_noaction_npv15            FLOAT(126);
         v_risk_reduction_npv15           FLOAT(126);
         v_risk_npv15_sif                     FLOAT(126);
         v_risk_noaction_npv15_sif         FLOAT(126);
         v_risk_reduction_npv15_sif        FLOAT(126);
         j                                             PLS_INTEGER;
         v_loc                              char(1);
         v_pickid                           varchar2(10);
--         v_scheduled                        CHAR(1);      --pickid init
         v_mtzn_dummy                     VARCHAR2(30);
         v_rr_npv15_sif_fdr_cat_adj       FLOAT(126);
         v_priority_ind                   PLS_INTEGER;
         v_pickid_suppressed				CHAR(1);
      
      BEGIN

         v_pickid := v_bay_strategy_run_tab(1).PICK_ID;
         v_pickid_suppressed := v_bay_strategy_run_tab(1).pickid_suppressed;

         IF i_rebuild THEN
            v_seg_rebuild_yr := v_seg_rebuild_tab(i_segment).rebuild_yr;
            v_rebuild_idx := v_seg_rebuild_yr - v_year_0; --_seg_rb;
            v_cons_seg_mtzn := v_seg_rebuild_tab(i_segment).seg_mtzn;

            /* Save affected structures (as vicinity id) and cons segment details for them */
            v_idx := INSTR(v_bay_strategy_run_tab(1).vic_id,'S',2);
            v_len := LENGTH(v_bay_strategy_run_tab(1).vic_id);
            v_stc := SUBSTR(v_bay_strategy_run_tab(1).vic_id,1,v_idx - 1);
            IF NOT v_seg_stc_tab.EXISTS(v_stc) THEN
               v_seg_stc_rec.cons_segment_id := i_segment;
               v_seg_stc_rec.rebuild_yr      := v_seg_rebuild_yr;
               v_seg_stc_rec.seg_mtzn        := v_cons_seg_mtzn;
               --v_seg_stc_rec.scheduled       := v_scheduled;       --pickid_init
               v_seg_stc_tab('L'||v_stc) := v_seg_stc_rec;
            END IF;
            v_stc := SUBSTR(v_bay_strategy_run_tab(1).vic_id, v_idx, v_len - v_idx + 1);
            IF NOT v_seg_stc_tab.EXISTS(v_stc) THEN
               v_seg_stc_tab('L'||v_stc) := v_seg_stc_rec;
            END IF;
         
            v_event := NULL;
            FOR i IN 1 .. v_run_years LOOP --_seg_rb LOOP
               IF v_event IS NULL AND v_bay_strategy_run_tab(i).event IS NOT NULL THEN
                  v_event := v_bay_strategy_run_tab(i).event;
                  v_event_reason := v_bay_strategy_run_tab(i).event_reason;
                  v_capex                            := v_bay_strategy_run_tab(i).capex;
                  v_labour_hrs                     := v_bay_strategy_run_tab(i).labour_hrs;
                  v_risk                                 := v_bay_strategy_run_tab(i).risk;
                  v_risk_noaction                    := v_bay_strategy_run_tab(i).risk_noaction;
                  v_risk_reduction                   := v_bay_strategy_run_tab(i).risk_reduction;
                  v_risk_npv15                           := v_bay_strategy_run_tab(i).risk_npv15;
                  v_risk_noaction_npv15              := v_bay_strategy_run_tab(i).risk_noaction_npv15;
                  v_risk_reduction_npv15             := v_bay_strategy_run_tab(i).risk_reduction_npv15;
                  v_risk_npv15_sif                       := v_bay_strategy_run_tab(i).risk_npv15_sif;
                  v_risk_noaction_npv15_sif          := v_bay_strategy_run_tab(i).risk_noaction_npv15_sif;
                  v_risk_reduction_npv15_sif          := v_bay_strategy_run_tab(i).risk_reduction_npv15_sif;
                  v_rr_npv15_sif_fdr_cat_adj          := v_bay_strategy_run_tab(i).rr_npv15_sif_fdr_cat_adj;
                  v_priority_ind                      := v_bay_strategy_run_tab(i).priority_ind;
                  EXIT;
               END IF;
            END LOOP;
            
            FOR i IN 1 .. v_run_years LOOP --_seg_rb LOOP
                J := i;
               IF i < v_rebuild_idx THEN
                  /* nullify event etc, populate segment details */
                  v_loc := '1';
                  INSERT 
                  INTO structools.st_bay_strategy_run_rb_tmp
                  	(
                     RUN_ID
                     ,VIC_ID
                     ,PICK_ID
                     ,EQUIP_GRP_ID
                     ,EVENT
                     ,RISK
                     ,RISK_NOACTION
--                     ,REMAINING_VAL
                     ,LABOUR_HRS
                     ,CAPEX
--                     ,OPEX
                     ,AGE
                     ,CALENDAR_YEAR
                     ,EVENT_REASON
                     ,RISK_REDUCTION
--                     ,PROT_ZONE_ID
--                     ,PROT_ZONE_LEN
                     ,SPAN_LEN
                     ,HVCO_CNT
                     ,LVCO_CNT
                     ,HVSP_CNT
                     ,MAINT_ZONE_NAM
                     ,RISK_NPV15
                     ,RISK_NOACTION_NPV15
                     ,RISK_REDUCTION_NPV15
                     ,RISK_NPV15_SIF
                     ,RISK_NOACTION_NPV15_SIF
                     ,RISK_REDUCTION_NPV15_SIF
                     ,SEGMENT_ID
                     ,COND_EQP_TYP
                     ,CONS_SEGMENT_ID
                     ,SCHEDULED
                     ,CONS_SEGMENT_MTZN
                     ,SEGMENT_REBUILD_YR
                     ,RR_NPV15_SIF_FDR_CAT_ADJ
                     ,PRIORITY_IND
                     ,PICKID_SUPPRESSED
                     )
                     VALUES ( v_bay_strategy_run_tab(i).RUN_ID                    
                             ,v_bay_strategy_run_tab(i).VIC_ID                    
                             ,v_bay_strategy_run_tab(i).PICK_ID                   
                             ,v_bay_strategy_run_tab(i).EQUIP_GRP_ID              
                             ,NULL                 --EVENT                     
                             ,v_bay_strategy_run_tab(i).RISK                      
                             ,v_bay_strategy_run_tab(i).RISK_NOACTION             
--                             ,NULL                 --remaining_val             
                             ,NULL                 --labour_hrs             
                             ,NULL                 --CAPEX                     
--                             ,v_bay_strategy_run_tab(i).OPEX                      
                             ,v_bay_strategy_run_tab(i).AGE                       
                             ,v_bay_strategy_run_tab(i).CALENDAR_YEAR             
                             ,NULL                 --EVENT_REASON              
                             ,0                    --RISK_REDUCTION            
--                             ,v_bay_strategy_run_tab(i).PROT_ZONE_ID              
--                             ,v_bay_strategy_run_tab(i).PROT_ZONE_LEN             
                             ,v_bay_strategy_run_tab(i).SPAN_LEN                  
                             ,v_bay_strategy_run_tab(i).HVCO_CNT                  
                             ,v_bay_strategy_run_tab(i).LVCO_CNT                  
                             ,v_bay_strategy_run_tab(i).HVSP_CNT                  
                             ,v_bay_strategy_run_tab(i).MAINT_ZONE_NAM            
                             ,NULL                 --RISK_NPV15                
                             ,NULL                 --RISK_NOACTION_NPV15       
                             ,0                    --RISK_REDUCTION_NPV15      
                             ,NULL                 --RISK_NPV15_SIF            
                             ,NULL                 --RISK_NOACTION_NPV15_SIF   
                             ,0                    --RISK_REDUCTION_NPV15_SIF  
                             ,v_bay_strategy_run_tab(i).SEGMENT_ID                
                             ,v_bay_strategy_run_tab(i).COND_EQP_TYP              
                             ,v_bay_strategy_run_tab(i).cons_segment_id
                             --,v_scheduled              --pickid init
                             ,v_bay_strategy_run_tab(i).scheduled
                             ,v_cons_seg_mtzn
                             ,v_seg_rebuild_yr
                             ,0                    --RR_NPV15_SIF_FDR_CAT_ADJ
                             ,NULL                 -- priority_ind
                             ,v_pickid_suppressed
                             )
                  ;
               ELSE
                  /* allocate event and event_reason etc */
                  IF v_event IS NULL THEN /* Rebuild of segment but this bay not selected for action */
                     v_loc := '2';
                     INSERT 
                     INTO structools.st_bay_strategy_run_rb_tmp
                           (
                           RUN_ID
                           ,VIC_ID
                           ,PICK_ID
                           ,EQUIP_GRP_ID
                           ,EVENT
                           ,RISK
                           ,RISK_NOACTION
--                           ,REMAINING_VAL
                           ,LABOUR_HRS
                           ,CAPEX
--                           ,OPEX
                           ,AGE
                           ,CALENDAR_YEAR
                           ,EVENT_REASON
                           ,RISK_REDUCTION
--                           ,PROT_ZONE_ID
--                           ,PROT_ZONE_LEN
                           ,SPAN_LEN
                           ,HVCO_CNT
                           ,LVCO_CNT
                           ,HVSP_CNT
                           ,MAINT_ZONE_NAM
                           ,RISK_NPV15
                           ,RISK_NOACTION_NPV15
                           ,RISK_REDUCTION_NPV15
                           ,RISK_NPV15_SIF
                           ,RISK_NOACTION_NPV15_SIF
                           ,RISK_REDUCTION_NPV15_SIF
                           ,SEGMENT_ID
                           ,COND_EQP_TYP
                           ,CONS_SEGMENT_ID
                           ,SCHEDULED
                           ,CONS_SEGMENT_MTZN
                           ,SEGMENT_REBUILD_YR
                           ,RR_NPV15_SIF_FDR_CAT_ADJ
                           ,PRIORITY_IND
                           ,PICKID_SUPPRESSED
                           )
                        VALUES ( v_bay_strategy_run_tab(i).RUN_ID                    
                                 ,v_bay_strategy_run_tab(i).VIC_ID                    
                                 ,v_bay_strategy_run_tab(i).PICK_ID                   
                                 ,v_bay_strategy_run_tab(i).EQUIP_GRP_ID              
                                 ,v_EVENT                     
                                 ,v_bay_strategy_run_tab(i).risk                      
                                 ,v_bay_strategy_run_tab(i).risk_noaction             
--                                 ,NULL                      --remaining_val              
                                 ,v_bay_strategy_run_tab(i).labour_hrs             
                                 ,v_bay_strategy_run_tab(i).capex                     
--                                 ,v_bay_strategy_run_tab(i).OPEX                      
                                 ,v_bay_strategy_run_tab(i).AGE                       
                                 ,v_bay_strategy_run_tab(i).CALENDAR_YEAR             
                                 ,v_bay_strategy_run_tab(i).EVENT_REASON              
                                 ,v_bay_strategy_run_tab(i).risk_reduction            
--                                 ,v_bay_strategy_run_tab(i).PROT_ZONE_ID              
--                                 ,v_bay_strategy_run_tab(i).PROT_ZONE_LEN             
                                 ,v_bay_strategy_run_tab(i).SPAN_LEN                  
                                 ,v_bay_strategy_run_tab(i).HVCO_CNT                  
                                 ,v_bay_strategy_run_tab(i).LVCO_CNT                  
                                 ,v_bay_strategy_run_tab(i).HVSP_CNT                  
                                 ,v_bay_strategy_run_tab(i).MAINT_ZONE_NAM            
                                 ,v_bay_strategy_run_tab(i).risk_npv15                
                                 ,v_bay_strategy_run_tab(i).risk_noaction_npv15       
                                 ,v_bay_strategy_run_tab(i).risk_reduction_npv15      
                                 ,v_bay_strategy_run_tab(i).risk_npv15_sif            
                                 ,v_bay_strategy_run_tab(i).risk_noaction_npv15_sif   
                                 ,v_bay_strategy_run_tab(i).risk_reduction_npv15_sif  
                                 ,v_bay_strategy_run_tab(i).SEGMENT_ID                
                                 ,v_bay_strategy_run_tab(i).COND_EQP_TYP              
                                 ,v_bay_strategy_run_tab(i).cons_segment_id
                                 --,v_scheduled             --pickid init
                                 ,v_bay_strategy_run_tab(i).scheduled
                                 ,v_cons_seg_mtzn
                                 ,v_seg_rebuild_yr
                                 ,v_bay_strategy_run_tab(i).rr_npv15_sif_fdr_cat_adj
                                 ,NULL                      --priority_ind
                                 ,v_pickid_suppressed
                             		)
                     ;
                  ELSE
                     v_loc := '3';
                     INSERT 
                     INTO structools.st_bay_strategy_run_rb_tmp
                        (
                        RUN_ID
                        ,VIC_ID
                        ,PICK_ID
                        ,EQUIP_GRP_ID
                        ,EVENT
                        ,RISK
                        ,RISK_NOACTION
--                        ,REMAINING_VAL
                        ,LABOUR_HRS
                        ,CAPEX
--                        ,OPEX
                        ,AGE
                        ,CALENDAR_YEAR
                        ,EVENT_REASON
                        ,RISK_REDUCTION
--                        ,PROT_ZONE_ID
--                        ,PROT_ZONE_LEN
                        ,SPAN_LEN
                        ,HVCO_CNT
                        ,LVCO_CNT
                        ,HVSP_CNT
                        ,MAINT_ZONE_NAM
                        ,RISK_NPV15
                        ,RISK_NOACTION_NPV15
                        ,RISK_REDUCTION_NPV15
                        ,RISK_NPV15_SIF
                        ,RISK_NOACTION_NPV15_SIF
                        ,RISK_REDUCTION_NPV15_SIF
                        ,SEGMENT_ID
                        ,COND_EQP_TYP
                        ,CONS_SEGMENT_ID
                        ,SCHEDULED
                        ,CONS_SEGMENT_MTZN
                        ,SEGMENT_REBUILD_YR
                        ,RR_NPV15_SIF_FDR_CAT_ADJ
                        ,PRIORITY_IND
                        ,PICKID_SUPPRESSED
                        )
                         VALUES ( v_bay_strategy_run_tab(i).RUN_ID                    
                                 ,v_bay_strategy_run_tab(i).VIC_ID                    
                                 ,v_bay_strategy_run_tab(i).PICK_ID                   
                                 ,v_bay_strategy_run_tab(i).EQUIP_GRP_ID              
                                 ,v_EVENT                     
                                 ,v_risk                      
                                 ,v_risk_noaction             
--                                 ,NULL                   --remaining_val              
                                 ,v_labour_hrs                                   
                                 ,v_capex                     
--                                 ,v_bay_strategy_run_tab(i).OPEX                      
                                 ,v_bay_strategy_run_tab(i).AGE                       
                                 ,v_bay_strategy_run_tab(i).CALENDAR_YEAR             
                                 ,v_EVENT_REASON              
                                 ,v_risk_reduction            
--                                 ,v_bay_strategy_run_tab(i).PROT_ZONE_ID              
--                                 ,v_bay_strategy_run_tab(i).PROT_ZONE_LEN             
                                 ,v_bay_strategy_run_tab(i).SPAN_LEN                  
                                 ,v_bay_strategy_run_tab(i).HVCO_CNT                  
                                 ,v_bay_strategy_run_tab(i).LVCO_CNT                  
                                 ,v_bay_strategy_run_tab(i).HVSP_CNT                  
                                 ,v_bay_strategy_run_tab(i).MAINT_ZONE_NAM            
                                 ,v_risk_npv15                
                                 ,v_risk_noaction_npv15       
                                 ,v_risk_reduction_npv15      
                                 ,v_risk_npv15_sif            
                                 ,v_risk_noaction_npv15_sif   
                                 ,v_risk_reduction_npv15_sif  
                                 ,v_bay_strategy_run_tab(i).SEGMENT_ID                
                                 ,v_bay_strategy_run_tab(i).COND_EQP_TYP              
                                 ,v_bay_strategy_run_tab(i).cons_segment_id
                                 --,v_scheduled          --pickid init
                                 ,v_bay_strategy_run_tab(i).scheduled
                                 ,v_cons_seg_mtzn
                                 ,v_seg_rebuild_yr
                                 ,v_rr_npv15_sif_fdr_cat_adj
                                 ,v_priority_ind
                                 ,v_pickid_suppressed
                             		)
                     ;
                  END IF; 
               END IF;
            END LOOP;
         ELSE
            /* copy as is with nulls in the segment details columns */
            FOR i IN 1 .. v_run_years LOOP  --_seg_rb LOOP
               v_loc := '4';
               INSERT
               INTO structools.st_bay_strategy_run_rb_tmp 
                  	(
                     RUN_ID
                     ,VIC_ID
                     ,PICK_ID
                     ,EQUIP_GRP_ID
                     ,EVENT
                     ,RISK
                     ,RISK_NOACTION
--                     ,REMAINING_VAL
                     ,LABOUR_HRS
                     ,CAPEX
--                     ,OPEX
                     ,AGE
                     ,CALENDAR_YEAR
                     ,EVENT_REASON
                     ,RISK_REDUCTION
--                     ,PROT_ZONE_ID
--                     ,PROT_ZONE_LEN
                     ,SPAN_LEN
                     ,HVCO_CNT
                     ,LVCO_CNT
                     ,HVSP_CNT
                     ,MAINT_ZONE_NAM
                     ,RISK_NPV15
                     ,RISK_NOACTION_NPV15
                     ,RISK_REDUCTION_NPV15
                     ,RISK_NPV15_SIF
                     ,RISK_NOACTION_NPV15_SIF
                     ,RISK_REDUCTION_NPV15_SIF
                     ,SEGMENT_ID
                     ,COND_EQP_TYP
                     ,CONS_SEGMENT_ID
                     ,SCHEDULED
                     ,CONS_SEGMENT_MTZN
                     ,SEGMENT_REBUILD_YR
                     ,RR_NPV15_SIF_FDR_CAT_ADJ
                     ,PRIORITY_IND
                     ,PICKID_SUPPRESSED
                     )
                  VALUES ( v_bay_strategy_run_tab(i).RUN_ID                    
                          ,v_bay_strategy_run_tab(i).VIC_ID                    
                          ,v_bay_strategy_run_tab(i).PICK_ID                   
                          ,v_bay_strategy_run_tab(i).EQUIP_GRP_ID              
                          ,v_bay_strategy_run_tab(i).EVENT                     
                          ,v_bay_strategy_run_tab(i).RISK                      
                          ,v_bay_strategy_run_tab(i).RISK_NOACTION             
--                          ,NULL                             --remaining_val              
                          ,v_bay_strategy_run_tab(i).labour_hrs             
                          ,v_bay_strategy_run_tab(i).CAPEX                     
--                          ,v_bay_strategy_run_tab(i).OPEX                      
                          ,v_bay_strategy_run_tab(i).AGE                       
                          ,v_bay_strategy_run_tab(i).CALENDAR_YEAR             
                          ,v_bay_strategy_run_tab(i).EVENT_REASON              
                          ,v_bay_strategy_run_tab(i).RISK_REDUCTION            
--                          ,v_bay_strategy_run_tab(i).PROT_ZONE_ID              
--                          ,v_bay_strategy_run_tab(i).PROT_ZONE_LEN             
                          ,v_bay_strategy_run_tab(i).SPAN_LEN                  
                          ,v_bay_strategy_run_tab(i).HVCO_CNT                  
                          ,v_bay_strategy_run_tab(i).LVCO_CNT                  
                          ,v_bay_strategy_run_tab(i).HVSP_CNT                  
                          ,v_bay_strategy_run_tab(i).MAINT_ZONE_NAM            
                          ,v_bay_strategy_run_tab(i).RISK_NPV15                
                          ,v_bay_strategy_run_tab(i).RISK_NOACTION_NPV15       
                          ,v_bay_strategy_run_tab(i).RISK_REDUCTION_NPV15      
                          ,v_bay_strategy_run_tab(i).RISK_NPV15_SIF            
                          ,v_bay_strategy_run_tab(i).RISK_NOACTION_NPV15_SIF   
                          ,v_bay_strategy_run_tab(i).RISK_REDUCTION_NPV15_SIF  
                          ,v_bay_strategy_run_tab(i).SEGMENT_ID                
                          ,v_bay_strategy_run_tab(i).COND_EQP_TYP              
                          ,v_bay_strategy_run_tab(i).cons_segment_id
                          ,v_bay_strategy_run_tab(i).scheduled
                          ,NULL         --seg_mtzn
                          ,NULL         --seg_rebuild_year
                          ,v_bay_strategy_run_tab(i).RR_NPV15_SIF_FDR_CAT_ADJ
                          ,v_bay_strategy_run_tab(i).PRIORITY_IND
                          ,v_pickid_suppressed
                          )
               ;
            END LOOP;
         END IF;
         
      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line('v_loc = '||v_loc||' i_segment = '||i_segment
                                    ||' v_pickid = '||v_pickid
                                    ||' v_capex = '||v_capex||' v_event = '||v_event
                                    ||' v_rebuild_idx = '||v_rebuild_idx);
            dbms_output.put_line('vic_id = '||v_bay_strategy_run_tab(1).vic_id||' v_idx = '||v_idx
                                    ||' v_len = '||v_len||' v_len - v_idx + 1 = '||v_len - v_idx + 1);
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);                    
            RAISE;   
      END process_bay_pick_id_tab;

      PROCEDURE process_eqp_pick_id_tab(i_rebuild           IN BOOLEAN
                                       ,i_cons_segment_id   IN VARCHAR2
                                       ,i_seg_rebuild_yr    IN PLS_INTEGER
                                       ,i_cons_seg_mtzn     IN VARCHAR2) IS         --pickid init
                                       --,i_scheduled         IN CHAR) IS
                                    
         v_rebuild_idx                    PLS_INTEGER;
         v_event                          VARCHAR2(10);
         v_event_reason                   VARCHAR2(50);
         v_capex                           PLS_INTEGER;
         v_labour_hrs                     PLS_INTEGER;
         v_risk                           FLOAT(126);
         v_risk_noaction                  FLOAT(126);
         v_risk_reduction                 FLOAT(126);
         v_risk_npv15                     FLOAT(126);
         v_risk_noaction_npv15            FLOAT(126);
         v_risk_reduction_npv15           FLOAT(126);
         v_risk_npv15_sif                 FLOAT(126);
         v_risk_noaction_npv15_sif        FLOAT(126);
         v_risk_reduction_npv15_sif       FLOAT(126);
         v_failure_prob                   CHAR(1);
--         j                                  PLS_INTEGER := 0;
         v_rr_npv15_sif_fdr_cat_adj       FLOAT(126);
         v_priority_ind                   PLS_INTEGER;
			v_pickid_suppressed				CHAR(1);

      BEGIN

			v_pickid_suppressed := v_eqp_strategy_run_tab(1).pickid_suppressed;
         
         IF i_rebuild THEN
            v_rebuild_idx := i_seg_rebuild_yr - v_year_0;  --_seg_rb;
         
            /* establish values for capex and risk, for the original action year */
            v_event := NULL;
            FOR i IN 1 .. v_run_years LOOP   --_seg_rb LOOP
--                j := i;
               IF v_event IS NULL AND v_eqp_strategy_run_tab(i).event IS NOT NULL THEN
                  v_event := v_eqp_strategy_run_tab(i).event;
                  v_event_reason := v_eqp_strategy_run_tab(i).event_reason;
                  v_capex                            := v_eqp_strategy_run_tab(i).capex;
                  v_labour_hrs                     := v_eqp_strategy_run_tab(i).labour_hrs;
                  v_risk                                 := v_eqp_strategy_run_tab(i).risk;
                  v_risk_noaction                    := v_eqp_strategy_run_tab(i).risk_noaction;
                  v_risk_reduction                   := v_eqp_strategy_run_tab(i).risk_reduction;
                  v_risk_npv15                           := v_eqp_strategy_run_tab(i).risk_npv15;
                  v_risk_noaction_npv15              := v_eqp_strategy_run_tab(i).risk_noaction_npv15;
                  v_risk_reduction_npv15             := v_eqp_strategy_run_tab(i).risk_reduction_npv15;
                  v_risk_npv15_sif                       := v_eqp_strategy_run_tab(i).risk_npv15_sif;
                  v_risk_noaction_npv15_sif          := v_eqp_strategy_run_tab(i).risk_noaction_npv15_sif;
                  v_risk_reduction_npv15_sif          := v_eqp_strategy_run_tab(i).risk_reduction_npv15_sif;
                  v_rr_npv15_sif_fdr_cat_adj          := v_eqp_strategy_run_tab(i).rr_npv15_sif_fdr_cat_adj;
                  v_failure_prob                             := v_eqp_strategy_run_tab(i).failure_prob;
                  v_priority_ind                             := v_eqp_strategy_run_tab(i).priority_ind;
                  EXIT;
               END IF;
            END LOOP;
 
            FOR i IN 1 .. v_run_years LOOP   --_seg_rb LOOP
               IF i < v_rebuild_idx THEN
                  /* nullify event etc, populate segment details */
--                  dbms_output.put_line('(1) i = '||i||' run_id = '||v_eqp_strategy_run_tab(i).RUN_ID     
--                                          ||' vic_id = '||v_eqp_strategy_run_tab(i).vic_ID||' pick_id = '||v_eqp_strategy_run_tab(i).pick_ID
--                                          ||' part_cde = '||v_eqp_strategy_run_tab(i).part_cde);
                  INSERT 
                  INTO structools.st_strategy_run_rb_tmp
                  	(
                     RUN_ID
                     ,VIC_ID
                     ,PICK_ID
                     ,EQUIP_GRP_ID
                     ,EVENT
                     ,RISK
                     ,RISK_NOACTION
--                     ,REMAINING_VAL
                     ,LABOUR_HRS
                     ,CAPEX
--                     ,OPEX
                     ,AGE
                     ,CALENDAR_YEAR
--                     ,VARIATION
                     ,EVENT_REASON
                     ,RISK_REDUCTION
                     ,MAINT_ZONE_NAM
--                     ,CONDUCTOR_STRATEGY
                     ,RISK_NPV15
                     ,RISK_NOACTION_NPV15
                     ,RISK_REDUCTION_NPV15
                     ,RISK_NPV15_SIF
                     ,RISK_NOACTION_NPV15_SIF
                     ,RISK_REDUCTION_NPV15_SIF
                     ,FAILURE_PROB
                     ,SCHEDULED
                     ,CONS_SEGMENT_ID
                     ,CONS_SEGMENT_MTZN
                     ,SEGMENT_REBUILD_YR
                     ,RR_NPV15_SIF_FDR_CAT_ADJ
                     ,PRIORITY_IND
                     ,PART_CDE
                     ,PICKID_SUPPRESSED
                     )
                     VALUES ( v_eqp_strategy_run_tab(i).RUN_ID     
                             ,v_eqp_strategy_run_tab(i).VIC_ID                    
                             ,v_eqp_strategy_run_tab(i).PICK_ID                   
                             ,v_eqp_strategy_run_tab(i).EQUIP_GRP_ID              
                             ,NULL                 --EVENT                     
                             ,v_eqp_strategy_run_tab(i).RISK                      
                             ,v_eqp_strategy_run_tab(i).RISK_NOACTION             
--                             ,NULL                 --v_eqp_strategy_run_tab(i).REMAINING_VAL             
                             ,NULL                 --labour_hrs     
                             ,NULL                 --CAPEX                     
--                             ,v_eqp_strategy_run_tab(i).OPEX                      
                             ,v_eqp_strategy_run_tab(i).AGE                       
                             ,v_eqp_strategy_run_tab(i).CALENDAR_YEAR             
--                             ,NULL                 
                             ,NULL                 --EVENT_REASON              
                             ,0                    --RISK_REDUCTION            
                             ,v_eqp_strategy_run_tab(i).MAINT_ZONE_NAM            
--                             ,v_eqp_strategy_run_tab(i).CONDUCTOR_STRATEGY        
                             ,NULL                 --RISK_NPV15                
                             ,NULL                 --RISK_NOACTION_NPV15       
                             ,0                    --RISK_REDUCTION_NPV15      
                             ,NULL                 --RISK_NPV15_SIF            
                             ,NULL                 --RISK_NOACTION_NPV15_SIF   
                             ,0                    --RISK_REDUCTION_NPV15_SIF  
                             ,v_eqp_strategy_run_tab(i).failure_prob               
                             --,i_scheduled           --pickid init
                             ,v_eqp_strategy_run_tab(i).scheduled               
                             ,i_cons_segment_id           
                             ,i_cons_seg_mtzn         
                             ,i_seg_rebuild_yr  
                             ,0                    --RR_NPV15_SIF_FDR_CAT_ADJ
                             ,NULL                 --priority_ind
                             ,v_eqp_strategy_run_tab(i).part_cde
                             ,v_pickid_suppressed
                             )       
                  ;
               ELSE
                  /* allocate event and event_reason etc */
                  IF v_event IS NULL THEN /* Rebuild of segment due to conductors but this pickid not selected for action */
--                  dbms_output.put_line('(2) i = '||i||' run_id = '||v_eqp_strategy_run_tab(i).RUN_ID     
--                                          ||' vic_id = '||v_eqp_strategy_run_tab(i).vic_ID||' pick_id = '||v_eqp_strategy_run_tab(i).pick_ID
--                                          ||' part_cde = '||v_eqp_strategy_run_tab(i).part_cde);
                    INSERT 
                    INTO structools.st_strategy_run_rb_tmp
                        (
                        RUN_ID
                        ,VIC_ID
                        ,PICK_ID
                        ,EQUIP_GRP_ID
                        ,EVENT
                        ,RISK
                        ,RISK_NOACTION
--                        ,REMAINING_VAL
                        ,LABOUR_HRS
                        ,CAPEX
--                        ,OPEX
                        ,AGE
                        ,CALENDAR_YEAR
--                        ,VARIATION
                        ,EVENT_REASON
                        ,RISK_REDUCTION
                        ,MAINT_ZONE_NAM
--                        ,CONDUCTOR_STRATEGY
                        ,RISK_NPV15
                        ,RISK_NOACTION_NPV15
                        ,RISK_REDUCTION_NPV15
                        ,RISK_NPV15_SIF
                        ,RISK_NOACTION_NPV15_SIF
                        ,RISK_REDUCTION_NPV15_SIF
                        ,FAILURE_PROB
                        ,SCHEDULED
                        ,CONS_SEGMENT_ID
                        ,CONS_SEGMENT_MTZN
                        ,SEGMENT_REBUILD_YR
                        ,RR_NPV15_SIF_FDR_CAT_ADJ
                        ,PRIORITY_IND
                        ,PART_CDE
                        ,PICKID_SUPPRESSED
                        )
                      VALUES ( v_eqp_strategy_run_tab(i).RUN_ID     
                            ,v_eqp_strategy_run_tab(i).VIC_ID                    
                            ,v_eqp_strategy_run_tab(i).PICK_ID                   
                            ,v_eqp_strategy_run_tab(i).EQUIP_GRP_ID              
                            ,v_event                     
                            ,v_eqp_strategy_run_tab(i).risk                      
                            ,v_eqp_strategy_run_tab(i).risk_noaction             
--                            ,NULL                        --v_eqp_strategy_run_tab(i).REMAINING_VAL
                            ,v_eqp_strategy_run_tab(i).labour_hrs         
                            ,v_eqp_strategy_run_tab(i).capex                     
--                            ,v_eqp_strategy_run_tab(i).OPEX                      
                            ,v_eqp_strategy_run_tab(i).AGE                       
                            ,v_eqp_strategy_run_tab(i).CALENDAR_YEAR             
--                            ,NULL                 
                            ,v_eqp_strategy_run_tab(i).event_reason              
                            ,v_eqp_strategy_run_tab(i).risk_reduction            
                            ,v_eqp_strategy_run_tab(i).MAINT_ZONE_NAM            
--                            ,v_eqp_strategy_run_tab(i).CONDUCTOR_STRATEGY        
                            ,v_eqp_strategy_run_tab(i).risk_npv15                
                            ,v_eqp_strategy_run_tab(i).risk_noaction_npv15       
                            ,v_eqp_strategy_run_tab(i).risk_reduction_npv15      
                            ,v_eqp_strategy_run_tab(i).risk_npv15_sif            
                            ,v_eqp_strategy_run_tab(i).risk_noaction_npv15_sif   
                            ,v_eqp_strategy_run_tab(i).risk_reduction_npv15_sif  
                            ,v_eqp_strategy_run_tab(i).failure_prob               
                            --,i_scheduled               --pickid init
                            ,v_eqp_strategy_run_tab(i).scheduled               
                             ,i_cons_segment_id           
                            ,i_cons_seg_mtzn         
                            ,i_seg_rebuild_yr
                            ,v_eqp_strategy_run_tab(i).rr_npv15_sif_fdr_cat_adj    
                            ,NULL                        --priority_ind
                            ,v_eqp_strategy_run_tab(i).part_cde
                            ,v_pickid_suppressed
                             )       
                    ;
                  ELSE
--                  dbms_output.put_line('(3) i = '||i||' run_id = '||v_eqp_strategy_run_tab(i).RUN_ID     
--                                          ||' vic_id = '||v_eqp_strategy_run_tab(i).vic_ID||' pick_id = '||v_eqp_strategy_run_tab(i).pick_ID
--                                          ||' part_cde = '||v_eqp_strategy_run_tab(i).part_cde);
                     INSERT 
                     INTO structools.st_strategy_run_rb_tmp
                        (
                        RUN_ID
                        ,VIC_ID
                        ,PICK_ID
                        ,EQUIP_GRP_ID
                        ,EVENT
                        ,RISK
                        ,RISK_NOACTION
--                        ,REMAINING_VAL
                        ,LABOUR_HRS
                        ,CAPEX
--                        ,OPEX
                        ,AGE
                        ,CALENDAR_YEAR
--                        ,VARIATION
                        ,EVENT_REASON
                        ,RISK_REDUCTION
                        ,MAINT_ZONE_NAM
--                        ,CONDUCTOR_STRATEGY
                        ,RISK_NPV15
                        ,RISK_NOACTION_NPV15
                        ,RISK_REDUCTION_NPV15
                        ,RISK_NPV15_SIF
                        ,RISK_NOACTION_NPV15_SIF
                        ,RISK_REDUCTION_NPV15_SIF
                        ,FAILURE_PROB
                        ,SCHEDULED
                        ,CONS_SEGMENT_ID
                        ,CONS_SEGMENT_MTZN
                        ,SEGMENT_REBUILD_YR
                        ,RR_NPV15_SIF_FDR_CAT_ADJ
                        ,PRIORITY_IND
                        ,PART_CDE
                        ,PICKID_SUPPRESSED
                        )
                      VALUES ( v_eqp_strategy_run_tab(i).RUN_ID     
                            ,v_eqp_strategy_run_tab(i).VIC_ID                    
                            ,v_eqp_strategy_run_tab(i).PICK_ID                   
                            ,v_eqp_strategy_run_tab(i).EQUIP_GRP_ID              
                            ,v_event                     
                            ,v_risk                      
                            ,v_risk_noaction             
--                            ,NULL                          --v_eqp_strategy_run_tab(i).REMAINING_VAL
                            ,v_labour_hrs
                            ,v_capex                     
--                            ,v_eqp_strategy_run_tab(i).OPEX                      
                            ,v_eqp_strategy_run_tab(i).AGE                       
                            ,v_eqp_strategy_run_tab(i).CALENDAR_YEAR             
--                            ,NULL
                            ,v_event_reason              
                            ,v_risk_reduction            
                            ,v_eqp_strategy_run_tab(i).MAINT_ZONE_NAM            
--                            ,v_eqp_strategy_run_tab(i).CONDUCTOR_STRATEGY        
                            ,v_risk_npv15                
                            ,v_risk_noaction_npv15       
                            ,v_risk_reduction_npv15      
                            ,v_risk_npv15_sif            
                            ,v_risk_noaction_npv15_sif   
                            ,v_risk_reduction_npv15_sif  
                            ,v_failure_prob               
                            --,i_scheduled            --pickid init
                            ,v_eqp_strategy_run_tab(i).scheduled               
                             ,i_cons_segment_id           
                            ,i_cons_seg_mtzn         
                            ,i_seg_rebuild_yr  
                            ,v_rr_npv15_sif_fdr_cat_adj
                            ,v_priority_ind
                            ,v_eqp_strategy_run_tab(i).part_cde
                             ,v_pickid_suppressed
                             )       
                     ;
                  END IF;
               END IF; 
            END LOOP;
         ELSE
            /* copy as is with nulls in the segment details columns */
           FOR i IN 1 .. v_run_years LOOP   --_seg_rb LOOP
--                  dbms_output.put_line('(4) i = '||i||' run_id = '||v_eqp_strategy_run_tab(i).RUN_ID     
--                                          ||' vic_id = '||v_eqp_strategy_run_tab(i).vic_ID||' pick_id = '||v_eqp_strategy_run_tab(i).pick_ID
--                                          ||' part_cde = '||v_eqp_strategy_run_tab(i).part_cde);
               INSERT 
               INTO structools.st_strategy_run_rb_tmp
                        (
                        RUN_ID
                        ,VIC_ID
                        ,PICK_ID
                        ,EQUIP_GRP_ID
                        ,EVENT
                        ,RISK
                        ,RISK_NOACTION
--                        ,REMAINING_VAL
                        ,LABOUR_HRS
                        ,CAPEX
--                        ,OPEX
                        ,AGE
                        ,CALENDAR_YEAR
--                        ,VARIATION
                        ,EVENT_REASON
                        ,RISK_REDUCTION
                        ,MAINT_ZONE_NAM
--                        ,CONDUCTOR_STRATEGY
                        ,RISK_NPV15
                        ,RISK_NOACTION_NPV15
                        ,RISK_REDUCTION_NPV15
                        ,RISK_NPV15_SIF
                        ,RISK_NOACTION_NPV15_SIF
                        ,RISK_REDUCTION_NPV15_SIF
                        ,FAILURE_PROB
                        ,SCHEDULED
                        ,CONS_SEGMENT_ID
                        ,CONS_SEGMENT_MTZN
                        ,SEGMENT_REBUILD_YR
                        ,RR_NPV15_SIF_FDR_CAT_ADJ
                        ,PRIORITY_IND
                        ,PART_CDE
                        ,PICKID_SUPPRESSED
                        )
                  VALUES ( v_eqp_strategy_run_tab(i).RUN_ID     
                          ,v_eqp_strategy_run_tab(i).VIC_ID                    
                          ,v_eqp_strategy_run_tab(i).PICK_ID                   
                          ,v_eqp_strategy_run_tab(i).EQUIP_GRP_ID              
                          ,v_eqp_strategy_run_tab(i).event                     
                          ,v_eqp_strategy_run_tab(i).RISK                      
                          ,v_eqp_strategy_run_tab(i).RISK_NOACTION             
--                          ,NULL                             --v_eqp_strategy_run_tab(i).REMAINING_VAL       
                          ,v_eqp_strategy_run_tab(i).labour_hrs       
                          ,v_eqp_strategy_run_tab(i).CAPEX                     
--                          ,v_eqp_strategy_run_tab(i).OPEX                      
                          ,v_eqp_strategy_run_tab(i).AGE                       
                          ,v_eqp_strategy_run_tab(i).CALENDAR_YEAR             
--                          ,NULL                 
                          ,v_eqp_strategy_run_tab(i).event_reason              
                          ,v_eqp_strategy_run_tab(i).RISK_REDUCTION            
                          ,v_eqp_strategy_run_tab(i).MAINT_ZONE_NAM            
--                          ,v_eqp_strategy_run_tab(i).CONDUCTOR_STRATEGY        
                          ,v_eqp_strategy_run_tab(i).RISK_NPV15                
                          ,v_eqp_strategy_run_tab(i).RISK_NOACTION_NPV15       
                          ,v_eqp_strategy_run_tab(i).RISK_REDUCTION_NPV15      
                          ,v_eqp_strategy_run_tab(i).RISK_NPV15_SIF            
                          ,v_eqp_strategy_run_tab(i).RISK_NOACTION_NPV15_SIF   
                          ,v_eqp_strategy_run_tab(i).RISK_REDUCTION_NPV15_SIF  
                          ,v_eqp_strategy_run_tab(i).failure_prob               
                          ,v_eqp_strategy_run_tab(i).scheduled
                          ,NULL --i_cons_segment_id           
                          ,NULL --i_cons_seg_mtzn         
                          ,NULL --i_seg_rebuild_yr  
                          ,v_eqp_strategy_run_tab(i).RR_NPV15_SIF_FDR_CAT_ADJ
                          ,v_eqp_strategy_run_tab(i).PRIORITY_IND
                          ,v_eqp_strategy_run_tab(i).part_cde
                           ,v_pickid_suppressed
                             )       
               ;
            END LOOP;
         END IF;

      EXCEPTION
            WHEN OTHERS THEN
               dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);                    
               RAISE;
      END process_eqp_pick_id_tab;
                 
   /************************************************************************************************************/
   /* Mainline for optimise_for_segment_rebuild.                                                               */                      
   /************************************************************************************************************/
   BEGIN

      dbms_output.put_line('Optimise_for_segment_rebuild start '||TO_CHAR(SYSDATE,'hh24:mi:ss'));

      /* Make sure we are not accidentally duplicating data in st_bay_Strategy_run_rb_tmp and st_strategy_run_rb_tmp tables */
      BEGIN
         v_dummy_int := NULL;
         SELECT run_id
         INTO   v_dummy_int
         FROM structools.st_bay_strategy_run_rb_tmp
         WHERE run_id = i_run_id    --since we now always set run_id to run_id. Make sure we are not duplicating data.
         AND ROWNUM = 1;
         dbms_output.put_line('Run ID '||i_run_id||' has DATA already IN st_bay_strategy_run_rb_tmp');
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;

      BEGIN         
         SELECT run_id
         INTO   v_dummy_int
         FROM structools.st_strategy_run_rb_tmp
         WHERE run_id = i_run_id    --since we now always set run_id to run_id. Make sure we are not duplicating data.
         AND ROWNUM = 1;
         dbms_output.put_line('Run ID '||i_run_id||' has DATA already IN st_strategy_run_rb_tmp');
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
      
      /* get time_horizon for the run */
      SELECT first_year
            ,run_years
      INTO   v_first_year
            ,v_run_years
      FROM structools.st_run_request
      WHERE run_id = i_run_id;

      IF v_first_year > v_this_year + 1 THEN
         v_year_0 := v_first_year - 1;
      ELSE
         v_year_0 := v_this_year;
      END IF;

      v_last_year := v_year_0 + v_run_years;
      
      structoolsapp.common_procs.get_parameter_scenarios(i_run_id, v_parm_scen_id_life_exp,v_parm_scen_id_seg_min_len, v_parm_scen_id_seg_cons_pcnt, v_parm_scen_id_sif_weighting
      																												,v_parm_scen_id_seg_rr_doll, v_parm_scen_id_eqp_rr_doll, v_parm_scen_id_fdr_cat_adj);

      /* Get threshold value for squashing segment treatments into a single year (percentage) */
      SELECT TO_NUMBER(parameter_val)
      INTO   v_cons_pcnt_thrshld
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'SEG_CONS_PCNT'
      AND   scenario_id = v_parm_scen_id_seg_cons_pcnt
      ;

      dbms_output.put_line('Run ID '||i_run_id||' v_first_year = '||v_first_year||' v_run_years '||v_run_years
                              ||' v_last_year '||v_last_year||' v_year_0 = '||v_year_0);
      v_seg_rebuild_tab.DELETE;
      FOR c IN seg_rebuild_csr LOOP
         v_seg_rebuild_tab(c.seg_id).cons_segment_id := c.seg_id;
         v_seg_rebuild_tab(c.seg_id).seg_mtzn        := c.seg_mtzn;
         v_seg_rebuild_tab(c.seg_id).rebuild_yr      := c.rebuild_yr + v_year_0; --_seg_rb;
      END LOOP;
                             
      v_seg_id_prev := ' ';
      v_pick_id_prev := ' ';
      v_bay_strategy_run_tab.DELETE;
      v_bay_strategy_run_tab.EXTEND(v_run_years); --(v_seg_rb_years);

      OPEN bay_strategy_run_csr;

      K := 0;
      FETCH bay_strategy_run_csr INTO v_bay_strategy_run_rec;
      v_seg_id_prev := ' '; --v_bay_strategy_run_rec.cons_segment_id; 
      v_seg_stc_tab.DELETE;

		structools.schema_utils.disable_table_indexes('ST_BAY_STRATEGY_RUN_RB_TMP');
                                   
      LOOP 
         IF v_bay_strategy_run_rec.pick_id != v_pick_id_prev THEN
            IF v_pick_id_prev != ' ' THEN
               process_bay_pick_id_tab(v_rebuild,v_seg_id_prev);
               v_bay_strategy_run_tab.DELETE;
               v_bay_strategy_run_tab.EXTEND(v_run_years); --(v_seg_rb_years); 
            END IF;
            K := 0;
         END IF;
                                  
         IF v_bay_strategy_run_rec.cons_segment_id IS NULL THEN
            v_rebuild := FALSE;
         ELSIF v_bay_strategy_run_rec.cons_segment_id != v_seg_id_prev THEN 
            IF v_seg_rebuild_tab.EXISTS(v_bay_strategy_run_rec.cons_segment_id) THEN
               v_rebuild := TRUE;
            ELSE
               v_rebuild := FALSE;
            END IF;
         END IF;
         v_seg_id_prev := v_bay_strategy_run_rec.cons_segment_id; 
         v_pick_id_prev := v_bay_strategy_run_rec.pick_id;
         K := K + 1;
         v_bay_strategy_run_tab(K) := v_bay_strategy_run_rec;
                                              
         FETCH bay_strategy_run_csr INTO v_bay_strategy_run_rec;
         EXIT WHEN bay_strategy_run_csr%NOTFOUND;         
      END LOOP;

      CLOSE bay_strategy_run_csr;

      /* process last bay pick_id */
      process_bay_pick_id_tab(v_rebuild,v_seg_id_prev);
                               
		structools.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN_RB_TMP');
      analyse_table('ST_BAY_STRATEGY_RUN_RB_TMP');
      
      /*********************************************************************/
      /* Process poles and eqp                                             */
      /*********************************************************************/
      v_vic_id_prev := ' ';
      v_pick_id_prev := ' ';
      v_part_cde_prev := ' ';
      v_eqp_strategy_run_tab.DELETE;
      v_eqp_strategy_run_tab.EXTEND(v_run_years); --(v_seg_rb_years);

      OPEN eqp_strategy_run_csr;

      K := 0;
      FETCH eqp_strategy_run_csr INTO v_eqp_strategy_run_rec;
      v_vic_id_prev := ' '; 
      
		structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN_RB_TMP');
                                   
      LOOP 
         IF v_eqp_strategy_run_rec.pick_id != v_pick_id_prev 
         OR v_eqp_strategy_run_rec.part_cde != v_part_cde_prev THEN
            IF v_pick_id_prev != ' ' THEN
               process_eqp_pick_id_tab(v_rebuild, v_cons_segment_id, v_seg_rebuild_yr, v_cons_seg_mtzn);
               v_eqp_strategy_run_tab.DELETE;
               v_eqp_strategy_run_tab.EXTEND(v_run_years); --(v_seg_rb_years);
            END IF;
            K := 0;
         END IF;
                                                                
         IF v_eqp_strategy_run_rec.vic_id != v_vic_id_prev THEN 
            IF v_seg_stc_tab.EXISTS(v_eqp_strategy_run_rec.vic_id) THEN
               v_rebuild := TRUE;
               v_cons_segment_id := v_seg_stc_tab(v_eqp_strategy_run_rec.vic_id).cons_segment_id;
               v_seg_rebuild_yr  := v_seg_stc_tab(v_eqp_strategy_run_rec.vic_id).rebuild_yr;
               v_cons_seg_mtzn   := v_seg_stc_tab(v_eqp_strategy_run_rec.vic_id).seg_mtzn;
               --v_scheduled       := v_seg_stc_tab(v_eqp_strategy_run_rec.vic_id).scheduled;  --pickid init
            ELSE
               v_rebuild := FALSE;
               v_cons_segment_id := NULL;
               v_seg_rebuild_yr  := NULL;
               v_cons_seg_mtzn   := NULL;
               --v_scheduled       := NULL;        --pickid init
            END IF;
         END IF;
         v_vic_id_prev := v_eqp_strategy_run_rec.vic_id; 
         v_pick_id_prev := v_eqp_strategy_run_rec.pick_id;
         v_part_cde_prev := v_eqp_strategy_run_rec.part_cde;
         K := K + 1;
      --            dbms_output.put_line('K = '||K);
         v_eqp_strategy_run_tab(K) := v_eqp_strategy_run_rec;
                                                                   
         FETCH eqp_strategy_run_csr INTO v_eqp_strategy_run_rec;
         EXIT WHEN eqp_strategy_run_csr%NOTFOUND;         

      END LOOP;

      CLOSE eqp_strategy_run_csr;

      /* process last eqp pick_id */
      --process_eqp_pick_id_tab(v_rebuild, v_cons_segment_id, v_seg_rebuild_yr, v_cons_seg_mtzn, v_scheduled);  --pickid init
      process_eqp_pick_id_tab(v_rebuild, v_cons_segment_id, v_seg_rebuild_yr, v_cons_seg_mtzn);

		structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN_RB_TMP');
      analyse_table('ST_STRATEGY_RUN_RB_TMP');

      UPDATE structools.st_run_request
         SET  run_ts2 = SYSDATE
         WHERE run_id = i_run_id;

      COMMIT;     --end of stage 2 (segment time optimisation stage)

      dbms_output.put_line('Optimise_for_segment_rebuild END '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
      
   EXCEPTION

      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END optimise_for_segment_rebuild;
      
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
   /**************************************************************************************************************/
   /* This procedure applies an extra criterion to only select segments and eqp which exceed a pre-defined       */
   /* of risk_redubction_npv/per dollar.                                                                         */     
   /**************************************************************************************************************/ 
   PROCEDURE adjust_for_partial_treatment (i_run_id IN PLS_INTEGER) IS

      v_run_years                        PLS_INTEGER;
      v_run_years_sum                    PLS_INTEGER;    
--      v_parent_run_id                  PLS_INTEGER;      
      v_seg_rr_doll_cutoff_tab         number_tab_typ := number_tab_typ();
      j                                 PLS_INTEGER;
      v_seg_rb_yr_new                  PLS_INTEGER;
      v_seg_upd_yr_first               PLS_INTEGER;
      v_seg_upd_yr_last                PLS_INTEGER;
      
      v_seg_undo_tab                   varchar_tab_typ := varchar_tab_typ();
      v_seg_undo_cnt                   PLS_INTEGER;
      v_sql                            VARCHAR2(1000);
      v_dummy_int                      PLS_INTEGER;
      
      /* Cursor to get all segments which do not qualify for treatment in their first year, ie their segment rebuild year, because of their low risk reduction  */
      CURSOR partial_tr_seg_csr IS 

         WITH pole_top_replace AS
         (
         SELECT cons_segment_id
               ,pick_id
               ,calendar_year
               ,risk_reduction_npv15
         FROM   structools.st_strategy_run_rb_tmp
         WHERE run_id = i_run_id
         AND equip_grp_id = 'PWOD'
         AND part_cde != ' '
         AND event = 'REPLACE'
         AND pickid_suppressed = 'N'
         AND segment_rebuild_yr IS NOT NULL
         AND calendar_year = segment_rebuild_yr
         )              
         ,
         pole_replace AS
         (
         SELECT cons_segment_id
               ,pick_id
               ,calendar_year
         FROM   structools.st_strategy_run_rb_tmp
         WHERE run_id = i_run_id
         AND equip_grp_id = 'PWOD'
         AND part_cde = ' '
         AND event = 'REPLACE'
         AND segment_rebuild_yr IS NOT NULL
         AND pickid_suppressed = 'N'
         AND calendar_year = segment_rebuild_yr
         )
         ,
         pole_top_only_replace AS
         (
         SELECT aa.* FROM
         (
         SELECT A.*
               ,b.pick_id           AS pole_pick_id
         FROM pole_top_replace            A  LEFT OUTER JOIN
              pole_replace                b
         ON A.cons_segment_id = b.cons_segment_id
         AND A.pick_id = b.pick_id
         AND A.calendar_year = b.calendar_year
         ) aa
         WHERE pole_pick_id IS NULL
         )
         ,
         all_events AS
         (
         SELECT cons_segment_id
               ,pick_id
               ,calendar_year
               ,risk_reduction_npv15
         FROM   structools.st_strategy_run_rb_tmp
         WHERE run_id = i_run_id
         AND part_cde = ' '
         AND (event = 'REPLACE' AND pickid_suppressed ='N' OR event = 'REINFORCE' AND pickid_suppressed IN ('N','P'))
         AND segment_rebuild_yr IS NOT NULL
         AND calendar_year = segment_rebuild_yr
         UNION ALL
         SELECT cons_segment_id
               ,pick_id
               ,calendar_year
               ,risk_reduction_npv15
         FROM   pole_top_only_replace
         ORDER BY cons_segment_id
               ,pick_id, calendar_year
         )
         ,
         poles_rr AS
         (
         SELECT cons_segment_id
               ,calendar_year
               ,SUM(risk_reduction_npv15) AS sum_seg_rr
--               ,SUM(capex)                  AS sum_seg_capex      -- no need; bays replace cost includes attached poles and eqp
         FROM all_events      
         GROUP BY cons_segment_id
               ,calendar_year
         )      
         ,
         span_capex AS
         (
         SELECT cons_segment_id
               ,calendar_year
               ,SUM(capex)            AS sum_seg_capex
         FROM(      
         SELECT DISTINCT cons_segment_id
               ,calendar_year
               ,vic_id         AS span_id
               ,capex
         FROM structools.st_bay_strategy_run_rb_tmp      
         WHERE segment_rebuild_yr IS NOT NULL
         AND calendar_year = segment_rebuild_yr
         AND event IS NOT NULL
         AND pickid_suppressed = 'N'
         AND run_id = i_run_id
         ) aa
         GROUP BY aa.cons_segment_id, calendar_year
         )
         ,
         bays_rr AS
         (
         SELECT cons_segment_id
               ,calendar_year
               ,SUM(risk_reduction_npv15) AS sum_seg_rr
         FROM structools.st_bay_strategy_run_rb_tmp      
         WHERE segment_rebuild_yr IS NOT NULL
         AND calendar_year = segment_rebuild_yr
         AND event IS NOT NULL
         AND pickid_suppressed ='N'
         AND run_id = i_run_id
         GROUP BY cons_segment_id
               ,calendar_year
         )
         ,
         bays_capex_rr AS
         (
         SELECT A.cons_segment_id
               ,A.calendar_year
               ,A.sum_seg_capex
               ,b.sum_seg_rr
         FROM span_capex               A   INNER JOIN
              bays_rr                  b
         ON A.cons_segment_id = b.cons_segment_id
         )
         ,
         poles_bays_capex_rr AS
         (
         SELECT NVL(A.cons_segment_id, b.cons_segment_id)    AS cons_segment_id
               ,NVL(A.calendar_year, b.calendar_year)    AS calendar_year
               ,b.sum_seg_capex            AS bays_capex
               ,A.sum_seg_rr               AS pole_rr
               ,b.sum_seg_rr               AS bays_rr
               ,NVL(b.sum_seg_capex,0)                                    AS sum_seg_capex         -- only bays capex, as it includes poles and eqp cost
               ,NVL(A.sum_seg_rr,0)  + NVL(b.sum_seg_rr,0)               AS sum_seg_rr
               ,CASE 
                  WHEN b.sum_seg_capex IS NULL OR b.sum_seg_capex = 0    THEN    0
                  ELSE                                                   (NVL(A.sum_seg_rr,0) + NVL(b.sum_seg_rr,0)) / (NVL(b.sum_seg_capex,0))   
               END                           AS rr_doll
         FROM  poles_rr               A      FULL OUTER JOIN
               bays_capex_rr         b
         ON A.cons_segment_id = b.cons_segment_id
         AND A.calendar_year = b.calendar_year
         )
         ,
         seg_rr_doll_cutoff AS
         (
         SELECT TO_NUMBER(SUBSTR(parameter_id, 15,2)) + v_this_year      AS calendar_year
               ,TO_NUMBER(parameter_val) AS parameter_val
         FROM structools.st_parameter_scenario
         WHERE parameter_id LIKE 'SEG_RR_DOLL_YR%'
         AND scenario_id = v_parm_scen_id_seg_rr_doll
         )
         SELECT A.cons_segment_id
               ,A.calendar_year            AS orig_seg_rb_yr
               ,A.rr_doll
               ,b.parameter_val            AS rr_doll_cutoff
         FROM poles_bays_capex_rr         A      INNER JOIN
              seg_rr_doll_cutoff            b
         ON   A.calendar_year = b.calendar_year                  
         WHERE A.rr_doll < b.parameter_val
         ORDER BY A.calendar_year, A.cons_segment_id
      ;          
      
      TYPE partial_tr_seg_tab_typ         IS TABLE OF partial_tr_seg_csr%ROWTYPE;
      v_partial_tr_seg_tab                partial_tr_seg_tab_typ := partial_tr_seg_tab_typ();                
            
      CURSOR seg_rr_doll_csr IS
         SELECT TO_NUMBER(parameter_val) AS paramater_val
         FROM structools.st_parameter_scenario
         WHERE parameter_id LIKE 'SEG_RR_DOLL_YR%'
         AND scenario_id = v_parm_scen_id_seg_rr_doll
         ORDER BY parameter_id
      ;      
      
      CURSOR reset_seg_bays_csr(i_seg_id IN varchar2) IS
         SELECT *
         FROM structools.st_bay_strategy_run
         WHERE run_id = i_run_id
         AND cons_segment_id = i_seg_id
         ORDER BY pick_id, calendar_year
      ;
      
      TYPE reset_seg_bays_tab_typ            IS TABLE OF reset_seg_bays_csr%ROWTYPE;
      v_reset_seg_bays_tab                  reset_seg_bays_tab_typ := reset_seg_bays_tab_typ();
      
      CURSOR reset_seg_poles_eqp_csr(i_seg_id IN varchar2) IS   --get pickids for the segment and get values for them from the strategy_run table
         SELECT /*+ ORDERED */ b.*
         FROM structools.st_strategy_run_rb_tmp   A   INNER JOIN
              structools.st_strategy_run      b
         ON A.run_id = b.run_id
         AND A.pick_id = b.pick_id
         AND A.part_cde = b.part_cde
         AND A.calendar_year = b.calendar_year           
         WHERE A.run_id = i_run_id
         AND A.cons_segment_id = i_seg_id
         ORDER BY b.pick_id, b.part_cde, b.calendar_year
      ;

      TYPE reset_seg_poles_eqp_tab_typ      IS TABLE OF reset_seg_poles_eqp_csr%ROWTYPE;
      v_reset_seg_poles_eqp_tab            reset_seg_poles_eqp_tab_typ := reset_seg_poles_eqp_tab_typ();
      
      /* Cursor to get all poles and eqp, not part of segment treatment, which do not qualify for treatment. */
      CURSOR partial_tr_eqp_csr IS 

         WITH pickid_rr_doll AS
         (
         SELECT pick_id
               ,part_cde
               ,calendar_year
               ,risk_reduction_npv15 / capex      AS rr_doll
               ,MIN(calendar_year) OVER (PARTITION BY pick_id) AS min_event_yr 
         FROM structools.st_strategy_run_rb --_tmp         --use _rb table, as the _tmp table may contain pickids with segments, which have      
         WHERE segment_rebuild_yr IS NULL         --been reset to no-segment by the preceding section of the code.
         --AND event IS NOT NULL AND event != 'REINFORCE'
         AND event = 'REPLACE'
         AND pickid_suppressed = 'N'
         AND run_id = i_run_id
         )
         ,
         rr_doll_cutoff AS
         (
         SELECT TO_NUMBER(SUBSTR(parameter_id, 15,2)) + v_this_year      AS calendar_year
               ,TO_NUMBER(parameter_val) AS parameter_val
         FROM structools.st_parameter_scenario
         WHERE parameter_id LIKE 'EQP_RR_DOLL_YR%'
         AND scenario_id = v_parm_scen_id_eqp_rr_doll
         )
         ,
         pickid_yr AS
         (
         SELECT A.pick_id
               ,A.part_cde
               ,A.calendar_year
               ,A.rr_doll
               ,A.min_event_yr
               ,b.parameter_val            AS rr_doll_cutoff
         FROM pickid_rr_doll               A      INNER JOIN
              rr_doll_cutoff               b
         ON   A.calendar_year = b.calendar_year                  
         )
         ,
         pickid_first_good_yr AS
         (
         SELECT pick_id
               ,part_cde
               ,MIN(calendar_year)       AS first_good_yr
         FROM pickid_yr
         WHERE rr_doll >= rr_doll_cutoff
         GROUP BY pick_id, part_cde
         )
         ,
         pickid_years AS
         (
         SELECT A.pick_id
               ,A.part_cde
               ,A.calendar_year
               ,A.rr_doll
               ,A.min_event_yr
               ,A.rr_doll_cutoff
               ,NVL(b.first_good_yr, v_this_year + v_run_years_sum + 1) AS first_good_yr  
         FROM pickid_yr            A      LEFT OUTER JOIN
              pickid_first_good_yr   b
         ON A.pick_id = b.pick_id
         AND A.part_cde = b.part_cde
         )
         SELECT DISTINCT pick_id
               ,part_cde
               ,min_event_yr
               ,first_good_yr 
         FROM pickid_years
         WHERE min_event_yr < first_good_yr   
         ORDER BY pick_id                        
      ;                           

      TYPE partial_tr_eqp_tab_typ         IS TABLE OF partial_tr_eqp_csr%ROWTYPE;
      v_partial_tr_eqp_tab                  partial_tr_eqp_tab_typ := partial_tr_eqp_tab_typ();

                 
   /************************************************************************************************************/
   /* Mainline for adjust_for_partial_treatment.                                                               */                      
   /************************************************************************************************************/
   BEGIN

      dbms_output.put_line(TO_CHAR(SYSDATE,'hh24:mi:ss')||' Start OF adjust_for_partial_treatment');

--      DELETE FROM structools.st_bay_strategy_run_rb
--      WHERE run_id = i_run_id;
--      DELETE FROM structools.st_strategy_run_rb
--      WHERE run_id = i_run_id;

    	structools.schema_utils.disable_table_indexes('ST_BAY_STRATEGY_RUN_RB');
     	structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN_RB');

      copy_strategy_run_tmp_to_rb(i_run_id);

    	structools.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN_RB');
     	structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN_RB');
      analyse_table('ST_BAY_STRATEGY_RUN_RB');
      analyse_table('ST_STRATEGY_RUN_RB');
      
      SELECT run_years
      INTO   v_run_years
      FROM    structools.st_run_request
      WHERE run_id = i_run_id;

      v_run_years_sum := v_run_years;

      structoolsapp.common_procs.get_parameter_scenarios(i_run_id, v_parm_scen_id_life_exp,v_parm_scen_id_seg_min_len, v_parm_scen_id_seg_cons_pcnt, v_parm_scen_id_sif_weighting
      																												,v_parm_scen_id_seg_rr_doll, v_parm_scen_id_eqp_rr_doll, v_parm_scen_id_fdr_cat_adj);

      /* Load the table of segment rr/dollar cutoff values */
      v_seg_rr_doll_cutoff_tab.EXTEND(v_run_years_sum);        
      j := 0;
      
      FOR c IN seg_rr_doll_csr LOOP
         j := j + 1;
         IF j <= v_run_years_sum THEN                          
            v_seg_rr_doll_cutoff_tab(j) := c.paramater_val;
         ELSE
            EXIT;
         END IF;
      END LOOP;
      
      v_seg_undo_cnt := 0;
      
      OPEN partial_tr_seg_csr;
      FETCH partial_tr_seg_csr BULK COLLECT INTO v_partial_tr_seg_tab;
      CLOSE partial_tr_seg_csr;
      
      dbms_output.put_line(TO_CHAR(SYSDATE,'hh24:mi:ss')
                        ||' Segments which need TO have their rb_year adjusted. COUNT =  '||v_partial_tr_seg_tab.COUNT);
      
      FOR c IN 1 .. v_partial_tr_seg_tab.COUNT LOOP
         /* Check if the segment qualifies in subsequent years */
         v_seg_rb_yr_new := NULL;
         v_seg_upd_yr_first := v_partial_tr_seg_tab(c).orig_seg_rb_yr; 
         FOR i IN (v_partial_tr_seg_tab(c).orig_seg_rb_yr - v_this_year + 1) .. v_run_years_sum LOOP     
            IF v_partial_tr_seg_tab(c).rr_doll > v_seg_rr_doll_cutoff_tab(i) THEN
               v_seg_rb_yr_new := v_this_year + i;
               v_seg_upd_yr_last := v_seg_rb_yr_new - 1;
               exit;
            END IF;
         END LOOP;

         IF v_seg_rb_yr_new IS NULL THEN         -- segment does not meet the rr-per-dollar requirement in any of the run years 
            /* Save it in a working table */
            v_seg_undo_tab.EXTEND;
            v_seg_undo_cnt := v_seg_undo_cnt + 1;
            v_seg_undo_tab(v_seg_undo_cnt) := v_partial_tr_seg_tab(c).cons_segment_id;
         ELSE
--            dbms_output.put_line(v_partial_tr_seg_tab(c).cons_segment_id||','||v_partial_tr_seg_tab(c).orig_seg_rb_yr
--                           ||','||v_seg_rb_yr_new||','||v_seg_upd_yr_first||','||v_seg_upd_yr_last
--                           ||','||v_partial_tr_seg_tab(c).rr_doll);

            /* Update segment rebuild year */
            UPDATE structools.st_bay_strategy_run_rb
            set segment_rebuild_yr = v_seg_rb_yr_new
            WHERE run_id = i_run_id
            AND cons_segment_id = v_partial_tr_seg_tab(c).cons_segment_id;
   --         dbms_output.put_line('Set (bay) segment_rebuild_yr TO '||v_seg_rb_yr_new||' WHERE cons_segment_id = '||c.cons_segment_id);

            /* Adjust values for non-qualifying years */
            UPDATE structools.st_bay_strategy_run_rb
            set event = NULL
               ,risk = risk_noaction
               ,capex = NULL
               ,labour_hrs = NULL
               ,event_reason = NULL
               ,risk_reduction = 0
               ,risk_npv15 = risk_noaction_npv15
               ,risk_reduction_npv15 = 0
               ,risk_npv15_sif = risk_noaction_npv15_sif
               ,risk_reduction_npv15_sif = 0
               ,rr_npv15_sif_fdr_cat_adj = 0
               ,priority_ind = NULL
            WHERE run_id = i_run_id
            AND cons_segment_id = v_partial_tr_seg_tab(c).cons_segment_id
            AND event IS NOT NULL
            AND calendar_year BETWEEN v_seg_upd_yr_first AND v_seg_upd_yr_last;          

            /* Update segment rebuild year */
            UPDATE structools.st_strategy_run_rb
            set segment_rebuild_yr = v_seg_rb_yr_new
            WHERE run_id = i_run_id
            AND cons_segment_id = v_partial_tr_seg_tab(c).cons_segment_id;

            /* Adjust values for non-qualifying years */
            UPDATE structools.st_strategy_run_rb
            set event = NULL
               ,risk = risk_noaction
               ,capex = NULL
               ,labour_hrs = NULL
               ,event_reason = NULL
               ,risk_reduction = 0
               ,risk_npv15 = risk_noaction_npv15
               ,risk_reduction_npv15 = 0
               ,risk_npv15_sif = risk_noaction_npv15_sif
               ,risk_reduction_npv15_sif = 0
               ,rr_npv15_sif_fdr_cat_adj = 0
               ,failure_prob = 'N'
               ,priority_ind = NULL
            WHERE run_id = i_run_id
            AND cons_segment_id = v_partial_tr_seg_tab(c).cons_segment_id
            AND event IS NOT NULL
            AND calendar_year BETWEEN v_seg_upd_yr_first AND v_seg_upd_yr_last;          
         END IF;
      END LOOP;
      
      --===========================================================================================================================
      dbms_output.put_line('=======================');
      dbms_output.put_line(TO_CHAR(SYSDATE,'hh24:mi:ss')||' Segments which DO NOT qualify IN ANY OF THE '||v_run_years||'. Count = '||v_seg_undo_tab.COUNT);
      
      FOR i IN 1 .. v_seg_undo_tab.COUNT LOOP
         v_reset_seg_bays_tab.DELETE;
         OPEN reset_seg_bays_csr(v_seg_undo_tab(i));
         FETCH reset_seg_bays_csr BULK COLLECT INTO v_reset_seg_bays_tab;
         CLOSE reset_seg_bays_csr;
         FORALL K IN 1 .. v_reset_seg_bays_tab.COUNT
            UPDATE structools.st_bay_strategy_run_rb
               set event                      = v_reset_seg_bays_tab(K).event
                  ,risk                       = v_reset_seg_bays_tab(K).risk
                  ,risk_noaction             = v_reset_seg_bays_tab(K).risk_noaction
                  ,capex                      = v_reset_seg_bays_tab(K).capex
                  ,labour_hrs               = v_reset_seg_bays_tab(K).labour_hrs
                  ,event_reason                = v_reset_seg_bays_tab(K).event_reason
                  ,risk_reduction             = v_reset_seg_bays_tab(K).risk_reduction
                  ,risk_npv15                = v_reset_seg_bays_tab(K).risk_npv15
                  ,risk_noaction_npv15       = v_reset_seg_bays_tab(K).risk_noaction_npv15
                  ,risk_reduction_npv15       = v_reset_seg_bays_tab(K).risk_reduction_npv15
                  ,risk_npv15_sif             = v_reset_seg_bays_tab(K).risk_npv15_sif
                  ,risk_noaction_npv15_sif    = v_reset_seg_bays_tab(K).risk_noaction_npv15_sif
                  ,risk_reduction_npv15_sif    = v_reset_seg_bays_tab(K).risk_reduction_npv15_sif
                  ,rr_npv15_sif_fdr_cat_adj    = v_reset_seg_bays_tab(K).rr_npv15_sif_fdr_cat_adj
                  ,segment_rebuild_yr          = NULL
                  ,priority_ind                 = v_reset_seg_bays_tab(K).priority_ind
            WHERE run_id          = i_run_id
            AND cons_segment_id    = v_seg_undo_tab(i)
            AND pick_id          = v_reset_seg_bays_tab(K).pick_id
            AND calendar_year    = v_reset_seg_bays_tab(K).calendar_year
         ;
         v_reset_seg_poles_eqp_tab.DELETE;
         OPEN reset_seg_poles_eqp_csr(v_seg_undo_tab(i));
         FETCH reset_seg_poles_eqp_csr BULK COLLECT INTO v_reset_seg_poles_eqp_tab;
         CLOSE reset_seg_poles_eqp_csr;
         FORALL K IN 1 .. v_reset_seg_poles_eqp_tab.COUNT
            UPDATE structools.st_strategy_run_rb
               set event                      = v_reset_seg_poles_eqp_tab(K).event
                  ,risk                       = v_reset_seg_poles_eqp_tab(K).risk
                  ,risk_noaction             = v_reset_seg_poles_eqp_tab(K).risk_noaction
                  ,capex                      = v_reset_seg_poles_eqp_tab(K).capex
                  ,labour_hrs               = v_reset_seg_poles_eqp_tab(K).labour_hrs
                  ,event_reason                = v_reset_seg_poles_eqp_tab(K).event_reason
                  ,risk_reduction             = v_reset_seg_poles_eqp_tab(K).risk_reduction
                  ,risk_npv15                = v_reset_seg_poles_eqp_tab(K).risk_npv15
                  ,risk_noaction_npv15       = v_reset_seg_poles_eqp_tab(K).risk_noaction_npv15
                  ,risk_reduction_npv15       = v_reset_seg_poles_eqp_tab(K).risk_reduction_npv15
                  ,risk_npv15_sif             = v_reset_seg_poles_eqp_tab(K).risk_npv15_sif
                  ,risk_noaction_npv15_sif    = v_reset_seg_poles_eqp_tab(K).risk_noaction_npv15_sif
                  ,risk_reduction_npv15_sif    = v_reset_seg_poles_eqp_tab(K).risk_reduction_npv15_sif
                  ,rr_npv15_sif_fdr_cat_adj    = v_reset_seg_poles_eqp_tab(K).rr_npv15_sif_fdr_cat_adj
                  ,cons_segment_id             = NULL
                  ,cons_segment_mtzn          = NULL
                  ,segment_rebuild_yr          = NULL
                  ,failure_prob              = v_reset_seg_poles_eqp_tab(K).failure_prob
                  ,priority_ind              = v_reset_seg_poles_eqp_tab(K).priority_ind
            WHERE run_id = i_run_id
            AND cons_segment_id    = v_seg_undo_tab(i)
            AND pick_id          = v_reset_seg_poles_eqp_tab(K).pick_id
            AND part_cde         = v_reset_seg_poles_eqp_tab(K).part_cde
            AND calendar_year    = v_reset_seg_poles_eqp_tab(K).calendar_year
         ;
      END LOOP;

      COMMIT;
      dbms_output.put_line(TO_CHAR(SYSDATE,'hh24:mi:ss')||' Completed SEGMENT processing');
      

      --===========================================================================================================================
      OPEN partial_tr_eqp_csr;
      FETCH partial_tr_eqp_csr BULK COLLECT INTO v_partial_tr_eqp_tab;
      CLOSE partial_tr_eqp_csr;

      dbms_output.put_line('=======================');
      dbms_output.put_line(TO_CHAR(SYSDATE,'hh24:mi:ss')
                           ||' Poles AND eqp, NOT part OF SEGMENT treatment, which DO NOT qualify IN SOME OR ALL years. COUNT = '
                           ||v_partial_tr_eqp_tab.COUNT);
     
--      FOR i IN 1 .. v_partial_tr_eqp_tab.COUNT LOOP
--         dbms_output.put_line(v_partial_tr_eqp_tab(i).pick_id||','||v_partial_tr_eqp_tab(i).min_event_yr
--                        ||','||v_partial_tr_eqp_tab(i).first_good_yr);
--      END LOOP;
      
      FORALL i IN 1 .. v_partial_tr_eqp_tab.COUNT
         UPDATE structools.st_strategy_run_rb
         set        event = NULL
                  ,risk  = risk_noaction
                  ,capex  = NULL
                  ,labour_hrs  = NULL
                  ,event_reason = NULL
                  ,risk_reduction = 0
                  ,risk_npv15 = NULL
                  ,risk_noaction_npv15 = NULL
                  ,risk_reduction_npv15 = 0
                  ,risk_npv15_sif = NULL
                  ,risk_noaction_npv15_sif = NULL
                  ,risk_reduction_npv15_sif = 0
                  ,rr_npv15_sif_fdr_cat_adj = 0
                  ,failure_prob = 'N'
                  ,priority_ind = NULL
         WHERE run_id = i_run_id
         AND   pick_id = v_partial_tr_eqp_tab(i).pick_id
         AND   part_cde = v_partial_tr_eqp_tab(i).part_cde
         AND calendar_year BETWEEN v_partial_tr_eqp_tab(i).min_event_yr AND (v_partial_tr_eqp_tab(i).first_good_yr - 1)
      ;

      UPDATE structools.st_run_request
         SET  run_ts3 = SYSDATE
         WHERE run_id = i_run_id;

      COMMIT;
      dbms_output.put_line(TO_CHAR(SYSDATE,'hh24:mi:ss')||' END OF adjust_for_partial_treatment');
      
   EXCEPTION

      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END adjust_for_partial_treatment;

   PROCEDURE upgrade_poles_under_cond (i_run_id IN PLS_INTEGER) IS

      /* Cursor to get all poles attached to bays being replaced, for which we want to 'UPGRADE' their original event */
      CURSOR pole_event_upgrade_csr IS
         WITH carrier_model AS
         (
         SELECT DISTINCT NVL(A.pick_id, b.conductor_pick_id) AS conductor_pick_id
               ,CASE
                  WHEN b.conductor_pick_id IS NOT NULL   THEN  TO_CHAR(b.carrier_model_code)
                  ELSE                                         TO_CHAR(A.carrier_model_code)
               END                     AS carrier_mdl_cde
         FROM structoolsw.carrier_detail            A     FULL OUTER JOIN
              structools.st_carrier_detail         b
         ON A.pick_id = b.conductor_pick_id     -- handle non existing carrier model; ie use BLANKS from the s/sheet
         )
         ,
         bay_stc AS
         (
         SELECT pick_id AS bay_pick_id
               ,stc1    AS pole_pick_id
               ,'C'||conductor_pick_id AS conductor_pick_id
               ,bay_len_m
         FROM  structools.st_bay_equipment
         WHERE bay_equip_cde LIKE 'HV%'
         UNION
         SELECT pick_id AS bay_pick_id
               ,stc2    AS pole_pick_id
               ,'C'||conductor_pick_id AS conductor_pick_id
               ,bay_len_m
         FROM  structools.st_bay_equipment
         WHERE bay_equip_cde LIKE 'HV%'
         )
         ,
         bay_pole_carr_mdl   AS
         (
         SELECT A.bay_pick_id
                ,A.pole_pick_id
               ,A.conductor_pick_id
               ,A.bay_len_m
               ,b.carrier_mdl_cde
               ,COUNT(*) OVER (PARTITION BY A.pole_pick_id) AS bay_cnt
         FROM  bay_stc                  A     LEFT OUTER JOIN
               carrier_model           b
         ON A.conductor_pick_id = b.conductor_pick_id  
         )
         ,
         candidate_poles AS
         (
         SELECT DISTINCT
               pole_segment_rebuild_yr
               ,pole_pick_id
               ,pole_event
               ,risk_noaction
               ,CASE
                  WHEN si_factor_gl IS NULL AND bay_cnt < 3         THEN    0.559269562 --v_si_gl_lt3_default
                  WHEN si_factor_gl IS NULL AND bay_cnt >=3         THEN    0.544554996 --v_si_gl_ge3_default  
                  ELSE                                               si_factor_gl
               END                                                   AS si_factor_gl              
               ,CASE
                  WHEN si_factor_1m_agl IS NULL AND bay_cnt < 3         THEN    0.548773955 --v_si_1m_agl_lt3_default
                  WHEN si_factor_1m_agl IS NULL AND bay_cnt >=3         THEN    0.512364739 --v_si_1m_agl_ge3_default
                  ELSE                                                      si_factor_1m_Agl                                                  
               END                                                   AS si_factor_1m_agl
         FROM
         (
         SELECT  b.pole_pick_id
                  ,b.conductor_pick_id
                  ,b.bay_len_m
                  ,b.carrier_mdl_cde
                  ,b.bay_cnt
                  ,MAX(b.bay_len_m) OVER (PARTITION BY b.pole_pick_id) AS max_bay_len_m
                  ,c.event          AS pole_event
                  ,c.risk_noaction
                  ,c.segment_rebuild_yr   AS pole_segment_rebuild_yr
                  ,d.carrier_mdl_cde   AS carr_mdl_si
                  ,d.bay_cnt           AS bay_cnt_si
                  ,d.si_factor_gl
                  ,d.si_factor_1m_agl
         FROM  structools.st_bay_strategy_run_rb   A  INNER JOIN
               bay_pole_carr_mdl                     b
         ON A.pick_id = b.bay_pick_id     
                                                   INNER JOIN
               structools.st_strategy_run_rb                      c
         ON b.pole_pick_id = c.pick_id               
                                                   LEFT OUTER JOIN
               structools.st_carr_mdl_si_factor           d
         ON b.carrier_mdl_cde = d.carrier_mdl_cde               
         WHERE A.run_id = i_run_id
         AND   A.segment_rebuild_yr IS NOT NULL
         AND   A.cond_eqp_typ LIKE 'HV%'
         AND   A.event = 'REPLACE'
         AND   A.calendar_year = A.segment_rebuild_yr
         AND   A.pickid_suppressed ='N'
         AND   c.run_id = i_run_id
         AND   c.scheduled = 'N'
         AND   (c.event = 'REINFORCE' AND	c.pickid_suppressed IN ('N','P') OR c.event IS NULL AND	c.pickid_suppressed = 'N')
         AND   c.calendar_year = c.segment_rebuild_yr
         AND   c.part_cde = ' '
         ) aa
         WHERE aa.bay_len_m = max_bay_len_m
         AND   (carr_mdl_si IS NULL
               OR bay_cnt < 3 AND bay_cnt_si = '<3'
               OR bay_cnt >= 3 AND bay_cnt_si = '>=3')
         )
         ,
         all_data AS
         (
         SELECT A.*
               ,b.good_wood_reading_1m_agl
               ,b.good_wood_reading_100_agl_mm
               ,b.good_wood_reading_100_bgl_mm
               ,b.si_at_gl       * si_factor_gl       AS si_gl
               ,b.si_at_1m_agl   * si_factor_1m_agl   AS si_1m_agl
               ,b.reinf_cde
              ,CASE 
                  WHEN UPPER(c.scnrrr_fdr_cat_nam) = 'NONE'            THEN 'SHORTRURAL'
                  WHEN c.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                  ELSE                                                            UPPER(c.scnrrr_fdr_cat_nam)
              END                                                            AS fdr_cat
              ,b.pole_complexity
              ,CASE b.region_nam
                  WHEN 'METRO'               THEN  'METRO'
                  ELSE                             'COUNTRY'
              END                            AS region
              ,b.labour_hrs
         FROM candidate_poles             A  INNER JOIN
               structools.st_equipment       b
         ON A.pole_pick_id = b.pick_id
                                                 LEFT OUTER JOIN
               structools.st_hv_feeder    c
         ON b.feeder_id =        c.hv_fdr_id                                                                        
         WHERE b.part_cde = ' '    
         )
         ,
         event_upgrades AS
         (
         SELECT A.*
               ,CASE
                  /* cases for when the pole has not been reinforced in the past (see printed out diagrams with marked paths*/
                  --sheet 1
                  WHEN  A.reinf_cde IS NULL AND pole_event IS NULL
                        AND good_wood_reading_100_bgl_mm >= 50 AND good_wood_reading_100_agl_mm >= 50
                        AND si_gl >= 1
                        AND good_wood_reading_1m_agl >= 50 AND si_1m_agl < 1
                                                               THEN 'REPLACE'
                  --sheet 2
                  WHEN  A.reinf_cde IS NULL AND pole_event IS NULL
                        AND good_wood_reading_100_bgl_mm >= 50 AND good_wood_reading_100_agl_mm >= 50
                        AND si_gl >= 1
                        AND good_wood_reading_1m_agl < 50
                                                               THEN 'REPLACE'
                  --sheet 3
                  WHEN  A.reinf_cde IS NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE') -- light green
                        AND good_wood_reading_100_bgl_mm >= 50 AND good_wood_reading_100_agl_mm >= 50
                        AND si_gl < 0.2 
                        AND good_wood_reading_1m_agl >= 50 AND si_1m_agl < 1.1
                                                               THEN 'REPLACE'
                  --sheet 4
                  WHEN  A.reinf_cde IS NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE') -- orange
                        AND good_wood_reading_100_bgl_mm >= 50 AND good_wood_reading_100_agl_mm >= 50
                        AND si_gl < 0.2 
                        AND good_wood_reading_1m_agl < 50 
                                                               THEN 'REPLACE'                  --sheet 5
                  --sheet 5                                                               THEN 'REPLACE'
                  WHEN  A.reinf_cde IS NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE') -- yellow
                        AND good_wood_reading_100_bgl_mm >= 50 AND good_wood_reading_100_agl_mm >= 50
                        AND si_gl >= 0.2 AND si_gl < 1
                        AND good_wood_reading_1m_agl >= 50 AND si_1m_agl < 1.1
                                                               THEN 'REPLACE'
                  --sheet 6
                  WHEN  A.reinf_cde IS NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE') -- navy
                        AND good_wood_reading_100_bgl_mm < 50 AND good_wood_reading_100_bgl_mm >= 30
                        AND good_wood_reading_100_agl_mm >= 50
                        AND good_wood_reading_1m_agl >= 50
                        AND si_1m_agl < 1.1
                                                               THEN 'REPLACE'
                  --sheet 7
                  WHEN  A.reinf_cde IS NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE') -- blue
                        AND good_wood_reading_100_bgl_mm >= 50 AND good_wood_reading_100_agl_mm >= 50
                        AND si_gl >= 0.2 AND si_gl < 1
                        AND good_wood_reading_1m_agl < 50 
                                                               THEN 'REPLACE'
                  --sheet 8
                  WHEN  A.reinf_cde IS NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE') -- yellow2
                        AND good_wood_reading_100_bgl_mm < 50 AND good_wood_reading_100_bgl_mm >= 30
                        AND good_wood_reading_100_agl_mm >= 50
                        AND good_wood_reading_1m_agl <= 50
                                                               THEN 'REPLACE'
                  --sheet 9
                  WHEN  A.reinf_cde IS NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE') -- dark green
                        AND good_wood_reading_100_bgl_mm < 50 AND good_wood_reading_100_bgl_mm >= 30
                        AND good_wood_reading_100_agl_mm < 50
                                                               THEN 'REPLACE'
                  --sheet 10
                  WHEN  A.reinf_cde IS NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE') -- pink
                        AND good_wood_reading_100_bgl_mm < 30
                                                               THEN 'REPLACE'
                  --sheet 1F
                  WHEN  A.reinf_cde IS NULL AND pole_event IS NULL
                        AND good_wood_reading_100_bgl_mm >= 50 AND good_wood_reading_100_agl_mm >= 50
                        AND si_gl >= 0.2 AND si_gl < 1
                        AND good_wood_reading_1m_agl >= 50 AND si_1m_agl >= 1.1
                                                               THEN 'REINFORCE'
                  --sheet 2F
                  WHEN  A.reinf_cde IS NULL AND pole_event IS NULL
                        AND good_wood_reading_100_bgl_mm < 50 AND good_wood_reading_100_bgl_mm >= 30 AND good_wood_reading_100_agl_mm >= 50
                        AND good_wood_reading_1m_agl >= 50 AND si_1m_agl >= 1.1
                                                               THEN 'REINFORCE'
                  --sheet 3F                                                               
                  WHEN  A.reinf_cde IS NULL AND pole_event IS NULL
                        AND good_wood_reading_100_bgl_mm >= 50 AND good_wood_reading_100_agl_mm >= 50
                        AND si_gl < 0.2
                        AND good_wood_reading_1m_agl >= 50 AND si_1m_agl >= 1.1
                                                               THEN 'REINFORCE'
                  /* cases for when the pole has been reinforced in the past (see gw_readings_test cases.docx)*/
                  WHEN  A.reinf_cde IS NOT NULL AND pole_event IS NULL --pink
                        AND good_wood_reading_100_agl_mm >= 50 AND good_wood_reading_1m_agl >= 50
                        AND si_gl < 1 AND si_1m_agl >= 1.1
                                                               THEN 'REINFORCE'
                  WHEN  A.reinf_cde IS NOT NULL AND pole_event IS NULL --orange
                        AND good_wood_reading_100_agl_mm < 50 AND good_wood_reading_100_agl_mm >= 40
                        AND good_wood_reading_1m_agl >= 50
                        AND si_1m_agl >= 1.1
                                                               THEN 'REINFORCE'
                  WHEN  A.reinf_cde IS NOT NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE') --blue
                        AND good_wood_reading_100_agl_mm >= 50 AND good_wood_reading_1m_agl < 50
                                                               THEN 'REPLACE'    --1a, 2b, 3a, 4a
                  WHEN  A.reinf_cde IS NOT NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE')  --yellow
                        AND good_wood_reading_100_agl_mm >= 50 AND good_wood_reading_1m_agl >= 50
                        AND si_1m_agl < 1.1
                                                               THEN 'REPLACE' --1a,2a,3a,4b, 1a,2a,3b,4b
                  WHEN  A.reinf_cde IS NOT NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE')  --pink
                        AND good_wood_reading_100_agl_mm < 50 AND good_wood_reading_100_agl_mm >= 40
                        AND good_wood_reading_1m_agl >= 50
                        AND si_1m_agl < 1.1
                                                               THEN 'REPLACE'    --1b, 1c, 2a, 3a, 4b
                  WHEN  A.reinf_cde IS NOT NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE')  --lighter green
                        AND good_wood_reading_100_agl_mm < 50 AND good_wood_reading_100_agl_mm >= 40
                        AND good_wood_reading_1m_agl < 50
                                                               THEN 'REPLACE'    --1b, 1c, 2a, 3a, 4b
                  WHEN  A.reinf_cde IS NOT NULL AND (pole_event IS NULL OR pole_event = 'REINFORCE')  --sea green
                        AND good_wood_reading_100_agl_mm < 40
                                                               THEN 'REPLACE'       --1b, 1d, 2a, 3a, 4a
                  ELSE                                               NULL
            END               AS pole_event_new       
         FROM all_data  A
         )
         SELECT * FROM event_upgrades WHERE pole_event_new IS NOT NULL              
      ;
      
      TYPE pole_event_upgrade_tab_typ      IS TABLE OF pole_event_upgrade_csr%ROWTYPE;    
      v_pole_event_upgrade_tab               pole_event_upgrade_tab_typ := pole_event_upgrade_tab_typ();   

      v_risk                     float(126);
      v_risk_reduction           float(126);
      v_risk_npv15               float(126);
      v_risk_noaction_npv15      float(26);
      v_risk_reduction_npv15     float(126);
      v_risk_npv15_sif           float(126);
      v_risk_noaction_npv15_sif  float(126); 
      v_risk_reduction_npv15_sif float(126);
      v_rr_npv15_sif_fdr_cat_adj float(126);
      v_capex                    PLS_INTEGER;
      v_labour_hrs                    PLS_INTEGER;
      v_age                      PLS_INTEGER := 0;
      v_first_year               PLS_INTEGER;
      v_last_year                PLS_INTEGER;
      v_run_years                PLS_INTEGER;
                 
   /************************************************************************************************************/
   /* Mainline for upgrade_poles_under_cond.                                                               */                      
   /************************************************************************************************************/
   BEGIN

      dbms_output.put_line(TO_CHAR(SYSDATE,'hh24:mi:ss')||' Start OF upgrade_poles_under_cond');

      SELECT      run_years
                 ,first_year
      INTO       v_run_years
                ,v_first_year
      FROM       structools.st_run_request    
      WHERE      run_id = i_run_id 
      ;

      v_last_year     := v_first_year - 1 + v_run_years; 

--      DELETE FROM structools.st_strategy_run_rb2
--      WHERE run_id = i_run_id;
--
      structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN_RB2');
     
      copy_st_strategy_run_rb_to_rb2(i_run_id);

      structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN_RB2');
      analyse_table ('ST_STRATEGY_RUN_RB2');

      structoolsapp.common_procs.get_parameter_scenarios(i_run_id, v_parm_scen_id_life_exp,v_parm_scen_id_seg_min_len, v_parm_scen_id_seg_cons_pcnt, v_parm_scen_id_sif_weighting
      																												,v_parm_scen_id_seg_rr_doll, v_parm_scen_id_eqp_rr_doll, v_parm_scen_id_fdr_cat_adj);

         
      populate_working_arrays;
         
      get_sif_weightings(v_parm_scen_id_sif_weighting, v_parm_scen_id_fdr_cat_adj);


      OPEN pole_event_upgrade_csr;
      FETCH pole_event_upgrade_csr BULK COLLECT INTO v_pole_event_upgrade_tab;
      CLOSE pole_event_upgrade_csr;
      dbms_output.put_line('v_pole_event_upgrade_tab count = '||v_pole_event_upgrade_tab.COUNT);
      
      FOR i IN 1 .. v_pole_event_upgrade_tab.COUNT LOOP
         --dbms_output.put_line('pick_id = '||v_pole_event_upgrade_tab(i).pole_pick_id);
         IF v_pole_event_upgrade_tab(i).pole_event_new = 'REPLACE' THEN
            FOR v_exec_year IN v_pole_event_upgrade_tab(i).pole_segment_rebuild_yr .. v_last_year LOOP
               get_risk_for_year(v_pole_event_upgrade_tab(i).pole_pick_id, 'PWOD', ' ', 'REPLACE', v_age
                                ,v_exec_year, NULL, NULL, v_pole_event_upgrade_tab(i).fdr_cat
                                ,v_risk, v_risk_npv15, v_risk_noaction_npv15
                                ,v_risk_npv15_sif, v_risk_noaction_npv15_sif); 
               
               IF v_risk < v_pole_event_upgrade_tab(i).risk_noaction THEN
                  v_risk_reduction := v_pole_event_upgrade_tab(i).risk_noaction - v_risk;
               ELSE
                  v_risk_reduction := 0;
               END IF;
               IF v_risk_npv15 < v_risk_noaction_npv15 THEN
                  v_risk_reduction_npv15 := v_risk_noaction_npv15 - v_risk_npv15;
               ELSE
                  v_risk_reduction_npv15 := 0;
               END IF;
               IF v_risk_npv15_sif < v_risk_noaction_npv15_sif THEN
                  v_risk_reduction_npv15_sif := v_risk_noaction_npv15_sif - v_risk_npv15_sif;
               ELSE
                  v_risk_reduction_npv15_sif := 0;
               END IF;
               v_rr_npv15_sif_fdr_cat_adj := v_risk_reduction_npv15_sif * v_fdr_cat_sif_tab(v_pole_event_upgrade_tab(i).fdr_cat).adj_factor;
               v_cost_key := 'PWOD'||'Y'||'FULL'||'REPLACE'||TRIM(v_pole_event_upgrade_tab(i).pole_complexity)||TRIM(v_pole_event_upgrade_tab(i).region);
               v_capex := v_cost_tab(v_cost_key).total_cost;
               v_labour_hrs := v_pole_event_upgrade_tab(i).labour_hrs;

               --dbms_output.put_line('UPDATE = '||v_pole_event_upgrade_tab(i).pole_pick_id||' cal YEAR = '||v_exec_year);
               UPDATE   structools.st_strategy_run_rb2
                  SET    risk = v_risk
                        ,risk_reduction = v_risk_reduction
                        ,risk_npv15 = v_risk_npv15
                        ,risk_noaction_npv15 = v_risk_noaction_npv15
                        ,risk_reduction_npv15 = v_risk_reduction_npv15
                        ,risk_npv15_sif = v_risk_npv15_sif
                        ,risk_noaction_npv15_sif = v_risk_noaction_npv15_sif
                        ,risk_reduction_npv15_sif = v_risk_reduction_npv15_sif
                        ,rr_npv15_sif_fdr_cat_adj = v_rr_npv15_sif_fdr_cat_adj
                        ,event = 'REPLACE'
                        ,event_reason = 'CONDUCTOR'
                        ,capex = v_capex
                        ,labour_hrs = v_labour_hrs
               WHERE run_id = i_run_id
               AND calendar_year = v_exec_year
               AND pick_id = v_pole_event_upgrade_tab(i).pole_pick_id AND part_cde = ' '
               ;
--               dbms_output.put_line(TO_CHAR(SYSDATE,'hh24:mi:ss')||' UPDATED pole '||v_pole_event_upgrade_tab(i).pole_pick_id);
            END LOOP;
         ELSIF v_pole_event_upgrade_tab(i).pole_event_new = 'REINFORCE' THEN
            FOR v_exec_year IN v_pole_event_upgrade_tab(i).pole_segment_rebuild_yr .. v_last_year LOOP
               get_risk_for_year(v_pole_event_upgrade_tab(i).pole_pick_id, 'PWOD', ' ', 'REINFORCE', v_age
                                ,v_exec_year, NULL, NULL, v_pole_event_upgrade_tab(i).fdr_cat
                                ,v_risk, v_risk_npv15, v_risk_noaction_npv15
                                ,v_risk_npv15_sif, v_risk_noaction_npv15_sif); 
               
               IF v_risk < v_pole_event_upgrade_tab(i).risk_noaction THEN
                  v_risk_reduction := v_pole_event_upgrade_tab(i).risk_noaction - v_risk;
               ELSE
                  v_risk_reduction := 0;
               END IF;
               IF v_risk_npv15 < v_risk_noaction_npv15 THEN
                  v_risk_reduction_npv15 := v_risk_noaction_npv15 - v_risk_npv15;
               ELSE
                  v_risk_reduction_npv15 := 0;
               END IF;
               IF v_risk_npv15_sif < v_risk_noaction_npv15_sif THEN
                  v_risk_reduction_npv15_sif := v_risk_noaction_npv15_sif - v_risk_npv15_sif;
               ELSE
                  v_risk_reduction_npv15_sif := 0;
               END IF;
               v_rr_npv15_sif_fdr_cat_adj := v_risk_reduction_npv15_sif * v_fdr_cat_sif_tab(v_pole_event_upgrade_tab(i).fdr_cat).adj_factor;
               v_cost_key := 'PWOD'||'N'||'FULL'||'REINFORCE'||TRIM(v_pole_event_upgrade_tab(i).region);
               v_capex := v_cost_tab(v_cost_key).total_cost;

               UPDATE   structools.st_strategy_run_rb2
                  SET    risk = v_risk
                        ,risk_reduction = v_risk_reduction
                        ,risk_npv15 = v_risk_npv15
                        ,risk_noaction_npv15 = v_risk_noaction_npv15
                        ,risk_reduction_npv15 = v_risk_reduction_npv15
                        ,risk_npv15_sif = v_risk_npv15_sif
                        ,risk_noaction_npv15_sif = v_risk_noaction_npv15_sif
                        ,risk_reduction_npv15_sif = v_risk_reduction_npv15_sif
                        ,rr_npv15_sif_fdr_cat_adj = v_rr_npv15_sif_fdr_cat_adj
                        ,event = 'REINFORCE'
                        ,event_reason = 'CONDUCTOR'
                        ,capex = v_capex
                        ,labour_hrs = NULL
               WHERE run_id = i_run_id
               AND calendar_year = v_exec_year
               AND pick_id = v_pole_event_upgrade_tab(i).pole_pick_id AND part_cde = ' '
               ;
--               dbms_output.put_line(TO_CHAR(SYSDATE,'hh24:mi:ss')||' UPDATED pole '||v_pole_event_upgrade_tab(i).pole_pick_id);
            END LOOP;
--         ELSE
--            dbms_output.put_line('unhandled NEW event '||' '||v_pole_event_upgrade_tab(i).pole_pick_id
--                              ||' '||v_pole_event_upgrade_tab(i).pole_event_new);
         END IF;
      END LOOP;
      
      UPDATE structools.st_run_request
         SET  run_ts4 = SYSDATE
         WHERE run_id = i_run_id;
      
      COMMIT;
      dbms_output.put_line(TO_CHAR(SYSDATE,'hh24:mi:ss')||' END OF upgrade_poles_under_cond');
      
   EXCEPTION

      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END upgrade_poles_under_cond;

      

   /**************************************************************************************************************/
   /* This procedure creates entries in st_del_program_sniper table for individual asstes selected for treatment,*/
   /* outside of MTZNs as selected by the external optimser, according to requested strategy (currently only     */
   /* risk reduction per dollar).                                                                                */
   /* This porcedure cretaes entries in st_del_program_sniper table for individual asstes selected for treatment,*/ 
   PROCEDURE create_sniper_program(i_program_id                   IN PLS_INTEGER
                                  ,i_base_run_id                  IN PLS_INTEGER 
                                  ,i_del_year                     IN PLS_INTEGER
                                  ,i_del_strategy_id              IN PLS_INTEGER
                                  ,i_del_strategy_scenario_id     IN PLS_INTEGER) IS
                                    
	BEGIN
   
   	structoolsapp.optimiser_cycle.create_sniper_program(i_program_id, i_base_run_id, i_del_year, i_del_strategy_id, i_del_strategy_scenario_id);
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END create_sniper_program;
   
   /**************************************************************************************************************/
   /* This procedure creates entries in st_del_program_reac_maint table for individual assets sampled for        */
   /* reactive treatment based on calculated number of anticipated failures following applied actions.           */
   /**************************************************************************************************************/
   PROCEDURE create_reac_maint_program(i_program_id                   IN PLS_INTEGER
                                  ,i_base_run_id                  IN PLS_INTEGER 
                                  ,i_del_year                     IN PLS_INTEGER
                                  ,i_del_strategy_id              IN PLS_INTEGER
                                  ,i_del_strategy_scenario_id     IN PLS_INTEGER) IS
                                    
	BEGIN
   
   	structoolsapp.optimiser_cycle.create_reac_maint_program(i_program_id, i_base_run_id, i_del_year, i_del_strategy_id, i_del_strategy_scenario_id);
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END create_reac_maint_program;
   
   PROCEDURE reload_temp_strategy_tables (i_base_run_id IN PLS_INTEGER
                                         ,i_strategy_run_stage IN PLS_INTEGER) IS

	BEGIN
   
   	structoolsapp.optimiser_cycle.reload_temp_strategy_tables(i_base_run_id, i_strategy_run_stage);
            
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END reload_temp_strategy_tables;      

-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
   /**************************************************************************************************************/   
   /* Populates table st_strategy_run_summary, which is interrogated by an external optimser script              */
   /**************************************************************************************************************/   
   PROCEDURE run_summary_for_opensolver(i_base_run_id    IN PLS_INTEGER
                                       ,i_year           IN PLS_INTEGER
                                       ,i_program_id     IN PLS_INTEGER 
                                       ,i_version_id     IN PLS_INTEGER
                                       ,i_mtzn_exclude_yrs      IN PLS_INTEGER DEFAULT 0) IS

	BEGIN
   
   	structoolsapp.optimiser_cycle.run_summary_for_opensolver(i_base_run_id, i_year, i_program_id, i_version_id, i_mtzn_exclude_yrs);
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END run_summary_for_opensolver;        
      
   PROCEDURE run_summary_for_segments  (i_base_run_id    IN PLS_INTEGER
                                       ,i_year           IN PLS_INTEGER
                                       ,i_program_id     IN PLS_INTEGER) IS 

	BEGIN
   
   	structoolsapp.optimiser_cycle.run_summary_for_segments(i_base_run_id, i_year, i_program_id);
         
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END run_summary_for_segments;

-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
   /**************************************************************************************************************/   
   /* Populates table st_del_program_mtzn_reinf_summ, which is interrogated by an external optimiser script                                                                  */
   /**************************************************************************************************************/   
   PROCEDURE run_summary_for_mtzn_reinf(i_base_run_id    IN PLS_INTEGER
                                       						 ,i_year           		IN PLS_INTEGER
                                      							 ,i_program_id      IN PLS_INTEGER) IS 

	BEGIN
   
   	structoolsapp.optimiser_cycle.run_summary_for_mtzn_reinf(i_base_run_id, i_year, i_program_id);
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END run_summary_for_mtzn_reinf;        
      
   /*---------------------------------------------------------------------------------------------------------------*/
   /* Generates a list of pickids treated for a given run.                                                          */
   /* If the one logical run consists of multiple physical runs (eg 5 * 10_year run for a 50-year outlook), this    */
   /* procedure is to be run after each physical run. It relies on strategy_run to be populated in the              */
   /* st_temp_bay_strategy_run and st_temp_stratgey_run tables.                                                     */
   /*---------------------------------------------------------------------------------------------------------------*/    
   PROCEDURE generate_opt_eqp_list(i_program_id         IN PLS_INTEGER
                                  ,i_base_run_id        IN PLS_INTEGER
                                  ,i_delete_flg         IN VARCHAR2    DEFAULT 'N') IS

	BEGIN
   
   	structoolsapp.reporting.generate_opt_eqp_list(i_program_id, i_base_run_id, i_delete_flg);   
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END generate_opt_eqp_list;        


   /*---------------------------------------------------------------------------------------------------------------*/
   /* Generates data on prob of failure (incident).                                                                 */
   /* If the one logical run consists of multiple physical runs (eg 5 * 10_year run for a 50-year outlook), this    */
   /* procedure is to be run with the last run_id, after all runs have completed.                                   */
   /*---------------------------------------------------------------------------------------------------------------*/    
   PROCEDURE generate_incd_forecast(i_base_run_id  IN PLS_INTEGER
                                   ,i_program_id   IN PLS_INTEGER     DEFAULT NULL
                                   ,i_years        IN PLS_INTEGER     DEFAULT NULL
                                   ,i_delete_flg   IN VARCHAR2        DEFAULT 'N') IS

   BEGIN

		structoolsapp.reporting.generate_incd_forecast(i_base_run_id, i_program_id, i_years, i_delete_flg);
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line(TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss')||' EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END generate_incd_forecast;        

   /*---------------------------------------------------------------------------------------------------------------*/
   /* Generates data on risk.                                                                                       */
   /* If the one logical run consists of multiple physical runs (eg 5 * 10_year run for a 50-year outlook), this    */
   /* procedure is to be run with the last run_id, after all runs have completed.                                   */
   /*---------------------------------------------------------------------------------------------------------------*/    
   PROCEDURE generate_post_del_risk_profile(i_base_run_id  IN PLS_INTEGER
                                           ,i_program_id   IN PLS_INTEGER    DEFAULT NULL
                                           ,i_years        IN PLS_INTEGER    DEFAULT NULL
                                           ,i_delete_flg   IN VARCHAR2       DEFAULT 'N') IS

   BEGIN
      
		structoolsapp.reporting.generate_post_del_risk_profile(i_base_run_id, i_program_id, i_years, i_delete_flg);
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line(TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss')||' EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END generate_post_del_risk_profile;        
   
   PROCEDURE generate_user_prog_risk_prof(i_user_id  IN VARCHAR2
                                        ,i_user_version   IN PLS_INTEGER
                                        ,i_years        IN PLS_INTEGER
                                        ,i_delete_flg   IN VARCHAR2       DEFAULT 'N') IS

   BEGIN
      
		structoolsapp.reporting.generate_user_prog_risk_prof(i_user_id, i_user_version, i_years, i_delete_flg);
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line(TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss')||' EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END generate_user_prog_risk_prof;        
   


   /*---------------------------------------------------------------------------------------------------------------*/
   /* Generates asset counts in all the different age values.                                                       */
   /* If the one logical run consists of multiple physical runs (eg 5 * 10_year run for a 50-year outlook), this    */
   /* procedure is to be rune with the last run_id, after all runs have completed.                                  */
   /*---------------------------------------------------------------------------------------------------------------*/    
   PROCEDURE generate_age_stats(i_program_id             IN PLS_INTEGER
                               ,i_base_run_id            IN PLS_INTEGER
                               ,i_years                  IN PLS_INTEGER
                               ,i_delete_flg             IN VARCHAR2    DEFAULT 'N') IS

   BEGIN
   
		structoolsapp.reporting.generate_age_stats(i_program_id, i_base_run_id, i_years, i_delete_flg);
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END generate_age_stats;        


   PROCEDURE generate_bays_seg_delta(i_base_run_id IN PLS_INTEGER
                                    ,i_program_id  IN PLS_INTEGER
                                    ,i_delete_flg  IN VARCHAR2    DEFAULT 'N') IS
      

   BEGIN

		structoolsapp.reporting.generate_bays_seg_delta(i_base_run_id, i_program_id, i_delete_flg); 
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END generate_bays_seg_delta;

   PROCEDURE calc_st_seg_maint_yr_cost(i_yr_maint_parm_scenario_id IN PLS_INTEGER DEFAULT 1
                                      ,i_segment_type IN PLS_INTEGER 
                                       ) IS
   
   BEGIN

		structoolsapp.reporting.CALC_ST_SEG_MAINT_YR_COST(i_yr_maint_parm_scenario_id, i_segment_type); 

   END calc_st_seg_maint_yr_cost;

   /**********************************************************************************************************************************************/
   /* Delete results of a strategy run. Deletes all rows related to the run, ie all programs and years (where applicable)                        */
   /* i_del_scope defines tables to be deleted. allowable values are:                                                                            */
   /*     ALL - will delete st_*_stratgey_run* tables and optimisation results tables and final summary tables.                                  */
   /*     OPT - will only delete optimisation results tables and final summary tables.                                                           */
   /*     TAB - will delete individual table from the predefined list of tables.                                                                 */
   /**********************************************************************************************************************************************/
   PROCEDURE delete_run (i_del_scope IN VARCHAR2 
                        ,i_table_nam IN VARCHAR2 DEFAULT NULL
                        ,i_qualifier  IN CHAR
                        ,i_del_run_tab IN structoolsapp.housekeeping.del_run_tab_typ) IS
         
   BEGIN
   
   	structoolsapp.housekeeping.delete_run(i_del_scope, i_table_nam, i_qualifier,i_del_run_tab);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END delete_run;

   /**********************************************************************************************************************************************/
   /* Delete results for run-program-year for as epcified table.                                                                                 */
   /* If i_year is null, it will delete all years for the run_id and program_id.                                                                 */
   /* Del stratgey id and scenario id are applicable only to st_del_program_reac_maint and st_del_program_sniper tables.                         */
   /**********************************************************************************************************************************************/
   PROCEDURE delete_program_year (i_table_nam                  IN VARCHAR2
                                 ,i_run_id                     IN PLS_INTEGER
                                 ,i_program_id                 IN PLS_INTEGER
                                 ,i_year                       IN PLS_INTEGER DEFAULT NULL
                                 ,i_del_strategy_id            IN PLS_INTEGER DEFAULT NULL
                                 ,i_del_strategy_scenario_id   IN PLS_INTEGER DEFAULT NULL) IS
   
   BEGIN

		   	structoolsapp.housekeeping.delete_program_year(i_table_nam, i_run_id, i_program_id, i_year, i_del_strategy_id, i_del_strategy_scenario_id);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END delete_program_year;

  PROCEDURE create_st_opt_program(i_PROGRAM_ID                      IN structools.st_opt_program.PROGRAM_ID%TYPE,
                                  i_PROGRAM_DESC                    IN structools.st_opt_program.PROGRAM_DESC%TYPE) IS
                   
   BEGIN
   
		structoolsapp.housekeeping.create_st_opt_program(i_program_id, i_program_desc);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

	END create_st_opt_program;
   
  PROCEDURE update_st_opt_program(i_PROGRAM_ID                      IN structools.st_opt_program.PROGRAM_ID%TYPE,
                                  i_PROGRAM_DESC                    IN structools.st_opt_program.PROGRAM_DESC%TYPE) IS
                   
   BEGIN

		structoolsapp.housekeeping.update_st_opt_program(i_program_id, i_program_desc);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END update_st_opt_program; 

  PROCEDURE delete_st_opt_program(i_PROGRAM_ID                      IN structools.st_opt_program.PROGRAM_ID%TYPE) IS
                   
   BEGIN

		structoolsapp.housekeeping.delete_st_opt_program(i_program_id);
	
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END delete_st_opt_program; 

  PROCEDURE create_st_opt_parm(i_OPT_PARM_NAME                      IN structools.st_opt_parm.OPT_PARM_NAME%TYPE,
                               i_OPT_PARM_DESC                      IN structools.st_opt_parm.OPT_PARM_DESC%TYPE) IS
                   
   BEGIN

		structoolsapp.housekeeping.create_st_opt_parm(i_OPT_PARM_NAME, i_OPT_PARM_DESC);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END create_st_opt_parm; 

  PROCEDURE update_st_opt_parm(i_OPT_PARM_NAME                      IN structools.st_opt_parm.OPT_PARM_NAME%TYPE,
                               i_OPT_PARM_DESC                      IN structools.st_opt_parm.OPT_PARM_DESC%TYPE) IS
                   
   BEGIN

		structoolsapp.housekeeping.create_st_opt_parm(i_OPT_PARM_NAME, i_OPT_PARM_DESC);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END update_st_opt_parm; 

  PROCEDURE delete_st_opt_parm(i_OPT_PARM_NAME                      IN structools.st_opt_parm.OPT_PARM_NAME%TYPE) IS
                   
   BEGIN

		structoolsapp.housekeeping.delete_st_opt_parm(i_OPT_PARM_NAME);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END delete_st_opt_parm; 

  PROCEDURE create_st_opt_program_parm_val(i_PROGRAM_ID             IN structools.st_opt_program_parm_val.PROGRAM_ID%TYPE,
                                           i_PARM_NAME              IN structools.st_opt_program_parm_val.PARM_NAME%TYPE,
                                           i_PARM_VAL               IN structools.st_opt_program_parm_val.PARM_VAL%TYPE) IS
                   
   BEGIN

		structoolsapp.housekeeping.create_st_opt_PROGRAM_parm_VAL(i_PROGRAM_ID, i_PARM_NAME, i_PARM_VAL);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END create_st_opt_program_parm_val;

  PROCEDURE update_st_opt_program_parm_val(i_PROGRAM_ID             IN structools.st_opt_program_parm_val.PROGRAM_ID%TYPE,
                                           i_PARM_NAME              IN structools.st_opt_program_parm_val.PARM_NAME%TYPE,
                                           i_PARM_VAL               IN structools.st_opt_program_parm_val.PARM_VAL%TYPE) IS
                   
   BEGIN

		structoolsapp.housekeeping.update_st_opt_program_parm_val(i_PROGRAM_ID, i_PARM_NAME, i_PARM_VAL);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END update_st_opt_program_parm_val;

  PROCEDURE delete_st_opt_program_parm_val(i_PROGRAM_ID             IN structools.st_opt_program_parm_val.PROGRAM_ID%TYPE,
                                           i_PARM_NAME              IN structools.st_opt_program_parm_val.PARM_NAME%TYPE) IS
                   

   BEGIN

		structoolsapp.housekeeping.delete_st_opt_program_parm_val(i_PROGRAM_ID, i_PARM_NAME);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END delete_st_opt_program_parm_val;

  PROCEDURE create_st_opt_program_run(i_PROGRAM_ID                  IN structools.st_opt_program_run.PROGRAM_ID%TYPE,
                                      i_RUN_ID                      IN structools.st_opt_program_run.RUN_ID%TYPE,
                                      i_PROGRAM_RUN_COMPONENT       IN structools.st_opt_program_run.PROGRAM_RUN_COMPONENT%TYPE,
                                      i_PROG_RUN_COMPONENT_STAT     IN structools.st_opt_program_run.PROG_RUN_COMPONENT_STAT%TYPE) IS
                   
   BEGIN

		structoolsapp.housekeeping.create_st_opt_program_run(i_PROGRAM_ID, i_RUN_ID, i_PROGRAM_RUN_COMPONENT, i_PROG_RUN_COMPONENT_STAT);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END create_st_opt_program_run;


  PROCEDURE update_st_opt_program_run(i_PROGRAM_ID                  IN structools.st_opt_program_run.PROGRAM_ID%TYPE,
                                      i_RUN_ID                      IN structools.st_opt_program_run.RUN_ID%TYPE,
                                      i_PROGRAM_RUN_COMPONENT       IN structools.st_opt_program_run.PROGRAM_RUN_COMPONENT%TYPE,
                                      i_PROG_RUN_COMPONENT_STAT     IN structools.st_opt_program_run.PROG_RUN_COMPONENT_STAT%TYPE) IS
                   
   BEGIN

		structoolsapp.housekeeping.update_st_opt_program_run(i_PROGRAM_ID, i_RUN_ID, i_PROGRAM_RUN_COMPONENT, i_PROG_RUN_COMPONENT_STAT);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END update_st_opt_program_run;

  PROCEDURE delete_st_opt_program_run(i_PROGRAM_ID                  IN structools.st_opt_program_run.PROGRAM_ID%TYPE,
                                      i_RUN_ID                      IN structools.st_opt_program_run.RUN_ID%TYPE,
                                      i_PROGRAM_RUN_COMPONENT       IN structools.st_opt_program_run.PROGRAM_RUN_COMPONENT%TYPE) IS
                                      
                   
   BEGIN

		structoolsapp.housekeeping.delete_st_opt_program_run(i_PROGRAM_ID, i_RUN_ID, i_PROGRAM_RUN_COMPONENT);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END delete_st_opt_program_run;

   PROCEDURE get_zbam_seg_reinf_counts(i_prog_type	IN VARCHAR2
                                             ,i_base_run_id	IN PLS_INTEGER
                                             ,i_program_id	IN PLS_INTEGER
                                             ,i_del_year			IN PLS_INTEGER
                                             ,o_p1_summ		OUT PLS_INTEGER
                                             ,o_p2_summ		OUT PLS_INTEGER
                                             ,o_p3_summ		OUT PLS_INTEGER
                                             ,o_p4_summ		OUT PLS_INTEGER
                                             ,o_p5_summ		OUT PLS_INTEGER
                                             ,o_p6_summ		OUT PLS_INTEGER
                                             ,o_p7_summ		OUT PLS_INTEGER
                                             ,o_p8_summ		OUT PLS_INTEGER
                                             ,o_p9_summ		OUT PLS_INTEGER) IS
   BEGIN

		structoolsapp.reporting.get_zbam_seg_reinf_counts(i_prog_type, i_base_run_id, i_program_id, i_del_year, o_p1_summ, o_p2_summ, o_p3_summ, o_p4_summ, o_p5_summ, o_p6_summ
      																							, o_p7_summ, o_p8_summ, o_p9_summ );

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

    END get_zbam_seg_reinf_counts;

/*****************************************************************/
/* Package initialisation                                        */
/*****************************************************************/

BEGIN

   NULL;
   
END structured_tools;
/
