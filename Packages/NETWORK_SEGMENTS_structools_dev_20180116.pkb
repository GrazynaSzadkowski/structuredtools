CREATE OR REPLACE PACKAGE BODY network_segments AS
/******************************************************************************************************************************************/
/* H5 Changes: */
/* egszadk 19/07/2016 - Created new procedure establish_segment_mtzn to update mtzn attribute based on max carrier length. */
/* */
/* H4 Changes: */
/*		Apply upper length limit to segments. */
/* Created procedure update_segment_type. */
/* Changed to take carrier model for oh conductors from table st_carrier_detail (field validated conductors). */
/* Chnaged to handle segments in a loop, when estbalishing spur id. */ 
/******************************************************************************************************************************************/

 v_null_end_dt CONSTANT DATE := TO_DATE('01-01-3000','dd-mm-yyyy');
 v_prog_excp EXCEPTION;
	 
	PROCEDURE create_segment_type(i_seg_type_descr		IN VARCHAR2) IS
 
	BEGIN

 INSERT INTO structools.nss_segment_type
 VALUES (structools.nss_segment_type_seq.NEXTVAL
 ,i_seg_type_descr
 )
 ;
 
 COMMIT;
 
 END create_segment_type; 
 
	PROCEDURE update_segment_type (i_seg_type			IN PLS_INTEGER
 										,i_seg_type_descr	IN VARCHAR2) IS
 
 	v_seg_type_descr			VARCHAR2(1024);
 
	BEGIN

		BEGIN
 SELECT segment_type_descr
 INTO v_seg_type_descr
 FROM	structools.nss_segment_type
 WHERE segment_type_id = i_seg_type;
 EXCEPTION
 	WHEN NO_DATA_FOUND THEN
 dbms_output.put_line('Segment type '||i_seg_type||' does not exist');
				RAISE;
 END;
 
 UPDATE	structools.nss_segment_type
 set		segment_type_descr = i_seg_type_descr
 WHERE segment_type_id = i_seg_type;

 dbms_output.put_line('Updated description of segment_type '||i_seg_type||' from '||v_seg_type_descr||' to '||i_seg_type_descr);
 
 COMMIT;
 
 END update_segment_type; 
 
	PROCEDURE drop_segment_indexes IS
 	
 BEGIN
 
 structools.schema_utils.disable_table_indexes('NSS_SEGMENT_ATTRIBUTE');
 structools.schema_utils.disable_table_indexes('NSS_SEGMENT_ASSET');
 structools.schema_utils.disable_table_indexes('NSS_SEGMENT');

	END drop_segment_indexes;
 

	PROCEDURE create_segment_indexes IS
 	
 	v_sql					VARCHAR2(1000);
 
 BEGIN

 structools.schema_utils.enable_table_indexes('NSS_SEGMENT');
 structools.schema_utils.enable_table_indexes('NSS_SEGMENT_ASSET');
 structools.schema_utils.enable_table_indexes('NSS_SEGMENT_ATTRIBUTE');

	END create_segment_indexes;
 
 PROCEDURE create_segment_type_1 (o_seg_id_first	OUT	PLS_INTEGER
 							 ,o_seg_id_last		OUT	PLS_INTEGER
 ,o_start_dt			OUT	VARCHAR2) IS
 /* sub-sections of protection zones, with entry point being a switch or a hvtm */
 
 
 v_carr_age_tolerance 	CONSTANT NUMBER := 10.0;
 v_backbone_spur_val 	CONSTANT NUMBER := 10.0;
 v_seg_len_upp_limit_metro			CONSTANT PLS_INTEGER := 2000;
 v_seg_len_upp_limit_goldfields	CONSTANT PLS_INTEGER := 5000;
 v_seg_len_upp_limit_s_country		CONSTANT PLS_INTEGER := 5000;
 v_seg_len_upp_limit_n_country		CONSTANT PLS_INTEGER := 5000;
 v_seg_len_upp_limit_default		CONSTANT PLS_INTEGER := 5000;
 v_segment_type_id CONSTANT PLS_INTEGER := 1;

 
 DUPLICATE_RECORD EXCEPTION;
 PRAGMA EXCEPTION_INIT (DUPLICATE_RECORD, -1); 

 v_end_dt 				DATE := SYSDATE;
 v_start_dt 				DATE := v_end_dt + (1 / 86400); --(60 * 60 * 24);

 v_segment_id NUMBER(12);
 v_prev_seg_id_id VARCHAR2(50); 
 
 TYPE seg_arr_typ IS TABLE OF NUMBER INDEX BY VARCHAR2(50);
 v_new_seg_arr seg_arr_typ; 
 
 v_section_parent_id PLS_INTEGER;
 v_ntwk_id PLS_INTEGER;
 v_pdev_id PLS_INTEGER;
 v_seg_id PLS_INTEGER;
 v_zone_sub VARCHAR2(100);
 v_mtzn VARCHAR2(100);
 j PLS_INTEGER;
 K PLS_INTEGER := 0; 
 l PLS_INTEGER;
 M PLS_INTEGER;
 n PLS_INTEGER;
 test_cnt PLS_INTEGER := 0;
 test_cnt1 PLS_INTEGER := 0;
 test_cnt2 PLS_INTEGER := 0;
 seg_first PLS_INTEGER;
 seg_last PLS_INTEGER;
 v_seg_min_avg_age NUMBER(5,1);
 v_seg_max_avg_age NUMBER(5,1);
 v_seg_min_age_tmp NUMBER(5,1);
 v_seg_max_age_tmp NUMBER(5,1);
 v_ntwk_id_prev PLS_INTEGER;
 v_pdev_id_prev PLS_INTEGER;
 v_ss_id_prev PLS_INTEGER;
 v_mtzn_prev VARCHAR2(100);
 v_section_id PLS_INTEGER;
 v_section_id_prev PLS_INTEGER;
 v_backbone_spur VARCHAR2(8);
 v_mat_cde VARCHAR2(20);
 v_ntwk_cust_cnt PLS_INTEGER;
 v_pdev_cust_cnt PLS_INTEGER; 
 v_seg_mat_cde_cnt PLS_INTEGER;
 v_seg_mat_cde VARCHAR2(20);
 v_sect_mat_cde_cnt PLS_INTEGER;
 v_sect_mat_cde VARCHAR2(20); 
 v_ntwk_cnt PLS_INTEGER := 0; 
 v_hvtm_cnt PLS_INTEGER := 0;
 v_seg_type VARCHAR2(4);
 v_mat_cde_match BOOLEAN;
 v_seg_carr_len_m NUMBER;
 v_seg_oh_ug_ind CHAR(2);
 v_seg_id_id VARCHAR2(20);
 v_seg_len_ok								BOOLEAN;
 v_rgn_nam									VARCHAR2(100);
 v_seg_len_upp_limit						PLS_INTEGER;
 v_pick_id									VARCHAR2(10);
		v_sql											VARCHAR2(1000); 
 v_dummy_int PLS_INTEGER;
 
 /* Cursor to get all sw sections, their protective devices and other ss details */ 
 CURSOR ntwk_ss_details_csr IS
 
 WITH pdevs AS
 (
 SELECT ntwk_id
 ,pdev_id
 ,MAX(pdev_mtr_cnt) AS pdev_mtr_cnt
 FROM
 ( 
 SELECT DISTINCT swsect_net_nid AS ntwk_id
 ,swsect_upstrm_protctn_dev_id AS pdev_id
 ,ss_prdev_ds_lv_mtr_cnt AS pdev_mtr_cnt
 FROM structoolsw.asset_topology 
 WHERE swsect_net_typ = 'HV'
 AND swsect_upstrm_protctn_dev_id > 0
 --AND swsect_net_nid IN (1032412) --graz 
 ) aa
 GROUP BY ntwk_id, pdev_id
 )
 ,
 ss_pdev_tmp AS
 (
 SELECT DISTINCT swsect_net_nid AS ntwk_id
 ,swsect_zone_substn_desc AS zone_sub 
 ,CASE swsect_nsw_nid
 WHEN 0 THEN swsect_net_nid
 ELSE swsect_nsw_nid
 END AS ss_id
 ,swsect_upstrm_protctn_dev_id AS pdev_id
 FROM structoolsw.asset_topology 
 WHERE swsect_net_typ = 'HV'
 --and swsect_upstrm_protctn_dev_id > 0
 AND asst_lyr_typ = 'N' AND (asst_id_no = swsect_nsw_nid OR asst_id_no = swsect_net_nid)
 --AND swsect_net_nid IN (1032412) --graz
 )
 ,
 ss_pdev_tmp1 AS
 (
 SELECT DISTINCT A.ntwk_id
 ,A.zone_sub 
 ,A.ss_id 
 ,NVL(b.pdev_id, A.pdev_id) AS pdev_id
 --,b.pdev_mtr_cnt as pdev_mtr_cnt
 FROM ss_pdev_tmp A LEFT OUTER JOIN
 pdevs b
 ON A.ntwk_id = b.ntwk_id
 AND A.ss_id = b.pdev_id
 )
 ,
 ss_pdev_tmp2 AS
 (
 SELECT DISTINCT A.ntwk_id
 ,A.ss_id 
 ,A.pdev_id
 ,A.zone_sub
 ,b.pdev_mtr_cnt AS pdev_mtr_cnt
 FROM ss_pdev_tmp1 A INNER JOIN
 pdevs b
 ON A.ntwk_id = b.ntwk_id
 AND A.pdev_id = b.pdev_id
 ORDER BY pdev_id, ss_id
 )
 ,
 ss_pdev_tmp3 AS
 (
 SELECT ntwk_id
 ,ss_id
 ,MIN(zone_sub) AS zone_sub
 ,MIN(pdev_id) AS pdev_id
 FROM ss_pdev_tmp2
 GROUP BY ntwk_id, ss_id 
 ) 
 ,
 ss_pdev AS
 (
 SELECT A.ntwk_id
 ,A.ss_id
 ,A.pdev_id
 ,A.zone_sub
 ,b.pdev_mtr_cnt
 ,'N'||A.ss_id AS ss_id_pickid
 FROM ss_pdev_tmp3 A INNER JOIN
 ss_pdev_tmp2 b
 ON A.ntwk_id = b.ntwk_id
 AND A.ss_id = b.ss_id
 AND A.pdev_id = b.pdev_id 
 )
 , 
 hv_carriers_tmp AS 
 (
 SELECT pick_id
 ,EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM created_date) AS carrier_age
 ,TO_CHAR(carrier_model_code) AS carrier_model_code
 ,'BB' AS oh_ug_ind
 FROM structoolsw.busbar_hv
 UNION
 SELECT pick_id
 ,EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM installation_date) AS carrier_age
 ,TO_CHAR(carrier_model_code) AS carrier_model_code
 ,'UG' AS oh_ug_ind
 FROM structoolsw.cable_hv
 UNION
 SELECT A.pick_id
 ,EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM A.installation_date) AS carrier_age
 --carrier_model_code --carrier model field val
 ,CASE
 WHEN b.carrier_model_code IS NOT NULL THEN TO_CHAR(b.carrier_model_code)
 ELSE TO_CHAR(A.carrier_model_code)
 END AS carrier_model_code
 ,'OH' AS oh_ug_ind
 FROM structoolsw.conductor_hv 
 A LEFT OUTER JOIN --carrier model field val
 structools.st_carrier_detail b
 ON A.pick_id = b.conductor_pick_id 
 )
 ,
 hv_carriers AS
 (
 SELECT A.pick_id
 ,A.carrier_age
 ,A.carrier_model_code
 ,A.oh_ug_ind
 ,ROUND(b.length_m) AS carrier_len_m
 ,c.material_cde
 FROM hv_carriers_tmp A LEFT OUTER JOIN
 structoolsw.carrier_detail b
 ON A.pick_id = b.pick_id
 LEFT OUTER JOIN
 structools.st_carr_mdl_material c
 ON A.carrier_model_code = TO_CHAR(c.carrier_mdl_cde) 
 )
 ,
 ntwk_ss AS
 (
 SELECT A.net_nid AS ntwk_id
 ,b.nsw_nid
 ,CASE 
 WHEN b.nsw_nid = 0 THEN A.net_nid
 ELSE b.nsw_nid
 END AS ss_id
 FROM structoolsw.ntwk A INNER JOIN
 structoolsw.net_sw_sect b
 ON A.net_nid = b.net_nid
 WHERE A.net_typ = 'HV' 
 --AND A.net_nid IN (1032412) --graz
 )
 ,
 ss_carriers AS
 (
 SELECT A.ntwk_id
 ,A.ss_id
 ,'C'||b.nec_nid AS pick_id
 ,b.nec_equip_typ AS EQP_TYP
 FROM ntwk_ss A INNER JOIN
 structoolsw.NET_SW_SECT_CAR b
 ON A.ntwk_id = b.net_nid
 AND A.nsw_nid = b.nsw_nid
 --AND A.ntwk_id IN (1032412) --graz
 AND B.NEC_EQUIP_TYP NOT IN ('FMET','FCAR') -- possible eqp types,'HVCO','HVSP','HVCU','HVBB','SPUG')
 )
 ,
 ss_oh_ug_type_tmp AS
 (
 SELECT A.ntwk_id
 ,A.ss_id
 ,b.oh_ug_ind
 ,COUNT(*) AS oh_ug_ind_cnt
 FROM ss_carriers A INNER JOIN
 hv_carriers b
 ON A.pick_id = b.pick_id 
 GROUP BY A.ntwk_id, A.ss_id, b.oh_ug_ind
 ORDER BY A.ntwk_id, A.ss_id
 )
 ,
 ss_oh_ug_type_tmp2 AS
 (
 SELECT A.ntwk_id
 ,A.ss_id
 ,MAX(A.oh_ug_ind_cnt) AS oh_ug_max_cnt
 FROM ss_oh_ug_type_tmp A 
 GROUP BY A.ntwk_id, A.ss_id
 ORDER BY A.ntwk_id, A.ss_id
 )
 ,
 ss_oh_ug_type AS
 (
 SELECT A.ntwk_id
 ,A.ss_id
 ,A.oh_ug_ind AS ss_oh_ug_ind
 FROM ss_oh_ug_type_tmp A INNER JOIN
 ss_oh_ug_type_tmp2 b
 ON A.ntwk_id = b.ntwk_id
 AND A.ss_id = b.ss_id
 AND A.oh_ug_ind_cnt = b.oh_ug_max_cnt 
 )
 ,
 ss_makeup AS
 (
 SELECT DISTINCT b.ntwk_id
 ,b.ss_id
 ,ROUND(AVG(A.carrier_age) OVER (PARTITION BY b.ntwk_id, b.ss_id)) AS avg_ss_carr_age
 ,NVL(A.material_cde,'UNK') AS material_cde
 ,ROUND(MIN(A.carrier_age) OVER (PARTITION BY b.ntwk_id, b.ss_id)) AS min_ss_carr_age
 ,ROUND(MAX(A.carrier_age) OVER (PARTITION BY b.ntwk_id, b.ss_id)) AS max_ss_carr_age
 ,SUM(A.carrier_len_m) OVER (PARTITION BY b.ntwk_id, b.ss_id) AS sum_carr_len_m
 ,c.ss_oh_ug_ind
 FROM ss_carriers b INNER JOIN
 ss_oh_ug_type c 
 ON b.ss_id = c.ss_id
 LEFT OUTER JOIN
 hv_carriers A 
 ON A.pick_id = b.pick_id 
 --ORDER BY b.ntwk_id, b.ss_id
 )
 ,
 ss_mtzn AS
 (
 SELECT A.ss_id
 ,b.maint_zone_nam
 FROM ss_pdev A INNER JOIN
-- rivaods.equipment_dim b 
 structoolsw.asset_geography b
 ON A.ss_id_pickid = b.pick_id
-- WHERE b.actv_flg = 'Y'
-- AND b.equip_stat = 'AC'
 )
 SELECT DISTINCT A.ntwk_id
 ,A.zone_sub
 ,A.pdev_id
 ,A.ss_id
 ,A.pdev_mtr_cnt
 ,b.avg_ss_carr_age
 ,b.min_ss_carr_age
 ,b.max_ss_carr_age
 ,b.material_cde AS ss_carr_mat_cde
 ,NVL(b.sum_carr_len_m, 0) AS carr_len_m
 ,c.maint_zone_nam AS mtzn
 ,b.ss_oh_ug_ind
 FROM ss_pdev A LEFT OUTER JOIN
 ss_makeup b
 ON A.ntwk_id = b.ntwk_id
 AND A.ss_id = b.ss_id
 LEFT OUTER JOIN
 ss_mtzn c
 ON A.ss_id = c.ss_id
 ORDER BY A.ntwk_id
 ,pdev_id
 ,ss_id
 ,ss_oh_ug_ind ;
 
 TYPE ntwk_ss_details_tab_typ IS TABLE OF ntwk_ss_details_csr%ROWTYPE; 
 v_ntwk_ss_details_tab ntwk_ss_details_tab_typ := ntwk_ss_details_tab_typ(); 
 
 CURSOR ss_with_hvtm_csr IS
 SELECT net_nid
 ,CASE nsw_nid
 WHEN 0 THEN net_nid
 ELSE nsw_nid
 END AS ss_id
 ,COUNT(*)
 FROM structoolsw.net_sw_sect_nod
 WHERE nen_equip_typ = 'HVTM'
 GROUP BY net_nid, nsw_nid 
 ;
 
 TYPE ss_with_hvtm_arr_typ IS TABLE OF CHAR(1) INDEX BY PLS_INTEGER;
 v_ss_with_hvtm_arr ss_with_hvtm_arr_typ;
 
 CURSOR mult_hvtm_csr IS
 WITH hvtm_mult_parent AS
 (
 SELECT pick_id
 FROM 
 (
 SELECT pick_id
 ,COUNT(*) AS cnt
 FROM structools.nss_temp_subsect_eqp
 WHERE eqp_type = 'HVTM'
 GROUP BY pick_id
 ) aa
 WHERE aa.cnt > 1
 )
 SELECT A.pick_id
 ,b.section_id
 ,b.section_type
 FROM hvtm_mult_parent A INNER JOIN
 structools.nss_temp_subsect_eqp b
 ON A.pick_id = b.pick_id 
 WHERE A.pick_id != b.section_id 
 ORDER BY b.pick_id 
 ;
 
 CURSOR section_age_mat_csr(p_ss_id PLS_INTEGER) IS
 WITH sect_carr_age AS
 (
 SELECT ss_id
 ,section_id
 ,section_type
 ,AVG(carrier_age) AS section_avg_age
 ,MIN(carrier_age) AS section_min_age
 ,MAX(carrier_age) AS section_max_age
 FROM structools.nss_temp_subsect_attr
 WHERE ss_id = p_ss_id
 GROUP BY ss_id
 ,section_id
 ,section_type
 )
 ,
 sect_carr_len AS
 (
 SELECT aa.section_id
 ,SUM(aa.carrier_len_m) AS section_carr_len_m
 ,MAX(oh_ug_ind) AS oh_ug_ind
 FROM 
 (
 SELECT DISTINCT carrier_pick_id
 ,carrier_len_m
 ,section_id
 ,oh_ug_ind
 FROM structools.nss_temp_subsect_attr 
 )aa
 GROUP BY aa.section_id
 )
 ,
 sect_carr_mat_cde AS
 (
 SELECT DISTINCT ss_id
 ,section_id
 ,section_type
 ,material_cde
 FROM structools.nss_temp_subsect_attr
 WHERE ss_id = p_ss_id
 --AND material_cde IS NOT NULL 
 )
 SELECT A.ss_id
 ,A.section_id
 ,A.section_type
 ,ROUND(A.section_avg_age,1) AS section_avg_age
 ,ROUND(A.section_min_age,1) AS section_min_age
 ,ROUND(A.section_max_age,1) AS section_max_age
 ,NVL(c.section_carr_len_m, 0) AS section_carr_len_m
 ,b.material_cde
 ,c.oh_ug_ind
 FROM sect_carr_age A INNER JOIN
 sect_carr_mat_cde b
 ON A.section_id = b.section_id 
 INNER JOIN
 sect_carr_len c
 ON A.section_id = c.section_id 
 ORDER BY ss_id, section_id
 ;
 
 TYPE section_det_rec_typ IS RECORD
 (pdev_id PLS_INTEGER
 ,carr_min_age NUMBER
 ,carr_max_age NUMBER
 ,carr_avg_age NUMBER
 ,carr_len_m NUMBER
 ,section_type VARCHAR2(4) /* 'SS' or 'HVTM' */
 ,split_ss BOOLEAN
 ,ss_id PLS_INTEGER
 ,mtzn VARCHAR2(100)
 ,oh_ug_ind CHAR(2)
 );
 v_section_det_rec section_det_rec_typ;

 TYPE section_det_arr_typ IS TABLE OF section_det_rec_typ INDEX BY PLS_INTEGER; --VARCHAR2(16); 
 v_section_det_arr section_det_arr_typ;
 
 TYPE mat_cde_arr_typ IS TABLE OF CHAR(1) INDEX BY VARCHAR2(20);
 v_mat_cde_arr mat_cde_arr_typ;
 v_mat_cde_arr2 mat_cde_arr_typ;
 
 TYPE sect_mat_cde_arr_typ IS TABLE OF mat_cde_arr_typ INDEX BY PLS_INTEGER;
 v_sect_mat_cde_arr sect_mat_cde_arr_typ;
 
 TYPE seg_mat_cde_arr_typ IS TABLE OF mat_cde_arr_typ INDEX BY VARCHAR2(20);
 v_seg_mat_cde_arr seg_mat_cde_arr_typ;
 
 TYPE ntwk_det_arr_typ IS TABLE OF INTEGER INDEX BY PLS_INTEGER; --VARCHAR2(16); 
 v_ntwk_det_arr ntwk_det_arr_typ;
 
 TYPE pdev_det_arr_typ IS TABLE OF INTEGER INDEX BY PLS_INTEGER; --VARCHAR2(16); 
 v_pdev_det_arr pdev_det_arr_typ;

 CURSOR child_section_csr_temp(i_parent_section_id IN INTEGER) IS 
 
 SELECT child_section_id AS section_id
 ,child_section_type AS section_type 
 FROM structools.nss_temp_subsect_hier
 WHERE ntwk_id= v_ntwk_id
 AND section_id = i_parent_section_id
 ORDER BY section_id
 ;

 CURSOR child_section_csr(i_parent_ss_id IN INTEGER) IS 
 
 SELECT nsw_nid AS section_id
 ,'SS ' AS section_type
 FROM structoolsw.net_sw_sect_trace
 WHERE NET_NID = v_ntwk_id
 AND nst_lvls_cnt = 1
 AND (nst_nid_upstream = i_parent_ss_id
 OR nst_nid_upstream = 0 AND net_nid = i_parent_ss_id)
 ORDER BY section_id
 ;

 TYPE potential_section_tab_typ IS TABLE OF child_section_csr%ROWTYPE;
 v_potential_section_tab potential_section_tab_typ := potential_section_tab_typ();
 
 TYPE seg_to_create_tab_typ IS TABLE OF INTEGER;
 v_seg_to_create_tab seg_to_create_tab_typ := seg_to_create_tab_typ();
 
 TYPE carrier_arr_typ IS TABLE OF CHAR(1) INDEX BY PLS_INTEGER;
 v_carrier_arr carrier_arr_typ;

 CURSOR segment_created_csr IS 
 SELECT b.segment_identifier_id AS n_segment_id_id
 ,b.ntwk_id AS n_ntwk_id
 ,b.prot_zone_id AS n_pdev_id
 ,b.seg_type AS n_seg_type
 ,b.carr_len_m AS n_carr_len_m
 ,b.b_s_ind AS n_b_s_ind
 ,B.MIN_AVG_CARR_AGE AS n_min_avg_carr_age
 ,B.MAX_AVG_CARR_AGE AS n_max_avg_carr_age
 ,B.CARR_MAT_CDE_CNT AS n_carr_mat_cde_cnt
 ,B.CARR_MAT_CDE AS n_carr_mat_cde
 ,B.ZONE_SUB AS n_zone_sub
 ,B.MTZN AS n_mtzn
 ,b.oh_ug_ind AS n_oh_ug_ind
 FROM structools.nss_temp_segment_detail b
 ORDER BY n_segment_id_id
 ,n_ntwk_id
 ,n_pdev_id
 ; 
 

 CURSOR segment_eqp_created_csr(i_segment_id_id IN VARCHAR2, i_ntwk_id IN PLS_INTEGER) IS 
 SELECT b.pick_id AS n_pick_id
 ,b.eqp_type
 FROM structools.nss_temp_segment_asset_detail b
 WHERE b.segment_identifier_id = i_segment_id_id
 AND b.ntwk_id = i_ntwk_id
 ; 
 
 
 PROCEDURE trace_sw_section(i_ntwk_id IN PLS_INTEGER, i_ss_id IN PLS_INTEGER, o_hvtm_cnt OUT PLS_INTEGER) IS 
 
 j PLS_INTEGER;
 K PLS_INTEGER;
 l PLS_INTEGER;
 wait_node_idx PLS_INTEGER;
 wait_hvtm_idx PLS_INTEGER;
 v_more_nodes BOOLEAN;
 v_node PLS_INTEGER;
 v_node_typ CHAR(4);
 v_first_car BOOLEAN;
 v_carr_age_tot NUMBER(5,1); 
 v_carr_cnt PLS_INTEGER;
 v_carr_id_prev PLS_INTEGER;
 v_cnt1 PLS_INTEGER := 0;
 v_cnt2 PLS_INTEGER := 0;
 v_cnt3 PLS_INTEGER := 0;
 v_parent_typ VARCHAR2(4);
 v_nod_fr_prev PLS_INTEGER;
 v_nod_fr PLS_INTEGER;
 v_nod_fr2 PLS_INTEGER;
 v_oh_ug CHAR(2);
 v_parent_oh_ug CHAR(2);
 v_parent_id PLS_INTEGER;
 v_ss_processed BOOLEAN; 
 v_potential_parent_fnd BOOLEAN;
 
 CURSOR ss_branch(p_ntwk_id IN PLS_INTEGER, p_ss_id IN PLS_INTEGER) IS
 WITH ntwk_ss AS
 (
 SELECT net_nid AS ntwk_id
 ,CASE NSW_NID
 WHEN 0 THEN net_nid
 ELSE nsw_nid
 END AS ss_id
 FROM structoolsw.net_sw_sect
 WHERE net_nid = p_ntwk_id
 )
 ,
 ntwk_ss_node AS
 (
 SELECT nen_nid
 FROM structoolsw.net_sw_sect_nod
 WHERE net_nid = p_ntwk_id
 AND (nsw_nid = p_ss_id OR nsw_nid = 0 AND p_ntwk_id = p_ss_id)
 )
 ,
 ss_branches AS
 ( 
 SELECT A.neb_fr_nod_nid
 ,A.neb_fr_nod_equip
 ,A.neb_to_nod_nid 
 ,A.neb_to_nod_equip
 ,'C'||A.neb_car_nid AS carr_pickid
 ,A.neb_car_equip_typ AS carr_eqp_typ
 ,CASE neb_fr_nod_equip
 WHEN 'PTSD' THEN 'Y'
 WHEN 'CCTB' THEN 'Y'
 WHEN 'DOF' THEN 'Y'
 WHEN 'FSSW' THEN 'Y'
 WHEN 'NLBC' THEN 'Y'
 WHEN 'CBDC' THEN 'Y'
 WHEN 'DISO' THEN 'Y'
 WHEN 'SWDC' THEN 'Y'
 WHEN 'RECL' THEN 'Y'
 WHEN 'LBS' THEN 'Y'
 WHEN 'SECT' THEN 'Y'
 WHEN 'FSDO' THEN 'Y'
 WHEN 'FSDU' THEN 'Y'
 WHEN 'DISU' THEN 'Y'
 ELSE 'N'
 END AS nod_fr_switch_ind
 ,CASE neb_to_nod_equip
 WHEN 'PTSD' THEN 'Y'
 WHEN 'CCTB' THEN 'Y'
 WHEN 'DOF' THEN 'Y'
 WHEN 'FSSW' THEN 'Y'
 WHEN 'NLBC' THEN 'Y'
 WHEN 'CBDC' THEN 'Y'
 WHEN 'DISO' THEN 'Y'
 WHEN 'SWDC' THEN 'Y'
 WHEN 'RECL' THEN 'Y'
 WHEN 'LBS' THEN 'Y'
 WHEN 'SECT' THEN 'Y'
 WHEN 'FSDO' THEN 'Y'
 WHEN 'FSDU' THEN 'Y'
 WHEN 'DISU' THEN 'Y'
 ELSE 'N'
 END AS nod_to_switch_ind
 ,NVL(b.ss_id, 0) AS ss_id
 FROM structoolsw.net_brh A LEFT OUTER JOIN
 ntwk_ss b
 ON A.net_nid = b.ntwk_id
 AND A.neb_to_nod_nid = b.ss_id
 WHERE A.net_nid = p_ntwk_id
 AND A.neb_to_nod_nid != p_ss_id
 AND (A.neb_fr_nod_nid IN (SELECT nen_nid FROM ntwk_ss_node)
 OR A.neb_to_nod_nid IN (SELECT nen_nid FROM ntwk_ss_node))
 )
 ,
 ss_carrier AS
 (
 SELECT DISTINCT carr_pickid
 ,carr_eqp_typ
 FROM ss_branches
 WHERE carr_eqp_typ NOT IN ('FCAR','FMET')
 )
 ,
 ss_carrier_age AS
 (
 SELECT A.carr_pickid
 ,A.carr_eqp_typ
 ,CASE A.carr_eqp_typ
 WHEN 'HVCO' THEN EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM b.installation_date)
 WHEN 'HVSP' THEN EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM b.installation_date)
 WHEN 'HVCU' THEN EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM c.installation_date)
 WHEN 'SPUG' THEN EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM c.installation_date)
 WHEN 'HVBB' THEN EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM d.created_date)
 END AS carrier_age
 ,CASE
 WHEN A.carr_eqp_typ = 'HVCO' AND E.carrier_model_code IS NULL THEN TO_CHAR(b.carrier_model_code) ----carrier model field val
 WHEN A.carr_eqp_typ = 'HVCO' AND E.carrier_model_code IS NOT NULL THEN TO_CHAR(E.carrier_model_code)
 WHEN A.carr_eqp_typ = 'HVSP' AND E.carrier_model_code IS NULL THEN TO_CHAR(b.carrier_model_code)
 WHEN A.carr_eqp_typ = 'HVSP' AND E.carrier_model_code IS NOT NULL THEN TO_CHAR(E.carrier_model_code)
-- WHEN A.carr_eqp_typ = 'HVCO' THEN b.carrier_model_code 
-- WHEN A.carr_eqp_typ = 'HVSP' THEN b.carrier_model_code
 WHEN A.carr_eqp_typ = 'HVCU' THEN TO_CHAR(c.carrier_model_code)
 WHEN A.carr_eqp_typ = 'SPUG' THEN TO_CHAR(c.carrier_model_code)
 WHEN A.carr_eqp_typ = 'HVBB' THEN TO_CHAR(d.carrier_model_code)
 END AS carrier_model_cde
 FROM ss_carrier A LEFT OUTER JOIN
 structoolsw.conductor_hv b
 ON A.carr_pickid = b.pick_id
 LEFT OUTER JOIN
 structoolsw.cable_hv c
 ON A.carr_pickid = c.pick_id
 LEFT OUTER JOIN
 structoolsw.busbar_hv d
 ON A.carr_pickid = d.pick_id
 LEFT OUTER JOIN --carrier model field val
 structools.st_carrier_detail E
 ON A.carr_pickid = E.conductor_pick_id
 )
 ,
 ss_carrier_age_mat AS
 (
 SELECT A.carr_pickid
 ,A.carrier_age
 ,ROUND(b.length_m) AS carrier_len_m
 ,c.material_cde
 FROM ss_carrier_age A LEFT OUTER JOIN
 structoolsw.carrier_detail b
 ON A.carr_pickid = b.pick_id 
 LEFT OUTER JOIN
 structools.st_carr_mdl_material c 
 ON A.carrier_model_cde = TO_CHAR(c.carrier_mdl_cde) 
 )
 SELECT A.neb_fr_nod_nid
 ,A.neb_fr_nod_equip
 ,A.neb_to_nod_nid 
 ,A.neb_to_nod_equip
 ,TO_NUMBER(SUBSTR(A.carr_pickid,2,15)) AS carr_pickid
 ,A.carr_eqp_typ
 ,A.nod_fr_switch_ind
 ,A.nod_to_switch_ind
 ,A.ss_id
 ,b.carrier_age
 ,CASE 
 WHEN b.carrier_age IS NOT NULL AND b.material_cde IS NULL THEN 'UNK'
 ELSE b.material_cde
 END AS material_cde
 ,b.carrier_len_m
 FROM ss_branches A LEFT OUTER JOIN
 ss_carrier_age_mat b
 ON A.carr_pickid = b.carr_pickid 
 WHERE (A.neb_fr_nod_nid = p_ss_id
 OR nod_fr_switch_ind = 'N')
 ORDER BY A.neb_fr_nod_nid, A.neb_to_nod_nid 
 ;
 
 TYPE processed_branch_tab_typ IS TABLE OF CHAR(1) INDEX BY PLS_INTEGER;
 v_processed_branch_tab processed_branch_tab_typ;
 
 TYPE parent_eqp_rec_typ IS RECORD
 (
 eqp_typ CHAR(4),
 parent_id INTEGER,
 parent_typ VARCHAR2(4)
 )
 ;
 
 TYPE branch_rec_typ IS RECORD
 (
 nod_fr_eqp_typ CHAR(4),
 nod_to PLS_INTEGER,
 nod_to_eqp_typ CHAR(4),
 carr_id PLS_INTEGER,
 carr_eqp_typ CHAR(4),
 nod_fr_switch_ind CHAR(1),
 nod_to_switch_ind CHAR(1),
 ss_id PLS_INTEGER,
 carrier_age NUMBER(5,1),
 carrier_len_m NUMBER,
 material_cde VARCHAR2(20),
 parent_id PLS_INTEGER,
 parent_oh_ug CHAR(2),
 parent_typ VARCHAR2(4),
 processed CHAR(1)
 )
 ;
 v_branch_rec branch_rec_typ;
 
 TYPE branch_tab_typ IS TABLE OF branch_rec_typ;
 v_branch_tab branch_tab_typ := branch_tab_typ();
 v_branch_tab2 branch_tab_typ := branch_tab_typ();
 
 TYPE branch_arr_typ IS TABLE OF branch_tab_typ INDEX BY PLS_INTEGER;
 v_branch_arr branch_arr_typ;
 
 
 
 BEGIN
 /* Get first node (switch) of the sw section and put it on the list of waiting nodes */
 
 wait_node_idx := 0;
 wait_hvtm_idx := 0;
 
 j := 1;
 
 v_more_nodes := TRUE;
 v_node := i_ss_id;
 v_node_typ := 'SS';

 
 v_first_car := TRUE;
 v_carr_age_tot := 0;
 v_carr_cnt := 0;
 v_carr_id_prev := 0;
 --dbms_output.put_line('TRACING sw SECTION. Ntwk = '||i_ntwk_id||' ss_id = '||i_ss_id); --graz
 
 /* collect all branches for the sw. section in a table, indexed by node_from */
 v_nod_fr_prev := 0;
 o_hvtm_cnt := 0;
 FOR c IN ss_branch(i_ntwk_id, i_ss_id) LOOP
 --dbms_output.put_line('got branch');
 v_nod_fr := c.neb_fr_nod_nid;
 IF c.nod_fr_switch_ind = 'N' OR v_nod_fr = i_ss_id THEN 
 v_nod_fr := c.neb_fr_nod_nid;
 IF v_nod_fr != v_nod_fr_prev THEN
 IF v_nod_fr_prev != 0 THEN
 v_branch_arr(v_nod_fr_prev) := v_branch_tab;
 END IF;
 v_branch_tab.DELETE;
 l := 0;
 v_nod_fr_prev := v_nod_fr; 
 END IF;
 IF c.neb_fr_nod_equip = 'HVTM' THEN
 o_hvtm_cnt := o_hvtm_cnt + 1;
 END IF;
 v_branch_rec.nod_fr_eqp_typ := c.neb_fr_nod_equip;
 v_branch_rec.nod_to := c.neb_to_nod_nid;
 v_branch_rec.nod_to_eqp_typ := c.neb_to_nod_equip;
 v_branch_rec.carr_id := c.carr_pickid;
 v_branch_rec.carr_eqp_typ := c.carr_eqp_typ;
 v_branch_rec.nod_fr_switch_ind := c.nod_fr_switch_ind;
 v_branch_rec.nod_to_switch_ind := c.nod_to_switch_ind;
 v_branch_rec.ss_id := c.ss_id;
 v_branch_rec.carrier_age := c.carrier_age;
 v_branch_rec.carrier_len_m := c.carrier_len_m;
 v_branch_rec.material_cde := c.material_cde;
 v_branch_rec.parent_id := NULL;
 v_branch_rec.parent_typ := NULL;
 v_branch_rec.parent_oh_ug := NULL;
 v_branch_rec.processed := ' ';
 v_branch_tab.EXTEND;
 l := l + 1;
 v_branch_tab(l) := v_branch_rec;
 END IF;
 END LOOP;
 v_branch_arr(v_nod_fr_prev) := v_branch_tab;
 
-- IF i_ss_id = 1032412 THEN --graz
-- v_nod_fr := v_branch_arr.FIRST;
-- dbms_output.put_line('INITIAL branch TABLE'); 
-- WHILE v_nod_fr IS NOT NULL LOOP
-- v_branch_tab := v_branch_arr(v_nod_fr);
-- FOR i IN 1 .. v_branch_tab.COUNT LOOP
-- dbms_output.put_line(v_nod_fr||' '||v_branch_tab(i).nod_fr_eqp_typ
-- ||' '||v_branch_tab(i).nod_to
-- ||' '||v_branch_tab(i).nod_to_eqp_typ
-- ||' '||v_branch_tab(i).carr_id
-- ||' '||v_branch_tab(i).carr_eqp_typ
-- ||' '||v_branch_tab(i).nod_fr_switch_ind
-- ||' '||v_branch_tab(i).nod_to_switch_ind
-- ||' '||v_branch_tab(i).ss_id
-- ||' '||v_branch_tab(i).carrier_age
-- ||' '||v_branch_tab(i).carrier_len_m
-- ||' '||v_branch_tab(i).material_cde);
-- END LOOP;
-- v_nod_fr := v_branch_arr.NEXT(v_nod_fr);
-- END LOOP;
-- END IF; 

 /* identify entry switch to the sw section */
 v_nod_fr := v_branch_arr.FIRST;

 IF v_nod_fr IS NULL THEN
 dbms_output.put_line('!!!NO branches FOUND FOR ntwk '||i_ntwk_id); --handle corrupt data (net_sw_sec not in sync with net_brh)
 GOTO exit_proc;
 END IF; 

 WHILE v_nod_fr IS NOT NULL LOOP
 IF v_nod_fr = i_ss_id THEN
 EXIT;
 END IF;
 v_nod_fr := v_branch_arr.NEXT(v_nod_fr);
 END LOOP;

 IF v_nod_fr IS NULL THEN
 dbms_output.put_line('!!!NO branch FOUND FOR ntwk '||i_ntwk_id||' ss '||i_ss_id); --handle corrupt data (net_sw_sec not in sync with net_brh)
 GOTO exit_proc;
 END IF; 

 --dbms_output.put_line('IDENTIFIED ENTRY SWITCH IN THE TABLE '||v_nod_fr); --graz
 v_branch_tab := v_branch_arr(v_nod_fr);
 
 /*identify the carrier type of the initial branch */
 v_oh_ug := ' ';
 WHILE v_oh_ug = ' ' LOOP
 
 FOR i IN 1 .. v_branch_tab.COUNT LOOP
 IF v_branch_tab(i).carr_eqp_typ != 'FCAR' AND v_branch_tab(i).carr_eqp_typ != 'FMET' THEN
 IF v_branch_tab(i).carr_eqp_typ = 'HVCO' OR v_branch_tab(i).carr_eqp_typ = 'HVSP' THEN
 v_oh_ug := 'OH';
 ELSIF v_branch_tab(i).carr_eqp_typ = 'HVCU' OR v_branch_tab(i).carr_eqp_typ = 'SPUG' THEN
 v_oh_ug := 'UG';
 ELSIF v_branch_tab(i).carr_eqp_typ = 'HVBB' THEN
 v_oh_ug := 'BB';
 ELSE
 dbms_output.put_line('unknown carrier TYPE '||i_ntwk_id||' '||i_ss_id||' '||v_nod_fr);
 RAISE v_prog_excp;
 END IF; 
 EXIT;
 END IF;
 END LOOP;
 IF v_oh_ug = ' ' THEN
 FOR i IN 1 .. v_branch_tab.COUNT LOOP
 IF v_branch_arr.EXISTS(v_branch_tab(i).nod_to) THEN
 v_branch_tab := v_branch_arr(v_branch_tab(i).nod_to);
 EXIT;
 END IF;
 IF i = v_branch_tab.COUNT THEN
 dbms_output.put_line('INITIAL carrier TYPE NOT IDENTIFIED. Branch DATA corrupt; ntwk = '||i_ntwk_id||' ss = '||i_ss_id);
 GOTO exit_proc;
 --RAISE v_prog_excp;
 END IF;
 END LOOP;
 END IF;
 END LOOP;
 --dbms_output.put_line('INITIAL carrier TYPE '||v_oh_ug); --graz

 v_branch_tab := v_branch_arr(v_nod_fr); -- table of branches for the entry switch
 v_parent_oh_ug := v_oh_ug;
 v_parent_id := v_nod_fr;
 v_parent_typ := 'SS';
 
 /* set parent_id for all matching branches from the entry switch */
 FOR i IN 1 .. v_branch_tab.COUNT LOOP
 IF (v_branch_tab(i).carr_eqp_typ = 'HVCO' OR v_branch_tab(i).carr_eqp_typ = 'HVSP')
 AND v_parent_oh_ug = 'OH' 
 OR (v_branch_tab(i).carr_eqp_typ = 'HVCU' OR v_branch_tab(i).carr_eqp_typ = 'SPUG')
 AND v_parent_oh_ug = 'UG' 
 OR v_branch_tab(i).carr_eqp_typ = 'HVBB'
 AND v_parent_oh_ug = 'BB' 
 OR v_branch_tab(i).carr_eqp_typ = 'FCAR' OR v_branch_tab(i).carr_eqp_typ = 'FMET' OR v_branch_tab(i).carr_id IS NULL THEN
 v_branch_tab(i).parent_id := v_parent_id;
 v_branch_tab(i).parent_oh_ug := v_parent_oh_ug;
 v_branch_tab(i).parent_typ := v_parent_typ;
 END IF;
 END LOOP;
 v_branch_arr(v_nod_fr) := v_branch_tab;
 
 v_nod_fr := v_branch_arr.FIRST;
 v_ss_processed := FALSE;
 
 /* Establish parent (sw section id or HVTM) for each branch */
 v_cnt1 := 0;
 WHILE NOT v_ss_processed LOOP
 v_cnt1 := v_cnt1 + 1;
 IF v_cnt1 > 10000 THEN
 dbms_output.put_line('looping 1 '||i_ntwk_id||' '||i_ss_id);
 RAISE v_prog_excp;
 END IF;
 
 v_branch_tab := v_branch_arr(v_nod_fr);
 
 -- dbms_output.put_line('0 v_nod_fr = '||v_nod_fr||' branch tab COUNT = '||v_branch_tab.count);
 v_cnt2 := 0;
 v_potential_parent_fnd := FALSE;
 FOR i IN 1 .. v_branch_tab.COUNT LOOP
 -- IF i = 1 THEN
 -- dbms_output.put_line('v_nod_fr = '||v_nod_fr||' '||v_branch_tab(1).nod_fr_eqp_typ);
 -- END IF;
 v_cnt2 := v_cnt2 + 1;
 IF v_cnt2 > 100 THEN
 dbms_output.put_line('looping 2 '||i_ntwk_id||' '||i_ss_id);
 RAISE v_prog_excp;
 END IF;
 IF v_branch_tab(i).parent_id IS NOT NULL AND v_branch_tab(i).processed = ' ' THEN
 v_potential_parent_fnd := TRUE;
 v_parent_id := v_branch_tab(i).parent_id;
 v_parent_oh_ug := v_branch_tab(i).parent_oh_ug;
 v_parent_typ := v_branch_tab(i).parent_typ;
 v_branch_tab(i).processed := 'Y';
 v_branch_arr(v_nod_fr) := v_branch_tab;
 -- dbms_output.put_line('1-set TO processed FOR node '||v_nod_fr||' i = '||i||' '||v_branch_tab(i).nod_to); --graz
 v_nod_fr2 := v_branch_tab(i).nod_to;
 -- dbms_output.put_line('1-v_nod_fr2 set TO '||v_nod_fr2);
 --IF v_branch_tab(i).nod_to_eqp_typ = 'HVTM' THEN
 -- dbms_output.put_line('v_nod_fr2 = '||v_nod_fr2||' '||v_branch_tab(i).nod_to_eqp_typ);
 --END IF;
 IF v_branch_arr.EXISTS(v_nod_fr2) THEN 
 v_potential_parent_fnd := TRUE;
 v_branch_tab2 := v_branch_arr(v_nod_fr2);
 v_cnt3 := 0;
 FOR x IN 1 .. v_branch_tab2.COUNT LOOP
 -- IF v_nod_fr2 = 5482262 OR v_nod_fr2 = 90144655 OR v_nod_fr2 = 5471387 THEN
 -- dbms_output.put_line('processing CHILD OF '||v_nod_fr2||' '||v_branch_tab2(x).nod_to||' '||v_branch_tab2(x).nod_to_eqp_typ);
 -- END IF;
 v_cnt3 := v_cnt3 + 1;
 IF v_cnt3 > 100 THEN
 dbms_output.put_line('looping 3 '||i_ntwk_id||' '||i_ss_id);
 RAISE v_prog_excp;
 END IF;
 IF v_branch_tab2(x).parent_id IS NULL THEN
 -- IF v_nod_fr2 = 5482262 OR v_nod_fr2 = 90144655 OR v_nod_fr2 = 5471387 THEN
 -- dbms_output.put_line('allocating PARENT ID, v_parent_oh_ug = '||v_parent_oh_ug);
 -- dbms_output.put_line('v_branch_tab2(x).carr_eqp_typ = '||v_branch_tab2(x).carr_eqp_typ);
 -- dbms_output.put_line('v_branch_tab2(x).nod_fr_eqp_typ = '||v_branch_tab2(x).nod_fr_eqp_typ);
 -- dbms_output.put_line('v_branch_tab2(x).parent_typ = '||v_parent_typ);
 -- END IF;
 IF (v_branch_tab2(x).carr_eqp_typ = 'HVCO' OR v_branch_tab2(x).carr_eqp_typ = 'HVSP')
 AND v_parent_oh_ug = 'OH'
 OR (v_branch_tab2(x).carr_eqp_typ = 'HVCU' OR v_branch_tab2(x).carr_eqp_typ = 'SPUG')
 AND v_parent_oh_ug = 'UG' 
 OR v_branch_tab2(x).carr_eqp_typ = 'HVBB'
 AND v_parent_oh_ug = 'BB' THEN 
 v_branch_tab2(x).parent_id := v_parent_id;
 v_branch_tab2(x).parent_oh_ug := v_parent_oh_ug;
 v_branch_tab2(x).parent_typ := v_parent_typ;
 -- IF v_nod_fr2 = 5482262 OR v_nod_fr2 = 90144655 OR v_nod_fr2 = 5471387 THEN
 -- dbms_output.put_line('allocated PARENT(1) '||v_parent_id||' v_parent_oh_ug = '||v_parent_oh_ug
 -- ||' '||v_parent_typ);
 -- END IF;
 ELSIF v_branch_tab2(x).carr_eqp_typ = 'FCAR' OR v_branch_tab2(x).carr_eqp_typ = 'FMET' OR v_branch_tab2(x).carr_id IS NULL THEN
 IF v_branch_tab2(x).nod_fr_eqp_typ = 'HVTM' THEN
 v_branch_tab2(x).parent_id := v_nod_fr2;
 v_branch_tab2(x).parent_oh_ug := ' ';
 v_branch_tab2(x).parent_typ := 'HVTM'; 
 -- IF v_nod_fr2 = 5482262 OR v_nod_fr2 = 90144655 OR v_nod_fr2 = 5471387 THEN
 -- dbms_output.put_line('allocated PARENT(2) '||v_nod_fr2||' v_parent_oh_ug = blank'||' HVTM');
 -- END IF;
 ELSE
 v_branch_tab2(x).parent_id := v_parent_id;
 v_branch_tab2(x).parent_oh_ug := v_parent_oh_ug;
 v_branch_tab2(x).parent_typ := v_parent_typ;
 -- IF v_nod_fr2 = 5482262 OR v_nod_fr2 = 90144655 OR v_nod_fr2 = 5471387 THEN
 -- dbms_output.put_line('allocated PARENT(3) '||v_parent_id||' v_parent_oh_ug = '||v_parent_oh_ug
 -- ||' '||v_parent_typ);
 -- END IF;
 END IF;
 ELSIF v_parent_typ = 'HVTM' 
 OR v_branch_tab2(x).nod_fr_eqp_typ = 'HVTM' THEN
 IF v_branch_tab2(x).nod_fr_eqp_typ = 'HVTM' THEN
 v_branch_tab2(x).parent_id := v_nod_fr2;
 ELSE
 v_branch_tab2(x).parent_id := v_parent_id;
 END IF;
 v_branch_tab2(x).parent_typ := 'HVTM';
 IF (v_branch_tab2(x).carr_eqp_typ = 'HVCO' OR v_branch_tab2(x).carr_eqp_typ = 'HVSP') THEN
 v_branch_tab2(x).parent_oh_ug := 'OH';
 ELSIF (v_branch_tab2(x).carr_eqp_typ = 'HVCU' OR v_branch_tab2(x).carr_eqp_typ = 'SPUG') THEN
 v_branch_tab2(x).parent_oh_ug := 'UG';
 ELSE 
 v_branch_tab2(x).parent_oh_ug := 'BB';
 END IF;
 -- IF v_nod_fr2 = 5482262 OR v_nod_fr2 = 90144655 OR v_nod_fr2 = 5471387 THEN
 -- dbms_output.put_line('allocated PARENT(4) '||v_parent_id||' v_parent_oh_ug = '
 -- ||v_branch_tab2(x).parent_oh_ug||' HVTM');
 -- END IF;
 END IF;
 END IF;
 END LOOP;
 v_branch_arr(v_nod_fr2) := v_branch_tab2;
 -- dbms_output.put_line('2-Set PARENT FOR node '||v_nod_fr2||' '||v_branch_tab2.COUNT||' entries'); --graz
 EXIT;
 END IF;
 END IF;
 END LOOP;
 IF NOT v_potential_parent_fnd THEN
 v_nod_fr := v_branch_arr.NEXT(v_nod_fr);
 -- dbms_output.put_line('2-v_nod_fr set TO '||v_nod_fr);
 IF v_nod_fr IS NULL THEN
 v_ss_processed := TRUE;
 END IF;
 ELSE
 v_nod_fr := v_branch_arr.FIRST;
 -- dbms_output.put_line('3-v_nod_fr set TO '||v_nod_fr);
 END IF;
 END LOOP;
 
-- IF i_ss_id = 1032412 THEN --graz
-- v_nod_fr := v_branch_arr.FIRST;
-- dbms_output.put_line('branch TABLE WITH sections assigned'); 
-- WHILE v_nod_fr IS NOT NULL LOOP
-- v_branch_tab := v_branch_arr(v_nod_fr);
-- FOR i IN 1 .. v_branch_tab.COUNT LOOP
-- dbms_output.put_line(v_nod_fr||' '||v_branch_tab(i).nod_fr_eqp_typ
-- ||' '||v_branch_tab(i).nod_to
-- ||' '||v_branch_tab(i).nod_to_eqp_typ
-- ||' '||v_branch_tab(i).carr_id
-- ||' '||v_branch_tab(i).carr_eqp_typ
-- ||' '||v_branch_tab(i).nod_fr_switch_ind
-- ||' '||v_branch_tab(i).nod_to_switch_ind
-- ||' '||v_branch_tab(i).ss_id
-- ||' '||v_branch_tab(i).carrier_age
-- ||' '||v_branch_tab(i).carrier_len_m
-- ||' '||v_branch_tab(i).material_cde
-- ||' '||v_branch_tab(i).parent_id
-- ||' '||v_branch_tab(i).parent_oh_ug
-- ||' '||v_branch_tab(i).processed
-- );
-- END LOOP;
-- v_nod_fr := v_branch_arr.NEXT(v_nod_fr);
-- END LOOP;
-- END IF; 

 
 IF o_hvtm_cnt > 0 THEN
 --dbms_output.put_line('INSERTING SECTION nodes INTO nss_temp_subsect_eqp TABLE'); --graz
 v_nod_fr := v_branch_arr.FIRST;
 WHILE v_nod_fr IS NOT NULL LOOP
 -- dbms_output.put_line('v_nod_fr = '||v_nod_fr);
 v_branch_tab := v_branch_arr(v_nod_fr);
 -- IF v_branch_tab(1).nod_fr_eqp_typ = 'HVTM' THEN
 -- v_hvtm_is_parent := FALSE;
 -- END IF;
 FOR i IN 1 .. v_branch_tab.COUNT LOOP
 -- dbms_output.put_line(' nod fr eqp typ = '||v_branch_tab(i).nod_fr_eqp_typ);
 IF v_branch_tab(i).parent_id IS NOT NULL THEN
 IF v_branch_tab(i).parent_id = i_ss_id THEN
 v_parent_typ := 'SS';
 ELSE
 v_parent_typ := 'HVTM';
 END IF;
 /* Insert into a temp table branch details with material code and carrier age for each branch. */
 /* This is for easy establishing material code and average age per section (mat codes sometimes differ */
 /* inside a section too */
 INSERT INTO structools.nss_temp_subsect_attr
 VALUES (i_ss_id
 ,v_branch_tab(i).parent_id
 ,v_parent_typ
 ,v_branch_tab(i).material_cde
 ,v_branch_tab(i).carrier_age
 ,v_branch_tab(i).carrier_len_m
 ,v_branch_tab(i).carr_id
 ,v_branch_tab(i).parent_oh_ug
 );
 -- /* Insert into a temp table of sub-sections and its children sections,ie only where the child node is another */
 /* sw section or a HVTM. This is for tracing sections and combining into segments in the mainline */
-- IF v_branch_tab(i).parent_id = 4786269 THEN
-- dbms_output.put_line('HIERARCHY FROM '||4786269);
-- END IF;
 IF v_processed_branch_tab.EXISTS(v_branch_tab(i).nod_to) THEN
 NULL;
-- IF v_branch_tab(i).parent_id = 4786269 THEN
-- dbms_output.put_line('Processed_branch EXISTS FOR nod_to '||v_branch_tab(i).nod_to);
-- END IF;
 ELSE
 v_processed_branch_tab(v_branch_tab(i).nod_to) := ' ';
-- IF v_branch_tab(i).parent_id = 4786269 THEN
-- dbms_output.put_line('Added '||v_branch_tab(i).nod_to||' TO processed_branch TABLE');
-- dbms_output.put_line('v_branch_tab(i).ss_id = '||v_branch_tab(i).ss_id);
-- dbms_output.put_line('v_branch_tab(i).nod_to_eqp_typ = '||v_branch_tab(i).nod_to_eqp_typ);
-- END IF;
 
 IF v_branch_tab(i).ss_id != 0 THEN
 INSERT INTO structools.nss_temp_subsect_hier
 VALUES(i_ntwk_id
 ,v_branch_tab(i).parent_id
 ,v_parent_typ
 ,v_branch_tab(i).nod_to
 ,'SS'
 );
 ELSIF v_branch_tab(i).nod_to_eqp_typ = 'HVTM' THEN
 INSERT INTO structools.nss_temp_subsect_hier
 VALUES(i_ntwk_id
 ,v_branch_tab(i).parent_id
 ,v_parent_typ
 ,v_branch_tab(i).nod_to
 ,'HVTM'
 );
 END IF;
 END IF;
 /* Record all equipment per sw subsection */
 IF v_branch_tab(i).nod_fr_eqp_typ != 'FNOD' THEN
 BEGIN
 INSERT INTO structools.nss_temp_subsect_eqp
 VALUES (i_ss_id
 ,v_branch_tab(i).parent_id
 ,v_parent_typ
 ,v_nod_fr
 ,v_branch_tab(i).nod_fr_eqp_typ
 );
 EXCEPTION
 WHEN DUPLICATE_RECORD THEN
 NULL;
 END;
 -- IF v_branch_tab(i).nod_fr_eqp_typ = 'HVTM' AND v_nod_fr = v_branch_tab(i).parent_id THEN
 -- v_hvtm_is_parent := TRUE;
 -- END IF; 
 END IF;
 IF v_branch_tab(i).nod_to_eqp_typ != 'FNOD' THEN --AND v_branch_tab(i).nod_to_eqp_typ != 'HVTM'
 --AND v_branch_tab(i).ss_id = 0 THEN
 BEGIN
 INSERT INTO structools.nss_temp_subsect_eqp
 VALUES (i_ss_id
 ,v_branch_tab(i).parent_id
 ,v_parent_typ
 ,v_branch_tab(i).nod_to
 ,v_branch_tab(i).nod_to_eqp_typ
 )
 ; 
 EXCEPTION
 WHEN DUPLICATE_RECORD THEN
 NULL;
 END;

 END IF;
 IF v_branch_tab(i).carr_eqp_typ != 'FCAR' AND v_branch_tab(i).carr_eqp_typ != 'FMET' THEN
 IF v_carrier_arr.EXISTS(v_branch_tab(i).carr_id) THEN
 NULL;
 ELSE
 v_carrier_arr(v_branch_tab(i).carr_id) := ' ';
 --dbms_output.put_line('INSERTING carrier '||i_ss_id||' '||v_branch_tab(i).carr_id);
 --BEGIN
 INSERT INTO structools.nss_temp_subsect_eqp
 VALUES (i_ss_id
 ,v_branch_tab(i).parent_id
 ,v_parent_typ
 ,v_branch_tab(i).carr_id
 ,v_branch_tab(i).carr_eqp_typ
 )
 ; 
 --EXCEPTION -- this is still needed. The assoc. array v_carrier_arr only
 -- WHEN DUPLICATE_RECORD THEN -- checks for duplicate carriers within a sw sections (local array),
 -- NULL; -- and this handles same sw section having mult prot devices
 --END; -- (all due to loops in the network)
 END IF;
 END IF;
 END IF;
 END LOOP;
 v_nod_fr := v_branch_arr.NEXT(v_nod_fr);
 END LOOP;
 END IF; 
 
 --COMMIT; --graz - remove - for testing and debugging only
 
 --o_hvtm_cnt := v_hvtm_cnt;
 
 <<exit_proc>>
 NULL;

 END trace_sw_section; 
 
 
 /* Mainline of create_segment_type_1 */
 /* Segment type 1 defines sub-sections of protection zones, based on difference in average carrier age between switchable sections */
 /* and carrier type between switchable sections. */
 /* This segment type is identified by a switchable section entry switch or a hvtm inside a switchable section. */
 
 BEGIN
 

 dbms_output.put_line('Start '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
 
 o_start_dt := TO_CHAR(v_start_dt,'dd-mm-yyyy hh24:mi:ss');

 /* Check that segment type exists */
 
 BEGIN
 SELECT segment_type_id
 INTO v_dummy_int
 FROM structools.nss_segment_type
 WHERE segment_type_id = v_segment_type_id
 ;
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 dbms_output.put_line('SEGMENT TYPE 2 DOES NOT EXIST; raising EXCEPTION');
				RAISE;
 END;
 
 

 /* collect all networks, protective devices and sw sections in a table, ordered by network id, protection device id, section_id */
 OPEN ntwk_ss_details_csr;
 FETCH ntwk_ss_details_csr BULK COLLECT INTO v_ntwk_ss_details_tab;
 CLOSE ntwk_ss_details_csr;
 
 /* Record all sw sections with a hvtm in an aassciative array */
 FOR c IN ss_with_hvtm_csr LOOP
 v_ss_with_hvtm_arr(c.ss_id) := ' ';
 END LOOP;

 /* Clear temporary tables */
 DELETE FROM structools.nss_temp_subsect_hier; /* contains child sections of sections of split sw sections */ 
 DELETE FROM structools.nss_temp_subsect_eqp; /* contains eqp of sections of split sw sections */
 DELETE FROM structools.nss_temp_subsect_attr; /* contains data for sections of split sw sections only */ 

 DELETE FROM structools.nss_temp_segment_asset_detail; /* segments and their assets, as generated by this run */
 DELETE FROM structools.nss_temp_segment_detail; /* segments, as generated by this run */
 DELETE FROM structools.nss_temp_segment_section; /* segments and their sections, as generated by this run */
 DELETE FROM structools.nss_segment_mat_cde; /* segments and their material_codes */
 --COMMIT; --graz remove - for testing and debugging only
 
 /* close off the current segments */
 UPDATE structools.nss_segment
 set end_dt = v_end_dt
 WHERE segment_type_id = v_segment_type_id
 AND 	 end_dt = v_null_end_dt;
 
 UPDATE structools.nss_segment_fpass
 set end_dt = v_end_dt
 WHERE segment_type_id = v_segment_type_id
 AND 	 end_dt = v_null_end_dt; 
 
 
 v_ntwk_id_prev := 0;
 v_ntwk_cnt := 0;
 v_ss_id_prev := 0;
 v_mtzn_prev := NULL;
 v_pdev_id_prev := 0;
 
 /* Go through the table and create associative arrays for sw sections, networks and protective devices */
 FOR i IN 1 .. v_ntwk_ss_details_tab.COUNT LOOP

 v_ntwk_id := v_ntwk_ss_details_tab(i).ntwk_id;
 IF v_ntwk_id != v_ntwk_id_prev THEN
 v_ntwk_cnt := v_ntwk_cnt + 1;
 -- if v_ntwk_cnt > 100 THEN --graz
 -- dbms_output.put_line('exiting INITIAL LOOP AFTER processing '||v_ntwk_cnt||' networks');
 -- EXIT;
 -- END IF;
 END IF;
 
 v_pdev_id := v_ntwk_ss_details_tab(i).pdev_id;
 
 IF v_ntwk_ss_details_tab(i).ss_id != v_ss_id_prev THEN
 v_section_det_rec.pdev_id := v_ntwk_ss_details_tab(i).pdev_id;
 v_section_det_rec.carr_avg_age := v_ntwk_ss_details_tab(i).avg_ss_carr_age;
 v_section_det_rec.carr_min_age := v_ntwk_ss_details_tab(i).min_ss_carr_age;
 v_section_det_rec.carr_max_age := v_ntwk_ss_details_tab(i).max_ss_carr_age;
 v_section_det_rec.carr_len_m := v_ntwk_ss_details_tab(i).carr_len_m; 
 v_section_det_rec.split_ss := FALSE;
 v_section_det_rec.section_type := 'SS';
 v_section_det_rec.ss_id := v_ntwk_ss_details_tab(i).ss_id; 
 v_section_det_rec.mtzn := v_ntwk_ss_details_tab(i).mtzn;
 v_section_det_rec.oh_ug_ind := v_ntwk_ss_details_tab(i).ss_oh_ug_ind;
 v_section_det_arr(v_ntwk_ss_details_tab(i).ss_id) := v_section_det_rec;
-- dbms_output.put_line (v_section_det_arr(v_ntwk_ss_details_tab(i).ss_id).ss_id
-- ||' '||v_ntwk_ss_details_tab(i).ss_id
-- ||' '||v_section_det_arr(v_ntwk_ss_details_tab(i).ss_id).section_type
-- ||' '||v_section_det_arr(v_ntwk_ss_details_tab(i).ss_id).pdev_id
-- ||' '||v_section_det_arr(v_ntwk_ss_details_tab(i).ss_id).carr_min_age
-- ||' '||v_section_det_arr(v_ntwk_ss_details_tab(i).ss_id).carr_max_age
-- ||' '||v_section_det_arr(v_ntwk_ss_details_tab(i).ss_id).carr_avg_age);
-- dbms_output.put_line (' ');

 IF v_ss_id_prev != 0 THEN
 v_sect_mat_cde_arr(v_ss_id_prev) := v_mat_cde_arr;
 /* If multiple mat codes found in the sw. section */
 IF v_mat_cde_arr.COUNT > 1 AND v_ss_with_hvtm_arr.EXISTS(v_ss_id_prev) THEN
 --dbms_output.put_line ('v_mat_cde_arr.COUNT = '||v_mat_cde_arr.COUNT||' FOR ss '||v_ss_id_prev); --graz
 --v_mat_cde := v_mat_cde_arr.FIRST;
 --WHILE v_mat_cde IS NOT NULL LOOP
 -- dbms_output.put_line (' '||v_mat_cde);
 -- v_mat_cde := v_mat_cde_arr.NEXT(v_mat_cde);
 --END LOOP;
 /* If multiple materials within a single sw section, then it is probably a change between overhead and underground */
 /* within the sw section. Trace the insides of the sw section and create new sections starting with the HVTM(s), */
 /* and overwrite the section starting with the sw section id, as it is now not a complete sw. section 
 */
 --dbms_output.put_line('about TO TRACE ss '||v_ntwk_id_prev||' '||v_ss_id_prev); --graz
 
 trace_sw_section(v_ntwk_id_prev, v_ss_id_prev, v_hvtm_cnt); 
 --IF v_hvtm_cnt > 0 THEN
 v_section_id_prev := 0;
 FOR c IN section_age_mat_csr(v_ss_id_prev) LOOP
 IF c.section_id != v_section_id_prev THEN
 v_section_det_rec.carr_avg_age := c.section_avg_age;
 v_section_det_rec.carr_min_age := c.section_min_age;
 v_section_det_rec.carr_max_age := c.section_max_age;
 v_section_det_rec.carr_len_m := c.section_carr_len_m;
 v_section_det_rec.split_ss := TRUE;
 v_section_det_rec.section_type := c.section_type;
 v_section_det_rec.ss_id := v_ss_id_prev; 
 v_section_det_rec.pdev_id := v_pdev_id_prev;
 v_section_det_rec.oh_ug_ind := c.oh_ug_ind;
 IF c.section_id = v_ss_id_prev THEN
 v_section_det_rec.mtzn := v_mtzn_prev;
 ELSE 
 BEGIN
 SELECT maint_zone_nam
 INTO v_section_det_rec.mtzn
-- FROM rivaods.equipment_dim
 FROM structoolsw.asset_geography
 WHERE pick_id = 'N'||c.section_id;
-- AND actv_flg = 'Y'
-- AND equip_stat = 'AC';
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_section_det_rec.mtzn := NULL;
 END;
 END IF;
 v_section_det_arr(c.section_id) := v_section_det_rec;
 IF v_section_id_prev != 0 THEN
 v_sect_mat_cde_arr(v_section_id_prev) := v_mat_cde_arr;
 END IF;
 v_mat_cde_arr.DELETE;
 END IF;
 IF c.material_cde IS NOT NULL THEN
 v_mat_cde_arr(c.material_cde) := ' '; -- we are only interested in existence
 END IF; -- v_mat_cde_arr is an array different of material codes
 v_section_id_prev := c.section_id;
 END LOOP; 
 v_sect_mat_cde_arr(v_section_id_prev) := v_mat_cde_arr;
 --END IF;
 END IF;
 END IF;
 v_mat_cde_arr.DELETE;
 END IF;

 IF v_ntwk_ss_details_tab(i).ss_carr_mat_cde IS NOT NULL THEN
 v_mat_cde_arr(v_ntwk_ss_details_tab(i).ss_carr_mat_cde) := ' '; -- we are only interested in existence
-- dbms_output.put_line ('countof v_mat_cde_arr = '||v_mat_cde_arr.COUNT);
-- dbms_output.put_line ('mat_cde = '||v_ntwk_ss_details_tab(i).ss_carr_mat_cde); 
 END IF; -- v_mat_cde_arr is an array different of material codes
 -- in the section
 v_ss_id_prev := v_ntwk_ss_details_tab(i).ss_id;
 v_mtzn_prev := v_ntwk_ss_details_tab(i).mtzn;
 v_pdev_id_prev := v_ntwk_ss_details_tab(i).pdev_id;
 v_ntwk_id_prev := v_ntwk_id;
 
 IF v_ntwk_ss_details_tab(i).ntwk_id = v_ntwk_ss_details_tab(i).pdev_id THEN
 v_ntwk_det_arr(v_ntwk_ss_details_tab(i).ntwk_id) := v_ntwk_ss_details_tab(i).pdev_mtr_cnt;
 END IF; 
 IF v_ntwk_ss_details_tab(i).pdev_id = v_ntwk_ss_details_tab(i).ss_id THEN
 v_pdev_det_arr(v_ntwk_ss_details_tab(i).pdev_id) := v_ntwk_ss_details_tab(i).pdev_mtr_cnt;
 END IF; 
 END LOOP;

 /* Process the last sw section (the loop handles up to ss - 1) */ 
 v_sect_mat_cde_arr(v_ss_id_prev) := v_mat_cde_arr;
 IF v_mat_cde_arr.COUNT > 1 AND v_ss_with_hvtm_arr.EXISTS(v_ss_id_prev) THEN
-- dbms_output.put_line ('going TO TRACE ss');
 trace_sw_section(v_ntwk_id_prev, v_ss_id_prev, v_hvtm_cnt); 
 --IF v_hvtm_cnt > 0 THEN
 v_section_id_prev := 0;
 FOR c IN section_age_mat_csr(v_ss_id_prev) LOOP
 IF c.section_id != v_section_id_prev THEN
 v_section_det_rec.carr_avg_age := c.section_avg_age;
 v_section_det_rec.carr_min_age := c.section_min_age;
 v_section_det_rec.carr_max_age := c.section_max_age;
 v_section_det_rec.carr_len_m := c.section_carr_len_m;
 v_section_det_rec.split_ss := TRUE;
 v_section_det_rec.section_type := c.section_type;
 v_section_det_rec.ss_id := v_ss_id_prev; 
 v_section_det_rec.pdev_id := v_pdev_id_prev;
 v_section_det_rec.oh_ug_ind := c.oh_ug_ind;
 IF c.section_id = v_ss_id_prev THEN
 v_section_det_rec.mtzn := v_mtzn_prev;
 ELSE 
 BEGIN
 SELECT maint_zone_nam
 INTO v_section_det_rec.mtzn
-- FROM rivaods.equipment_dim
 FROM structoolsw.asset_geography
 WHERE pick_id = 'N'||c.section_id;
-- AND actv_flg = 'Y'
-- AND equip_stat = 'AC';
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 v_section_det_rec.mtzn := NULL;
 END;
 END IF;
 v_section_det_arr(c.section_id) := v_section_det_rec;
 IF v_section_id_prev != 0 THEN
 v_sect_mat_cde_arr(v_section_id_prev) := v_mat_cde_arr;
-- dbms_output.put_line('material code cnt FOR '||v_section_id_prev||' '||v_mat_cde_arr.count);
 END IF;
 v_mat_cde_arr.DELETE;
-- dbms_output.put_line('deleting v_mat_cde_arr');
 END IF;
 IF c.material_cde IS NOT NULL THEN
 v_mat_cde_arr(c.material_cde) := ' '; -- we are only interested in existence
-- dbms_output.put_line('addning mat cde '|| c.material_cde);
 END IF; -- v_mat_cde_arr is an array different of material codes
 v_section_id_prev := c.section_id;
 END LOOP; 
 v_sect_mat_cde_arr(v_section_id_prev) := v_mat_cde_arr;
-- dbms_output.put_line('material code cnt FOR(2) '||v_section_id_prev||' '||v_mat_cde_arr.count);
 --END IF;
 END IF;
 
 /* At this point, v_section_det_array contains details of all sections, section being either switchable section or a subsection */
 /* of a switchable section, if the whole switchable section has attributes that may prevent it from allocation it to a single segment.*/
 
 /* Display array of section attributes */ --graz
-- v_section_id := v_section_det_arr.FIRST;
-- dbms_output.put_line ('SECTION ATTRIBUTES');
-- WHILE v_section_id IS NOT NULL LOOP
-- dbms_output.put_line (v_section_det_arr(v_section_id).ss_id
-- ||' '||v_section_id
-- ||' '||v_section_det_arr(v_section_id).section_type
-- ||' '||v_section_det_arr(v_section_id).pdev_id
-- ||' '||v_section_det_arr(v_section_id).carr_min_age
-- ||' '||v_section_det_arr(v_section_id).carr_max_age
-- ||' '||v_section_det_arr(v_section_id).carr_avg_age
-- ||' '||v_section_det_arr(v_section_id).oh_ug_ind);
-- v_mat_cde_arr := v_sect_mat_cde_arr(v_section_id);
-- v_mat_cde := v_mat_cde_arr.FIRST;
-- WHILE v_mat_cde IS NOT NULL LOOP
-- dbms_output.put_line (' '||v_mat_cde);
-- v_mat_cde := v_mat_cde_arr.NEXT(v_mat_cde);
-- END LOOP;
-- v_section_id := v_section_det_arr.NEXT(v_section_id);
-- END LOOP;
-- dbms_output.put_line ('END OF SECTION ATTRIBUTES');

 /*-----------------*/
 /* create segments */
 /*-----------------*/
 
 v_ntwk_id_prev := 0;
 v_pdev_id_prev := 0;
 v_ntwk_cnt := 0;
 

 FOR idx IN 1 .. v_ntwk_ss_details_tab.COUNT LOOP

 -- test_cnt := test_cnt + 1;
 -- if test_cnt > 1000 THEN
 -- dbms_output.put_line ('looping(0)');
 -- RAISE v_prog_excp;
 -- --EXIT;
 -- END IF;

 v_ntwk_id := v_ntwk_ss_details_tab(idx).ntwk_id;
 v_zone_sub := v_ntwk_ss_details_tab(idx).zone_sub;
 v_pdev_id := v_ntwk_ss_details_tab(idx).pdev_id;
 IF v_ntwk_id != v_ntwk_id_prev THEN
 v_ntwk_cnt := v_ntwk_cnt + 1;
-- IF v_ntwk_cnt > 100 THEN
-- dbms_output.put_line ('Exiting AFTER processing 100 networks');
-- dbms_output.put_line ('NEXT ntwk would be '||v_ntwk_id);
-- EXIT;
-- ELSE
-- dbms_output.put_line ('processing ntwk '||v_ntwk_id||' '||to_char(sysdate,'hh24:mi:ss'));
-- END IF; --graz
 END IF;
 IF v_ntwk_id = v_ntwk_id_prev AND v_pdev_id = v_pdev_id_prev 
 OR (NOT v_pdev_det_arr.EXISTS(v_pdev_id)) THEN -- should never happen but it does due to some bad data in GISSDD tables
 CONTINUE;
 END IF;
 IF v_ntwk_id != v_ntwk_id_prev THEN
 --dbms_output.put_line('Line 1411 v_ntwk_id = '||v_ntwk_id); --graz
 v_ntwk_cust_cnt := v_ntwk_det_arr(v_ntwk_id);
 --dbms_output.put_line(v_ntwk_id||' cust cnt = '||v_ntwk_cust_cnt);
 END IF;
 IF v_pdev_id != v_pdev_id_prev THEN
 v_pdev_cust_cnt := v_pdev_det_arr(v_pdev_id);
 IF v_ntwk_cust_cnt = 0 THEN
 v_backbone_spur := 'SPUR';
 ELSIF (v_pdev_cust_cnt / v_ntwk_cust_cnt) * 100 < v_backbone_spur_val THEN
 v_backbone_spur := 'SPUR';
 ELSE
 v_backbone_spur := 'BACKBONE';
 END IF;
 --dbms_output.put_line(v_pdev_id||' cust cnt = '||v_pdev_cust_cnt||' '||v_backbone_spur);
 END IF;
 v_ntwk_id_prev := v_ntwk_id;
 v_pdev_id_prev := v_pdev_id;
 M := 1; /*index for segments_to_create for a prot device */
 v_seg_to_create_tab.DELETE; 
 v_seg_to_create_tab.EXTEND(1);
 v_seg_to_create_tab(M) := v_pdev_id;
 
 --dbms_output.put_line ('NEW pdev '||v_pdev_id); --graz 
 
 test_cnt1 := 0;
 M := v_seg_to_create_tab.LAST;
 WHILE M IS NOT NULL LOOP /* One loop iteration creates single segment */
 test_cnt1 := test_cnt1 + 1;
 IF test_cnt1 > 100 THEN
-- dbms_output.put_line ('looping(1)');
-- RAISE v_prog_excp;
 EXIT;
 END IF;
 
 v_seg_id := v_seg_to_create_tab(M); 
 /*get region for the segment entry point */
 v_pick_id := 'N'||v_seg_id;
 BEGIN
 SELECT 
 CASE
 WHEN UPPER(std_rgn_nam) LIKE 'GOLDFIELDS%' 		THEN 'GOLDFIELDS' 
 WHEN UPPER(std_rgn_nam) LIKE 'SOUTH COUNTRY%' 	THEN 'SOUTH COUNTRY'
 WHEN UPPER(std_rgn_nam) LIKE 'NORTH COUNTRY%' 	THEN 'NORTH COUNTRY'
 WHEN UPPER(std_rgn_nam) LIKE 'UNKNOWN%' 			THEN NULL
 ELSE UPPER(std_rgn_nam) 
 END								AS rgn_nam 
 INTO	 v_rgn_nam
 FROM structoolsw.asset_geography 
 WHERE pick_id = v_pick_id;
 EXCEPTION
 	WHEN NO_DATA_FOUND THEN
-- 	dbms_output.put_line('Asset_geography NOT FOUND FOR '||'N'||v_seg_id);
 v_rgn_nam := NULL;
 END;
 
 v_seg_type := v_section_det_arr(v_seg_id).section_type;
 --dbms_output.put_line ('Creating SEGMENT '||v_seg_id||' TYPE '||v_seg_type); --graz
 
 v_seg_min_avg_age := 0; 
 v_seg_max_avg_age := 0;
 
 v_mat_cde_arr2.DELETE; --array of material codes per segment 
 
 /* record this section as the first section in this segment */
 v_section_id := v_seg_id;
 IF v_sect_mat_cde_arr.EXISTS(v_section_id) THEN
 v_mat_cde_arr := v_sect_mat_cde_arr(v_section_id);
 v_sect_mat_cde := v_mat_cde_arr.FIRST;
 WHILE v_sect_mat_cde IS NOT NULL LOOP -- populate array of distinct material codes for segment 
 v_mat_cde_arr2(v_sect_mat_cde) := ' ';
 v_sect_mat_cde := v_mat_cde_arr.NEXT(v_sect_mat_cde);
 END LOOP;
 v_sect_mat_cde_cnt := v_mat_cde_arr.COUNT;
 v_sect_mat_cde := v_mat_cde_arr.FIRST; 
 ELSE
 v_sect_mat_cde_cnt := 0;
 v_sect_mat_cde := NULL;
 END IF;

 v_mtzn := v_section_det_arr(v_section_id).mtzn;
 
 INSERT INTO structools.nss_temp_segment_section
 VALUES ('N'||v_seg_id -- segment_identifier_id
 ,v_section_id
 ,v_section_det_arr(v_seg_id).section_type --section ontry point type (SS or HVTM)
 ,v_section_det_arr(v_section_id).carr_min_age
 ,v_section_det_arr(v_section_id).carr_max_age
 ,v_section_det_arr(v_section_id).carr_avg_age
 ,v_section_det_arr(v_section_id).carr_len_m
 ,v_sect_mat_cde_cnt -- count of different material types (typically 1, but not always)
 ,v_sect_mat_cde -- section material type (random if mult mat types in a section)
 ,v_section_det_arr(v_section_id).mtzn
 );
 
-- IF v_pdev_id = 5172349 THEN --graz
-- dbms_output.put_line('Seg = '||v_seg_id||' Inserted INTO temp_segment_section(1) '||v_seg_id||' '||v_section_id); 
-- END IF; 
 IF v_section_det_arr(v_section_id).carr_avg_age IS NOT NULL THEN
 v_seg_min_avg_age := v_section_det_arr(v_section_id).carr_avg_age;
 v_seg_max_avg_age := v_section_det_arr(v_section_id).carr_avg_age;
 ELSE
 v_seg_min_avg_age := 0;
 v_seg_max_avg_age := 0;
 END IF;
 v_seg_carr_len_m := v_section_det_arr(v_section_id).carr_len_m;
 v_seg_oh_ug_ind := v_section_det_arr(v_section_id).oh_ug_ind;
 
 /* Get sw sections downstream of this section */
 v_section_parent_id := v_section_id;

 v_potential_section_tab.DELETE;

 /* If this section is a complete sw section, get its children from net_sw_sect_trace; otherwise search the temp table */
 /* that we set up earlier, as a result of inside sw section trace */
 IF v_section_det_arr(v_section_id).split_ss THEN
-- IF v_pdev_id = 5172349 THEN 
-- dbms_output.put_line('Seg = '||v_seg_id||'. Getting children OF SPLIT ss'||v_section_parent_id); 
-- END IF; 
 OPEN child_section_csr_temp(v_section_parent_id);
 FETCH child_section_csr_temp BULK COLLECT INTO v_potential_section_tab; 
 CLOSE child_section_csr_temp;
 ELSE
-- IF v_pdev_id = 5172349 THEN 
-- dbms_output.put_line('Seg = '||v_seg_id||'. Getting children OF ss'||v_section_parent_id); 
-- END IF; 
 OPEN child_section_csr(v_section_parent_id);
 FETCH child_section_csr BULK COLLECT INTO v_potential_section_tab; 
 CLOSE child_section_csr;
 END IF;
 
 --dbms_output.put_line('Potential SECTION TABLE COUNT '||v_potential_section_tab.COUNT); --graz
 
 test_cnt2 := 0;
 j := v_potential_section_tab.LAST;
 WHILE j IS NOT NULL LOOP
-- IF v_pdev_id = 5172349 THEN 
-- dbms_output.put_line('Seg = '||v_seg_id||' Got potential SECTION '||v_potential_section_tab(j).section_id); 
-- END IF; 
 test_cnt2 := test_cnt2 + 1;
 IF test_cnt2 > 1000 THEN
 dbms_output.put_line ('looping2');
 RAISE v_prog_excp;
 --EXIT;
 END IF;
 /* check if the sw section is in the same prot zone and has similar characteristics to the 1st sw section in the segment */
 IF NOT v_section_det_arr.EXISTS(v_potential_section_tab(j).section_id) THEN
-- dbms_output.put_line ('ss '||v_potential_section_tab(j).section_id||' does NOT exist IN ARRAY. Ntwk = '||v_ntwk_id); --graz
 --RAISE v_prog_excp; --should never happen but it does due to lack of data integrity between asset_topology and other tables
 GOTO finish_loop; 
 END IF;
-- IF v_potential_section_tab(j).section_id = 4675038 THEN
-- dbms_output.put_line ('ss '||v_potential_section_tab(j).section_id||' pdev_id = '
-- ||v_section_det_arr(v_potential_section_tab(j).section_id).pdev_id||' v_pdev_id = '||v_pdev_id);
-- END IF;
 IF v_section_det_arr(v_potential_section_tab(j).section_id).pdev_id = v_pdev_id THEN
 IF v_section_det_arr(v_potential_section_tab(j).section_id).carr_avg_age IS NOT NULL THEN
 IF v_section_det_arr(v_potential_section_tab(j).section_id).carr_avg_age < v_seg_min_avg_age OR v_seg_min_avg_age = 0 THEN
 v_seg_min_age_tmp := v_section_det_arr(v_potential_section_tab(j).section_id).carr_avg_age;
 ELSE
 v_seg_min_age_tmp := v_seg_min_avg_age;
 END IF;
 IF v_section_det_arr(v_potential_section_tab(j).section_id).carr_avg_age > v_seg_max_avg_age OR v_seg_max_avg_age = 0 THEN
 v_seg_max_age_tmp := v_section_det_arr(v_potential_section_tab(j).section_id).carr_avg_age;
 ELSE
 v_seg_max_age_tmp := v_seg_max_avg_age;
 END IF;
 ELSE
 v_seg_min_age_tmp := 0;
 v_seg_max_age_tmp := 0;
 END IF;
 
 v_seg_len_ok := TRUE;
 IF v_rgn_nam = 'METRO' THEN
 	v_seg_len_upp_limit := v_seg_len_upp_limit_metro;
 ELSIF v_rgn_nam = 'GOLDFIELDS' THEN
 	v_seg_len_upp_limit := v_seg_len_upp_limit_goldfields;
 ELSIF v_rgn_nam = 'NORTH COUNTRY' THEN
 	v_seg_len_upp_limit := v_seg_len_upp_limit_n_country;
 ELSIF v_rgn_nam = 'SOUTH COUNTRY' THEN
 	v_seg_len_upp_limit := v_seg_len_upp_limit_s_country;
 ELSE
 	v_seg_len_upp_limit := v_seg_len_upp_limit_default;
 END IF;
 
 IF v_seg_carr_len_m + v_section_det_arr(v_potential_section_tab(j).section_id).carr_len_m > v_seg_len_upp_limit THEN
 	v_seg_len_ok := FALSE;
-- dbms_output.put_line('Shortening seg '||v_seg_id||' '||v_rgn_nam||' due exceeding len LIMIT');	--graz
-- dbms_output.put_line('LENGTH so far = '||v_seg_carr_len_m||' SECTION len = '||v_section_det_arr(v_potential_section_tab(j).section_id).carr_len_m);
						END IF;
 
 /* check if any of this section's mat code matches THE existing SEGMENT codes */
 IF v_seg_oh_ug_ind IS NULL THEN
 v_seg_oh_ug_ind := v_section_det_arr(v_potential_section_tab(j).section_id).oh_ug_ind;
 END IF;

 v_mat_cde_match := FALSE;
 
 IF v_seg_len_ok THEN
 IF v_section_det_arr(v_potential_section_tab(j).section_id).oh_ug_ind != v_seg_oh_ug_ind
 AND v_section_det_arr(v_potential_section_tab(j).section_id).oh_ug_ind IS NOT NULL THEN
 NULL;
 ELSE
 v_seg_mat_cde := v_mat_cde_arr2.FIRST;
 IF v_seg_mat_cde IS NULL THEN
 v_mat_cde_match := TRUE;
 ELSE
 WHILE v_seg_mat_cde IS NOT NULL LOOP
 v_mat_cde_arr := v_sect_mat_cde_arr(v_potential_section_tab(j).section_id);
 v_sect_mat_cde := v_mat_cde_arr.FIRST;
 IF v_sect_mat_cde IS NULL THEN
 v_mat_cde_match := TRUE;
 EXIT;
 END IF;
 WHILE v_sect_mat_cde IS NOT NULL LOOP
 IF v_seg_mat_cde = v_sect_mat_cde THEN
 v_mat_cde_match := TRUE;
 EXIT;
 END IF;
 v_sect_mat_cde := v_mat_cde_arr.NEXT(v_sect_mat_cde);
 END LOOP;
 IF v_mat_cde_match THEN
 EXIT;
 END IF;
 v_seg_mat_cde := v_mat_cde_arr2.NEXT(v_seg_mat_cde);
 END LOOP;
 END IF;
 END IF;
 					END IF;
 IF (v_seg_max_age_tmp - v_seg_min_age_tmp) <= v_carr_age_tolerance 
 AND v_mat_cde_match AND v_seg_len_ok THEN
-- IF v_pdev_id = 5172349 THEN 
-- dbms_output.put_line ('Seg = '||v_seg_id||' ss '||v_potential_section_tab(j).section_id||' belongs TO SEGMENT '||v_seg_id);
-- END IF;
 IF v_section_det_arr(v_potential_section_tab(j).section_id).carr_avg_age IS NOT NULL THEN
 IF v_section_det_arr(v_potential_section_tab(j).section_id).carr_avg_age < v_seg_min_avg_age OR v_seg_min_avg_age = 0 THEN
 v_seg_min_avg_age := v_section_det_arr(v_potential_section_tab(j).section_id).carr_avg_age;
 END IF;
 IF v_section_det_arr(v_potential_section_tab(j).section_id).carr_avg_age > v_seg_max_avg_age OR v_seg_max_avg_age = 0 THEN
 v_seg_max_avg_age := v_section_det_arr(v_potential_section_tab(j).section_id).carr_avg_age;
 END IF;
 END IF;
 v_seg_carr_len_m := v_seg_carr_len_m + v_section_det_arr(v_potential_section_tab(j).section_id).carr_len_m;
 
 -- dbms_output.put_line ('ss car age MIN = '||v_seg_min_avg_age||' ss car age MAX = '||v_seg_max_avg_age);
 /* Establish the new section's material code AND material code COUNT */
 /* If count > 1, material code is randomly selected */ 
 v_section_id := v_potential_section_tab(j).section_id;
 IF v_sect_mat_cde_arr.EXISTS(v_section_id) THEN
 v_mat_cde_arr := v_sect_mat_cde_arr(v_section_id);
 v_sect_mat_cde := v_mat_cde_arr.FIRST;
 WHILE v_sect_mat_cde IS NOT NULL LOOP -- update array of distinct material codes for segment 
 v_mat_cde_arr2(v_sect_mat_cde) := ' ';
 v_sect_mat_cde := v_mat_cde_arr.NEXT(v_sect_mat_cde);
 END LOOP;
 v_sect_mat_cde_cnt := v_mat_cde_arr.COUNT;
 v_sect_mat_cde := v_mat_cde_arr.FIRST;
 ELSE
 v_sect_mat_cde_cnt := 0;
 v_sect_mat_cde := NULL;
 END IF;
 INSERT INTO structools.nss_temp_segment_section
 VALUES ('N'||v_seg_id
 ,v_potential_section_tab(j).section_id
 ,v_potential_section_tab(j).section_type
 ,v_section_det_arr(v_potential_section_tab(j).section_id).carr_min_age
 ,v_section_det_arr(v_potential_section_tab(j).section_id).carr_max_age
 ,v_section_det_arr(v_potential_section_tab(j).section_id).carr_avg_age
 ,v_section_det_arr(v_potential_section_tab(j).section_id).carr_len_m
 ,v_sect_mat_cde_cnt
 ,v_sect_mat_cde
 ,v_section_det_arr(v_potential_section_tab(j).section_id).mtzn
 )
 ;
-- IF v_pdev_id = 5172349 THEN 
-- dbms_output.put_line('Seg = '||v_seg_id||' Inserted into temp_segment_section(2) '||v_seg_id ||' '||v_potential_section_tab(j).section_id); 
-- END IF; 
 v_section_parent_id := v_potential_section_tab(j).section_id;
 l := v_potential_section_tab.LAST;
 IF v_section_det_arr(v_section_parent_id).split_ss THEN
 FOR c IN child_section_csr_temp(v_section_parent_id) LOOP
 l := l + 1;
 IF l > 100 THEN
 dbms_output.put_line ('looping 3. HVTM v_section_parent_id = '||v_section_parent_id );
 RAISE v_prog_excp;
 --EXIT;
 END IF;
 v_potential_section_tab.EXTEND(1);
 v_potential_section_tab(l).section_id := c.section_id;
 v_potential_section_tab(l).section_type := c.section_type;
-- IF v_seg_id = 1746599 THEN 
-- dbms_output.put_line ('Added ss '||c.ss_id||' to potentials at position '||l);
-- end if;
 END LOOP;
 ELSE
 FOR c IN child_section_csr(v_section_parent_id) LOOP
 l := l + 1;
 IF l > 100 THEN
 dbms_output.put_line ('looping 4. SS v_section_parent_id = '||v_section_parent_id );
 RAISE v_prog_excp;
 --EXIT;
 END IF;
 v_potential_section_tab.EXTEND(1);
 v_potential_section_tab(l).section_id := c.section_id;
 v_potential_section_tab(l).section_type := c.section_type;
 END LOOP;
 END IF;
 ELSE
 --l := m + 1; 
 l := v_seg_to_create_tab.LAST; 
 l := l + 1; 
 v_seg_to_create_tab.EXTEND(1);
 v_seg_to_create_tab(l) := v_potential_section_tab(j).section_id;
-- IF v_pdev_id = 5172349 THEN 
-- dbms_output.put_line('Seg = '||v_seg_id||' Added(2) '||v_potential_section_tab(j).section_id||' as a segment to create');
-- END IF; 
 END IF;
 END IF;
 <<finish_loop>>
 v_potential_section_tab.DELETE(j);
 --v_potential_section_tab.TRIM;

 j := v_potential_section_tab.LAST;
 WHILE j IS NOT NULL AND v_potential_section_tab(j).section_id IS NULL LOOP
 v_potential_section_tab.TRIM;
 j := v_potential_section_tab.LAST;
 END LOOP;
 
 END LOOP;
 
 v_seg_mat_cde := v_mat_cde_arr2.FIRST;
 IF v_seg_mat_cde IS NOT NULL THEN
 v_seg_mat_cde_cnt := v_mat_cde_arr2.COUNT;
 ELSE
 v_seg_mat_cde_cnt := 0;
 END IF;
 
 /* save material codes for a segment */
 v_seg_mat_cde_arr('N'||v_seg_id) := v_mat_cde_arr2;
 
 /* Insert segment data into the temp segment table */ 
 INSERT INTO structools.nss_temp_segment_detail
 VALUES (v_ntwk_id
 ,v_pdev_id
 ,'N'||v_seg_id
 ,v_seg_type
 ,v_seg_carr_len_m
 ,v_backbone_spur
 ,v_seg_min_avg_age
 ,v_seg_max_avg_age
 ,v_seg_mat_cde_cnt
 ,v_seg_mat_cde
 ,v_zone_sub
 ,v_mtzn
 ,v_seg_oh_ug_ind
 )
 ;
-- IF v_pdev_id = 5172349 THEN 
-- dbms_output.put_line('Seg = '||v_seg_id||' Inserted into temp_segment '||v_seg_id); 
-- END IF; 
 v_seg_to_create_tab.DELETE(M);
 M := v_seg_to_create_tab.LAST;
 WHILE M IS NOT NULL AND v_seg_to_create_tab(M) IS NULL LOOP
 v_seg_to_create_tab.TRIM;
 M := v_seg_to_create_tab.LAST;
 END LOOP;
-- IF v_pdev_id = 5172349 THEN 
-- IF m IS NOT NULL THEN 
-- dbms_output.put_line('Seg = '||v_seg_id||' Got next segment to create '||v_seg_to_create_tab(m));
-- ELSE 
-- dbms_output.put_line('Seg = '||v_seg_id||' no more segment to create');
-- END IF; 
-- END IF;
 END LOOP; 
 END LOOP; 
 
 --dbms_output.put_line('Segments and their mat codes');
 /* Store segment and their distinct material codes in a temp table */
 v_seg_id_id := v_seg_mat_cde_arr.FIRST;
 WHILE v_seg_id_id IS NOT NULL LOOP
 --dbms_output.put_line(v_seg_id_id);
 v_mat_cde_arr2 := v_seg_mat_cde_arr(v_seg_id_id);
 IF v_mat_cde_arr2.COUNT > 0 THEN
 v_seg_mat_cde := v_mat_cde_arr2.FIRST;
 WHILE v_Seg_mat_cde IS NOT NULL LOOP
 INSERT INTO structools.nss_segment_mat_cde
 VALUES (v_seg_id_id
 ,v_seg_mat_cde)
 ; 
 --dbms_output.put_line(' '||v_Seg_mat_cde);
 v_seg_mat_cde := v_mat_cde_arr2.NEXT(v_Seg_mat_cde);
 END LOOP;
 ELSE
 INSERT INTO structools.nss_segment_mat_cde
 VALUES (v_seg_id_id
 ,NULL)
 ; 
 --dbms_output.put_line(' no mat ocdes');
 END IF;
 v_seg_id_id:= v_seg_mat_cde_arr.NEXT(v_seg_id_id);
 END LOOP;


 /* Populate table of segments and their equipment */
 
 INSERT INTO structools.nss_temp_segment_asset_detail
 WITH whole_ss AS
 (
 SELECT CASE
 WHEN nsw_nid = 0 THEN A.net_nid
 ELSE A.nsw_nid
 END AS ss_id
 FROM structoolsw.net_sw_sect A INNER JOIN
 structoolsw.ntwk b
 ON A.net_nid = b.net_nid
 WHERE b.net_typ = 'HV'
 --AND A.net_nid = 1032412 --graz
 --order by ss_id
 MINUS
 SELECT DISTINCT ss_id
 FROM structools.nss_temp_subsect_eqp
 WHERE section_type = 'SS'
 ORDER BY ss_id
 )
 ,
 all_carriers_tmp AS
 (
 SELECT CASE
 WHEN nsw_nid = 0 THEN net_nid
 ELSE nsw_nid
 END AS ss_id
 ,TO_CHAR('C'||nec_nid) AS pick_id
 ,nec_equip_typ AS eqp_typ
 FROM structoolsw.net_sw_sect_car
 WHERE nec_equip_typ IN ('HVBB','HVCO','HVSP','HVCU','SPUG')
 )
 ,
 all_carriers AS
 (
 SELECT b.ss_id AS section_id
 ,'SS' AS section_type
 ,A.pick_id
 ,A.eqp_typ
 FROM all_carriers_tmp A INNER JOIN
 whole_ss b
 ON A.ss_id = b.ss_id
 UNION 
 SELECT section_id 
 ,section_type
 ,'C'||pick_id AS pick_id
 ,eqp_type AS eqp_typ
 FROM structools.nss_temp_subsect_eqp
 WHERE eqp_type IN ('HVBB','HVCO','HVSP','HVCU','SPUG')
 )
 ,
 bays_and_poles AS
 (
 SELECT A.section_id 
 ,A.section_type
 ,TO_CHAR(b.pick_id) AS pick_id 
 ,A.eqp_typ 
 ,(CASE 
 WHEN structure_1_pick_id < structure_2_pick_id THEN structure_1_pick_id
 ELSE structure_2_pick_id
 END) AS stc1
 ,(CASE 
 WHEN structure_1_pick_id < structure_2_pick_id THEN structure_2_pick_id
 ELSE structure_1_pick_id
 END) AS stc2 
 FROM all_carriers A INNER JOIN
 structoolsw.bay_detail b
 ON A.pick_id = b.conductor_pick_id 
 WHERE b.structure_1_pick_id IS NOT NULL AND b.structure_2_pick_id IS NOT NULL 
 AND b.pick_id IS NOT NULL --cater for gissdd data integrity issues 
 )
 ,
 poles AS
 (
 SELECT DISTINCT section_id
 ,section_type
 ,TO_CHAR(pick_id) AS pick_id
 ,'POLE' AS equip_grp_id 
 FROM
 (
 SELECT DISTINCT section_id
 ,section_type
 ,stc1 AS pick_id
 FROM bays_and_poles
 UNION
 SELECT DISTINCT section_id
 ,section_type
 ,stc2 AS pick_id
 FROM bays_and_poles
 )
 )
 ,
 all_stcs AS
 (
 SELECT CASE
 WHEN nsw_nid = 0 THEN A.net_nid
 ELSE A.nsw_nid
 END AS section_id
 ,'SS' AS section_type
 ,TO_CHAR('S'||nes_nid) AS pick_id
 ,CASE nes_equip_typ
 WHEN 'TPOL' THEN 'POLE'
 ELSE nes_equip_typ
 END AS equip_grp_id
 FROM structoolsw.net_sw_sect_stc A INNER JOIN
 structoolsw.ntwk b
 ON A.net_nid = b.net_nid
 WHERE b.net_typ = 'HV'
 )
 ,
 other_stcs AS
 (
 SELECT section_id
 ,section_type
 ,pick_id
 ,equip_grp_id
 FROM all_stcs
 MINUS
 SELECT section_id
 ,section_type
 ,pick_id
 ,equip_grp_id
 FROM poles
 )
 ,
 all_structures AS
 (
 SELECT aa.section_id
 ,aa.section_type
 ,aa.pick_id
 ,aa.equip_grp_id AS eqp_typ
 --,b.equip_grp_id as eqp_typ
 FROM
 (
 SELECT section_id
 ,section_type
 ,pick_id
 ,equip_grp_id
 FROM poles
 UNION 
 SELECT section_id
 ,section_type
 ,pick_id
 ,equip_grp_id
 FROM other_stcs
 ) aa)
 ,
 all_nodes_tmp AS
 (
 SELECT CASE
 WHEN nsw_nid = 0 THEN net_nid
 ELSE nsw_nid
 END AS ss_id
 ,TO_CHAR('N'||nen_nid) AS pick_id
 ,nen_equip_typ AS eqp_typ
 FROM structoolsw.net_sw_sect_nod
 WHERE nen_equip_typ != 'FNOD'
 )
 ,
 all_nodes AS
 (
 SELECT b.ss_id AS section_id
 ,'SS' AS section_type
 ,A.pick_id
 ,A.eqp_typ
 FROM all_nodes_tmp A INNER JOIN
 whole_ss b
 ON A.ss_id = b.ss_id 
 UNION 
 SELECT section_id 
 ,section_type
 ,TO_CHAR('N'||pick_id) AS pick_id
 ,eqp_type AS eqp_typ
 FROM structools.nss_temp_subsect_eqp
 WHERE eqp_type NOT IN ('HVBB','HVCO','HVSP','HVCU','SPUG')
 ORDER BY section_id
 )
 ,
 sections AS
 (
 SELECT *
 FROM all_carriers
 UNION
 SELECT section_id
 ,section_type
 ,pick_id
 ,eqp_typ
 FROM bays_and_poles 
 UNION
 SELECT *
 FROM all_structures
 UNION
 SELECT *
 FROM all_nodes
 )
 SELECT DISTINCT c.ntwk_id
 ,c.prot_zone_id
 ,c.segment_identifier_id
 ,c.seg_type
 ,A.pick_id
 ,TRIM(A.eqp_typ) AS eqp_type
 FROM sections A INNER JOIN
 structools.nss_temp_segment_section b 
 ON A.section_id = b.section_id
 INNER JOIN 
 structools.nss_temp_segment_detail c
 ON b.segment_identifier_id = c.segment_identifier_id
 ORDER BY ntwk_id, prot_zone_id, segment_identifier_id, eqp_type
 ;
 
 v_prev_seg_id_id := ' ';
 FOR c IN segment_created_csr LOOP
 IF c.n_segment_id_id != v_prev_seg_id_id THEN
 BEGIN
 INSERT INTO structools.nss_segment_fpass
 VALUES (structools.nss_segment_seq.NEXTVAL
 ,v_segment_type_id
 ,c.n_segment_id_id
 ,v_start_dt
 ,v_null_end_dt)
 RETURNING segment_id INTO v_segment_id;
 IF v_prev_seg_id_id = ' ' THEN
 	o_seg_id_first := v_segment_id;
 END IF;
 o_seg_id_last := v_segment_id;
 EXCEPTION
 WHEN OTHERS THEN
 dbms_output.put_line('Exception c.n_segment_id_id = '||c.n_segment_id_id);
 -- COMMIT;
 RAISE;
 END;

 v_new_seg_arr(c.n_segment_id_id) := v_segment_id; /* save segmnet id of the new segment in an associative array keyed by seg id id */
 INSERT INTO structools.nss_segment_attribute_fpass
 VALUES (v_segment_id
 ,'NTWK_ID'
 ,TO_CHAR(c.n_ntwk_id));
 INSERT INTO structools.nss_segment_attribute_fpass
 VALUES (v_segment_id
 ,'PDEV_ID'
 ,TO_CHAR(c.n_pdev_id));
 INSERT INTO structools.nss_segment_attribute_fpass
 VALUES (v_segment_id
 ,'SEG_TYPE'
 ,TO_CHAR(c.n_seg_type));
 INSERT INTO structools.nss_segment_attribute_fpass
 VALUES (v_segment_id
 ,'CARR_LEN_M'
 ,TO_CHAR(c.n_carr_len_m));
 INSERT INTO structools.nss_segment_attribute_fpass
 VALUES (v_segment_id
 ,'B_S_IND'
 ,TO_CHAR(c.n_b_s_ind));
 INSERT INTO structools.nss_segment_attribute_fpass
 VALUES (v_segment_id
 ,'MIN_AVG_CARR_AGE'
 ,TO_CHAR(c.n_min_avg_carr_age));
 INSERT INTO structools.nss_segment_attribute_fpass
 VALUES (v_segment_id
 ,'MAX_AVG_CARR_AGE'
 ,TO_CHAR(c.n_max_avg_carr_age));
 INSERT INTO structools.nss_segment_attribute_fpass
 VALUES (v_segment_id
 ,'CARR_MAT_CDE_CNT'
 ,TO_CHAR(c.n_carr_mat_cde_cnt));
 INSERT INTO structools.nss_segment_attribute_fpass
 VALUES (v_segment_id
 ,'CARR_MAT_CDE'
 ,TO_CHAR(c.n_carr_mat_cde));
 INSERT INTO structools.nss_segment_attribute_fpass
 VALUES (v_segment_id
 ,'ZONE_SUB'
 ,TO_CHAR(c.n_zone_sub));
 INSERT INTO structools.nss_segment_attribute_fpass
 VALUES (v_segment_id
 ,'MTZN'
 ,TO_CHAR(c.n_mtzn));
 INSERT INTO structools.nss_segment_attribute_fpass
 VALUES (v_segment_id
 ,'OH_UG_IND'
 ,TO_CHAR(c.n_oh_ug_ind));
 FOR c2 IN segment_eqp_created_csr(c.n_segment_id_id, c.n_ntwk_id) LOOP
 BEGIN
 INSERT INTO structools.nss_segment_asset_fpass
 VALUES (structools.nss_segment_asset_seq.NEXTVAL
 ,v_segment_id
 ,c2.n_pick_id
 ,c2.eqp_type)
 ;
 EXCEPTION
 WHEN OTHERS THEN
 dbms_output.put_line('Exception on inserting nss_segment_asset_fpass. v_segment_id = '||v_segment_id||' pick id = '||c2.n_pick_id
 ||' '||c2.eqp_type||' '||TO_CHAR(v_start_dt,'dd-mm-yyyy')||' '||c.n_segment_id_id);
 RAISE;
 END;
 END LOOP;
 END IF;
 v_prev_seg_id_id := c.n_segment_id_id;
 END LOOP;


 COMMIT; 
 --ROLLBACK; --graz
 dbms_output.put_line('Ended '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
 
 combine_segments(o_seg_id_first, o_seg_id_last);
 
 establish_segment_mtzn(o_seg_id_first, o_seg_id_last);

 EXCEPTION
 WHEN v_prog_excp THEN
 RAISE;
 WHEN OTHERS THEN
 dbms_output.put_line('Exception '||TO_CHAR(SYSDATE,'hh24:mi:ss')||' '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
 dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
 RAISE;


 END create_segment_type_1;

 PROCEDURE combine_segments(i_seg_id_first IN PLS_INTEGER
 								 ,i_seg_id_last IN PLS_INTEGER)	 IS

 v_null_end_dt CONSTANT DATE := TO_DATE('01-01-3000','dd-mm-yyyy'); 
 v_seg_len_min_parm CONSTANT PLS_INTEGER := 500; 
 v_segment_type_id CONSTANT PLS_INTEGER := 1;
 
 v_mat_cde VARCHAR2(50);
 v_seg_id_id_prev VARCHAR2(20);
 
 TYPE mat_cde_arr_typ IS TABLE OF CHAR(1) INDEX BY VARCHAR2(20);
 v_mat_cde_arr mat_cde_arr_typ;
 
 TYPE seg_mat_cde_arr_typ IS TABLE OF mat_cde_arr_typ INDEX BY VARCHAR2(20);
 v_seg_mat_cde_arr seg_mat_cde_arr_typ; 

 CURSOR seg_mat_cde_csr IS
 
 SELECT segment_identifier_id AS seg_id_id
 ,material_cde AS mat_cde
 FROM structools.nss_segment_mat_cde 
 ORDER BY segment_identifier_id
 ; 

 PROCEDURE update_segments (i_down_seg_id IN PLS_INTEGER, i_up_seg_id IN PLS_INTEGER, i_seg_len IN PLS_INTEGER
 , i_mat_cde_arr IN mat_cde_arr_typ, i_oh_ug_ind IN CHAR) IS
 
 
 CURSOR seg_assets_csr IS
 
 SELECT pick_id
 ,eqp_type
 FROM structools.nss_segment_asset
 WHERE segment_id = i_down_seg_id
 ;
 
 v_mat_cde VARCHAR2(50);
 v_mat_cde_cnt PLS_INTEGER;
 v_min_age NUMBER(5,1);
 v_max_age NUMBER(5,1);
 v_min_age2 NUMBER(5,1);
 v_max_age2 NUMBER(5,1);
 
 DUPLICATE_RECORD EXCEPTION;
 PRAGMA EXCEPTION_INIT (DUPLICATE_RECORD, -1); 
 
 
 BEGIN
 
 /* Transfer asset from the scratched segment (i_down_seg_id) to the upstream segment (i_up_seg_id)*/
 -- dbms_output.put_line('Getting assets for '||i_down_seg_id);
 FOR c IN seg_assets_csr LOOP
 BEGIN
 INSERT INTO structools.nss_segment_asset
 VALUES (structools.nss_segment_asset_seq.NEXTVAL
 ,i_up_seg_id
 ,c.pick_id
 ,c.eqp_type)
 ;
 EXCEPTION
 WHEN DUPLICATE_RECORD THEN
 NULL;
 END;
 -- dbms_output.put_line('Transfered '||c.pick_id||' from '||i_down_seg_id||' to '||i_up_seg_id);
 END LOOP;

 /* Update attributes of the upstream upstream segment*/
 
 /* B_S_IND does not need updating, as the upstream segment's value should also be true for the combined segments */
 /* SEG_TYPE does not need updating, as the upstream segment's value should also be true for the combined segments */
 /* NTWK_ID does not need updating, as segments do not cross network boundaries */ 
 /* PDEV_ID does not need updating. If we combine a protection zones with its upstream protection zone,
 /* the upstream one becomes the PDEV_ID by default. */
 /* ZONE_SUB does not need updating.
 /* MTZN does not need updating. If we combine a segment with its upstream segment, mtzn of the upstream one will be corrrect
 /* for the combined segment (mtzn of a segment = mtzn of its entry switch/hvtm). */
 
 /* Update length */
 UPDATE structools.nss_segment_attribute
 SET attribute_val = TO_CHAR(i_seg_len)
 WHERE segment_id = i_up_seg_id
 AND attribute_key = 'CARR_LEN_M'
 ;
 -- dbms_output.put_line('Updated length of '||i_up_seg_id||' to '||i_seg_len);
 /* Update min and max avg_section_age */

 SELECT TO_NUMBER(A.attribute_val) AS min_age
 ,TO_NUMBER(b.attribute_val) AS max_age
 INTO v_min_age
 ,v_max_age 
 FROM structools.nss_segment_attribute A INNER JOIN
 structools.nss_segment_attribute b
 ON A.segment_id = b.segment_id
 WHERE A.segment_id = i_down_seg_id
 AND A.attribute_key = 'MIN_AVG_CARR_AGE' 
 AND b.attribute_key = 'MAX_AVG_CARR_AGE';
 
 SELECT TO_NUMBER(A.attribute_val) AS min_age
 ,TO_NUMBER(b.attribute_val) AS max_age
 INTO v_min_age2
 ,v_max_age2 
 FROM structools.nss_segment_attribute A INNER JOIN
 structools.nss_segment_attribute b
 ON A.segment_id = b.segment_id
 WHERE A.segment_id = i_up_seg_id
 AND A.attribute_key = 'MIN_AVG_CARR_AGE' 
 AND b.attribute_key = 'MAX_AVG_CARR_AGE';
 
 IF v_min_age < v_min_age2 AND v_min_age != 0 THEN
 UPDATE structools.nss_segment_attribute
 SET attribute_val = TO_CHAR(v_min_age)
 WHERE segment_id = i_up_seg_id
 AND attribute_key = 'MIN_AVG_CARR_AGE';
 -- dbms_output.put_line('Updated min age of '||i_up_seg_id||' to '||v_min_age); 
 END IF;
 
 IF v_max_age > v_max_age2 THEN
 UPDATE structools.nss_segment_attribute
 SET attribute_val = TO_CHAR(v_max_age)
 WHERE segment_id = i_up_seg_id
 AND attribute_key = 'MAX_AVG_CARR_AGE';
 -- dbms_output.put_line('Updated max age of '||i_up_seg_id||' to '||v_max_age);
 END IF;
 
 /* Update carrier mat code count */
 v_mat_cde_cnt := i_mat_cde_arr.COUNT;

 UPDATE structools.nss_segment_attribute
 SET attribute_val = TO_CHAR(v_mat_cde_cnt)
 WHERE segment_id = i_up_seg_id
 AND attribute_key = 'CARR_MAT_CDE_CNT';
 -- dbms_output.put_line('Updated mat_cde_cnt of '||i_up_seg_id||' to '||v_mat_cde_cnt);

 /* Update carrier mat code in case it was null. If multiple mat types, just get the first one */
 v_mat_cde := i_mat_cde_arr.FIRST;
 UPDATE structools.nss_segment_attribute
 SET attribute_val = v_mat_cde
 WHERE segment_id = i_up_seg_id
 AND attribute_key = 'CARR_MAT_CDE';
 -- dbms_output.put_line('Updated carr mat cde '||i_up_seg_id||' to '||v_mat_cde); 
 
 /* Update oh_ug_indicator */
 IF i_oh_ug_ind IS NOT NULL THEN
 UPDATE structools.nss_segment_attribute
 SET attribute_val = i_oh_ug_ind
 WHERE segment_id = i_up_seg_id
 AND attribute_key = 'OH_UG_IND';
 -- dbms_output.put_line('Updated oh_ug_ind '||i_up_seg_id||' to '||i_oh_ug_ind); 
 END IF; 
 
 
 /* Delete the downstream segment. Data in its child tables will be deleted due to cascade rule */

 DELETE FROM structools.nss_segment
 WHERE segment_id = i_down_seg_id; 
 
 END update_segments;

 PROCEDURE combine_short_segments(i_cursor IN CHAR, i_cross_pz IN CHAR) IS
 
 CURSOR short_segments_csr1 IS
 
 /* Get all segments where length is < 500 m. Get their parent segment. */

 WITH parent_segments AS 
 (
 SELECT A.pick_id
 ,b.segment_identifier_id
 ,b.segment_id
 FROM structools.nss_segment_asset A INNER JOIN
 structools.nss_segment b
 ON A.segment_id = b.segment_id
 WHERE A.pick_id LIKE 'N%'
 AND A.pick_id != b.segment_identifier_id
 AND b.segment_type_id = v_segment_type_id 
 AND b.end_dt = v_null_end_dt 
 ORDER BY A.segment_id
 )
 , 
 short_segments AS
 (
 SELECT A.segment_id AS short_seg_id
 ,b.segment_identifier_id AS short_seg_id_id
 FROM structools.nss_segment_attribute A INNER JOIN 
 structools.nss_segment b
 ON A.segment_id = b.segment_id 
 WHERE b.end_dt = v_null_end_dt
 AND b.segment_type_id = v_segment_type_id
 AND A.attribute_key = 'CARR_LEN_M'
 AND TO_NUMBER(A.attribute_val) < v_seg_len_min_parm
 )
 ,
 short_seg_with_parent AS
 (
 SELECT A.short_seg_id
 ,A.short_seg_id_id
 ,b.segment_identifier_id AS parent_seg_id_id
 ,b.segment_id AS parent_seg_id
 FROM short_segments A LEFT OUTER JOIN
 parent_segments b
 ON A.short_seg_id_id = b.pick_id
 )
 ,
 short_segments_attr AS
 (
 SELECT * 
 FROM
 (
 SELECT A.short_seg_id 
 ,b.attribute_key
 ,b.attribute_val
 FROM short_seg_with_parent A INNER JOIN
 structools.nss_segment_attribute b
 ON A.short_seg_id = b.segment_id
 )
 PIVOT (MAX(attribute_val) FOR attribute_key IN ('NTWK_ID' short_ntwk_id, 'PDEV_ID' short_pdev_id, 'SEG_TYPE' short_seg_type
 ,'CARR_LEN_M' short_carr_len_m
 ,'B_S_IND' short_b_s_ind, 'MIN_AVG_CARR_AGE' short_min_avg_carr_age
 ,'MAX_AVG_CARR_AGE' short_max_avg_carr_age
 ,'CARR_MAT_CDE_CNT' short_carr_mat_cde_cnt, 'CARR_MAT_CDE' short_carr_mat_cde
 ,'ZONE_SUB' zone_sub, 'MTZN' mtzn, 'OH_UG_IND' oh_ug_ind))
 ) 
 ,
 parent_segments_attr AS
 (
 SELECT * 
 FROM
 (
 SELECT A.parent_seg_id 
 ,b.attribute_key
 ,b.attribute_val
 FROM short_seg_with_parent A INNER JOIN
 structools.nss_segment_attribute b
 ON A.parent_seg_id = b.segment_id
 )
 PIVOT (MAX(attribute_val) FOR attribute_key IN ('NTWK_ID' parent_ntwk_id, 'PDEV_ID' parent_pdev_id, 'SEG_TYPE' parent_seg_type
 ,'CARR_LEN_M' parent_carr_len_m
 ,'B_S_IND' parent_b_s_ind, 'MIN_AVG_CARR_AGE' parent_min_avg_carr_age
 ,'MAX_AVG_CARR_AGE' parent_max_avg_carr_age
 ,'CARR_MAT_CDE_CNT' parent_carr_mat_cde_cnt, 'CARR_MAT_CDE' parent_carr_mat_cde
 ,'ZONE_SUB' zone_sub, 'MTZN' mtzn, 'OH_UG_IND' oh_ug_ind)) 
 )
 SELECT A.short_seg_id AS down_seg_id 
 ,A.short_seg_id_id AS down_seg_id_id
 ,b.short_ntwk_id AS down_ntwk_id
 ,b.short_pdev_id AS down_pdev_id 
 ,b.short_seg_type AS down_seg_type
 ,TO_NUMBER(b.short_carr_len_m) AS down_carr_len_m
 ,b.short_b_s_ind AS down_b_s_ind
 ,TO_NUMBER(b.short_min_avg_carr_age) AS down_min_avg_carr_age
 ,TO_NUMBER(b.short_max_avg_carr_age) AS down_max_avg_carr_age
 ,b.short_carr_mat_cde_cnt AS down_carr_mat_cde_cnt
 ,b.short_carr_mat_cde AS down_carr_mat_cde
 ,b.zone_sub AS down_zone_sub
 ,b.mtzn AS down_mtzn
 ,b.oh_ug_ind AS down_oh_ug_ind
 ,A.parent_seg_id AS up_seg_id
 ,A.parent_seg_id_id AS up_seg_id_id
 ,c.parent_ntwk_id AS up_ntwk_id
 ,c.parent_pdev_id AS up_pdev_id
 ,c.parent_seg_type AS up_seg_type
 ,TO_NUMBER(c.parent_carr_len_m) AS up_carr_len_m
 ,c.parent_b_s_ind AS up_b_s_ind
 ,TO_NUMBER(c.parent_min_avg_carr_age) AS up_min_avg_carr_age
 ,TO_NUMBER(c.parent_max_avg_carr_age) AS up_max_avg_carr_age
 ,TO_NUMBER(c.parent_carr_mat_cde_cnt) AS up_carr_mat_cde_cnt
 ,c.parent_carr_mat_cde AS up_carr_mat_cde
 ,c.zone_sub AS up_zone_sub
 ,c.mtzn AS up_mtzn
 ,c.oh_ug_ind AS up_oh_ug_ind
 FROM short_seg_with_parent A INNER JOIN
 short_segments_attr b
 ON A.short_seg_id = b.short_seg_id 
 LEFT OUTER JOIN
 parent_segments_attr c
 ON A.parent_seg_id = c.parent_seg_id
 --WHERE b.short_ntwk_id = 1774676 --graz 
 ORDER BY short_carr_len_m, up_seg_id 
 ;

 /* Get all segments where length is < 500 m. Get their child segments */

 CURSOR short_segments_csr2 IS

 WITH short_segments AS
 (
 SELECT A.segment_id AS short_seg_id
 ,b.segment_identifier_id AS short_seg_id_id
 ,c.pick_id AS short_seg_pot_child_id
 FROM structools.nss_segment_attribute A INNER JOIN 
 structools.nss_segment b
 ON A.segment_id = b.segment_id INNER JOIN 
 structools.nss_segment_asset c
 ON A.segment_id = c.segment_id 
 WHERE A.attribute_key = 'CARR_LEN_M'
 AND TO_NUMBER(A.attribute_val) < v_seg_len_min_parm
 AND c.pick_id LIKE 'N%'
 AND b.end_dt = v_null_end_dt
 AND b.segment_type_id = v_segment_type_id
 AND c.pick_id != b.segment_identifier_id
 ORDER BY short_seg_id
 )
 ,
 short_seg_with_child AS
 (
 SELECT A.short_seg_id
 ,A.short_seg_id_id
 ,A.short_seg_pot_child_id
 ,b.segment_identifier_id AS child_seg_id_id
 ,b.segment_id AS child_seg_id 
 FROM short_segments A INNER JOIN
 structools.nss_segment b
 ON A.short_seg_pot_child_id = b.segment_identifier_id
 WHERE b.segment_type_id = v_segment_type_id
 AND b.end_dt = v_null_end_dt
 )
 ,
 short_segments_attr AS
 (
 SELECT * 
 FROM
 (
 SELECT A.short_seg_id 
 ,b.attribute_key
 ,b.attribute_val
 FROM short_seg_with_child A INNER JOIN
 structools.nss_segment_attribute b
 ON A.short_seg_id = b.segment_id
 )
 PIVOT (MAX(attribute_val) FOR attribute_key IN ('NTWK_ID' short_ntwk_id, 'PDEV_ID' short_pdev_id, 'SEG_TYPE' short_seg_type
 ,'CARR_LEN_M' short_carr_len_m
 ,'B_S_IND' short_b_s_ind, 'MIN_AVG_CARR_AGE' short_min_avg_carr_age
 ,'MAX_AVG_CARR_AGE' short_max_avg_carr_age
 ,'CARR_MAT_CDE_CNT' short_carr_mat_cde_cnt, 'CARR_MAT_CDE' short_carr_mat_cde
 ,'ZONE_SUB' zone_sub, 'MTZN' mtzn, 'OH_UG_IND' oh_ug_ind)) 
 ) 
 ,
 child_segments_attr AS
 (
 SELECT * 
 FROM
 (
 SELECT A.child_seg_id 
 ,b.attribute_key
 ,b.attribute_val
 FROM short_seg_with_child A INNER JOIN
 structools.nss_segment_attribute b
 ON A.child_seg_id = b.segment_id
 )
 PIVOT (MAX(attribute_val) FOR attribute_key IN ('NTWK_ID' child_ntwk_id, 'PDEV_ID' child_pdev_id, 'SEG_TYPE' child_seg_type,'CARR_LEN_M' child_carr_len_m
 ,'B_S_IND' child_b_s_ind, 'MIN_AVG_CARR_AGE' child_min_avg_carr_age
 ,'MAX_AVG_CARR_AGE' child_max_avg_carr_age
 ,'CARR_MAT_CDE_CNT' child_carr_mat_cde_cnt, 'CARR_MAT_CDE' child_carr_mat_cde
 ,'ZONE_SUB' zone_sub, 'MTZN' mtzn, 'OH_UG_IND' oh_ug_ind)) 
 )
 SELECT A.child_seg_id AS down_seg_id 
 ,A.child_seg_id_id AS down_seg_id_d
 ,c.child_ntwk_id AS down_ntwk_id
 ,c.child_pdev_id AS down_pdev_id
 ,c.child_seg_type AS down_seg_type
 ,TO_NUMBER(c.child_carr_len_m) AS down_carr_len_m
 ,c.child_b_s_ind AS down_b_s_ind
 ,TO_NUMBER(c.child_min_avg_carr_age) AS down_min_avg_carr_age
 ,TO_NUMBER(c.child_max_avg_carr_age) AS down_max_avg_carr_age
 ,TO_NUMBER(c.child_carr_mat_cde_cnt) AS down_carr_mat_cde_cnt
 ,c.child_carr_mat_cde AS down_carr_mat_cde
 ,c.zone_sub AS down_zone_sub
 ,c.mtzn AS down_mtzn
 ,c.oh_ug_ind AS down_oh_ug_ind
 ,A.short_seg_id AS up_seg_id
 ,A.short_seg_id_id AS up_seg_id_id
 ,b.short_ntwk_id AS up_ntwk_id
 ,b.short_pdev_id AS up_pdev_id
 ,b.short_seg_type AS up_seg_type
 ,TO_NUMBER(b.short_carr_len_m) AS up_carr_len_m
 ,b.short_b_s_ind AS up_b_s_ind
 ,TO_NUMBER(b.short_min_avg_carr_age) AS up_min_avg_carr_age
 ,TO_NUMBER(b.short_max_avg_carr_age) AS up_max_avg_carr_age
 ,b.short_carr_mat_cde_cnt AS up_carr_mat_cde_cnt
 ,b.short_carr_mat_cde AS up_carr_mat_cde 
 ,b.zone_sub AS up_zone_sub
 ,b.mtzn AS up_mtzn
 ,b.oh_ug_ind AS up_oh_ug_ind
 FROM short_seg_with_child A INNER JOIN
 short_segments_attr b
 ON A.short_seg_id = b.short_seg_id 
 LEFT OUTER JOIN
 child_segments_attr c
 ON A.child_seg_id = c.child_seg_id 
 ORDER BY short_carr_len_m, down_seg_id 
 ; 
 
 v_seg_rec short_segments_csr1%ROWTYPE;
 TYPE seg_arr_typ IS TABLE OF short_segments_csr1%ROWTYPE INDEX BY PLS_INTEGER;
 v_seg_arr seg_arr_typ;
 
 TYPE updated_seg_arr IS TABLE OF CHAR(1) INDEX BY PLS_INTEGER;
 v_updated_seg_arr updated_seg_arr;
 
 v_down_seg_id PLS_INTEGER;
 v_up_seg_id PLS_INTEGER;
 v_down_seg_id_id VARCHAR2(20);
 v_up_seg_id_id VARCHAR2(20);
 -- v_mat_cde VARCHAR2(50);
 v_combined_carr_len PLS_INTEGER;
 v_dummy_seg_id PLS_INTEGER;
 v_combine BOOLEAN;
 v_updated BOOLEAN;
 v_mat_cde_arr_down mat_cde_arr_typ;
 v_mat_cde_arr_up mat_cde_arr_typ;
 v_oh_ug_ind CHAR(2);
-- v_cnt1 PLS_INTEGER;
-- v_cnt2 PLS_INTEGER;

 BEGIN

 v_updated := TRUE;
-- v_cnt1 := 0; --graz
 WHILE v_updated LOOP 
-- v_cnt1 := v_cnt1 + 1; --graz
-- dbms_output.put_line('outer loop');
-- IF v_cnt1 > 2 THEN
-- exit;
-- END IF;
 v_updated := FALSE;
 v_seg_arr.DELETE;
 IF i_cursor = 'csr1' THEN
 OPEN short_segments_csr1;
 LOOP
 FETCH short_segments_csr1 INTO v_seg_rec;
 EXIT WHEN short_segments_csr1%NOTFOUND;
 v_seg_arr(v_seg_rec.down_seg_id) := v_seg_rec;
 END LOOP;
 CLOSE short_segments_csr1;
 ELSE
 OPEN short_segments_csr2;
 LOOP
 FETCH short_segments_csr2 INTO v_seg_rec;
 EXIT WHEN short_segments_csr2%NOTFOUND;
 v_seg_arr(v_seg_rec.down_seg_id) := v_seg_rec;
 END LOOP;
 CLOSE short_segments_csr2;
 END IF; 

 v_updated_seg_arr.DELETE;

 v_down_seg_id := v_seg_arr.FIRST; 
 v_updated_seg_arr.DELETE;
-- v_cnt2 := 0; --graz
 WHILE v_down_seg_id IS NOT NULL LOOP
-- dbms_output.put_line('inner loop');
-- v_cnt2 := v_cnt2 + 1; --graz
-- IF v_cnt2 > 3 THEN
-- exit;
-- END IF;
 v_seg_rec := v_seg_arr(v_down_seg_id);
 v_up_seg_id := v_seg_rec.up_seg_id;
 v_down_seg_id_id := v_seg_rec.down_seg_id_id;
 v_up_seg_id_id := v_seg_rec.up_seg_id_id;
 
-- dbms_output.put_line(v_down_seg_id||' '||v_seg_rec.down_seg_id_id --graz
-- ||' '||v_seg_rec.down_ntwk_id||' '||v_seg_rec.down_pdev_id
-- ||' '||v_seg_rec.down_seg_type||' '||v_seg_rec.down_carr_len_m
-- ||' '||v_seg_rec.down_oh_ug_ind||' '||v_seg_rec.down_oh_ug_ind
-- ||' '||v_seg_rec.down_b_s_ind||' '||v_seg_rec.down_min_avg_carr_age
-- ||' '||v_seg_rec.down_max_avg_carr_age||' '||v_seg_rec.down_carr_mat_cde_cnt
-- ||' '||v_seg_rec.down_carr_mat_cde
-- ||' '||v_seg_rec.down_zone_sub||' '||v_seg_rec.down_mtzn
-- ||' '||v_up_seg_id||' '||v_seg_rec.up_seg_id_id
-- ||' '||v_seg_rec.up_ntwk_id||' '||v_seg_rec.up_pdev_id
-- ||' '||v_seg_rec.up_seg_type||' '||v_seg_rec.up_carr_len_m
-- ||' '||v_seg_rec.up_oh_ug_ind||' '||v_seg_rec.up_oh_ug_ind
-- ||' '||v_seg_rec.up_b_s_ind||' '||v_seg_rec.up_min_avg_carr_age
-- ||' '||v_seg_rec.up_max_avg_carr_age||' '||v_seg_rec.up_carr_mat_cde_cnt
-- ||' '||v_seg_rec.up_carr_mat_cde
-- ||' '||v_seg_rec.up_zone_sub||' '||v_seg_rec.up_mtzn);
 
 IF v_seg_rec.down_ntwk_id = v_seg_rec.up_ntwk_id 
 AND (v_seg_rec.down_oh_ug_ind = v_seg_rec.up_oh_ug_ind OR v_seg_rec.down_oh_ug_ind IS NULL OR v_seg_rec.up_oh_ug_ind IS NULL) THEN
 IF i_cross_pz = 'N' AND v_seg_rec.down_pdev_id = v_seg_rec.up_pdev_id
 OR i_cross_pz = 'Y' THEN
 v_combine := TRUE;
 v_combined_carr_len := v_seg_rec.down_carr_len_m + v_seg_rec.up_carr_len_m;
 /* Update segment length for the upstream segment (if the upstream segment is also in the 'short' list) */
 IF v_updated_seg_arr.EXISTS(v_up_seg_id) OR v_updated_seg_arr.EXISTS(v_down_seg_id) THEN
 v_combine := FALSE;
 END IF;
 IF v_combine THEN
 /* Transfer mat code(s) to the upstream segment in the assoc array of material types for each segment */
 v_mat_cde_arr_down := v_seg_mat_cde_arr(v_down_seg_id_id);
 v_mat_cde_arr_up := v_seg_mat_cde_arr(v_up_seg_id_id);
 v_mat_cde := v_mat_cde_arr_down.FIRST;
 WHILE v_mat_cde IS NOT NULL LOOP
 v_mat_cde_arr_up(v_mat_cde) := ' ';
 v_mat_cde := v_mat_cde_arr_down.NEXT(v_mat_cde);
 END LOOP;
 v_seg_mat_cde_arr(v_up_seg_id_id) := v_mat_cde_arr_up;
 IF v_seg_rec.up_oh_ug_ind IS NULL AND v_seg_rec.down_oh_ug_ind IS NOT NULL THEN
 v_oh_ug_ind := v_seg_rec.down_oh_ug_ind;
 ELSE
 v_oh_ug_ind := NULL;
 END IF;
 
 /* Update db */
 update_segments(v_down_seg_id, v_up_seg_id, v_combined_carr_len, v_mat_cde_arr_up, v_oh_ug_ind);
 /* add the upstream segment id to the array of updated segments */
 v_updated_seg_arr(v_up_seg_id) := ' ';
 v_updated_seg_arr(v_down_seg_id) := ' ';
 v_updated := TRUE;
 END IF;
 ELSE
 NULL;
-- dbms_output.put_line('Prot dev different and not allowed to cross pz boundary');
 END IF;
 END IF;
 v_down_seg_id := v_seg_arr.NEXT(v_down_seg_id);
 END LOOP;
 END LOOP;
 END combine_short_segments; 

 PROCEDURE establish_spur_id IS
 /* Establish the highest level SPUR segment and make it a spur_id to all children segments. */
 /* Only segmnets with spur_id will have the attribute with attribute_key of SPUR_ID */
 /* Some segments with value 'SPUR for attribute 'B_S_IND' will not have a spur_id, meaning */
 /* the segments initially flagged as SPUR has no parent of BACKBONE type, which indicates that flagging */
 /* the segments as SPUR or BACKBONE based on count of customers downstream is flawed, ie some networks */
 /* will have very dew */ 
 
 
 /* Cursor to get all current segments flagged as SPUR, and their parent and their B_S_IND */
 CURSOR spurs_csr IS

 WITH spur_parent AS
 (
 SELECT segment_id
 ,segment_identifier_id
 ,parent_seg_id
 ,parent_segment_identifier_id
 FROM
 (
 SELECT b.segment_id
 ,b.segment_identifier_id 
 ,c.segment_id AS parent_seg_id
 ,d.segment_identifier_id AS parent_segment_identifier_id
 ,MIN(d.segment_identifier_id) OVER (PARTITION BY b.segment_identifier_id) AS u_parent_segment_identifier_id
 FROM structools.nss_segment_attribute A INNER JOIN
 structools.nss_segment b
 ON A.segment_id = b.segment_id
 INNER JOIN
 structools.nss_segment_asset c
 ON b.segment_identifier_id = c.pick_id 
 INNER JOIN
 structools.nss_segment d
 ON c.segment_id = d.segment_id 
 WHERE A.attribute_key = 'B_S_IND'
 AND A.attribute_val = 'SPUR'
 AND b.segment_id != c.segment_id
 AND b.segment_type_id = v_segment_type_id
 AND b.end_dt = v_null_end_dt 
 AND d.segment_type_id = v_segment_type_id
 AND d.end_dt = v_null_end_dt 
 ) aa
 WHERE aa.parent_segment_identifier_id = aa.u_parent_segment_identifier_id
 )
 SELECT A.segment_id
 ,A.segment_identifier_id
 ,A.parent_seg_id
 ,A.parent_segment_identifier_id
 ,b.attribute_val AS parent_b_s_ind
 FROM spur_parent A INNER JOIN
 structools.nss_segment_attribute b
 ON A.parent_seg_id = b.segment_id 
 WHERE b.attribute_key = 'B_S_IND'
 ORDER BY segment_identifier_id
 ;
 
 v_spurs_rec spurs_csr%ROWTYPE; 
 TYPE spur_tab_typ IS TABLE OF spurs_csr%ROWTYPE INDEX BY PLS_INTEGER;
 v_spur_tab spur_tab_typ;
 v_spur_tab2 spur_tab_typ;
 v_spur PLS_INTEGER;
 v_spur_tmp PLS_INTEGER;
 v_spur_id VARCHAR2(10);
 v_end_of_trace BOOLEAN;
 TYPE trace_tab_typ IS TABLE OF char(1) INDEX BY PLS_INTEGER;
 v_trace_tab trace_tab_typ;

 BEGIN

 dbms_output.put_line('Start establish_spur_id '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
 /* collect all spurs with a parnet into an associtainve array indexed by segment id */
 OPEN spurs_csr;
 LOOP 
 FETCH spurs_csr INTO v_spurs_rec;
 EXIT WHEN spurs_csr%NOTFOUND; 
 v_spur_tab(v_spurs_rec.segment_id) := v_spurs_rec;
 END LOOP;
 CLOSE spurs_csr;
 
 dbms_output.put_line('establish_spur_id 1 '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
 
 /* create another tableas exact copy */
 v_spur_tab2 := v_spur_tab;

 /* go through the table and established the highest level parent spur for each spur */
 v_spur := v_spur_tab.FIRST;
 v_trace_tab.DELETE;
 v_trace_tab(v_spur) := ' ';
 WHILE v_spur IS NOT NULL LOOP
 v_spur_id := NULL;
 IF v_spur_tab(v_spur).parent_b_s_ind = 'BACKBONE' THEN
 v_spur_id := v_spur_tab(v_spur).segment_identifier_id;
 ELSE 
 v_end_of_trace := FALSE;
 v_spur_tmp := v_spur_tab(v_spur).parent_seg_id;
 WHILE NOT v_end_of_trace LOOP -- trace until top level spur found, using the copy table 
 IF v_spur_tab2.EXISTS(v_spur_tmp) THEN
 v_trace_tab(v_spur_tmp) := ' ';
 IF v_spur_tab2(v_spur_tmp).parent_b_s_ind = 'BACKBONE' THEN
 v_end_of_trace := TRUE;
 v_spur_id := v_spur_tab2(v_spur_tmp).segment_identifier_id;
 ELSE 
 IF v_trace_tab.EXISTS(v_spur_tab2(v_spur_tmp).parent_seg_id) THEN
 dbms_output.put_line('loop detected starting with spur '||v_spur);
 v_spur_id := v_spur_tab2(v_spur_tmp).segment_identifier_id;
 v_end_of_trace := TRUE; 
 dbms_output.put_line('v_spur_id = '||v_spur_id);
 ELSE
 v_spur_tmp := v_spur_tab2(v_spur_tmp).parent_seg_id;
 END IF;
 END IF;
 ELSE
 v_end_of_trace := TRUE;
 END IF; 
 END LOOP;
 END IF;
 IF v_spur_id IS NOT NULL THEN
 -- dbms_output.put_line ('INSERTING '||v_spur||' '||'SPUR_ID'||' '||v_spur_id);
 INSERT INTO structools.nss_segment_attribute
 VALUES(v_spur
 ,'SPUR_ID'
 ,v_spur_id
 );
 END IF;
 v_spur := v_spur_tab.NEXT(v_spur);
 END LOOP;
 dbms_output.put_line('establish_spur_id 2 '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
 
 COMMIT;

 EXCEPTION
 WHEN OTHERS THEN
 dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
 dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
 RAISE;

 END establish_spur_id;
 

 /* Mainline for combine_segments */ 
 BEGIN
 /* Get existing mat cdes for all segments from the temp seg_mat_cde table into associative array */
 /* This step is redundant if it is done as part of the main code for segment creation. */
 /* If these functions are combined, then the nss_segment_mat_cde table can be dropped and code removed */
 /* Keep it, as it is handy if we want to run both procedures (create segments and combine segments) separately, */
 /* but it is getting info from global temp tables, so it will be lost if we establish a new Oracle session, in which */
 /* case the whole thing needs to be rerun. */
 
 -- dbms_output.put_line('segments with mat code array');
 v_seg_id_id_prev := ' ';
 FOR c IN seg_mat_cde_csr LOOP
 IF c.seg_id_id != v_seg_id_id_prev THEN
 IF v_seg_id_id_prev != ' ' THEN
 v_seg_mat_cde_arr(v_seg_id_id_prev) := v_mat_cde_arr;
 -- dbms_output.put_line(v_seg_id_id_prev);
 v_mat_cde := v_mat_cde_arr.FIRST;
 WHILE v_mat_cde IS NOT NULL LOOP
 -- dbms_output.put_line(' '||v_mat_cde);
 v_mat_cde := v_mat_cde_arr.NEXT(v_mat_cde);
 END LOOP;
 END IF;
 v_mat_cde_arr.DELETE;
 END IF;
 IF c.mat_cde IS NOT NULL THEN
 v_mat_cde_arr(c.mat_cde) := ' ';
 END IF;
 v_seg_id_id_prev := c.seg_id_id;
 END LOOP;
 v_seg_mat_cde_arr(v_seg_id_id_prev) := v_mat_cde_arr;
 -- dbms_output.put_line(v_seg_id_id_prev);
 v_mat_cde := v_mat_cde_arr.FIRST;
 WHILE v_mat_cde IS NOT NULL LOOP
 -- dbms_output.put_line(' '||v_mat_cde);
 v_mat_cde := v_mat_cde_arr.NEXT(v_mat_cde);
 END LOOP;

		/* Copy results of the run from FPASS tables to primary tables, for the combine_segments procedure to update them. */
 /* At some point, we may want to give up keeping the FPASS tables, and create original results directly in the */
 /* primary tables. 																												 */		
 
 /* drop indexes in the primary tables for faster copy */
 /* Does not make that much difference, but the DDL performs an explicit COMMIT, which */
 /* is inconvenient in case of an exception, so not doing the drop and create indexes after all. */ 
 
-- drop_segment_indexes; 

		INSERT INTO structools.nss_segment
 SELECT * FROM structools.nss_segment_fpass
 WHERE segment_id BETWEEN i_seg_id_first AND i_seg_id_last;
 
		INSERT INTO structools.nss_segment_asset
 SELECT * FROM structools.nss_segment_asset_fpass
 WHERE segment_id BETWEEN i_seg_id_first AND i_seg_id_last;
 
		INSERT INTO structools.nss_segment_attribute
 SELECT * FROM structools.nss_segment_attribute_fpass
 WHERE segment_id BETWEEN i_seg_id_first AND i_seg_id_last;
 
-- create_segment_indexes;
 
 /* Combine short segments with their upstream segment within the same protection zone */
 dbms_output.put_line('=======');
 dbms_output.put_line('=========================================================');
 dbms_output.put_line('=========================================================');
 dbms_output.put_line('Combining short segments with an upstream segment');
 combine_short_segments('csr1','N');
 dbms_output.put_line('COMPLETED Combining short segments with an upstream segment');

 /* Combine short segments with their downstream segment within the same protection zone */
 dbms_output.put_line('=======');
 dbms_output.put_line('=========================================================');
 dbms_output.put_line('=========================================================');
 dbms_output.put_line('=========================================================');
 dbms_output.put_line('Combining short segments with a downstream segment');
 combine_short_segments('csr2','N');
 dbms_output.put_line('COMPLETED Combining short segments with a downstream segment');

 /* Combine short segments with their upstream segment, crossing protection zone boundary */
 dbms_output.put_line('=======');
 dbms_output.put_line('=========================================================');
 dbms_output.put_line('=========================================================');
 dbms_output.put_line('Combining short segments with an upstream segment crossing pz boundary');
 combine_short_segments('csr1','Y');
 dbms_output.put_line('COMPLETED Combining short segments with an upstream segment crossing pz boundary');

 /* Combine short segments with their downstream segment, crossing protection zone boundary */
 dbms_output.put_line('=======');
 dbms_output.put_line('=========================================================');
 dbms_output.put_line('=========================================================');
 dbms_output.put_line('Combining short segments with an downstream segment crossing pz boundary');
 combine_short_segments('csr2','Y');
 dbms_output.put_line('COMPLETED Combining short segments with an downstream segment crossing pz boundary');

		/* allocate one extra attribute to each current segment. Can only be done after combining. */
 establish_spur_id;
 
 -- ROLLBACK; --graz
 COMMIT; 
 EXCEPTION
 WHEN OTHERS THEN
 dbms_output.put_line('Exception '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
 dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
 RAISE;

 END combine_segments;
 
 PROCEDURE establish_segment_mtzn (i_seg_id_first IN PLS_INTEGER
 ,i_seg_id_last IN PLS_INTEGER) IS
 
 CURSOR seg_mtzn_csr IS
 WITH mtzn_len AS
 (
 SELECT A.segment_id
 ,c.maint_zone_nam
 ,SUM(d.length_m) AS seg_mtzn_len_m
 FROM structools.nss_segment_attribute A INNER JOIN
 structools.nss_segment_asset b
 ON A.segment_id = b.segment_id
 LEFT OUTER JOIN
 structoolsw.asset_geography c
 ON b.pick_id = c.pick_id 
 LEFT OUTER JOIN
 structoolsw.bay_detail d
 ON b.pick_id = d.pick_id 
 WHERE A.attribute_key = 'OH_UG_IND'
 AND A.attribute_val = 'OH' 
 AND A.segment_id BETWEEN i_seg_id_first AND i_seg_id_last
 AND b.pick_id LIKE 'B%'
 GROUP BY A.segment_id
 ,c.maint_zone_nam
 UNION
 SELECT A.segment_id
 ,c.maint_zone_nam
 ,SUM(d.length_m) AS seg_mtzn_len_m
 FROM structools.nss_segment_attribute A INNER JOIN
 structools.nss_segment_asset b
 ON A.segment_id = b.segment_id
 LEFT OUTER JOIN
 structoolsw.asset_geography c
 ON b.pick_id = c.pick_id 
 LEFT OUTER JOIN
 structoolsw.carrier_detail d
 ON b.pick_id = d.pick_id 
 WHERE A.attribute_key = 'OH_UG_IND'
 AND A.attribute_val != 'OH' 
 AND A.segment_id BETWEEN i_seg_id_first AND i_seg_id_last
 AND b.pick_id LIKE 'C%'
 GROUP BY A.segment_id
 ,c.maint_zone_nam
 )
 ,
 seg_len AS
 (
 SELECT * FROM
 (
 SELECT segment_id
 ,maint_zone_nam
 ,seg_mtzn_len_m
 ,ROW_NUMBER() OVER (PARTITION BY segment_id ORDER BY seg_mtzn_len_m DESC) AS rn
 FROM MTZN_LEN 
 )
 WHERE rn = 1 
 )
 ,
 seg_len_final AS
 (
 SELECT A.segment_id
 ,A.segment_identifier_id
 ,b.maint_zone_nam
 ,b.seg_mtzn_len_m
 FROM structools.nss_segment A LEFT OUTER JOIN
 seg_len b
 ON A.segment_id = b.segment_id
 WHERE A.segment_id BETWEEN i_seg_id_first AND i_seg_id_last
 )
 SELECT * FROM
 (
 SELECT A.*
 ,b.attribute_val
 FROM seg_len_final A INNER JOIN
 structools.nss_segment_attribute b
 ON A.segment_id = b.segment_id
 WHERE b.attribute_key = 'MTZN'
 ) aa
 WHERE maint_zone_nam != attribute_val OR maint_zone_nam IS NULL OR attribute_val IS NULL
 ORDER BY aa.segment_id
 ;
 
 BEGIN
 
 FOR c IN seg_mtzn_csr LOOP
 IF c.maint_zone_nam IS NOT NULL THEN
 UPDATE structools.NSS_SEGMENT_ATTRIBUTE
 SET attribute_val = c.maint_zone_nam
 WHERE attribute_key = 'MTZN'
 AND segment_id = c.segment_id;
 dbms_output.put_line('segment_id = '||c.segment_id||' segment_id_id = '||c.segment_identifier_id
 ||' Old mtzn '||c.attribute_val||' New mtzn '||c.maint_zone_nam);
 END IF;
 END LOOP;
 
 COMMIT;
 
 END establish_segment_mtzn;
 
 PROCEDURE delete_segment_version (i_segment_type	IN	PLS_INTEGER
 											,i_start_dt			IN DATE
 ,i_end_dt			IN DATE) IS

		v_seg_cnt						PLS_INTEGER := 0; 
 v_seg_fpass_cnt				PLS_INTEGER := 0;
 v_seg_id_min					PLS_INTEGER;
 v_seg_id_max					PLS_INTEGER; 
	
 BEGIN 
 
 SELECT COUNT(*)
 INTO				v_seg_cnt
 FROM				structools.nss_segment
 WHERE				segment_type_id = i_segment_type
 AND 				start_dt = i_start_dt
 AND				end_dt = i_end_dt;
 IF v_seg_cnt = 0 THEN
 dbms_output.put_line('Segment version '||TO_CHAR(i_start_dt, 'dd-mm-yy hh24:mi:ss')||' '||
 TO_CHAR(i_end_dt, 'dd-mm-yy hh24:mi:ss')||' for segment type '||i_segment_type||' does not exist');
 --RAISE v_prog_excp;
		ELSE 
 	dbms_output.put_line('Deleting '||v_seg_cnt||' segment');
 END IF; 
 
 SELECT COUNT(*)
 INTO				v_seg_fpass_cnt
 FROM				structools.nss_segment_fpass
 WHERE				segment_type_id = i_segment_type
 AND 				start_dt = i_start_dt
 AND				end_dt = i_end_dt;
 IF v_seg_fpass_cnt = 0 THEN
 dbms_output.put_line('Segment FPASS version '||TO_CHAR(i_start_dt, 'dd-mm-yy hh24:mi:ss')||' '||
 TO_CHAR(i_end_dt, 'dd-mm-yy hh24:mi:ss')||' for segment type '||i_segment_type||' does not exist');
 --RAISE v_prog_excp;
		ELSE 
 	dbms_output.put_line('Deleting '||v_seg_fpass_cnt||' FPASS segment');
 END IF; 
 
 /* Delete nss_segment (and nss_segment_asset and nss_segment_attribute due to the cascade ri rule). */
 IF v_seg_cnt > 0 THEN
 DELETE FROM structools.nss_segment
 WHERE			segment_type_id = i_segment_type
 AND 			start_dt = i_start_dt
 AND			end_dt = i_end_dt; 
 END IF;
 
 /* There is to ri on the _FPASS tables, so have to delete them individually, first establishing the segment_id range. */
 IF v_seg_fpass_cnt > 0 THEN
 SELECT	MIN(segment_id)
 ,MAX(segment_id)
 INTO 	v_seg_id_min 
 ,v_seg_id_max
 FROM structools.nss_segment_fpass
 WHERE segment_type_id = i_segment_type
 AND start_dt = i_start_dt
 AND end_dt = i_end_dt;	 
 
 DELETE FROM structools.nss_segment_fpass
 WHERE segment_id BETWEEN v_seg_id_min AND v_seg_id_max; 	
 
 DELETE FROM structools.nss_segment_asset_fpass
 WHERE segment_id BETWEEN v_seg_id_min AND v_seg_id_max; 	
 
 DELETE FROM structools.nss_segment_attribute_fpass
 WHERE segment_id BETWEEN v_seg_id_min AND v_seg_id_max; 
 END IF; 	

		COMMIT;
 
 EXCEPTION
 WHEN OTHERS THEN
 dbms_output.put_line('Exception '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
 dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
 RAISE;

	END delete_segment_version;
 
END STRUCTOOLSAPP.network_segments;
/

