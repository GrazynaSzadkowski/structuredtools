CREATE OR REPLACE PACKAGE BODY structoolsapp.housekeeping
AS

   /*****************************************************************************************************************************************/
   /* H6 Changes:                                                                                                                                                                                                                                                           */
   /*		02/01/18 Grazyna Szadkowski	No longer maintain table ST_BAY_DEFECT_ACTION.                                                                                     																					*/	 
   /*		29/11/17 Grazyna Szadkowski	Included defect_crtd_date in st_bay_defect table.                                                                                         																					*/	 
   /*		28/11/17 Grazyna Szadkowski	Included mtzn_reinf tables in the deletion procs.                                                                                         																					*/	 
   /*		27/11/17 Grazyna Szadkowski	Modified st_create_equipment to obtain the LR value from pit_pole_infopack table.                                         																					*/	 
   /*		22/11/17 Grazyna Szadkowski	Modified to cater for suppress_pickids indicator in st_run_request table.                                                        																					*/	 
   /*		08/11/17 Grazyna Szadkowski	Modified create_st_equipment to populate new column CCA_TREATED (fior PWODs).                                     																					*/	 
   /*		07/11/17 Grazyna Szadkowski	Added 3 new defect_status_desc when selecting defects for st_eqp_defect, as per email from 07/11/17.        																					*/	 
   /*		30/10/17 Grazyna Szadkowski	Populate new column in st_mtzn - pub_safety_zone_cls.                                                   																														*/	 
   /*    24/08/17 Grazyna Szadkowski   Populate new column in st_equipment - part_defective, make part_defect_hier obsolete.                                                                                             */
   /*    14/08/17 Grazyna Szadkowski   Created by extracting various housekeeping procedures from pkb structured_tools.pkb                                                                                                 */
   /*																	Modified create_st_equipment to extract reinforcing indicators based on a mapping table.                                                                                              */
   /*																	Modified create_st_eqp_defect to not exclude alpha sev codes and to access structoolsw.defect_st_mv (including closed UPAL defects).      							  */
   /*																	Corrected the column order in insert stmt in create_st_equipment procedure.                                                                          													 */
   /*																	Modified create_st_bay_defect to access structoolsw.defect_st_mv instead of structoolswdefect_mv.                                                          							  */
   /*																	Modified register_run_request to accept and validate parameter upgrade_poles_under_cond.                                                                   	    					   */
   /*																	Modified create_st_equipment to extract PMET and PCON poles in addition to current types. Modified to populate urbn_inspn_classfn, and reinf_cde_dw,     */ 
   /*																	last_full_insp_dt and fisc_prd_nam columns.                                                                                                                                                               */
   /*																	Modified create_st_equipment to populate column LABOUR_HRS for poles.                                                                                                                    */ 
   /******************************************************************************************************************************************/
   
        
   v_this_year                   PLS_INTEGER := EXTRACT (YEAR FROM SYSDATE);
   v_year_0                      PLS_INTEGER := EXTRACT (YEAR FROM SYSDATE);
   v_null_end_dt               CONSTANT DATE := TO_DATE('01-01-3000','dd-mm-yyyy');
   v_prog_excp                 EXCEPTION;
   
   v_request_not_valid      EXCEPTION;

	/* Cursors */
   CURSOR last_insp_csr IS
      SELECT /*+ ORDERED */  trim (eqp.equip_no) AS equip_no
      		 ,eqp.plnt_no	
             ,max(ID.inspn_dt) AS last_full_insp_dt
             ,max(dt.fisc_prd_nam) AS last_full_insp_fy
        FROM  structoolsw.equipment_dim 				 				EQP			INNER JOIN 
                  structoolsw.equipment_nvgtn_dim 					 NAV 
                  ON NAV.equip_nvgtn_sk = EQP.equip_nvgtn_sk		
                                                                              INNER JOIN 
                  structoolsw.inspection_result_fact 					  IRF 
                  ON IRF.equip_sk = NAV.equip_sk					INNER JOIN 
                  structoolsw.inspection_dim 									ID 
                  ON  ID.inspn_sk=IRF.inspn_sk							INNER JOIN 
                  structoolsw.inspection_procedure_dim 		   IPD 
                  ON  IPD.inspn_proc_sk=IRF.inspn_proc_sk	INNER JOIN 
                  structoolsw.inspection_attribute_dim 			  IAD 		
                  ON  IRF.inspn_attr_sk=IAD.inspn_attr_sk 		INNER JOIN 
                  structoolsw.inspection_result_dim 					IRD 
                  ON  IRD.inspn_rslt_sk=IRF.inspn_rslt_sk AND IRD.yr_no=IRF.yr_no
             																						  INNER JOIN 
                  structoolsw.date_dim 											  DT 
                  ON  DT.dt_sk = IRF.inspn_dt_sk
      WHERE eqp.equip_grp_id IN ('PAUS', 'PWOD','PCON','PMET')
           AND eqp.equip_stat   = 'AC'
           AND eqp.asst_seg_cde='3000'
           AND iad.inspn_attr_id  =   'PO-IN-001   '
           AND upper (ird.inspn_resp_desc) = 'YES' 
           AND upper(ipd.inspn_proc_desc) IN ('WOOD POLE INSPECTION', 'WOOD POLE FULL INSPECTION','AUS POLE INSPECTION', 'AUSTPOLE FULL INSPECTION'
                                                               ,'CONCRETE POLE INSPECTION', 'CONCRETE POLE FULL INSPECTION',  'METAL POLE FULL INSPECTION')
           AND ID.INSPN_TSK_CURR_FLG = 'Y'
           AND eqp.equip_no = ID.equip_no    
      --      and eqp.plnt_no = 'S14036'
      GROUP BY trim (eqp.equip_no), eqp.plnt_no
   ;
         PROCEDURE create_st_tables(i_table_name         IN VARCHAR2
                             ,i_segment_type       IN PLS_INTEGER) IS

      v_sql       VARCHAR2(1000);
      
      PROCEDURE create_st_bay_defect IS
      
      BEGIN
         dbms_output.put_line('create_st_bay_defect start. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      
      
         structools.schema_utils.truncate_table('ST_BAY_DEFECT');
         structools.schema_utils.disable_table_indexes('ST_BAY_DEFECT');

         INSERT /*+ APPEND */ INTO structools.st_bay_defect
         (
         EQUIP_NO
         ,PICK_ID
         ,EQUIP_GRP_ID
         ,DEFECT_NO
         ,DEFECT_DESC
         ,PART_CDE
         ,DEFECT_CDE
         ,DEFECT_PART_DESC
         ,SEV_CDE
         ,STD_JOB_NO
         ,STD_JOB_DESC
         ,DEFECT_ACTION								-- this was only used by bay stratgey 57, which is no longer in use. Therefore this column will no longer be maintained.
         ,DEFECT_CRTD_DATE
         )
--         WITH equip_defect_bay AS
--         (
         SELECT DISTINCT A.equip_no
               ,A.pick_id
               ,TRIM(b.equip_grp_id) AS equip_grp_id
               ,defect_no
               ,defect_desc
               ,TRIM(SUBSTR(defect_desc, 1, INSTR(defect_desc,'-',1,1) - 1)) AS part_cde
               ,TRIM(SUBSTR(defect_desc, INSTR(defect_desc,'-',1,1) + 1, INSTR(defect_desc,' ',1,1) - INSTR(defect_desc,'-',1,1))) AS defect_cde
               ,defect_part_desc
               ,sev_cde
               ,std_job_no
               ,std_job_desc
               ,NULL						AS defect_action  -- no longer needed or maintained
               ,A.defect_crtd_date
         FROM structoolsw.defect_st_mv  A  INNER JOIN
              structoolsw.EQUIPMENT_DIM b
         ON A.equip_no = b.equip_no      
         WHERE TRIM(b.equip_grp_id) IN ('BBUNBHVL','BBUNBHVS','BBUNBLVL','BNBUBHVL','BNBUBHVS','BNBUBLVL'
                           ,'BOPEBHVL','BOPEBHVS','BOPEBLVL','BUNDBHVL','BUNDBHVS','BUNDBLVL','UNKNOWN')
         AND UPPER(TRIM(defect_stat_desc)) IN ('OPEN', 'SELECTED FOR WORK') 
         ;
         
--         )
--         SELECT DISTINCT 
--         		A.EQUIP_NO
--               ,A.PICK_ID
--               ,A.EQUIP_GRP_ID
--               ,A.DEFECT_NO
--               ,A.DEFECT_DESC
--               ,A.PART_CDE
--               ,A.DEFECT_CDE
--               ,A.DEFECT_PART_DESC
--               ,A.SEV_CDE
--               ,A.STD_JOB_NO
--               ,A.STD_JOB_DESC
--               ,b.defect_action
--               ,A.defect_crtd_date  
--         FROM  equip_defect_bay      			A LEFT OUTER JOIN ---INNER JOIN
--               structools.st_bay_defect_action b
--         ON    A.equip_grp_id = b.equip_grp_id
--         AND   A.part_cde = b.part_cde
--         AND   A.defect_cde = b.defect_cde                   
--         ;
         
         structools.schema_utils.enable_table_indexes('ST_BAY_DEFECT');
         structools.schema_utils.anal_table('ST_BAY_DEFECT');
         
         COMMIT;

         dbms_output.put_line('create_st_bay_defect end. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      
         
      END create_st_bay_defect;
      
      PROCEDURE create_st_bay_equipment(i_segment_type IN PLS_INTEGER) IS
      
      BEGIN
      
         dbms_output.put_line('create_st_bay_equipment start. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

         structools.schema_utils.truncate_table('ST_BAY_EQUIPMENT');
         structools.schema_utils.disable_table_indexes('ST_BAY_EQUIPMENT');

         INSERT /*+ APPEND */ INTO structools.st_bay_equipment
         WITH conductors AS
         (
         SELECT pick_id       --eg C1234
               ,installation_date
         FROM structoolsw.CONDUCTOR_HV
         UNION ALL
         SELECT pick_id       --eg C1234
               ,installation_date
         FROM structoolsw.CONDUCTOR_LV
         )
         ,
         segments AS
         (
         SELECT A.segment_id
               ,A.segment_identifier_id
               ,b.pick_id
         FROM  structools.nss_segment        A  INNER JOIN
               structools.nss_Segment_asset  b
         ON A.segment_id = b.segment_id
         WHERE A.segment_type_id = i_segment_type
         AND A.end_dt = v_null_end_dt
         )               
         ,
         bays AS
         (
         SELECT TRIM(A.plnt_no) AS pick_id
               ,A.equip_cde   AS bay_equip_cde
               ,(CASE 
                  WHEN structure_1_pick_id < structure_2_pick_id THEN  structure_1_pick_id
                  ELSE structure_2_pick_id
                 END)               AS stc1
               ,(CASE 
                  WHEN structure_1_pick_id < structure_2_pick_id THEN  structure_2_pick_id
                  ELSE structure_1_pick_id
                 END)               AS stc2  
               ,swsect_net_typ
               ,swsect_nsw_nid       AS ss_id
               ,CASE
                  WHEN swsect_upstrm_prdev_id = 0 AND swsect_net_typ = 'HV' THEN NULL
                  WHEN swsect_upstrm_prdev_id = 0 AND swsect_net_typ = 'LV' THEN swsect_net_nid
                  ELSE swsect_upstrm_prdev_id
               END                                    AS prot_zone_id
               ,CASE
                  WHEN swsect_upstrm_prdev_id = 0 AND swsect_net_typ = 'HV' THEN NULL
                  WHEN swsect_upstrm_prdev_id = 0 AND swsect_net_typ = 'LV' THEN 'DSTR'
                  ELSE swsect_upstrm_prdev_typ
               END                                    AS prot_zone_typ
               ,swsect_net_nid      AS ntwk_id
               ,b.maint_zone_nam    AS mtzn
               ,EXTRACT (YEAR FROM d.installation_date) AS instln_year
               ,CASE A.swsect_hv_fdr_id_no   
                  WHEN -8888           THEN NULL
                  ELSE                      A.swsect_hv_fdr_id_no
               END                                 AS feeder_id 
               ,A.rgn_nam                 AS region_nam
               ,A.conductor_pick_id
               ,CASE A.swsect_hv_fdr_nam   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      A.swsect_hv_fdr_nam
               END                                 AS feeder_nam
               ,CASE A.swsect_zone_substn_desc   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      A.swsect_zone_substn_desc
               END                                 AS zone_subst_nam
               ,MAX(E.segment_identifier_id) OVER (PARTITION BY TRIM(A.plnt_no)) AS segment_id
               ,ROUND(A.bay_len_m,2)               AS bay_len_m
               ,CASE
               	WHEN b.fire_risk_zone_cls IN ('X','H','M','L')				THEN	b.fire_risk_zone_cls
                  ELSE																										NULL
               END															AS fire_risk_zone_cls
               ,A.max_safety_zone_rtg
               ,A.nearst_dist_from_coast_km
               ,d.installation_date
         FROM   structoolsw.TOPO_GEOG_MV         A  INNER JOIN
                structoolsw.EQUIPMENT_DIM        b
         ON A.equip_no = b.equip_no   
                                                   INNER JOIN
                conductors                   d
         ON 'C'||A.conductor_pick_id = d.pick_id                                                 
                                                   LEFT OUTER JOIN
                segments                     E                                                   
         ON TRIM(A.plnt_no) = E.pick_id                                                 
         WHERE A.equip_cde IN ('HVCO','HVSP','LVCO')
         AND b.equip_cls = 'W2'
         ORDER BY prot_zone_id, mtzn, stc1, stc2 --swsect_net_typ, ss_id, swsect_upstrm_prdev_typ 
         )
         SELECT DISTINCT bays.pick_id
               ,bays.bay_equip_cde
               ,CASE                                                       --to handle bad data in asset_topology (stc pickid not starting with 'S')
                  WHEN SUBSTR(bays.stc1,1,1) = 'S'    THEN  bays.stc1
                  ELSE                                      'S'||bays.stc1
               END                        AS stc1 
               ,CASE
                  WHEN SUBSTR(bays.stc2,1,1) = 'S'    THEN  bays.stc2
                  ELSE                                      'S'||bays.stc2
               END                        AS stc1 
--               ,bays.stc1
--               ,bays.stc2
               ,bays.swsect_net_typ
               ,bays.ss_id
               ,bays.prot_zone_id
               ,bays.prot_zone_typ
               ,bays.ntwk_id
               ,bays.mtzn
               ,bays.instln_year
               ,bays.feeder_id
               ,bays.region_nam
               ,bays.conductor_pick_id
               ,bays.feeder_nam
               ,bays.zone_subst_nam
               ,CASE swsect_net_typ
                  WHEN 'LV'   THEN 'N'||ntwk_id
                  ELSE             segment_id
               END                  AS segment_id
               ,COUNT(*) OVER (PARTITION BY bays.prot_zone_id, mtzn ORDER BY mtzn ) AS mtzn_cnt
               ,bay_len_m
               ,fire_risk_zone_cls
               ,max_safety_zone_rtg
               ,(SELECT MAX(CASE WHEN NVL(TRIM(cond.condition_code), ' ') = ' ' THEN NULL
                      WHEN NVL(TRIM(TRANSLATE(cond.condition_code,'0123456789',' ')), ' ') = ' ' THEN TO_NUMBER(cond.condition_code)
                      WHEN cond.condition_code = '100 (near-NEW)' THEN 100
                      WHEN cond.condition_code = '0 (fault)' THEN 0
                      ELSE NULL END )
                      FROM structoolsw.CONDUCTOR_FIELD_COND_UPLOAD cond 
                      WHERE  COND.BAY_PICK_ID =  bays.pick_id
                      ) AS condition_code
               ,nearst_dist_from_coast_km    AS dist_coast_km
               ,installation_date
         FROM bays                                      
         -- LEFT OUTER JOIN
          --    structoolsw.CONDUCTOR_FIELD_COND_UPLOAD cond 
        -- ON   bays.pick_id = COND.BAY_PICK_ID
         ;
         structools.schema_utils.enable_table_indexes('ST_BAY_EQUIPMENT');
         structools.schema_utils.anal_table('ST_BAY_EQUIPMENT');
         
         COMMIT;
         
         dbms_output.put_line('create_st_bay_equipment start. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

      END create_st_bay_equipment;
      
      
--      PROCEDURE create_st_eqp_type_def_action IS
--
--         v_egi                   VARCHAR2(300);
--         v_part                  VARCHAR2(300);
--         v_defect_description    VARCHAR2(300);
--         v_defect_descr          VARCHAR2(300);
--      
--      BEGIN
--         structools.schema_utils.truncate_table('ST_EQP_TYPE_DEFECT_ACTION');
--         structools.schema_utils.disable_table_indexes('ST_EQP_TYPE_DEFECT_ACTION');
--
--         BEGIN
--            WITH repl_defect_descr AS
--            (
--               SELECT TRIM(egi)      AS egi
--                     ,TRIM(part_id) AS part
--                     ,defect_description
--                     ,REPLACE(REPLACE(REPLACE(defect_description, ' ','-'),'---','-'), '--', '-') AS defect_descr
--               FROM structoolsw.eqp_egi_part_defect_std_job_mv
--              WHERE TRIM(egi) IN ('CAPB', 'DOF', 'DSTR','LBS','PAUS','PTSD','PWOD','REAC','RECL','RGTR','SECT')
--            )
--            SELECT egi
--                  ,part
--                  ,defect_description 
--                  ,defect_descr
--            INTO   v_egi
--                  ,v_part
--                  ,v_defect_description 
--                  ,v_defect_descr
--            FROM repl_defect_descr
--            WHERE INSTR(defect_descr, '-',1,2) = 0;
--            dbms_output.put_line('Defect_std_job translation failed, v_egi = '||v_egi||' v_part = '||v_part
--                                    ||' defect_description = '||v_defect_description||' defect_descr = '||v_defect_descr);
--            RAISE v_prog_excp;
--         EXCEPTION
--            WHEN NO_DATA_FOUND THEN NULL;
--         END;
--
--         INSERT /*+ APPEND */ INTO structools.st_eqp_type_defect_action
--            WITH repl_defect_descr AS
--            (
--               SELECT TRIM(egi)      AS egi
--                     ,TRIM(part_id) AS part_cde1
--                     ,REPLACE(REPLACE(REPLACE(defect_description, ' ','-'),'---','-'), '--', '-') AS defect_descr
--                     ,NVL(default_sj_description, ' ')                AS std_job   
--               FROM structoolsw.eqp_egi_part_defect_std_job_mv
--               WHERE TRIM(egi) IN ('CAPB', 'DOF', 'DSTR','LBS','PAUS','PTSD','PWOD','REAC','RECL','RGTR','SECT')
--            )
--            ,
--            delimiters AS
--            (
--            SELECT    egi
--                     ,NVL(part_cde1,' ') AS part_cde1
--                     ,defect_descr
--                     ,INSTR(defect_descr,'-',1,1)      AS first_dash
--                     ,INSTR(defect_descr,'-',1,2)      AS second_dash
--                     ,std_job
--            FROM repl_defect_descr
--            )
--            ,
--            temp AS
--            (
--            SELECT    egi
--                     ,part_cde1
--                     ,SUBSTR(defect_descr, 1, first_dash - 1)  AS part_cde2
--                     ,GREATEST(part_cde1, SUBSTR(defect_descr, 1, first_dash - 1)) AS part_cde
--                     ,SUBSTR(defect_descr, first_dash + 1, second_dash - first_dash - 1)  AS defect_cde
--                     ,std_job
--                     ,NULL       AS action --action; for subsequent manual update
--            FROM delimiters
--            )
--            SELECT egi
--                  ,part_cde
--                  ,defect_cde
--                  ,MAX(std_job) AS std_job
--                  ,action
--            FROM temp
--            GROUP BY egi
--                  ,part_cde
--                  ,defect_cde
--                  ,action
--            ORDER BY egi, part_cde, defect_cde
--            ;
--         
--         structools.schema_utils.enable_table_indexes('ST_EQP_TYPE_DEFECT_ACTION');
--
--         COMMIT;
--         
--      EXCEPTION
--         WHEN v_prog_excp THEN
--            RAISE;
--         WHEN OTHERS THEN
--            dbms_output.put_line('Exception '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
--            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
--            RAISE;
--
--      END create_st_eqp_type_def_action;

--      PROCEDURE create_st_bay_defect_action IS		--defunct now
--
--         v_egi                   VARCHAR2(300);
--         v_part                  VARCHAR2(300);
--         v_defect_description    VARCHAR2(300);
--         v_defect_descr          VARCHAR2(300);
--      
--      BEGIN
--         dbms_output.put_line('create_st_bay_defect_actiin start. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      
--
--         structools.schema_utils.truncate_table('ST_BAY_DEFECT_ACTION');
--         structools.schema_utils.disable_table_indexes('ST_BAY_DEFECT_ACTION');
--
--         BEGIN
--            WITH repl_defect_descr AS
--            (
--               SELECT TRIM(egi)      AS egi
--                     ,TRIM(part_id) AS part
--                     ,defect_description
--                     ,REPLACE(REPLACE(REPLACE(defect_description, ' ','-'),'---','-'), '--', '-') AS defect_descr
--               FROM structoolsw.eqp_egi_part_defect_std_job_mv
--              WHERE TRIM(egi) LIKE 'B%'
--            )
--            SELECT egi
--                  ,part
--                  ,defect_description 
--                  ,defect_descr
--            INTO   v_egi
--                  ,v_part
--                  ,v_defect_description 
--                  ,v_defect_descr
--            FROM repl_defect_descr
--            WHERE INSTR(defect_descr, '-',1,2) = 0;
--            dbms_output.put_line('Defect_std_job translation failed, v_egi = '||v_egi||' v_part = '||v_part
--                                    ||' defect_description = '||v_defect_description||' defect_descr = '||v_defect_descr);
--            RAISE v_prog_excp;
--         EXCEPTION
--            WHEN NO_DATA_FOUND THEN NULL;
--         END;
--
--         INSERT /*+ APPEND */ INTO structools.st_bay_defect_action
--            WITH repl_defect_descr AS
--            (
--               SELECT TRIM(egi)      AS egi
--                     ,TRIM(part_id) AS part
--                     ,defect_description
--                     ,REPLACE(REPLACE(REPLACE(defect_description, ' ','-'),'---','-'), '--', '-') AS defect_descr
--                     ,default_sj_description                AS std_job        
--               FROM structoolsw.eqp_egi_part_defect_std_job_mv
--               WHERE TRIM(egi) LIKE 'B%'
--            )
--            ,
--            delimiters AS
--            (
--            SELECT    egi
--                     ,part
--                     ,defect_description
--                     ,defect_descr
--                     ,INSTR(defect_descr,'-',1,1)      AS first_dash
--                     ,INSTR(defect_descr,'-',1,2)      AS second_dash
--                     ,std_job
--            FROM repl_defect_descr
--            )
--            SELECT    egi
--                     ,SUBSTR(defect_descr, 1, first_dash - 1)  AS part_cde
--                     ,SUBSTR(defect_descr, first_dash + 1, second_dash - first_dash - 1)  AS defect_cde
--                     ,std_job
--                     ,NULL       --bay_defect_action; for subsequent manual update.  This was only used by bay stratgey 57, which is no longer in use. Therefore this column will no longer be maintained,
--                     					--, ie the manual update is no longer required
--            FROM delimiters
--            ORDER BY egi, part_cde
--            ;
--         
--         structools.schema_utils.enable_table_indexes('ST_BAY_DEFECT_ACTION');
--
--         COMMIT;
--
--         dbms_output.put_line('create_st_bay_defect_action end. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      
--         
--      EXCEPTION
--         WHEN v_prog_excp THEN
--            RAISE;
--         WHEN OTHERS THEN
--            dbms_output.put_line('Exception '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
--            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
--            RAISE;
--
--      END create_st_bay_defect_action;
      
      PROCEDURE create_st_eqp_defect IS
      
      BEGIN
      
         dbms_output.put_line('create_st_eqp_defect start. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

         structools.schema_utils.truncate_table('ST_EQP_DEFECT');
         structools.schema_utils.disable_table_indexes('ST_EQP_DEFECT');

         INSERT /*+ APPEND */ INTO structools.st_eqp_defect
         (equip_no
         ,pick_id
         ,equip_grp_id
         ,defect_no
         ,defect_desc
         ,part_cde
         ,defect_cde
         ,defect_part_desc
         ,sev_cde
         ,std_job_no
         ,std_job_desc
         ,defect_action
         )
         WITH equip_defect_tmp AS
         (
         SELECT equip_no
               ,pick_id
               ,CASE UPPER(TRIM(equip_grp_id_desc))
                  WHEN 'POLE WOOD'           THEN 'PWOD'  
                  WHEN 'AUSTPOLE REINFORCED WOOD POLE'   THEN 'PAUS'
                  WHEN 'LOAD BREAK SWITCH'   THEN 'LBS'               
                  WHEN 'RECLOSER'            THEN 'RECL'
                  WHEN 'SECTIONALISER'       THEN 'SECT'
                  WHEN 'DROP OUT FUSE'       THEN 'DOF'
                  WHEN 'CAPACITOR BANK'      THEN 'CAPB'
                  WHEN 'DN REACTORS'         THEN 'REAC'
                  WHEN 'DISTRIBUTION TRANSFRORMER'  THEN 'DSTR'
                  WHEN 'POLE TOP SWITCH'     THEN 'PTSD'
                  WHEN 'DN REGULATING TRANSFORMERS'   THEN 'RGTR'
               END                           equip_grp_id
               ,defect_no
               ,defect_desc
               ,INSTR(defect_desc,'-',1,1)      AS dash_pos
--               ,INSTR(defect_desc,' ',INSTR(defect_desc,'-',1,1),1)      AS space_pos
--               ,TRIM(SUBSTR(defect_desc, 1, INSTR(defect_desc,'-',1,1) - 1)) AS part_cde
--               --,TRIM(SUBSTR(defect_desc, INSTR(defect_desc,'-',1,1) + 1, INSTR(defect_desc,' ',1,1) - INSTR(defect_desc,'-',1,1))) AS defect_cde
--               ,TRIM(SUBSTR(defect_desc, INSTR(defect_desc,'-',1,1) + 1, 4)) AS defect_cde
               ,defect_part_desc
               ,sev_cde
               ,MIN(sev_cde) OVER (PARTITION BY pick_id, TRIM(SUBSTR(defect_desc, 1, INSTR(defect_desc,'-',1,1) - 1)), TRIM(SUBSTR(defect_desc, INSTR(defect_desc,'-',1,1) + 1, INSTR(defect_desc,' ',1,1) - INSTR(defect_desc,'-',1,1)))) AS min_sev_cde
               ,std_job_no
               ,std_job_desc
         FROM structoolsw.defect_st_mv
         WHERE UPPER(TRIM(equip_grp_id_desc)) IN ('POLE WOOD','AUSTPOLE REINFORCED WOOD POLE','LOAD BREAK SWITCH','RECLOSER','SECTIONALISER','DROP OUT FUSE'
                                 ,'CAPACITOR BANK','DISTRIBUTION TRANSFRORMER','POLE TOP SWITCH','DN REGULATING TRANSFORMERS','DN REACTORS')
         AND (UPPER(TRIM(defect_stat_desc)) IN ('OPEN','SELECTED FOR WORK','WORK ORDER ASSIGNED TO DEFECT','WORK SCHEDULED','COST ASSIGNED') OR defect_desc LIKE 'WPOLE-UPAL%')
         --AND sev_cde IN('1','2','3','4','5')
         )
         ,
         equip_defect AS
         (
         SELECT equip_no
               ,pick_id
               ,equip_grp_id
               ,defect_no
               ,defect_desc
               ,dash_pos
--               ,space_pos
               ,TRIM(SUBSTR(defect_desc, 1, dash_pos - 1)) AS part_cde
               ,TRIM(SUBSTR(defect_desc, dash_pos + 1, 4)) AS defect_cde
               ,defect_part_desc
               ,sev_cde
               ,min_sev_cde
               ,std_job_no
               ,std_job_desc
         FROM equip_defect_tmp
         )         
         SELECT DISTINCT A.equip_no
               ,A.pick_id
               ,A.equip_grp_id
               ,A.defect_no
               ,A.defect_desc
               ,A.part_cde
               ,A.defect_cde
               ,A.defect_part_desc
               ,CASE
                  WHEN A.part_cde = 'AGOST' AND A.defect_cde = 'SIMS'                     THEN  '2'
                  WHEN A.part_cde = 'AGOST' AND A.defect_cde = 'SWNC'                     THEN  '2'
                  WHEN A.part_cde = 'AGOST' AND A.defect_cde = 'UNSE'                     THEN  '2'
                  WHEN A.part_cde = 'ASTAY' AND A.defect_cde = 'SIMS'                     THEN  '2'
                  WHEN A.part_cde = 'ASTAY' AND A.defect_cde = 'SWNC'                     THEN  '2'
                  WHEN A.part_cde = 'ASTAY' AND A.defect_cde = 'UNSE'                     THEN  '2'
                  WHEN A.part_cde = 'GSTAY' AND A.defect_cde = 'SIMS'                     THEN  '2'
                  WHEN A.part_cde = 'GSTAY' AND A.defect_cde = 'SWNC'                     THEN  '2'
                  WHEN A.part_cde = 'GSTAY' AND A.defect_cde = 'UNSE'                     THEN  '2'
                  WHEN A.part_cde = 'OSTAY' AND A.defect_cde = 'SIMS'                     THEN  '2'
                  WHEN A.part_cde = 'OSTAY' AND A.defect_cde = 'SWNC'                     THEN  '2'
                  WHEN A.part_cde = 'OSTAY' AND A.defect_cde = 'UNSE'                     THEN  '2'
                  WHEN A.part_cde = 'HVXM'  AND A.defect_cde = 'BURN' AND A.sev_cde = '1' THEN  '2'
                  WHEN A.part_cde = 'HVXM'  AND A.defect_cde = 'UNSE'                     THEN  '2'
                  WHEN A.part_cde = 'HVXM'  AND A.defect_cde = 'SXAM'                     THEN  '5'
                  WHEN A.part_cde = 'HVXM'  AND A.defect_cde = 'WANT'                     THEN  '2'
                  WHEN A.part_cde = 'LVXM'  AND A.defect_cde = 'BURN' AND A.sev_cde = '1' THEN  '2'
                  WHEN A.part_cde = 'LVXM'  AND A.defect_cde = 'UNSE'                     THEN  '2'
                  WHEN A.part_cde = 'LVXM'  AND A.defect_cde = 'SXAM'                     THEN  '5'
                  WHEN A.part_cde = 'LVXM'  AND A.defect_cde = 'WANT'                     THEN  '2'
                  ELSE                                                                          A.sev_cde
               END                  AS sev_cde
               ,A.std_job_no
               ,A.std_job_desc
               ,NULL             --b.defect_action -- no longer needed; no longer maintained 
         FROM  equip_defect                        A  --INNER JOIN
--               structools.st_eqp_type_defect_action   b
--         ON    TRIM(A.equip_grp_id) = TRIM(b.equip_grp_id)
--         AND   TRIM(A.part_cde) = TRIM(b.part_cde)
--         AND   TRIM(A.defect_cde) = TRIM(b.defect_cde)
         WHERE sev_cde = min_sev_cde      
         ;
         
         structools.schema_utils.enable_table_indexes('ST_EQP_DEFECT');
         structools.schema_utils.anal_table('ST_EQP_DEFECT');
         
         COMMIT;
         
         dbms_output.put_line('create_st_eqp_defect end. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

      END create_st_eqp_defect;
      
      PROCEDURE create_st_eqp_mtzn IS
      
      BEGIN
      
         dbms_output.put_line('create_st_eqp_mtzn start. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

         structools.schema_utils.truncate_table('ST_EQP_MTZN');
         structools.schema_utils.disable_table_indexes('ST_EQP_MTZN');

         INSERT /*+ APPEND */ INTO structools.st_eqp_mtzn
         SELECT pick_id
               ,maint_zone_nam AS mtzn
         FROM structoolsw.equipment_dim
         WHERE PICK_ID LIKE 'N%'
         AND equip_stat = 'AC'
         AND actv_flg = 'Y'
         AND EQUIP_GRP_ID  NOT IN ('LAMP','UNKNOWN','UMPS');
         
         structools.schema_utils.enable_table_indexes('ST_EQP_MTZN');
         structools.schema_utils.anal_table('ST_EQP_MTZN');
         
         COMMIT;
         
         dbms_output.put_line('create_st_eqp_mtzn end;. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

      END create_st_eqp_mtzn;
      
      PROCEDURE create_st_equipment IS
      
      BEGIN

         dbms_output.put_line('create_st_equipment start. '||TO_CHAR(SYSDATE,'hh24:mi:ss')); 
              
         structools.schema_utils.truncate_table('st_equipment');
         structools.schema_utils.disable_table_indexes('st_equipment');

         INSERT /*+ APPEND */ INTO structools.st_equipment
         (
         PICK_ID
			,EQUIP_GRP_ID
         ,PRNT_PLNT_NO
         ,REGION_NAM
         ,MAINT_ZONE_NAM
         ,REINF_CDE
         ,INSTLN_YEAR
         ,WOOD_TYPE
         ,RECL_PHASES
         ,FIRE_RISK_ZONE_CLS
         ,POLE_DIAMETER_MM
         ,POLE_COMPLEXITY
         ,DSTR_SIZE
         ,PROT_ZONE_ID
         ,PROT_ZONE_TYP
         ,FEEDER_ID
         ,SS_ID
         ,FEEDER_NAM
         ,ZONE_SUBST_NAM
         ,MANUF
         ,STOCK_CDE
         ,CTL_TYP
         ,DEV_TYP
         ,MANUF_AND_MDL
         ,EXPULSION_TYPE
         ,NTWK_ID
         ,MAX_SAFETY_ZONE_RTG
         ,PART_CDE
         ,PART_DEFECT_HIER
         ,EQUIP_NO
         ,GOOD_WOOD_READING_100_AGL_MM
         ,GOOD_WOOD_READING_100_BGL_MM
         ,GOOD_WOOD_READING_1M_AGL
         ,SI_AT_1M_AGL
         ,SI_AT_GL
         ,CUST_DS_CNT
         ,urbn_inspn_classfn
         ,reinf_cde_dw
         ,labour_hrs
         ,part_defective
         ,cca_treated
			)
         WITH eqp_dim AS
         (
         SELECT *
         FROM structoolsw.equipment_dim
         WHERE equip_stat = 'AC'
         AND actv_flg = 'Y'
         AND pick_id != ' '
         AND equip_grp_id IN ('PWOD','PAUS','PCON','PMET','CAPB','DOF','DSTR','LBS','PTSD','REAC','RECL','RGTR','SECT')         
         )            
         ,
         INSP_RSLT AS
         (
         SELECT aa.equip_no
               ,CASE aa.pole_diam
                  WHEN 999999 THEN NULL
                  ELSE  aa.pole_diam 
               END                    AS pole_diam
               ,CASE 
                  WHEN aa.good_wood_reading_1000mm_agl = 'Y' AND aa.good_wood_reading_1m_agl != 999999   THEN  aa.good_wood_reading_1m_agl
                  ELSE                                                                                         NULL
               END                    AS good_wood_reading_1m_agl 
               ,CASE
                  WHEN internal_inspection_required = 'Y' AND aa.good_wood_reading_100_agl_mm != 999999  THEN aa.good_wood_reading_100_agl_mm
                  ELSE                                                                                         NULL
               END                    AS good_wood_reading_100_agl_mm 
               ,CASE
                  WHEN internal_inspection_required = 'Y' AND aa.good_wood_reading_100_bgl_mm != 999999  THEN  aa.good_wood_reading_100_bgl_mm
                  ELSE                                                                                         NULL
               END                    AS good_wood_reading_100_bgl_mm 
               ,CASE
                  WHEN internal_inspection_required = 'Y' AND aa.si_at_gl != 999999                      THEN  aa.si_at_gl
                  ELSE                                                                                         NULL 
               END                    AS si_at_gl 
               ,CASE
                  WHEN internal_inspection_required = 'Y' AND aa.si_at_1m_agl != 999999                  THEN  aa.si_at_1m_agl
                  ELSE                                                                                         NULL 
               END                    AS si_at_1m_agl 
         FROM 
         (
         SELECT b.equip_no
               ,LEAST(     --decoding dots to handle weird data in inspection_result_current_dim
                TO_NUMBER(NVL(TRIM(DECODE(pole_diameter_1m_agl_mm,'.',NULL, ' ', NULL, pole_diameter_1m_agl_mm)), 999999))            
               ,TO_NUMBER(NVL(TRIM(DECODE(pole_diameter_bg_line_mm,'.',NULL, ' ', NULL, pole_diameter_bg_line_mm)), 999999))           
               ,TO_NUMBER(NVL(TRIM(DECODE(pole_diameter_ground_line_mm,'.',NULL, ' ', NULL, '370-', NULL, '290-', NULL, '260.', NULL, pole_diameter_ground_line_mm)), 999999))       
               ,TO_NUMBER(NVL(TRIM(DECODE(diamtr_at_ground_line_mm,'.',NULL, ' ', NULL, diamtr_at_ground_line_mm)), 999999))  
               )                                                         pole_diam
               ,good_wood_reading_1000mm_agl
               ,internal_inspection_required
               ,TO_NUMBER(NVL(RTRIM(DECODE(good_wood_reading_1m_agl,'.',NULL,'-',NULL,'0', NULL,' ', NULL, good_wood_reading_1m_agl),' .-'), 999999))
                                                                     AS good_wood_reading_1m_agl
               ,TO_NUMBER(NVL(RTRIM (DECODE(good_wood_reading_100_agl_mm,'.',NULL,'-',NULL,'0', NULL,' ', NULL, good_wood_reading_100_agl_mm),' .-'), 999999))
                                                                     AS good_wood_reading_100_agl_mm
               ,TO_NUMBER(NVL(RTRIM(DECODE(good_wood_reading_100_bgl_mm,'.',NULL,'-',NULL,'0', NULL,' ', NULL, good_wood_reading_100_bgl_mm),' .-'), 999999))
                                                                     AS good_wood_reading_100_bgl_mm
               ,TO_NUMBER(NVL(RTRIM(DECODE(si_at_gl,'.',NULL,'-',NULL,' ', NULL, 'NAN', NULL,  'NaN', NULL, si_at_gl),' .-'), 999999))
                                                                     AS si_at_gl
               ,TO_NUMBER(NVL(RTRIM(DECODE(si_at_1m_agl,'.',NULL,'-',NULL,' ', NULL,'NAN', NULL, 'NaN', NULL, si_at_1m_agl),' .-'), 999999))
                                                                     AS si_at_1m_agl
           FROM structoolsw.inspection_result_current_dim A INNER JOIN eqp_dim b ON A.equip_no = b.equip_no
--          WHERE (pole_diameter_1m_agl_mm IS NOT NULL AND pole_diameter_1m_agl_mm != '.'
--            OR       pole_diameter_bg_line_mm IS NOT NULL AND pole_diameter_bg_line_mm != '.'
--            OR        pole_diameter_ground_line_mm IS NOT NULL AND pole_diameter_ground_line_mm != '.'
--            OR        diamtr_at_ground_line_mm IS NOT NULL AND diamtr_at_ground_line_mm != '.')
          WHERE equip_grp_id = 'PWOD'
         ) aa
         )
         ,
         xarms_and_stays_tmp1 AS         --stays and xarms. At this stage determined only by existence of selected defects
         (
         SELECT pick_id
               ,part_cde
               ,'Y' 				AS	part_defective
--               ,defect_cde
--               ,MIN(sev_cde) AS sev_cde  -- 1 means most severe or most urgent
         FROM structools.st_eqp_defect
         WHERE equip_grp_id IN ('PWOD','PAUS')
         AND (part_cde = 'ASTAY' AND defect_cde IN ('STMS','UNSE','CORR','SIMS','SWNC')
              OR part_cde = 'AGOST' AND defect_cde IN ('STMS','UNSE','CORR','SIMS','SWNC')
              OR part_cde = 'GSTAY' AND defect_cde IN ('STMS','UNSE','CORR','SIMS','SWNC')
              OR part_cde = 'OSTAY' AND defect_cde IN ('STMS','UNSE','CORR','SIMS','SWNC')
              OR part_cde = 'HVXM' AND defect_cde IN ('SPLI','UNSE','WANT','CORR','BURN','SXAM','LOOS')
              OR part_cde = 'LVXM' AND defect_cde IN ('SPLI','UNSE','WANT','CORR','BURN','SXAM','LOOS') )
--         GROUP BY pick_id, part_cde, defect_cde
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'HVXM'          AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_pwod_mv
         WHERE hv_crssarm_cnstr_typ != ' ' AND hv_crssarm_cnstr_typ != 'N/A' AND hv_crssarm_cnstr_typ != 'NO HV' AND hv_crssarm_cnstr_typ != '#NA#'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'LVXM'         AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_pwod_mv
         WHERE lv_crssarm_len != ' ' AND lv_crssarm_len != 'N/A' AND hv_crssarm_cnstr_typ != '#NA#'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'ASTAY'         AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_pwod_mv
         WHERE stay_1_typ = 'Aerial Stay' OR stay_2_typ = 'Aerial Stay'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'GSTAY'         AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_pwod_mv
         WHERE stay_1_typ = 'Ground Stay' OR stay_2_typ = 'Ground Stay'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'OSTAY'         AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_pwod_mv
         WHERE stay_1_typ = 'Outrigger Stay' OR stay_2_typ = 'Outrigger Stay'         
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'HVXM'         AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_paus_mv
         WHERE hv_crssarm_cnstr_typ != ' ' AND hv_crssarm_cnstr_typ != 'N/A' AND hv_crssarm_cnstr_typ != 'NO HV' AND hv_crssarm_cnstr_typ != '#NA#'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'LVXM'         AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_paus_mv
         WHERE lv_crssarm_len != ' ' AND lv_crssarm_len != 'N/A' AND hv_crssarm_cnstr_typ != '#NA#'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'ASTAY'         AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_paus_mv
         WHERE stay_1_typ = 'Aerial Stay' OR stay_2_typ = 'Aerial Stay'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'GSTAY'         AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_paus_mv
         WHERE stay_1_typ = 'Ground Stay' OR stay_2_typ = 'Ground Stay'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'OSTAY'         AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_paus_mv
         WHERE stay_1_typ = 'Outrigger Stay' OR stay_2_typ = 'Outrigger Stay'         
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'HVXM'        AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_pwod_mv  A  LEFT OUTER JOIN
              structools.st_bay_equipment       b
         ON A.plnt_no = b.stc1
         WHERE b.bay_equip_cde LIKE 'HV%'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'HVXM'        AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_pwod_mv  A  LEFT OUTER JOIN
              structools.st_bay_equipment       b
         ON A.plnt_no = b.stc2
         WHERE b.bay_equip_cde LIKE 'HV%'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'LVXM'        AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_pwod_mv  A  LEFT OUTER JOIN
              structools.st_bay_equipment       b
         ON A.plnt_no = b.stc1
         WHERE b.bay_equip_cde LIKE 'LV%'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'LVXM'        AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_pwod_mv  A  LEFT OUTER JOIN
              structools.st_bay_equipment       b
         ON A.plnt_no = b.stc2
         WHERE b.bay_equip_cde LIKE 'LV%'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'HVXM'        AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_paus_mv  A  LEFT OUTER JOIN
              structools.st_bay_equipment       b
         ON A.plnt_no = b.stc1
         WHERE b.bay_equip_cde LIKE 'HV%'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'HVXM'        AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_paus_mv  A  LEFT OUTER JOIN
              structools.st_bay_equipment       b
         ON A.plnt_no = b.stc2
         WHERE b.bay_equip_cde LIKE 'HV%'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'LVXM'        AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_paus_mv  A  LEFT OUTER JOIN
              structools.st_bay_equipment       b
         ON A.plnt_no = b.stc1
         WHERE b.bay_equip_cde LIKE 'LV%'
         UNION ALL
         SELECT plnt_no       AS pick_id
               ,'LVXM'        AS part_cde
               ,'N'     			AS	part_defective
--               ,NULL          AS defect_cde
--               ,NULL          AS sev_cde
         FROM structoolsw.nameplate_paus_mv  A  LEFT OUTER JOIN
              structools.st_bay_equipment       b
         ON A.plnt_no = b.stc2
         WHERE b.bay_equip_cde LIKE 'LV%'
         )
--         ,
--         xarms_and_stays_tmp2 AS
--         (
--         SELECT pick_id
--               ,part_cde
--               ,CASE
--                  WHEN part_cde = 'HVXM' AND defect_cde = 'SPLI' AND sev_cde = '1'   THEN 1
--                  WHEN part_cde = 'HVXM' AND defect_cde = 'UNSE'                     THEN 2
--                  WHEN part_cde = 'HVXM' AND defect_cde = 'WANT'                     THEN 3
--                  WHEN part_cde = 'HVXM' AND defect_cde = 'CORR' AND sev_cde = '1'   THEN 4
--                  WHEN part_cde = 'HVXM' AND defect_cde = 'SPLI' AND sev_cde = '2'   THEN 5
--                  WHEN part_cde = 'HVXM' AND defect_cde = 'BURN' AND sev_cde <= '2'  THEN 6
--                  WHEN part_cde = 'HVXM' AND defect_cde = 'CORR' AND sev_cde = '2'  THEN 7
--                  WHEN part_cde = 'HVXM' AND defect_cde = 'SPLI' AND sev_cde = '3'   THEN 8
--                  WHEN part_cde = 'HVXM' AND defect_cde = 'BURN' AND sev_cde = '3'   THEN 9
--                  WHEN part_cde = 'HVXM' AND defect_cde = 'CORR' AND sev_cde = '3'  THEN 10
--                  WHEN part_cde = 'HVXM' AND defect_cde = 'SXAM'                     THEN 11
--                  WHEN part_cde = 'LVXM' AND defect_cde = 'SPLI' AND sev_cde = '1'   THEN 1
--                  WHEN part_cde = 'LVXM' AND defect_cde = 'UNSE'                     THEN 2
--                  WHEN part_cde = 'LVXM' AND defect_cde = 'WANT'                     THEN 3
--                  WHEN part_cde = 'LVXM' AND defect_cde = 'CORR' AND sev_cde = '1'   THEN 4
--                  WHEN part_cde = 'LVXM' AND defect_cde = 'SPLI' AND sev_cde = '2'   THEN 5
--                  WHEN part_cde = 'LVXM' AND defect_cde = 'BURN' AND sev_cde <= '2'  THEN 6
--                  WHEN part_cde = 'LVXM' AND defect_cde = 'CORR' AND sev_cde = '2'  THEN 7
--                  WHEN part_cde = 'LVXM' AND defect_cde = 'SPLI' AND sev_cde = '3'   THEN 8
--                  WHEN part_cde = 'LVXM' AND defect_cde = 'BURN' AND sev_cde = '3'   THEN 9
--                  WHEN part_cde = 'LVXM' AND defect_cde = 'CORR' AND sev_cde = '3'  THEN 10
--                  WHEN part_cde = 'LVXM' AND defect_cde = 'SXAM'                     THEN 11
--                  WHEN part_cde = 'AGOST' AND defect_cde = 'STMS' AND sev_cde = '1'   THEN 1
--                  WHEN part_cde = 'AGOST' AND defect_cde = 'UNSE'                     THEN 2
--                  WHEN part_cde = 'AGOST' AND defect_cde = 'CORR' AND sev_cde = '1'   THEN 3
--                  WHEN part_cde = 'AGOST' AND defect_cde = 'STMS' AND sev_cde = '2'   THEN 4
--                  WHEN part_cde = 'AGOST' AND defect_cde = 'SIMS'                     THEN 5
--                  WHEN part_cde = 'AGOST' AND defect_cde = 'CORR' AND sev_cde = '2'   THEN 6
--                  WHEN part_cde = 'AGOST' AND defect_cde = 'SWNC'                     THEN 7
--                  WHEN part_cde = 'AGOST' AND defect_cde = 'STMS' AND sev_cde = '3'   THEN 8
--                  WHEN part_cde = 'AGOST' AND defect_cde = 'CORR' AND sev_cde = '3'   THEN 9
--                  WHEN part_cde = 'ASTAY' AND defect_cde = 'STMS' AND sev_cde = '1'   THEN 1
--                  WHEN part_cde = 'ASTAY' AND defect_cde = 'UNSE'                     THEN 2
--                  WHEN part_cde = 'ASTAY' AND defect_cde = 'CORR' AND sev_cde = '1'   THEN 3
--                  WHEN part_cde = 'ASTAY' AND defect_cde = 'STMS' AND sev_cde = '2'   THEN 4
--                  WHEN part_cde = 'ASTAY' AND defect_cde = 'SIMS'                     THEN 5
--                  WHEN part_cde = 'ASTAY' AND defect_cde = 'CORR' AND sev_cde = '2'   THEN 6
--                  WHEN part_cde = 'ASTAY' AND defect_cde = 'SWNC'                     THEN 7
--                  WHEN part_cde = 'ASTAY' AND defect_cde = 'STMS' AND sev_cde = '3'   THEN 8
--                  WHEN part_cde = 'ASTAY' AND defect_cde = 'CORR' AND sev_cde = '3'   THEN 9
--                  WHEN part_cde = 'GSTAY' AND defect_cde = 'STMS' AND sev_cde = '1'   THEN 1
--                  WHEN part_cde = 'GSTAY' AND defect_cde = 'UNSE'                     THEN 2
--                  WHEN part_cde = 'GSTAY' AND defect_cde = 'CORR' AND sev_cde = '1'   THEN 3
--                  WHEN part_cde = 'GSTAY' AND defect_cde = 'STMS' AND sev_cde = '2'   THEN 4
--                  WHEN part_cde = 'GSTAY' AND defect_cde = 'SIMS'                     THEN 5
--                  WHEN part_cde = 'GSTAY' AND defect_cde = 'CORR' AND sev_cde = '2'   THEN 6
--                  WHEN part_cde = 'GSTAY' AND defect_cde = 'SWNC'                     THEN 7
--                  WHEN part_cde = 'GSTAY' AND defect_cde = 'STMS' AND sev_cde = '3'   THEN 8
--                  WHEN part_cde = 'GSTAY' AND defect_cde = 'CORR' AND sev_cde = '3'   THEN 9
--                  WHEN part_cde = 'OSTAY' AND defect_cde = 'STMS' AND sev_cde = '1'   THEN 1
--                  WHEN part_cde = 'OSTAY' AND defect_cde = 'UNSE'                     THEN 2
--                  WHEN part_cde = 'OSTAY' AND defect_cde = 'CORR' AND sev_cde = '1'   THEN 3
--                  WHEN part_cde = 'OSTAY' AND defect_cde = 'STMS' AND sev_cde = '2'   THEN 4
--                  WHEN part_cde = 'OSTAY' AND defect_cde = 'SIMS'                     THEN 5
--                  WHEN part_cde = 'OSTAY' AND defect_cde = 'CORR' AND sev_cde = '2'   THEN 6
--                  WHEN part_cde = 'OSTAY' AND defect_cde = 'SWNC'                     THEN 7
--                  WHEN part_cde = 'OSTAY' AND defect_cde = 'STMS' AND sev_cde = '3'   THEN 8
--                  WHEN part_cde = 'OSTAY' AND defect_cde = 'CORR' AND sev_cde = '3'   THEN 9
--                  ELSE                                                                     99
--               END                                       AS xarm_stay_defect_hier
--         FROM xarms_and_stays_tmp1
--         )
         ,
         xarms_and_stays_tmp3 AS
         (
         SELECT pick_id
               ,part_cde
               ,max(part_defective)					AS part_defective
--               ,MIN(xarm_stay_defect_hier) AS xarm_stay_defect_hier
         FROM xarms_and_stays_tmp1
--         FROM xarms_and_stays_tmp2
--         WHERE xarm_stay_defect_hier IS NOT NULL      -- it can be null if sev_cde > '3' but we don'T want TO include > '3' FOR THE particular part cde         
         GROUP BY pick_id, part_cde
         )
         ,
         pole_labour_hrs AS
         (
         SELECT equip_no
         			,MAX(dervd_tool_tim_tot) AS labour_hrs   --some poles have two rows, if they support both lv and hv. The tool_tim_tot should be the same on both rows, but just in case we do max() to ensure there is one entry for pickid
			FROM	structoolsw.pit_pole_configuration
         WHERE curr_flg = 'Y'														--can be removed if the table in structoolsw only includes current set.       
         GROUP BY equip_no           
         )
         ,
         poles AS
         (
         SELECT trim(A.equip_no) AS equip_no 
               ,A.pick_id
               ,TRIM(A.equip_grp_id)         AS equip_grp_id
               ,A.prnt_plnt_no
               ,c.rgn_nam       AS region_nam
               ,A.maint_zone_nam
--               ,CASE
--                  WHEN UPPER(TRIM(b.reinf_cde)) = 'NO REINFORCING' THEN NULL 
--                  WHEN b.reinf_cde = ' '															   THEN NULL
--                  WHEN b.reinf_cde IS NULL													   THEN NULL
--                  WHEN UPPER(TRIM(b.reinf_cde)) = '#NA# '					  THEN NULL
--                  WHEN E.legacy_ind = 'LR'														 THEN 'LR'     
--                  ELSE																											  'R'
--                END                             AS reinf_cde
               ,CASE
                  WHEN UPPER(TRIM(E.reinforced)) = 'REINFORCED' AND upper(trim(E.reinforcing)) = 'LEGACY REINFORCING' THEN 'LR'
                  WHEN UPPER(TRIM(E.reinforced)) = 'REINFORCED' 																												 THEN 'R'
                  ELSE																											  																											NULL
                END                             AS reinf_cde
               ,EXTRACT (YEAR FROM b.instln_dt) AS instln_year
               ,CASE b.wood_species
                  WHEN 'Radiata Pine' THEN 'S'
                  WHEN 'Pinus Silvestris/Baltic/Scotts' THEN 'S'
                  WHEN 'Pinasta Pine' THEN 'S'
                  WHEN 'Maritime Pine' THEN 'S'
                  WHEN 'Unknown Softwood' THEN 'S'
                  ELSE                       'H'
                END           wood_type
               ,NULL          recl_phases
               ,A.fire_risk_zone_cls
               ,d.pole_diam  pole_diameter_mm
               ,d.good_wood_reading_1m_agl   
               ,d.good_wood_reading_100_agl_mm
               ,d.good_wood_reading_100_bgl_mm
               ,d.si_at_gl
               ,d.si_at_1m_agl
               ,CASE A.Equip_Desc
                  WHEN 'ANGLE POLE'             THEN 'MEDIUM'
                  WHEN 'CORNER POLE'            THEN 'MAJOR'
                  WHEN 'IN-LINE STRAIN'         THEN 'MEDIUM'
                  WHEN 'IN LINE STRAIN POLE'    THEN 'MEDIUM'
                  WHEN 'IN-LINE TERMINATION'    THEN 'MEDIUM'
                  WHEN 'IN LINE TERMINATION POLE' THEN 'MEDIUM'
                  WHEN 'INTERMEDIATE POLE'      THEN 'MINOR'
                  WHEN 'LAMP POLE'              THEN 'MINOR'
                  WHEN 'SERVICE POLE'           THEN 'MINOR'
                  WHEN 'STAY POLE'              THEN 'STAY'
                  WHEN 'TEE OFF POLE'           THEN 'MAJOR'
                  WHEN 'TERMINATION POLE'       THEN 'MEDIUM'
                  WHEN 'TRANSFORMER POLE'       THEN 'MAJOR'
                  ELSE                               'MINOR'
               END                  AS pole_complexity
               ,NULL                AS dstr_size
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN c.swsect_net_nid
                  ELSE c.swsect_upstrm_prdev_id
               END                                    AS prot_zone_id
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN 'DSTR'
                  ELSE c.swsect_upstrm_prdev_typ
               END                                    AS prot_zone_typ
               ,CASE c.swsect_hv_fdr_id_no   
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_hv_fdr_id_no
               END                                 AS feeder_id 
               ,CASE c.swsect_nsw_nid
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_nsw_nid
               END                                 AS ss_id
               ,CASE c.swsect_hv_fdr_nam   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_hv_fdr_nam
               END                                 AS feeder_nam
               ,CASE c.swsect_zone_substn_desc   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_zone_substn_desc
               END                                 AS zone_subst_nam
               ,NULL                      AS manuf
               ,NULL                      AS stock_cde         
               ,NULL                      AS ctl_typ
               ,NULL                      AS dev_typ         
               ,NULL                      AS manuf_and_mdl
               ,NULL                      AS expulsion_typ
               ,CASE c.swsect_net_nid
                  WHEN -8888              THEN  NULL
                  ELSE                          c.swsect_net_nid
               END                        AS swsect_net_nid
               ,c.max_safety_zone_rtg
               ,CASE
                  WHEN c.ss_prdev_ds_lv_mtr_cnt = 0 AND c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN
                     GREATEST(
                     (SELECT ss_prdev_ds_lv_mtr_cnt 
                     FROM STRUCTOOLSW.TOPO_GEOG_MV
                     WHERE plnt_no = 'N'||c.swsect_net_nid), c.ss_ds_lv_mtr_cnt)
                  WHEN c.ss_prdev_ds_lv_mtr_cnt < c.ss_ds_lv_mtr_cnt                                  THEN  c.ss_ds_lv_mtr_cnt
                  WHEN c.ss_prdev_ds_lv_mtr_cnt IS NULL OR c.ss_prdev_ds_lv_mtr_cnt = -8888           THEN  NULL
                  ELSE                                                                                      c.ss_prdev_ds_lv_mtr_cnt
               END                        AS cust_ds_cnt
               ,c.urbn_inspn_classfn
               --,E.reinf_cde				AS reinf_cde_dw
               ,E.reinf_code				AS reinf_cde_dw
               ,f.labour_hrs
               ,CASE
               	WHEN upper(b.cca_treated) = 'YES'	THEN	'Y'
                  ELSE																			'N'
               END			AS cca_treated
         FROM eqp_dim A INNER JOIN
            structoolsw.NAMEPLATE_PWOD_MV b
         ON A.equip_no = b.equip_no   
                                       INNER JOIN
           structoolsw.TOPO_GEOG_MV         c
         ON A.equip_no = c.equip_no   
                                       LEFT OUTER JOIN
            INSP_RSLT d
         ON A.equip_no = d.equip_no                                 
         										 LEFT OUTER JOIN
				--structools.insp_reinf_cde_map	E
			--ON upper(b.reinf_cde) = E.reinf_cde       
            structoolsw.pit_pole_infopack	E
            ON A.pick_id = E.plnt_no
         										LEFT OUTER JOIN
				pole_labour_hrs						f
			ON A.equip_no = f.equip_no                                                                                                
         WHERE A.equip_cls = 'WE'
         AND A.equip_grp_id = 'PWOD'
         UNION ALL
         SELECT A.equip_no 
               ,A.pick_id
               ,TRIM(A.equip_grp_id)   AS equip_grp_id   
               ,A.prnt_plnt_no
               ,c.rgn_nam              AS region_nam
               ,A.maint_zone_nam
               ,NULL          reinf_cde
               ,EXTRACT (YEAR FROM b.instln_dt) AS instln_year
               ,CASE b.wood_species
                  WHEN 'Radiata Pine' THEN 'S'
                  WHEN 'Pinus Silvestris/Baltic/Scotts' THEN 'S'
                  WHEN 'Pinasta Pine' THEN 'S'
                  WHEN 'Maritime Pine' THEN 'S'
                  ELSE                       'H'
                END           wood_type
               ,NULL          recl_phases
               ,A.fire_risk_zone_cls
               ,NULL          pole_diameter_mm
               ,NULL          good_wood_reading_1m_agl   
               ,NULL          good_wood_reading_100_agl_mm
               ,NULL          good_wood_reading_100_bgl_mm
               ,NULL          si_at_gl
               ,NULL          si_at_1m_agl
               ,CASE A.Equip_Desc
                  WHEN 'ANGLE POLE'             THEN 'MEDIUM'
                  WHEN 'CORNER POLE'            THEN 'MAJOR'
                  WHEN 'IN-LINE STRAIN'         THEN 'MEDIUM'
                  WHEN 'IN LINE STRAIN POLE'    THEN 'MEDIUM'
                  WHEN 'IN-LINE TERMINATION'    THEN 'MEDIUM'
                  WHEN 'IN LINE TERMINATION POLE' THEN 'MEDIUM'
                  WHEN 'INTERMEDIATE POLE'      THEN 'MINOR'
                  WHEN 'LAMP POLE'              THEN 'MINOR'
                  WHEN 'SERVICE POLE'           THEN 'MINOR'
                  WHEN 'STAY POLE'              THEN 'STAY'
                  WHEN 'TEE OFF POLE'           THEN 'MAJOR'
                  WHEN 'TERMINATION POLE'       THEN 'MEDIUM'
                  WHEN 'TRANSFORMER POLE'       THEN 'MAJOR'
                  ELSE                               'MINOR'
               END                  AS pole_complexity
               ,NULL          dstr_size
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN c.swsect_net_nid
                  ELSE c.swsect_upstrm_prdev_id
               END                                    AS prot_zone_id
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN 'DSTR'
                  ELSE c.swsect_upstrm_prdev_typ
               END                                    AS prot_zone_typ
               ,CASE c.swsect_hv_fdr_id_no   
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_hv_fdr_id_no
               END                                 AS feeder_id 
               ,CASE c.swsect_nsw_nid
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_nsw_nid
               END                                 AS ss_id
               ,CASE c.swsect_hv_fdr_nam   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_hv_fdr_nam
               END                                 AS feeder_nam
               ,CASE c.swsect_zone_substn_desc   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_zone_substn_desc
               END                                 AS zone_subst_nam 
               ,NULL                      AS manuf
               ,NULL                      AS stock_cde         
               ,NULL                      AS ctl_typ
               ,NULL                      AS dev_typ         
               ,NULL                      AS manuf_and_mdl
               ,NULL                      AS expulsion_typ
               ,CASE c.swsect_net_nid
                  WHEN -8888              THEN  NULL
                  ELSE                          c.swsect_net_nid
               END                        AS swsect_net_nid
               ,c.max_safety_zone_rtg
               ,CASE
                  WHEN c.ss_prdev_ds_lv_mtr_cnt = 0 AND c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN
                     GREATEST(
                     (SELECT ss_prdev_ds_lv_mtr_cnt 
                     FROM STRUCTOOLSW.TOPO_GEOG_MV
                     WHERE plnt_no = 'N'||c.swsect_net_nid), c.ss_ds_lv_mtr_cnt)
                  WHEN c.ss_prdev_ds_lv_mtr_cnt < c.ss_ds_lv_mtr_cnt                                  THEN  c.ss_ds_lv_mtr_cnt
                  WHEN c.ss_prdev_ds_lv_mtr_cnt IS NULL OR c.ss_prdev_ds_lv_mtr_cnt = -8888           THEN  NULL
                  ELSE                                                                                      c.ss_prdev_ds_lv_mtr_cnt
               END                        AS cust_ds_cnt
               ,c.urbn_inspn_classfn
               ,NULL						AS reinf_cde_dw
               ,d.labour_hrs
               ,NULL						AS cca_treated
         FROM eqp_dim A INNER JOIN
            structoolsw.NAMEPLATE_PAUS_MV b
         ON A.equip_no = b.equip_no   
                                       INNER JOIN
           structoolsw.TOPO_GEOG_MV         c
         ON A.equip_no = c.equip_no   
         										LEFT OUTER JOIN
				pole_labour_hrs						d
			ON A.equip_no = d.equip_no                                                                                                
         WHERE A.equip_cls = 'WE'
         AND A.equip_grp_id = 'PAUS'
         UNION ALL
         SELECT A.equip_no 
               ,A.pick_id
               ,TRIM(A.equip_grp_id)   AS equip_grp_id   
               ,A.prnt_plnt_no
               ,c.rgn_nam              AS region_nam
               ,A.maint_zone_nam
               ,NULL          reinf_cde
               ,EXTRACT (YEAR FROM b.instln_dt) AS instln_year
               ,NULL				wood_type
               ,NULL          recl_phases
               ,A.fire_risk_zone_cls
               ,NULL          pole_diameter_mm
               ,NULL          good_wood_reading_1m_agl   
               ,NULL          good_wood_reading_100_agl_mm
               ,NULL          good_wood_reading_100_bgl_mm
               ,NULL          si_at_gl
               ,NULL          si_at_1m_agl
               ,CASE A.Equip_Desc
                  WHEN 'ANGLE POLE'             THEN 'MEDIUM'
                  WHEN 'CORNER POLE'            THEN 'MAJOR'
                  WHEN 'IN-LINE STRAIN'         THEN 'MEDIUM'
                  WHEN 'IN LINE STRAIN POLE'    THEN 'MEDIUM'
                  WHEN 'IN-LINE TERMINATION'    THEN 'MEDIUM'
                  WHEN 'IN LINE TERMINATION POLE' THEN 'MEDIUM'
                  WHEN 'INTERMEDIATE POLE'      THEN 'MINOR'
                  WHEN 'LAMP POLE'              THEN 'MINOR'
                  WHEN 'SERVICE POLE'           THEN 'MINOR'
                  WHEN 'STAY POLE'              THEN 'STAY'
                  WHEN 'TEE OFF POLE'           THEN 'MAJOR'
                  WHEN 'TERMINATION POLE'       THEN 'MEDIUM'
                  WHEN 'TRANSFORMER POLE'       THEN 'MAJOR'
                  ELSE                               'MINOR'
               END                  AS pole_complexity
               ,NULL          dstr_size
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN c.swsect_net_nid
                  ELSE c.swsect_upstrm_prdev_id
               END                                    AS prot_zone_id
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN 'DSTR'
                  ELSE c.swsect_upstrm_prdev_typ
               END                                    AS prot_zone_typ
               ,CASE c.swsect_hv_fdr_id_no   
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_hv_fdr_id_no
               END                                 AS feeder_id 
               ,CASE c.swsect_nsw_nid
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_nsw_nid
               END                                 AS ss_id
               ,CASE c.swsect_hv_fdr_nam   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_hv_fdr_nam
               END                                 AS feeder_nam
               ,CASE c.swsect_zone_substn_desc   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_zone_substn_desc
               END                                 AS zone_subst_nam 
               ,NULL                      AS manuf
               ,NULL                      AS stock_cde         
               ,NULL                      AS ctl_typ
               ,NULL                      AS dev_typ         
               ,NULL                      AS manuf_and_mdl
               ,NULL                      AS expulsion_typ
               ,CASE c.swsect_net_nid
                  WHEN -8888              THEN  NULL
                  ELSE                          c.swsect_net_nid
               END                        AS swsect_net_nid
               ,c.max_safety_zone_rtg
               ,CASE
                  WHEN c.ss_prdev_ds_lv_mtr_cnt = 0 AND c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN
                     GREATEST(
                     (SELECT ss_prdev_ds_lv_mtr_cnt 
                     FROM STRUCTOOLSW.TOPO_GEOG_MV
                     WHERE plnt_no = 'N'||c.swsect_net_nid), c.ss_ds_lv_mtr_cnt)
                  WHEN c.ss_prdev_ds_lv_mtr_cnt < c.ss_ds_lv_mtr_cnt                                  THEN  c.ss_ds_lv_mtr_cnt
                  WHEN c.ss_prdev_ds_lv_mtr_cnt IS NULL OR c.ss_prdev_ds_lv_mtr_cnt = -8888           THEN  NULL
                  ELSE                                                                                      c.ss_prdev_ds_lv_mtr_cnt
               END                        AS cust_ds_cnt
               ,c.urbn_inspn_classfn
               ,NULL						AS reinf_cde_dw
               ,d.labour_hrs
               ,NULL						AS cca_treated
         FROM eqp_dim A INNER JOIN
            structoolsw.NAMEPLATE_PCON_MV b
         ON A.equip_no = b.equip_no   
                                       INNER JOIN
           structoolsw.TOPO_GEOG_MV         c
         ON A.equip_no = c.equip_no   
         										LEFT OUTER JOIN
				pole_labour_hrs						d
			ON A.equip_no = d.equip_no                                                                                                
         WHERE A.equip_cls = 'WE'
         AND A.equip_grp_id = 'PCON'
         UNION ALL
         SELECT A.equip_no 
               ,A.pick_id
               ,TRIM(A.equip_grp_id)   AS equip_grp_id   
               ,A.prnt_plnt_no
               ,c.rgn_nam              AS region_nam
               ,A.maint_zone_nam
               ,NULL          reinf_cde
               ,EXTRACT (YEAR FROM b.instln_dt) AS instln_year
               ,NULL				wood_type
               ,NULL          recl_phases
               ,A.fire_risk_zone_cls
               ,NULL          pole_diameter_mm
               ,NULL          good_wood_reading_1m_agl   
               ,NULL          good_wood_reading_100_agl_mm
               ,NULL          good_wood_reading_100_bgl_mm
               ,NULL          si_at_gl
               ,NULL          si_at_1m_agl
               ,CASE A.Equip_Desc
                  WHEN 'ANGLE POLE'             THEN 'MEDIUM'
                  WHEN 'CORNER POLE'            THEN 'MAJOR'
                  WHEN 'IN-LINE STRAIN'         THEN 'MEDIUM'
                  WHEN 'IN LINE STRAIN POLE'    THEN 'MEDIUM'
                  WHEN 'IN-LINE TERMINATION'    THEN 'MEDIUM'
                  WHEN 'IN LINE TERMINATION POLE' THEN 'MEDIUM'
                  WHEN 'INTERMEDIATE POLE'      THEN 'MINOR'
                  WHEN 'LAMP POLE'              THEN 'MINOR'
                  WHEN 'SERVICE POLE'           THEN 'MINOR'
                  WHEN 'STAY POLE'              THEN 'STAY'
                  WHEN 'TEE OFF POLE'           THEN 'MAJOR'
                  WHEN 'TERMINATION POLE'       THEN 'MEDIUM'
                  WHEN 'TRANSFORMER POLE'       THEN 'MAJOR'
                  ELSE                               'MINOR'
               END                  AS pole_complexity
               ,NULL          dstr_size
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN c.swsect_net_nid
                  ELSE c.swsect_upstrm_prdev_id
               END                                    AS prot_zone_id
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN 'DSTR'
                  ELSE c.swsect_upstrm_prdev_typ
               END                                    AS prot_zone_typ
               ,CASE c.swsect_hv_fdr_id_no   
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_hv_fdr_id_no
               END                                 AS feeder_id 
               ,CASE c.swsect_nsw_nid
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_nsw_nid
               END                                 AS ss_id
               ,CASE c.swsect_hv_fdr_nam   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_hv_fdr_nam
               END                                 AS feeder_nam
               ,CASE c.swsect_zone_substn_desc   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_zone_substn_desc
               END                                 AS zone_subst_nam 
               ,NULL                      AS manuf
               ,NULL                      AS stock_cde         
               ,NULL                      AS ctl_typ
               ,NULL                      AS dev_typ         
               ,NULL                      AS manuf_and_mdl
               ,NULL                      AS expulsion_typ
               ,CASE c.swsect_net_nid
                  WHEN -8888              THEN  NULL
                  ELSE                          c.swsect_net_nid
               END                        AS swsect_net_nid
               ,c.max_safety_zone_rtg
               ,CASE
                  WHEN c.ss_prdev_ds_lv_mtr_cnt = 0 AND c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN
                     GREATEST(
                     (SELECT ss_prdev_ds_lv_mtr_cnt 
                     FROM STRUCTOOLSW.TOPO_GEOG_MV
                     WHERE plnt_no = 'N'||c.swsect_net_nid), c.ss_ds_lv_mtr_cnt)
                  WHEN c.ss_prdev_ds_lv_mtr_cnt < c.ss_ds_lv_mtr_cnt                                  THEN  c.ss_ds_lv_mtr_cnt
                  WHEN c.ss_prdev_ds_lv_mtr_cnt IS NULL OR c.ss_prdev_ds_lv_mtr_cnt = -8888           THEN  NULL
                  ELSE                                                                                      c.ss_prdev_ds_lv_mtr_cnt
               END                        AS cust_ds_cnt
               ,c.urbn_inspn_classfn
               ,NULL						AS reinf_cde_dw
               ,d.labour_hrs
               ,NULL						AS cca_treated
         FROM eqp_dim A INNER JOIN
            structoolsw.NAMEPLATE_PMET_MV b
         ON A.equip_no = b.equip_no   
                                       INNER JOIN
           structoolsw.TOPO_GEOG_MV         c
         ON A.equip_no = c.equip_no   
         										LEFT OUTER JOIN
				pole_labour_hrs						d
			ON A.equip_no = d.equip_no                                                                                                
         WHERE A.equip_cls = 'WE'
         AND A.equip_grp_id = 'PMET'
         )
         ,
         xarms_and_stays AS 
         (
         SELECT * FROM
         (
         SELECT b.*
               ,A.part_cde
               ,A.part_defective
--               ,CASE
--                  WHEN A.xarm_stay_defect_hier = 99      THEN     0
--                  ELSE                                            A.xarm_stay_defect_hier
--               END         AS xarm_stay_defect_hier
         FROM  poles                b      LEFT OUTER JOIN  
               xarms_and_stays_tmp3     A    
         ON A.pick_id = b.pick_id           
         ) aa
         WHERE aa.part_cde IS NOT NULL     
         )
         ,
         eqp AS
         (
         SELECT A.equip_no 
               ,A.pick_id
               ,TRIM(A.equip_grp_id)   AS equip_grp_id
               ,A.prnt_plnt_no
               ,c.rgn_nam        AS region_nam
               ,A.maint_zone_nam
               ,NULL          reinf_cde
               ,EXTRACT (YEAR FROM b.instln_dt) AS instln_year
               ,NULL          wood_type
               ,NULL          recl_phases
               ,A.fire_risk_zone_cls
               ,NULL          pole_diameter_mm
               ,NULL          good_wood_reading_1m_agl   
               ,NULL          good_wood_reading_100_agl_mm
               ,NULL          good_wood_reading_100_bgl_mm
               ,NULL          si_at_gl
               ,NULL          si_at_1m_agl
               ,NULL             pole_complexity
               ,CASE 
                  WHEN b.ratg_kva < 100 THEN   'SMALL'
                  WHEN b.ratg_kva < 300 THEN   'MEDIUM'
                  ELSE              'LARGE'
               END            AS dstr_size
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN c.swsect_net_nid
                  ELSE c.swsect_upstrm_prdev_id
               END                                    AS prot_zone_id
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN 'DSTR'
                  ELSE c.swsect_upstrm_prdev_typ
               END                                    AS prot_zone_typ
               ,CASE c.swsect_hv_fdr_id_no   
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_hv_fdr_id_no
               END                                 AS feeder_id
               ,CASE c.swsect_nsw_nid
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_nsw_nid
               END                                 AS ss_id
               ,CASE c.swsect_hv_fdr_nam   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_hv_fdr_nam
               END                                 AS feeder_nam
               ,CASE c.swsect_zone_substn_desc   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_zone_substn_desc
               END                                 AS zone_subst_nam
               ,CASE b.manuf
                  WHEN ' '                THEN NULL
                  WHEN '#NA#'             THEN NULL
                  WHEN 'Unknown'          THEN NULL
                  ELSE                    UPPER(b.manuf)
               END                                 AS manuf 
               ,CASE b.stck_cde
                  WHEN ' '                THEN NULL
                  WHEN 'UNKNOWN'          THEN NULL
                  ELSE                    UPPER(b.stck_cde)
               END                                 AS stock_cde         
               ,NULL                               AS ctl_typ
               ,NULL                               AS dev_typ         
               ,NULL                               AS manuf_and_mdl
               ,NULL                      AS expulsion_typ
               ,CASE c.swsect_net_nid
                  WHEN -8888              THEN  NULL
                  ELSE                          c.swsect_net_nid
               END                        AS swsect_net_nid
               ,c.max_safety_zone_rtg
               ,CASE
                  WHEN c.ss_prdev_ds_lv_mtr_cnt = 0 AND c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN
                     GREATEST(
                     (SELECT ss_prdev_ds_lv_mtr_cnt 
                     FROM STRUCTOOLSW.TOPO_GEOG_MV
                     WHERE plnt_no = 'N'||c.swsect_net_nid), c.ss_ds_lv_mtr_cnt)
                  WHEN c.ss_prdev_ds_lv_mtr_cnt < c.ss_ds_lv_mtr_cnt                                  THEN  c.ss_ds_lv_mtr_cnt
                  WHEN c.ss_prdev_ds_lv_mtr_cnt IS NULL OR c.ss_prdev_ds_lv_mtr_cnt = -8888           THEN  NULL
                  ELSE                                                                                      c.ss_prdev_ds_lv_mtr_cnt
               END   AS cust_ds_cnt
         FROM eqp_dim A INNER JOIN
            structoolsw.NAMEPLATE_WL_MV b
         ON A.equip_no = b.equip_no   
                                       INNER JOIN
           structoolsw.TOPO_GEOG_MV         c
         ON A.equip_no = c.equip_no   
         WHERE A.equip_cls = 'WL'
         AND A.equip_grp_id = 'DSTR'
         UNION ALL
         SELECT A.equip_no 
               ,A.pick_id
               ,TRIM(A.equip_grp_id)   AS equip_grp_id
               ,A.prnt_plnt_no
               ,c.rgn_nam        AS region_nam
               ,A.maint_zone_nam
               ,NULL          reinf_cde
               ,EXTRACT (YEAR FROM b.instln_dt) AS instln_year
               ,NULL          wood_type
               ,CASE
                  WHEN TRIM(A.equip_grp_id) = 'LBS' THEN NULL 
                  WHEN TRIM(A.equip_grp_id) = 'RECL'  AND b.recloser_phs_typ = 'Single Phase'  THEN '1'
                  WHEN TRIM(A.equip_grp_id) = 'RECL'  AND b.recloser_phs_typ = 'Three Phase'   THEN '3'
                  WHEN TRIM(A.equip_grp_id) = 'RECL'                                           THEN '3'
                END                 recl_phases
               ,A.fire_risk_zone_cls
               ,NULL          pole_diameter_mm
               ,NULL          good_wood_reading_1m_agl   
               ,NULL          good_wood_reading_100_agl_mm
               ,NULL          good_wood_reading_100_bgl_mm
               ,NULL          si_at_gl
               ,NULL          si_at_1m_agl
               ,NULL          pole_complexity
               ,NULL          dstr_size
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN c.swsect_net_nid
                  ELSE c.swsect_upstrm_prdev_id
               END                                    AS prot_zone_id
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN 'DSTR'
                  ELSE c.swsect_upstrm_prdev_typ
               END                                    AS prot_zone_typ
               ,CASE c.swsect_hv_fdr_id_no   
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_hv_fdr_id_no
               END                                 AS feeder_id 
               ,CASE c.swsect_nsw_nid
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_nsw_nid
               END                                 AS ss_id
               ,CASE c.swsect_hv_fdr_nam   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_hv_fdr_nam
               END                                 AS feeder_nam
               ,CASE c.swsect_zone_substn_desc   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_zone_substn_desc
               END                                 AS zone_subst_nam 
               ,NULL                               AS manuf
               ,CASE b.stck_cde
                  WHEN ' '                THEN NULL
                  WHEN 'UNKNOWN'          THEN NULL
                  ELSE                    UPPER(b.stck_cde)
               END                                 AS stock_cde         
               ,CASE b.ctl_typ
                  WHEN ' '                THEN NULL
                  WHEN '#NA#'             THEN NULL
                  WHEN 'Unknown'          THEN NULL
                  ELSE                    UPPER(b.ctl_typ)
               END                                 AS ctl_typ
               ,CASE b.dev_typ         
                  WHEN ' '                THEN NULL
                  WHEN '#NA#'             THEN NULL
                  WHEN 'Unknown'          THEN NULL
                  ELSE                    UPPER(b.dev_typ)
               END                                 AS dev_typ
               ,CASE b.manuf_and_mdl
                  WHEN ' '                THEN NULL
                  WHEN '#NA#'             THEN NULL
                  WHEN 'Unknown'          THEN NULL
                  ELSE                    UPPER(b.manuf_and_mdl)
               END                                 AS manuf_and_mdl 
               ,NULL                      AS expulsion_typ
               ,CASE c.swsect_net_nid
                  WHEN -8888              THEN  NULL
                  ELSE                          c.swsect_net_nid
               END                        AS swsect_net_nid
               ,c.max_safety_zone_rtg
               ,CASE
                  WHEN c.ss_prdev_ds_lv_mtr_cnt = 0 AND c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN
                     GREATEST(
                     (SELECT ss_prdev_ds_lv_mtr_cnt 
                     FROM STRUCTOOLSW.TOPO_GEOG_MV
                     WHERE plnt_no = 'N'||c.swsect_net_nid), c.ss_ds_lv_mtr_cnt)
                  WHEN c.ss_prdev_ds_lv_mtr_cnt < c.ss_ds_lv_mtr_cnt                                  THEN  c.ss_ds_lv_mtr_cnt
                  WHEN c.ss_prdev_ds_lv_mtr_cnt IS NULL OR c.ss_prdev_ds_lv_mtr_cnt = -8888           THEN  NULL
                  ELSE                                                                                      c.ss_prdev_ds_lv_mtr_cnt
               END   AS cust_ds_cnt
         FROM eqp_dim             A  INNER JOIN
            structoolsw.NAMEPLATE_ACTIVE_DEVICE_MV  b
         ON A.equip_no = b.equip_no   
                                                   INNER JOIN
           structoolsw.TOPO_GEOG_MV                 c
         ON A.equip_no = c.equip_no   
         WHERE A.equip_cls = 'W1'
         AND A.equip_grp_id IN ('RECL','LBS','SECT')
         UNION ALL
         SELECT A.equip_no 
               ,A.pick_id
               ,TRIM(A.equip_grp_id)   AS equip_grp_id
               ,A.prnt_plnt_no
               ,c.rgn_nam        AS region_nam
               ,A.maint_zone_nam
               ,NULL          reinf_cde
               ,EXTRACT (YEAR FROM b.instln_dt) AS instln_year
               ,NULL          wood_type
               ,NULL          recl_phases
               ,A.fire_risk_zone_cls
               ,NULL          pole_diameter_mm
               ,NULL          good_wood_reading_1m_agl   
               ,NULL          good_wood_reading_100_agl_mm
               ,NULL          good_wood_reading_100_bgl_mm
               ,NULL          si_at_gl
               ,NULL          si_at_1m_agl
               ,NULL          pole_complexity
               ,NULL          dstr_size
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN c.swsect_net_nid
                  ELSE c.swsect_upstrm_prdev_id
               END                                    AS prot_zone_id
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN 'DSTR'
                  ELSE c.swsect_upstrm_prdev_typ
               END                                    AS prot_zone_typ
               ,CASE c.swsect_hv_fdr_id_no   
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_hv_fdr_id_no
               END                                 AS feeder_id 
               ,CASE c.swsect_nsw_nid
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_nsw_nid
               END                                 AS ss_id
               ,CASE c.swsect_hv_fdr_nam   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_hv_fdr_nam
               END                                 AS feeder_nam
               ,CASE c.swsect_zone_substn_desc   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_zone_substn_desc
               END                                 AS zone_subst_nam 
               ,CASE b.manuf
                  WHEN ' '                THEN NULL
                  WHEN '#NA#'             THEN NULL
                  WHEN 'Unknown'          THEN NULL
                  ELSE                    UPPER(b.manuf)
               END                                 AS manuf 
               ,CASE b.stck_cde
                  WHEN ' '                THEN NULL
                  WHEN 'UNKNOWN'          THEN NULL
                  ELSE                    UPPER(b.stck_cde)
               END                                 AS stock_cde         
               ,NULL                               AS ctl_typ
               ,NULL                               AS dev_typ         
               ,CASE b.manuf_and_mdl
                  WHEN ' '                THEN NULL
                  WHEN '#NA#'             THEN NULL
                  WHEN 'Unknown'          THEN NULL
                  ELSE                    UPPER(b.manuf_and_mdl)
               END                                 AS manuf_and_mdl 
              ,expulsion_typ 
               ,CASE c.swsect_net_nid
                  WHEN -8888              THEN  NULL
                  ELSE                          c.swsect_net_nid
               END                        AS swsect_net_nid
               ,c.max_safety_zone_rtg
               ,CASE
                  WHEN c.ss_prdev_ds_lv_mtr_cnt = 0 AND c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN
                     GREATEST(
                     (SELECT ss_prdev_ds_lv_mtr_cnt 
                     FROM STRUCTOOLSW.TOPO_GEOG_MV
                     WHERE plnt_no = 'N'||c.swsect_net_nid), c.ss_ds_lv_mtr_cnt)
                  WHEN c.ss_prdev_ds_lv_mtr_cnt < c.ss_ds_lv_mtr_cnt                                  THEN  c.ss_ds_lv_mtr_cnt
                  WHEN c.ss_prdev_ds_lv_mtr_cnt IS NULL OR c.ss_prdev_ds_lv_mtr_cnt = -8888           THEN  NULL
                  ELSE                                                                                      c.ss_prdev_ds_lv_mtr_cnt
               END   AS cust_ds_cnt
         FROM eqp_dim A INNER JOIN
            structoolsw.NAMEPLATE_FUSE_MV b
         ON A.equip_no = b.equip_no   
                                       INNER JOIN
           structoolsw.TOPO_GEOG_MV         c
         ON A.equip_no = c.equip_no   
         WHERE A.equip_cls = 'W6'
         AND A.equip_grp_id = 'DOF'
         UNION ALL
         SELECT A.equip_no 
               ,A.pick_id
               ,TRIM(A.equip_grp_id)   AS equip_grp_id
               ,A.prnt_plnt_no
               ,c.rgn_nam        AS region_nam
               ,A.maint_zone_nam
               ,NULL          reinf_cde
               ,EXTRACT (YEAR FROM b.instln_dt) AS instln_year
               ,NULL          wood_type
               ,NULL          recl_phases
               ,A.fire_risk_zone_cls
               ,NULL          pole_diameter_mm
               ,NULL          good_wood_reading_1m_agl   
               ,NULL          good_wood_reading_100_agl_mm
               ,NULL          good_wood_reading_100_bgl_mm
               ,NULL          si_at_gl
               ,NULL          si_at_1m_agl
               ,NULL          pole_complexity
               ,NULL          dstr_size
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN c.swsect_net_nid
                  ELSE c.swsect_upstrm_prdev_id
               END                                    AS prot_zone_id
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN 'DSTR'
                  ELSE c.swsect_upstrm_prdev_typ
               END                                    AS prot_zone_typ
               ,CASE c.swsect_hv_fdr_id_no   
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_hv_fdr_id_no
               END                                 AS feeder_id 
               ,CASE c.swsect_nsw_nid 
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_nsw_nid
               END                                 AS ss_id
               ,CASE c.swsect_hv_fdr_nam   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_hv_fdr_nam
               END                                 AS feeder_nam
               ,CASE c.swsect_zone_substn_desc   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_zone_substn_desc
               END                                 AS zone_subst_nam 
               ,NULL                      AS manuf
               ,NULL                      AS stock_cde         
               ,NULL                      AS ctl_typ
               ,NULL                      AS dev_typ         
               ,NULL                      AS manuf_and_mdl
               ,NULL                      AS expulsion_typ
               ,CASE c.swsect_net_nid
                  WHEN -8888              THEN  NULL
                  ELSE                          c.swsect_net_nid
               END                        AS swsect_net_nid
               ,c.max_safety_zone_rtg
               ,CASE
                  WHEN c.ss_prdev_ds_lv_mtr_cnt = 0 AND c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN
                     GREATEST(
                     (SELECT ss_prdev_ds_lv_mtr_cnt 
                     FROM STRUCTOOLSW.TOPO_GEOG_MV
                     WHERE plnt_no = 'N'||c.swsect_net_nid), c.ss_ds_lv_mtr_cnt)
                  WHEN c.ss_prdev_ds_lv_mtr_cnt < c.ss_ds_lv_mtr_cnt                                  THEN  c.ss_ds_lv_mtr_cnt
                  WHEN c.ss_prdev_ds_lv_mtr_cnt IS NULL OR c.ss_prdev_ds_lv_mtr_cnt = -8888           THEN  NULL
                  ELSE                                                                                      c.ss_prdev_ds_lv_mtr_cnt
               END   AS cust_ds_cnt
         FROM eqp_dim A INNER JOIN
            structoolsw.NAMEPLATE_WN_MV b
         ON A.equip_no = b.equip_no   
                                       INNER JOIN
           structoolsw.TOPO_GEOG_MV         c
         ON A.equip_no = c.equip_no   
         WHERE A.equip_cls = 'WN'
         AND A.equip_grp_id = 'PTSD'
         UNION ALL
         SELECT A.equip_no 
               ,A.pick_id
               ,TRIM(A.equip_grp_id)   AS equip_grp_id
               ,A.prnt_plnt_no
               ,c.rgn_nam        AS region_nam
               ,A.maint_zone_nam
               ,NULL          reinf_cde
               ,EXTRACT (YEAR FROM b.instln_dt) AS instln_year
               ,NULL          wood_type
               ,NULL          recl_phases
               ,A.fire_risk_zone_cls
               ,NULL          pole_diameter_mm
               ,NULL          good_wood_reading_1m_agl   
               ,NULL          good_wood_reading_100_agl_mm
               ,NULL          good_wood_reading_100_bgl_mm
               ,NULL          si_at_gl
               ,NULL          si_at_1m_agl
               ,NULL          pole_complexity
               ,NULL          dstr_size
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN c.swsect_net_nid
                  ELSE c.swsect_upstrm_prdev_id
               END                                    AS prot_zone_id
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN 'DSTR'
                  ELSE c.swsect_upstrm_prdev_typ
               END                                    AS prot_zone_typ
               ,CASE c.swsect_hv_fdr_id_no   
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_hv_fdr_id_no
               END                                 AS feeder_id 
               ,CASE c.swsect_nsw_nid
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_nsw_nid
               END                                 AS ss_id
               ,CASE c.swsect_hv_fdr_nam   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_hv_fdr_nam
               END                                 AS feeder_nam
               ,CASE c.swsect_zone_substn_desc   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_zone_substn_desc
               END                                 AS zone_subst_nam 
               ,NULL                      AS manuf
               ,NULL                      AS stock_cde         
               ,NULL                      AS ctl_typ
               ,NULL                      AS dev_typ         
               ,NULL                      AS manuf_and_mdl
               ,NULL                      AS expulsion_typ
               ,CASE c.swsect_net_nid
                  WHEN -8888              THEN  NULL
                  ELSE                          c.swsect_net_nid
               END                        AS swsect_net_nid
               ,c.max_safety_zone_rtg
               ,CASE
                  WHEN c.ss_prdev_ds_lv_mtr_cnt = 0 AND c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN
                     GREATEST(
                     (SELECT ss_prdev_ds_lv_mtr_cnt 
                     FROM STRUCTOOLSW.TOPO_GEOG_MV
                     WHERE plnt_no = 'N'||c.swsect_net_nid), c.ss_ds_lv_mtr_cnt)
                  WHEN c.ss_prdev_ds_lv_mtr_cnt < c.ss_ds_lv_mtr_cnt                                  THEN  c.ss_ds_lv_mtr_cnt
                  WHEN c.ss_prdev_ds_lv_mtr_cnt IS NULL OR c.ss_prdev_ds_lv_mtr_cnt = -8888           THEN  NULL
                  ELSE                                                                                      c.ss_prdev_ds_lv_mtr_cnt
               END   AS cust_ds_cnt
         FROM eqp_dim A INNER JOIN
            structoolsw.NAMEPLATE_WF_MV b
         ON A.equip_no = b.equip_no   
                                       INNER JOIN
           structoolsw.TOPO_GEOG_MV         c
         ON A.equip_no = c.equip_no   
         WHERE A.equip_cls = 'WF'
         AND A.equip_grp_id IN ('CAPB','REAC')
         UNION ALL
         SELECT A.equip_no 
               ,A.pick_id
               ,TRIM(A.equip_grp_id)   AS equip_grp_id
               ,A.prnt_plnt_no
               ,c.rgn_nam        AS region_nam
               ,A.maint_zone_nam
               ,NULL          reinf_cde
               ,EXTRACT (YEAR FROM b.instln_dt) AS instln_year
               ,NULL          wood_type
               ,NULL          recl_phases
               ,A.fire_risk_zone_cls
               ,NULL          pole_diameter_mm
               ,NULL          good_wood_reading_1m_agl   
               ,NULL          good_wood_reading_100_agl_mm
               ,NULL          good_wood_reading_100_bgl_mm
               ,NULL          si_at_gl
               ,NULL          si_at_1m_agl
               ,NULL          pole_complexity
               ,NULL          dstr_size
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN c.swsect_net_nid
                  ELSE c.swsect_upstrm_prdev_id
               END                                    AS prot_zone_id
               ,CASE
                  WHEN c.swsect_upstrm_prdev_typ = '#NA#'                       THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'HV' THEN NULL
                  WHEN c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN 'DSTR'
                  ELSE c.swsect_upstrm_prdev_typ
               END                                    AS prot_zone_typ
               ,CASE c.swsect_hv_fdr_id_no   
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_hv_fdr_id_no
               END                                 AS feeder_id 
               ,CASE c.swsect_nsw_nid 
                  WHEN -8888           THEN NULL
                  ELSE                      c.swsect_nsw_nid
               END                                 AS ss_id
               ,CASE c.swsect_hv_fdr_nam   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_hv_fdr_nam
               END                                 AS feeder_nam
               ,CASE c.swsect_zone_substn_desc   
                  WHEN '#NA#'           THEN NULL
                  ELSE                      c.swsect_zone_substn_desc
               END                                 AS zone_subst_nam 
               ,NULL                      AS manuf
               ,NULL                      AS stock_cde         
               ,NULL                      AS ctl_typ
               ,NULL                      AS dev_typ  
               ,CASE b.manuf_and_mdl
                  WHEN ' '                THEN NULL
                  WHEN '#NA#'             THEN NULL
                  ELSE                    UPPER(b.manuf_and_mdl)
               END                                 AS manuf_and_mdl 
               ,NULL                      AS expulsion_typ
               ,CASE c.swsect_net_nid
                  WHEN -8888              THEN  NULL
                  ELSE                          c.swsect_net_nid
               END                        AS swsect_net_nid
               ,c.max_safety_zone_rtg
               ,CASE
                  WHEN c.ss_prdev_ds_lv_mtr_cnt = 0 AND c.swsect_upstrm_prdev_id = 0 AND c.swsect_net_typ = 'LV' THEN
                     GREATEST(
                     (SELECT ss_prdev_ds_lv_mtr_cnt 
                     FROM STRUCTOOLSW.TOPO_GEOG_MV
                     WHERE plnt_no = 'N'||c.swsect_net_nid), c.ss_ds_lv_mtr_cnt)
                  WHEN c.ss_prdev_ds_lv_mtr_cnt < c.ss_ds_lv_mtr_cnt                                  THEN  c.ss_ds_lv_mtr_cnt
                  WHEN c.ss_prdev_ds_lv_mtr_cnt IS NULL OR c.ss_prdev_ds_lv_mtr_cnt = -8888           THEN  NULL
                  ELSE                                                                                      c.ss_prdev_ds_lv_mtr_cnt
               END   AS cust_ds_cnt
         FROM eqp_dim A INNER JOIN
            structoolsw.NAMEPLATE_WO_MV b
         ON A.equip_no = b.equip_no   
                                       INNER JOIN
           structoolsw.TOPO_GEOG_MV         c
         ON A.equip_no = c.equip_no   
         WHERE A.equip_cls = 'WO'
         AND A.equip_grp_id = 'RGTR'
         )
         SELECT DISTINCT 
                A.PICK_ID
               ,A.EQUIP_GRP_ID
               ,trim(A.prnt_plnt_no) AS prnt_plnt_no
               ,A.REGION_NAM
               ,A.MAINT_ZONE_NAM
               ,A.REINF_CDE
               ,NVL(A.INSTLN_YEAR, 1901) AS instln_year
               ,A.WOOD_TYPE
               ,A.RECL_PHASES
               ,CASE
               	WHEN A.fire_risk_zone_cls IN ('X','H','M','L')				THEN	A.fire_risk_zone_cls
                  ELSE																										NULL
               END															AS fire_risk_zone_cls
               ,A.POLE_DIAMETER_MM
               ,A.POLE_COMPLEXITY
               ,A.DSTR_SIZE
               ,A.PROT_ZONE_ID
               ,A.PROT_ZONE_TYP
               ,A.FEEDER_ID
               ,A.SS_ID
               ,A.FEEDER_NAM
               ,A.ZONE_SUBST_NAM
               ,A.MANUF
               ,A.STOCK_CDE
               ,A.CTL_TYP
               ,A.DEV_TYP
               ,A.MANUF_AND_MDL
               ,A.EXPULSION_TYP
               ,A.swsect_net_nid
               ,A.max_safety_zone_rtg
               ,' '       AS part_cde
               ,NULL       AS part_defect_hier
               ,A.equip_no
               ,A.good_wood_reading_100_agl_mm
               ,A.good_wood_reading_100_bgl_mm
               ,A.good_wood_reading_1m_agl   
               ,A.si_at_1m_agl
               ,A.si_at_gl
               ,A.cust_ds_cnt
               ,A.urbn_inspn_classfn
               ,A.reinf_cde_dw
               ,labour_hrs
               ,NULL						AS part_defective
               ,A.cca_treated
         FROM poles        A
         WHERE trim(prnt_plnt_no) IS NOT NULL
         UNION ALL
         SELECT DISTINCT 
                A.PICK_ID
               ,A.EQUIP_GRP_ID
               ,trim(A.prnt_plnt_no) AS prnt_plnt_no
               ,A.REGION_NAM
               ,A.MAINT_ZONE_NAM
               ,NULL AS REINF_CDE
               ,NVL(A.INSTLN_YEAR, 1901) AS instln_year
               ,A.WOOD_TYPE
               ,A.RECL_PHASES
               ,CASE
               	WHEN A.fire_risk_zone_cls IN ('X','H','M','L')				THEN	A.fire_risk_zone_cls
                  ELSE																										NULL
               END															AS fire_risk_zone_cls
               ,A.POLE_DIAMETER_MM
               ,A.POLE_COMPLEXITY
               ,A.DSTR_SIZE
               ,A.PROT_ZONE_ID
               ,A.PROT_ZONE_TYP
               ,A.FEEDER_ID
               ,A.SS_ID
               ,A.FEEDER_NAM
               ,A.ZONE_SUBST_NAM
               ,A.MANUF
               ,A.STOCK_CDE
               ,A.CTL_TYP
               ,A.DEV_TYP
               ,A.MANUF_AND_MDL
               ,A.EXPULSION_TYP
               ,A.swsect_net_nid
               ,A.max_safety_zone_rtg
               ,' '       AS part_cde
               ,NULL       AS part_defect_hier
               ,A.equip_no
               ,A.good_wood_reading_100_agl_mm
               ,A.good_wood_reading_100_bgl_mm
               ,A.good_wood_reading_1m_agl   
               ,A.si_at_1m_agl
               ,A.si_at_gl
               ,A.cust_ds_cnt
               ,NULL						AS urbn_inspn_classfn
               ,NULL						AS reinf_cde_dw
               ,NULL						AS labour_hrs
               ,NULL						AS part_defective
               ,NULL						AS cca_treated
         FROM eqp      A
         WHERE trim(prnt_plnt_no) IS NOT NULL
         UNION ALL
         SELECT DISTINCT 
                A.PICK_ID
               ,A.EQUIP_GRP_ID
					,trim(A.prnt_plnt_no) AS prnt_plnt_no
               ,A.REGION_NAM
               ,A.MAINT_ZONE_NAM
               ,NULL AS REINF_CDE
               ,NVL(A.INSTLN_YEAR, 1901) AS instln_year
               ,A.WOOD_TYPE
               ,A.RECL_PHASES
               ,CASE
               	WHEN A.fire_risk_zone_cls IN ('X','H','M','L')				THEN	A.fire_risk_zone_cls
                  ELSE																										NULL
               END															AS fire_risk_zone_cls
               ,A.POLE_DIAMETER_MM
               ,A.POLE_COMPLEXITY
               ,A.DSTR_SIZE
               ,A.PROT_ZONE_ID
               ,A.PROT_ZONE_TYP
               ,A.FEEDER_ID
               ,A.SS_ID
               ,A.FEEDER_NAM
               ,A.ZONE_SUBST_NAM
               ,A.MANUF
               ,A.STOCK_CDE
               ,A.CTL_TYP
               ,A.DEV_TYP
               ,A.MANUF_AND_MDL
               ,A.EXPULSION_TYP
               ,A.swsect_net_nid
               ,A.max_safety_zone_rtg
               ,A.part_cde
--               ,A.xarm_stay_defect_hier AS part_defect_hier
               ,NULL 										AS part_defect_hier
               ,NULL AS equip_no
               ,NULL AS good_wood_reading_100_agl_mm
               ,NULL AS good_wood_reading_100_bgl_mm
               ,NULL AS good_wood_reading_1m_agl   
               ,NULL AS si_at_1m_agl
               ,NULL AS si_at_gl
               ,A.cust_ds_cnt
               ,NULL						AS urbn_inspn_classfn
               ,NULL						AS reinf_cde_dw
               ,NULL						AS labour_hrs
               ,part_defective
               ,NULL						AS cca_treated
         FROM xarms_and_stays     A
         WHERE trim(prnt_plnt_no) IS NOT NULL
         ;

         COMMIT;

			structools.schema_utils.enable_table_indexes('ST_EQUIPMENT');
         structools.schema_utils.anal_table('ST_EQUIPMENT');
         
         FOR c IN last_insp_csr LOOP	
         	UPDATE structools.st_equipment
            set last_full_insp_dt = c.last_full_insp_dt
            	, fisc_prd_nam = c.last_full_insp_fy
            WHERE pick_id = c.plnt_no
            AND part_cde = ' ';
         END LOOP;
         
         COMMIT;

         dbms_output.put_line('create_st_equipment end. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

      END create_st_equipment;

      PROCEDURE create_st_mtzn IS
      
           
         --INSERT /*+ APPEND */ INTO structools.st_mtzn
         
         CURSOR C_ST_MTZN_CUR IS
         WITH eqp_counts AS
         (    
         SELECT mtzn
               ,region_nam
               ,SUM(cnt) AS eqp_cnt
         FROM      
         (
         SELECT         maint_zone_nam AS mtzn
                        ,region_nam
                        ,COUNT(*)   AS cnt
         FROM structools.st_equipment
         WHERE region_nam IS NOT NULL
         AND region_nam > ' '
         AND region_nam != '#NA#'
         AND region_nam != 'UNKNOWN'
         AND part_cde = ' '
         GROUP BY maint_zone_nam, region_nam               
         UNION ALL
         SELECT          mtzn
                        ,region_nam
                        ,COUNT(*)   AS cnt
         FROM structools.st_bay_equipment
         WHERE region_nam IS NOT NULL
         AND region_nam > ' '
         AND region_nam != '#NA#'
         AND region_nam != 'UNKNOWN'GROUP BY mtzn, region_nam
         )
         GROUP BY mtzn, region_nam
         )
         ,
         mtzn_max_cnt AS
         (
         SELECT mtzn
               ,MAX(eqp_cnt) max_mtzn_cnt
         FROM eqp_counts
         GROUP BY mtzn
         )
         ,
         mtzn_tmp AS
         (
         SELECT A.mtzn
               ,b.region_nam
               ,REPLACE(REPLACE(A.mtzn,'/','_'),'-','_') AS mtzn_opt
         FROM mtzn_max_cnt    A  INNER JOIN
              eqp_counts          b
         ON A.mtzn = b.mtzn
         WHERE b.eqp_cnt = A.max_mtzn_cnt
         )
         ,
         eqp_fr_cnt AS
         (
         SELECT maint_zone_nam   AS mtzn
               ,fire_risk_zone_cls       
               ,COUNT(*) AS fr_zone_cnt
         FROM structools.st_equipment
         WHERE part_cde = ' '
         GROUP BY maint_zone_nam, fire_risk_zone_cls
         )
         ,
         eqp_fr_cnt_max AS
         (
         SELECT mtzn
               ,MAX(fr_zone_cnt) AS max_fr_eqp
         FROM eqp_fr_cnt   
         GROUP BY mtzn
         )
         ,
         mtzn_fr AS
         (
         SELECT A.mtzn
               ,A.fire_risk_zone_cls
         FROM eqp_fr_cnt      A  INNER JOIN
              eqp_fr_cnt_max  b
         ON A.mtzn = b.mtzn       
         WHERE A.fr_zone_cnt = b.max_fr_eqp  
         )
         ,
         eqp_ps_cnt AS
         (
         SELECT maint_zone_nam   AS mtzn
               ,max_safety_zone_rtg       
               ,COUNT(*) AS ps_zone_cnt
         FROM structools.st_equipment
         WHERE part_cde = ' '
         GROUP BY maint_zone_nam, max_safety_zone_rtg
         )
         ,
         eqp_ps_cnt_max AS
         (
         SELECT mtzn
               ,MAX(ps_zone_cnt) AS max_ps_eqp
         FROM eqp_ps_cnt   
         GROUP BY mtzn
         )
         ,
         mtzn_ps_temp1 AS
         (
         SELECT A.mtzn
               ,A.max_safety_zone_rtg
               ,CASE A.max_safety_zone_rtg
               	WHEN 'VERY HIGH'				THEN 1
                  WHEN 'HIGH'							THEN 2
                  WHEN 'MEDIUM'					THEN 3
                  WHEN 'LOW'							THEN 4
                  WHEN 'VERY LOW'				THEN 5
                  ELSE											99
               END			AS ps_rank
         FROM eqp_ps_cnt      A  INNER JOIN
              eqp_ps_cnt_max  b
         ON A.mtzn = b.mtzn       
         WHERE A.ps_zone_cnt = b.max_ps_eqp
         )
         ,
         mtzn_ps_temp2 AS
         (
         SELECT mtzn
         				,min(ps_rank) AS ps_rank
			FROM  mtzn_ps_temp1
         GROUP BY mtzn               
         )
         ,
         mtzn_ps AS      
         (
         SELECT A.mtzn
         				,b.max_safety_zone_rtg
			FROM mtzn_ps_temp2			A			INNER JOIN
         			mtzn_ps_temp1			b
			ON A.mtzn = b.mtzn
         WHERE b.ps_rank = A.ps_rank
         )
         SELECT A.mtzn
               ,A.region_nam
               ,A.mtzn_opt
               --,NULL            --area_nam
              -- ,NULL             --depot_nam
               ,CASE b.fire_risk_zone_cls
                  WHEN 'UNKNOWN'    THEN  NULL
                  ELSE                    b.fire_risk_zone_cls
               END                  AS fire_risk_zone_cls
               ,CASE c.max_safety_zone_rtg
                  WHEN 'VERY LOW'		THEN  c.max_safety_zone_rtg
                  WHEN 'LOW'					THEN  c.max_safety_zone_rtg
                  WHEN 'MEDIUM'			THEN  c.max_safety_zone_rtg
                  WHEN 'HIGH'					THEN  c.max_safety_zone_rtg
                  WHEN 'VERY HIGH'    THEN  c.max_safety_zone_rtg
                  ELSE                    NULL
               END                  AS pub_safety_zone_cls
         FROM mtzn_tmp  A  INNER JOIN
              mtzn_fr   b
         ON A.mtzn = b.mtzn
															INNER JOIN
              mtzn_ps   c
         ON A.mtzn = c.mtzn         
         ORDER BY A.mtzn;
         
         V_CNT  NUMBER := 0;
         v_ins_cnt  NUMBER := 0;
         v_upd_cnt  NUMBER := 0;
        
       BEGIN
       
         dbms_output.put_line('create_st_mtzn start. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

         structools.schema_utils.disable_table_indexes('ST_MTZN');
                
         UPDATE structools.st_mtzn
         set CURRENT_IND = 'N';
                
         FOR ST_MTZN_REC  IN C_ST_MTZN_CUR LOOP
                
            SELECT COUNT(*) INTO V_CNT FROM structools.st_mtzn
            WHERE mtzn = ST_MTZN_REC.mtzn;
                     
            IF V_CNT = 0 THEN
                       
               INSERT INTO structools.st_mtzn
               (
               mtzn
               ,region_nam
               ,mtzn_opt
               ,area_nam
               ,depot_nam
               ,fire_risk_zone_cls
               ,current_ind
               ,pub_safety_zone_cls
               )
               VALUES
               (ST_MTZN_REC.mtzn
                  ,ST_MTZN_REC.region_nam
                  ,ST_MTZN_REC.mtzn_opt
                  ,NULL             --area_nam
                  ,NULL    
                  ,ST_MTZN_REC.fire_risk_zone_cls 
                  ,'Y'
                  ,ST_MTZN_REC.pub_safety_zone_cls 
               );
               v_ins_cnt := v_ins_cnt + 1;
                     
            ELSE
                     
               UPDATE structools.st_mtzn
               set region_nam = ST_MTZN_REC.region_nam,
               mtzn_opt = ST_MTZN_REC.mtzn_opt,
               fire_risk_zone_cls = ST_MTZN_REC.fire_risk_zone_cls,
               CURRENT_IND = 'Y',
               pub_safety_zone_cls = ST_MTZN_REC.pub_safety_zone_cls
               WHERE mtzn = ST_MTZN_REC.mtzn;
                                  
               v_upd_cnt := v_upd_cnt + SQL%ROWCOUNT;
                     
            END IF;
                
         END LOOP;
                  
         structools.schema_utils.enable_table_indexes('ST_MTZN');
         structools.schema_utils.anal_table('ST_MTZN');
                
         COMMIT;
                
         dbms_output.put_line('create_st_mtzn END. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      
                
      EXCEPTION
                
         WHEN OTHERS THEN
          dbms_output.put_line('ERROR WITH create_st_mtzn'||SUBSTR(SQLERRM, 1, 1000));
          RAISE;

      END create_st_mtzn;
      
      PROCEDURE create_st_span IS
      
      BEGIN
      
         dbms_output.put_line('create_st_span start. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

         structools.schema_utils.truncate_table('ST_SPAN');
         structools.schema_utils.disable_table_indexes('ST_SPAN');

         INSERT /*+ APPEND */ INTO structools.st_span
         WITH bays1 AS
         (
         SELECT A.pick_id               
               ,A.stc1
               ,A.stc2
               ,A.bay_len_m  
               ,A.bay_equip_cde           AS equip_cde             
               ,A.mtzn 
               ,b.area_cat_desc            AS area_type
         FROM   structools.st_bay_equipment A         LEFT OUTER JOIN     
                structoolsw.asset_geography    b
         ON    A.pick_id = b.pick_id   
         )
         ,
         bays2 AS
         (
         SELECT DISTINCT
                stc1
               ,stc2 
               ,MAX(bay_len_m) OVER (PARTITION BY stc1, stc2) AS span_len
               ,MIN(mtzn) OVER (PARTITION BY stc1, stc2) AS mtzn
               ,MIN(area_type)  OVER (PARTITION BY stc1, stc2) AS area_type
               ,equip_cde
               ,COUNT(*) OVER (PARTITION BY stc1, stc2, equip_cde) AS eqp_cde_cnt
         FROM bays1                    
         )
         SELECT DISTINCT stc1
                        ,stc2
                        ,mtzn
                        ,NVL(area_type, 'RURAL') AS area_type
                        ,ROUND(span_len,2) AS span_len
                        ,NVL(lvco_cnt,0)  AS lvco_cnt
                        ,NVL(hvco_cnt,0) AS hvco_cnt
                        ,NVL(hvsp_cnt,0) AS HVSP_CNT  
         FROM 
            (
            SELECT *
            FROM bays2
            PIVOT (
            MAX(eqp_cde_cnt) cnt
            FOR equip_cde IN ('LVCO' lvco , 'HVCO' hvco , 'HVSP' hvsp ))
            )
         ;
         
         structools.schema_utils.enable_table_indexes('ST_SPAN');
         structools.schema_utils.anal_table('ST_SPAN');
         
         COMMIT;
                  
         dbms_output.put_line('create_st_span END. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

      END create_st_span;
      
      PROCEDURE create_st_prot_zone IS
      
      BEGIN
      
         dbms_output.put_line('create_st_prot_zone start. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

         structools.schema_utils.truncate_table('ST_PROT_ZONE');
         structools.schema_utils.disable_table_indexes('ST_PROT_ZONE');

         INSERT /*+ APPEND */ INTO structools.st_prot_zone
         SELECT aa.prot_zone_id
               ,MIN(bb.mtzn)  AS mtzn
         FROM
         (      
         SELECT prot_zone_id
               ,MAX(mtzn_cnt) max_mtzn_cnt
         FROM structools.st_bay_equipment  
         GROUP BY prot_zone_id    
         ) aa           INNER JOIN
         (
         SELECT DISTINCT prot_zone_id
               ,mtzn
               ,mtzn_cnt
         FROM structools.st_bay_equipment  
         ) bb
         ON aa.prot_zone_id = bb.prot_zone_id
         AND aa.max_mtzn_cnt = bb.mtzn_cnt
         GROUP BY aa.prot_zone_id
         ;
         
         structools.schema_utils.enable_table_indexes('ST_PROT_ZONE');
         structools.schema_utils.anal_table('ST_PROT_ZONE');
         
         COMMIT;
         
         dbms_output.put_line('create_st_prot_zone END. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

      END create_st_prot_zone;

      PROCEDURE create_st_carrier_detail IS
      
      BEGIN
      
         dbms_output.put_line('create_st_carrier_detail start. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

         structools.schema_utils.truncate_table('ST_CARRIER_DETAIL');
         structools.schema_utils.disable_table_indexes('ST_CARRIER_DETAIL');

         INSERT  INTO structools.st_carrier_detail
         WITH field_conductor_upload AS
         (
         SELECT 
         DISTINCT conductor_pick_id
         			,struc_carrier_model_code  
         FROM structoolsw.conductor_field_cond_upload
         WHERE NVL(TRIM(conductor_pick_id), ' ') <> ' '
         )
         ,
         joined AS
         (
         SELECT A.carrier_pick_id
         				,A.carrier_mdl_cde
                     ,b.conductor_pick_id
                     ,b.struc_carrier_model_code
			FROM    structools.st_carr_mdl_update_user			A	FULL OUTER JOIN
                      field_conductor_upload									b
			ON A.carrier_pick_id = b.conductor_pick_id           
         )
         SELECT nvl(carrier_pick_id, conductor_pick_id) 
         			  ,nvl(carrier_mdl_cde, struc_carrier_model_code)
                    ,NULL              
			FROM joined                    
         ;
         
         structools.schema_utils.enable_table_indexes('ST_CARRIER_DETAIL');
         structools.schema_utils.anal_table('ST_CARRIER_DETAIL');
         
         COMMIT;
         
         dbms_output.put_line('create_st_carrier_detail END. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

      EXCEPTION
         WHEN OTHERS THEN 
            dbms_output.put_line('ERROR WITH create_st_carrier_detail'||SUBSTR(SQLERRM, 1, 1000));
            RAISE;

      END create_st_carrier_detail;

      PROCEDURE create_st_carr_mdl_material IS
      
      BEGIN
      
         structools.schema_utils.truncate_table('ST_CARR_MDL_MATERIAL');
         structools.schema_utils.disable_table_indexes('ST_CARR_MDL_MATERIAL');

         INSERT  INTO structools.st_carr_mdl_material
         SELECT carr_mdl_cde
               ,phs_num_of_conductors        --num_phases
               ,phs_condr_no_of_strnds        --num_strands
               ,phs_condr_wire_diam_mm        --wire_diam
               ,phs_condr_matrl_typ           --material_cde
               ,NULL                          --size rating
               ,NULL                          --intr_yr
               ,NULL                          --phase_out_yr
               ,CASE
                        WHEN  phs_condr_matrl_typ = 'AAAC'           THEN  'AAAC'
                        WHEN  phs_condr_matrl_typ = 'AAC'            THEN  'AAC'
                        WHEN  phs_condr_matrl_typ = 'AL'             THEN  'ALUMINIUM'
                        WHEN  phs_condr_matrl_typ LIKE 'ACSR%'       THEN  'ACSR'
                        WHEN  phs_condr_matrl_typ = 'CU'             THEN  'COPPER'
                        WHEN  phs_condr_matrl_typ = 'COVERED/CU'  THEN  'COPPER'
                        WHEN  phs_condr_matrl_typ = 'COPPER'         THEN  'COPPER'
                        WHEN  phs_condr_matrl_typ LIKE '%HENDRIX%'   THEN  'HENDRIX'
                        WHEN  phs_condr_matrl_typ LIKE '22KV ABC%'   THEN  'HVABC'
                        WHEN  phs_condr_matrl_typ LIKE 'HVABC%'          THEN  'HVABC'
                        WHEN  phs_condr_matrl_typ = 'LV ABC'         THEN  'LVABC'
                        WHEN  phs_condr_matrl_typ LIKE 'LVABC%'          THEN  'LVABC'
                        WHEN  phs_condr_matrl_typ = 'SCAC'           THEN  'STEEL'
                        WHEN  phs_condr_matrl_typ = 'SCGZ'           THEN  'STEEL'
                        WHEN  phs_condr_matrl_typ = 'CU PVC BLACK'   THEN  'OTHER'
                        WHEN  phs_condr_matrl_typ = 'PVC'            THEN  'OTHER'
                        WHEN  phs_condr_matrl_typ = 'SERVICE'        THEN  'OTHER'
                        WHEN  phs_condr_matrl_typ LIKE 'SRVC%'   THEN  'OTHER'
                        WHEN  phs_condr_matrl_typ = 'UNK'            THEN  'OTHER'
                        WHEN  phs_condr_matrl_typ = 'XLPE'           THEN  'OTHER'
                        WHEN  phs_condr_matrl_typ = 'dead 33kv'      THEN  'OTHER'
                        WHEN  phs_condr_matrl_typ = 'GZAC'           THEN  'GZAC'
                        WHEN  phs_condr_matrl_typ IS NULL            THEN  NULL
                        ELSE                                               '????'
               END            AS          material_cde_common
               ,phs_condr_overall_diam_mm           AS cond_diam		-- In DW this is sometimes null for a couple or carr models, so check and update manually based on other carr models with the same attributes.
         FROM structoolsw.carrier_model_attribute_dim							-- Also, check if the mapping to material type common is exhaustive.
         WHERE curr_flg = 'Y'
         ;

         structools.schema_utils.enable_table_indexes('ST_CARR_MDL_MATERIAL');
         structools.schema_utils.anal_table('ST_CARR_MDL_MATERIAL');
         
         dbms_output.put_line('Records inserted INTO ST_CARR_MDL_MATERIAL'||SQL%ROWCOUNT);

         COMMIT;
         
      EXCEPTION
         WHEN OTHERS THEN 
            dbms_output.put_line('ERROR WITH create_st_carr_mdl_material'||SUBSTR(SQLERRM, 1, 1000));
            RAISE;

      END create_st_carr_mdl_material;

      PROCEDURE create_st_hv_feeder IS
      
      BEGIN
      
         dbms_output.put_line('create_st_hv_feeder start. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

         structools.schema_utils.truncate_table('ST_HV_FEEDER');
         structools.schema_utils.disable_table_indexes('ST_HV_FEEDER');

         INSERT  INTO structools.st_hv_feeder
         SELECT hv_fdr_id
               ,hv_fdr_nam
               ,scnrrr_fdr_cat_nam
         FROM structoolsw.hv_feeder_name_dim
         WHERE SYSDATE BETWEEN efec_strt_dt AND efec_end_dt
         AND del_flg = 'N'
         AND curr_flg = 'Y'
         ;
         
         structools.schema_utils.enable_table_indexes('ST_HV_FEEDER');
         structools.schema_utils.anal_table('ST_HV_FEEDER');
         
         COMMIT;
         
         dbms_output.put_line('create_st_hv_feeder END. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

      EXCEPTION
         WHEN OTHERS THEN 
            dbms_output.put_line('ERROR WITH create_st_hv_feeder'||SUBSTR(SQLERRM, 1, 1000));
            RAISE;

      END create_st_hv_feeder;

      PROCEDURE create_st_carr_mdl_si_factor IS
      
      BEGIN
      
         dbms_output.put_line('create_st_carr_mdl_si_factor start. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

         structools.schema_utils.truncate_table('ST_CARR_MDL_SI_FACTOR');
         structools.schema_utils.disable_table_indexes('ST_CARR_MDL_SI_FACTOR');

         INSERT INTO structools.st_carr_mdl_si_factor
         SELECT carrier_mdl_cde
               ,bay_cnt
               ,si_factor_gl
               ,si_factor_1m_agl
               ,si_factor_avg
         FROM
         (      
         SELECT A.carrier_mdl_cde
               --,A.carrier_mdl_map
               ,b.carrier_mdl_map
               ,b.bay_cnt
               ,b.si_factor_gl
               ,b.si_factor_1m_agl
               ,b.si_factor_avg
         FROM  structools.st_si_temp_carr_mdl_map         A  LEFT OUTER JOIN
               structools.st_si_temp_values              b
         ON A.carrier_mdl_map = b.carrier_mdl_map   
         )
         WHERE  carrier_mdl_map IS NOT NULL         
         ;
         
         structools.schema_utils.enable_table_indexes('ST_CARR_MDL_SI_FACTOR');
         structools.schema_utils.anal_table('ST_CARR_MDL_SI_FACTOR');
         
         COMMIT;
         
         dbms_output.put_line('create_st_carr_mdl_si_factor END. '||TO_CHAR(SYSDATE,'hh24:mi:ss'));      

      EXCEPTION
         WHEN OTHERS THEN 
            dbms_output.put_line('ERROR WITH create_st_carr_mdl_si_factor'||SUBSTR(SQLERRM, 1, 1000));
            RAISE;

      END create_st_carr_mdl_si_factor;

   BEGIN

      IF UPPER(i_table_name) = 'ST_BAY_DEFECT' THEN
         create_st_bay_defect;
      ELSIF UPPER(i_table_name) = 'ST_BAY_EQUIPMENT' THEN
         create_st_bay_equipment(i_segment_type);
      ELSIF UPPER(i_table_name) = 'ST_EQP_DEFECT' THEN
         create_st_eqp_defect;
      ELSIF UPPER(i_table_name) = 'ST_EQP_MTZN' THEN
         create_st_eqp_mtzn;
      ELSIF UPPER(i_table_name) = 'ST_EQUIPMENT' THEN
         create_st_equipment;
      ELSIF UPPER(i_table_name) = 'ST_MTZN' THEN
         create_st_mtzn;
      ELSIF UPPER(i_table_name) = 'ST_SPAN' THEN
         create_st_span;
      ELSIF UPPER(i_table_name) = 'ST_PROT_ZONE' THEN
         create_st_prot_zone;
      ELSIF UPPER(i_table_name) = 'ST_CARRIER_DETAIL' THEN
         create_st_carrier_detail;
--      ELSIF UPPER(i_table_name) = 'ST_EQP_TYPE_DEFECT_ACTION' THEN
--         create_st_eqp_type_def_action;
--      ELSIF UPPER(i_table_name) = 'ST_BAY_DEFECT_ACTION' THEN
--         create_st_bay_defect_action;
      ELSIF UPPER(i_table_name) = 'ST_CARR_MDL_MATERIAL' THEN
         create_st_carr_mdl_material;
      ELSIF UPPER(i_table_name) = 'ST_HV_FEEDER' THEN
         create_st_hv_feeder;
      ELSIF UPPER(i_table_name) = 'ST_CARR_MDL_SI_FACTOR' THEN
         create_st_carr_mdl_si_factor;
      END IF;
      
      COMMIT;
      
   END create_st_tables;

--    /* These 2 procs need to be changed to copy/update scenario for a particular parameter (as scenarios are now applicable to individual parameters) 
--    /* Also, the table now contains more columns. 
--   PROCEDURE copy_parameter_scenario(i_scenario_id_from  IN PLS_INTEGER
--                                      ,i_scenario_id_to  IN PLS_INTEGER) IS
--
--      v_scenario_id        PLS_INTEGER;
--
--   BEGIN
--      /* Validate */
--      
--      /* Scenario to copy from must exist */
--      BEGIN
--         SELECT DISTINCT scenario_id
--            INTO v_scenario_id
--            FROM structools.st_parameter_scenario
--           WHERE scenario_id = i_scenario_id_from;
--      EXCEPTION
--         WHEN NO_DATA_FOUND THEN
--            dbms_output.put_line('Scenario TO copy FROM does NOT exist');
--            RAISE v_prog_excp;
--      END;
--
--      /* Scenario to copy to must not exist */
--      BEGIN
--         SELECT DISTINCT scenario_id
--            INTO v_scenario_id
--            FROM structools.st_parameter_scenario
--           WHERE scenario_id = i_scenario_id_to;
--           dbms_output.put_line('Scenario TO copy TO already EXISTS');
--           RAISE v_prog_excp;
--      EXCEPTION
--         WHEN NO_DATA_FOUND THEN
--            NULL;
--      END;
--      
--      /* Copy existing scenario_from to scenario_to */
--      INSERT INTO structools.st_parameter_scenario
--      SELECT parameter_id
--            ,parameter_egi
--            ,i_scenario_id_to
--            ,parameter_val
--            ,parameter_age
--            ,parameter_risk_comp
--            ,parameter_wood_type
--            ,parameter_maint_cost_type
--            ,parameter_fdr_cat
--      FROM structools.st_parameter_scenario
--      WHERE scenario_id = i_scenario_id_from;
--
--      COMMIT;                  
--   
--   EXCEPTION
--      WHEN v_prog_excp THEN
--         RAISE;
--      WHEN OTHERS THEN
--         dbms_output.put_line(TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss')||' EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
--         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
--         RAISE;
--   
--   END copy_parameter_scenario;
--
--   PROCEDURE update_parameter_scenario(i_update_row      IN structools.st_parameter_scenario%ROWTYPE) IS 
--
--      v_scenario_id        PLS_INTEGER;
--      v_scenario_rec       structools.st_parameter_scenario%ROWTYPE;
--
--   BEGIN
--      /* Validate */
--      
--      /* Scenario must exist */
--      BEGIN
--         SELECT DISTINCT scenario_id
--            INTO v_scenario_id
--            FROM structools.st_parameter_scenario
--           WHERE scenario_id = i_update_row.scenario_id;
--      EXCEPTION
--         WHEN NO_DATA_FOUND THEN
--            dbms_output.put_line('Scenario TO copy FROM does NOT exist');
--            RAISE v_prog_excp;
--      END;
--
--      /* Required parameter must exist */
--      BEGIN
--         SELECT parameter_id
--               ,parameter_egi
--               ,scenario_id
--               ,parameter_val
--               ,parameter_age
--               ,parameter_risk_comp
--               ,parameter_wood_type
--               ,parameter_maint_cost_type
--               ,parameter_fdr_cat
--            INTO v_scenario_rec
--            FROM structools.st_parameter_scenario
--           WHERE parameter_id =  i_update_row.parameter_id
--             AND (parameter_egi = i_update_row.parameter_egi OR parameter_egi IS NULL)
--             AND scenario_id = i_update_row.scenario_id
--             AND (parameter_age = i_update_row.parameter_age OR parameter_age IS NULL)
--             AND (parameter_risk_comp = i_update_row.parameter_risk_comp OR parameter_risk_comp IS NULL)
--             AND (parameter_wood_type = i_update_row.parameter_wood_type OR parameter_wood_type IS NULL)
--             AND (parameter_maint_cost_type = i_update_row.parameter_maint_cost_type OR parameter_maint_cost_type IS NULL)
--             AND (parameter_fdr_cat = i_update_row.parameter_fdr_cat OR parameter_fdr_cat IS NULL)
--         ;
--             
--         IF v_scenario_rec.parameter_egi IS NULL AND i_update_row.parameter_egi IS NOT NULL
--         OR v_scenario_rec.parameter_egi IS NOT NULL AND i_update_row.parameter_egi IS NULL
--         OR v_scenario_rec.parameter_age IS NULL AND i_update_row.parameter_age IS NOT NULL
--         OR v_scenario_rec.parameter_age IS NOT NULL AND i_update_row.parameter_age IS NULL
--         OR v_scenario_rec.parameter_risk_comp IS NULL AND i_update_row.parameter_risk_comp IS NOT NULL
--         OR v_scenario_rec.parameter_risk_comp IS NOT NULL AND i_update_row.parameter_risk_comp IS NULL
--         OR v_scenario_rec.parameter_wood_type IS NULL AND i_update_row.parameter_wood_type IS NOT NULL
--         OR v_scenario_rec.parameter_wood_type IS NOT NULL AND i_update_row.parameter_wood_type IS NULL
--         OR v_scenario_rec.parameter_maint_cost_type IS NULL AND i_update_row.parameter_maint_cost_type IS NOT NULL
--         OR v_scenario_rec.parameter_maint_cost_type IS NOT NULL AND i_update_row.parameter_maint_cost_type IS NULL
--         OR v_scenario_rec.parameter_fdr_cat IS NOT NULL AND i_update_row.parameter_fdr_cat IS NULL THEN
--            dbms_output.put_line('parameter does NOT exist(1)');
--            RAISE v_prog_excp;
--         END IF;
--      EXCEPTION
--         WHEN NO_DATA_FOUND THEN
--            dbms_output.put_line('parameter does NOT exist(2)');
--            dbms_output.put_line(i_update_row.parameter_id);
--            dbms_output.put_line(i_update_row.parameter_egi);
--            dbms_output.put_line(i_update_row.scenario_id);
--            dbms_output.put_line(i_update_row.parameter_val);
--            dbms_output.put_line(i_update_row.parameter_age);
--            dbms_output.put_line(i_update_row.parameter_risk_comp);
--            dbms_output.put_line(i_update_row.parameter_wood_type);
--            dbms_output.put_line(i_update_row.parameter_maint_cost_type);
--            dbms_output.put_line(i_update_row.parameter_fdr_cat);
--            RAISE v_prog_excp;
--      END;
--
--      /* Update requested parameter */
--      UPDATE structools.st_parameter_scenario
--         SET parameter_val = i_update_row.parameter_val
--        WHERE parameter_id =  i_update_row.parameter_id
--          AND (parameter_egi = i_update_row.parameter_egi OR parameter_egi IS NULL)
--          AND scenario_id = i_update_row.scenario_id
--          AND (parameter_age = i_update_row.parameter_age OR parameter_age IS NULL)
--          AND (parameter_risk_comp = i_update_row.parameter_risk_comp OR parameter_risk_comp IS NULL)
--          AND (parameter_wood_type = i_update_row.parameter_wood_type OR parameter_wood_type IS NULL)
--          AND (parameter_maint_cost_type = i_update_row.parameter_maint_cost_type OR parameter_maint_cost_type IS NULL)
--          AND (parameter_fdr_cat = i_update_row.parameter_fdr_cat OR parameter_fdr_cat IS NULL);
--
--      COMMIT;                  
--   
--   EXCEPTION
--      WHEN v_prog_excp THEN
--         RAISE;
--      WHEN OTHERS THEN
--         dbms_output.put_line(TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss')||' EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
--         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
--         RAISE;
--   
--   END update_parameter_scenario;

   /**********************************************************************************************************************************************/
   /* Delete results of a strategy run. Deletes all rows related to the run, ie all programs and years (where applicable)                        */
   /* i_del_scope defines tables to be deleted. allowable values are:                                                                            */
   /*     ALL - will delete st_*_stratgey_run* tables and optimisation results tables and final summary tables.                                  */
   /*     OPT - will only delete optimisation results tables and final summary tables.                                                           */
   /*     TAB - will delete individual table from the predefined list of tables.                                                                 */
   /**********************************************************************************************************************************************/
   PROCEDURE delete_run (i_del_scope IN VARCHAR2 
                        ,i_table_nam IN VARCHAR2 DEFAULT NULL
                        ,i_qualifier  IN CHAR
                        ,i_del_run_tab IN del_run_tab_typ) IS
         
      v_del_scope          VARCHAR2(50) := UPPER(i_del_scope);
      v_table_nam          VARCHAR2(50) := UPPER(i_table_nam);
      v_qualifier          CHAR(2)      := UPPER(i_qualifier);

      v_run_id_1  PLS_INTEGER;
      v_run_id_2  PLS_INTEGER;
      v_run_id_3  PLS_INTEGER;
      v_run_id_4  PLS_INTEGER;
      v_run_id_5  PLS_INTEGER;
      
   BEGIN

      IF v_del_scope != 'ALL' AND v_del_scope != 'OPT' AND v_del_scope != 'TAB' THEN
         dbms_output.put_line('Del SCOPE NOT valid');
         RAISE v_prog_excp;
      END IF;
      
      IF v_del_scope = 'TAB' THEN
         IF v_table_nam IS NULL THEN
            dbms_output.put_line('TABLE NAME NOT provided');
            RAISE v_prog_excp;
         END IF;
         IF v_table_nam != 'ST_BAY_STRATEGY_RUN' 
         AND v_table_nam != 'ST_BAY_STRATEGY_RUN_RB'
         AND v_table_nam != 'ST_BAY_STRATEGY_RUN_RB_TMP'
         AND v_table_nam != 'ST_STRATEGY_RUN' 
         AND v_table_nam != 'ST_STRATEGY_RUN_RB'
         AND v_table_nam != 'ST_STRATEGY_RUN_RB2'
         AND v_table_nam != 'ST_STRATEGY_RUN_RB_TMP'
         AND v_table_nam != 'ST_STRATEGY_RUN_SUMMARY'
         AND v_table_nam != 'ST_DEL_PROGRAM_ZBAM_REINF_SUMM'
         AND v_table_nam != 'ST_DEL_PROGRAM_MTZN_OPTIMISED'
         AND v_table_nam != 'ST_DEL_PROGRAM_SNIPER'
         AND v_table_nam != 'ST_DEL_PROGRAM_SEG_SUMMARY'
         AND v_table_nam != 'ST_DEL_PROGRAM_SEG_OPTIMISED'
         AND v_table_nam != 'ST_DEL_PROGRAM_REINF_SUMM'
         AND v_table_nam != 'ST_DEL_PROGRAM_REINF_OPT'
         AND v_table_nam != 'ST_DEL_PROGRAM_SEG_REINF_SUMM'
         AND v_table_nam != 'ST_DEL_PROGRAM_REAC_MAINT'
         AND v_table_nam != 'ST_DEL_PROGRAM_PICKID'
         AND v_table_nam != 'ST_DEL_PROGRAM_AGE_STATS'
         AND v_table_nam != 'ST_DEL_PROGRAM_SEG_DELTA'
         AND v_table_nam != 'ST_DEL_PROGRAM_INCD_FORECAST'
         AND v_table_nam != 'ST_DEL_PROGRAM_RISK' THEN
            dbms_output.put_line('TABLE NAME NOT valid');
            RAISE v_prog_excp;
         END IF;
      ELSE 
         IF v_table_nam IS NOT NULL THEN
            dbms_output.put_line('TABLE NAME must be NULL FOR THE del SCOPE');
            RAISE v_prog_excp;
         END IF;
      END IF;
      
      IF v_qualifier != 'EQ' AND v_qualifier != 'GE' AND v_qualifier != 'IN' THEN
         dbms_output.put_line('Qualifier NOT valid');
         RAISE v_prog_excp;
      END IF;
      
      IF v_qualifier = 'EQ' OR v_qualifier = 'GE' THEN
         IF i_del_run_tab.COUNT > 1 THEN
            dbms_output.put_line('Multiple runs NOT allowed FOR THE qualifier');
            RAISE v_prog_excp;
         END IF;
      ELSIF v_qualifier = 'IN' THEN
         IF i_del_run_tab.COUNT > 5 THEN
            dbms_output.put_line('Up TO 5 runs ONLY allowed');
            RAISE v_prog_excp;
         ELSE
            FOR i IN 1 .. 5 LOOP
               IF i = 1 THEN
                  v_run_id_1 := i_del_run_tab(i);
               ELSIF i = 2 THEN
                  IF i_del_run_tab.COUNT > 1 THEN
                     v_run_id_2 := i_del_run_tab(i);
                  ELSE
                     v_run_id_2 := -1;
                  END IF;
               ELSIF i = 3 THEN
                  IF i_del_run_tab.COUNT > 2 THEN
                     v_run_id_3 := i_del_run_tab(i);
                  ELSE
                     v_run_id_3 := -1;
                  END IF;
               ELSIF i = 4 THEN
                  IF i_del_run_tab.COUNT > 3 THEN
                     v_run_id_4 := i_del_run_tab(i);
                  ELSE
                     v_run_id_4 := -1;
                  END IF;
               ELSIF i = 5 THEN
                  IF i_del_run_tab.COUNT > 4 THEN
                     v_run_id_5 := i_del_run_tab(i);
                  ELSE
                     v_run_id_5 := -1;
                  END IF;
               END IF;
            END LOOP;
            dbms_output.put_line ('v_run_id_1 = '||v_run_id_1);
            dbms_output.put_line ('v_run_id_2 = '||v_run_id_2);
            dbms_output.put_line ('v_run_id_3 = '||v_run_id_3);
            dbms_output.put_line ('v_run_id_4 = '||v_run_id_4);
            dbms_output.put_line ('v_run_id_5 = '||v_run_id_5);
         END IF;
      END IF;
      
      IF v_del_scope = 'ALL' THEN
         structools.schema_utils.disable_table_indexes('ST_BAY_STRATEGY_RUN');
         structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN');
         structools.schema_utils.disable_table_indexes('st_bay_strategy_run_rb');
         structools.schema_utils.disable_table_indexes('st_strategy_run_rb');
         structools.schema_utils.disable_table_indexes('st_strategy_run_rb2');
         structools.schema_utils.disable_table_indexes('st_bay_strategy_run_rb_tmp');
         structools.schema_utils.disable_table_indexes('st_strategy_run_rb_tmp');
         IF v_qualifier = 'EQ' THEN
            dbms_output.put_line ('Deleting ALL TABLES FOR run_id = '||i_del_run_tab(1));
            DELETE FROM structools.ST_BAY_STRATEGY_RUN
            WHERE run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.st_bay_Strategy_run_rb_tmp
            WHERE run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.st_bay_Strategy_run_rb
            WHERE run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_STRATEGY_RUN
            WHERE run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.st_strategy_run_rb_tmp
            WHERE run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.st_strategy_run_rb
            WHERE run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.st_strategy_run_rb2
            WHERE run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_STRATEGY_RUN_SUMMARY
            WHERE run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_ZBAM_REINF_SUMM
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_MTZN_OPTIMISED
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SNIPER
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_SUMMARY
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_OPTIMISED
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_SUMM
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_OPT
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_REINF_SUMM
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REAC_MAINT
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_PICKID
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_AGE_STATS
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_DELTA
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_INCD_FORECAST
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_RISK
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
         ELSIF v_qualifier = 'GE' THEN
            dbms_output.put_line ('Deleting ALL TABLES FOR run_id >= '||i_del_run_tab(1));
            DELETE FROM structools.ST_BAY_STRATEGY_RUN
            WHERE run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.st_bay_Strategy_run_rb_tmp
            WHERE run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.st_bay_Strategy_run_rb
            WHERE run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_STRATEGY_RUN
            WHERE run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.st_strategy_run_rb_tmp
            WHERE run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.st_strategy_run_rb
            WHERE run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.st_strategy_run_rb2
            WHERE run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_STRATEGY_RUN_SUMMARY
            WHERE run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_ZBAM_REINF_SUMM
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_MTZN_OPTIMISED
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SNIPER
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_SUMMARY
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_OPTIMISED
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_SUMM
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_OPT
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_REINF_SUMM
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REAC_MAINT
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_PICKID
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_AGE_STATS
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_DELTA
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_INCD_FORECAST
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_RISK
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
         ELSIF v_qualifier = 'IN' THEN
            dbms_output.put_line ('Deleting ALL TABLES FOR run_id IN  '||v_run_id_1||' '||v_run_id_2||' '||v_run_id_3||' '||v_run_id_4||' '||v_run_id_5);
            DELETE FROM structools.ST_BAY_STRATEGY_RUN
            WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.st_bay_Strategy_run_rb_tmp
            WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.st_bay_Strategy_run_rb
            WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_STRATEGY_RUN
            WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.st_strategy_run_rb_tmp
            WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.st_strategy_run_rb
            WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.st_strategy_run_rb2
            WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_STRATEGY_RUN_SUMMARY
            WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_ZBAM_REINF_SUMM
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_MTZN_OPTIMISED
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SNIPER
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_SUMMARY
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_OPTIMISED
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_SUMM
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_OPT
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_REINF_SUMM
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REAC_MAINT
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_PICKID
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_AGE_STATS
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_DELTA
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_INCD_FORECAST
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_RISK
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
         END IF;
         structools.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN');
         structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN');
         structools.schema_utils.enable_table_indexes('st_bay_strategy_run_rb');
         structools.schema_utils.enable_table_indexes('st_strategy_run_rb');
         structools.schema_utils.enable_table_indexes('st_strategy_run_rb2');
         structools.schema_utils.enable_table_indexes('st_bay_strategy_run_rb_tmp');
         structools.schema_utils.enable_table_indexes('st_strategy_run_rb_tmp');
      ELSIF v_del_scope = 'OPT' THEN
         IF v_qualifier = 'EQ' THEN
            dbms_output.put_line ('Deleting opt TABLES FOR run_id =  '||i_del_run_tab(1));
            DELETE FROM structools.ST_STRATEGY_RUN_SUMMARY
            WHERE run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_ZBAM_REINF_SUMM
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_MTZN_OPTIMISED
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SNIPER
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_SUMMARY
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_OPTIMISED
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_SUMM
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_OPT
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_REINF_SUMM
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REAC_MAINT
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_PICKID
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_AGE_STATS
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_DELTA
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_INCD_FORECAST
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_RISK
            WHERE base_run_id = i_del_run_tab(1);
            COMMIT; 
         ELSIF v_qualifier = 'GE' THEN
            dbms_output.put_line ('Deleting opt TABLES FOR run_id >=  '||i_del_run_tab(1));
            DELETE FROM structools.ST_STRATEGY_RUN_SUMMARY
            WHERE run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_ZBAM_REINF_SUMM
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_MTZN_OPTIMISED
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SNIPER
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_SUMMARY
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_OPTIMISED
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_SUMM
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_OPT
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_REINF_SUMM
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REAC_MAINT
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_PICKID
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_AGE_STATS
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_DELTA
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_INCD_FORECAST
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_RISK
            WHERE base_run_id >= i_del_run_tab(1);
            COMMIT; 
         ELSIF v_qualifier = 'IN' THEN
            dbms_output.put_line ('Deleting OPT TABLES FOR run_id IN  '||v_run_id_1||' '||v_run_id_2||' '||v_run_id_3||' '||v_run_id_4||' '||v_run_id_5);
            DELETE FROM structools.ST_STRATEGY_RUN_SUMMARY
            WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_ZBAM_REINF_SUMM
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_MTZN_OPTIMISED
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SNIPER
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_SUMMARY
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_OPTIMISED
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_SUMM
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_OPT
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_REINF_SUMM
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_REAC_MAINT
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_PICKID
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_AGE_STATS
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_DELTA
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_INCD_FORECAST
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
            DELETE FROM structools.ST_DEL_PROGRAM_RISK
            WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
            COMMIT; 
         END IF;
      ELSIF v_del_scope = 'TAB' THEN
         IF v_qualifier = 'EQ' THEN
            dbms_output.put_line ('Deleting TABLE '||v_table_nam||' FOR run_id IN  '||i_del_run_tab(1));
            IF v_table_nam = 'ST_BAY_STRATEGY_RUN' THEN
               structools.schema_utils.disable_table_indexes('ST_BAY_STRATEGY_RUN');
               DELETE FROM structools.ST_BAY_STRATEGY_RUN
               WHERE run_id = i_del_run_tab(1);
               structools.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN');
               COMMIT;
            ELSIF v_table_nam = 'ST_BAY_STRATEGY_RUN_RB_TMP' THEN
               structools.schema_utils.disable_table_indexes('st_bay_strategy_run_rb_tmp');
               DELETE FROM structools.st_bay_Strategy_run_rb_tmp
               WHERE run_id = i_del_run_tab(1);
               COMMIT;
               structools.schema_utils.enable_table_indexes('st_bay_strategy_run_rb_tmp');
            ELSIF v_table_nam = 'ST_BAY_STRATEGY_RUN_RB' THEN
               structools.schema_utils.disable_table_indexes('st_bay_strategy_run_rb');
               DELETE FROM structools.st_bay_Strategy_run_rb
               WHERE run_id = i_del_run_tab(1);
               COMMIT;
               structools.schema_utils.enable_table_indexes('st_bay_strategy_run_rb');
            ELSIF v_table_nam = 'ST_STRATEGY_RUN' THEN
               structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN');
               DELETE FROM structools.ST_STRATEGY_RUN
               WHERE run_id = i_del_run_tab(1);
               COMMIT;
               structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN');
            ELSIF v_table_nam = 'ST_STRATEGY_RUN_RB_TMP' THEN
               structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN_RB_TMP');
               DELETE FROM structools.st_strategy_run_rb_tmp
               WHERE run_id = i_del_run_tab(1);
               COMMIT;
               structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN_RB_TMP');
            ELSIF v_table_nam = 'ST_STRATEGY_RUN_RB' THEN
               structools.schema_utils.disable_table_indexes('st_strategy_run_rb');
               DELETE FROM structools.st_strategy_run_rb
               WHERE run_id = i_del_run_tab(1);
               structools.schema_utils.enable_table_indexes('st_strategy_run_rb');
               COMMIT;
            ELSIF v_table_nam = 'ST_STRATEGY_RUN_RB2' THEN
               structools.schema_utils.disable_table_indexes('st_strategy_run_rb2');
               DELETE FROM structools.st_strategy_run_rb2
               WHERE run_id = i_del_run_tab(1);
               structools.schema_utils.enable_table_indexes('st_strategy_run_rb2');
               COMMIT;
            ELSIF v_table_nam = 'ST_STRATEGY_RUN_SUMMARY' THEN
               DELETE FROM structools.ST_STRATEGY_RUN_SUMMARY
               WHERE run_id = i_del_run_tab(1);
               COMMIT;
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_ZBAM_REINF_SUMM' THEN
               DELETE FROM structools.ST_DEL_PROGRAM_ZBAM_REINF_SUMM
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT;
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_MTZN_OPTIMISED' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_MTZN_OPTIMISED
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SNIPER' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SNIPER
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_SUMMARY' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SEG_SUMMARY
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_OPTIMISED' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SEG_OPTIMISED
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_REINF_SUMM' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_REINF_SUMM
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_REINF_OPT' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_REINF_OPT
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_REINF_SUMM' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SEG_REINF_SUMM
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_REAC_MAINT' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_REAC_MAINT
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_PICKID' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_PICKID
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_AGE_STATS' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_AGE_STATS
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_DELTA' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SEG_DELTA
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_INCD_FORECAST' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_INCD_FORECAST
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_RISK' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_RISK
               WHERE base_run_id = i_del_run_tab(1);
               COMMIT; 
            END IF;
         ELSIF v_qualifier = 'GE' THEN
            dbms_output.put_line ('Deleting TABLE '||v_table_nam||' FOR run_id >= '||i_del_run_tab(1));
            IF v_table_nam = 'ST_BAY_STRATEGY_RUN' THEN
               structools.schema_utils.disable_table_indexes('ST_BAY_STRATEGY_RUN');
               DELETE FROM structools.ST_BAY_STRATEGY_RUN
               WHERE run_id >= i_del_run_tab(1);
               structools.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN');
               COMMIT;
            ELSIF v_table_nam = 'ST_BAY_STRATEGY_RUN_RB_TMP' THEN
               structools.schema_utils.disable_table_indexes('ST_BAY_STRATEGY_RUN_RB_TMP');
               DELETE FROM structools.st_bay_Strategy_run_rb_tmp
               WHERE run_id >= i_del_run_tab(1);
               COMMIT;
               structools.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN_RB_TMP');
            ELSIF v_table_nam = 'ST_BAY_STRATEGY_RUN_RB' THEN
               structools.schema_utils.disable_table_indexes('st_bay_strategy_run_rb');
               DELETE FROM structools.st_bay_Strategy_run_rb
               WHERE run_id >= i_del_run_tab(1);
               structools.schema_utils.enable_table_indexes('st_bay_strategy_run_rb');
               COMMIT;
            ELSIF v_table_nam = 'ST_STRATEGY_RUN' THEN
               structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN');
               DELETE FROM structools.ST_STRATEGY_RUN
               WHERE run_id >= i_del_run_tab(1);
               structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN');
               COMMIT;
            ELSIF v_table_nam = 'ST_STRATEGY_RUN_RB_TMP' THEN
               structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN_RB_TMP');
               DELETE FROM structools.st_strategy_run_rb_tmp
               WHERE run_id >= i_del_run_tab(1);
               COMMIT;
               structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN_RB_TMP');
            ELSIF v_table_nam = 'ST_STRATEGY_RUN_RB' THEN
               structools.schema_utils.disable_table_indexes('st_strategy_run_rb');
               DELETE FROM structools.st_strategy_run_rb
               WHERE run_id >= i_del_run_tab(1);
               COMMIT;
               structools.schema_utils.enable_table_indexes('st_strategy_run_rb');
            ELSIF v_table_nam = 'ST_STRATEGY_RUN_RB2' THEN
               structools.schema_utils.disable_table_indexes('st_strategy_run_rb2');
               DELETE FROM structools.st_strategy_run_rb2
               WHERE run_id >= i_del_run_tab(1);
               COMMIT;
               structools.schema_utils.enable_table_indexes('st_strategy_run_rb2');
            ELSIF v_table_nam = 'ST_STRATEGY_RUN_SUMMARY' THEN
               DELETE FROM structools.ST_STRATEGY_RUN_SUMMARY
               WHERE run_id >= i_del_run_tab(1);
               COMMIT;
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_ZBAM_REINF_SUMM' THEN
               DELETE FROM structools.ST_DEL_PROGRAM_ZBAM_REINF_SUMM
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT;
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_MTZN_OPTIMISED' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_MTZN_OPTIMISED
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SNIPER' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SNIPER
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_SUMMARY' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SEG_SUMMARY
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_OPTIMISED' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SEG_OPTIMISED
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_REINF_SUMM' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_REINF_SUMM
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_REINF_OPT' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_REINF_OPT
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_REINF_SUMM' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SEG_REINF_SUMM
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_REAC_MAINT' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_REAC_MAINT
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_PICKID' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_PICKID
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_AGE_STATS' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_AGE_STATS
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_DELTA' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SEG_DELTA
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_INCD_FORECAST' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_INCD_FORECAST
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_RISK' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_RISK
               WHERE base_run_id >= i_del_run_tab(1);
               COMMIT; 
            END IF;
         ELSIF v_qualifier = 'IN' THEN
            dbms_output.put_line ('Deleting TABLE '||v_table_nam||' FOR run_id IN '||v_run_id_1||','||v_run_id_2||','||v_run_id_3||','||v_run_id_4||','||v_run_id_5);
            IF v_table_nam = 'ST_BAY_STRATEGY_RUN' THEN
               structools.schema_utils.disable_table_indexes('ST_BAY_STRATEGY_RUN');
               DELETE FROM structools.ST_BAY_STRATEGY_RUN
               WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               structools.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN');
               COMMIT;
            ELSIF v_table_nam = 'ST_BAY_STRATEGY_RUN_RB_TMP' THEN
               structools.schema_utils.disable_table_indexes('ST_BAY_STRATEGY_RUN_RB_TMP');
               DELETE FROM structools.st_bay_Strategy_run_rb_tmp
               WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT;
               structools.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN_RB_TMP');
            ELSIF v_table_nam = 'ST_BAY_STRATEGY_RUN_RB' THEN
               structools.schema_utils.disable_table_indexes('st_bay_strategy_run_rb');
               DELETE FROM structools.st_bay_Strategy_run_rb
               WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT;
               structools.schema_utils.enable_table_indexes('st_bay_strategy_run_rb');
            ELSIF v_table_nam = 'ST_STRATEGY_RUN' THEN
               structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN');
               DELETE FROM structools.ST_STRATEGY_RUN
               WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN');
               COMMIT;
            ELSIF v_table_nam = 'ST_STRATEGY_RUN_RB_TMP' THEN
               structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN_RB_TMP');
               DELETE FROM structools.st_strategy_run_rb_tmp
               WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT;
               structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN_RB_TMP');
            ELSIF v_table_nam = 'ST_STRATEGY_RUN_RB' THEN
               structools.schema_utils.disable_table_indexes('st_strategy_run_rb');
               DELETE FROM structools.st_strategy_run_rb
               WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT;
               structools.schema_utils.enable_table_indexes('st_strategy_run_rb');
            ELSIF v_table_nam = 'ST_STRATEGY_RUN_RB2' THEN
               structools.schema_utils.disable_table_indexes('st_strategy_run_rb2');
               DELETE FROM structools.st_strategy_run_rb2
               WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT;
               structools.schema_utils.enable_table_indexes('st_strategy_run_rb2');
            ELSIF v_table_nam = 'ST_STRATEGY_RUN_SUMMARY' THEN
               DELETE FROM structools.ST_STRATEGY_RUN_SUMMARY
               WHERE run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT;
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_ZBAM_REINF_SUMM' THEN
               DELETE FROM structools.ST_DEL_PROGRAM_ZBAM_REINF_SUMM
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT;
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_MTZN_OPTIMISED' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_MTZN_OPTIMISED
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SNIPER' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SNIPER
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_SUMMARY' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SEG_SUMMARY
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_OPTIMISED' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SEG_OPTIMISED
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_REINF_SUMM' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_REINF_SUMM
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_REINF_OPT' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_REINF_OPT
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_REINF_SUMM' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SEG_REINF_SUMM
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_REAC_MAINT' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_REAC_MAINT
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_PICKID' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_PICKID
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_AGE_STATS' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_AGE_STATS
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_DELTA' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_SEG_DELTA
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_INCD_FORECAST' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_INCD_FORECAST
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            ELSIF v_table_nam = 'ST_DEL_PROGRAM_RISK' THEN 
               DELETE FROM structools.ST_DEL_PROGRAM_RISK
               WHERE base_run_id IN (v_run_id_1, v_run_id_2, v_run_id_3, v_run_id_4, v_run_id_5);
               COMMIT; 
            END IF;
         END IF;   
      END IF;      
            
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END delete_run;

   /**********************************************************************************************************************************************/
   /* Delete results for run-program-year for as epcified table.                                                                                 */
   /* If i_year is null, it will delete all years for the run_id and program_id.                                                                 */
   /* Del stratgey id and scenario id are applicable only to st_del_program_reac_maint and st_del_program_sniper tables.                         */
   /**********************************************************************************************************************************************/
   PROCEDURE delete_program_year (i_table_nam                  IN VARCHAR2
                                 ,i_run_id                     IN PLS_INTEGER
                                 ,i_program_id                 IN PLS_INTEGER
                                 ,i_year                       IN PLS_INTEGER DEFAULT NULL
                                 ,i_del_strategy_id            IN PLS_INTEGER DEFAULT NULL
                                 ,i_del_strategy_scenario_id   IN PLS_INTEGER DEFAULT NULL) IS
   
   v_table_nam          VARCHAR2(50) := UPPER(i_table_nam);
      
   BEGIN

      IF v_table_nam  != 'ST_DEL_PROGRAM_MTZN_OPTIMISED' 
      AND v_table_nam != 'ST_STRATEGY_RUN_SUMMARY'
      AND v_table_nam != 'ST_DEL_PROGRAM_ZBAM_REINF_SUMM'
      AND v_table_nam != 'ST_DEL_PROGRAM_SNIPER' 
      AND v_table_nam != 'ST_DEL_PROGRAM_SEG_SUMMARY'
      AND v_table_nam != 'ST_DEL_PROGRAM_SEG_OPTIMISED'
      AND v_table_nam != 'ST_DEL_PROGRAM_REINF_SUMM'
      AND v_table_nam != 'ST_DEL_PROGRAM_REINF_OPT'
      AND v_table_nam != 'ST_DEL_PROGRAM_SEG_REINF_SUMM'
      AND v_table_nam != 'ST_DEL_PROGRAM_REAC_MAINT'
      AND v_table_nam != 'ST_DEL_PROGRAM_INCD_FORECAST'
      AND v_table_nam != 'ST_DEL_PROGRAM_RISK'
      AND v_table_nam != 'ST_DEL_PROGRAM_AGE_STATS' THEN
         dbms_output.put_line('TABLE NAME NOT valid');
         RAISE v_prog_excp;
      END IF;

      IF v_table_nam != 'ST_DEL_PROGRAM_SNIPER' 
      AND v_table_nam != 'ST_DEL_PROGRAM_REAC_MAINT' THEN
         IF i_del_strategy_id IS NOT NULL OR i_del_strategy_scenario_id IS NOT NULL THEN
            dbms_output.put_line('Del stratgey PARAMETERS are NOT applicable TO THE specified TABLE');
            RAISE v_prog_excp;
         END IF;
      END IF;
      
      IF v_table_nam = 'ST_DEL_PROGRAM_REAC_MAINT'
      OR v_table_nam = 'ST_DEL_PROGRAM_SNIPER' THEN
         IF i_year IS NULL THEN
            IF i_del_strategy_id IS NOT NULL OR i_del_strategy_scenario_id IS NOT NULL THEN
               dbms_output.put_line('Del strategy PARAMETERS must be NULL IF YEAR IS NULL');
               RAISE v_prog_excp;
            END IF;
         ELSE
            IF i_del_strategy_id IS NOT NULL AND i_del_strategy_scenario_id IS NULL
            OR i_del_strategy_id IS NULL AND i_del_strategy_scenario_id IS NOT NULL THEN
               dbms_output.put_line('BOTH del strategy PARAMETERS must be NULL OR BOTH NOT NULL');
               RAISE v_prog_excp;
            END IF;
         END IF;    
      END IF;
      
      IF v_table_nam = 'ST_DEL_PROGRAM_MTZN_OPTIMISED' THEN
         IF i_year IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_MTZN_OPTIMISED
            WHERE base_run_id = i_run_id
            AND   program_id  = i_program_id;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_MTZN_OPTIMISED
            WHERE base_run_id = i_run_id
            AND   program_id  = i_program_id
            AND   del_year    = i_year;
         END IF;
      ELSIF v_table_nam = 'ST_STRATEGY_RUN_SUMMARY' THEN
         IF i_year IS NULL THEN
            DELETE FROM structools.ST_STRATEGY_RUN_SUMMARY
            WHERE run_id      = i_run_id
            AND   program_id  = i_program_id;
         ELSE
            DELETE FROM structools.ST_STRATEGY_RUN_SUMMARY
            WHERE run_id      = i_run_id
            AND   program_id  = i_program_id
            AND   YEAR        = i_year;
         END IF;
      ELSIF v_table_nam = 'ST_DEL_PROGRAM_ZBAM_REINF_SUMM' THEN
         IF i_year IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_ZBAM_REINF_SUMM
            WHERE base_run_id      = i_run_id
            AND   program_id  = i_program_id;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_ZBAM_REINF_SUMM
            WHERE base_run_id      = i_run_id
            AND   program_id  = i_program_id
            AND   del_YEAR        = i_year;
         END IF;
      ELSIF v_table_nam = 'ST_DEL_PROGRAM_SNIPER' THEN
         IF i_year IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_SNIPER
            WHERE base_run_id = i_run_id
            AND   program_id  = i_program_id;
         ELSIF i_del_strategy_id IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_SNIPER
            WHERE base_run_id = i_run_id
            AND   program_id  = i_program_id
            AND   del_year    = i_year;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_SNIPER
            WHERE base_run_id = i_run_id
            AND   program_id  = i_program_id
            AND   del_year    = i_year
            AND   del_strategy_id           = i_del_strategy_id
            AND   del_strategy_scenario_id  = i_del_strategy_scenario_id;
         END IF;
      ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_SUMMARY' THEN
         IF i_year IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_SUMMARY
            WHERE base_run_id      = i_run_id
            AND   program_id  = i_program_id;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_SUMMARY
            WHERE base_run_id      = i_run_id
            AND   program_id  = i_program_id
            AND   del_year        = i_year;
         END IF;
      ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_OPTIMISED' THEN
         IF i_year IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_OPTIMISED
            WHERE base_run_id      = i_run_id
            AND   program_id  = i_program_id;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_OPTIMISED
            WHERE base_run_id      = i_run_id
            AND   program_id  = i_program_id
            AND   del_year        = i_year;
         END IF;
      ELSIF v_table_nam = 'ST_DEL_PROGRAM_REINF_SUMM' THEN
         IF i_year IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_SUMM
            WHERE base_run_id      = i_run_id
            AND   program_id  = i_program_id;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_SUMM
            WHERE base_run_id      = i_run_id
            AND   program_id  = i_program_id
            AND   del_year        = i_year;
         END IF;
      ELSIF v_table_nam = 'ST_DEL_PROGRAM_REINF_OPT' THEN
         IF i_year IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_OPT
            WHERE base_run_id      = i_run_id
            AND   program_id  = i_program_id;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_REINF_OPT
            WHERE base_run_id      = i_run_id
            AND   program_id  = i_program_id
            AND   del_year        = i_year;
         END IF;
      ELSIF v_table_nam = 'ST_DEL_PROGRAM_SEG_REINF_SUMM' THEN
         IF i_year IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_REINF_SUMM
            WHERE base_run_id      = i_run_id
            AND   program_id  = i_program_id;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_SEG_REINF_SUMM
            WHERE base_run_id      = i_run_id
            AND   program_id  = i_program_id
            AND   del_year        = i_year;
         END IF;
      ELSIF v_table_nam = 'ST_DEL_PROGRAM_REAC_MAINT' THEN
         IF i_year IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_REAC_MAINT
            WHERE base_run_id = i_run_id
            AND   program_id  = i_program_id;
         ELSIF i_del_strategy_id IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_REAC_MAINT
            WHERE base_run_id = i_run_id
            AND   program_id  = i_program_id
            AND   del_year    = i_year;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_REAC_MAINT
            WHERE base_run_id = i_run_id
            AND   program_id  = i_program_id
            AND   del_year    = i_year
            AND   del_strategy_id           = i_del_strategy_id
            AND   del_strategy_scenario_id  = i_del_strategy_scenario_id;
         END IF;
      ELSIF v_table_nam = 'ST_DEL_PROGRAM_INCD_FORECAST' THEN  -- all years
         IF i_program_id IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_INCD_FORECAST
            WHERE base_run_id = i_run_id
            AND   program_id  IS NULL;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_INCD_FORECAST
            WHERE base_run_id = i_run_id
            AND   program_id  = i_program_id;
         END IF;
      ELSIF v_table_nam = 'ST_DEL_PROGRAM_RISK' THEN  -- all years
         IF i_program_id IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_RISK
            WHERE base_run_id = i_run_id
            AND   program_id  IS NULL;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_RISK
            WHERE base_run_id = i_run_id
            AND   program_id  = i_program_id;
         END IF;
      ELSIF v_table_nam = 'ST_DEL_PROGRAM_AGE_STATS' THEN  -- all years
         IF i_program_id IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_AGE_STATS
            WHERE base_run_id = i_run_id
            AND   program_id  IS NULL;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_AGE_STATS
            WHERE base_run_id = i_run_id
            AND   program_id  = i_program_id;
         END IF;
        END IF;

      COMMIT; 
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END delete_program_year;

   PROCEDURE register_run_request(i_run_id                        IN PLS_INTEGER
                                 ,i_run_years                     IN PLS_INTEGER
                                 ,i_run_requestor                 IN VARCHAR2    DEFAULT NULL     
                                 ,i_run_comment                   IN VARCHAR2    DEFAULT NULL
                                 ,i_treatment_type                IN VARCHAR2
                                 ,i_mtzn_committed_version_id     IN PLS_INTEGER
                                 ,i_first_year                    IN PLS_INTEGER
                                 ,i_parent_run_program_id         IN PLS_INTEGER DEFAULT NULL
                                 ,i_parent_run_id                 IN PLS_INTEGER DEFAULT NULL
                                 ,i_strategy_set_id               IN PLS_INTEGER
                                 ,i_upgrade_poles_under_cond IN CHAR
                                 ,i_suppress_pickids				 IN CHAR
                                 ,i_suppress_pickids_ver				 IN VARCHAR2
                                 ,i_run_request_parm_scen_tab     IN rr_parm_scen_tab_typ DEFAULT NULL
                                 ,i_run_request_strat_scen_tab    IN rr_strat_scen_tab_typ) IS

      v_request_not_valid     EXCEPTION;
      v_run_reqeust_id        PLS_INTEGER;
      v_run_id                PLS_INTEGER;
      v_parent_run_yrs        PLS_INTEGER;
      v_dummy_int             PLS_INTEGER;
      v_parent_run_id2        PLS_INTEGER;
      v_parent_run_program_id2   PLS_INTEGER;
      v_wrong_strategy_cnt       PLS_INTEGER;
      
   BEGIN 
   
      /* Validate run request */
      BEGIN
         SELECT run_id
               ,run_id
         INTO  v_run_reqeust_id
              ,v_run_id
         FROM structools.st_run_request
         WHERE run_id = i_run_id;  
         dbms_output.put_line('Run ID already EXISTS IN st_run_request');
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;                      
            
      IF i_treatment_type != 'FULL' AND i_treatment_type != 'PARTIAL' THEN
         dbms_output.put_line('Run request '||i_run_id||' - treatment TYPE NOT valid');
         RAISE v_request_not_valid;
      END IF;
      
      IF i_upgrade_poles_under_cond IS NULL OR (i_upgrade_poles_under_cond != 'Y' AND i_upgrade_poles_under_cond != 'N') THEN
         dbms_output.put_line('Run request '||i_run_id||' - upgrade_poles_under_cond NOT valid');
         RAISE v_request_not_valid;
      END IF;
      
      IF i_suppress_pickids IS NULL OR (i_suppress_pickids != 'Y' AND i_suppress_pickids != 'N') THEN
         dbms_output.put_line('Run request '||i_run_id||' - suppress_pickids NOT valid');
         RAISE v_request_not_valid;
      END IF;
      
      IF i_suppress_pickids = 'Y' AND i_suppress_pickids_ver IS NULL THEN
         dbms_output.put_line('Run request '||i_run_id||' - suppress_pickids_ver must NOT be NULL');
         RAISE v_request_not_valid;
      END IF;
      
      IF i_first_year <= v_this_year THEN
         dbms_output.put_line('Run request '||i_run_id||' - first_year must be > '||v_this_year);
         RAISE v_request_not_valid;
      END IF;
            
      IF i_first_year = v_this_year + 1 THEN
         IF i_parent_run_program_id IS NOT NULL OR i_parent_run_id IS NOT NULL THEN
            dbms_output.put_line('Run request '||i_run_id||' - program_id AND parent_run_id must be NULL FOR INITIAL run');
            RAISE v_request_not_valid;
         END IF;
      END IF;
            
      IF i_first_year > v_this_year + 1 THEN
         IF i_parent_run_program_id IS NULL OR i_parent_run_id IS NULL THEN
            dbms_output.put_line('Run request '||i_run_id||' - program_id AND parent_run_id must NOT be NULL FOR run continuation');
            RAISE v_request_not_valid;
         END IF;
      END IF;
         
      IF i_parent_run_id IS NOT NULL THEN
         BEGIN
            SELECT DISTINCT run_years 
            INTO v_parent_run_yrs 
            FROM structools.st_run_request
            WHERE run_id BETWEEN i_parent_run_id AND i_run_id - 1;
            IF v_parent_run_yrs != i_run_years THEN 
               dbms_output.put_line('Run request '||i_run_id||' - this run AND ALL PARENT runs must ALL have same run_years');
               RAISE v_request_not_valid;
            END IF;
         EXCEPTION
            WHEN TOO_MANY_ROWS THEN
               dbms_output.put_line('Run request '||i_run_id||' - PARENT runs must ALL have same run_years');
               RAISE v_request_not_valid;
         END;
         BEGIN
            SELECT run_id
            INTO   v_dummy_int
            FROM structools.st_run_request
            WHERE run_id = i_run_id - 1
            AND first_year + run_years = i_first_year;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               dbms_output.put_line('Run request '||i_run_id||' - first_year must be CONSISTENT WITH PREVIOUS run');
               RAISE v_request_not_valid;
         END;
         IF i_run_id > i_parent_run_id + 1 THEN
            BEGIN
               SELECT DISTINCT parent_run_id 
               INTO v_parent_run_id2
               FROM structools.st_run_request
               WHERE run_id BETWEEN i_parent_run_id + 1 AND i_run_id; -- the first run will not have a parent
            EXCEPTION
               WHEN TOO_MANY_ROWS THEN
                  dbms_output.put_line('Run request '||i_run_id||' - ALL related runs must have same PARENT run_id');
                  RAISE v_request_not_valid;
            END;
            BEGIN
               SELECT DISTINCT parent_run_program_id 
               INTO v_parent_run_program_id2
               FROM structools.st_run_request
               WHERE run_id BETWEEN i_parent_run_id + 1 AND i_run_id; -- the first run will not have a parent
            EXCEPTION
               WHEN TOO_MANY_ROWS THEN
                  dbms_output.put_line('Run request '||i_run_id||' - ALL related runs must have same PARENT program_id');
                  RAISE v_request_not_valid;
            END;
         END IF;
                           
      END IF;
         
      /* Check that run strategies all belong to the strategy set */
      WITH strategy_set_strategies AS
      (
      SELECT strategy_id
      FROM   structools.st_strategy
      WHERE strategy_set_id = i_strategy_set_id
      )
      SELECT COUNT(*)
         INTO v_wrong_strategy_cnt
      FROM
      (
      SELECT A.strategy_id AS strategy_1
            ,b.strategy_id AS strategy_2
      FROM   structools.st_run_request_strategy A  LEFT OUTER JOIN
             strategy_set_strategies         b
      ON A.strategy_id = b.strategy_id    
      WHERE A.run_id = i_run_id
      ) aa
      WHERE aa.strategy_2 IS NULL
      ; 
      IF v_wrong_strategy_cnt > 0 THEN  
         dbms_output.put_line('Run request strategy must be part OF strategy set '||i_strategy_set_id);
         RAISE v_request_not_valid;
      END IF;
         
      /* End of run request validation */
      
      INSERT INTO structools.st_run_request
                  VALUES (i_run_id
                         ,i_run_years
                         ,i_run_requestor
                         ,i_run_comment
                         ,UPPER(i_treatment_type)
                         ,NULL
                         ,'Y'
                         ,i_mtzn_committed_version_id
                         ,i_first_year
                         ,i_parent_run_program_id
                         ,i_parent_run_id
                         ,i_strategy_set_id
                         ,NULL
                         ,NULL
                         ,NULL
                         ,NULL
                         ,i_upgrade_poles_under_cond
                         ,i_suppress_pickids
                         ,i_suppress_pickids_ver
                         );
          
      FOR i IN 1 .. i_run_request_strat_scen_tab.COUNT LOOP
         INSERT INTO structools.st_run_request_strategy
               VALUES (i_run_id
                      ,i_run_request_strat_scen_tab(i).rr_strategy_id
                      ,i_run_request_strat_scen_tab(i).rr_strat_scen_id
                      );
      END LOOP;
      
      IF i_run_request_parm_scen_tab IS NOT NULL THEN
         FOR i IN 1 .. i_run_request_parm_scen_tab.COUNT LOOP
            INSERT INTO structools.st_run_request_parameter
                  VALUES (i_run_id
                         ,i_run_request_parm_scen_tab(i).rr_parm_scen_id
                         ,UPPER(i_run_request_parm_scen_tab(i).rr_parm_id)
                         );
         END LOOP;
      END IF;
                                 
      COMMIT;   
         
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END register_run_request;

   /******************************************************************************************************************************/
   /* Nullify run_ts in st_run_request, so that a run can be redone.                                                             */
   /*****************************************************************************************************************************8*/ 
   PROCEDURE reset_run_request   (i_run_id                        IN PLS_INTEGER) IS

      v_run_id                PLS_INTEGER;
      
   BEGIN 
   
      /* Validate request */
      BEGIN
         SELECT run_id
         INTO  v_run_id
         FROM structools.st_run_request
         WHERE run_id = i_run_id; 
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            dbms_output.put_line('Run request '||i_run_id||' does NOT exist');
            RAISE v_request_not_valid;
      END;                      
            
      v_run_id := NULL;
      
      /* Do not allow the reset if strategy run results exist */
      BEGIN
         SELECT run_id 
         INTO   v_run_id
         FROM   structools.st_bay_strategy_run
         WHERE run_id = i_run_id
         AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
      
      IF v_run_id IS NULL THEN
         BEGIN
            SELECT run_id 
            INTO   v_run_id
            FROM   structools.st_bay_strategy_run_rb_tmp
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      
      IF v_run_id IS NULL THEN
         BEGIN
            SELECT run_id 
            INTO   v_run_id
            FROM   structools.st_bay_strategy_run_rb
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      
      IF v_run_id IS NULL THEN
         BEGIN
            SELECT run_id 
            INTO   v_run_id
            FROM   structools.st_strategy_run
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      
      IF v_run_id IS NULL THEN
         BEGIN
            SELECT run_id 
            INTO   v_run_id
            FROM   structools.st_strategy_run_rb_tmp
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      
      IF v_run_id IS NULL THEN
         BEGIN
            SELECT run_id 
            INTO   v_run_id
            FROM   structools.st_strategy_run_rb
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      
      IF v_run_id IS NULL THEN
         BEGIN
            SELECT run_id 
            INTO   v_run_id
            FROM   structools.st_strategy_run_rb2
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      
      IF v_run_id IS NOT NULL THEN
         dbms_output.put_line('Run request RESET NOT allowed; DATA EXISTS IN strategy_run TABLES');
         RAISE v_request_not_valid;
      END IF;
                      
         
      /* End of run request reset validation */
      
      UPDATE structools.st_run_request
      SET run_ts = NULL
         ,run_ts2 = NULL
         ,run_ts3 = NULL
         ,run_ts4 = NULL
      WHERE run_id = i_run_id;
                                 
      COMMIT;   
         
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END reset_run_request;

   /******************************************************************************************************************************/
   /* Delete a single run request from st_run_request table.                                                                     */
   /*****************************************************************************************************************************8*/ 
   PROCEDURE delete_run_request   (i_run_id                        IN PLS_INTEGER) IS

      v_run_id                PLS_INTEGER := NULL;
      
   BEGIN 
   
      /* Validate request */
--      BEGIN
--         SELECT run_id
--         INTO  v_run_id
--         FROM structools.st_run_request
--         WHERE run_id = i_run_id; 
--      EXCEPTION
--         WHEN NO_DATA_FOUND THEN
--            NULL;
--      END;                      
            
      /* Do not allow the deletion if strategy run results exist */
      BEGIN
         SELECT run_id 
         INTO   v_run_id
         FROM   structools.st_bay_strategy_run
         WHERE run_id = i_run_id
         AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
      
      IF v_run_id IS NULL THEN
         BEGIN
            SELECT run_id 
            INTO   v_run_id
            FROM   structools.st_bay_strategy_run_rb_tmp
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      
      IF v_run_id IS NULL THEN
         BEGIN
            SELECT run_id 
            INTO   v_run_id
            FROM   structools.st_bay_strategy_run_rb
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      
      IF v_run_id IS NULL THEN
         BEGIN
            SELECT run_id 
            INTO   v_run_id
            FROM   structools.st_strategy_run
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      
      IF v_run_id IS NULL THEN
         BEGIN
            SELECT run_id 
            INTO   v_run_id
            FROM   structools.st_strategy_run_rb_tmp
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      
      IF v_run_id IS NULL THEN
         BEGIN
            SELECT run_id 
            INTO   v_run_id
            FROM   structools.st_strategy_run_rb
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      
      IF v_run_id IS NULL THEN
         BEGIN
            SELECT run_id 
            INTO   v_run_id
            FROM   structools.st_strategy_run_rb2
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END IF;
      
      IF v_run_id IS NOT NULL THEN
         dbms_output.put_line('Run request deletion NOT allowed; DATA EXISTS IN strategy_run TABLES');
         RAISE v_request_not_valid;
      END IF;
                      
         
      /* End of run request reset validation */
      
      DELETE FROM structools.st_run_request
      WHERE run_id = i_run_id;

      DELETE FROM structools.st_run_request_strategy
      WHERE run_id = i_run_id;
                                 
      DELETE FROM structools.st_run_request_parameter
      WHERE run_id = i_run_id;
                                 
      COMMIT;   
         
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END delete_run_request;

   /******************************************************************************************************************************/
   /* update_span_unit_cost                                                                     */
   /*****************************************************************************************************************************8*/ 
   PROCEDURE update_span_unit_cost   (i_area_type        IN CHAR
                                     ,i_lvco_cnt        IN PLS_INTEGER
                                     ,i_hvco_cnt        IN PLS_INTEGER
                                     ,i_hvsp_cnt        IN PLS_INTEGER
                                     ,i_labour_hrs        IN PLS_INTEGER    DEFAULT 0
                                     ,i_tot_cost_per_km    IN PLS_INTEGER) IS

   BEGIN 
   
       UPDATE structools.st_span_action_cost
      SET    labour_hrs = i_labour_hrs    
          ,total_cost   = i_tot_cost_per_km
      WHERE area_type = UPPER(i_area_type)
      AND    lvco_cnt  = i_lvco_cnt
      AND    hvco_cnt  = i_hvco_cnt
      AND    hvsp_cnt  = i_hvsp_cnt;
      
      IF SQL%ROWCOUNT = 0 THEN
          INSERT INTO structools.st_span_action_cost
            VALUES    (UPPER(i_area_type)
                     ,i_lvco_cnt
                  ,i_hvco_cnt
                  ,i_hvsp_cnt
                  ,i_labour_hrs
                  ,i_tot_cost_per_km
                  );             
          dbms_output.put_line('ROW NOT FOUND. Inserted NEW ROW');
      ELSE
          dbms_output.put_line('ROW UPDATED');
      END IF;
      
                                 
      COMMIT;   
         
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END update_span_unit_cost;

-- /* Review these procedure in the view that table definition changed since the procs were written.
--   PROCEDURE create_parameter_scenario(i_parameter_id                    IN VARCHAR2
--                                     ,i_parameter_egi                    IN VARCHAR2
--                                     ,i_scenario_id                    IN PLS_INTEGER
--                                     ,i_parameter_val                    IN VARCHAR2
--                                     ,i_parameter_age                    IN PLS_INTEGER
--                                     ,i_parameter_risk_comp            IN VARCHAR2
--                                     ,i_parameter_wood_type            IN CHAR
--                                     ,i_parameter_maint_cost_type    IN VARCHAR2
--                                     ,i_parameter_fdr_cat            IN VARCHAR2) IS
--
--      v_parameter_id                VARCHAR2(30);
--      v_parameter_egi            VARCHAR2(30);
--      v_scenario_id                PLS_INTEGER;  
--      
--   BEGIN 
--
--        /* If the parameter itself does not exist, create it on the fly */        
--       BEGIN   
--         IF i_parameter_egi IS NULL THEN
--            SELECT parameter_id
--                  ,parameter_egi
--            INTO     v_parameter_id
--                  ,v_parameter_egi
--            FROM structools.st_parameter
--            WHERE parameter_id = UPPER(i_parameter_id)
--            AND   parameter_egi IS NULL;
--         ELSE                     
--            SELECT parameter_id
--                  ,parameter_egi
--            INTO     v_parameter_id
--                  ,v_parameter_egi
--            FROM structools.st_parameter
--            WHERE parameter_id = UPPER(i_parameter_id)
--            AND   parameter_egi = UPPER(i_parameter_egi);
--         END IF;
--      EXCEPTION
--          WHEN NO_DATA_FOUND THEN
--             INSERT INTO structools.st_parameter
--            VALUES (UPPER(i_parameter_id)
--                   ,UPPER(i_parameter_egi)); 
--      END;
--      
--      /* Ensure that the scenario does not exist already */
--      BEGIN
--         IF i_parameter_egi IS NULL THEN
--             SELECT parameter_id
--                    ,parameter_egi
--                  ,scenario_id
--                INTO     v_parameter_id
--                    ,v_parameter_egi
--                  ,v_scenario_id       
--                FROM structools.st_parameter_scenario
--            WHERE parameter_id = UPPER(i_parameter_id)
--            AND   parameter_egi IS NULL
--            AND     scenario_id = i_scenario_id;
--            ELSE                                       
--             SELECT parameter_id
--                    ,parameter_egi
--                  ,scenario_id
--                INTO     v_parameter_id
--                    ,v_parameter_egi
--                  ,v_scenario_id       
--                FROM structools.st_parameter_scenario
--            WHERE parameter_id = UPPER(i_parameter_id)
--            AND   parameter_egi = UPPER(i_parameter_egi)
--            AND     scenario_id = i_scenario_id;
--         END IF;
--         dbms_output.put_line('parameter scenario already EXISTS');
--         RAISE v_request_not_valid;
--      EXCEPTION
--          WHEN NO_DATA_FOUND THEN
--             NULL;
--      END;
--
--        INSERT INTO structools.st_parameter_scenario
--      VALUES    (UPPER(i_parameter_id)
--                  ,UPPER(i_parameter_egi)
--               ,i_scenario_id
--               ,UPPER(i_parameter_val)
--               ,i_parameter_age
--               ,i_parameter_risk_comp
--               ,UPPER(i_parameter_wood_type)
--               ,UPPER(i_parameter_maint_cost_type)
--               ,UPPER(i_parameter_fdr_cat)
--               );
--
--         dbms_output.put_line('Parameter scenario created.');
--      
--                                 
--      COMMIT;   
--         
--   EXCEPTION
--      WHEN v_request_not_valid THEN
--         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
--         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
--         RAISE;
--      WHEN OTHERS THEN
--         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
--         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
--         RAISE;
--
--   END create_parameter_scenario;
--
--   PROCEDURE delete_parameter_scenario(i_parameter_id        IN VARCHAR2
--                                     ,i_parameter_egi        IN VARCHAR2
--                                     ,i_scenario_id        IN PLS_INTEGER) IS
--
--      v_scen_cnt                    PLS_INTEGER;
--      
--   BEGIN 
--
--      IF i_parameter_egi IS NULL THEN
--         SELECT COUNT(*)
--         INTO   v_scen_cnt
--         FROM structools.st_parameter_scenario
--         WHERE parameter_id = UPPER(i_parameter_id)
--         AND   parameter_egi IS NULL
--         AND     scenario_id = i_scenario_id;
--      ELSE
--         SELECT COUNT(*)
--         INTO   v_scen_cnt
--         FROM structools.st_parameter_scenario
--         WHERE parameter_id = UPPER(i_parameter_id)
--         AND   parameter_egi = UPPER(i_parameter_egi)
--         AND     scenario_id = i_scenario_id;
--      END IF;
--      
--      IF v_scen_cnt = 0 THEN
--         dbms_output.put_line('parameter scenario NOT FOUND.');
--         RAISE v_request_not_valid;
--      ELSIF v_scen_cnt > 1 THEN
--         dbms_output.put_line('multiple parameter scenarios FOUND !!!.');
--         RAISE v_request_not_valid;
--      END IF;
--
--      IF i_parameter_egi IS NULL THEN
--         DELETE FROM structools.st_parameter_scenario
--         WHERE parameter_id = UPPER(i_parameter_id)
--         AND   parameter_egi IS NULL
--         AND     scenario_id = i_scenario_id;
--      ELSE
--         DELETE FROM structools.st_parameter_scenario
--         WHERE parameter_id = UPPER(i_parameter_id)
--         AND   parameter_egi = UPPER(i_parameter_egi)
--         AND     scenario_id = i_scenario_id;
--      END IF;
--      dbms_output.put_line('Parameter scenario deleted.');
--      
--      /* If there are no scenarios left for this parameter, delete the parameter itself from table st_parameter */
--      IF i_parameter_egi IS NULL THEN
--         SELECT COUNT(*)
--         INTO   v_scen_cnt
--         FROM structools.st_parameter_scenario
--         WHERE parameter_id = UPPER(i_parameter_id)
--         AND   parameter_egi IS NULL;
--         ELSE
--         SELECT COUNT(*)
--         INTO   v_scen_cnt
--         FROM structools.st_parameter_scenario
--         WHERE parameter_id = UPPER(i_parameter_id)
--         AND   parameter_egi = UPPER(i_parameter_egi);
--      END IF;
--      
--      IF v_scen_cnt = 0 THEN
--         IF i_parameter_egi IS NULL THEN
--            DELETE FROM structools.st_parameter
--            WHERE parameter_id = UPPER(i_parameter_id)
--            AND   parameter_egi IS NULL;
--         ELSE
--            DELETE FROM structools.st_parameter
--            WHERE parameter_id = UPPER(i_parameter_id)
--            AND   parameter_egi = UPPER(i_parameter_egi);
--         END IF;
--         dbms_output.put_line('Parameter deleted.');
--      END IF;
--      
--      COMMIT;   
--         
--   EXCEPTION
--      WHEN v_request_not_valid THEN
--         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
--         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
--         RAISE;
--      WHEN OTHERS THEN
--         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
--         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
--         RAISE;
--
--   END delete_parameter_scenario;

   PROCEDURE create_strategy_set    (i_strategy_set_id                    IN PLS_INTEGER
                                             ,i_strategy_set_descr                IN VARCHAR2) IS

      v_strategy_set_id            PLS_INTEGER;
      
   BEGIN 

        /* Check if the id is already there */        
       BEGIN   
         SELECT strategy_set_id
         INTO     v_strategy_set_id
         FROM structools.st_strategy_set
         WHERE strategy_set_id = i_strategy_set_id;
         dbms_output.put_line('Strategy set already EXISTS.');
         RAISE v_request_not_valid;

      EXCEPTION
          WHEN NO_DATA_FOUND THEN
             INSERT INTO structools.st_strategy_set
            VALUES (i_strategy_set_id
                   ,i_strategy_set_descr); 
      END;
      
         dbms_output.put_line('Strategy set created.');
      
                                 
      COMMIT;   
         
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END create_strategy_set;

   PROCEDURE update_strategy_set    (i_strategy_set_id                    IN PLS_INTEGER
                                             ,i_strategy_set_descr                IN VARCHAR2) IS

      v_strategy_set_id            PLS_INTEGER;
      
   BEGIN 

        /* Check if the id is already there */        
       BEGIN   
         SELECT strategy_set_id
         INTO     v_strategy_set_id
         FROM structools.st_strategy_set
         WHERE strategy_set_id = i_strategy_set_id;
         UPDATE structools.st_strategy_set
         SET     strategy_set_descr = i_strategy_set_descr
         WHERE strategy_set_id = i_strategy_set_id; 
      EXCEPTION
          WHEN NO_DATA_FOUND THEN
            dbms_output.put_line('Strategy set does NOT exist.');
            RAISE v_request_not_valid;
      END;
      
         dbms_output.put_line('Strategy set UPDATED.');
                                 
      COMMIT;   
         
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END update_strategy_set;

   PROCEDURE delete_strategy_set    (i_strategy_set_id                    IN PLS_INTEGER) IS

      v_strategy_set_id            PLS_INTEGER;
      v_strategy_id                PLS_INTEGER;
      
   BEGIN 

        /* Check if the id is there */        
       BEGIN   
         SELECT strategy_set_id
         INTO     v_strategy_set_id
         FROM structools.st_strategy_set
         WHERE strategy_set_id = i_strategy_set_id;
      EXCEPTION
          WHEN NO_DATA_FOUND THEN
            dbms_output.put_line('Strategy set does NOT exist.');
            RAISE v_request_not_valid;
      END;
      
      /* Do not allow deletion if the set has related strategies */
      BEGIN
         SELECT strategy_id
         INTO   v_strategy_id
         FROM structools.st_strategy
         WHERE strategy_set_id = i_strategy_set_id
         AND ROWNUM = 1;
         dbms_output.put_line('Strategy set has related strategies.');
         RAISE v_request_not_valid;
      EXCEPTION
          WHEN NO_DATA_FOUND THEN
             DELETE FROM structools.st_strategy_set
            WHERE strategy_set_id = i_strategy_set_id;
      END;
      
      
         dbms_output.put_line('Strategy set deleted.');
                                 
      COMMIT;   
         
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END delete_strategy_set;

   /*********************************************************************************************************************************/
   /* Insert strategy conditions for a single strategy (ie single egi).                                                             */
   /*********************************************************************************************************************************/
   PROCEDURE create_strategy_condition (i_strategy_id                IN PLS_INTEGER
                                       ,i_strategy_condition_tab     IN strategy_condition_tab_typ 
                                       ,i_commit_ind                 IN CHAR) IS

      v_egi                         VARCHAR2(10);
      K                             PLS_INTEGER;
      v_parm_fr                     PLS_INTEGER;
      v_parm_to                     PLS_INTEGER;
      v_parm                        VARCHAR2(30);
      v_event_reason                VARCHAR2(50);
      v_event_reason_cnt            PLS_INTEGER; 
      v_strategy_condition_seq      PLS_INTEGER;
      v_strategy_condition_seq_cnt  PLS_INTEGER;
      
   BEGIN

      dbms_output.put_line('Strategy_condition_tab FOR strategy '||i_strategy_id);
      FOR i IN 1 .. i_strategy_condition_tab.COUNT LOOP
         dbms_output.put_line(i_strategy_condition_tab(i).strategy_cond_egi||' '||i_strategy_condition_tab(i).strategy_cond_seq
                                 ||' '||i_strategy_condition_tab(i).strategy_cond_descr||' '||i_strategy_condition_tab(i).strategy_cond_action
                                 ||' '||i_strategy_condition_tab(i).strategy_cond_event_reason);
      END LOOP;

      /* Validate data and create strategy_condition and strategy_condition_parm, if applicable */
      FOR i IN 1 .. i_strategy_condition_tab.COUNT LOOP
         SELECT equip_grp_id
         INTO   v_egi
         FROM structools.st_strategy
         WHERE strategy_id = i_strategy_id;
         IF i_strategy_condition_tab(i).strategy_cond_egi != v_egi THEN
            dbms_output.put_line('Strategy condition egi does NOT match strategy ID '||i_strategy_condition_tab(i).strategy_cond_egi
                                 ||' '||i_strategy_id);
            RAISE v_request_not_valid;
         END IF;
         IF i_strategy_condition_tab(i).strategy_cond_action = 'REPLACE'
         OR i_strategy_condition_tab(i).strategy_cond_action = 'REINFORCE' AND v_egi = 'PWOD' THEN
            NULL;
         ELSE
            dbms_output.put_line('Strategy condition action '||i_strategy_condition_tab(i).strategy_cond_action||' invalid');
            RAISE v_request_not_valid;
         END IF;
         IF i_strategy_condition_tab(i).strategy_cond_descr IS NULL THEN
            dbms_output.put_line('Strategy condition description IS NULL');
            RAISE v_request_not_valid;
         ELSE
            /* Check if it contains a parameter */
            K := 0;
--            dbms_output.put_line('i = '||i);
--            dbms_output.put_line('i_strategy_condition_tab(i).strategy_cond_descr = '||i_strategy_condition_tab(i).strategy_cond_descr);
            FOR j IN 1 .. LENGTH(i_strategy_condition_tab(i).strategy_cond_descr) LOOP
--               dbms_output.put_line('j = '||j);
               IF SUBSTR(i_strategy_condition_tab(i).strategy_cond_descr, j, 1) = '%' THEN
                  K := K + 1;
                  IF K = 1 THEN
                     v_parm_fr := j + 1;
                  ELSIF K = 2 THEN
                     v_parm_to := j - 1;
                     v_parm := SUBSTR(i_strategy_condition_tab(i).strategy_cond_descr, v_parm_fr, v_parm_to - v_parm_fr + 1);
                     INSERT INTO structools.st_strategy_condition_parm
                        VALUES   (i_strategy_id
                                 ,i_strategy_condition_tab(i).strategy_cond_seq
                                 ,v_parm
                                 );
                     K := 0;
                  ELSE 
                     dbms_output.put_line('Something wrong WITH parameter DEFINITION(1)');
                     RAISE v_request_not_valid;
                  END IF;
               END IF;
            END LOOP;
            IF K != 0 THEN
               dbms_output.put_line('Something wrong WITH parameter DEFINITION(2)');
               RAISE v_request_not_valid;
            END IF;
         END IF;
         /* Insert condition */
         INSERT INTO structools.st_strategy_condition
            VALUES   (i_strategy_id
                     ,i_strategy_condition_tab(i).strategy_cond_seq
                     ,i_strategy_condition_tab(i).strategy_cond_descr
                     ,i_strategy_condition_tab(i).strategy_cond_action
                     ,i_strategy_condition_tab(i).strategy_cond_event_reason);
      END LOOP;   

      /* Ensure that there are no duplicate event reasons for the strategy. */
      BEGIN
         SELECT event_reason
               ,COUNT(*)
         INTO v_event_reason
             ,v_event_reason_cnt                 
         FROM structools.st_strategy_condition
         WHERE strategy_id = i_strategy_id
         GROUP BY event_reason
         HAVING COUNT(*) > 1;
         dbms_output.put_line('Strategy condition TABLE contains duplicate event reason FOR strategy(1) '||i_strategy_id||' '||v_event_reason);
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
         WHEN TOO_MANY_ROWS THEN
            dbms_output.put_line('Strategy condition TABLE contains duplicate event reasons FOR strategy(2) '||i_strategy_id);
            RAISE v_request_not_valid;
      END;      

      /* Ensure that there are no duplicate condition sequences for the strategy. */
                  
      BEGIN
         SELECT strategy_condition_seq
               ,COUNT(*)
         INTO   v_strategy_condition_seq
               ,v_strategy_condition_seq_cnt
         FROM  structools.st_strategy_condition
         WHERE strategy_id = i_strategy_id
         GROUP BY strategy_condition_seq
         HAVING COUNT(*) > 1;
         dbms_output.put_line('Strategy condition TABLE contains duplicate condition SEQUENCE FOR strategy(1) '||i_strategy_id);
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
         WHEN TOO_MANY_ROWS THEN
            dbms_output.put_line('Strategy condition TABLE contains duplicate condition sequences FOR strategy(2) '||i_strategy_id);
            RAISE v_request_not_valid;
      END;      
      
      IF i_commit_ind = 'Y' THEN
         COMMIT;
      END IF;   
         
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END create_strategy_condition; 
   
   /*********************************************************************************************************************************/
   /* Insert stratgey conditions for a single stratgey (ie single egi).                                                             */
   /*********************************************************************************************************************************/
   PROCEDURE create_strategy_scenario  (i_strategy_id                IN PLS_INTEGER
                                       ,i_strategy_scenario_tab      IN strategy_scenario_tab_typ 
                                       ,i_commit_ind                 IN CHAR) IS

      v_strategy_condition_seq         PLS_INTEGER;
      v_strategy_condition_parm_name   VARCHAR2(30);
      v_cnt                            PLS_INTEGER;
      v_strategy_id                    PLS_INTEGER;
      v_scenario_id                    PLS_INTEGER;
      v_scenario_cnt                   PLS_INTEGER;
      v_scenario_cnt_prev              PLS_INTEGER;
      v_egi                            VARCHAR2(10);
      
   BEGIN

      dbms_output.put_line('Strategy_scenario_tab FOR strategy '||i_strategy_id);
      FOR i IN 1 .. i_strategy_scenario_tab.COUNT LOOP
         dbms_output.put_line(i_strategy_scenario_tab(i).strategy_scenario_egi||' '||i_strategy_scenario_tab(i).strategy_scenario_id
                                 ||' '||i_strategy_scenario_tab(i).strategy_cond_seq||' '||i_strategy_scenario_tab(i).strategy_cond_parm_name
                                 ||' '||i_strategy_scenario_tab(i).strategy_cond_parm_val);
      END LOOP;


      SELECT equip_grp_id
      INTO   v_egi
      FROM structools.st_strategy
      WHERE strategy_id = i_strategy_id;

      /* Validate data and create strategy_scenarios for all stratgey condition parameters. */
      FOR i IN 1 .. i_strategy_scenario_tab.COUNT LOOP
         IF i_strategy_scenario_tab(i).strategy_scenario_egi != v_egi THEN
            dbms_output.put_line('Strategy scenario egi does NOT match strategy ID '||i_strategy_scenario_tab(i).strategy_scenario_egi
                                 ||' '||i_strategy_id);
            RAISE v_request_not_valid;
         END IF;
         /* Make sure the condition parameter exists */
         BEGIN
            SELECT strategy_condition_seq
                  ,strategy_condition_parm_name
            INTO   v_strategy_condition_seq
                  ,v_strategy_condition_parm_name
            FROM structools.st_strategy_condition_parm
            WHERE strategy_id = i_strategy_id
            AND   strategy_condition_seq = i_strategy_scenario_tab(i).strategy_cond_seq
            AND   strategy_condition_parm_name = i_strategy_scenario_tab(i).strategy_cond_parm_name;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               dbms_output.put_line('Strategy condition parameter does NOT EXISTS FOR  '||i_strategy_id
                                    ||' '||i_strategy_scenario_tab(i).strategy_cond_seq
                                    ||' '||i_strategy_scenario_tab(i).strategy_cond_parm_name);
               RAISE v_request_not_valid;
         END;
         /* Make sure we are not creating duplicates for a parameter*/
         BEGIN
            SELECT DISTINCT strategy_id
                  ,scenario_id
                  ,strategy_condition_seq
                  ,strategy_condition_parm_name
            INTO   v_strategy_id
                  ,v_scenario_id
                  ,v_strategy_condition_seq
                  ,v_strategy_condition_parm_name
            FROM structools.st_strategy_scenario
            WHERE strategy_id = i_strategy_id
            AND   scenario_id = i_strategy_scenario_tab(i).strategy_scenario_id
            AND   strategy_condition_seq = i_strategy_scenario_tab(i).strategy_cond_seq
            AND   strategy_condition_parm_name = i_strategy_scenario_tab(i).strategy_cond_parm_name;
            dbms_output.put_line('Strategy scenario already EXISTS FOR '||v_strategy_id||' '||v_scenario_id||' '
                                 ||v_strategy_condition_seq||' '||v_strategy_condition_parm_name);
            RAISE v_request_not_valid;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
         
         IF i_strategy_scenario_tab(i).strategy_cond_parm_val IS NULL THEN
            dbms_output.put_line('Strategy scenario value IS NULL FOR '||v_strategy_id||' '||v_scenario_id||' '||v_strategy_condition_seq);
            RAISE v_request_not_valid;
         END IF;
         
         /* Insert the scenario */
         INSERT INTO structools.st_strategy_scenario
            VALUES   (i_strategy_id
                     ,i_strategy_scenario_tab(i).strategy_scenario_id
                     ,i_strategy_scenario_tab(i).strategy_cond_seq
                     ,i_strategy_scenario_tab(i).strategy_cond_parm_name
                     ,i_strategy_scenario_tab(i).strategy_cond_parm_val);
      END LOOP;
      
      /* At the end, all strategy parameters must have at least one scenario, and all parameters must have the same number of scenarios. */
      
      v_scenario_cnt_prev := NULL;
      FOR c IN  (SELECT  strategy_condition_seq    /* get each parameter for the strategy */
                        ,strategy_condition_parm_name
                 FROM structools.st_strategy_condition_parm
                 WHERE strategy_id = i_strategy_id) LOOP
         SELECT COUNT(*)
         INTO   v_scenario_cnt
         FROM structools.st_strategy_scenario
         WHERE strategy_id = i_strategy_id
         AND strategy_condition_seq = c.strategy_condition_seq
         AND strategy_condition_parm_name = c.strategy_condition_parm_name;
         IF v_scenario_cnt != v_scenario_cnt_prev AND v_scenario_cnt_prev IS NOT NULL 
         OR v_scenario_cnt = 0 THEN
            dbms_output.put_line('ALL strategy PARAMETERS must have AT least one scenario AND THE same number OF scenarios '||i_strategy_id
                                    ||' '||c.strategy_condition_seq||' '||c.strategy_condition_parm_name);
            RAISE v_request_not_valid;
         END IF;
      END LOOP;
                     
      IF i_commit_ind = 'Y' THEN
         COMMIT;
      END IF;   
         
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END create_strategy_scenario; 
   
   PROCEDURE create_strategy    (i_strategy_set_id                    IN PLS_INTEGER
                                       ,i_strategy_egi_tab                   IN strategy_egi_tab_typ
                              ,i_strategy_condition_tab        IN strategy_condition_tab_typ
                              ,i_strategy_scenario_tab         IN strategy_scenario_tab_typ) IS

      v_strategy_id                   PLS_INTEGER;
      v_strategy_id_first        PLS_INTEGER;
      v_strategy_id_last         PLS_INTEGER;
      v_strategy_set_id               PLS_INTEGER;
      v_last_id                       PLS_INTEGER;
      K                          PLS_INTEGER;
      v_strategy_condition_tab   strategy_condition_tab_typ := strategy_condition_tab_typ();
      v_strategy_scenario_tab    strategy_scenario_tab_typ := strategy_scenario_tab_typ();
      
   BEGIN 

        /* Strategy set must exist.*/        
      BEGIN
         SELECT strategy_set_id
         INTO     v_strategy_set_id
         FROM structools.st_strategy_set
         WHERE strategy_set_id = i_strategy_set_id;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            dbms_output.put_line('Strategy set does NOT exist.');
            RAISE v_request_not_valid;
      END;

        /* Check if the strategies (EGIs) are already part of the strategy set */        
      FOR i IN 1 .. i_strategy_egi_tab.COUNT LOOP
          BEGIN
            SELECT strategy_id
            INTO     v_strategy_id
            FROM structools.st_strategy
            WHERE strategy_set_id = i_strategy_set_id
            AND equip_grp_id = i_strategy_egi_tab(i);
            dbms_output.put_line('Strategy set already contains strategy FOR '||i_strategy_egi_tab(i));
            RAISE v_request_not_valid;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
      END LOOP;
      
      SELECT MAX(strategy_id)
      INTO    v_strategy_id
      FROM structools.st_strategy;
      v_strategy_id_first := v_strategy_id + 1;
      
      FOR i IN 1 .. i_strategy_egi_tab.COUNT LOOP
          v_strategy_id := v_strategy_id + 1;
         INSERT INTO structools.st_strategy
         VALUES (v_strategy_id
                  ,i_strategy_egi_tab(i)
                ,i_strategy_set_id);
         /* Get conditions for the strategy into a table */
         IF i_strategy_condition_tab IS NOT NULL THEN
            v_strategy_condition_tab.DELETE;
            K := 0;
            FOR j IN 1 .. i_strategy_condition_tab.COUNT LOOP
               IF i_strategy_condition_tab(j).strategy_cond_egi = i_strategy_egi_tab(i) THEN
                  v_strategy_condition_tab.EXTEND;
                  K := K + 1;
                  v_strategy_condition_tab(K).strategy_cond_egi            := i_strategy_condition_tab(j).strategy_cond_egi;
                  v_strategy_condition_tab(K).strategy_cond_seq            := i_strategy_condition_tab(j).strategy_cond_seq;
                  v_strategy_condition_tab(K).strategy_cond_descr          := i_strategy_condition_tab(j).strategy_cond_descr;
                  v_strategy_condition_tab(K).strategy_cond_action         := i_strategy_condition_tab(j).strategy_cond_action;
                  v_strategy_condition_tab(K).strategy_cond_event_reason   := i_strategy_condition_tab(j).strategy_cond_event_reason;
               END IF;
            END LOOP;
            IF v_strategy_condition_tab.COUNT = 0 THEN
               dbms_output.put_line('Strategy FOR '||i_strategy_egi_tab(i)||' contains NO conditions.');
               RAISE v_request_not_valid;
            END IF;
            create_strategy_condition(v_strategy_id, v_strategy_condition_tab,'N');
         END IF;
         IF i_strategy_scenario_tab IS NOT NULL THEN
            v_strategy_scenario_tab.DELETE;
            K := 0;
            FOR j IN 1 .. i_strategy_scenario_tab.COUNT LOOP
               IF i_strategy_scenario_tab(j).strategy_scenario_egi = i_strategy_egi_tab(i) THEN
                  v_strategy_scenario_tab.EXTEND;
                  K := K + 1;
                  v_strategy_scenario_tab(K).strategy_scenario_egi         := i_strategy_scenario_tab(j).strategy_scenario_egi;
                  v_strategy_scenario_tab(K).strategy_scenario_id          := i_strategy_scenario_tab(j).strategy_scenario_id;
                  v_strategy_scenario_tab(K).strategy_cond_seq             := i_strategy_scenario_tab(j).strategy_cond_seq;
                  v_strategy_scenario_tab(K).strategy_cond_parm_name       := i_strategy_scenario_tab(j).strategy_cond_parm_name;
                  v_strategy_scenario_tab(K).strategy_cond_parm_val        := i_strategy_scenario_tab(j).strategy_cond_parm_val;
               END IF;
            END LOOP;
            create_strategy_scenario(v_strategy_id, v_strategy_scenario_tab,'N');
         END IF;
      END LOOP;
      
      v_strategy_id_last := v_strategy_id;
      
         dbms_output.put_line('Strategies '||v_strategy_id_first||' TO '||v_strategy_id_last||' created.');

            
      COMMIT;   
         
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END create_strategy;

   PROCEDURE allocate_strategy_to_set   (i_strategy_set_id               IN PLS_INTEGER
                              ,i_strategy_id_tab               IN strategy_id_tab_typ) IS

      v_strategy_id               PLS_INTEGER;
      v_strategy_set_id            PLS_INTEGER;
      v_egi                        VARCHAR2(30); 
      
   BEGIN 

      /* Strategy set must exist.*/      
      BEGIN
         SELECT strategy_set_id
         INTO    v_strategy_set_id
         FROM structools.st_strategy_set
         WHERE strategy_set_id = i_strategy_set_id;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            dbms_output.put_line('Strategy set does NOT exist.');
            RAISE v_request_not_valid;
      END;

      /* Get egi of the requested strategies and check if they are already part of the strategy set */      
      FOR i IN 1 .. i_strategy_id_tab.COUNT LOOP
         SELECT equip_grp_id
         INTO    v_egi
         FROM structools.st_strategy
         WHERE strategy_id = i_strategy_id_tab(i);
         BEGIN
            SELECT strategy_id
            INTO    v_strategy_id
            FROM structools.st_strategy
            WHERE strategy_set_id = i_strategy_set_id
            AND equip_grp_id = v_egi;
            dbms_output.put_line('Strategy set already contains strategy FOR '||v_egi);
            RAISE v_request_not_valid;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               INSERT INTO structools.st_strategy
               VALUES (i_strategy_id_tab(i)
                      ,v_egi
                      ,i_strategy_set_id);
               dbms_output.put_line('Added exisitng strategy '||i_strategy_id_tab(i)||' '||v_egi||'TO strategy set '||i_strategy_set_id);
         END;
      END LOOP;
      
      COMMIT;   
         
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END allocate_strategy_to_set;

   PROCEDURE delete_strategy    (i_strategy_set_id                    IN PLS_INTEGER
                                       ,i_egi                                    IN VARCHAR2) IS

      v_strategy_id                PLS_INTEGER;
      v_run_id                        PLS_INTEGER;
      v_delete_children       BOOLEAN;
      v_strategy_cnt          PLS_INTEGER;
      
   BEGIN 

        /* Check that the strategy (EGI) is part of the strategy set and get its strategy_id*/        
      BEGIN
         SELECT strategy_id
         INTO     v_strategy_id
         FROM structools.st_strategy
         WHERE strategy_set_id = i_strategy_set_id
         AND equip_grp_id = i_egi;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
         dbms_output.put_line('Strategy does NOT exist IN this strategy set');
         RAISE v_request_not_valid;
      END;

        /* Do not allow deletion if results exist for a run with this strategy set and this strategy */
      FOR c IN (SELECT A.run_id
      FROM     structools.st_run_request                A    INNER JOIN
               structools.st_run_request_strategy    b
        ON      A.run_id = b.run_id             
      WHERE     A.run_ts IS NOT NULL
      AND      A.strategy_set_id = i_strategy_set_id
      AND      b.strategy_id = v_strategy_id) LOOP
          SELECT run_id
         INTO     v_run_id
         FROM structools.st_bay_strategy_run
         WHERE run_id = c.run_id
         AND ROWNUM = 1;
         dbms_output.put_line('Strategy run results exist FOR THE strategy set AND egi. Deletion NOT allowed.');
         RAISE v_request_not_valid;
          SELECT run_id
         INTO     v_run_id
         FROM structools.st_strategy_run
         WHERE run_id = c.run_id
         AND ROWNUM = 1;
         dbms_output.put_line('Strategy run results exist FOR THE strategy set AND egi. Deletion NOT allowed.');
         RAISE v_request_not_valid;
      END LOOP;
      
      DELETE FROM structools.st_strategy
      WHERE strategy_id = v_strategy_id
      AND strategy_set_id = i_strategy_set_id;
      
      /* If the strategy does not belong to any other strategy sets, delete data from its children tables */
      v_delete_children := FALSE;
      BEGIN
         SELECT COUNT(*)
         INTO v_strategy_cnt
         FROM structools.st_strategy
         WHERE strategy_id = v_strategy_id;
         IF v_strategy_cnt = 0 THEN
            v_delete_children := TRUE;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            v_delete_children := TRUE;
      END;
      
      IF v_delete_children THEN
         DELETE FROM structools.st_strategy_condition
         WHERE strategy_id = v_strategy_id;
         
         DELETE FROM structools.st_strategy_condition_parm
         WHERE strategy_id = v_strategy_id;

         DELETE FROM structools.st_strategy_scenario
         WHERE strategy_id = v_strategy_id;
      END IF;
      
      IF v_delete_children THEN
         dbms_output.put_line('Strategy AND its children DATA deleted.');
      ELSE
            dbms_output.put_line('Strategy '||v_strategy_id||' FOR strategy_set '||i_strategy_set_id||' deleted.');
      END IF;
                                 
      COMMIT;   
         
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END delete_strategy;

   PROCEDURE create_del_strategy(i_del_strategy_descr IN VARCHAR2
                                 ,o_del_strategy_id   OUT PLS_INTEGER) IS
                            
--      v_del_strategy_id       PLS_INTEGER;              
   BEGIN
   
      SELECT MAX(strategy_id) + 1     
      INTO     o_del_strategy_id
      FROM  structools.st_del_strategy;
      
      INSERT INTO structools.st_del_strategy
         VALUES(o_del_strategy_id
               ,i_del_strategy_descr);
   
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END create_del_strategy;                                          

   PROCEDURE delete_del_strategy_scenario(i_del_strategy_id   IN PLS_INTEGER
                                          ,i_del_strategy_parm_name IN VARCHAR2
                                          ,i_del_strategy_scenario   IN PLS_INTEGER) IS
                            
   BEGIN
   
      IF i_del_strategy_parm_name IS NULL AND i_del_strategy_scenario IS NULL THEN
         DELETE FROM structools.st_del_strategy_scenario
         WHERE del_strategy_id = i_del_strategy_id;
         IF SQL%ROWCOUNT = 0 THEN
            dbms_output.put_line('NO scenarios FOUND FOR del_strategy '||i_del_strategy_id);
         ELSE
            dbms_output.put_line('Deleted '||SQL%ROWCOUNT||' st_del_strategy_scenario ROWS');
         END IF;
      ELSIF i_del_strategy_parm_name IS NOT NULL AND i_del_strategy_scenario IS NULL THEN    
         DELETE FROM structools.st_del_strategy_scenario
         WHERE del_strategy_id = i_del_strategy_id
         AND   del_strategy_parm_nam = i_del_strategy_parm_name;
         IF SQL%ROWCOUNT = 0 THEN
            dbms_output.put_line('NO scenarios FOUND FOR del_strategy '||i_del_strategy_id||' AND parameter '||i_del_strategy_parm_name);
         ELSE
            dbms_output.put_line('Deleted '||SQL%ROWCOUNT||' st_del_strategy_scenario ROWS');
         END IF;
      ELSE
         DELETE FROM structools.st_del_strategy_scenario
         WHERE del_strategy_id = i_del_strategy_id
         AND   del_strategy_parm_nam = i_del_strategy_parm_name
         AND   del_strategy_scenario_id = i_del_strategy_scenario;
         IF SQL%ROWCOUNT = 0 THEN
            dbms_output.put_line('Scenario '||i_del_strategy_scenario||' NOT FOUND FOR del_strategy '||i_del_strategy_id
                                    ||' AND parameter '||i_del_strategy_parm_name);
         ELSE
            dbms_output.put_line('Deleted '||SQL%ROWCOUNT||' st_del_strategy_scenario ROWS');
         END IF;
      END IF;
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END delete_del_strategy_scenario;                                          

   PROCEDURE delete_del_strategy_parameter(i_del_strategy_id   IN PLS_INTEGER
                                          ,i_del_strategy_parm_name IN VARCHAR2) IS
                            
   BEGIN
   
      IF i_del_strategy_parm_name IS NULL THEN
         DELETE FROM structools.st_del_strategy_parameter
         WHERE del_strategy_id = i_del_strategy_id;
         IF SQL%ROWCOUNT = 0 THEN
            dbms_output.put_line('NO PARAMETERS FOUND FOR del_stratgey '||i_del_strategy_id);
         ELSE
            dbms_output.put_line('Deleted '||SQL%ROWCOUNT||' st_del_strategy_parameter ROWS');
         END IF;
         delete_del_strategy_scenario(i_del_strategy_id, NULL, NULL); 
      ELSE
         DELETE FROM structools.st_del_strategy_parameter
         WHERE del_strategy_id = i_del_strategy_id
         AND   del_strategy_parm_nam = i_del_strategy_parm_name;
      END IF;

      delete_del_strategy_scenario(i_del_strategy_id, i_del_strategy_parm_name, NULL);
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END delete_del_strategy_parameter;                                          

   PROCEDURE delete_del_strategy(i_del_strategy_id   IN PLS_INTEGER) IS
                            
   BEGIN
   
      DELETE FROM structools.st_del_strategy
      WHERE strategy_id = i_del_strategy_id;
      IF SQL%ROWCOUNT = 0 THEN
         dbms_output.put_line('Stratgey '||i_del_strategy_id||' does NOT EXISTS');
      ELSE
         dbms_output.put_line('Deleted '||SQL%ROWCOUNT||' st_del_strategy ROWS');
      END IF;

      delete_del_strategy_parameter(i_del_strategy_id, NULL);
      
   
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END delete_del_strategy;                                          

   PROCEDURE create_del_strategy_parameter(i_del_strategy_id         IN PLS_INTEGER
                                          ,i_del_strategy_parm_name  IN VARCHAR2) IS
                   
      v_del_strategy_id             PLS_INTEGER;
      v_del_strategy_parm_nam       VARCHAR2(30);
                             
   BEGIN
      BEGIN
         SELECT strategy_id
         INTO   v_del_strategy_id
         FROM   structools.st_del_strategy
         WHERE strategy_id = i_Del_strategy_id;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            dbms_output.put_line('Del strategy '||i_del_strategy_id||' does NOT exist');
            RAISE v_request_not_valid;
      END;
   
      BEGIN
         SELECT del_strategy_parm_nam
         INTO   v_del_strategy_parm_nam
         FROM   structools.st_del_strategy_parameter
         WHERE del_strategy_id = i_del_strategy_id
         AND del_strategy_parm_nam = i_del_strategy_parm_name;
         dbms_output.put_line('Parameter '||i_del_strategy_parm_name||' already EXISTS FOR del strategy '||i_del_strategy_id);
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            NULL;
      END;
   
      INSERT INTO structools.st_del_strategy_parameter
         VALUES(i_del_strategy_id
               ,i_del_strategy_parm_name);
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END create_del_strategy_parameter;                                          

  PROCEDURE create_del_strategy_scenario(i_del_strategy_id         IN PLS_INTEGER
                                          ,i_del_strategy_parm_name  IN VARCHAR2
                                          ,i_del_strategy_scenario   IN PLS_INTEGER
                                          ,i_del_strategy_parm_val   IN NUMBER) IS
                   
      v_del_strategy_id             PLS_INTEGER;
      v_del_strategy_parm_nam       VARCHAR2(30);
      v_del_strategy_scenario_id    PLS_INTEGER;
                             
   BEGIN
      BEGIN
         SELECT del_strategy_parm_nam
         INTO   v_del_strategy_parm_nam
         FROM   structools.st_del_strategy_parameter
         WHERE del_strategy_id = i_del_strategy_id
         AND del_strategy_parm_nam = i_del_strategy_parm_name;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            dbms_output.put_line('Parameter '||i_del_strategy_parm_name||' already EXISTS FOR del strategy '||i_del_strategy_id);
            RAISE v_request_not_valid;
      END;
   
      BEGIN
         SELECT del_strategy_scenario_id
         INTO   v_del_strategy_scenario_id
         FROM   structools.st_del_strategy_scenario
         WHERE del_strategy_id = i_del_strategy_id
         AND del_strategy_parm_nam = i_del_strategy_parm_name
         AND del_strategy_scenario_id = i_del_strategy_scenario;
         dbms_output.put_line('Scenario '||i_del_strategy_scenario||' already EXISTS FOR del strategy '||i_del_strategy_id
                              ||' AND parameter '||i_del_strategy_parm_name);
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            NULL;
      END;
   
      INSERT INTO structools.st_del_strategy_scenario
         VALUES(i_del_strategy_id
               ,i_del_strategy_scenario
               ,i_del_strategy_parm_name
               ,i_del_strategy_parm_val);
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
         
   END create_del_strategy_scenario;                                          


  PROCEDURE create_st_opt_program(i_PROGRAM_ID                      IN structools.st_opt_program.PROGRAM_ID%TYPE,
                                  i_PROGRAM_DESC                    IN structools.st_opt_program.PROGRAM_DESC%TYPE) IS
                   
      v_program_id             structools.st_opt_program.PROGRAM_ID%TYPE;
                             
   BEGIN
      BEGIN
         SELECT program_id
         INTO   v_program_id
         FROM   structools.st_opt_program
         WHERE program_id = i_program_id;
         dbms_output.put_line('PROGRAM '||i_program_id||' already EXISTS IN st_opt_program');
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            NULL;
      END;
   
      INSERT INTO structools.st_opt_program(PROGRAM_ID, PROGRAM_DESC)
         VALUES(i_PROGRAM_ID
               ,i_PROGRAM_DESC);
      COMMIT;
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END create_st_opt_program; 

  PROCEDURE update_st_opt_program(i_PROGRAM_ID                      IN structools.st_opt_program.PROGRAM_ID%TYPE,
                                  i_PROGRAM_DESC                    IN structools.st_opt_program.PROGRAM_DESC%TYPE) IS
                   
      v_program_id             structools.st_opt_program.PROGRAM_ID%TYPE;
                             
   BEGIN
      BEGIN
         SELECT program_id
         INTO   v_program_id
         FROM   structools.st_opt_program
         WHERE program_id = i_program_id;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            dbms_output.put_line('PROGRAM '||i_program_id||' does NOT EXISTS IN st_opt_program');
            RAISE v_request_not_valid;
      END;
   
      UPDATE structools.st_opt_program
         SET PROGRAM_DESC = i_PROGRAM_DESC
       WHERE PROGRAM_ID = i_PROGRAM_ID;
      COMMIT;
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END update_st_opt_program; 

  PROCEDURE delete_st_opt_program(i_PROGRAM_ID                      IN structools.st_opt_program.PROGRAM_ID%TYPE) IS
                   
      v_program_id             structools.st_opt_program.PROGRAM_ID%TYPE;
                             
   BEGIN
      BEGIN
         SELECT program_id
         INTO   v_program_id
         FROM   structools.st_opt_program
         WHERE program_id = i_program_id;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            dbms_output.put_line('PROGRAM '||i_program_id||' does NOT EXISTS IN st_opt_program');
            RAISE v_request_not_valid;
      END;
   
      DELETE FROM structools.st_opt_program
       WHERE PROGRAM_ID = i_PROGRAM_ID;
      COMMIT;
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END delete_st_opt_program; 

  PROCEDURE create_st_opt_parm(i_OPT_PARM_NAME                      IN structools.st_opt_parm.OPT_PARM_NAME%TYPE,
                               i_OPT_PARM_DESC                      IN structools.st_opt_parm.OPT_PARM_DESC%TYPE) IS
                   
      v_opt_parm_name             structools.st_opt_parm.OPT_PARM_NAME%TYPE;
                             
   BEGIN
      BEGIN
         SELECT opt_parm_name
         INTO   v_opt_parm_name
         FROM   structools.st_opt_parm
         WHERE opt_parm_name = i_opt_parm_name;
         dbms_output.put_line('Parameter '||i_opt_parm_name||' already EXISTS IN st_opt_parm');
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            NULL;
      END;
   
      INSERT INTO structools.st_opt_parm (OPT_PARM_NAME, OPT_PARM_DESC)
         VALUES(i_OPT_PARM_NAME
               ,i_OPT_PARM_DESC);
      COMMIT;
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END create_st_opt_parm; 

  PROCEDURE update_st_opt_parm(i_OPT_PARM_NAME                      IN structools.st_opt_parm.OPT_PARM_NAME%TYPE,
                               i_OPT_PARM_DESC                      IN structools.st_opt_parm.OPT_PARM_DESC%TYPE) IS
                   
      v_opt_parm_name             structools.st_opt_parm.OPT_PARM_NAME%TYPE;
                             
   BEGIN
      BEGIN
         SELECT opt_parm_name
         INTO   v_opt_parm_name
         FROM   structools.st_opt_parm
         WHERE opt_parm_name = i_opt_parm_name;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            dbms_output.put_line('Parameter '||i_opt_parm_name||' does NOT exist IN st_opt_parm');
            RAISE v_request_not_valid;
      END;
   
      UPDATE structools.st_opt_parm
         SET OPT_PARM_DESC = i_OPT_PARM_DESC
       WHERE OPT_PARM_NAME = i_OPT_PARM_NAME;
      COMMIT;
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END update_st_opt_parm; 

  PROCEDURE delete_st_opt_parm(i_OPT_PARM_NAME                      IN structools.st_opt_parm.OPT_PARM_NAME%TYPE) IS
                   
      v_opt_parm_name             structools.st_opt_parm.OPT_PARM_NAME%TYPE;
                             
   BEGIN
      BEGIN
         SELECT opt_parm_name
         INTO   v_opt_parm_name
         FROM   structools.st_opt_parm
         WHERE opt_parm_name = i_opt_parm_name;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            dbms_output.put_line('Parameter '||i_opt_parm_name||' does NOT exist IN st_opt_parm');
            RAISE v_request_not_valid;
      END;
   
      DELETE FROM structools.st_opt_parm
       WHERE OPT_PARM_NAME = i_OPT_PARM_NAME;
      COMMIT;
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END delete_st_opt_parm; 

  PROCEDURE create_st_opt_program_parm_val(i_PROGRAM_ID             IN structools.st_opt_program_parm_val.PROGRAM_ID%TYPE,
                                           i_PARM_NAME              IN structools.st_opt_program_parm_val.PARM_NAME%TYPE,
                                           i_PARM_VAL               IN structools.st_opt_program_parm_val.PARM_VAL%TYPE) IS
                   
      v_program_id             structools.st_opt_program_parm_val.PROGRAM_ID%TYPE;
                             
   BEGIN
      BEGIN
         SELECT program_id 
         INTO   v_program_id
         FROM   structools.st_opt_program_parm_val
         WHERE  program_id  = i_program_id
         AND    parm_name = i_parm_name;
         dbms_output.put_line('PROGRAM '||i_PROGRAM_ID||', Parameter' || i_PARM_NAME || ' already EXISTS IN st_opt_program_parm_val');
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            NULL;
      END;
   
      INSERT INTO structools.st_opt_program_parm_val (PROGRAM_ID, PARM_NAME, PARM_VAL)
         VALUES(i_PROGRAM_ID
               ,i_PARM_NAME
               ,i_PARM_VAL);
      COMMIT;
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END create_st_opt_program_parm_val;

  PROCEDURE update_st_opt_program_parm_val(i_PROGRAM_ID             IN structools.st_opt_program_parm_val.PROGRAM_ID%TYPE,
                                           i_PARM_NAME              IN structools.st_opt_program_parm_val.PARM_NAME%TYPE,
                                           i_PARM_VAL               IN structools.st_opt_program_parm_val.PARM_VAL%TYPE) IS
                   
      v_program_id             structools.st_opt_program_parm_val.PROGRAM_ID%TYPE;
                             
   BEGIN
      BEGIN
         SELECT program_id 
         INTO   v_program_id
         FROM   structools.st_opt_program_parm_val
         WHERE  program_id  = i_program_id
         AND    parm_name = i_parm_name;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            dbms_output.put_line('PROGRAM '||i_PROGRAM_ID||', Parameter' || i_PARM_NAME || ' does NOT exist IN st_opt_program_parm_val');
            RAISE v_request_not_valid;
      END;
   
      UPDATE structools.st_opt_program_parm_val
         SET PARM_VAL = i_PARM_VAL 
       WHERE PROGRAM_ID = i_PROGRAM_ID
         AND PARM_NAME = i_PARM_NAME;
      COMMIT;
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END update_st_opt_program_parm_val;

  PROCEDURE delete_st_opt_program_parm_val(i_PROGRAM_ID             IN structools.st_opt_program_parm_val.PROGRAM_ID%TYPE,
                                           i_PARM_NAME              IN structools.st_opt_program_parm_val.PARM_NAME%TYPE) IS
                   
      v_program_id             structools.st_opt_program_parm_val.PROGRAM_ID%TYPE;
                             
   BEGIN
      BEGIN
         SELECT program_id 
         INTO   v_program_id
         FROM   structools.st_opt_program_parm_val
         WHERE  program_id  = i_program_id
         AND    parm_name = i_parm_name;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            dbms_output.put_line('PROGRAM '||i_PROGRAM_ID||', Parameter' || i_PARM_NAME || ' does NOT exist IN st_opt_program_parm_val');
            RAISE v_request_not_valid;
      END;
   
       DELETE FROM structools.st_opt_program_parm_val 
       WHERE PROGRAM_ID = i_PROGRAM_ID
         AND PARM_NAME = i_PARM_NAME;
      COMMIT;
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END delete_st_opt_program_parm_val;

  PROCEDURE create_st_opt_program_run(i_PROGRAM_ID                  IN structools.st_opt_program_run.PROGRAM_ID%TYPE,
                                      i_RUN_ID                      IN structools.st_opt_program_run.RUN_ID%TYPE,
                                      i_PROGRAM_RUN_COMPONENT       IN structools.st_opt_program_run.PROGRAM_RUN_COMPONENT%TYPE,
                                      i_PROG_RUN_COMPONENT_STAT     IN structools.st_opt_program_run.PROG_RUN_COMPONENT_STAT%TYPE) IS
                   
      v_program_id             structools.st_opt_program_run.PROGRAM_ID%TYPE;
                             
   BEGIN
      BEGIN
         SELECT program_id 
         INTO   v_program_id
         FROM   structools.st_opt_program_run
         WHERE  program_id  = i_program_id
         AND    run_id = i_run_id
         AND    program_run_component = i_program_run_component;
         dbms_output.put_line('PROGRAM '||i_PROGRAM_ID||', Run ID' || i_RUN_ID ||', Component' || i_PROGRAM_RUN_COMPONENT
                           || ' already EXISTS IN st_opt_program_run');
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            NULL;
      END;
   
      INSERT INTO structools.st_opt_program_run (PROGRAM_ID, RUN_ID, PROGRAM_RUN_COMPONENT, PROG_RUN_COMPONENT_STAT)
         VALUES(i_PROGRAM_ID
               ,i_RUN_ID
               ,i_PROGRAM_RUN_COMPONENT
               ,i_PROG_RUN_COMPONENT_STAT);
      COMMIT;
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END create_st_opt_program_run;


  PROCEDURE update_st_opt_program_run(i_PROGRAM_ID                  IN structools.st_opt_program_run.PROGRAM_ID%TYPE,
                                      i_RUN_ID                      IN structools.st_opt_program_run.RUN_ID%TYPE,
                                      i_PROGRAM_RUN_COMPONENT       IN structools.st_opt_program_run.PROGRAM_RUN_COMPONENT%TYPE,
                                      i_PROG_RUN_COMPONENT_STAT     IN structools.st_opt_program_run.PROG_RUN_COMPONENT_STAT%TYPE) IS
                   
      v_program_id             structools.st_opt_program_run.PROGRAM_ID%TYPE;
                             
   BEGIN
      BEGIN
         SELECT program_id 
         INTO   v_program_id
         FROM   structools.st_opt_program_run
         WHERE  program_id  = i_program_id
         AND    run_id = i_run_id
         AND    program_run_component = i_program_run_component;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            dbms_output.put_line('PROGRAM '||i_PROGRAM_ID||', Run ID' || i_RUN_ID||', Component' || i_PROGRAM_RUN_COMPONENT 
            || ' does NOT exist IN st_opt_program_run');
            RAISE v_request_not_valid;
      END;
   
      UPDATE structools.st_opt_program_run
         SET PROG_RUN_COMPONENT_STAT = i_PROG_RUN_COMPONENT_STAT
       WHERE PROGRAM_ID = i_PROGRAM_ID
         AND RUN_ID = i_RUN_ID
         AND PROGRAM_RUN_COMPONENT = i_PROGRAM_RUN_COMPONENT;
      COMMIT;
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END update_st_opt_program_run;

  PROCEDURE delete_st_opt_program_run(i_PROGRAM_ID                  IN structools.st_opt_program_run.PROGRAM_ID%TYPE,
                                      i_RUN_ID                      IN structools.st_opt_program_run.RUN_ID%TYPE,
                                      i_PROGRAM_RUN_COMPONENT       IN structools.st_opt_program_run.PROGRAM_RUN_COMPONENT%TYPE) IS
                                      
                   
      v_program_id             structools.st_opt_program_run.PROGRAM_ID%TYPE;
                             
   BEGIN
      BEGIN
         SELECT program_id 
         INTO   v_program_id
         FROM   structools.st_opt_program_run
         WHERE  program_id  = i_program_id
         AND    run_id = i_run_id
         AND    program_run_component = i_program_run_component;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN 
            dbms_output.put_line('PROGRAM '||i_PROGRAM_ID||', Run ID' || i_RUN_ID || ' does NOT exist IN st_opt_program_run');
            RAISE v_request_not_valid;
      END;
   
      DELETE FROM structools.st_opt_program_run
       WHERE PROGRAM_ID = i_PROGRAM_ID
         AND RUN_ID = i_RUN_ID
         AND program_run_component = i_program_run_component;
      COMMIT;
               
   EXCEPTION
      WHEN v_request_not_valid THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END delete_st_opt_program_run;

/*****************************************************************/
/* Package initialisation                                        */
/*****************************************************************/

BEGIN

   NULL;
   
END housekeeping;
/
