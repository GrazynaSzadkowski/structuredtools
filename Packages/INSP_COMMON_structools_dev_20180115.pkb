CREATE OR REPLACE PACKAGE BODY structoolsapp.insp_common
AS

   /*****************************************************************************************************************************************/
   /* 22/08/17 Grazyna Szadkowski   Created.                                                                                                                                                                                                                   */
   /*****************************************************************************************************************************************/

   PROCEDURE determine_insp(i_yr IN PLS_INTEGER, i_last_full_insp_yr IN PLS_INTEGER, i_insp_eqp_rec IN insp_eqp_csr%ROWTYPE, o_insp_typ OUT varchar2, o_insp_scope OUT varchar2) IS
                
   	v_insp_typ					VARCHAR2(10);
   BEGIN

      o_insp_typ              := NULL;
      o_insp_scope          := NULL;

		/* Determine inspection type */
      IF i_insp_eqp_rec.fire_risk_zone_cls = 'H' OR i_insp_eqp_rec.fire_risk_zone_cls = 'X' 
      OR i_insp_eqp_rec.urbn_inspn_classfn = 'HIGH'			THEN
         IF i_yr - i_last_full_insp_yr >= 6 THEN
            o_insp_typ := 'FULL';
         ELSIF i_yr - i_last_full_insp_yr <= 2 THEN
            o_insp_typ := 'VEG';
         ELSIF i_yr - i_last_full_insp_yr = 3 THEN
            o_insp_typ := 'VIS';
         ELSIF i_yr - i_last_full_insp_yr <= 5 THEN
            o_insp_typ := 'VEG';
         END IF;
      ELSE
         IF i_yr - i_last_full_insp_yr >= 6 THEN
            o_insp_typ := 'FULL';
         ELSIF i_yr - i_last_full_insp_yr = 2 THEN
            o_insp_typ := 'VEG';
         ELSIF i_yr - i_last_full_insp_yr = 3 THEN
            o_insp_typ := 'VIS';
         ELSIF i_yr - i_last_full_insp_yr = 4 THEN
            o_insp_typ := 'VEG';
         END IF;
		END IF;
      
      v_insp_typ := o_insp_typ;

		/* Determine inspection scope */
      determine_insp_scope(i_yr,  v_insp_typ, i_insp_eqp_rec, o_insp_scope);

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
         
   END determine_insp;   
 
	PROCEDURE determine_insp_scope(i_yr IN PLS_INTEGER, i_insp_typ IN VARCHAR2, i_insp_eqp_rec IN insp_eqp_csr%ROWTYPE, o_insp_scope OUT varchar2) IS
     
   BEGIN
   
      IF i_insp_typ IS NOT NULL THEN
         o_insp_scope := 'FULL';
         IF i_insp_typ = 'FULL' AND i_insp_eqp_rec.equip_grp_id = 'PWOD' THEN
            IF i_insp_eqp_rec.reinf_cde IS NULL AND i_insp_eqp_rec.wood_type = 'H' AND (i_yr - i_insp_eqp_rec.instln_year) <= 8
            OR i_insp_eqp_rec.reinf_cde IS NULL AND i_insp_eqp_rec.wood_type = 'S' AND (i_yr - i_insp_eqp_rec.instln_year) <= 20
            OR i_insp_eqp_rec.reinf_cde IS NOT NULL AND i_insp_eqp_rec.adeq_strength = 'Y' AND i_insp_eqp_rec.defective_reinf != 'Y' THEN
               o_insp_scope := 'REDUCED';
            END IF;
         ELSIF i_insp_typ = 'VIS' THEN
            IF  i_insp_eqp_rec.fire_risk_zone_cls != 'H' AND i_insp_eqp_rec.fire_risk_zone_cls != 'X' AND i_insp_eqp_rec.urbn_inspn_classfn != 'HIGH' THEN
               o_insp_scope := 'REDUCED';
            END IF;
         END IF;
      END IF;

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
         
   END determine_insp_scope;   

END insp_common;
/
