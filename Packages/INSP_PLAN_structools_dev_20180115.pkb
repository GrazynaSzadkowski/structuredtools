CREATE OR REPLACE PACKAGE BODY structoolsapp.insp_plan
AS

   /*****************************************************************************************************************************************/
   /* 22/08/17 Grazyna Szadkowski   Created.                                                                                                                                                                                                                   */
   /*****************************************************************************************************************************************/
	
   v_cycle_yrs											CONSTANT PLS_INTEGER := 6;

   TYPE float_126_tab_typ             IS TABLE OF FLOAT(126);
   TYPE risk_array_typ                  IS TABLE OF float_126_tab_typ INDEX BY VARCHAR2(10);
   
   v_this_year                   PLS_INTEGER := EXTRACT (YEAR FROM SYSDATE);
   v_prog_excp                 EXCEPTION;
   v_request_not_valid      EXCEPTION;
	v_no_action_required		EXCEPTION;

   v_first_year                     PLS_INTEGER;
   v_last_year                      PLS_INTEGER;
   v_run_years							PLS_INTEGER;
   v_smoothing_ind				PLS_INTEGER;
   
   v_risk_array						risk_array_typ;

	/* Cursors */

   -- cursor to get risk of noaction for PWODs, for the first 20 years
   CURSOR risk_pwod_noaction_csr IS
      SELECT    'S'||pick_id        AS pick_id
                    ,risk_indx_val_y0     
                    ,risk_indx_val_y1     
                    ,risk_indx_val_y2     
                    ,risk_indx_val_y3     
                    ,risk_indx_val_y4     
                    ,risk_indx_val_y5     
                    ,risk_indx_val_y6     
                    ,risk_indx_val_y7     
                    ,risk_indx_val_y8     
                    ,risk_indx_val_y9     
                    ,risk_indx_val_y10     
                    ,risk_indx_val_y11     
                    ,risk_indx_val_y12    
                    ,risk_indx_val_y13    
                    ,risk_indx_val_y14    
                    ,risk_indx_val_y15    
                    ,risk_indx_val_y16    
                    ,risk_indx_val_y17    
                    ,risk_indx_val_y18    
                    ,risk_indx_val_y19
                    ,risk_indx_val_y20
                    ,risk_indx_val_y21
      FROM   structools.st_risk_pwod_noaction   A                       
   ;                       

   PROCEDURE validate_run_request (i_run_id IN INTEGER) IS
         
      v_run_id                            PLS_INTEGER;
      v_run_start_ts                   DATE;
      v_run_end_ts                    DATE;
      v_smooth_start_ts                   DATE;
      v_smooth_end_ts                    DATE;
      
   BEGIN

      SELECT   run_id
                   ,run_years
                   ,first_year
                   ,run_start_ts
                   ,run_end_ts
                   ,smoothing_ind
                   ,smooth_start_ts
                   ,smooth_end_ts
         INTO    v_run_id
                   ,v_run_years
                   ,v_first_year
                   ,v_run_start_ts
                   ,v_run_end_ts
                   ,v_smoothing_ind
                   ,v_smooth_start_ts
                   ,v_smooth_end_ts
         FROM       structools.insp_run_request    
         WHERE      run_id = i_run_id 
         ;

      dbms_output.put_line('processing request = '|| i_run_id||' '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
      dbms_output.put_line('v_run_id = '||v_run_id);
      dbms_output.put_line('v_first_year := '||v_first_year);
      dbms_output.put_line('v_run_years := '||v_run_years);
         
      IF v_run_start_ts IS NOT NULL THEN
         dbms_output.put_line('Run '||i_run_id||' has run_ts_start NOT NULL');
         RAISE v_request_not_valid;
      END IF;

      IF v_smooth_start_ts IS NOT NULL THEN
         dbms_output.put_line('Run '||i_run_id||' has smooth_ts_start NOT NULL');
         RAISE v_request_not_valid;
      END IF;
		            
      IF v_run_end_ts IS NOT NULL THEN
         dbms_output.put_line('Run '||i_run_id||' has run_ts_end NOT NULL');
         RAISE v_request_not_valid;
      END IF;

      IF v_smooth_end_ts IS NOT NULL THEN
         dbms_output.put_line('Run '||i_run_id||' has smooth_run_ts_end NOT NULL');
         RAISE v_request_not_valid;
      END IF;

      IF v_smoothing_ind NOT IN (0,1,2,3) THEN
         dbms_output.put_line('Run '||i_run_id||' has invalid smoothing_ind');
         RAISE v_request_not_valid;
      END IF;

      BEGIN
         SELECT run_id 
         INTO   v_run_id
         FROM   structools.insp_plan_run
         WHERE run_id = i_run_id
         AND ROWNUM = 1;
         dbms_output.put_line('Data already exists for run_id '||v_run_id||' in tale INSP_PLAN_RUN');
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;

      BEGIN
         SELECT run_id 
         INTO   v_run_id
         FROM   structools.insp_plan_run_sm
         WHERE run_id = i_run_id
         AND ROWNUM = 1;
         dbms_output.put_line('Data already exists for run_id '||v_run_id||' in tale INSP_PLAN_RUN_SM');
         RAISE v_request_not_valid;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;

      v_last_year     := v_first_year - 1 + v_run_years; 

      IF v_first_year <= v_this_year THEN
         dbms_output.put_line('Run request '||i_run_id||' - first_year must be > '||v_this_year);
         RAISE v_request_not_valid;
      END IF;
            
   EXCEPTION
      WHEN v_request_not_valid THEN
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
                                           
   END validate_run_request;

   PROCEDURE initialise_insp_run IS
   /*******************************************************************************/
   /* Load what can be loaded from db into internal tables, initialise values etc */
   /*******************************************************************************/ 

		v_risk_tab				float_126_tab_typ := float_126_tab_typ();
 
   /* Mainline of Initialise_insp_run */
   BEGIN

      /* get risk values for poles into memory (associative array, indexed by pickid) */  
        
      FOR c IN risk_pwod_noaction_csr LOOP
         v_risk_tab.DELETE;
         v_risk_tab.EXTEND(21);
         v_risk_tab(1) :=c.risk_indx_val_y0; 
         v_risk_tab(2) :=c.risk_indx_val_y1;
         v_risk_tab(3) :=c.risk_indx_val_y2;
         v_risk_tab(4) :=c.risk_indx_val_y3;
         v_risk_tab(5) :=c.risk_indx_val_y4;
         v_risk_tab(6) :=c.risk_indx_val_y5;
         v_risk_tab(7) :=c.risk_indx_val_y6;
         v_risk_tab(8) :=c.risk_indx_val_y7;
         v_risk_tab(9) :=c.risk_indx_val_y8;
         v_risk_tab(10) :=c.risk_indx_val_y9;
         v_risk_tab(11) :=c.risk_indx_val_y10;
         v_risk_tab(12) :=c.risk_indx_val_y11;
         v_risk_tab(13) :=c.risk_indx_val_y12;
         v_risk_tab(14) :=c.risk_indx_val_y13;
         v_risk_tab(15) :=c.risk_indx_val_y14;
         v_risk_tab(16) :=c.risk_indx_val_y15;
         v_risk_tab(17) :=c.risk_indx_val_y16;
         v_risk_tab(18) :=c.risk_indx_val_y17;
         v_risk_tab(19) :=c.risk_indx_val_y18;
         v_risk_tab(20) :=c.risk_indx_val_y19;
         v_risk_tab(21) :=c.risk_indx_val_y20;
         v_risk_array(c.pick_id) := v_risk_tab;    -- store 20 years of risk in an associative array, indexed by pick_id
      END LOOP;

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END initialise_insp_run;

   PROCEDURE process_pole(i_run_id	IN PLS_INTEGER, i_insp_eqp_rec IN  structoolsapp.insp_common.insp_eqp_csr%ROWTYPE) IS
   
   	v_last_full_insp_yr		PLS_INTEGER;
      v_insp_typ						VARCHAR2(10) := NULL;
      v_insp_plan_rec			structools.insp_plan_run%ROWTYPE;
      v_risk_tab						float_126_tab_typ;
      v_risk_idx						PLS_INTEGER;
      v_insp_scope            varchar2(10);
           
   BEGIN                                                   
      
--      dbms_output.put_line('Processing pole '||i_insp_eqp_rec.pick_id||' '||i_insp_eqp_rec.instln_year||' '||i_insp_eqp_rec.wood_type
--                           ||' '||i_insp_eqp_rec.reinf_cde||' '||i_insp_eqp_rec.fire_risk_zone_cls||' '||i_insp_eqp_rec.urbn_inspn_classfn||' '||i_insp_eqp_rec.reinf_cde_dw
--                           ||' '||i_insp_eqp_rec.last_full_insp_yr||' '||i_insp_eqp_rec.defective_reinf);
                              
      /* populate fields in insp_plan_run, common for all years */
      v_insp_plan_rec.run_id := i_run_id;
      v_insp_plan_rec.pick_id := i_insp_eqp_rec.pick_id;
      v_insp_plan_rec.equip_grp_id := i_insp_eqp_rec.equip_grp_id;
      v_insp_plan_rec.maint_zone_nam := i_insp_eqp_rec.maint_zone_nam;
      v_insp_plan_rec.insp_cost := 0;
      v_insp_plan_rec.reinf_cde := i_insp_eqp_rec.reinf_cde;
      v_last_full_insp_yr := i_insp_eqp_rec.last_full_insp_yr;
      IF v_last_full_insp_yr IS NULL THEN
         v_last_full_insp_yr := i_insp_eqp_rec.instln_year;
      END IF;
      
      IF v_risk_array.EXISTS(i_insp_eqp_rec.pick_id) THEN
      	v_risk_tab := v_risk_array(i_insp_eqp_rec.pick_id);
      ELSE
      	v_risk_tab := NULL;
      END IF;

      FOR v_yr IN v_first_year .. v_last_year	LOOP
      
			structoolsapp.insp_common.determine_insp(v_yr, v_last_full_insp_yr, i_insp_eqp_rec, v_insp_typ, v_insp_scope);

         v_insp_plan_rec.last_full_insp_yr := v_last_full_insp_yr;
         v_insp_plan_rec.insp_type := v_insp_typ;
         v_insp_plan_rec.insp_scope := v_insp_scope;
         v_insp_plan_rec.calendar_year := v_yr;
         v_insp_plan_rec.age := v_yr - i_insp_eqp_rec.instln_year;
         IF v_risk_tab IS NOT NULL THEN
            v_risk_idx := v_yr - v_first_year + 2;
            v_insp_plan_rec.risk_noaction := v_risk_tab(v_risk_idx);
         ELSE
         	v_insp_plan_rec.risk_noaction := NULL;
         END IF;
         v_insp_plan_rec.last_full_insp_yr := v_last_full_insp_yr;

--         dbms_output.put_line('inserting '||v_insp_plan_rec.run_id||' '||v_insp_plan_rec.pick_id||' '||v_insp_plan_rec.calendar_year);
         
         INSERT INTO structools.insp_plan_run
            (
            run_id
            ,pick_id
            ,equip_grp_id
            ,risk_noaction
            ,age
            ,calendar_year
            ,maint_zone_nam
            ,insp_cost
            ,last_full_insp_yr
            ,insp_type
            ,insp_scope
            ,reinf_cde
            )
            VALUES
            (
            v_insp_plan_rec.run_id
            ,v_insp_plan_rec.pick_id
            ,v_insp_plan_rec.equip_grp_id
            ,v_insp_plan_rec.risk_noaction
            ,v_insp_plan_rec.age
            ,v_insp_plan_rec.calendar_year
            ,v_insp_plan_rec.maint_zone_nam
            ,v_insp_plan_rec.insp_cost
            ,v_insp_plan_rec.last_full_insp_yr
            ,v_insp_plan_rec.insp_type
            ,v_insp_plan_rec.insp_scope
            ,v_insp_plan_rec.reinf_cde
            )
         ;
         IF v_insp_typ = 'FULL' THEN
         	v_last_full_insp_yr := v_yr;
			END IF;
         
      END LOOP;   
         
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END process_pole;

   PROCEDURE create_insp_plan(i_run_id IN PLS_INTEGER) IS
      
      v_insp_eqp_rec          structoolsapp.insp_common.insp_eqp_csr%ROWTYPE;
         
   BEGIN
   
      structools.schema_utils.disable_table_indexes('INSP_PLAN_RUN');

      FOR v_insp_eqp_rec IN structoolsapp.insp_common.insp_eqp_csr LOOP 
         process_pole(i_run_id, v_insp_eqp_rec);
      END LOOP;

      structools.schema_utils.enable_table_indexes('INSP_PLAN_RUN');

   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END create_insp_plan;
      
   PROCEDURE apply_smoothing(i_run_id IN PLS_INTEGER, i_smoothing_ind IN PLS_INTEGER) IS

      CURSOR pickids_to_shift_csr IS

         WITH pickid_insp_type AS
         (
         SELECT * FROM
         (
         SELECT pick_id
         			,CASE calendar_year
                  	WHEN v_first_year							THEN 'YR_1'
                     WHEN v_first_year + 1				 THEN 'YR_2'
                     WHEN v_first_year + 2				 THEN 'YR_3'
                     WHEN v_first_year + 3				 THEN 'YR_4'
                     WHEN v_first_year + 4				 THEN 'YR_5'
                     WHEN v_first_year + 5				 THEN 'YR_6'
                  END															AS cal_year_idx
                  ,nvl(insp_type, ' ') 								AS insp
         FROM structools.insp_plan_run
         WHERE run_id = i_run_id
         )
         pivot (MAX(insp) AS insp FOR cal_year_idx IN ('YR_1' yr_1, 'YR_2' yr_2, 'YR_3' yr_3, 'YR_4' yr_4, 'YR_5' yr_5, 'YR_6' yr_6))  -- check if year can be a variable. 
         )
         ,
         pickid_pattern AS
         (
         SELECT A.pick_id
                     ,CASE
                        WHEN	yr_1_insp = 'FULL' AND yr_2_insp = 'VEG' AND yr_3_insp = 'VEG' AND yr_4_insp = 'VIS' AND yr_5_insp = 'VEG' AND yr_6_insp = 'VEG'	THEN	1
                        WHEN	yr_1_insp = 'VEG' AND yr_2_insp = 'VEG' AND yr_3_insp = 'VIS' AND yr_4_insp = 'VEG' AND yr_5_insp = 'VEG' AND yr_6_insp = 'FULL'	THEN	2
                        WHEN	yr_1_insp = 'VEG' AND yr_2_insp = 'VIS' AND yr_3_insp = 'VEG' AND yr_4_insp = 'VEG' AND yr_5_insp = 'FULL' AND yr_6_insp = 'VEG'	THEN	3
                        WHEN	yr_1_insp = 'VIS' AND yr_2_insp = 'VEG' AND yr_3_insp = 'VEG' AND yr_4_insp = 'FULL' AND yr_5_insp = 'VEG' AND yr_6_insp = 'VEG'	THEN	4
                        WHEN	yr_1_insp = 'VEG' AND yr_2_insp = 'VEG' AND yr_3_insp = 'FULL' AND yr_4_insp = 'VEG' AND yr_5_insp = 'VEG' AND yr_6_insp = 'VIS'	THEN	5
                        WHEN	yr_1_insp = 'VEG' AND yr_2_insp = 'FULL' AND yr_3_insp = 'VEG' AND yr_4_insp = 'VEG' AND yr_5_insp = 'VIS' AND yr_6_insp = 'VEG'	THEN	6
                        WHEN	yr_1_insp = 'FULL' AND yr_2_insp = ' ' AND yr_3_insp = 'VEG' AND yr_4_insp = 'VIS' AND yr_5_insp = 'VEG' AND yr_6_insp = ' '			   THEN	7
                        WHEN	yr_1_insp = ' ' AND yr_2_insp = 'VEG' AND yr_3_insp = 'VIS' AND yr_4_insp = 'VEG' AND yr_5_insp = ' ' AND yr_6_insp = 'FULL'			   THEN	8
                        WHEN	yr_1_insp = 'VEG' AND yr_2_insp = 'VIS' AND yr_3_insp = 'VEG' AND yr_4_insp = ' ' AND yr_5_insp = 'FULL' AND yr_6_insp = ' '			   THEN	9
                        WHEN	yr_1_insp = 'VIS' AND yr_2_insp = 'VEG' AND yr_3_insp = ' ' AND yr_4_insp = 'FULL' AND yr_5_insp = ' ' AND yr_6_insp = 'VEG'			   THEN	10
                        WHEN	yr_1_insp = 'VEG' AND yr_2_insp = ' ' AND yr_3_insp = 'FULL' AND yr_4_insp = ' ' AND yr_5_insp = 'VEG' AND yr_6_insp = 'VIS'			   THEN	11
                        WHEN	yr_1_insp = ' ' AND yr_2_insp = 'FULL' AND yr_3_insp = ' ' AND yr_4_insp = 'VEG' AND yr_5_insp = 'VIS' AND yr_6_insp = 'VEG'			   THEN	12
                        ELSE			0
                     END				AS pattern
         FROM pickid_insp_type             A
      	)
      	,
         eqp AS
         (
         SELECT A.pick_id
         				,A.equip_grp_id
                     ,A.maint_zone_nam
                     ,b.instln_year
                     ,b.wood_type
                     ,A.reinf_cde
                     ,A.insp_cost
                     ,b.fire_risk_zone_cls
                     ,b.urbn_inspn_classfn
                     ,b.reinf_cde_dw
                     ,A.last_full_insp_yr
                     ,A.insp_type
                     ,c.pattern
                     ,CASE
                     	WHEN i_smoothing_ind = 1			AND c.pattern IN (3,4,5,6) 						THEN 1
                        WHEN i_smoothing_ind = 2			AND c.pattern IN (3,4,5,6,9,10,11,12)   THEN 1
                        WHEN i_smoothing_ind = 3			AND c.pattern IN (3,4,5,6) 						THEN 2
                        WHEN i_smoothing_ind = 3			AND c.pattern IN (9,10,11,12) 				 THEN 1
                        ELSE																															 					0
                     END						AS shift_val
         FROM structools.insp_plan_run			A	INNER JOIN
                  structools.st_equipment			b
         ON A.pick_id = b.pick_id           
         																		INNER JOIN
						pickid_pattern							c
			ON A.pick_id = c.pick_id                                                                                    
         WHERE A.run_id = i_run_id
         AND A.calendar_year = v_first_year
         AND b.part_cde = ' '
         )
         ,
         defective_reinforcing AS
         (
         SELECT DISTINCT pick_id
                  ,'Y'      AS defective_reinf
         FROM structools.st_eqp_defect
         WHERE equip_grp_id = 'PWOD'
         AND part_cde = 'REINF' 
         )
         SELECT A.pick_id
                  ,A.equip_grp_id
                  ,A.maint_zone_nam
                  ,A.instln_year
                  ,A.wood_type
                  ,A.reinf_cde
                  ,A.fire_risk_zone_cls
                  ,A.urbn_inspn_classfn
                  ,A.reinf_cde_dw
                  ,A.last_full_insp_yr
                  ,b.defective_reinf
                  ,c.adeq_strength
                  ,A.insp_cost
                  ,A.insp_type
                  ,A.pattern
                  ,A.shift_val
         FROM   eqp                                A   LEFT OUTER JOIN
                     defective_reinforcing       b
            ON A.pick_id = b.pick_id
                                                               LEFT OUTER JOIN
                     structools.insp_reinf_cde_map            c
            ON A.reinf_cde_dw = c.reinf_cde
      ;
      
      TYPE pickids_to_shift_tab_typ          IS TABLE OF pickids_to_shift_csr%ROWTYPE;
      v_pickids_to_shift_tab							pickids_to_shift_tab_typ := pickids_to_shift_tab_typ();
      
      CURSOR pickid_years_csr(i_pickid IN VARCHAR2) IS
      	SELECT risk_noaction
         				,age
                     ,calendar_year
                     ,insp_type
                     ,insp_scope
			FROM structools.insp_plan_run
         WHERE run_id = i_run_id
         AND pick_id =  i_pickid
         ORDER BY calendar_year                    
      ;
      
      TYPE pickid_years_tab_typ					IS TABLE OF pickid_years_csr%ROWTYPE;
      v_pickid_years_tab								pickid_years_tab_typ := pickid_years_tab_typ();
      
      v_run_id										PLS_INTEGER;
      v_smooth_start_ts					date;
      v_smooth_end_ts					date;
      v_shift_forward_scen_id		PLS_INTEGER;
      v_shift_forward_yrs								PLS_INTEGER;
      v_shift_forward_hx_frz_scen_id		PLS_INTEGER;
      v_shift_forward_hx_frz_yrs					PLS_INTEGER;
		l																	PLS_INTEGER;
      v_last_full_insp_yr									PLS_INTEGER;
      v_shift_from											PLS_INTEGER;
      v_insp_eqp_rec										structoolsapp.insp_common.insp_eqp_csr%ROWTYPE;
      v_insp_typ												varchar2(10);
      v_insp_scope 										 varchar2(10);
            
      /* Private procedures of apply_smoothing */
      
   	PROCEDURE validate_smoothing_request IS
      
      BEGIN
      
			IF i_smoothing_ind NOT IN (0,1,2,3) THEN
            dbms_output.put_line('Run '||i_run_id||' Smoothing_ind is not valid');
            RAISE v_request_not_valid;
         END IF;

			IF i_smoothing_ind = 0 THEN
            dbms_output.put_line('Run '||i_run_id||' Smoothing_ind = 0; no action performed');
            RAISE v_no_action_required;
         END IF;


         SELECT run_years
                   ,first_year
                   ,smooth_start_ts
                   ,smooth_end_ts
         INTO    v_run_years
                   ,v_first_year
                   ,v_smooth_start_ts
                   ,v_smooth_end_ts
         FROM       structools.insp_run_request    
         WHERE      run_id = i_run_id 
         ;

			v_last_year     := v_first_year - 1 + v_run_years;
         
         dbms_output.put_line('Smoothing start, run_id = '|| i_run_id||' '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
         dbms_output.put_line('i_run_id = '||i_run_id);
         dbms_output.put_line('v_first_year := '||v_first_year);
         dbms_output.put_line('v_run_years := '||v_run_years);

         IF v_smooth_end_ts IS NOT NULL THEN
            dbms_output.put_line('Run '||i_run_id||' has smooth_ts_end NOT NULL');
            RAISE v_request_not_valid;
         END IF;
         
         IF v_last_year < v_first_year + v_cycle_yrs - 1 THEN
            dbms_output.put_line('Smoothing not possible; Insp plan data must exist for at least '||v_cycle_yrs);
            RAISE v_request_not_valid;
         END IF;
         
            
         BEGIN
            SELECT run_id
            INTO   v_run_id
            FROM structools.insp_plan_run
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               dbms_output.put_line('Run '||i_run_id||' has smooth_ts_end NOT NULL');
               RAISE v_request_not_valid;
         END;            
               	
         BEGIN
            SELECT run_id
            INTO   v_run_id
            FROM structools.insp_plan_run_sm
            WHERE run_id = i_run_id
            AND ROWNUM = 1;
            dbms_output.put_line('Data already exist in table st_insp_run_sm for run_id '||i_run_id);
            RAISE v_request_not_valid;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;            
      
      EXCEPTION
         WHEN v_no_action_required THEN
				RAISE;
         WHEN v_request_not_valid THEN
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;
         WHEN OTHERS THEN
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;
      END validate_smoothing_request;

      
   BEGIN /* Mainline of apply_smoothing */
   
   	validate_smoothing_request;

   	UPDATE structools.insp_run_request
      set smooth_start_ts = sysdate
      WHERE run_id = i_run_id;
         
		OPEN pickids_to_shift_csr;
      FETCH pickids_to_shift_csr BULK COLLECT INTO v_pickids_to_shift_tab;
      CLOSE pickids_to_shift_csr;
      
      structools.schema_utils.disable_table_indexes('INSP_PLAN_RUN_SM');

      FOR i IN 1 .. v_pickids_to_shift_tab.count LOOP
      	OPEN pickid_years_csr(v_pickids_to_shift_tab(i).pick_id);
         FETCH pickid_years_csr BULK COLLECT INTO v_pickid_years_tab;
         CLOSE pickid_years_csr;
         
         v_shift_from := v_pickids_to_shift_tab(i).shift_val + 1;
         
			IF v_pickids_to_shift_tab(i).shift_val = 0 THEN
            v_shift_from := 1;			/* will cause an exact copy from insp_plan_run to insp_plan_run_sm, without any time shift */
         ELSE
            FOR j IN 2 .. (v_pickids_to_shift_tab(i).shift_val + 1)  LOOP
               IF v_pickid_years_tab(j).insp_type = 'FULL'
               AND (v_pickid_years_tab(j).calendar_year - v_first_year) < v_pickids_to_shift_tab(i).shift_val THEN
                  v_shift_from := j;
               END IF;
               EXIT;
            END LOOP;
         END IF;
      	
      	/* insert rows into insp_plan_run_sm for all years for the pickid */
      	l := 0;
         v_last_full_insp_yr := v_pickids_to_shift_tab(i).last_full_insp_yr;
         
      	FOR K IN v_shift_from .. v_run_years LOOP
         	l := l + 1;
				v_insp_scope := v_pickid_years_tab(K).insp_scope;		--take insp scope from the from-year, exception FULL for PWOds
            IF v_pickid_years_tab(K).insp_type = 'FULL'  AND v_pickids_to_shift_tab(i).equip_grp_id = 'PWOD' THEN --only for FULL insp type for PWOD, there is a possibility theta insp scope may be different aftre the shift          
            	v_insp_eqp_rec.equip_grp_id := 'PWOD';
               v_insp_eqp_rec.reinf_cde := v_pickids_to_shift_tab(i).reinf_cde;
               v_insp_eqp_rec.wood_type := v_pickids_to_shift_tab(i).wood_type;
               v_insp_eqp_rec.instln_year := v_pickids_to_shift_tab(i).instln_year;
               v_insp_eqp_rec.adeq_strength := v_pickids_to_shift_tab(i).adeq_strength;
               v_insp_eqp_rec.defective_reinf := v_pickids_to_shift_tab(i).defective_reinf;
               structoolsapp.insp_common.determine_insp_scope(v_pickid_years_tab(l).calendar_year, 'FULL', v_insp_eqp_rec, v_insp_scope);
            END IF;
            
            BEGIN
               INSERT INTO structools.insp_plan_run_sm
                  (run_id
                  ,pick_id
                  ,equip_grp_id
                  ,risk_noaction
                  ,age
                  ,calendar_year
                  ,maint_zone_nam
                  ,insp_cost
                  ,last_full_insp_yr
                  ,insp_type
                  ,insp_scope
                  ,reinf_cde)
               VALUES	
                  (i_run_id
                  ,v_pickids_to_shift_tab(i).pick_id
                  ,v_pickids_to_shift_tab(i).equip_grp_id
                  ,v_pickid_years_tab(l).risk_noaction
                  ,v_pickid_years_tab(l).age
                  ,v_pickid_years_tab(l).calendar_year
                  ,v_pickids_to_shift_tab(i).maint_zone_nam
                  ,v_pickids_to_shift_tab(i).insp_cost
                  ,v_last_full_insp_yr
                  ,v_pickid_years_tab(K).insp_type
                  ,v_insp_scope
                  ,v_pickids_to_shift_tab(i).reinf_cde)
               ;
            EXCEPTION
            	WHEN others THEN
               	dbms_output.put_line('pickid '||v_pickids_to_shift_tab(i).pick_id);
               	dbms_output.put_line('v_shift_from = '||v_shift_from||' K = '||K||' l = '||l);
               	FOR z IN 1 .. v_pickid_years_tab.count LOOP
                  	dbms_output.put_line(z||'  '|| v_pickid_years_tab(z).calendar_year);
                  END LOOP;
                  RAISE;
               	
            END;
            
            IF v_pickid_years_tab(K).insp_type = 'FULL' THEN
            	v_last_full_insp_yr := v_pickid_years_tab(l).calendar_year;
            END IF;
            
               
         END LOOP;

         /* fill in remaining years */
         WHILE l < v_run_years LOOP 
            l := l + 1;
            v_insp_eqp_rec.pick_id := v_pickids_to_shift_tab(i).pick_id;
            v_insp_eqp_rec.equip_grp_id := v_pickids_to_shift_tab(i).equip_grp_id;
            v_insp_eqp_rec.maint_zone_nam := v_pickids_to_shift_tab(i).maint_zone_nam;
            v_insp_eqp_rec.instln_year := v_pickids_to_shift_tab(i).instln_year;
            v_insp_eqp_rec.wood_type := v_pickids_to_shift_tab(i).wood_type;
            v_insp_eqp_rec.reinf_cde := v_pickids_to_shift_tab(i).reinf_cde;
            v_insp_eqp_rec.fire_risk_zone_cls := v_pickids_to_shift_tab(i).fire_risk_zone_cls;
            v_insp_eqp_rec.urbn_inspn_classfn := v_pickids_to_shift_tab(i).urbn_inspn_classfn;
            v_insp_eqp_rec.reinf_cde_dw := v_pickids_to_shift_tab(i).reinf_cde_dw;
            v_insp_eqp_rec.last_full_insp_yr := v_pickids_to_shift_tab(i).last_full_insp_yr;
            v_insp_eqp_rec.defective_reinf := v_pickids_to_shift_tab(i).defective_reinf;
            v_insp_eqp_rec.adeq_strength := v_pickids_to_shift_tab(i).adeq_strength;
            structoolsapp.insp_common.determine_insp(v_pickid_years_tab(l).calendar_year, v_last_full_insp_yr, v_insp_eqp_rec, v_insp_typ, v_insp_scope);
            INSERT INTO structools.insp_plan_run_sm
               (run_id
               ,pick_id
               ,equip_grp_id
               ,risk_noaction
               ,age
               ,calendar_year
               ,maint_zone_nam
               ,insp_cost
               ,last_full_insp_yr
               ,insp_type
               ,insp_scope
               ,reinf_cde)
            VALUES	
               (i_run_id
               ,v_pickids_to_shift_tab(i).pick_id
               ,v_pickids_to_shift_tab(i).equip_grp_id
               ,v_pickid_years_tab(l).risk_noaction
               ,v_pickid_years_tab(l).age
               ,v_pickid_years_tab(l).calendar_year
               ,v_pickids_to_shift_tab(i).maint_zone_nam
               ,v_pickids_to_shift_tab(i).insp_cost
               ,v_last_full_insp_yr
               ,v_insp_typ
               ,v_insp_scope
               ,v_pickids_to_shift_tab(i).reinf_cde)
            ;
         END LOOP;

      END LOOP;

      structools.schema_utils.enable_table_indexes('INSP_PLAN_RUN_SM');

   	UPDATE structools.insp_run_request
      set smooth_end_ts = sysdate
      WHERE run_id = i_run_id;

      dbms_output.put_line('Smoothing END, run_id = '|| i_run_id||' '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
      
      COMMIT;
            
   EXCEPTION
         WHEN v_no_action_required THEN
         	dbms_output.put_line('EXCEPTION v_no_action_required');
				NULL;
      WHEN v_request_not_valid THEN
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END apply_smoothing;
      

   PROCEDURE execute_run_request(i_run_id  IN  PLS_INTEGER) IS

      /*****************************************************************/
      /* Global declarations for execute_run_request procedure               */
      /*****************************************************************/

      v_risk_array                     risk_array_typ;

   BEGIN

      validate_run_request(i_run_id);
      
      initialise_insp_run;
      
      UPDATE structools.insp_run_request
         set run_start_ts = SYSDATE
      WHERE run_id = i_run_id;
         
      create_insp_plan(i_run_id);
      
      UPDATE structools.insp_run_request
         SET  run_end_ts = SYSDATE
         WHERE run_id = i_run_id;

      IF v_smoothing_ind != 0 THEN
      	apply_smoothing(i_run_id, v_smoothing_ind);
      END IF;
      
      --ROLLBACK;
      COMMIT;
      
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END execute_run_request;


/*****************************************************************/
/* Package initialisation                                                                                               */
/*****************************************************************/

BEGIN

   NULL;
   
END insp_plan;
/
