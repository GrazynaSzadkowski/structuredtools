CREATE OR REPLACE PACKAGE BODY structoolsapp.reporting
AS

   /****************************************************************************************************************************************/
   /* H6 Changes:                                                                                                                                                                                                                                                          */
   /*		28/11/17 Grazyna Szadkowski	Modified  to cater for suppressed pickids.                                                                                                                                                                */
   /*		21/11/17 Grazyna Szadkowski	Modified procedure generate_incd_forecast to cater for reinforcements done as part of zonal reinforcement program.                                                   */
   /*																	 Modified procedure generate_post_del_risk_profile to cater for reinforcements done as part of zonal reinforcement program.                                         */
   /*    14/08/17 Grazyna Szadkowski   Created by extracting various reporting procs from structured_tools.pkb.                                                                                                                    */
   /*    26/09/17 Grazyna Szadkowski	Created procedure generate_user_prog_risk_prof to generate risk profile given user provided list of pickids and events.                                                 */
   /*****************************************************************************************************************************************/
   

   v_this_year                   PLS_INTEGER := EXTRACT (YEAR FROM SYSDATE);
   v_year_0                      PLS_INTEGER := EXTRACT (YEAR FROM SYSDATE);
   v_null_end_dt                     CONSTANT DATE := TO_DATE('01-01-3000','dd-mm-yyyy');
   v_prog_excp                   EXCEPTION;
   
   v_request_not_valid     EXCEPTION;

   v_parm_scen_id_life_exp          PLS_INTEGER;
   v_parm_scen_id_seg_min_len          PLS_INTEGER;
   v_parm_scen_id_seg_cons_pcnt     PLS_INTEGER;
   v_parm_scen_id_sif_weighting     PLS_INTEGER;
   v_parm_scen_id_seg_rr_doll            PLS_INTEGER;        -- single scenario id applies to the whole set, ie all years
   v_parm_scen_id_eqp_rr_doll            PLS_INTEGER;        -- single scenario id applies to the whole set, ie all years
   v_parm_scen_id_fdr_cat_adj       PLS_INTEGER;
   

   /*---------------------------------------------------------------------------------------------------------------*/
   /* Generates a list of pickids treated for a given run.                                                          */
   /* If the one logical run consists of multiple physical runs (eg 5 * 10_year run for a 50-year outlook), this    */
   /* procedure is to be run after each physical run. It relies on strategy_run to be populated in the              */
   /* st_temp_bay_strategy_run and st_temp_stratgey_run tables.                                                     */
   /*---------------------------------------------------------------------------------------------------------------*/    
   PROCEDURE generate_opt_eqp_list(i_program_id         IN PLS_INTEGER
                                  ,i_base_run_id        IN PLS_INTEGER
                                  ,i_delete_flg         IN VARCHAR2    DEFAULT 'N') IS

      v_prog_excp           EXCEPTION;
      v_request_not_valid   EXCEPTION;
      v_base_run_id         PLS_INTEGER;
      v_first_year          PLS_INTEGER;
      v_existing_record_cnt PLS_INTEGER;

   BEGIN
   
      dbms_output.put_line('generate_opt_eqp_list Start '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

      SELECT COUNT(*)
      INTO   v_existing_record_cnt
      FROM   structools.ST_DEL_program_pickid
      WHERE  program_id = i_program_id
      AND    base_run_id = i_base_run_id;

      IF i_delete_flg = 'Y' THEN
         dbms_output.put_line('Deleting existing results');
         
         DELETE FROM structools.ST_DEL_program_pickid
         WHERE program_id = i_program_id
         AND   base_run_id = i_base_run_id;
      ELSE
         dbms_output.put_line('Existing results not deleted');
         
         IF v_existing_record_cnt > 0 THEN  
            dbms_output.put_line('Exception: Existing records found');
            RAISE v_request_not_valid;
         END IF;
      END IF;

      INSERT 
      INTO structools.ST_DEL_program_pickid
      (del_year
      ,mtzn
      ,program_id
      ,pick_id
      ,equip_grp_id
      ,event
      ,capex
      ,prog_type
      ,feeder_nam
      ,zone_subst_nam
      ,risk_reduction
      ,risk_reduction_npr
      ,risk_reduction_npr_sif
      ,event_reason
      ,base_run_id
      ,bay_equip_cde
      ,stc1
      ,stc2
      ,bay_len_m
      ,conductor_pick_id
      ,conductor_len_m
      ,failure_prob
      ,carrier_mdl_cde
      ,carrier_mdl_mat_cde
      ,age
      ,cons_segment_id
      ,p_mtzn
      ,s_mtzn
      ,npr_fire
      ,npr_eshock
      ,npr_wforce
      ,npr_reliability
      ,npr_physimp
      ,npr_env
      ,span_id
      ,wood_type
      ,part_cde
      ,npr_fin
      ,labour_hrs
      )
      WITH mtzn_program AS
      (
      SELECT del_year
            ,b.mtzn
      FROM structools.st_del_program_mtzn_optimised     A  INNER JOIN
           structools.st_mtzn                           b
      ON A.mtzn = b.mtzn_opt
      WHERE program_id = i_program_id
      AND  base_run_id = i_base_run_id
      AND del_year != 0        
      )
      ,
      mtzn_program_reinf AS
      (
      SELECT A.del_year
            ,b.mtzn
      FROM structools.st_del_program_reinf_opt     A  INNER JOIN
           structools.st_mtzn                           b
      ON A.mtzn = b.mtzn_opt
      WHERE program_id = i_program_id
      AND  base_run_id = i_base_run_id
      AND del_year != 0        
      )         
      ,
      seg_program AS
      (
      SELECT del_year
            ,cons_segment_id
      FROM structools.st_del_program_seg_optimised
      WHERE program_id = i_program_id
      AND  base_run_id = i_base_run_id
      AND del_year > 0
      )       
      ,
      zonal_and_seg_pole_eqp_pickids AS
      (
      SELECT A.del_year          AS mtzn_del_year
            ,b.mtzn_grp          AS mtzn
            ,b.pick_id
            ,b.equip_grp_id
            ,b.event
            ,b.capex
            ,b.risk_reduction
            ,b.risk_reduction_npv15 AS risk_reduction_npr
            ,b.risk_reduction_npv15_sif AS risk_reduction_npr_sif
            ,b.event_reason
            ,b.failure_prob
            ,b.age
            ,b.cons_segment_id
            ,b.maint_zone_nam               AS p_mtzn
            ,CASE
                    WHEN b.segment_rebuild_yr IS NOT NULL   THEN b.mtzn_grp
                    ELSE                                         NULL
            END                                 AS s_mtzn
           ,NULL                        AS span_id
           ,b.wood_type 
           ,b.part_cde               
           ,c.del_year           AS seg_del_year                                  --duplicate replaces between pole and xarm/stay on the pole have been eliminated 
           ,b.labour_hrs
           ,d.del_year           AS mtzn_reinf_del_year
      FROM     structools.st_temp_strategy_run           b   LEFT OUTER JOIN
               mtzn_program                           A
      ON   A.MTZN = B.mtzn_grp
      AND A.del_year = B.CALENDAR_YEAR
                                                         LEFT OUTER JOIN
               seg_program                           c
      ON   b.cons_segment_id = c.cons_segment_id
      AND  B.CALENDAR_YEAR = c.del_year
      																	LEFT OUTER JOIN
               mtzn_program_reinf                           d
      ON   d.MTZN = B.mtzn_grp
      AND d.del_year = B.CALENDAR_YEAR
      WHERE B.EVENT > ' '
      AND (b.pickid_suppressed = 'N' OR b.pickid_suppressed = 'P' AND event = 'REINFORCE')
      )  
      ,
      zonal_and_seg_bay_pickids AS
      (
      SELECT A.del_year          AS mtzn_del_year
            ,b.mtzn_grp          AS mtzn
            ,b.pick_id
            ,b.equip_grp_id
            ,b.event
            ,b.capex
            ,b.risk_reduction
            ,b.risk_reduction_npv15 AS risk_reduction_npr
            ,b.risk_reduction_npv15_sif AS risk_reduction_npr_sif
            ,b.event_reason
            ,NULL                            AS failure_prob
            ,b.age
            ,b.cons_segment_id
            ,b.maint_zone_nam               AS p_mtzn
            ,CASE
                    WHEN b.segment_rebuild_yr IS NOT NULL   THEN b.mtzn_grp
                    ELSE                                         NULL
            END                                 AS s_mtzn 
           ,b.vic_id                            AS span_id
           ,' '                  AS wood_type 
           ,' '                  AS part_cde               
           ,c.del_year           AS seg_del_year                                  --duplicate replaces between pole and xarm/stay on the pole have been eliminated 
           ,b.labour_hrs
           ,NULL			           AS mtzn_reinf_del_year
      FROM     structools.st_temp_bay_strategy_run         b   LEFT OUTER JOIN
               mtzn_program                           A
      ON   A.MTZN = B.mtzn_grp
      AND A.del_year = B.CALENDAR_YEAR
                                                         LEFT OUTER JOIN
               seg_program                           c
      ON   b.cons_segment_id = c.cons_segment_id
      AND  B.CALENDAR_YEAR = c.del_year
      WHERE B.EVENT > ' '
      AND b.segment_rebuild_yr IS NOT NULL
      AND b.pickid_suppressed = 'N'
      )  
      ,
      pickids AS
      (
      SELECT 
      		CASE
            	WHEN mtzn_del_year IS NOT NULL 		  THEN	mtzn_del_year	
               WHEN seg_del_year IS NOT NULL 			THEN	seg_del_year
               WHEN mtzn_reinf_del_year IS NOT NULL THEN	mtzn_reinf_del_year
               ELSE																					NULL
      		END 						AS del_year  
            ,mtzn
            ,pick_id
            ,equip_grp_id
            ,event
            ,capex
            ,risk_reduction
            ,risk_reduction_npr
            ,risk_reduction_npr_sif
            ,CASE
               WHEN mtzn_del_year IS NOT NULL         			THEN  'MTZN'
               WHEN seg_del_year IS NOT NULL         				THEN  'SEGMENT'
               WHEN mtzn_reinf_del_year IS NOT NULL         THEN  'REINF'
            END                     AS prog_type
            ,'1'           AS prog_seq
            ,event_reason
            ,failure_prob
            ,age
            ,cons_segment_id
            ,p_mtzn
            ,s_mtzn
            ,span_id
            ,wood_type 
            ,part_cde 
            ,labour_hrs
      FROM zonal_and_seg_pole_eqp_pickids
      WHERE mtzn_del_year IS NOT NULL
      OR seg_del_year IS NOT NULL
      OR (mtzn_reinf_del_year IS NOT NULL AND event = 'REINFORCE')
      UNION
      SELECT NVL(mtzn_del_year, seg_del_year)      AS del_year
            ,mtzn
            ,pick_id
            ,equip_grp_id
            ,event
            ,capex
            ,risk_reduction
            ,risk_reduction_npr
            ,risk_reduction_npr_sif
            ,CASE
               WHEN mtzn_del_year IS NOT NULL         THEN  'MTZN'
               ELSE                                         'SEGMENT'
            END                     AS prog_type
            ,'1'           AS prog_seq
            ,event_reason
            ,failure_prob
            ,age
            ,cons_segment_id
            ,p_mtzn
            ,s_mtzn
            ,span_id
            ,wood_type 
            ,part_cde 
            ,labour_hrs
      FROM zonal_and_seg_bay_pickids
      WHERE mtzn_del_year IS NOT NULL
      OR seg_del_year IS NOT NULL
      UNION
      SELECT A.del_year
            ,A.maint_zone_nam   AS mtzn
            ,A.pick_id
            ,A.equip_typ  AS equip_grp_id
            ,A.event
            ,A.capex          --it is important to use capex from st_del_program_sniper, as it is different from the one in st_strategy_run   
            ,b.risk_reduction
            ,b.risk_reduction_npv15 AS risk_reduction_npr
            ,b.risk_reduction_npv15_sif AS risk_reduction_npr_sif
            ,'SNIPER'      AS prog_type
            ,'1'           AS prog_seq
            ,b.event_reason
            ,b.failure_prob
            ,b.age
            ,b.cons_segment_id
            ,b.maint_zone_nam               AS p_mtzn
            ,CASE
                    WHEN b.segment_rebuild_yr IS NOT NULL   THEN b.mtzn_grp
                    ELSE                                                                    NULL
            END                                 AS s_mtzn 
           ,NULL                        AS span_id
           ,b.wood_type
           ,A.part_cde
           ,b.labour_hrs
      FROM structools.st_del_program_sniper    A  INNER JOIN
           structools.st_temp_strategy_run                 b
      ON   A.pick_id = b.pick_id
      AND A.part_cde = b.part_cde
      AND A.del_year = B.CALENDAR_YEAR
      AND A.base_run_id = b.run_id         
      WHERE A.program_id = i_program_id
      AND   A.base_run_id = i_base_run_id
      UNION
      SELECT A.del_year
            ,A.maint_zone_nam   AS mtzn
            ,A.pick_id
            ,A.equip_typ  AS equip_grp_id
            ,A.event
            ,A.capex             
            ,A.risk_reduction
            ,A.risk_reduction_npv15             AS risk_reduction_npr
            ,A.risk_reduction_npv15_sif         AS risk_reduction_npr_sif
            ,'REAC_MAINT'                       AS prog_type
            ,'2'                                AS prog_seq
            ,'REAC_MAINT'                       AS event_reason
            ,NULL                               AS failure_prob
            ,A.del_year - b.instln_year         AS age
            ,NULL                               AS cons_segment_id
            ,A.maint_zone_nam                   AS p_mtzn
            ,NULL                               AS s_mtzn
            ,NULL                               AS span_id
            ,b.wood_type
            ,A.part_cde
            ,b.labour_hrs
      FROM structools.st_del_program_reac_maint    A  INNER JOIN
           structools.st_equipment                 b
      ON    A.pick_id = b.pick_id
      AND   A.part_cde = b.part_cde
      WHERE A.program_id = i_program_id
      AND   A.base_run_id = i_base_run_id
      AND   A.equip_typ = 'PWOD'
      UNION
      SELECT A.del_year
            ,A.maint_zone_nam   AS mtzn
            ,A.pick_id
            ,A.equip_typ  AS equip_grp_id
            ,A.event
            ,A.capex             
            ,A.risk_reduction
            ,A.risk_reduction_npv15             AS risk_reduction_npr
            ,A.risk_reduction_npv15_sif         AS risk_reduction_npr_sif
            ,'REAC_MAINT'                       AS prog_type
            ,'2'                                AS prog_seq
            ,'REAC_MAINT'                       AS event_reason
            ,NULL                               AS failure_prob
            ,A.del_year - b.instln_year         AS age
            ,NULL                               AS cons_segment_id
            ,A.maint_zone_nam                   AS p_mtzn
            ,NULL                               AS s_mtzn
            ,NULL                               AS span_id
            ,NULL                               AS wood_type
            ,' '                               AS part_cde
            ,NULL									 AS labour_hrs
      FROM structools.st_del_program_reac_maint    A  INNER JOIN
           structools.st_bay_equipment             b
      ON   A.pick_id = b.pick_id
      WHERE A.program_id = i_program_id
      AND   A.base_run_id = i_base_run_id
      AND   A.equip_typ = 'BAY'
      )
      ,
      min_strat_del_yr AS
      (
      SELECT MIN(del_year) AS del_year
            ,pick_id
            ,part_cde
      FROM pickids
      WHERE prog_seq = '1'
      GROUP BY pick_id, part_cde
      )
      ,
      min_reac_del_yr AS
      (
      SELECT MIN(del_year) AS del_year
            ,pick_id
            ,part_cde
      FROM pickids
      WHERE prog_seq = '2'
      GROUP BY pick_id, part_cde
      )
      ,
      strat_del_pickid_yr AS          -- select those stratgey repl assets that did not have any reactive replacement prior to strategy replacement
      (
      SELECT aa.strat_pick_id
            ,aa.strat_part_cde
            ,aa.strat_del_year
      FROM            
      (
      SELECT A.pick_id     AS strat_pick_id
            ,A.part_cde    AS strat_part_cde
            ,A.del_year    AS strat_del_year
            ,b.pick_id     AS reac_pick_id
            ,b.del_year    AS reac_del_year
      FROM  min_strat_del_yr  A  LEFT OUTER JOIN   
            min_reac_del_yr   b
      ON A.pick_id = b.pick_id         
      AND A.part_cde = b.part_cde           
      ) aa
      WHERE aa.reac_pick_id IS NULL
      OR aa.reac_del_year > aa.strat_del_year
      )
      ,
      pickids3 AS
      (
      SELECT A.*
      FROM pickids               A        INNER JOIN
           strat_del_pickid_yr   b
      ON A.pick_id = b.strat_pick_id
      AND A.part_cde = b.strat_part_cde
      AND A.del_year= b.strat_del_year
      WHERE prog_seq = '1'
      UNION
      SELECT A.*
      FROM pickids   A
      WHERE prog_seq = '2'
      )
      ,
      risk_comp AS
      ( 
      SELECT  pick_id
            ,CASE model_name
               WHEN 'XARMHVI'        THEN  'HVXM'
               WHEN 'XARMLVI'        THEN  'LVXM'
               WHEN 'STAY'          THEN  'STAY'
               ELSE                       ' '
            END         AS part_cde
             ,fire_frac
             ,eshock_frac
             ,wforce_frac
             ,reliability_frac
             ,physimp_frac
             ,env_frac
             ,fin_frac
      FROM structools.st_risk_fractions 
      WHERE model_name IN ('BAY', 'DOF','DSTR', 'PTSD', 'PWOD',  'RECL', 'RGTR', 'SECT','XARMHVI','XARMLVI','STAY')
      )
      ,
      carrier_model AS         --carrier model field val
      (
      SELECT DISTINCT NVL(A.pick_id, b.conductor_pick_id) AS pick_id
            ,A.length_m
            ,CASE
               WHEN b.conductor_pick_id IS NOT NULL   THEN  TO_CHAR(b.carrier_model_code)
               ELSE                                         TO_CHAR(A.carrier_model_code)
            END                     AS carrier_model_code
      FROM structoolsw.carrier_detail            A     FULL OUTER JOIN
           structools.st_carrier_detail         b
      ON A.pick_id = b.conductor_pick_id               
      )
      SELECT A.del_year
            ,A.mtzn
            ,i_program_id
            ,A.pick_id
            ,A.equip_grp_id
            ,A.event
            ,A.capex   
            ,A.prog_type
            ,NVL(b.feeder_nam, c.feeder_nam) AS feeder_nam
            ,NVL(b.zone_subst_nam, c.zone_subst_nam) AS zone_subst_nam
            ,A.risk_reduction
            ,A.risk_reduction_npr
            ,A.risk_reduction_npr_sif
            ,A.event_reason
            ,i_base_run_id
            ,c.bay_equip_cde
            ,c.stc1
            ,c.stc2
            ,CASE SUBSTR(A.pick_id,1,1)
               WHEN 'B'             THEN  ROUND(c.bay_len_m,2) 
               ELSE                       NULL
            END                        AS bay_len_m
            ,CASE
               WHEN c.conductor_pick_id IS NOT NULL THEN 'C'||c.conductor_pick_id
               ELSE                                      NULL
            END                        AS conductor_pick_id
           ,CASE
               WHEN c.conductor_pick_id IS NOT NULL THEN ROUND(E.length_m,2) 
               ELSE                                      NULL
            END                        AS conductor_len_m
            ,A.failure_prob
           ,CASE
               WHEN c.conductor_pick_id IS NOT NULL THEN E.carrier_model_code 
               ELSE                                      NULL
            END                        AS carr_mdl_cde
           ,CASE
               WHEN c.conductor_pick_id  IS NOT NULL THEN f.material_cde_common 
               ELSE                                      NULL
            END                        AS carr_mdl_mat_cde
           ,A.age 
           ,A.cons_segment_id
           ,A.p_mtzn
           ,A.s_mtzn
           ,NVL(A.risk_reduction_npr * G.fire_frac, 0)            AS npr_fire
           ,NVL(A.risk_reduction_npr * G.eshock_frac, 0)          AS npr_eshock
           ,NVL(A.risk_reduction_npr * G.wforce_frac, 0)          AS npr_wforce
           ,NVL(A.risk_reduction_npr * G.reliability_frac, 0)     AS npr_reliability
           ,NVL(A.risk_reduction_npr * G.physimp_frac, 0)         AS npr_physimp
           ,NVL(A.risk_reduction_npr * G.env_frac, 0)             AS npr_env
           ,A.span_id
           ,A.wood_type
           ,A.part_cde
           ,NVL(A.risk_reduction_npr * G.fin_frac, 0)             AS npr_fin
           ,A.labour_hrs
      FROM pickids3           A  LEFT OUTER JOIN
           structools.st_equipment  b
      ON A.pick_id = b.pick_id     
      AND A.part_cde = b.part_cde
                                 LEFT OUTER JOIN
           structools.st_bay_equipment  c
      ON A.pick_id = c.pick_id           
                                 LEFT OUTER JOIN
           carrier_model     E
      ON 'C'||c.conductor_pick_id = E.pick_id           
                                 LEFT OUTER JOIN
           structools.st_carr_mdl_material     f
      ON E.carrier_model_code = TO_CHAR(f.carrier_mdl_cde)           
                                 LEFT OUTER JOIN
           risk_comp                        G
      ON A.pick_id = G.pick_id          
      AND A.part_cde = G.part_cde 
      ;      
      
      COMMIT;

   dbms_output.put_line('generate_opt_eqp_list End '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));
   
   EXCEPTION
      WHEN v_prog_excp THEN
         RAISE;
      WHEN v_request_not_valid THEN
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('Exception '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END generate_opt_eqp_list;        


   /*---------------------------------------------------------------------------------------------------------------*/
   /* Generates data on prob of failure (incident).                                                                 */
   /*---------------------------------------------------------------------------------------------------------------*/    
   PROCEDURE generate_incd_forecast(i_base_run_id  IN PLS_INTEGER
                                   ,i_program_id   IN PLS_INTEGER     DEFAULT NULL
                                   ,i_years        IN PLS_INTEGER     DEFAULT NULL
                                   ,i_delete_flg   IN VARCHAR2        DEFAULT 'N') IS

      v_dummy              CHAR(1);
      v_base_run_id        PLS_INTEGER;
      v_years              PLS_INTEGER;
      v_first_year         PLS_INTEGER;
      v_pick_id_part          VARCHAR2(20);
      v_part_cde              VARCHAR2(10);
      v_part_cde_orig         VARCHAR2(10);
      v_suppress_pickids_ver	VARCHAR2(10);

      /*Private procedures for generate_incd_forecast */
      
      /* Generate incd forecast data following a delivery program */
      PROCEDURE incd_forecast_for_run_program IS

         CURSOR actioned_pwod_eqp_csr IS

            WITH mtzn_program AS
            (
            SELECT A.del_year
            				,b.mtzn
                        ,A.program_id
                        ,A.base_run_id
            FROM structools.st_del_program_mtzn_optimised		A	INNER JOIN
            			structools.st_mtzn														b
				ON A.mtzn = b.mtzn_opt                            
               WHERE base_run_id = i_base_run_id
               AND   program_id = i_program_id
               AND del_year > 0
            )
            ,    
            mtzn_reinf_program AS
            (
            SELECT A.del_year
            				,b.mtzn
                        ,A.program_id
                        ,A.base_run_id
            FROM structools.st_del_program_reinf_opt   A	INNER JOIN
            			structools.st_mtzn													b
				ON A.mtzn = b.mtzn_opt                         
               WHERE base_run_id = i_base_run_id
               AND   program_id = i_program_id
               AND del_year > 0
            )    
            ,
            seg_program AS
            (
            SELECT del_year
                  ,cons_segment_id
                  ,program_id
                  ,base_run_id
            FROM structools.st_del_program_seg_optimised       
               --WHERE (base_run_id BETWEEN v_parent_run_id_first AND v_parent_run_id_last OR base_run_id = i_base_run_id)
               WHERE base_run_id = i_base_run_id
               AND   program_id = i_program_id
               AND del_year > 0
            )    
            ,
            zonal_and_seg_pickids AS
            (
            SELECT
                   b.pick_id
                  ,b.equip_grp_id
                  ,b.event
                  ,A.del_year    AS mtzn_del_year
                  ,b.part_cde
                  ,c.del_year    AS seg_del_year
                  ,d.del_year    AS mtzn_reinf_del_year
            FROM  structools.st_temp_strategy_run        b  LEFT OUTER JOIN
                  mtzn_program                         A
            ON   A.MTZN = b.mtzn_grp
            AND A.del_year = B.CALENDAR_YEAR
                                                         LEFT OUTER JOIN
                  seg_program                         c                                                            
            ON   c.cons_segment_id = b.cons_segment_id
            AND  c.del_year = B.CALENDAR_YEAR
                                                         LEFT OUTER JOIN
                  mtzn_reinf_program                         d                                                            
            ON   d.mtzn = b.mtzn_grp
            AND d.del_year = b.CALENDAR_YEAR
            WHERE B.EVENT > ' '
            AND b.equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
            AND (b.pickid_suppressed = 'N' OR b.pickid_suppressed = 'P' AND event = 'REINFORCE')
            )
            SELECT    pick_id
                     ,equip_grp_id
                     ,CASE
                        WHEN mtzn_del_year IS NOT NULL   THEN  'MTZN'
                        WHEN seg_del_year IS NOT NULL   THEN  'SEGMENT'
                        WHEN mtzn_reinf_del_year IS NOT NULL   THEN  'MTZN_REINF'
                        ELSE                                  NULL
                     END                           AS event_type
                     ,event
                     ,CASE
                        WHEN mtzn_del_year IS NOT NULL   THEN  mtzn_del_year
                        WHEN seg_del_year IS NOT NULL   THEN  seg_del_year
                        WHEN mtzn_reinf_del_year IS NOT NULL   THEN  mtzn_reinf_del_year
                        ELSE                                  NULL
                     END                           AS del_year
                     ,part_cde
            FROM zonal_and_seg_pickids
            WHERE mtzn_del_year IS NOT NULL
            OR seg_del_year IS NOT NULL
            OR (mtzn_reinf_del_year IS NOT NULL AND event = 'REINFORCE')
            UNION
            SELECT
                   A.pick_id
                  ,A.equip_typ     AS equip_grp_id
                  ,'SNIPER'        AS event_type
                  ,A.event
                  ,A.del_year
                  ,A.part_cde
              FROM  structools.st_del_program_sniper    A
            WHERE A.base_run_id = i_base_run_id
            AND   A.program_id = i_program_id
            AND A.equip_typ IN ('PWOD','DSTR','PTSD','SECT')
            UNION
            SELECT A.pick_id
                  ,A.equip_typ  AS equip_grp_id
                  ,'REAC'        AS event_type
                  ,A.event
                  ,A.del_year
                  ,A.part_cde
            FROM structools.st_del_program_reac_maint   A       
            WHERE A.base_run_id = i_base_run_id
            AND   program_id = i_program_id
            AND A.equip_typ IN ('PWOD','DSTR','PTSD','SECT')
            ORDER BY pick_id, part_cde, del_year
         ;
            
         CURSOR   incd_pwod_eqp_csr IS
            WITH fail_noaction_pwod_and_eqp AS
            (
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,' '                  AS part_cde 
                    ,A.* 
            FROM structools.st_risk_pole_NOACTION_fail  A
            UNION ALL
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,'HVXM'               AS part_cde 
                    ,A.* 
            FROM structools.st_risk_xarmhv_noaction_fail  A
            UNION ALL
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,'LVXM'               AS part_cde 
                    ,A.* 
            FROM structools.st_risk_xarmlv_noaction_fail  A
            UNION ALL 
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,'STAY'               AS part_cde 
                    ,A.* 
            FROM structools.st_risk_stay_noaction_fail  A
            UNION ALL
            SELECT   'N'||A.pick_id       AS pick_id_typ
                    ,' '                  AS part_cde 
                    ,A.*
            FROM structools.st_risk_dstr_NOACTION_fail  A
            UNION ALL 
            SELECT 'N'||A.pick_id       AS pick_id_typ
                    ,' '                  AS part_cde 
                   ,A.* 
            FROM structools.st_risk_ptsd_NOACTION_fail  A
            UNION ALL
            SELECT 'N'||A.pick_id       AS pick_id_typ
                    ,' '                  AS part_cde 
                    ,A.* 
            FROM structools.st_risk_sect_NOACTION_fail  A
            )
            ,
            fail_replace_pwod_and_eqp AS
            (
            SELECT 'S'||A.pick_id         AS pick_id_typ
                    ,' '                  AS part_cde 
                  ,A.* 
            FROM structools.st_risk_pole_replace_fail   A
            UNION ALL
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,'HVXM'               AS part_cde 
                    ,A.* 
            FROM structools.st_risk_xarmhv_replace_fail  A
            UNION ALL
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,'LVXM'               AS part_cde 
                    ,A.* 
            FROM structools.st_risk_xarmlv_replace_fail  A
            UNION ALL 
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,'STAY'               AS part_cde 
                    ,A.* 
            FROM structools.st_risk_stay_replace_fail  A
            UNION ALL 
            SELECT 'N'||A.pick_id         AS pick_id_typ
                    ,' '                  AS part_cde 
                  ,A.* 
            FROM structools.st_risk_dstr_REPLACE_fail   A
            UNION ALL 
            SELECT 'N'||A.pick_id         AS pick_id_typ
                    ,' '                  AS part_cde 
                  ,A.* 
            FROM structools.st_risk_ptsd_REPLACE_fail   A
            UNION ALL 
            SELECT 'N'||A.pick_id         AS pick_id_typ
                    ,' '                  AS part_cde 
                  ,A.* 
            FROM structools.st_risk_sect_REPLACE_fail   A
            )
            ,
            fail_reinforce_pwod AS
            (
            SELECT 'S'||A.pick_id         AS pick_id_typ
                  ,' '                  AS part_cde
                  ,A.*
            FROM structools.st_risk_pole_reinf_fail    A
            )
            ,
            suppressed_eqp AS
            (
            SELECT pick_id
                        ,'Y'					AS suppressed
            FROM structools.st_pickid_suppressed_init
            WHERE equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
            UNION
            SELECT pick_id
                     ,'P'						AS suppressed				
            FROM structools.st_pickid_suppressed
            WHERE equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
            AND scenario_id = v_suppress_pickids_ver
            )
--            ,
--            vic AS
--            (
--            SELECT DISTINCT prnt_plnt_no
--            (
--            SELECT A.prnt_plnt_no 
--                       ,b.suppression_reason
--            FROM   structools.st_EQUIPMENT            A               LEFT OUTER JOIN
--                   suppressed_eqp    b
--            ON A.pick_id = b.pick_id   
----            AND A.part_cde = b.part_cde                
--            WHERE  A.equip_grp_id = 'PWOD'
--            AND A.part_cde = ' '
--            ) aa
--            WHERE aa.suppression_reason IS NULL OR 
--            )
            ,
            equipment_tmp AS
            (
            SELECT A.pick_id
                  ,A.part_cde                     AS part_cde_orig
                  ,CASE A.part_cde
                     WHEN 'ASTAY'            THEN  'STAY'
                     WHEN 'GSTAY'            THEN  'STAY'
                     WHEN 'OSTAY'            THEN  'STAY'
                     WHEN 'AGOST'            THEN  'STAY'
                     ELSE                          part_cde
                  END               AS part_cde
                  ,A.equip_grp_id
--                  ,a.prnt_plnt_no
						,nvl(b.suppressed, 'N')			AS suppressed
            FROM structools.st_equipment			A				LEFT OUTER JOIN
            			suppressed_eqp						B
				ON A.pick_id = b.pick_id                     
            )
            ,
            failures AS
            (
            SELECT A.pick_id
                  ,A.part_cde_orig
                  ,A.part_cde
                  ,A.equip_grp_id
                  ,A.suppressed
                  ,c.fail_prob_y0         AS fail_na_y0
                  ,c.fail_prob_y1         AS fail_na_y1
                  ,c.fail_prob_y2         AS fail_na_y2
                  ,c.fail_prob_y3         AS fail_na_y3
                  ,c.fail_prob_y4         AS fail_na_y4
                  ,c.fail_prob_y5         AS fail_na_y5
                  ,c.fail_prob_y6         AS fail_na_y6
                  ,c.fail_prob_y7         AS fail_na_y7
                  ,c.fail_prob_y8         AS fail_na_y8
                  ,c.fail_prob_y9         AS fail_na_y9
                  ,c.fail_prob_y10        AS fail_na_y10      /* 10 years + 0-year plus 2 years to allow to past replaces (via pickid_committed list) */
                  ,c.fail_prob_y11        AS fail_na_y11      /* If the run is for more than 10 years, this needs to be changed to extract more fail-years */
                  ,c.fail_prob_y12        AS fail_na_y12
                  ,d.fail_prob_y0         AS fail_repl_y0
                  ,d.fail_prob_y1         AS fail_repl_y1
                  ,d.fail_prob_y2         AS fail_repl_y2
                  ,d.fail_prob_y3         AS fail_repl_y3
                  ,d.fail_prob_y4         AS fail_repl_y4
                  ,d.fail_prob_y5         AS fail_repl_y5
                  ,d.fail_prob_y6         AS fail_repl_y6
                  ,d.fail_prob_y7         AS fail_repl_y7
                  ,d.fail_prob_y8         AS fail_repl_y8
                  ,d.fail_prob_y9         AS fail_repl_y9
                  ,d.fail_prob_y10        AS fail_repl_y10
                  ,d.fail_prob_y11        AS fail_repl_y11
                  ,d.fail_prob_y12        AS fail_repl_y12
                  ,E.fail_prob_y0         AS fail_reinf_y0
                  ,E.fail_prob_y1         AS fail_reinf_y1
                  ,E.fail_prob_y2         AS fail_reinf_y2
                  ,E.fail_prob_y3         AS fail_reinf_y3
                  ,E.fail_prob_y4         AS fail_reinf_y4
                  ,E.fail_prob_y5         AS fail_reinf_y5
                  ,E.fail_prob_y6         AS fail_reinf_y6
                  ,E.fail_prob_y7         AS fail_reinf_y7
                  ,E.fail_prob_y8         AS fail_reinf_y8
                  ,E.fail_prob_y9         AS fail_reinf_y9
                  ,E.fail_prob_y10        AS fail_reinf_y10
                  ,E.fail_prob_y11        AS fail_reinf_y11
                  ,E.fail_prob_y12        AS fail_reinf_y12
--            FROM  vic                      v     INNER JOIN
--                  equipment_tmp             A
--            ON v.prnt_plnt_no = A.prnt_plnt_no      
            FROM equipment_tmp			A
                                                 INNER JOIN
                 fail_NOACTION_pwod_and_eqp c
            ON A.pick_id = c.pick_id_typ
            AND A.part_cde = c.part_cde
                                                 INNER JOIN
                 fail_replace_pwod_and_eqp d
            ON A.pick_id = d.pick_id_typ
            AND A.part_cde = d.part_cde
                                                 LEFT OUTER JOIN
                 fail_reinforce_pwod       E
            ON A.pick_id = E.pick_id_typ
            AND A.part_cde = E.part_cde
            )
            SELECT pick_id
                  ,part_cde_orig
                  ,part_cde
                  ,equip_grp_id
--                  ,suppressed
                  ,v_this_year + yr AS calendar_year
                  ,fail_na
                  ,fail_repl
                  ,fail_reinf       AS fail_reinf_repair             /* renamed so that resultant columns are identical between the cursor for polles+eqp */
                                                                     /* and bays, and have a single table based on the cursor definition. */
            FROM  failures
            UNPIVOT ((fail_na, fail_repl, fail_reinf) 
                                          FOR yr IN 
                                          ((fail_na_y0,fail_repl_y0,fail_reinf_y0) AS '00'
                                          ,(fail_na_y1,fail_repl_y1,fail_reinf_y1) AS '01'
                                          ,(fail_na_y2,fail_repl_y2,fail_reinf_y2) AS '02'
                                          ,(fail_na_y3,fail_repl_y3,fail_reinf_y3) AS '03'
                                          ,(fail_na_y4,fail_repl_y4,fail_reinf_y4) AS '04'
                                          ,(fail_na_y5,fail_repl_y5,fail_reinf_y5) AS '05'
                                          ,(fail_na_y6,fail_repl_y6,fail_reinf_y6) AS '06'
                                          ,(fail_na_y7,fail_repl_y7,fail_reinf_y7) AS '07'
                                          ,(fail_na_y8,fail_repl_y8,fail_reinf_y8) AS '08'
                                          ,(fail_na_y9,fail_repl_y9,fail_reinf_y9) AS '09'
                                          ,(fail_na_y10,fail_repl_y10,fail_reinf_y10) AS '10'
                                          ,(fail_na_y11,fail_repl_y11,fail_reinf_y11) AS '11'
                                          ,(fail_na_y12,fail_repl_y12,fail_reinf_y12) AS '12'
                                          ))
            WHERE yr < i_years + 3
            AND suppressed = 'N'
            ORDER BY pick_id, part_cde_orig, calendar_year
         ;
                  
                  
         CURSOR   incd_fractions_csr IS

            SELECT pick_id 
                  ,CASE model_name
                     WHEN 'XARMHV'           THEN 'HVXM'
                     WHEN 'XARMLV'           THEN 'LVXM'
                     WHEN 'STAY'             THEN 'STAY'
                     ELSE                          ' '
                  END                  AS part_cde
                  ,fire_incd
                  ,elec_shock_incd
                  ,lrge_rlblty_incd
                  ,elec_shock_fatality_incd
                  ,fire_fatality_incd
            FROM  structools.st_risk_fractions
            WHERE MODEL_NAME IN ('COND',  'PTSD', 'POLE', 'DSTR', 'SECT','XARMHV','XARMLV','STAY')
         ;

         CURSOR actioned_bay_csr IS

            WITH mtzn_program AS
            (
            SELECT A.del_year
                  ,b.mtzn
                  ,A.program_id
                  ,A.base_run_id
            FROM structools.st_del_program_mtzn_optimised		A	INNER JOIN
            			structools.st_mtzn														b
				ON A.mtzn = b.mtzn_opt                            
            WHERE base_run_id = i_base_run_id   
            AND program_id = i_program_id
            AND del_year > 0
            )
            ,
            seg_program AS
            (
            SELECT del_year
                  ,cons_segment_id
                  ,program_id
                  ,base_run_id
            FROM structools.st_del_program_seg_optimised       
               WHERE base_run_id = i_base_run_id
               AND   program_id = i_program_id
               AND del_year > 0
            )
            ,
            zonal_and_seg_pickids AS
            (
            SELECT
                   b.pick_id
                  ,b.equip_grp_id
                  ,b.event
                  ,A.del_year    AS mtzn_del_year
                  ,c.del_year    AS seg_del_year
            FROM  structools.st_temp_bay_strategy_run        b  LEFT OUTER JOIN
                  mtzn_program                         A
            ON   A.MTZN = b.mtzn_grp
            AND A.del_year = B.CALENDAR_YEAR
                                                         LEFT OUTER JOIN
                  seg_program                         c                                                            
            ON   c.cons_segment_id = b.cons_segment_id
            AND  c.del_year = B.CALENDAR_YEAR
            WHERE B.EVENT > ' '
            AND   b.segment_rebuild_yr IS NOT NULL
            AND	b.pickid_suppressed = 'N'
            )
            SELECT    pick_id
                     ,equip_grp_id
                     ,CASE
                        WHEN mtzn_del_year IS NOT NULL   THEN  'MTZN'
                        ELSE                                   'SEGMENT'
                     END                           AS event_type
                     ,event
                     ,NVL(mtzn_del_year,seg_del_year) AS del_year
                     ,' '                          AS part_cde
            FROM zonal_and_seg_pickids
            WHERE mtzn_del_year IS NOT NULL
            OR seg_del_year IS NOT NULL
            UNION
            SELECT A.pick_id
                  ,A.equip_typ  AS equip_grp_id
                  ,'REAC'        AS event_type
                  ,A.event
                  ,A.del_year
                  ,' '           AS part_cde
            FROM structools.st_del_program_reac_maint   A       
            WHERE A.base_run_id = i_base_run_id
            AND   A.program_id = i_program_id
            AND   A.equip_typ = 'BAY'
            ORDER BY pick_id, del_year
            ;

         CURSOR   incd_bay_csr IS

            WITH fail_noaction_bay AS
            (
            SELECT  'B'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_cond_NOACTION_fail  A
            )
            ,
            fail_replace_bay AS
            (
            SELECT  'B'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_cond_replace_fail  A
            )
            ,
            fail_repair_bay AS
            (
            SELECT  'B'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_cond_repair_fail  A
            )
            ,
            suppressed_bays AS
            (
            SELECT pick_id
                        ,'Y'					AS suppressed
            FROM structools.st_pickid_suppressed_init
            WHERE equip_grp_id = 'BAY'
            UNION
            SELECT pick_id
                     ,'P'						AS suppressed				
            FROM structools.st_pickid_suppressed
            WHERE equip_grp_id = 'BAY'
            AND scenario_id = v_suppress_pickids_ver
            )
            ,
            failures AS
            (
            SELECT A.pick_id
                  ,'BAY'                  AS equip_grp_id
                  ,c.fail_prob_y0         AS fail_na_y0
                  ,c.fail_prob_y1         AS fail_na_y1
                  ,c.fail_prob_y2         AS fail_na_y2
                  ,c.fail_prob_y3         AS fail_na_y3
                  ,c.fail_prob_y4         AS fail_na_y4
                  ,c.fail_prob_y5         AS fail_na_y5
                  ,c.fail_prob_y6         AS fail_na_y6
                  ,c.fail_prob_y7         AS fail_na_y7
                  ,c.fail_prob_y8         AS fail_na_y8
                  ,c.fail_prob_y9         AS fail_na_y9
                  ,c.fail_prob_y10        AS fail_na_y10      /* 10 years + 0-year plus 2 years to allow to past replaces (via pickid_committed list) */
                  ,c.fail_prob_y11        AS fail_na_y11      /* If the run is for more than 10 years, this needs to be changed to extract more fail-years */
                  ,c.fail_prob_y12        AS fail_na_y12
                  ,d.fail_prob_y0         AS fail_repl_y0
                  ,d.fail_prob_y1         AS fail_repl_y1
                  ,d.fail_prob_y2         AS fail_repl_y2
                  ,d.fail_prob_y3         AS fail_repl_y3
                  ,d.fail_prob_y4         AS fail_repl_y4
                  ,d.fail_prob_y5         AS fail_repl_y5
                  ,d.fail_prob_y6         AS fail_repl_y6
                  ,d.fail_prob_y7         AS fail_repl_y7
                  ,d.fail_prob_y8         AS fail_repl_y8
                  ,d.fail_prob_y9         AS fail_repl_y9
                  ,d.fail_prob_y10        AS fail_repl_y10
                  ,d.fail_prob_y11        AS fail_repl_y11
                  ,d.fail_prob_y12        AS fail_repl_y12
                  ,E.fail_prob_y0         AS fail_reinf_y0
                  ,E.fail_prob_y1         AS fail_reinf_y1
                  ,E.fail_prob_y2         AS fail_reinf_y2
                  ,E.fail_prob_y3         AS fail_reinf_y3
                  ,E.fail_prob_y4         AS fail_reinf_y4
                  ,E.fail_prob_y5         AS fail_reinf_y5
                  ,E.fail_prob_y6         AS fail_reinf_y6
                  ,E.fail_prob_y7         AS fail_reinf_y7
                  ,E.fail_prob_y8         AS fail_reinf_y8
                  ,E.fail_prob_y9         AS fail_reinf_y9
                  ,E.fail_prob_y10        AS fail_reinf_y10
                  ,E.fail_prob_y11        AS fail_reinf_y11
                  ,E.fail_prob_y12        AS fail_reinf_y12
                  ,f.event
                  ,G.suppressed
            FROM  structools.st_bay_equipment             A
                                                 INNER JOIN
                 fail_noaction_bay c
            ON A.pick_id = c.pick_id_typ
                                                 INNER JOIN
                 fail_replace_bay d
            ON A.pick_id = d.pick_id_typ
                                                 LEFT OUTER JOIN
                 fail_repair_bay       E
            ON A.pick_id = E.pick_id_typ
                                                 LEFT OUTER JOIN
                 structools.st_pickid_committed f
            ON A.pick_id = f.pick_id
                                                 LEFT OUTER JOIN
                 suppressed_bays						G
            ON A.pick_id = G.pick_id
            )
            SELECT pick_id
                  ,' '              AS part_cde_orig
                  ,' '              AS part_cde
                  ,equip_grp_id
--                  ,suppressed
                  ,v_this_year + yr AS calendar_year
                  ,fail_na
                  ,fail_repl
                  ,fail_reinf       AS fail_reinf_repair             /* renamed so that resultant columns are identical between the cursor for polles+eqp */
                                                                     /* and bays, and have a single table based on the cursor definition. */
            FROM  failures
            UNPIVOT ((fail_na, fail_repl, fail_reinf) 
                                          FOR yr IN 
                                          ((fail_na_y0,fail_repl_y0,fail_reinf_y0) AS '00'
                                          ,(fail_na_y1,fail_repl_y1,fail_reinf_y1) AS '01'
                                          ,(fail_na_y2,fail_repl_y2,fail_reinf_y2) AS '02'
                                          ,(fail_na_y3,fail_repl_y3,fail_reinf_y3) AS '03'
                                          ,(fail_na_y4,fail_repl_y4,fail_reinf_y4) AS '04'
                                          ,(fail_na_y5,fail_repl_y5,fail_reinf_y5) AS '05'
                                          ,(fail_na_y6,fail_repl_y6,fail_reinf_y6) AS '06'
                                          ,(fail_na_y7,fail_repl_y7,fail_reinf_y7) AS '07'
                                          ,(fail_na_y8,fail_repl_y8,fail_reinf_y8) AS '08'
                                          ,(fail_na_y9,fail_repl_y9,fail_reinf_y9) AS '09'
                                          ,(fail_na_y10,fail_repl_y10,fail_reinf_y10) AS '10'
                                          ,(fail_na_y11,fail_repl_y11,fail_reinf_y11) AS '11'
                                          ,(fail_na_y12,fail_repl_y12,fail_reinf_y12) AS '12'
                                          ))
            WHERE yr < i_years + 3
            AND suppressed = 'N'
            ORDER BY pick_id, calendar_year
            ;

         TYPE actioned_eqp_tab_typ    IS TABLE OF actioned_pwod_eqp_csr%ROWTYPE;
         v_actioned_eqp_tab           actioned_eqp_tab_typ := actioned_eqp_tab_typ();

         TYPE actioned_rec_typ IS RECORD
         (event                        VARCHAR2(15)
         ,del_year                     PLS_INTEGER
         );
         TYPE action_tab_typ           IS TABLE OF actioned_rec_typ;
         v_action_tab                  action_tab_typ := action_tab_typ();
         
         TYPE actioned_arr_typ IS TABLE OF action_tab_typ INDEX BY VARCHAR2(20);
         v_actioned_arr        actioned_arr_typ;
            
         TYPE incd_tab_typ    IS TABLE OF incd_pwod_eqp_csr%ROWTYPE;
         v_incd_tab           incd_tab_typ := incd_tab_typ();

         TYPE incd_fractions_tab_typ   IS TABLE OF incd_fractions_csr%ROWTYPE;
         v_incd_fractions_tab          incd_fractions_tab_typ := incd_fractions_tab_typ();

         TYPE incd_fractions_rec_typ IS RECORD
         (fire_frac                    float(126)
         ,eshock_frac                  float(126)
         ,reliability_frac             float(126)
         ,env_frac                     float(126)
         ,fin_frac                     float(126)
         ,eshock_fatality_frac         float(126)
         ,fire_fatality_frac           float(126)
         );
         
         TYPE incd_fractions_arr_typ IS TABLE OF incd_fractions_rec_typ INDEX BY VARCHAR2(20);
         v_incd_fractions_arr          incd_fractions_arr_typ;

         v_incd_fire                   float(126);
         v_incd_eshock                 float(126);
         v_incd_reliability            float(126);
         v_incd_eshock_fatality        float(126);
         v_incd_fire_fatality          float(126);
         v_pick_id                     varchar2(10);
         j                             PLS_INTEGER;
         v_year                        PLS_INTEGER;
         v_action_cnt                  PLS_INTEGER;
         v_max_event                   varchar2(15);
         v_max_event_type              char(10);
         v_max_year                    PLS_INTEGER;
         v_del_year                    PLS_INTEGER;
         v_egi                         varchar2(4);
         v_event                       varCHAR2(15);
         v_event_type                  CHAR(10);
         v_calendar_year               PLS_INTEGER;
         v_incd                        FLOAT(126);
         
         

      BEGIN
         
         /************************************************/
         /**** Process poles and equipment on poles *****/
         /************************************************/
         
         /* dump all actioned eqp into a nested table */
         OPEN actioned_pwod_eqp_csr;
         FETCH actioned_pwod_eqp_csr BULK COLLECT INTO v_actioned_eqp_tab;
         CLOSE actioned_pwod_eqp_csr;
         
         /*reload them into an associative array, indexed by pick_id||part_cde. Fill in missing years. */
         v_pick_id_part := v_actioned_eqp_tab(1).pick_id||v_actioned_eqp_tab(1).part_cde;
         j := 1;
         WHILE j <= v_actioned_eqp_tab.COUNT LOOP
            v_year := v_this_year + 1;
            v_action_cnt := 0;
            v_action_tab.DELETE;
            v_max_event := ' ';
            v_max_year  := 0;
      
            WHILE v_year <= v_this_year + i_years LOOP
      
               v_del_year := v_actioned_eqp_tab(j).del_year;
               v_event := v_actioned_eqp_tab(j).event;
               v_event_type := v_actioned_eqp_tab(j).event_type;
            
               IF v_year < v_del_year THEN
                  v_action_tab.EXTEND;
                  v_action_cnt := v_action_cnt + 1;
                  IF v_max_event = ' ' THEN
                     v_action_tab(v_action_cnt).event    := NULL;
                     v_action_tab(v_action_cnt).del_year := NULL;
                  ELSE
                     v_action_tab(v_action_cnt).event    := v_max_event;
                     v_action_tab(v_action_cnt).del_year := v_max_year;
                  END IF;
               ELSIF v_year = v_del_year THEN
                  v_action_tab.EXTEND;
                  v_action_cnt := v_action_cnt + 1;
                  IF v_max_event = ' ' THEN
                     v_max_event      := v_event;
                     v_max_event_type := v_event_type;
                     v_max_year       := v_year;
                  ELSIF v_max_event = 'REINFORCE' THEN
                     IF v_event = 'REINFORCE' THEN
                        v_max_year := v_del_year;
                     ELSE  /* event = REPLACE, either zonal, sniper or reac_maint */
                        v_max_event      := v_event;
                        v_max_event_type := v_event_type;
                        v_max_year       := v_year;
                     END IF;
                  ELSE  /* ie the latest event was REPLACE */
                     IF v_event = 'REINFORCE' THEN
                        NULL;
                     ELSE  /* was replace and we have another replace, either zonal, sniper or reac_maint */
                        IF v_event_type = 'REAC' THEN
                           v_max_event       := v_event;
                           v_max_event_type  := v_event_type;
                           v_max_year        := v_year;
                        ELSIF v_event_type = 'MTZN' OR v_event_type = 'MTZN_REINF' OR v_event_type = 'SNIPER' OR v_event_type = 'SEGMENT' THEN
                           IF v_max_event_type = 'MTZN' OR v_max_event_type = 'MTZN_REINF' OR v_max_event_type = 'SNIPER' OR v_max_event_type = 'SEGMENT'  THEN
                              v_max_event       := v_event;
                              v_max_event_type  := v_event_type;
                              v_max_year        := v_year;
                           ELSE
                              NULL;
                           END IF;
                        END IF;
                     END IF;
                  END IF;
                     
                  v_action_tab(v_action_cnt).event    := v_max_event;
                  v_action_tab(v_action_cnt).del_year := v_max_year;
      
                  j := j + 1;
                  IF j <= v_actioned_eqp_tab.COUNT AND v_actioned_eqp_tab(j).pick_id||v_actioned_eqp_tab(j).part_cde = v_pick_id_part THEN
                     NULL;
                  ELSE
                     IF v_year < v_this_year + i_years THEN
                        WHILE v_year < v_this_year + i_years LOOP
                           v_year := v_year + 1;
                           v_action_tab.EXTEND;
                           v_action_cnt := v_action_cnt + 1;
                           v_action_tab(v_action_cnt).event    := v_max_event;
                           v_action_tab(v_action_cnt).del_year := v_max_year;
                        END LOOP;
                     END IF;
                  END IF;
               END IF;
               v_year := v_year + 1;
            END LOOP;        
            v_actioned_arr(v_pick_id_part) := v_action_tab;
      
            IF j <= v_actioned_eqp_tab.COUNT THEN
               v_pick_id_part := v_actioned_eqp_tab(j).pick_id||v_actioned_eqp_tab(j).part_cde;
            END IF;
            
         END LOOP;
      
         v_actioned_eqp_tab.DELETE;
         v_action_tab.DELETE;


         /* Fetch incd fractions for all pickids, all years, and store in a nested table */
         OPEN incd_fractions_csr;
         FETCH incd_fractions_csr BULK COLLECT INTO v_incd_fractions_tab;
         CLOSE incd_fractions_csr;

         /* Reload into an associative array, indexed by pick_id||part_cde */
         FOR i IN 1 .. v_incd_fractions_tab.COUNT LOOP
            v_incd_fractions_arr(v_incd_fractions_tab(i).pick_id||v_incd_fractions_tab(i).part_cde).fire_frac               := v_incd_fractions_tab(i).fire_incd;
            v_incd_fractions_arr(v_incd_fractions_tab(i).pick_id||v_incd_fractions_tab(i).part_cde).eshock_frac             := v_incd_fractions_tab(i).elec_shock_incd;
            v_incd_fractions_arr(v_incd_fractions_tab(i).pick_id||v_incd_fractions_tab(i).part_cde).reliability_frac        := v_incd_fractions_tab(i).lrge_rlblty_incd;
            v_incd_fractions_arr(v_incd_fractions_tab(i).pick_id||v_incd_fractions_tab(i).part_cde).eshock_fatality_frac    := v_incd_fractions_tab(i).elec_shock_fatality_incd;
            v_incd_fractions_arr(v_incd_fractions_tab(i).pick_id||v_incd_fractions_tab(i).part_cde).fire_fatality_frac      := v_incd_fractions_tab(i).fire_fatality_incd;
         END LOOP;
            
         v_incd_fractions_tab.DELETE;
      
         /* Process risk data, matching with the actioned table where exists. Insert into st_del_program_incd_forecast table */
         OPEN incd_pwod_eqp_csr;
      
         LOOP 
      
            /* Fetch risk data for a single pickid, all years, and store in a nested table */
            FETCH incd_pwod_eqp_csr BULK COLLECT INTO v_incd_tab LIMIT i_years + 3;
            EXIT WHEN incd_pwod_eqp_csr%NOTFOUND;
               
            v_pick_id                  := v_incd_tab(1).pick_id;
            v_part_cde                 := v_incd_tab(1).part_cde;
            v_part_cde_orig            := v_incd_tab(1).part_cde_orig;
            v_pick_id_part             := v_pick_id||v_part_cde;
            v_action_tab.DELETE;
            IF v_actioned_arr.EXISTS(v_pick_id_part) THEN
               v_action_tab         := v_actioned_arr(v_pick_id_part);
            END IF;
            v_egi                := v_incd_tab(1).equip_grp_id;
            FOR i IN 2 .. (v_incd_tab.COUNT - 2) LOOP /* only need to process i_years, starting from v_this_year + 1. */
                                                               /* The rest is to access risk for replace/reinforce for committed pickids.*/
               IF v_action_tab.COUNT > 0 THEN
                  v_event     := v_action_tab(i - 1).event;
                  v_del_year  := v_action_tab(i - 1).del_year;
               ELSE
                  v_event     := NULL;
                  v_del_year  := NULL;
               END IF;     
               v_calendar_year := v_incd_tab(i).calendar_year;
               IF v_event IS NULL THEN                         
                  v_incd := v_incd_tab(i).fail_na;
               ELSIF v_event = 'REINFORCE' THEN
                  v_incd := v_incd_tab(i).fail_reinf_repair;
               ELSIF v_event = 'REPLACE' THEN
                  j := v_calendar_year - v_del_year + 1;
                  v_incd := v_incd_tab(j).fail_repl;
               END IF;
               v_incd_fire             := v_incd * v_incd_fractions_arr(v_pick_id_part).fire_frac;
               v_incd_eshock           := v_incd * v_incd_fractions_arr(v_pick_id_part).eshock_frac;
               v_incd_reliability      := v_incd * v_incd_fractions_arr(v_pick_id_part).reliability_frac;
               v_incd_eshock_fatality  := v_incd * v_incd_fractions_arr(v_pick_id_part).eshock_fatality_frac;
               v_incd_fire_fatality    := v_incd * v_incd_fractions_arr(v_pick_id_part).fire_fatality_frac;
      
               INSERT INTO structools.st_del_program_incd_forecast
                  VALUES   (i_base_run_id
                           ,i_program_id
                           ,v_pick_id
                           ,v_egi
                           ,v_calendar_year
                           ,v_event
                           ,v_incd
                           ,v_incd_fire
                           ,v_incd_eshock
                           ,v_incd_reliability
                           ,v_incd_eshock_fatality
                           ,v_incd_fire_fatality
                           ,v_part_cde_orig
                           );
            END LOOP;
         END LOOP;
         CLOSE incd_pwod_eqp_csr;
         
         v_actioned_arr.DELETE;
            
         /***********************/
         /**** Process bays *****/
         /***********************/
            
         /* dump all actioned eqp into a nested table */
         OPEN actioned_bay_csr;
         FETCH actioned_bay_csr BULK COLLECT INTO v_actioned_eqp_tab;
         CLOSE actioned_bay_csr;
         
         /*reload them into an associative array, indexed by pick_id. Fill in missing years. */
         v_pick_id := v_actioned_eqp_tab(1).pick_id;
         j := 1;
         WHILE j <= v_actioned_eqp_tab.COUNT LOOP
            v_year := v_this_year + 1;
            v_action_cnt := 0;
            v_action_tab.DELETE;
            v_max_event := ' ';
            v_max_year := 0;

            WHILE v_year <= v_this_year + i_years LOOP

               v_del_year := v_actioned_eqp_tab(j).del_year;
               v_event := v_actioned_eqp_tab(j).event;
               v_event_type := v_actioned_eqp_tab(j).event_type;
            
               IF v_year < v_del_year THEN
                  v_action_tab.EXTEND;
                  v_action_cnt := v_action_cnt + 1;
                  IF v_max_event = ' ' THEN
                     v_action_tab(v_action_cnt).event    := NULL;
                     v_action_tab(v_action_cnt).del_year := NULL;
                  ELSE
                     v_action_tab(v_action_cnt).event    := v_max_event;
                     v_action_tab(v_action_cnt).del_year := v_max_year;
                  END IF;
               ELSIF v_year = v_del_year THEN
                  v_action_tab.EXTEND;
                  v_action_cnt := v_action_cnt + 1;
                  IF v_max_event = ' ' THEN
                     v_max_event       := v_event;
                     v_max_event_type  := v_event_type;
                     v_max_year        := v_year;
                  ELSIF v_max_event = 'REPAIR' THEN
                     IF v_event = 'REPAIR' THEN
                        v_max_year := v_del_year;
                     ELSE
                        v_max_event       := v_event;
                        v_max_event_type  := v_event_type;
                        v_max_year        := v_year;
                     END IF;
                  ELSE
                     IF v_event = 'REPAIR' THEN
                        NULL;
                     ELSE  /* was replace and we have another replace, either zonal or reac_maint */
                        IF v_event_type = 'REAC' THEN
                           v_max_event       := v_event;
                           v_max_event_type  := v_event_type;
                           v_max_year        := v_year;
                        ELSIF v_event_type = 'MTZN' OR v_event_type = 'SEGMENT' THEN
                           IF v_max_event_type = 'MTZN' OR v_event_type = 'SEGMENT' THEN
                              v_max_event       := v_event;
                              v_max_event_type  := v_event_type;
                              v_max_year        := v_year;
                           ELSE
                              NULL;
                           END IF;
                        END IF;
                     END IF;
                  END IF;
                  v_action_tab(v_action_cnt).event    := v_max_event;
                  v_action_tab(v_action_cnt).del_year := v_max_year;

                  j := j + 1;
                  IF j <= v_actioned_eqp_tab.COUNT AND v_actioned_eqp_tab(j).pick_id = v_pick_id THEN
                     NULL;
                  ELSE
                     IF v_year < v_this_year + i_years THEN
                        WHILE v_year < v_this_year + i_years LOOP
                           v_year := v_year + 1;
                           v_action_tab.EXTEND;
                           v_action_cnt := v_action_cnt + 1;
                           v_action_tab(v_action_cnt).event    := v_max_event;
                           v_action_tab(v_action_cnt).del_year := v_max_year;
                        END LOOP;
                     END IF;
                  END IF;
               END IF;
               v_year := v_year + 1;
            END LOOP;        
            v_actioned_arr(v_pick_id) := v_action_tab;

            IF j <= v_actioned_eqp_tab.COUNT THEN
               v_pick_id := v_actioned_eqp_tab(j).pick_id;
            END IF;
            
         END LOOP;

         v_actioned_eqp_tab.DELETE;
         v_action_tab.DELETE;


         /* Process risk data, matching with the actioned table where exists. Insert into st_del_program_incd_forecast table */
         OPEN incd_bay_csr;

         LOOP 

            /* Fetch risk data for a single pickid, all years, and store in a nested table */
            FETCH incd_bay_csr BULK COLLECT INTO v_incd_tab LIMIT i_years + 3;
            EXIT WHEN incd_bay_csr%NOTFOUND;
            
            v_pick_id            := v_incd_tab(1).pick_id;
            v_action_tab.DELETE;
            IF v_actioned_arr.EXISTS(v_pick_id) THEN
               v_action_tab         := v_actioned_arr(v_pick_id);
            END IF;
            v_egi                := v_incd_tab(1).equip_grp_id;
            
            FOR i IN 2 .. (v_incd_tab.COUNT - 2) LOOP /* only need to process i_years, starting from v_this_year + 1. */
                                                               /* The rest is to access risk for replace/reinforce for committed pickids.*/
               IF v_action_tab.COUNT > 0 THEN
                  v_event     := v_action_tab(i - 1).event;
                  v_del_year  := v_action_tab(i - 1).del_year;
               ELSE
                  v_event     := NULL;
                  v_del_year  := NULL;
               END IF;     
               v_calendar_year := v_incd_tab(i).calendar_year;
               IF v_event IS NULL THEN                         
                  v_incd := v_incd_tab(i).fail_na;
               ELSIF v_event = 'REPAIR' THEN
                  v_incd := v_incd_tab(i).fail_reinf_repair;
               ELSIF v_event = 'REPLACE' THEN
                  j := v_calendar_year - v_del_year + 1;
                  v_incd := v_incd_tab(j).fail_repl;
               END IF;

               v_incd_fire             := v_incd * v_incd_fractions_arr(v_pick_id||' ').fire_frac;
               v_incd_eshock           := v_incd * v_incd_fractions_arr(v_pick_id||' ').eshock_frac;
               v_incd_reliability      := v_incd * v_incd_fractions_arr(v_pick_id||' ').reliability_frac;
               v_incd_eshock_fatality  := v_incd * v_incd_fractions_arr(v_pick_id||' ').eshock_fatality_frac;
               v_incd_fire_fatality    := v_incd * v_incd_fractions_arr(v_pick_id||' ').fire_fatality_frac;

               INSERT INTO structools.st_del_program_incd_forecast
                  VALUES   (i_base_run_id
                           ,i_program_id
                           ,v_pick_id
                           ,v_egi
                           ,v_calendar_year
                           ,v_event
                           ,v_incd
                           ,v_incd_fire
                           ,v_incd_eshock
                           ,v_incd_reliability
                           ,v_incd_eshock_fatality
                           ,v_incd_fire_fatality
                           ,' '           --part_cde
                           );
            END LOOP;
         END LOOP;
         CLOSE incd_bay_csr;

      END incd_forecast_for_run_program;                                   


   /* mainline of generate_incd_forecast */
   BEGIN
      dbms_output.put_line('generate_incd_forecast Start '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));
      
      BEGIN
         SELECT run_years
         			,suppress_pickids_ver
--               ,parent_run_id
         INTO   v_years
         			,v_suppress_pickids_ver
--               ,v_parent_run_id
         FROM structools.st_run_request
         WHERE run_id = i_base_run_id;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            IF i_years IS NULL OR i_years = 0 THEN
               dbms_output.put_line('run ID NOT FOUND AND i_years parameter invalid');
               RAISE v_prog_excp;
            END IF;
      END;

      IF i_years IS NOT NULL THEN
         v_years := i_years;
      END IF;
      
      IF v_years > 50 THEN 
         dbms_output.put_line ('maximum OF 50 years VALUES can be GENERATED'); 
         RAISE v_prog_excp;
      END IF;
      
      IF i_delete_flg = 'Y' THEN
         dbms_output.put_line('Deleting existing results');
         IF i_program_id IS NULL THEN
            DELETE FROM structools.st_del_program_incd_forecast
            WHERE program_id IS NULL
            AND   base_run_id = i_base_run_id;
         ELSE
            DELETE FROM structools.st_del_program_incd_forecast
            WHERE program_id = i_program_id
            AND   base_run_id = i_base_run_id;
         END IF;
      ELSE
         BEGIN
            IF i_program_id IS NULL THEN
               SELECT NULL
               INTO     v_dummy                        
               FROM structools.st_del_program_incd_forecast
               WHERE base_run_id = i_base_run_id
               AND program_id IS NULL
               AND ROWNUM = 1;
            ELSE
               SELECT NULL
               INTO     v_dummy                        
               FROM structools.st_del_program_incd_forecast
               WHERE base_run_id = i_base_run_id
               AND program_id = i_program_id
               AND ROWNUM = 1;
            END IF;
            dbms_output.put_line ('st_del_program_incd_forecast already contains DATA FOR run_id '||i_base_run_id 
                                    ||' AND program_id '||i_program_id);
            RAISE v_prog_excp;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN 
               NULL;
         END;
      END IF;                     


      IF i_program_id IS NULL THEN      /* assuming nothing done */
         INSERT
            INTO structools.st_del_program_incd_forecast
         WITH incd_frac AS
         (
         SELECT pick_id 
               ,CASE model_name
                  WHEN 'XARMHV'           THEN 'HVXM'
                  WHEN 'XARMLV'           THEN 'LVXM'
                  WHEN 'STAY'             THEN 'STAY'
                  ELSE                          ' '
               END                  AS part_cde
               ,fire_incd
               ,elec_shock_incd
               ,lrge_rlblty_incd
               ,elec_shock_fatality_incd
               ,fire_fatality_incd
         FROM  structools.st_risk_fractions
         WHERE MODEL_NAME IN ('COND',  'PTSD', 'POLE', 'DSTR', 'SECT','XARMHV','XARMLV','STAY')
         )
         ,
         fail_prob_pickid AS
         (
         SELECT 'S'||aa.pick_id AS pick_id
               ,' '              AS part_cde
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.fail_prob
         FROM
            (
            SELECT *
            FROM  structools.st_risk_pole_noaction_fail
            UNPIVOT (fail_prob FOR yr_idx IN (fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03'
               ,fail_prob_y4 AS '04', fail_prob_y5 AS '05', fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09'
               ,fail_prob_y10 AS '10',fail_prob_y11 AS '11', fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14'
               ,fail_prob_y15 AS '15', fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19'
               ,fail_prob_y20 AS '20', fail_prob_y21 AS '21', fail_prob_y22 AS '22', fail_prob_y23 AS '23', fail_prob_y24 AS '24'
               ,fail_prob_y25 AS '25', fail_prob_y26 AS '26', fail_prob_y27 AS '27', fail_prob_y28 AS '28', fail_prob_y29 AS '29'
               ,fail_prob_y30 AS '30', fail_prob_y31 AS '31', fail_prob_y32 AS '32', fail_prob_y33 AS '33', fail_prob_y34 AS '34'
               ,fail_prob_y35 AS '35', fail_prob_y36 AS '36', fail_prob_y37 AS '37', fail_prob_y38 AS '38', fail_prob_y39 AS '39'
               ,fail_prob_y40 AS '40', fail_prob_y41 AS '41', fail_prob_y42 AS '42', fail_prob_y43 AS '43', fail_prob_y44 AS '44'
               ,fail_prob_y45 AS '45', fail_prob_y46 AS '46', fail_prob_y47 AS '47', fail_prob_y48 AS '48', fail_prob_y49 AS '49', fail_prob_y50 AS '50'))
            )     aa 
         UNION ALL
         SELECT 'S'||aa.pick_id  AS pick_id
               ,'HVXM'           AS part_cde
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.fail_prob
         FROM
            (
            SELECT *
            FROM  structools.st_risk_xarmhv_noaction_fail
            UNPIVOT (fail_prob FOR yr_idx IN (fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03'
               ,fail_prob_y4 AS '04', fail_prob_y5 AS '05', fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09'
               ,fail_prob_y10 AS '10',fail_prob_y11 AS '11', fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14'
               ,fail_prob_y15 AS '15', fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19'
               ,fail_prob_y20 AS '20', fail_prob_y21 AS '21', fail_prob_y22 AS '22', fail_prob_y23 AS '23', fail_prob_y24 AS '24'
               ,fail_prob_y25 AS '25', fail_prob_y26 AS '26', fail_prob_y27 AS '27', fail_prob_y28 AS '28', fail_prob_y29 AS '29'
               ,fail_prob_y30 AS '30', fail_prob_y31 AS '31', fail_prob_y32 AS '32', fail_prob_y33 AS '33', fail_prob_y34 AS '34'
               ,fail_prob_y35 AS '35', fail_prob_y36 AS '36', fail_prob_y37 AS '37', fail_prob_y38 AS '38', fail_prob_y39 AS '39'
               ,fail_prob_y40 AS '40', fail_prob_y41 AS '41', fail_prob_y42 AS '42', fail_prob_y43 AS '43', fail_prob_y44 AS '44'
               ,fail_prob_y45 AS '45', fail_prob_y46 AS '46', fail_prob_y47 AS '47', fail_prob_y48 AS '48', fail_prob_y49 AS '49', fail_prob_y50 AS '50'))
            )     aa 
         UNION ALL
         SELECT 'S'||aa.pick_id AS pick_id
               ,'LVXM'           AS part_cde
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.fail_prob
         FROM
            (
            SELECT *
            FROM  structools.st_risk_xarmlv_noaction_fail
            UNPIVOT (fail_prob FOR yr_idx IN (fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03'
               ,fail_prob_y4 AS '04', fail_prob_y5 AS '05', fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09'
               ,fail_prob_y10 AS '10',fail_prob_y11 AS '11', fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14'
               ,fail_prob_y15 AS '15', fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19'
               ,fail_prob_y20 AS '20', fail_prob_y21 AS '21', fail_prob_y22 AS '22', fail_prob_y23 AS '23', fail_prob_y24 AS '24'
               ,fail_prob_y25 AS '25', fail_prob_y26 AS '26', fail_prob_y27 AS '27', fail_prob_y28 AS '28', fail_prob_y29 AS '29'
               ,fail_prob_y30 AS '30', fail_prob_y31 AS '31', fail_prob_y32 AS '32', fail_prob_y33 AS '33', fail_prob_y34 AS '34'
               ,fail_prob_y35 AS '35', fail_prob_y36 AS '36', fail_prob_y37 AS '37', fail_prob_y38 AS '38', fail_prob_y39 AS '39'
               ,fail_prob_y40 AS '40', fail_prob_y41 AS '41', fail_prob_y42 AS '42', fail_prob_y43 AS '43', fail_prob_y44 AS '44'
               ,fail_prob_y45 AS '45', fail_prob_y46 AS '46', fail_prob_y47 AS '47', fail_prob_y48 AS '48', fail_prob_y49 AS '49', fail_prob_y50 AS '50'))
            )     aa 
         UNION ALL
         SELECT 'S'||aa.pick_id AS pick_id
               ,'STAY'           AS part_cde
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.fail_prob
         FROM
            (
            SELECT *
            FROM  structools.st_risk_stay_noaction_fail
            UNPIVOT (fail_prob FOR yr_idx IN (fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03'
               ,fail_prob_y4 AS '04', fail_prob_y5 AS '05', fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09'
               ,fail_prob_y10 AS '10',fail_prob_y11 AS '11', fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14'
               ,fail_prob_y15 AS '15', fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19'
               ,fail_prob_y20 AS '20', fail_prob_y21 AS '21', fail_prob_y22 AS '22', fail_prob_y23 AS '23', fail_prob_y24 AS '24'
               ,fail_prob_y25 AS '25', fail_prob_y26 AS '26', fail_prob_y27 AS '27', fail_prob_y28 AS '28', fail_prob_y29 AS '29'
               ,fail_prob_y30 AS '30', fail_prob_y31 AS '31', fail_prob_y32 AS '32', fail_prob_y33 AS '33', fail_prob_y34 AS '34'
               ,fail_prob_y35 AS '35', fail_prob_y36 AS '36', fail_prob_y37 AS '37', fail_prob_y38 AS '38', fail_prob_y39 AS '39'
               ,fail_prob_y40 AS '40', fail_prob_y41 AS '41', fail_prob_y42 AS '42', fail_prob_y43 AS '43', fail_prob_y44 AS '44'
               ,fail_prob_y45 AS '45', fail_prob_y46 AS '46', fail_prob_y47 AS '47', fail_prob_y48 AS '48', fail_prob_y49 AS '49', fail_prob_y50 AS '50'))
            )     aa 
         UNION ALL
         SELECT 'N'||aa.pick_id AS pick_id
               ,' '              AS part_cde
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.fail_prob
         FROM
            (
            SELECT *
            FROM  structools.st_risk_dstr_noaction_fail
            UNPIVOT (fail_prob FOR yr_idx IN (fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03'
               ,fail_prob_y4 AS '04', fail_prob_y5 AS '05', fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09'
               ,fail_prob_y10 AS '10',fail_prob_y11 AS '11', fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14'
               ,fail_prob_y15 AS '15', fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19'
               ,fail_prob_y20 AS '20', fail_prob_y21 AS '21', fail_prob_y22 AS '22', fail_prob_y23 AS '23', fail_prob_y24 AS '24'
               ,fail_prob_y25 AS '25', fail_prob_y26 AS '26', fail_prob_y27 AS '27', fail_prob_y28 AS '28', fail_prob_y29 AS '29'
               ,fail_prob_y30 AS '30', fail_prob_y31 AS '31', fail_prob_y32 AS '32', fail_prob_y33 AS '33', fail_prob_y34 AS '34'
               ,fail_prob_y35 AS '35', fail_prob_y36 AS '36', fail_prob_y37 AS '37', fail_prob_y38 AS '38', fail_prob_y39 AS '39'
               ,fail_prob_y40 AS '40', fail_prob_y41 AS '41', fail_prob_y42 AS '42', fail_prob_y43 AS '43', fail_prob_y44 AS '44'
               ,fail_prob_y45 AS '45', fail_prob_y46 AS '46', fail_prob_y47 AS '47', fail_prob_y48 AS '48', fail_prob_y49 AS '49', fail_prob_y50 AS '50'))
            )     aa 
         UNION ALL
         SELECT 'N'||aa.pick_id AS pick_id
               ,' '              AS part_cde
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.fail_prob
         FROM
            (
            SELECT *
            FROM  structools.st_risk_ptsd_noaction_fail
            UNPIVOT (fail_prob FOR yr_idx IN (fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03'
               ,fail_prob_y4 AS '04', fail_prob_y5 AS '05', fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09'
               ,fail_prob_y10 AS '10',fail_prob_y11 AS '11', fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14'
               ,fail_prob_y15 AS '15', fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19'
               ,fail_prob_y20 AS '20', fail_prob_y21 AS '21', fail_prob_y22 AS '22', fail_prob_y23 AS '23', fail_prob_y24 AS '24'
               ,fail_prob_y25 AS '25', fail_prob_y26 AS '26', fail_prob_y27 AS '27', fail_prob_y28 AS '28', fail_prob_y29 AS '29'
               ,fail_prob_y30 AS '30', fail_prob_y31 AS '31', fail_prob_y32 AS '32', fail_prob_y33 AS '33', fail_prob_y34 AS '34'
               ,fail_prob_y35 AS '35', fail_prob_y36 AS '36', fail_prob_y37 AS '37', fail_prob_y38 AS '38', fail_prob_y39 AS '39'
               ,fail_prob_y40 AS '40', fail_prob_y41 AS '41', fail_prob_y42 AS '42', fail_prob_y43 AS '43', fail_prob_y44 AS '44'
               ,fail_prob_y45 AS '45', fail_prob_y46 AS '46', fail_prob_y47 AS '47', fail_prob_y48 AS '48', fail_prob_y49 AS '49', fail_prob_y50 AS '50'))
            )     aa 
         UNION ALL
         SELECT 'N'||aa.pick_id AS pick_id
               ,' '              AS part_cde
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.fail_prob
         FROM
            (
            SELECT *
            FROM  structools.st_risk_sect_noaction_fail
            UNPIVOT (fail_prob FOR yr_idx IN (fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03'
               ,fail_prob_y4 AS '04', fail_prob_y5 AS '05', fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09'
               ,fail_prob_y10 AS '10',fail_prob_y11 AS '11', fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14'
               ,fail_prob_y15 AS '15', fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19'
               ,fail_prob_y20 AS '20', fail_prob_y21 AS '21', fail_prob_y22 AS '22', fail_prob_y23 AS '23', fail_prob_y24 AS '24'
               ,fail_prob_y25 AS '25', fail_prob_y26 AS '26', fail_prob_y27 AS '27', fail_prob_y28 AS '28', fail_prob_y29 AS '29'
               ,fail_prob_y30 AS '30', fail_prob_y31 AS '31', fail_prob_y32 AS '32', fail_prob_y33 AS '33', fail_prob_y34 AS '34'
               ,fail_prob_y35 AS '35', fail_prob_y36 AS '36', fail_prob_y37 AS '37', fail_prob_y38 AS '38', fail_prob_y39 AS '39'
               ,fail_prob_y40 AS '40', fail_prob_y41 AS '41', fail_prob_y42 AS '42', fail_prob_y43 AS '43', fail_prob_y44 AS '44'
               ,fail_prob_y45 AS '45', fail_prob_y46 AS '46', fail_prob_y47 AS '47', fail_prob_y48 AS '48', fail_prob_y49 AS '49', fail_prob_y50 AS '50'))
            )     aa 
         UNION ALL
         SELECT 'B'||aa.pick_id AS pick_id
               ,' '              AS part_cde
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.fail_prob
         FROM
            (
            SELECT *
            FROM  structools.st_risk_cond_noaction_fail
            UNPIVOT (fail_prob FOR yr_idx IN (fail_prob_y1 AS '01', fail_prob_y2 AS '02', fail_prob_y3 AS '03'
               ,fail_prob_y4 AS '04', fail_prob_y5 AS '05', fail_prob_y6 AS '06', fail_prob_y7 AS '07',fail_prob_y8 AS '08', fail_prob_y9 AS '09'
               ,fail_prob_y10 AS '10',fail_prob_y11 AS '11', fail_prob_y12 AS '12', fail_prob_y13 AS '13', fail_prob_y14 AS '14'
               ,fail_prob_y15 AS '15', fail_prob_y16 AS '16', fail_prob_y17 AS '17', fail_prob_y18 AS '18', fail_prob_y19 AS '19'
               ,fail_prob_y20 AS '20', fail_prob_y21 AS '21', fail_prob_y22 AS '22', fail_prob_y23 AS '23', fail_prob_y24 AS '24'
               ,fail_prob_y25 AS '25', fail_prob_y26 AS '26', fail_prob_y27 AS '27', fail_prob_y28 AS '28', fail_prob_y29 AS '29'
               ,fail_prob_y30 AS '30', fail_prob_y31 AS '31', fail_prob_y32 AS '32', fail_prob_y33 AS '33', fail_prob_y34 AS '34'
               ,fail_prob_y35 AS '35', fail_prob_y36 AS '36', fail_prob_y37 AS '37', fail_prob_y38 AS '38', fail_prob_y39 AS '39'
               ,fail_prob_y40 AS '40', fail_prob_y41 AS '41', fail_prob_y42 AS '42', fail_prob_y43 AS '43', fail_prob_y44 AS '44'
               ,fail_prob_y45 AS '45', fail_prob_y46 AS '46', fail_prob_y47 AS '47', fail_prob_y48 AS '48', fail_prob_y49 AS '49', fail_prob_y50 AS '50'))
            )     aa 
         )
--         ,
--         vic AS
--         (
--         SELECT aa.prnt_plnt_no FROM
--         (
--         SELECT DISTINCT A.prnt_plnt_no 
--         SELECT A.prnt_plnt_no 
--                        ,b.event--
--         FROM   structools.st_EQUIPMENT         A     --LEFT OUTER JOIN
--                structools.st_pickid_committed  b
--         ON     A.pick_id = b.pick_id  
--         AND    A.part_cde = b.part_cde              
--         WHERE  equip_grp_id = 'PWOD'
--         AND A.part_cde = ' '
--         ) aa
--         WHERE aa.event IS NULL OR aa.event NOT IN ('SUPP', 'PUP')
--         )
	 , 
         suppressed_eqp_bays AS
         (
         SELECT pick_id
                     ,'Y'					AS suppressed
         FROM structools.st_pickid_suppressed_init
         WHERE equip_grp_id IN ('PWOD','DSTR','PTSD','SECT','BAY')
--         UNION
--         SELECT pick_id
--                  ,'P'						AS suppressed				
--         FROM structools.st_pickid_suppressed
--         WHERE equip_grp_id IN ('PWOD','DSTR','PTSD','SECT','BAY')
--         AND scenario_id = v_suppress_pickids_ver
         )
         ,
         pickid AS
         (
         SELECT DISTINCT b.pick_id
                        ,b.part_cde             AS part_cde_orig
                        ,CASE b.part_cde
                           WHEN     'ASTAY'           THEN  'STAY'
                           WHEN     'GSTAY'           THEN  'STAY'
                           WHEN     'OSTAY'           THEN  'STAY'
                           WHEN     'AGOST'           THEN  'STAY'
                           ELSE                             b.part_cde
                        END                  AS part_cde
                        ,b.equip_grp_id
			,nvl(c.suppressed,'N')	AS suppressed
         FROM  --vic                     A                    INNER JOIN
               structools.st_equipment     b		
--         ON    A.prnt_plnt_no = b.prnt_plnt_no	
								LEFT OUTER JOIN
		suppressed_eqp_bays		c
	 			ON b.pick_id = c.pick_id
         WHERE b.equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
         UNION ALL
--         SELECT      aa.pick_id
--                    ,aa.part_cde_orig
--                    ,aa.part_cde
--                    ,aa.equip_grp_id
--         FROM
--         (                     
--         SELECT DISTINCT A.pick_id
         SELECT A.pick_id
                        ,' '  AS part_cde_orig
                        ,' '  AS part_cde
                        ,'BAY'         AS equip_grp_id
--                        ,b.event
			,nvl(c.suppressed,'N')	AS suppressed
         FROM structools.st_bay_equipment       A        LEFT OUTER JOIN
	      suppressed_eqp_bays		     c
	 ON A.pick_id = c.pick_id			
--              structools.st_pickid_committed    b
--         ON   A.pick_id = b.pick_id              
--         ) aa
--         WHERE aa.event IS NULL OR aa.event NOT IN ('SUPP', 'PUP')
						
         )
         SELECT i_base_run_id
               ,NULL                AS program_id
               ,A.pick_id
               ,A.equip_grp_id
               ,b.calendar_year
               ,NULL          AS event
               ,b.fail_prob
               ,b.fail_prob  * c.fire_incd         AS fire_incd_prob
               ,b.fail_prob  * c.elec_shock_incd   AS elec_shock_incd_prob
               ,b.fail_prob  * c.lrge_rlblty_incd  AS lrge_rlblty_incd_prob
               ,b.fail_prob  * c.elec_shock_fatality_incd  AS elec_shock_fatality_incd
               ,b.fail_prob  * c.fire_fatality_incd  AS fire_fatality_incd
               ,A.part_cde_orig
         FROM pickid                A     INNER JOIN
              fail_prob_pickid      b
         ON A.pick_id = b.pick_id
         AND A.part_cde = b.part_cde
                                          LEFT OUTER JOIN
            incd_frac               c
         ON A.pick_id = c.pick_id
         AND A.part_cde = c.part_cde
         WHERE b.calendar_year <= v_this_year + i_years
	 AND A.suppressed = 'N'
         ;
         COMMIT;
      ELSE /* based on delivery program from optimisation */
         incd_forecast_for_run_program;
      END IF;
      
      COMMIT;

      dbms_output.put_line('generate_incd_forecast END '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

   EXCEPTION
      WHEN v_prog_excp THEN
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line(TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss')||' EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END generate_incd_forecast;        

   /*---------------------------------------------------------------------------------------------------------------*/
   /* Generates data on risk.                                                                                       */
   /* If the one logical run consists of multiple physical runs (eg 5 * 10_year run for a 50-year outlook), this    */
   /* procedure is to be run with the last run_id, after all runs have completed.                                   */
   /*---------------------------------------------------------------------------------------------------------------*/    
   PROCEDURE generate_post_del_risk_profile(i_base_run_id  IN PLS_INTEGER
                                           ,i_program_id   IN PLS_INTEGER    DEFAULT NULL
                                           ,i_years        IN PLS_INTEGER    DEFAULT NULL
                                           ,i_delete_flg   IN VARCHAR2       DEFAULT 'N') IS

      v_dummy              CHAR(1);
      v_base_run_id        PLS_INTEGER;
      v_years              PLS_INTEGER;
--      v_parent_run_years   PLS_INTEGER;
--      v_parent_run_id      PLS_INTEGER;
--      v_parent_run_id_first      PLS_INTEGER;
--      v_parent_run_id_last       PLS_INTEGER;
      v_pick_id               VARCHAR2(20);
      v_suppress_pickids_ver	VARCHAR2(10);
   
      /*Private procedures for generate_post_del_risk_profile */
      
      /* Generate risk data following a delivery program */
      PROCEDURE risk_prof_for_run_program IS
         
         CURSOR actioned_pwod_eqp_csr IS

            WITH mtzn_program AS
            (
            SELECT A.del_year
            				,b.mtzn
                        ,A.program_id
                        ,A.base_run_id
            FROM structools.st_del_program_mtzn_optimised	A	INNER JOIN
            			 structools.st_mtzn													b
				ON A.mtzn = b.mtzn_opt                             
               WHERE base_run_id = i_base_run_id
               AND   program_id = i_program_id
               AND del_year > 0
            )
            ,    
            mtzn_reinf_program AS
            (
            SELECT A.del_year
            				,b.mtzn
                        ,A.program_id
                        ,A.base_run_id
            FROM structools.st_del_program_reinf_opt	A	INNER JOIN
            			structools.st_mtzn													b
				ON A.mtzn = b.mtzn_opt                            
               WHERE base_run_id = i_base_run_id
               AND   program_id = i_program_id
               AND del_year > 0
            )    
            ,
            seg_program AS
            (
            SELECT del_year
                  ,cons_segment_id
                  ,program_id
                  ,base_run_id
            FROM structools.st_del_program_seg_optimised       
               WHERE base_run_id = i_base_run_id
               AND   program_id = i_program_id
               AND del_year > 0
            )    
            ,
            zonal_and_seg_pickids AS
            (
            SELECT
                   b.pick_id
                  ,b.equip_grp_id
                  ,b.event
                  ,A.del_year    AS mtzn_del_year
                  ,c.del_year    AS seg_del_year
                  ,d.del_year    AS mtzn_reinf_del_year
            FROM  structools.st_temp_strategy_run        b  LEFT OUTER JOIN
                  mtzn_program                         A
            ON   A.MTZN = b.mtzn_grp
            AND A.del_year = B.CALENDAR_YEAR
                                                         LEFT OUTER JOIN
                  seg_program                         c                                                            
            ON   c.cons_segment_id = b.cons_segment_id
            AND  c.del_year = B.CALENDAR_YEAR
            															LEFT OUTER JOIN
                  mtzn_reinf_program                         d
            ON   d.MTZN = b.mtzn_grp
            AND d.del_year = B.CALENDAR_YEAR
            WHERE B.EVENT > ' '
            AND b.equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
            AND b.part_cde = ' '
            AND (b.pickid_suppressed = 'N' OR b.pickid_suppressed = 'P' AND event = 'REINFORCE')
            )
            SELECT    pick_id
                     ,equip_grp_id
                     ,event
                     ,CASE
                     	WHEN	mtzn_del_year	IS NOT NULL	THEN	mtzn_del_year
                        WHEN	seg_del_year	IS NOT NULL	THEN	seg_del_year
                        WHEN	mtzn_reinf_del_year	IS NOT NULL	THEN	mtzn_reinf_del_year
                     END		AS del_year
            FROM zonal_and_seg_pickids
            WHERE mtzn_del_year IS NOT NULL
            OR seg_del_year IS NOT NULL
            OR (mtzn_reinf_del_year IS NOT NULL AND event = 'REINFORCE')
            UNION
            SELECT
                   A.pick_id
                  ,A.equip_typ     AS equip_grp_id
                  ,A.event
                  ,A.del_year
              FROM  structools.st_del_program_sniper    A
            WHERE A.base_run_id = i_base_run_id
            AND   A.program_id = i_program_id
            AND A.equip_typ IN ('PWOD','DSTR','PTSD','SECT')
            AND A.part_cde = ' '
            UNION
            SELECT A.pick_id
                  ,A.equip_typ  AS equip_grp_id
                  ,A.event
                  ,A.del_year
            FROM structools.st_del_program_reac_maint   A       
            WHERE A.base_run_id = i_base_run_id
            AND   program_id = i_program_id
            AND A.equip_typ IN ('PWOD','DSTR','PTSD','SECT')
            AND A.part_cde =  ' '
            ORDER BY pick_id, del_year
         ;
         
         CURSOR   risk_pwod_eqp_csr IS

            WITH risk_pwod_eqp_noaction AS
            (
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_pwod_NOACTION  A
            UNION ALL
            SELECT   'N'||A.pick_id       AS pick_id_typ
                    ,A.*
            FROM structools.st_risk_dstr_NOACTION  A
            UNION ALL  
            SELECT 'N'||A.pick_id       AS pick_id_typ
                   ,A.* 
            FROM structools.st_risk_ptsd_NOACTION  A
            UNION ALL
            SELECT 'N'||A.pick_id       AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_sect_NOACTION  A
            )
            ,
            risk_pwod_eqp_noaction_npv15 AS
            (
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_pwod_NOACTION_npv15  A
            UNION ALL
            SELECT   'N'||A.pick_id       AS pick_id_typ
                    ,A.*
            FROM structools.st_risk_dstr_NOACTION_npv15  A
            UNION ALL
            SELECT 'N'||A.pick_id       AS pick_id_typ
                   ,A.* 
            FROM structools.st_risk_ptsd_NOACTION_npv15  A
            UNION ALL 
            SELECT 'N'||A.pick_id       AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_sect_NOACTION_npv15  A
            )
            ,
            risk_pwod_eqp_replace AS
            (
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_pwod_REPLACE  A
            UNION ALL
            SELECT   'N'||A.pick_id       AS pick_id_typ
                    ,A.*
            FROM structools.st_risk_dstr_REPLACE  A
            UNION ALL 
            SELECT 'N'||A.pick_id       AS pick_id_typ
                   ,A.* 
            FROM structools.st_risk_ptsd_REPLACE  A
            UNION ALL
            SELECT 'N'||A.pick_id       AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_sect_REPLACE  A
            )
            ,
            risk_pwod_eqp_replace_npv15 AS
            (
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_pwod_REPLACE_npv15  A
            UNION ALL
            SELECT   'N'||A.pick_id       AS pick_id_typ
                    ,A.*
            FROM structools.st_risk_dstr_REPLACE_npv15  A
            UNION ALL 
            SELECT 'N'||A.pick_id       AS pick_id_typ
                   ,A.* 
            FROM structools.st_risk_ptsd_REPLACE_npv15  A
            UNION ALL
            SELECT 'N'||A.pick_id       AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_sect_REPLACE_npv15  A
            )
            ,
            risk_pwod_reinforce AS
            (
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_pwod_REINF  A
            )
            ,
            risk_pwod_reinforce_npv15 AS
            (
            SELECT  'S'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_pwod_REINF_npv15  A
            )
--            ,
--            vic AS
--            (
--            SELECT aa.prnt_plnt_no
--            FROM
--            (
--            SELECT DISTINCT A.prnt_plnt_no
--            SELECT A.prnt_plnt_no
--                           ,b.event 
--            FROM   structools.st_EQUIPMENT            A  --LEFT OUTER JOIN
--                   structools.st_pickid_committed     b
--            ON A.pick_id = b.pick_id                  
--            AND A.part_cde = b.part_cde 
--            WHERE  A.equip_grp_id = 'PWOD'
--            AND A.part_cde = ' '
--            ) aa
--            WHERE aa.event IS NULL OR aa.event NOT IN ('SUPP', 'PUP')
--            )
	    ,
            suppressed_eqp AS
            (
            SELECT pick_id
                        ,'Y'					AS suppressed
            FROM structools.st_pickid_suppressed_init
	    WHERE equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
            UNION
            SELECT pick_id
                     ,'P'						AS suppressed				
            FROM structools.st_pickid_suppressed
	    WHERE equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
            AND scenario_id = v_suppress_pickids_ver
            )
            ,
--            equipment_tmp AS  
	    pickid_pwod_eqp AS
            (
            SELECT A.pick_id
                  ,A.equip_grp_id
--                  ,prnt_plnt_no
		  ,nvl(b.suppressed,'N')	AS suppressed
            FROM structools.st_equipment		A	LEFT OUTER JOIN
                 suppressed_eqp			b
	    ON A.pick_id = b.pick_id	
	    WHERE A.equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
            AND part_cde = ' '
            )
--            ,
--            pickid_pwod_eqp AS
--            (
--            SELECT DISTINCT A.pick_id
--                           ,A.equip_grp_id
--            FROM vic                      v        INNER JOIN
--                 equipment_tmp     A
--            ON v.prnt_plnt_no = A.prnt_plnt_no                 
--            WHERE A.equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
--            )
            ,
            risk AS
            (
            SELECT A.pick_id
                  ,A.equip_grp_id
                     ,c.risk_indx_val_Y0  AS risk_na_y0
                     ,c.risk_indx_val_Y1  AS risk_na_y1
                     ,c.risk_indx_val_Y2  AS risk_na_y2
                     ,c.risk_indx_val_y3  AS risk_na_y3
                     ,c.risk_indx_val_y4  AS risk_na_y4
                     ,c.risk_indx_val_y5  AS risk_na_y5
                     ,c.risk_indx_val_y6  AS risk_na_y6
                     ,c.risk_indx_val_y7  AS risk_na_y7
                     ,c.risk_indx_val_y8  AS risk_na_y8
                     ,c.risk_indx_val_y9  AS risk_na_y9
                     ,c.risk_indx_val_y10  AS risk_na_y10      /* 10 years + 0-year plus 2 years to allow to past replaces (via pickid_committed list) */
                     ,c.risk_indx_val_y11  AS risk_na_y11      /* If the run is for more than 10 years, this needs to be changed to extract more risk-years */
                     ,c.risk_indx_val_y12  AS risk_na_y12
                     ,d.risk_indx_val_Y0  AS risk_npv15_na_y0
                     ,d.risk_indx_val_Y1  AS risk_npv15_na_y1
                     ,d.risk_indx_val_Y2  AS risk_npv15_na_y2
                     ,d.risk_indx_val_y3  AS risk_npv15_na_y3
                     ,d.risk_indx_val_y4  AS risk_npv15_na_y4
                     ,d.risk_indx_val_y5  AS risk_npv15_na_y5
                     ,d.risk_indx_val_y6  AS risk_npv15_na_y6
                     ,d.risk_indx_val_y7  AS risk_npv15_na_y7
                     ,d.risk_indx_val_y8  AS risk_npv15_na_y8
                     ,d.risk_indx_val_y9  AS risk_npv15_na_y9
                     ,d.risk_indx_val_y10  AS risk_npv15_na_y10
                     ,d.risk_indx_val_y11  AS risk_npv15_na_y11
                     ,d.risk_indx_val_y12  AS risk_npv15_na_y12
                     ,E.risk_indx_val_Y0  AS risk_repl_y0
                     ,E.risk_indx_val_Y1  AS risk_repl_y1
                     ,E.risk_indx_val_Y2  AS risk_repl_y2
                     ,E.risk_indx_val_y3  AS risk_repl_y3
                     ,E.risk_indx_val_y4  AS risk_repl_y4
                     ,E.risk_indx_val_y5  AS risk_repl_y5
                     ,E.risk_indx_val_y6  AS risk_repl_y6
                     ,E.risk_indx_val_y7  AS risk_repl_y7
                     ,E.risk_indx_val_y8  AS risk_repl_y8
                     ,E.risk_indx_val_y9  AS risk_repl_y9
                     ,E.risk_indx_val_y10  AS risk_repl_y10
                     ,E.risk_indx_val_y11  AS risk_repl_y11
                     ,E.risk_indx_val_y12  AS risk_repl_y12
                     ,f.risk_indx_val_Y0  AS risk_npv15_repl_y0
                     ,f.risk_indx_val_Y1  AS risk_npv15_repl_y1
                     ,f.risk_indx_val_Y2  AS risk_npv15_repl_y2
                     ,f.risk_indx_val_y3  AS risk_npv15_repl_y3
                     ,f.risk_indx_val_y4  AS risk_npv15_repl_y4
                     ,f.risk_indx_val_y5  AS risk_npv15_repl_y5
                     ,f.risk_indx_val_y6  AS risk_npv15_repl_y6
                     ,f.risk_indx_val_y7  AS risk_npv15_repl_y7
                     ,f.risk_indx_val_y8  AS risk_npv15_repl_y8
                     ,f.risk_indx_val_y9  AS risk_npv15_repl_y9
                     ,f.risk_indx_val_y10  AS risk_npv15_repl_y10
                     ,f.risk_indx_val_y11  AS risk_npv15_repl_y11
                     ,f.risk_indx_val_y12  AS risk_npv15_repl_y12
                     ,G.risk_indx_val_Y0  AS risk_reinf_y0
                     ,G.risk_indx_val_Y1  AS risk_reinf_y1
                     ,G.risk_indx_val_Y2  AS risk_reinf_y2
                     ,G.risk_indx_val_y3  AS risk_reinf_y3
                     ,G.risk_indx_val_y4  AS risk_reinf_y4
                     ,G.risk_indx_val_y5  AS risk_reinf_y5
                     ,G.risk_indx_val_y6  AS risk_reinf_y6
                     ,G.risk_indx_val_y7  AS risk_reinf_y7
                     ,G.risk_indx_val_y8  AS risk_reinf_y8
                     ,G.risk_indx_val_y9  AS risk_reinf_y9
                     ,G.risk_indx_val_y10  AS risk_reinf_y10
                     ,G.risk_indx_val_y11  AS risk_reinf_y11
                     ,G.risk_indx_val_y12  AS risk_reinf_y12
                     ,h.risk_indx_val_Y0  AS risk_npv15_reinf_y0
                     ,h.risk_indx_val_Y1  AS risk_npv15_reinf_y1
                     ,h.risk_indx_val_Y2  AS risk_npv15_reinf_y2
                     ,h.risk_indx_val_y3  AS risk_npv15_reinf_y3
                     ,h.risk_indx_val_y4  AS risk_npv15_reinf_y4
                     ,h.risk_indx_val_y5  AS risk_npv15_reinf_y5
                     ,h.risk_indx_val_y6  AS risk_npv15_reinf_y6
                     ,h.risk_indx_val_y7  AS risk_npv15_reinf_y7
                     ,h.risk_indx_val_y8  AS risk_npv15_reinf_y8
                     ,h.risk_indx_val_y9  AS risk_npv15_reinf_y9
                     ,h.risk_indx_val_y10  AS risk_npv15_reinf_y10
                     ,h.risk_indx_val_y11  AS risk_npv15_reinf_y11
                     ,h.risk_indx_val_y12  AS risk_npv15_reinf_y12
            FROM  pickid_pwod_eqp                A  INNER JOIN
                  risk_pwod_eqp_noaction c                                          
            ON A.pick_id = c.pick_id_typ
                                          INNER JOIN
                  risk_pwod_eqp_noaction_npv15 d                                          
            ON A.pick_id = d.pick_id_typ
                                          INNER JOIN
                  risk_pwod_eqp_replace E                                          
            ON A.pick_id = E.pick_id_typ
                                          INNER JOIN
                  risk_pwod_eqp_replace_npv15 f                                          
            ON A.pick_id = f.pick_id_typ
                                          LEFT OUTER JOIN
                  risk_pwod_reinforce G                                          
            ON A.pick_id = G.pick_id_typ
                                          LEFT OUTER JOIN
                  risk_pwod_reinforce_npv15 h                                          
            ON A.pick_id = h.pick_id_typ
	    WHERE A.suppressed = 'N'
            )
            SELECT /*+ parallel (risk,8) */
                   pick_id
                  ,equip_grp_id
                  ,v_this_year + yr AS calendar_year
                  ,risk_na
                  ,risk_npv15_na
                  ,risk_repl
                  ,risk_npv15_repl
                  ,risk_reinf       AS risk_reinf_repair             /* renamed so that resultant columns are identical between the cursor for polles+eqp */
                  ,risk_npv15_reinf AS risk_npv15_reinf_repair       /* and bays, and have a single table based on the cursor definition. */
            FROM  risk
            UNPIVOT ((risk_na, risk_npv15_na, risk_repl, risk_npv15_repl, risk_reinf, risk_npv15_reinf) 
                                          FOR yr IN 
                                          ((risk_na_y0,risk_npv15_na_y0,risk_repl_y0,risk_npv15_repl_y0,risk_reinf_y0,risk_npv15_reinf_y0) AS '00'
                                          ,(risk_na_y1,risk_npv15_na_y1,risk_repl_y1,risk_npv15_repl_y1,risk_reinf_y1,risk_npv15_reinf_y1) AS '01'
                                          ,(risk_na_y2,risk_npv15_na_y2,risk_repl_y2,risk_npv15_repl_y2,risk_reinf_y2,risk_npv15_reinf_y2) AS '02'
                                          ,(risk_na_y3,risk_npv15_na_y3,risk_repl_y3,risk_npv15_repl_y3,risk_reinf_y3,risk_npv15_reinf_y3) AS '03'
                                          ,(risk_na_y4,risk_npv15_na_y4,risk_repl_y4,risk_npv15_repl_y4,risk_reinf_y4,risk_npv15_reinf_y4) AS '04'
                                          ,(risk_na_y5,risk_npv15_na_y5,risk_repl_y5,risk_npv15_repl_y5,risk_reinf_y5,risk_npv15_reinf_y5) AS '05'
                                          ,(risk_na_y6,risk_npv15_na_y6,risk_repl_y6,risk_npv15_repl_y6,risk_reinf_y6,risk_npv15_reinf_y6) AS '06'
                                          ,(risk_na_y7,risk_npv15_na_y7,risk_repl_y7,risk_npv15_repl_y7,risk_reinf_y7,risk_npv15_reinf_y7) AS '07'
                                          ,(risk_na_y8,risk_npv15_na_y8,risk_repl_y8,risk_npv15_repl_y8,risk_reinf_y8,risk_npv15_reinf_y8) AS '08'
                                          ,(risk_na_y9,risk_npv15_na_y9,risk_repl_y9,risk_npv15_repl_y9,risk_reinf_y9,risk_npv15_reinf_y9) AS '09'
                                          ,(risk_na_y10,risk_npv15_na_y10,risk_repl_y10,risk_npv15_repl_y10,risk_reinf_y10,risk_npv15_reinf_y10) AS '10'
                                          ,(risk_na_y11,risk_npv15_na_y11,risk_repl_y11,risk_npv15_repl_y11,risk_reinf_y11,risk_npv15_reinf_y11) AS '11'
                                          ,(risk_na_y12,risk_npv15_na_y12,risk_repl_y12,risk_npv15_repl_y12,risk_reinf_y12,risk_npv15_reinf_y12) AS '12'
                                          ))
            WHERE yr < i_years + 3
            ORDER BY pick_id, calendar_year
         ;

         CURSOR actioned_bay_csr IS
         
            WITH mtzn_program AS
            (
            SELECT A.del_year
                  ,b.mtzn
                  ,A.program_id
                  ,A.base_run_id
            FROM structools.st_del_program_mtzn_optimised		A	INNER JOIN
            			structools.st_mtzn														b
            ON A.mtzn = b.mtzn_opt       
            WHERE base_run_id = i_base_run_id   
            AND program_id = i_program_id
            AND del_year > 0
            )
            ,
            seg_program AS
            (
            SELECT del_year
                  ,cons_segment_id
                  ,program_id
                  ,base_run_id
            FROM structools.st_del_program_seg_optimised       
               WHERE base_run_id = i_base_run_id
               AND   program_id = i_program_id
               AND del_year > 0
            )
            ,
            zonal_and_seg_pickids AS
            (
            SELECT
                   b.pick_id
                  ,b.equip_grp_id
                  ,b.event
                  ,A.del_year    AS mtzn_del_year
                  ,c.del_year    AS seg_del_year
            FROM  structools.st_temp_bay_strategy_run        b  LEFT OUTER JOIN
                  mtzn_program                         A
            ON   A.MTZN = b.mtzn_grp
            AND A.del_year = B.CALENDAR_YEAR
                                                         LEFT OUTER JOIN
                  seg_program                         c                                                            
            ON   c.cons_segment_id = b.cons_segment_id
            AND  c.del_year = B.CALENDAR_YEAR
            WHERE B.EVENT > ' '
            AND   b.segment_rebuild_yr IS NOT NULL
            AND	b.pickid_suppressed = 'N'
            )
            SELECT    pick_id
                     ,equip_grp_id
                     ,event
                     ,NVL(mtzn_del_year,seg_del_year) AS del_year
            FROM zonal_and_seg_pickids
            WHERE mtzn_del_year IS NOT NULL
            OR seg_del_year IS NOT NULL
            UNION
            SELECT A.pick_id
                  ,A.equip_typ  AS equip_grp_id
                  ,A.event
                  ,A.del_year
            FROM structools.st_del_program_reac_maint   A       
            WHERE A.base_run_id = i_base_run_id
            AND   A.program_id = i_program_id
            AND   A.equip_typ = 'BAY'
            ORDER BY pick_id, del_year
         ;

         
         CURSOR   risk_bay_csr IS

            WITH risk_bay_noaction AS
            (
            SELECT  'B'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_bay_NOACTION  A
            )
            ,
            risk_bay_noaction_npv15 AS
            (
            SELECT  'B'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_bay_NOACTION_npv15  A
            )
            ,
            risk_bay_replace AS
            (
            SELECT  'B'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_bay_REPLACE  A
            )
            ,
            risk_bay_replace_npv15 AS
            (
            SELECT  'B'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_bay_REPLACE_npv15  A
            )
            ,
            risk_bay_repair AS
            (
            SELECT  'B'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_bay_repair  A
            )
            ,
            risk_bay_repair_npv15 AS
            (
            SELECT  'B'||A.pick_id        AS pick_id_typ
                    ,A.* 
            FROM structools.st_risk_bay_repair_npv15  A
            )
	    ,
            suppressed_bays AS
            (
            SELECT pick_id
                        ,'Y'					AS suppressed
            FROM structools.st_pickid_suppressed_init
	    WHERE equip_grp_id = 'BAY'
            UNION
            SELECT pick_id
                     ,'P'						AS suppressed				
            FROM structools.st_pickid_suppressed
	    WHERE equip_grp_id = 'BAY'
            AND scenario_id = v_suppress_pickids_ver
            )
            ,
            pickid_bay AS
            (
--            SELECT aa.pick_id
--                  ,aa.equip_grp_id
--            FROM                  
--            (
            SELECT A.pick_id
                   ,'BAY'        AS         equip_grp_id
--                   ,b.event
		   ,nvl(c.suppressed,'N')	AS suppressed
            FROM structools.st_bay_equipment    A  LEFT OUTER JOIN
		 suppressed_bays	             c
	    ON A.pick_id = c.pick_id			
--                 structools.st_pickid_committed b
--            ON A.pick_id = b.pick_id                 
--            ) aa
--            WHERE aa.event IS NULL OR aa.event NOT IN ('SUPP', 'PUP')
            )
            ,
            risk AS
            (
            SELECT A.pick_id
                  ,A.equip_grp_id
                     ,c.risk_indx_val_Y0  AS risk_na_y0
                     ,c.risk_indx_val_Y1  AS risk_na_y1
                     ,c.risk_indx_val_Y2  AS risk_na_y2
                     ,c.risk_indx_val_y3  AS risk_na_y3
                     ,c.risk_indx_val_y4  AS risk_na_y4
                     ,c.risk_indx_val_y5  AS risk_na_y5
                     ,c.risk_indx_val_y6  AS risk_na_y6
                     ,c.risk_indx_val_y7  AS risk_na_y7
                     ,c.risk_indx_val_y8  AS risk_na_y8
                     ,c.risk_indx_val_y9  AS risk_na_y9
                     ,c.risk_indx_val_y10  AS risk_na_y10      /* 10 years + 0-year plus 2 years to allow to past replaces (via pickid_committed list) */
                     ,c.risk_indx_val_y11  AS risk_na_y11      /* If the run is for more than 10 years, this needs to be changed to extract more risk-years */
                     ,c.risk_indx_val_y12  AS risk_na_y12
                     ,d.risk_indx_val_Y0  AS risk_npv15_na_y0
                     ,d.risk_indx_val_Y1  AS risk_npv15_na_y1
                     ,d.risk_indx_val_Y2  AS risk_npv15_na_y2
                     ,d.risk_indx_val_y3  AS risk_npv15_na_y3
                     ,d.risk_indx_val_y4  AS risk_npv15_na_y4
                     ,d.risk_indx_val_y5  AS risk_npv15_na_y5
                     ,d.risk_indx_val_y6  AS risk_npv15_na_y6
                     ,d.risk_indx_val_y7  AS risk_npv15_na_y7
                     ,d.risk_indx_val_y8  AS risk_npv15_na_y8
                     ,d.risk_indx_val_y9  AS risk_npv15_na_y9
                     ,d.risk_indx_val_y10  AS risk_npv15_na_y10
                     ,d.risk_indx_val_y11  AS risk_npv15_na_y11
                     ,d.risk_indx_val_y12  AS risk_npv15_na_y12
                     ,E.risk_indx_val_Y0  AS risk_repl_y0
                     ,E.risk_indx_val_Y1  AS risk_repl_y1
                     ,E.risk_indx_val_Y2  AS risk_repl_y2
                     ,E.risk_indx_val_y3  AS risk_repl_y3
                     ,E.risk_indx_val_y4  AS risk_repl_y4
                     ,E.risk_indx_val_y5  AS risk_repl_y5
                     ,E.risk_indx_val_y6  AS risk_repl_y6
                     ,E.risk_indx_val_y7  AS risk_repl_y7
                     ,E.risk_indx_val_y8  AS risk_repl_y8
                     ,E.risk_indx_val_y9  AS risk_repl_y9
                     ,E.risk_indx_val_y10  AS risk_repl_y10
                     ,E.risk_indx_val_y11  AS risk_repl_y11
                     ,E.risk_indx_val_y12  AS risk_repl_y12
                     ,f.risk_indx_val_Y0  AS risk_npv15_repl_y0
                     ,f.risk_indx_val_Y1  AS risk_npv15_repl_y1
                     ,f.risk_indx_val_Y2  AS risk_npv15_repl_y2
                     ,f.risk_indx_val_y3  AS risk_npv15_repl_y3
                     ,f.risk_indx_val_y4  AS risk_npv15_repl_y4
                     ,f.risk_indx_val_y5  AS risk_npv15_repl_y5
                     ,f.risk_indx_val_y6  AS risk_npv15_repl_y6
                     ,f.risk_indx_val_y7  AS risk_npv15_repl_y7
                     ,f.risk_indx_val_y8  AS risk_npv15_repl_y8
                     ,f.risk_indx_val_y9  AS risk_npv15_repl_y9
                     ,f.risk_indx_val_y10  AS risk_npv15_repl_y10
                     ,f.risk_indx_val_y11  AS risk_npv15_repl_y11
                     ,f.risk_indx_val_y12  AS risk_npv15_repl_y12
                     ,G.risk_indx_val_Y0  AS risk_repair_y0
                     ,G.risk_indx_val_Y1  AS risk_repair_y1
                     ,G.risk_indx_val_Y2  AS risk_repair_y2
                     ,G.risk_indx_val_y3  AS risk_repair_y3
                     ,G.risk_indx_val_y4  AS risk_repair_y4
                     ,G.risk_indx_val_y5  AS risk_repair_y5
                     ,G.risk_indx_val_y6  AS risk_repair_y6
                     ,G.risk_indx_val_y7  AS risk_repair_y7
                     ,G.risk_indx_val_y8  AS risk_repair_y8
                     ,G.risk_indx_val_y9  AS risk_repair_y9
                     ,G.risk_indx_val_y10  AS risk_repair_y10
                     ,G.risk_indx_val_y11  AS risk_repair_y11
                     ,G.risk_indx_val_y12  AS risk_repair_y12
                     ,h.risk_indx_val_Y0  AS risk_npv15_repair_y0
                     ,h.risk_indx_val_Y1  AS risk_npv15_repair_y1
                     ,h.risk_indx_val_Y2  AS risk_npv15_repair_y2
                     ,h.risk_indx_val_y3  AS risk_npv15_repair_y3
                     ,h.risk_indx_val_y4  AS risk_npv15_repair_y4
                     ,h.risk_indx_val_y5  AS risk_npv15_repair_y5
                     ,h.risk_indx_val_y6  AS risk_npv15_repair_y6
                     ,h.risk_indx_val_y7  AS risk_npv15_repair_y7
                     ,h.risk_indx_val_y8  AS risk_npv15_repair_y8
                     ,h.risk_indx_val_y9  AS risk_npv15_repair_y9
                     ,h.risk_indx_val_y10  AS risk_npv15_repair_y10
                     ,h.risk_indx_val_y11  AS risk_npv15_repair_y11
                     ,h.risk_indx_val_y12  AS risk_npv15_repair_y12
            FROM  pickid_bay                A  INNER JOIN
                  risk_bay_noaction c                                          
            ON A.pick_id = c.pick_id_typ
                                          INNER JOIN
                  risk_bay_noaction_npv15 d                                          
            ON A.pick_id = d.pick_id_typ
                                          INNER JOIN
                  risk_bay_replace E                                          
            ON A.pick_id = E.pick_id_typ
                                          INNER JOIN
                  risk_bay_replace_npv15 f                                          
            ON A.pick_id = f.pick_id_typ
                                          INNER JOIN
                  risk_bay_repair         G                                          
            ON A.pick_id = G.pick_id_typ
                                          INNER JOIN
                  risk_bay_repair_npv15 h                                          
            ON A.pick_id = h.pick_id_typ
	    WHERE A.suppressed = 'N'
            )
            SELECT /*+ parallel (risk,8) */
                   pick_id
                  ,equip_grp_id
                  ,v_this_year + yr AS calendar_year
                  ,risk_na
                  ,risk_npv15_na
                  ,risk_repl
                  ,risk_npv15_repl
                  ,risk_repair         AS risk_reinf_repair          /* renamed so that resultant columns are identical between the cursor for poles+eqp */
                  ,risk_npv15_repair   AS risk_npv15_reinf_repair    /* and bays, and have a single table based on the cursor definition. */
                  
            FROM  risk
            UNPIVOT ((risk_na, risk_npv15_na, risk_repl, risk_npv15_repl, risk_repair, risk_npv15_repair) 
                                          FOR yr IN 
                                          ((risk_na_y0,risk_npv15_na_y0,risk_repl_y0,risk_npv15_repl_y0,risk_repair_y0,risk_npv15_repair_y0) AS '00'
                                          ,(risk_na_y1,risk_npv15_na_y1,risk_repl_y1,risk_npv15_repl_y1,risk_repair_y1,risk_npv15_repair_y1) AS '01'
                                          ,(risk_na_y2,risk_npv15_na_y2,risk_repl_y2,risk_npv15_repl_y2,risk_repair_y2,risk_npv15_repair_y2) AS '02'
                                          ,(risk_na_y3,risk_npv15_na_y3,risk_repl_y3,risk_npv15_repl_y3,risk_repair_y3,risk_npv15_repair_y3) AS '03'
                                          ,(risk_na_y4,risk_npv15_na_y4,risk_repl_y4,risk_npv15_repl_y4,risk_repair_y4,risk_npv15_repair_y4) AS '04'
                                          ,(risk_na_y5,risk_npv15_na_y5,risk_repl_y5,risk_npv15_repl_y5,risk_repair_y5,risk_npv15_repair_y5) AS '05'
                                          ,(risk_na_y6,risk_npv15_na_y6,risk_repl_y6,risk_npv15_repl_y6,risk_repair_y6,risk_npv15_repair_y6) AS '06'
                                          ,(risk_na_y7,risk_npv15_na_y7,risk_repl_y7,risk_npv15_repl_y7,risk_repair_y7,risk_npv15_repair_y7) AS '07'
                                          ,(risk_na_y8,risk_npv15_na_y8,risk_repl_y8,risk_npv15_repl_y8,risk_repair_y8,risk_npv15_repair_y8) AS '08'
                                          ,(risk_na_y9,risk_npv15_na_y9,risk_repl_y9,risk_npv15_repl_y9,risk_repair_y9,risk_npv15_repair_y9) AS '09'
                                          ,(risk_na_y10,risk_npv15_na_y10,risk_repl_y10,risk_npv15_repl_y10,risk_repair_y10,risk_npv15_repair_y10) AS '10'
                                          ,(risk_na_y11,risk_npv15_na_y11,risk_repl_y11,risk_npv15_repl_y11,risk_repair_y11,risk_npv15_repair_y11) AS '11'
                                          ,(risk_na_y12,risk_npv15_na_y12,risk_repl_y12,risk_npv15_repl_y12,risk_repair_y12,risk_npv15_repair_y12) AS '12'
                                          ))
            WHERE yr < i_years + 3
            ORDER BY pick_id, calendar_year
         ;
         CURSOR   risk_fractions_csr IS

            SELECT pick_id
                  ,(fire_frac + eshock_frac + physimp_frac) AS pubsafety_frac
                  ,wforce_frac
                  ,reliability_frac
                  ,env_frac
                  ,fin_frac
            FROM structools.st_risk_fractions
            WHERE MODEL_NAME IN ('DSTR', 'PTSD', 'PWOD',  'SECT', 'BAY')
         ;

         TYPE actioned_eqp_tab_typ    IS TABLE OF actioned_pwod_eqp_csr%ROWTYPE;
         v_actioned_eqp_tab           actioned_eqp_tab_typ := actioned_eqp_tab_typ();

         TYPE actioned_rec_typ IS RECORD
         (event                        VARCHAR2(15)
         ,del_year                     PLS_INTEGER
         );
         TYPE action_tab_typ           IS TABLE OF actioned_rec_typ;
         v_action_tab                  action_tab_typ := action_tab_typ();
         
         TYPE actioned_arr_typ IS TABLE OF action_tab_typ INDEX BY VARCHAR2(20);
         v_actioned_arr        actioned_arr_typ;
            
         TYPE risk_tab_typ    IS TABLE OF risk_pwod_eqp_csr%ROWTYPE;
         v_risk_tab           risk_tab_typ := risk_tab_typ();

         TYPE risk_fractions_tab_typ   IS TABLE OF risk_fractions_csr%ROWTYPE;
         v_risk_fractions_tab          risk_fractions_tab_typ := risk_fractions_tab_typ();

         TYPE risk_fractions_rec_typ IS RECORD
         (pubsafety_frac               float(126)
         ,wforce_frac                  float(126)
         ,reliability_frac             float(126)
         ,env_frac                     float(126)
         ,fin_frac                     float(126)
         );
         
         TYPE risk_fractions_arr_typ IS TABLE OF risk_fractions_rec_typ INDEX BY VARCHAR2(20);
         v_risk_fractions_arr          risk_fractions_arr_typ;

         v_pick_id                     varchar2(10);
         v_egi                         varchar2(4);
         v_event                       VARCHAR2(15);
         v_del_year                    PLS_INTEGER;
         v_calendar_year               PLS_INTEGER;
         v_risk                        FLOAT(126);
         v_risk_npv15                  FLOAT(126);
         j                             PLS_INTEGER;
         v_risk_pubsafety              FLOAT(126);
         v_risk_wforce                 FLOAT(126);
         v_risk_reliability            FLOAT(126);
         v_risk_env                    FLOAT(126);
         v_risk_fin                    FLOAT(126);
         v_risk_npv15_pubsafety        FLOAT(126);
         v_risk_npv15_wforce           FLOAT(126);
         v_risk_npv15_reliability      FLOAT(126);
         v_risk_npv15_env              FLOAT(126);
         v_risk_npv15_fin              FLOAT(126);
         v_year                        PLS_INTEGER;
         v_action_cnt                  PLS_INTEGER;
         v_max_event_del_year          char(13);
         v_event_del_year              char(13);
         
      BEGIN

         /************************************************/
         /**** Process poles and eequipment on poles *****/
         /************************************************/
         
         /* dump all actioned eqp into a nested table */
         OPEN actioned_pwod_eqp_csr;
         FETCH actioned_pwod_eqp_csr BULK COLLECT INTO v_actioned_eqp_tab;
         CLOSE actioned_pwod_eqp_csr;
         
         /*reload them into an associative array, indexed by pick_id. Fill in missing years. */
         v_pick_id := v_actioned_eqp_tab(1).pick_id;
         j := 1;
         WHILE j <= v_actioned_eqp_tab.COUNT LOOP
            v_year := v_this_year + 1;
            v_action_cnt := 0;
            v_action_tab.DELETE;
            v_max_event_del_year := ' ';

            WHILE v_year <= v_this_year + i_years LOOP --2026
               v_del_year := v_actioned_eqp_tab(j).del_year;
               v_event := v_actioned_eqp_tab(j).event;
            
               IF v_year < v_del_year THEN
                  v_action_tab.EXTEND;
                  v_action_cnt := v_action_cnt + 1;
                  IF v_max_event_del_year = ' ' THEN
                     v_action_tab(v_action_cnt).event    := NULL;
                     v_action_tab(v_action_cnt).del_year := NULL;
                  ELSE
                     v_action_tab(v_action_cnt).event    := RTRIM(SUBSTR(v_max_event_del_year,1,9));
                     v_action_tab(v_action_cnt).del_year := SUBSTR(v_max_event_del_year,10,4);
                  END IF;
               ELSIF v_year = v_del_year THEN
                  v_action_tab.EXTEND;
                  v_action_cnt := v_action_cnt + 1;
                  v_event_del_year := RPAD(v_actioned_eqp_tab(j).event,9)||v_actioned_eqp_tab(j).del_year;
                  IF v_event_del_year > v_max_event_del_year THEN
                     v_max_event_del_year := v_event_del_year;
                  END IF;
                  v_action_tab(v_action_cnt).event    := RTRIM(SUBSTR(v_max_event_del_year,1,9));
                  v_action_tab(v_action_cnt).del_year := SUBSTR(v_max_event_del_year,10,4);

                  j := j + 1;
                  IF j <= v_actioned_eqp_tab.COUNT AND v_actioned_eqp_tab(j).pick_id = v_pick_id THEN
                     NULL;
                  ELSE
                     IF v_year < v_this_year + i_years THEN
                        WHILE v_year < v_this_year + i_years LOOP
                           v_year := v_year + 1;
                           v_action_tab.EXTEND;
                           v_action_cnt := v_action_cnt + 1;
                           v_action_tab(v_action_cnt).event    := RTRIM(SUBSTR(v_max_event_del_year,1,9));
                           v_action_tab(v_action_cnt).del_year := SUBSTR(v_max_event_del_year,10,4);
                        END LOOP;
                     END IF;
                  END IF;
               END IF;
               v_year := v_year + 1;
            END LOOP;        
            v_actioned_arr(v_pick_id) := v_action_tab;

            IF j <= v_actioned_eqp_tab.COUNT THEN
               v_pick_id := v_actioned_eqp_tab(j).pick_id;
            END IF;
            
         END LOOP;

         v_actioned_eqp_tab.DELETE;
         v_action_tab.DELETE;


         /* Fetch risk fractions for all pickids, all years, and store in a nested table */
         OPEN risk_fractions_csr;
         FETCH risk_fractions_csr BULK COLLECT INTO v_risk_fractions_tab;
         CLOSE risk_fractions_csr;

         /* Reload into an associative array, indexed by pick_id */
         FOR i IN 1 .. v_risk_fractions_tab.COUNT LOOP
            v_pick_id := v_risk_fractions_tab(i).pick_id;
            v_risk_fractions_arr(v_pick_id).pubsafety_frac    := v_risk_fractions_tab(i).pubsafety_frac;
            v_risk_fractions_arr(v_pick_id).wforce_frac       := v_risk_fractions_tab(i).wforce_frac;
            v_risk_fractions_arr(v_pick_id).reliability_frac := v_risk_fractions_tab(i).reliability_frac;
            v_risk_fractions_arr(v_pick_id).env_frac         := v_risk_fractions_tab(i).env_frac;
            v_risk_fractions_arr(v_pick_id).fin_frac         := v_risk_fractions_tab(i).fin_frac;
         END LOOP;
         
         v_risk_fractions_tab.DELETE;

         /* Process risk data, matching with the actioned table where exists. Insert into st_del_program_risk table */
         OPEN risk_pwod_eqp_csr;

         LOOP 

            /* Fetch risk data for a single pickid, all years, and store in a nested table */
            FETCH risk_pwod_eqp_csr BULK COLLECT INTO v_risk_tab LIMIT i_years + 3;
            EXIT WHEN risk_pwod_eqp_csr%NOTFOUND;
               
            v_pick_id            := v_risk_tab(1).pick_id;
            v_action_tab.DELETE;
            IF v_actioned_arr.EXISTS(v_pick_id) THEN
               v_action_tab         := v_actioned_arr(v_pick_id);
            END IF;
            v_egi                := v_risk_tab(1).equip_grp_id;
            FOR i IN 2 .. (v_risk_tab.COUNT - 2) LOOP          /* only need to process i_years, starting from v_this_year + 1. */
                                                               /* The rest is to access risk for replace/reinforce for committed pickids.*/
               IF v_action_tab.COUNT > 0 THEN
                  v_event     := v_action_tab(i - 1).event;
                  v_del_year  := v_action_tab(i - 1).del_year;
               ELSE
                  v_event     := NULL;
                  v_del_year  := NULL;
               END IF;     
               v_calendar_year := v_risk_tab(i).calendar_year;
               IF v_event IS NULL THEN                         
                  v_risk := v_risk_tab(i).risk_na;
                  v_risk_npv15 := v_risk_tab(i).risk_npv15_na;
               ELSIF v_event = 'REINFORCE' THEN
                  v_risk := v_risk_tab(i).risk_reinf_repair;
                  v_risk_npv15 := v_risk_tab(i).risk_npv15_reinf_repair;
               ELSIF v_event = 'REPLACE' THEN
                  j := v_calendar_year - v_del_year + 1;
                  v_risk := v_risk_tab(j).risk_repl;
                  v_risk_npv15 := v_risk_tab(j).risk_npv15_repl;
               END IF;
               v_risk_pubsafety     := v_risk * v_risk_fractions_arr(v_pick_id).pubsafety_frac;
               v_risk_wforce        := v_risk * v_risk_fractions_arr(v_pick_id).wforce_frac;
               v_risk_reliability   := v_risk * v_risk_fractions_arr(v_pick_id).reliability_frac;
               v_risk_env           := v_risk * v_risk_fractions_arr(v_pick_id).env_frac;
               v_risk_fin           := v_risk * v_risk_fractions_arr(v_pick_id).fin_frac;
               v_risk_npv15_pubsafety     := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).pubsafety_frac;
               v_risk_npv15_wforce        := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).wforce_frac;
               v_risk_npv15_reliability   := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).reliability_frac;
               v_risk_npv15_env           := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).env_frac;
               v_risk_npv15_fin           := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).fin_frac;
               
               INSERT INTO structools.st_del_program_risk
                  VALUES   (i_base_run_id
                           ,i_program_id
                           ,v_pick_id
                           ,v_egi
                           ,v_calendar_year
                           ,v_event
                           ,v_risk
                           ,v_risk_pubsafety
                           ,v_risk_wforce
                           ,v_risk_reliability
                           ,v_risk_env
                           ,v_risk_npv15
                           ,v_risk_npv15_pubsafety
                           ,v_risk_npv15_wforce
                           ,v_risk_npv15_reliability
                           ,v_risk_npv15_env
                           ,' '         --,v_part_cde_orig
                           ,v_risk_fin
                           ,v_risk_npv15_fin
                            );
            END LOOP;
         END LOOP;
         CLOSE risk_pwod_eqp_csr;
         
         v_actioned_arr.DELETE;
         
         /***********************/
         /**** Process bays *****/
         /***********************/
         
         /* dump all actioned eqp into a nested table */
         OPEN actioned_bay_csr;
         FETCH actioned_bay_csr BULK COLLECT INTO v_actioned_eqp_tab;
         CLOSE actioned_bay_csr;
         
         /*reload them into an associative array, indexed by pick_id. Fill in missing years. */
         v_pick_id := v_actioned_eqp_tab(1).pick_id;
         j := 1;
         WHILE j <= v_actioned_eqp_tab.COUNT LOOP
            v_year := v_this_year + 1;
            v_action_cnt := 0;
            v_action_tab.DELETE;
            v_max_event_del_year := ' ';

            WHILE v_year <= v_this_year + i_years LOOP

               v_del_year := v_actioned_eqp_tab(j).del_year;
               v_event := v_actioned_eqp_tab(j).event;
            
               IF v_year < v_del_year THEN
                  v_action_tab.EXTEND;
                  v_action_cnt := v_action_cnt + 1;
                  IF v_max_event_del_year = ' ' THEN
                     v_action_tab(v_action_cnt).event    := NULL;
                     v_action_tab(v_action_cnt).del_year := NULL;
                  ELSE
                     v_action_tab(v_action_cnt).event    := RTRIM(SUBSTR(v_max_event_del_year,1,9));
                     v_action_tab(v_action_cnt).del_year := SUBSTR(v_max_event_del_year,10,4);
                  END IF;
               ELSIF v_year = v_del_year THEN
                  v_action_tab.EXTEND;
                  v_action_cnt := v_action_cnt + 1;
                  v_event_del_year := RPAD(v_actioned_eqp_tab(j).event,9)||v_actioned_eqp_tab(j).del_year;
                  IF v_event_del_year > v_max_event_del_year THEN
                     v_max_event_del_year := v_event_del_year;
                  END IF;
                  v_action_tab(v_action_cnt).event    := RTRIM(SUBSTR(v_max_event_del_year,1,9));
                  v_action_tab(v_action_cnt).del_year := SUBSTR(v_max_event_del_year,10,4);

                  j := j + 1;
                  IF j <= v_actioned_eqp_tab.COUNT AND v_actioned_eqp_tab(j).pick_id = v_pick_id THEN
                     NULL;
                  ELSE
                     IF v_year < v_this_year + i_years THEN
                        WHILE v_year < v_this_year + i_years LOOP
                           v_year := v_year + 1;
                           v_action_tab.EXTEND;
                           v_action_cnt := v_action_cnt + 1;
                           v_action_tab(v_action_cnt).event    := RTRIM(SUBSTR(v_max_event_del_year,1,9));
                           v_action_tab(v_action_cnt).del_year := SUBSTR(v_max_event_del_year,10,4);
                        END LOOP;
                     END IF;
                  END IF;
               END IF;
               v_year := v_year + 1;
            END LOOP;        
            v_actioned_arr(v_pick_id) := v_action_tab;

            IF j <= v_actioned_eqp_tab.COUNT THEN
               v_pick_id := v_actioned_eqp_tab(j).pick_id;
            END IF;
            
         END LOOP;

         v_actioned_eqp_tab.DELETE;
         v_action_tab.DELETE;


         /* Process risk data, matching with the actioned table where exists. Insert into st_del_program_risk table */
         OPEN risk_bay_csr;

         LOOP 

            /* Fetch risk data for a single pickid, all years, and store in a nested table */
            FETCH risk_bay_csr BULK COLLECT INTO v_risk_tab LIMIT i_years + 3;
            EXIT WHEN risk_bay_csr%NOTFOUND;
               
            v_pick_id            := v_risk_tab(1).pick_id;
            v_action_tab.DELETE;
            IF v_actioned_arr.EXISTS(v_pick_id) THEN
               v_action_tab         := v_actioned_arr(v_pick_id);
            END IF;
            v_egi                := v_risk_tab(1).equip_grp_id;
            FOR i IN 2 .. (v_risk_tab.COUNT - 2) LOOP          /* only need to process i_years, starting from v_this_year + 1. */
                                                               /* The rest is to access risk for replace/reinforce for committed pickids.*/
               IF v_action_tab.COUNT > 0 THEN
                  v_event     := v_action_tab(i - 1).event;
                  v_del_year  := v_action_tab(i - 1).del_year;
               ELSE
                  v_event     := NULL;
                  v_del_year  := NULL;
               END IF;     
               v_calendar_year := v_risk_tab(i).calendar_year;
               IF v_event IS NULL THEN                         
                  v_risk := v_risk_tab(i).risk_na;
                  v_risk_npv15 := v_risk_tab(i).risk_npv15_na;
               ELSIF v_event = 'REPAIR' THEN
                  v_risk := v_risk_tab(i).risk_reinf_repair;
                  v_risk_npv15 := v_risk_tab(i).risk_npv15_reinf_repair;
               ELSIF v_event = 'REPLACE' THEN
                  j := v_calendar_year - v_del_year + 1;
                  v_risk := v_risk_tab(j).risk_repl;
                  v_risk_npv15 := v_risk_tab(j).risk_npv15_repl;
               END IF;
               v_risk_pubsafety     := v_risk * v_risk_fractions_arr(v_pick_id).pubsafety_frac;
               v_risk_wforce        := v_risk * v_risk_fractions_arr(v_pick_id).wforce_frac;
               v_risk_reliability   := v_risk * v_risk_fractions_arr(v_pick_id).reliability_frac;
               v_risk_env           := v_risk * v_risk_fractions_arr(v_pick_id).env_frac;
               v_risk_fin           := v_risk * v_risk_fractions_arr(v_pick_id).fin_frac;
               v_risk_npv15_pubsafety     := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).pubsafety_frac;
               v_risk_npv15_wforce        := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).wforce_frac;
               v_risk_npv15_reliability   := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).reliability_frac;
               v_risk_npv15_env           := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).env_frac;
               v_risk_npv15_fin           := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).fin_frac;
               INSERT INTO structools.st_del_program_risk
                  VALUES   (i_base_run_id
                           ,i_program_id
                           ,v_pick_id
                           ,v_egi
                           ,v_calendar_year
                           ,v_event
                           ,v_risk
                           ,v_risk_pubsafety
                           ,v_risk_wforce
                           ,v_risk_reliability
                           ,v_risk_env
                           ,v_risk_npv15
                           ,v_risk_npv15_pubsafety
                           ,v_risk_npv15_wforce
                           ,v_risk_npv15_reliability
                           ,v_risk_npv15_env
                           ,' '     --part_cde
                           ,v_risk_fin
                           ,v_risk_npv15_fin
                           );
            END LOOP;
         END LOOP;
         CLOSE risk_bay_csr;
   
         
      END risk_prof_for_run_program;   
   

   /*Mainline for generate_post_del_risk_profile procedure */
   BEGIN
      
      dbms_output.put_line('generate_post_del_risk_profile Start '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));
      
      BEGIN
         SELECT run_years
      		,suppress_pickids_ver
--               ,parent_run_id
         INTO   v_years
	       ,v_suppress_pickids_ver

--               ,v_parent_run_id
         FROM structools.st_run_request
         WHERE run_id = i_base_run_id;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            IF i_years IS NULL OR i_years = 0 THEN
               dbms_output.put_line('run ID NOT FOUND AND i_years parameter invalid');
               RAISE v_prog_excp;
            END IF;
      END;

      IF i_years IS NOT NULL THEN
         v_years := i_years;
      END IF;

      IF v_years > 50 THEN 
         dbms_output.put_line ('maximum OF 50 years VALUES can be GENERATED'); 
         RAISE v_prog_excp;
      END IF;
      
      IF i_delete_flg = 'Y' THEN
         dbms_output.put_line('Deleting existing results');
         IF i_program_id IS NULL THEN
            DELETE FROM structools.st_del_program_risk
            WHERE program_id IS NULL
            AND   base_run_id = i_base_run_id;
         ELSE
            DELETE FROM structools.st_del_program_risk
            WHERE program_id = i_program_id
            AND   base_run_id = i_base_run_id;
         END IF;
      ELSE
         BEGIN
            IF i_program_id IS NULL THEN
               SELECT NULL
               INTO     v_dummy                        
               FROM structools.st_del_program_risk
               WHERE base_run_id = i_base_run_id
               AND program_id IS NULL
               AND ROWNUM = 1;
            ELSE
               SELECT NULL
               INTO     v_dummy                        
               FROM structools.st_del_program_risk
               WHERE base_run_id = i_base_run_id
               AND program_id = i_program_id
               AND ROWNUM = 1;
            END IF;
            dbms_output.put_line ('st_del_program_risk already contains DATA FOR run_id '||i_base_run_id 
                                    ||' AND program_id '||i_program_id);
            RAISE v_prog_excp;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN 
               NULL;
         END;
      END IF;                     

      IF i_program_id IS NULL THEN      /* assuming nothing done */
         INSERT
            INTO structools.st_del_program_risk
         WITH risk_pickid AS
         (
         SELECT 'S'||aa.pick_id AS pick_id
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.risk_npv15
              ,bb.risk
              ,(aa.fire_frac + aa.eshock_frac + aa.physimp_frac) AS pubsafety_frac
              ,aa.wforce_frac
              ,aa.reliability_frac
              ,aa.env_frac
              ,aa.fin_frac
         FROM
            (
            SELECT *
            FROM  structools.st_risk_pwod_noaction_npv15
            UNPIVOT (risk_npv15 FOR yr_idx IN (risk_indx_val_y1 AS '01', risk_indx_val_y2 AS '02', risk_indx_val_y3 AS '03'
               ,risk_indx_val_y4 AS '04', risk_indx_val_y5 AS '05', risk_indx_val_y6 AS '06', risk_indx_val_y7 AS '07',risk_indx_val_y8 AS '08', risk_indx_val_y9 AS '09'
               ,risk_indx_val_y10 AS '10',risk_indx_val_y11 AS '11', risk_indx_val_y12 AS '12', risk_indx_val_y13 AS '13', risk_indx_val_y14 AS '14'
               ,risk_indx_val_y15 AS '15', risk_indx_val_y16 AS '16', risk_indx_val_y17 AS '17', risk_indx_val_y18 AS '18', risk_indx_val_y19 AS '19'
               ,risk_indx_val_y20 AS '20', risk_indx_val_y21 AS '21', risk_indx_val_y22 AS '22', risk_indx_val_y23 AS '23', risk_indx_val_y24 AS '24'
               ,risk_indx_val_y25 AS '25', risk_indx_val_y26 AS '26', risk_indx_val_y27 AS '27', risk_indx_val_y28 AS '28', risk_indx_val_y29 AS '29'
               ,risk_indx_val_y30 AS '30', risk_indx_val_y31 AS '31', risk_indx_val_y32 AS '32', risk_indx_val_y33 AS '33', risk_indx_val_y34 AS '34'
               ,risk_indx_val_y35 AS '35', risk_indx_val_y36 AS '36', risk_indx_val_y37 AS '37', risk_indx_val_y38 AS '38', risk_indx_val_y39 AS '39'
               ,risk_indx_val_y40 AS '40', risk_indx_val_y41 AS '41', risk_indx_val_y42 AS '42', risk_indx_val_y43 AS '43', risk_indx_val_y44 AS '44'
               ,risk_indx_val_y45 AS '45', risk_indx_val_y46 AS '46', risk_indx_val_y47 AS '47', risk_indx_val_y48 AS '48', risk_indx_val_y49 AS '49', risk_indx_val_y50 AS '50'))
            )     aa          INNER JOIN
            (
            SELECT *
            FROM  structools.st_risk_pwod_noaction
            UNPIVOT (risk FOR yr_idx IN (risk_indx_val_y1 AS '01', risk_indx_val_y2 AS '02', risk_indx_val_y3 AS '03'
               ,risk_indx_val_y4 AS '04', risk_indx_val_y5 AS '05', risk_indx_val_y6 AS '06', risk_indx_val_y7 AS '07',risk_indx_val_y8 AS '08', risk_indx_val_y9 AS '09'
               ,risk_indx_val_y10 AS '10',risk_indx_val_y11 AS '11', risk_indx_val_y12 AS '12', risk_indx_val_y13 AS '13', risk_indx_val_y14 AS '14'
               ,risk_indx_val_y15 AS '15', risk_indx_val_y16 AS '16', risk_indx_val_y17 AS '17', risk_indx_val_y18 AS '18', risk_indx_val_y19 AS '19'
               ,risk_indx_val_y20 AS '20', risk_indx_val_y21 AS '21', risk_indx_val_y22 AS '22', risk_indx_val_y23 AS '23', risk_indx_val_y24 AS '24'
               ,risk_indx_val_y25 AS '25', risk_indx_val_y26 AS '26', risk_indx_val_y27 AS '27', risk_indx_val_y28 AS '28', risk_indx_val_y29 AS '29'
               ,risk_indx_val_y30 AS '30', risk_indx_val_y31 AS '31', risk_indx_val_y32 AS '32', risk_indx_val_y33 AS '33', risk_indx_val_y34 AS '34'
               ,risk_indx_val_y35 AS '35', risk_indx_val_y36 AS '36', risk_indx_val_y37 AS '37', risk_indx_val_y38 AS '38', risk_indx_val_y39 AS '39'
               ,risk_indx_val_y40 AS '40', risk_indx_val_y41 AS '41', risk_indx_val_y42 AS '42', risk_indx_val_y43 AS '43', risk_indx_val_y44 AS '44'
               ,risk_indx_val_y45 AS '45', risk_indx_val_y46 AS '46', risk_indx_val_y47 AS '47', risk_indx_val_y48 AS '48', risk_indx_val_y49 AS '49', risk_indx_val_y50 AS '50'))
            )     bb
            ON aa.pick_id = bb.pick_id
            AND aa.yr_idx = bb.yr_idx
         UNION ALL
         SELECT 'N'||aa.pick_id AS pick_id
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.risk_npv15
              ,bb.risk
              ,(aa.fire_frac + aa.eshock_frac + aa.physimp_frac) AS pubsafety_frac
              ,aa.wforce_frac
              ,aa.reliability_frac
              ,aa.env_frac
              ,aa.fin_frac
         FROM
            (
            SELECT *
            FROM  structools.st_risk_dstr_noaction_npv15
            UNPIVOT (risk_npv15 FOR yr_idx IN (risk_indx_val_y1 AS '01', risk_indx_val_y2 AS '02', risk_indx_val_y3 AS '03'
               ,risk_indx_val_y4 AS '04', risk_indx_val_y5 AS '05', risk_indx_val_y6 AS '06', risk_indx_val_y7 AS '07',risk_indx_val_y8 AS '08', risk_indx_val_y9 AS '09'
               ,risk_indx_val_y10 AS '10',risk_indx_val_y11 AS '11', risk_indx_val_y12 AS '12', risk_indx_val_y13 AS '13', risk_indx_val_y14 AS '14'
               ,risk_indx_val_y15 AS '15', risk_indx_val_y16 AS '16', risk_indx_val_y17 AS '17', risk_indx_val_y18 AS '18', risk_indx_val_y19 AS '19'
               ,risk_indx_val_y20 AS '20', risk_indx_val_y21 AS '21', risk_indx_val_y22 AS '22', risk_indx_val_y23 AS '23', risk_indx_val_y24 AS '24'
               ,risk_indx_val_y25 AS '25', risk_indx_val_y26 AS '26', risk_indx_val_y27 AS '27', risk_indx_val_y28 AS '28', risk_indx_val_y29 AS '29'
               ,risk_indx_val_y30 AS '30', risk_indx_val_y31 AS '31', risk_indx_val_y32 AS '32', risk_indx_val_y33 AS '33', risk_indx_val_y34 AS '34'
               ,risk_indx_val_y35 AS '35', risk_indx_val_y36 AS '36', risk_indx_val_y37 AS '37', risk_indx_val_y38 AS '38', risk_indx_val_y39 AS '39'
               ,risk_indx_val_y40 AS '40', risk_indx_val_y41 AS '41', risk_indx_val_y42 AS '42', risk_indx_val_y43 AS '43', risk_indx_val_y44 AS '44'
               ,risk_indx_val_y45 AS '45', risk_indx_val_y46 AS '46', risk_indx_val_y47 AS '47', risk_indx_val_y48 AS '48', risk_indx_val_y49 AS '49', risk_indx_val_y50 AS '50'))
            )     aa INNER JOIN
            (
            SELECT *
            FROM  structools.st_risk_dstr_noaction
            UNPIVOT (risk FOR yr_idx IN (risk_indx_val_y1 AS '01', risk_indx_val_y2 AS '02', risk_indx_val_y3 AS '03'
               ,risk_indx_val_y4 AS '04', risk_indx_val_y5 AS '05', risk_indx_val_y6 AS '06', risk_indx_val_y7 AS '07',risk_indx_val_y8 AS '08', risk_indx_val_y9 AS '09'
               ,risk_indx_val_y10 AS '10',risk_indx_val_y11 AS '11', risk_indx_val_y12 AS '12', risk_indx_val_y13 AS '13', risk_indx_val_y14 AS '14'
               ,risk_indx_val_y15 AS '15', risk_indx_val_y16 AS '16', risk_indx_val_y17 AS '17', risk_indx_val_y18 AS '18', risk_indx_val_y19 AS '19'
               ,risk_indx_val_y20 AS '20', risk_indx_val_y21 AS '21', risk_indx_val_y22 AS '22', risk_indx_val_y23 AS '23', risk_indx_val_y24 AS '24'
               ,risk_indx_val_y25 AS '25', risk_indx_val_y26 AS '26', risk_indx_val_y27 AS '27', risk_indx_val_y28 AS '28', risk_indx_val_y29 AS '29'
               ,risk_indx_val_y30 AS '30', risk_indx_val_y31 AS '31', risk_indx_val_y32 AS '32', risk_indx_val_y33 AS '33', risk_indx_val_y34 AS '34'
               ,risk_indx_val_y35 AS '35', risk_indx_val_y36 AS '36', risk_indx_val_y37 AS '37', risk_indx_val_y38 AS '38', risk_indx_val_y39 AS '39'
               ,risk_indx_val_y40 AS '40', risk_indx_val_y41 AS '41', risk_indx_val_y42 AS '42', risk_indx_val_y43 AS '43', risk_indx_val_y44 AS '44'
               ,risk_indx_val_y45 AS '45', risk_indx_val_y46 AS '46', risk_indx_val_y47 AS '47', risk_indx_val_y48 AS '48', risk_indx_val_y49 AS '49', risk_indx_val_y50 AS '50'))
            )     bb
            ON aa.pick_id = bb.pick_id
            AND aa.yr_idx = bb.yr_idx
         UNION ALL
         SELECT 'N'||aa.pick_id AS pick_id
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.risk_npv15
              ,bb.risk
              ,(aa.fire_frac + aa.eshock_frac + aa.physimp_frac) AS pubsafety_frac
              ,aa.wforce_frac
              ,aa.reliability_frac
              ,aa.env_frac
              ,aa.fin_frac
         FROM
            (
            SELECT *
            FROM  structools.st_risk_ptsd_noaction_npv15
            UNPIVOT (risk_npv15 FOR yr_idx IN (risk_indx_val_y1 AS '01', risk_indx_val_y2 AS '02', risk_indx_val_y3 AS '03'
               ,risk_indx_val_y4 AS '04', risk_indx_val_y5 AS '05', risk_indx_val_y6 AS '06', risk_indx_val_y7 AS '07',risk_indx_val_y8 AS '08', risk_indx_val_y9 AS '09'
               ,risk_indx_val_y10 AS '10',risk_indx_val_y11 AS '11', risk_indx_val_y12 AS '12', risk_indx_val_y13 AS '13', risk_indx_val_y14 AS '14'
               ,risk_indx_val_y15 AS '15', risk_indx_val_y16 AS '16', risk_indx_val_y17 AS '17', risk_indx_val_y18 AS '18', risk_indx_val_y19 AS '19'
               ,risk_indx_val_y20 AS '20', risk_indx_val_y21 AS '21', risk_indx_val_y22 AS '22', risk_indx_val_y23 AS '23', risk_indx_val_y24 AS '24'
               ,risk_indx_val_y25 AS '25', risk_indx_val_y26 AS '26', risk_indx_val_y27 AS '27', risk_indx_val_y28 AS '28', risk_indx_val_y29 AS '29'
               ,risk_indx_val_y30 AS '30', risk_indx_val_y31 AS '31', risk_indx_val_y32 AS '32', risk_indx_val_y33 AS '33', risk_indx_val_y34 AS '34'
               ,risk_indx_val_y35 AS '35', risk_indx_val_y36 AS '36', risk_indx_val_y37 AS '37', risk_indx_val_y38 AS '38', risk_indx_val_y39 AS '39'
               ,risk_indx_val_y40 AS '40', risk_indx_val_y41 AS '41', risk_indx_val_y42 AS '42', risk_indx_val_y43 AS '43', risk_indx_val_y44 AS '44'
               ,risk_indx_val_y45 AS '45', risk_indx_val_y46 AS '46', risk_indx_val_y47 AS '47', risk_indx_val_y48 AS '48', risk_indx_val_y49 AS '49', risk_indx_val_y50 AS '50'))
            )     aa INNER JOIN
            (
            SELECT *
            FROM  structools.st_risk_ptsd_noaction
            UNPIVOT (risk FOR yr_idx IN (risk_indx_val_y1 AS '01', risk_indx_val_y2 AS '02', risk_indx_val_y3 AS '03'
               ,risk_indx_val_y4 AS '04', risk_indx_val_y5 AS '05', risk_indx_val_y6 AS '06', risk_indx_val_y7 AS '07',risk_indx_val_y8 AS '08', risk_indx_val_y9 AS '09'
               ,risk_indx_val_y10 AS '10',risk_indx_val_y11 AS '11', risk_indx_val_y12 AS '12', risk_indx_val_y13 AS '13', risk_indx_val_y14 AS '14'
               ,risk_indx_val_y15 AS '15', risk_indx_val_y16 AS '16', risk_indx_val_y17 AS '17', risk_indx_val_y18 AS '18', risk_indx_val_y19 AS '19'
               ,risk_indx_val_y20 AS '20', risk_indx_val_y21 AS '21', risk_indx_val_y22 AS '22', risk_indx_val_y23 AS '23', risk_indx_val_y24 AS '24'
               ,risk_indx_val_y25 AS '25', risk_indx_val_y26 AS '26', risk_indx_val_y27 AS '27', risk_indx_val_y28 AS '28', risk_indx_val_y29 AS '29'
               ,risk_indx_val_y30 AS '30', risk_indx_val_y31 AS '31', risk_indx_val_y32 AS '32', risk_indx_val_y33 AS '33', risk_indx_val_y34 AS '34'
               ,risk_indx_val_y35 AS '35', risk_indx_val_y36 AS '36', risk_indx_val_y37 AS '37', risk_indx_val_y38 AS '38', risk_indx_val_y39 AS '39'
               ,risk_indx_val_y40 AS '40', risk_indx_val_y41 AS '41', risk_indx_val_y42 AS '42', risk_indx_val_y43 AS '43', risk_indx_val_y44 AS '44'
               ,risk_indx_val_y45 AS '45', risk_indx_val_y46 AS '46', risk_indx_val_y47 AS '47', risk_indx_val_y48 AS '48', risk_indx_val_y49 AS '49', risk_indx_val_y50 AS '50'))
            )     bb
            ON aa.pick_id = bb.pick_id
            AND aa.yr_idx = bb.yr_idx
         UNION ALL
         SELECT 'N'||aa.pick_id AS pick_id
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.risk_npv15
              ,bb.risk
              ,(aa.fire_frac + aa.eshock_frac + aa.physimp_frac) AS pubsafety_frac
              ,aa.wforce_frac
              ,aa.reliability_frac
              ,aa.env_frac
              ,aa.fin_frac
         FROM
            (
            SELECT *
            FROM  structools.st_risk_sect_noaction_npv15
            UNPIVOT (risk_npv15 FOR yr_idx IN (risk_indx_val_y1 AS '01', risk_indx_val_y2 AS '02', risk_indx_val_y3 AS '03'
               ,risk_indx_val_y4 AS '04', risk_indx_val_y5 AS '05', risk_indx_val_y6 AS '06', risk_indx_val_y7 AS '07',risk_indx_val_y8 AS '08', risk_indx_val_y9 AS '09'
               ,risk_indx_val_y10 AS '10',risk_indx_val_y11 AS '11', risk_indx_val_y12 AS '12', risk_indx_val_y13 AS '13', risk_indx_val_y14 AS '14'
               ,risk_indx_val_y15 AS '15', risk_indx_val_y16 AS '16', risk_indx_val_y17 AS '17', risk_indx_val_y18 AS '18', risk_indx_val_y19 AS '19'
               ,risk_indx_val_y20 AS '20', risk_indx_val_y21 AS '21', risk_indx_val_y22 AS '22', risk_indx_val_y23 AS '23', risk_indx_val_y24 AS '24'
               ,risk_indx_val_y25 AS '25', risk_indx_val_y26 AS '26', risk_indx_val_y27 AS '27', risk_indx_val_y28 AS '28', risk_indx_val_y29 AS '29'
               ,risk_indx_val_y30 AS '30', risk_indx_val_y31 AS '31', risk_indx_val_y32 AS '32', risk_indx_val_y33 AS '33', risk_indx_val_y34 AS '34'
               ,risk_indx_val_y35 AS '35', risk_indx_val_y36 AS '36', risk_indx_val_y37 AS '37', risk_indx_val_y38 AS '38', risk_indx_val_y39 AS '39'
               ,risk_indx_val_y40 AS '40', risk_indx_val_y41 AS '41', risk_indx_val_y42 AS '42', risk_indx_val_y43 AS '43', risk_indx_val_y44 AS '44'
               ,risk_indx_val_y45 AS '45', risk_indx_val_y46 AS '46', risk_indx_val_y47 AS '47', risk_indx_val_y48 AS '48', risk_indx_val_y49 AS '49', risk_indx_val_y50 AS '50'))
            )     aa INNER JOIN
            (
            SELECT *
            FROM  structools.st_risk_sect_noaction
            UNPIVOT (risk FOR yr_idx IN (risk_indx_val_y1 AS '01', risk_indx_val_y2 AS '02', risk_indx_val_y3 AS '03'
               ,risk_indx_val_y4 AS '04', risk_indx_val_y5 AS '05', risk_indx_val_y6 AS '06', risk_indx_val_y7 AS '07',risk_indx_val_y8 AS '08', risk_indx_val_y9 AS '09'
               ,risk_indx_val_y10 AS '10',risk_indx_val_y11 AS '11', risk_indx_val_y12 AS '12', risk_indx_val_y13 AS '13', risk_indx_val_y14 AS '14'
               ,risk_indx_val_y15 AS '15', risk_indx_val_y16 AS '16', risk_indx_val_y17 AS '17', risk_indx_val_y18 AS '18', risk_indx_val_y19 AS '19'
               ,risk_indx_val_y20 AS '20', risk_indx_val_y21 AS '21', risk_indx_val_y22 AS '22', risk_indx_val_y23 AS '23', risk_indx_val_y24 AS '24'
               ,risk_indx_val_y25 AS '25', risk_indx_val_y26 AS '26', risk_indx_val_y27 AS '27', risk_indx_val_y28 AS '28', risk_indx_val_y29 AS '29'
               ,risk_indx_val_y30 AS '30', risk_indx_val_y31 AS '31', risk_indx_val_y32 AS '32', risk_indx_val_y33 AS '33', risk_indx_val_y34 AS '34'
               ,risk_indx_val_y35 AS '35', risk_indx_val_y36 AS '36', risk_indx_val_y37 AS '37', risk_indx_val_y38 AS '38', risk_indx_val_y39 AS '39'
               ,risk_indx_val_y40 AS '40', risk_indx_val_y41 AS '41', risk_indx_val_y42 AS '42', risk_indx_val_y43 AS '43', risk_indx_val_y44 AS '44'
               ,risk_indx_val_y45 AS '45', risk_indx_val_y46 AS '46', risk_indx_val_y47 AS '47', risk_indx_val_y48 AS '48', risk_indx_val_y49 AS '49', risk_indx_val_y50 AS '50'))
            )     bb
            ON aa.pick_id = bb.pick_id
            AND aa.yr_idx = bb.yr_idx
         UNION ALL
         SELECT 'B'||aa.pick_id AS pick_id
              ,(aa.yr_idx + v_this_year) AS calendar_year
              ,aa.risk_npv15
              ,bb.risk
              ,(aa.fire_frac + aa.eshock_frac + aa.physimp_frac) AS pubsafety_frac
              ,aa.wforce_frac
              ,aa.reliability_frac
              ,aa.env_frac
              ,aa.fin_frac
         FROM
            (
            SELECT *
            FROM  structools.st_risk_bay_noaction_npv15
            UNPIVOT (risk_npv15 FOR yr_idx IN (risk_indx_val_y1 AS '01', risk_indx_val_y2 AS '02', risk_indx_val_y3 AS '03'
               ,risk_indx_val_y4 AS '04', risk_indx_val_y5 AS '05', risk_indx_val_y6 AS '06', risk_indx_val_y7 AS '07',risk_indx_val_y8 AS '08', risk_indx_val_y9 AS '09'
               ,risk_indx_val_y10 AS '10',risk_indx_val_y11 AS '11', risk_indx_val_y12 AS '12', risk_indx_val_y13 AS '13', risk_indx_val_y14 AS '14'
               ,risk_indx_val_y15 AS '15', risk_indx_val_y16 AS '16', risk_indx_val_y17 AS '17', risk_indx_val_y18 AS '18', risk_indx_val_y19 AS '19'
               ,risk_indx_val_y20 AS '20', risk_indx_val_y21 AS '21', risk_indx_val_y22 AS '22', risk_indx_val_y23 AS '23', risk_indx_val_y24 AS '24'
               ,risk_indx_val_y25 AS '25', risk_indx_val_y26 AS '26', risk_indx_val_y27 AS '27', risk_indx_val_y28 AS '28', risk_indx_val_y29 AS '29'
               ,risk_indx_val_y30 AS '30', risk_indx_val_y31 AS '31', risk_indx_val_y32 AS '32', risk_indx_val_y33 AS '33', risk_indx_val_y34 AS '34'
               ,risk_indx_val_y35 AS '35', risk_indx_val_y36 AS '36', risk_indx_val_y37 AS '37', risk_indx_val_y38 AS '38', risk_indx_val_y39 AS '39'
               ,risk_indx_val_y40 AS '40', risk_indx_val_y41 AS '41', risk_indx_val_y42 AS '42', risk_indx_val_y43 AS '43', risk_indx_val_y44 AS '44'
               ,risk_indx_val_y45 AS '45', risk_indx_val_y46 AS '46', risk_indx_val_y47 AS '47', risk_indx_val_y48 AS '48', risk_indx_val_y49 AS '49', risk_indx_val_y50 AS '50'))
            )     aa INNER JOIN
            (
            SELECT *
            FROM  structools.st_risk_bay_noaction
            UNPIVOT (risk FOR yr_idx IN (risk_indx_val_y1 AS '01', risk_indx_val_y2 AS '02', risk_indx_val_y3 AS '03'
               ,risk_indx_val_y4 AS '04', risk_indx_val_y5 AS '05', risk_indx_val_y6 AS '06', risk_indx_val_y7 AS '07',risk_indx_val_y8 AS '08', risk_indx_val_y9 AS '09'
               ,risk_indx_val_y10 AS '10',risk_indx_val_y11 AS '11', risk_indx_val_y12 AS '12', risk_indx_val_y13 AS '13', risk_indx_val_y14 AS '14'
               ,risk_indx_val_y15 AS '15', risk_indx_val_y16 AS '16', risk_indx_val_y17 AS '17', risk_indx_val_y18 AS '18', risk_indx_val_y19 AS '19'
               ,risk_indx_val_y20 AS '20', risk_indx_val_y21 AS '21', risk_indx_val_y22 AS '22', risk_indx_val_y23 AS '23', risk_indx_val_y24 AS '24'
               ,risk_indx_val_y25 AS '25', risk_indx_val_y26 AS '26', risk_indx_val_y27 AS '27', risk_indx_val_y28 AS '28', risk_indx_val_y29 AS '29'
               ,risk_indx_val_y30 AS '30', risk_indx_val_y31 AS '31', risk_indx_val_y32 AS '32', risk_indx_val_y33 AS '33', risk_indx_val_y34 AS '34'
               ,risk_indx_val_y35 AS '35', risk_indx_val_y36 AS '36', risk_indx_val_y37 AS '37', risk_indx_val_y38 AS '38', risk_indx_val_y39 AS '39'
               ,risk_indx_val_y40 AS '40', risk_indx_val_y41 AS '41', risk_indx_val_y42 AS '42', risk_indx_val_y43 AS '43', risk_indx_val_y44 AS '44'
               ,risk_indx_val_y45 AS '45', risk_indx_val_y46 AS '46', risk_indx_val_y47 AS '47', risk_indx_val_y48 AS '48', risk_indx_val_y49 AS '49', risk_indx_val_y50 AS '50'))
            )     bb
            ON aa.pick_id = bb.pick_id
            AND aa.yr_idx = bb.yr_idx
         )
            ,
            suppressed_eqp_bays AS
            (
            SELECT pick_id
                        ,'Y'					AS suppressed
            FROM structools.st_pickid_suppressed_init
            WHERE equip_grp_id IN ('PWOD','DSTR','PTSD','SECT','BAY')
--            UNION
--            SELECT pick_id
--                     ,'P'						AS suppressed				
--            FROM structools.st_pickid_suppressed
--            WHERE equip_grp_id IN ('PWOD','DSTR','PTSD','SECT','BAY')
--            AND scenario_id = v_suppress_pickids_ver
            )
--         ,
--         vic AS 
--         (
--         SELECT aa.prnt_plnt_no
--         FROM
--         (
--         SELECT DISTINCT A.prnt_plnt_no 
--         SELECT A.prnt_plnt_no 
--                        ,b.event
--         FROM   structools.st_EQUIPMENT               A  --LEFT OUTER JOIN
--                structools.st_pickid_committed        b
--         ON A.pick_id = b.pick_id     
--         AND A.part_cde = b.part_cde           
--         WHERE  equip_grp_id IN = 'PWOD'
--         AND A.part_cde = ' '
--         ) aa
--         WHERE aa.event IS NULL OR aa.event NOT IN ('SUPP', 'PUP')
--         )
         ,
         pickid AS
         (
         SELECT DISTINCT A.pick_id
                        ,A.equip_grp_id
		        ,nvl(b.suppressed,'N') AS suppressed
         FROM --vic                      v        INNER JOIN
              structools.st_equipment     A	LEFT OUTER JOIN
              suppressed_eqp_bays		b
	 ON A.pick_id = b.pick_id
--         ON v.prnt_plnt_no = A.prnt_plnt_no                 
         WHERE A.equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
         AND A.part_cde = ' '
         UNION ALL
--         SELECT aa.pick_id
--               ,aa.equip_grp_id
--         FROM
--         (
--         SELECT DISTINCT A.pick_id
         SELECT   A.pick_id
                        ,'BAY'         AS equip_grp_id
--                        ,b.event
			,nvl(c.suppressed,'N') AS suppressed
         FROM structools.st_bay_equipment    A      LEFT OUTER JOIN
	      suppressed_eqp_bays              c
	 ON A.pick_id = c.pick_id
--              structools.st_pickid_committed b
--         ON A.pick_id = b.pick_id               
--         ) aa
--         WHERE aa.event IS NULL OR aa.event NOT IN ('SUPP', 'PUP')
         )
         SELECT --i_base_run_id       AS run_id
               0                    AS run_id
               ,NULL                AS program_id
               ,A.pick_id
               ,A.equip_grp_id
               ,b.calendar_year
               --,'NOACTION'          AS event
               ,NULL                   AS event
               ,b.risk
               ,b.risk * pubsafety_frac    AS risk_pubsafety
               ,b.risk * wforce_frac       AS risk_wforce
               ,b.risk * reliability_frac  AS risk_reliability
               ,b.risk * env_frac          AS risk_env
               ,b.risk_npv15
               ,b.risk_npv15 * pubsafety_frac    AS risk_npv15_pubsafety
               ,b.risk_npv15 * wforce_frac       AS risk_npv15_wforce
               ,b.risk_npv15 * reliability_frac  AS risk_npv15_reliability
               ,b.risk_npv15 * env_frac          AS risk_npv15_env
               ,' '                       AS part_cde --A.part_cde_orig
               ,b.risk * fin_frac          AS risk_fin
               ,b.risk_npv15 * fin_frac          AS risk_npv15_fin
         FROM pickid              A     INNER JOIN
              risk_pickid      b
         ON A.pick_id = b.pick_id
         WHERE b.calendar_year <= v_this_year + v_years
	 AND A.suppressed = 'N'
         ;
      ELSE /* based on delivery program from optimisation */
         risk_prof_for_run_program;
      END IF;            

      COMMIT;            

      dbms_output.put_line('generate_post_del_risk_profile END '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

   EXCEPTION
      WHEN v_prog_excp THEN
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line(TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss')||' EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END generate_post_del_risk_profile;        

   /*---------------------------------------------------------------------------------------------------------------*/
   /* Generates asset counts in all the different age values.                                                       */
   /* If the one logical run consists of multiple physical runs (eg 5 * 10_year run for a 50-year outlook), this    */
   /* procedure is to be rune with the last run_id, after all runs have completed.                                  */
   /*---------------------------------------------------------------------------------------------------------------*/    
   PROCEDURE generate_age_stats(i_program_id             IN PLS_INTEGER
                               ,i_base_run_id            IN PLS_INTEGER
                               ,i_years                  IN PLS_INTEGER
                               ,i_delete_flg             IN VARCHAR2    DEFAULT 'N') IS

      v_prog_excp           EXCEPTION;
      v_base_run_id         PLS_INTEGER;
      v_first_year          PLS_INTEGER;
      v_run_years           PLS_INTEGER;
      v_existing_record_cnt PLS_INTEGER;
      v_suppress_pickids_ver	VARCHAR2(10);

   BEGIN
   
      dbms_output.put_line('generate_age_stats Start '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

      IF i_program_id IS NULL THEN
         SELECT COUNT(*)
         INTO   v_existing_record_cnt
         FROM   structools.ST_DEL_PROGRAM_age_stats
         WHERE  program_id IS NULL
         AND    base_run_id = i_base_run_id;
      ELSE
         SELECT COUNT(*)
         INTO   v_existing_record_cnt
         FROM   structools.ST_DEL_PROGRAM_age_stats
         WHERE  program_id = i_program_id
         AND    base_run_id = i_base_run_id;
      END IF;

      IF i_delete_flg = 'Y' THEN
         dbms_output.put_line('Deleting existing results');
         
         IF i_program_id IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_age_stats
            WHERE program_id IS NULL
            AND   base_run_id = i_base_run_id;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_age_stats
            WHERE program_id = i_program_id
            AND   base_run_id = i_base_run_id;
         END IF;
      ELSE
         dbms_output.put_line('Existing results NOT deleted');
         
         IF v_existing_record_cnt > 0 THEN
            dbms_output.put_line('EXCEPTION Existing results NOT deleted');
            RAISE v_prog_excp;
         END IF;
      END IF;

      

      IF i_base_run_id != 0 THEN
         SELECT first_year
               ,run_years
      	       ,suppress_pickids_ver
         INTO   v_first_year
               ,v_run_years
	       ,v_suppress_pickids_ver
         FROM structools.st_run_request
         WHERE run_id = i_base_run_id;
         
      END IF;

--      get_parameter_scenarios(i_base_run_id);
      structoolsapp.common_procs.get_parameter_scenarios(i_base_run_id, v_parm_scen_id_life_exp, v_parm_scen_id_seg_min_len,  v_parm_scen_id_seg_cons_pcnt, v_parm_scen_id_sif_weighting
                                                               , v_parm_scen_id_seg_rr_doll,  v_parm_scen_id_eqp_rr_doll, v_parm_scen_id_fdr_cat_adj);   
      

      IF i_program_id IS NULL THEN
         INSERT
         INTO structools.ST_DEL_PROGRAM_age_stats
         WITH years_tmp AS
         (
         SELECT v_this_year      AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 1  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 2  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 3  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 4  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 5  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 6  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 7  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 8  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 9  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 10  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 11  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 12  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 13  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 14  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 15  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 16  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 17  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 18  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 19  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 20  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 21  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 22  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 23  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 24  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 25  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 26  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 27  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 28  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 29  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 30  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 31  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 32  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 33  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 34  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 35  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 36  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 37  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 38  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 39  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 40  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 41  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 42  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 43  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 44  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 45  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 46  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 47  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 48  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 49  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 50  AS calc_year
         FROM dual
         ORDER BY calc_year
         )
         ,
         years AS
         (
         SELECT calc_year
         FROM years_tmp 
         WHERE calc_year <= v_this_year + i_years
         )
--         ,
--         vic AS
--         (
--         SELECT aa.prnt_plnt_no
--         FROM
--         (
--         SELECT DISTINCT A.prnt_plnt_no 
--         SELECT A.prnt_plnt_no 
--                        ,b.event
--         FROM   structools.st_EQUIPMENT               A  --LEFT OUTER JOIN
--                structools.st_pickid_committed        b
--         ON A.pick_id = b.pick_id
--         AND A.part_cde = b.part_cde
--         WHERE  A.equip_grp_id = 'PWOD'
--         AND A.part_cde = ' '
--         ) aa
--         WHERE aa.event IS NULL OR aa.event NOT IN ('SUPP', 'PUP')
--         )
            ,
         suppressed_eqp_bays AS
         (
         SELECT pick_id
                     ,'Y'					AS suppressed
         FROM structools.st_pickid_suppressed_init
--         UNION
--         SELECT pick_id
--                  ,'P'						AS suppressed				
--         FROM structools.st_pickid_suppressed
--         WHERE scenario_id = v_suppress_pickids_ver
         )
         ,
         assets AS
         (
         SELECT b.pick_id
               ,equip_grp_id
               ,CASE
                  WHEN  equip_grp_id = 'PWOD' AND (wood_type IS NULL OR wood_type = 'S')  THEN  equip_grp_id||NVL(wood_type, ' ')
                  WHEN  equip_grp_id = 'PWOD' AND wood_type = 'H' AND reinf_cde IS NOT NULL AND instln_year < 1960  
                                                                                          THEN  equip_grp_id||wood_type||'Y'||'Y'
                  WHEN  equip_grp_id = 'PWOD' AND wood_type = 'H' AND reinf_cde IS NOT NULL AND instln_year >= 1960  
                                                                                          THEN  equip_grp_id||wood_type||'Y'||'N'
                  WHEN  equip_grp_id = 'PWOD' AND wood_type = 'H' AND reinf_cde IS NULL AND instln_year < 1960  
                                                                                          THEN  equip_grp_id||wood_type||'N'||'Y'
                  WHEN  equip_grp_id = 'PWOD' AND wood_type = 'H' AND reinf_cde IS NULL AND instln_year >= 1960  
                                                                                          THEN  equip_grp_id||wood_type||'N'||'N'
                  WHEN  equip_grp_id = 'DSTR' AND manuf = 'ASEA BROWN BOVERI PTY LTD'     THEN  equip_grp_id||NVL(dstr_size, ' ')||'ABB'
                  WHEN  equip_grp_id = 'DSTR'                                             THEN  equip_grp_id||NVL(dstr_size, ' ')||' '
                  ELSE                                                                          equip_grp_id
               END                           AS egi
               ,instln_year  
               ,0                              AS bay_len_m
               ,CASE
                   WHEN b.fire_risk_zone_cls = 'UNKNOWN'  THEN ' '
                   WHEN b.fire_risk_zone_cls IS NULL      THEN ' '
                   ELSE                                         b.fire_risk_zone_cls
                END                           AS fire_zone 
               ,NVL(wood_type, ' ')          AS wood_type
               ,NVL(dstr_size, ' ')          AS dstr_size
               ,CASE
                  WHEN equip_grp_id = 'PWOD' AND wood_type = 'H' AND (reinf_cde IS NULL OR reinf_cde = ' ')             THEN  'N'
                  WHEN equip_grp_id = 'PWOD' AND wood_type = 'H' AND reinf_cde != ' '                                   THEN  'Y'
                  ELSE                                                                                                        ' '
               END                    AS reinf_ind
               ,CASE
                  WHEN equip_grp_id = 'PWOD' AND wood_type = 'H' AND (instln_year < 1960 OR instln_year IS NULL)        THEN  'Y'
                  WHEN equip_grp_id = 'PWOD' AND wood_type = 'H' AND instln_year >= 1960                                THEN  'N'
                  ELSE                                                                                                        ' '
               END                    AS pre1960_ind
               ,CASE
                  WHEN equip_grp_id = 'DSTR' AND manuf = 'ASEA BROWN BOVERI PTY LTD'   THEN 'ABB'
                  ELSE                                                                       ' '
               END                           AS dstr_manuf
               ,NVL(UPPER(c.scnrrr_fdr_cat_nam), ' ')         AS fdr_cat
	       ,NVL(d.suppressed,'N')				AS suppressed	
         FROM  --vic                      A                    INNER JOIN
               structools.st_equipment     b		LEFT OUTER JOIN
    	       suppressed_eqp_bays		d
	 		ON b.pick_id = d.pick_id
--         ON    A.prnt_plnt_no = b.prnt_plnt_no
                                                             LEFT OUTER JOIN
               structools.st_hv_feeder        c
         ON    b.feeder_id = c.hv_fdr_id                                                                            
         WHERE b.equip_grp_id NOT IN ('PAUS','PCON','PMET') AND part_cde = ' '
         UNION ALL
         SELECT A.pick_id
            ,'BAY'                        AS equip_grp_id
            ,'BAY'                        AS egi
            ,A.instln_year  
            ,A.bay_len_m
            ,CASE
                WHEN A.fire_risk_zone_cls = 'UNKNOWN'  THEN ' '
                WHEN A.fire_risk_zone_cls IS NULL      THEN ' '
                ELSE                                         A.fire_risk_zone_cls
             END                          AS fire_zone 
            ,' '                          AS wood_type
            ,' '                          AS dstr_size
            ,' '                          AS reinf_ind
            ,' '                          AS pre1960_ind
            ,' '                          AS dstr_manuf
            ,NVL(UPPER(c.scnrrr_fdr_cat_nam), ' ')         AS fdr_cat
	    ,nvl(b.suppressed,'N')	  AS suppressed
         FROM  structools.st_bay_equipment     A               LEFT OUTER JOIN
	       suppressed_eqp_bays		    b
	 ON A.pick_id = b.pick_id
							    LEFT OUTER JOIN
               structools.st_hv_feeder         c
         ON    A.feeder_id = c.hv_fdr_id
	 )
         ,
         assets_year AS
         (
         SELECT A.*
               ,b.calc_year
               ,b.calc_year - A.instln_year            AS age
         FROM   assets                        A             CROSS JOIN               
                years                         b
	 WHERE A.suppressed = 'N'
         )
         ,
         life_expectancy AS
         (
         SELECT DISTINCT parameter_egi
               ,NVL(parameter_wood_type, ' ')            AS parameter_wood_type
               ,NVL(parameter_dstr_size, ' ')            AS parameter_dstr_size
               ,NVL(parameter_dstr_manuf, ' ')           AS parameter_dstr_manuf
               ,NVL(parameter_pwod_reinf_ind, ' ')       AS parameter_pwod_reinf_ind
               ,NVL(parameter_pwod_pre1960_ind, ' ')     AS parameter_pwod_pre1960_ind
               ,parameter_val       AS life_exp
         FROM structools.st_parameter_scenario
         WHERE parameter_id = 'LIFE_EXPECTANCY'
         AND scenario_id = v_parm_scen_id_life_exp
         )
         ,
         temp2 AS
         (
         SELECT A.pick_id
               ,A.egi
               ,A.fire_zone
               ,A.calc_year
               ,A.age
               ,A.bay_len_m
               ,A.fdr_cat
               ,ROUND((A.age/c.life_exp), 2) * 100 AS pcnt_des_lfe
         FROM assets_year           A  INNER JOIN
              life_expectancy       c
         ON   A.equip_grp_id      = c.parameter_egi
         AND  A.wood_type        = c.parameter_wood_type
         AND  A.reinf_ind        = c.parameter_pwod_reinf_ind
         AND  A.pre1960_ind      = c.parameter_pwod_pre1960_ind
         AND  A.dstr_size        = c.parameter_dstr_size
         AND  A.dstr_manuf       = c.parameter_dstr_manuf
         )
         ,
         temp3 AS
         (
         SELECT calc_year
               ,age
               ,egi
               ,pcnt_des_lfe
               ,fire_zone
               ,fdr_cat
               ,NVL(COUNT(*), 0)       AS cnt
         FROM temp2
         GROUP BY calc_year
               ,age
               ,egi  
               ,pcnt_des_lfe
               ,fire_zone    
               ,fdr_cat
         )
         ,
         bay_len   AS
         (
         SELECT calc_year
               ,age
               ,fire_zone
               ,fdr_cat
               ,SUM(bay_len_m)         AS bay_len_m
         FROM temp2
         WHERE egi = 'BAY'
         GROUP BY calc_year
               ,age
               ,fire_zone    
               ,fdr_cat
         )
--         ,
--         stats AS
--         (
         SELECT NULL                AS program_id
               ,i_base_run_id       AS base_run_id
               ,calc_year
               ,age
               ,fire_zone
               ,NULL                         AS pwod_h_cnt
               ,NULL                         AS pwod_h_pcnt_des_lfe
               ,NVL(pwod_s_cnt, 0) AS pwod_s_cnt
               ,NVL(pwod_s_pcnt_des_lfe, 0) AS pwod_s_pcnt_des_lfe
               ,NULL                         AS dstr_cnt
               ,NULL                         AS dstr_pcnt_des_lfe
               ,NVL(dof_cnt, 0)  AS dof_cnt
               ,NVL(dof_pcnt_des_lfe, 0) AS dof_pcnt_des_lfe
               ,NVL(ptsd_cnt, 0) AS ptsd_cnt
               ,NVL(ptsd_pcnt_des_lfe, 0) AS ptsd_pcnt_des_lfe
               ,NVL(recl_cnt, 0) AS recl_cnt
               ,NVL(recl_pcnt_des_lfe, 0) AS recl_pcnt_des_lfe
               ,NVL(sect_cnt, 0) AS sect_cnt
               ,NVL(sect_pcnt_des_lfe, 0) AS sect_pcnt_des_lfe
               ,NVL(lbs_cnt, 0)  AS lbs_cnt
               ,NVL(lbs_pcnt_des_lfe, 0) AS lbs_pcnt_des_lfe
               ,NVL(capb_cnt, 0) AS capb_cnt
               ,NVL(capb_pcnt_des_lfe, 0) AS capb_pcnt_des_lfe
               ,NVL(rgtr_cnt, 0) AS rgtr_cnt
               ,NVL(rgtr_pcnt_des_lfe, 0) AS rgtr_pcnt_des_lfe
               ,NVL(bay_cnt, 0)  AS bay_cnt
               ,NVL(bay_pcnt_des_lfe, 0) AS bay_pcnt_des_lfe
               ,NVL(reac_cnt, 0) AS reac_cnt
               ,NVL(reac_pcnt_des_lfe, 0) AS reac_pcnt_des_lfe
               ,fdr_cat
               ,NULL                      AS bay_len_m_dnu
               ,NVL(dstr_small_abb_cnt, 0) AS dstr_small_abb_cnt
               ,NVL(dstr_small_abb_pcnt_des_lfe, 0) AS dstr_small_abb_pcnt_des_lfe
               ,NVL(dstr_small_n_abb_cnt, 0) AS dstr_small_n_abb_cnt
               ,NVL(dstr_small_n_abb_pcnt_des_lfe, 0) AS dstr_small_n_abb_pcnt_des_lfe
               ,NVL(dstr_medium_abb_cnt, 0)              AS dstr_medium_abb_cnt
               ,NVL(dstr_medium_abb_pcnt_des_lfe, 0)    AS dstr_medium_abb_pcnt_des_lfe
               ,NVL(dstr_medium_n_abb_cnt, 0)            AS dstr_medium_n_abb_cnt
               ,NVL(dstr_medium_n_abb_pcnt_des_lfe, 0)  AS dstr_medium_n_abb_pcnt_des_lfe
               ,NVL(dstr_large_abb_cnt, 0)              AS dstr_large_abb_cnt
               ,NVL(dstr_large_abb_pcnt_des_lfe, 0)    AS dstr_large_abb_pcnt_des_lfe
               ,NVL(dstr_large_n_abb_cnt, 0)            AS dstr_large_n_abb_cnt
               ,NVL(dstr_large_n_abb_pcnt_des_lfe, 0)  AS dstr_large_n_abb_pcnt_des_lfe
               ,NVL(pwod_h_y_y_cnt, 0)                AS pwod_h_r_pre60_cnt
               ,NVL(pwod_h_y_y_pcnt_des_lfe, 0)       AS pwod_h_r_pre60_pcnt_des_lfe
               ,NVL(pwod_h_y_n_cnt, 0)                AS pwod_h_r_post60_cnt
               ,NVL(pwod_h_y_n_pcnt_des_lfe, 0)       AS pwod_h_r_post60_pcnt_des_lfe
               ,NVL(pwod_h_n_y_cnt, 0)                AS pwod_h_nr_pre60_cnt
               ,NVL(pwod_h_n_y_pcnt_des_lfe, 0)       AS pwod_h_nr_pre60_pcnt_des_lfe
               ,NVL(pwod_h_n_n_cnt, 0)                AS pwod_h_nr_post60_cnt
               ,NVL(pwod_h_n_n_pcnt_des_lfe, 0)       AS pwod_h_nr_post60_pcnt_des_lfe
               ,bay_len_m
               ,NULL                      AS hvxm_cnt
               ,NULL                      AS hvxm_pcnt_des_lfe
               ,NULL                      AS lvxm_cnt
               ,NULL                      AS lvxm_pcnt_des_lfe
               ,NULL                      AS astay_cnt
               ,NULL                      AS astay_pcnt_des_lfe
               ,NULL                      AS gstay_cnt
               ,NULL                      AS gstay_pcnt_des_lfe
               ,NULL                      AS ostay_cnt
               ,NULL                      AS ostay_pcnt_des_lfe
               ,NULL                      AS agost_cnt
               ,NULL                      AS agost_pcnt_des_lfe
         FROM 
         (
         SELECT A.calc_year
               ,A.age
               ,A.fire_zone
               ,A.cnt
               ,A.egi
               ,A.pcnt_des_lfe
               ,A.fdr_cat
               ,NVL(b.bay_len_m, 0)   AS bay_len_m
         FROM temp3           A                   LEFT OUTER JOIN 
               bay_len        b  
         ON    A.calc_year = b.calc_year
         AND   A.age         = b.age
         AND   A.fire_zone = b.fire_zone
         AND   A.fdr_cat   = b.fdr_cat              
         )
         PIVOT (MAX(cnt) AS cnt,
               MAX(pcnt_des_lfe) AS pcnt_des_lfe FOR egi IN ('PWODHYY' pwod_h_y_y, 'PWODHYN' pwod_h_y_n, 'PWODHNY' pwod_h_n_y, 'PWODHNN' pwod_h_n_n
                                                   , 'PWODS' pwod_s
                                                   , 'DSTRSMALL ABB' dstr_small_abb, 'DSTRSMALL  ' dstr_small_n_abb
                                                   , 'DSTRMEDIUMABB' dstr_medium_abb, 'DSTRMEDIUM ' dstr_medium_n_abb
                                                   , 'DSTRLARGE ABB' dstr_large_abb, 'DSTRLARGE  ' dstr_large_n_abb
                                                   , 'DOF' dof, 'PTSD' ptsd, 'RECL' recl
                                                     ,'SECT' sect, 'LBS' lbs, 'CAPB' capb, 'RGTR' rgtr, 'BAY' bay, 'REAC' reac ))
         ORDER BY calc_year, age, fire_zone, fdr_cat
         ;
      ELSE
         INSERT
         INTO structools.ST_DEL_PROGRAM_age_stats
         WITH years_tmp AS
         (
         SELECT v_this_year      AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 1  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 2  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 3  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 4  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 5  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 6  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 7  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 8  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 9  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 10  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 11  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 12  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 13  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 14  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 15  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 16  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 17  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 18  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 19  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 20  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 21  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 22  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 23  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 24  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 25  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 26  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 27  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 28  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 29  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 30  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 31  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 32  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 33  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 34  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 35  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 36  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 37  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 38  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 39  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 40  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 41  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 42  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 43  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 44  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 45  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 46  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 47  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 48  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 49  AS calc_year
         FROM dual
         UNION ALL
         SELECT v_this_year + 50  AS calc_year
         FROM dual
         ORDER BY calc_year
         )
         ,
         years AS
         (
         SELECT calc_year
         FROM years_tmp 
         WHERE calc_year <= v_this_year + i_years
         )
         ,
         suppressed_eqp_bays AS
         (
         SELECT pick_id
                     ,'Y'					AS suppressed
         FROM structools.st_pickid_suppressed_init
         UNION
         SELECT pick_id
                  ,'P'						AS suppressed				
         FROM structools.st_pickid_suppressed
         WHERE scenario_id = v_suppress_pickids_ver
         )
--         vic AS
--         (
--         SELECT aa.prnt_plnt_no
--         FROM
--         (
--         SELECT DISTINCT A.prnt_plnt_no
--         SELECT A.prnt_plnt_no
--                        ,b.event 
--         FROM   structools.st_EQUIPMENT            A  --LEFT OUTER JOIN
--                structools.st_pickid_committed     b
--         ON A.pick_id = b.pick_id
--         AND A.part_cde = b.part_cde                
--         WHERE  A.equip_grp_id = 'PWOD'
--         AND A.part_cde = ' '
--         ) aa
--         WHERE aa.event IS NULL OR aa.event NOT IN ('SUPP', 'PUP')
--         )
         ,
         del_prog AS
         (
         SELECT pick_id
               ,base_run_id
               ,event
               ,MAX(del_year)                AS del_year
         FROM  structools.st_del_program_pickid   
         WHERE  base_run_id = i_base_run_id
         AND    program_id = i_program_id
         AND   event IN ('REPLACE','REINFORCE')
         AND part_cde =  ' '
         GROUP BY pick_id, base_run_id, event
         )
         ,                       
         assets AS
         (
         SELECT b.pick_id
               ,equip_grp_id
               ,CASE
                  WHEN  equip_grp_id = 'PWOD' AND (wood_type IS NULL OR wood_type = 'S')  THEN  equip_grp_id||NVL(wood_type, ' ')
                  WHEN  equip_grp_id = 'PWOD' AND wood_type = 'H' AND d.commit_yr IS NOT NULL AND d.event = 'REPLACE'
                                                                                          THEN  equip_grp_id||wood_type||'N'||'N'
                  WHEN  equip_grp_id = 'PWOD' AND wood_type = 'H' AND instln_year < 1960 AND d.commit_yr IS NOT NULL AND d.event = 'REINFORCE' 
                                                                                          THEN  equip_grp_id||wood_type||'Y'||'Y'
                  WHEN  equip_grp_id = 'PWOD' AND wood_type = 'H' AND instln_year >= 1960 AND d.commit_yr IS NOT NULL AND d.event = 'REINFORCE' 
                                                                                          THEN  equip_grp_id||wood_type||'Y'||'N'
                  WHEN  equip_grp_id = 'PWOD' AND wood_type = 'H' AND reinf_cde IS NOT NULL AND instln_year < 1960  
                                                                                          THEN  equip_grp_id||wood_type||'Y'||'Y'
                  WHEN  equip_grp_id = 'PWOD' AND wood_type = 'H' AND reinf_cde IS NOT NULL AND instln_year >= 1960  
                                                                                          THEN  equip_grp_id||wood_type||'Y'||'N'
                  WHEN  equip_grp_id = 'PWOD' AND wood_type = 'H' AND reinf_cde IS NULL AND instln_year < 1960  
                                                                                          THEN  equip_grp_id||wood_type||'N'||'Y'
                  WHEN  equip_grp_id = 'PWOD' AND wood_type = 'H' AND reinf_cde IS NULL AND instln_year >= 1960  
                                                                                          THEN  equip_grp_id||wood_type||'N'||'N'
                  WHEN  equip_grp_id = 'DSTR' AND manuf = 'ASEA BROWN BOVERI PTY LTD'     THEN  equip_grp_id||NVL(dstr_size, ' ')||'ABB'
                  WHEN  equip_grp_id = 'DSTR'                                             THEN  equip_grp_id||NVL(dstr_size, ' ')||' '
                  ELSE                                                                          equip_grp_id
               END                           AS egi
               ,CASE                      --pickid init
                  WHEN d.commit_yr IS NOT NULL AND d.event = 'REPLACE'     THEN  d.commit_yr
                  ELSE                                                           b.instln_year
               END                              AS instln_year
               ,0                              AS bay_len_m
               ,CASE
                   WHEN b.fire_risk_zone_cls = 'UNKNOWN'  THEN ' '
                   WHEN b.fire_risk_zone_cls IS NULL      THEN ' '
                   ELSE                                         b.fire_risk_zone_cls
                END                           AS fire_zone 
               ,NVL(wood_type, ' ')          AS wood_type
               ,NVL(dstr_size, ' ')          AS dstr_size
               ,CASE
                  WHEN equip_grp_id = 'DSTR' AND manuf = 'ASEA BROWN BOVERI PTY LTD'   THEN 'ABB'
                  ELSE                                                                       ' '
               END                           AS dstr_manuf
               ,NVL(UPPER(c.scnrrr_fdr_cat_nam), ' ')         AS fdr_cat
               ,d.event
	       ,nvl(E.suppressed,'N')			       AS suppressed			
         FROM  --vic                      A                    INNER JOIN
               structools.st_equipment     b
--         ON    A.prnt_plnt_no = b.prnt_plnt_no
                                                             LEFT OUTER JOIN
               structools.st_hv_feeder        c
         ON    b.feeder_id = c.hv_fdr_id                                                                            
                                                             LEFT OUTER JOIN
               structools.st_pickid_committed    d
         ON    b.pick_id = d.pick_id       
         AND   b.part_cde = d.part_cde         		
																				LEFT OUTER JOIN
	       suppressed_eqp_bays			E
	 	ON b.pick_id = b.pick_id                                                             
         WHERE b.equip_grp_id NOT IN ('PAUS','PCON','PMET') 
         AND   b.part_cde = ' '
         UNION ALL
         SELECT b.pick_id
               ,'BAY'                        AS equip_grp_id
               ,'BAY'                        AS egi
               ,CASE                      --pickid init
                  WHEN d.commit_yr IS NOT NULL AND d.event = 'REPLACE'    THEN  d.commit_yr
                  ELSE                                                            b.instln_year
               END                              AS instln_year
               ,bay_len_m
               ,CASE
                   WHEN b.fire_risk_zone_cls = 'UNKNOWN'  THEN ' '
                   WHEN b.fire_risk_zone_cls IS NULL      THEN ' '
                   ELSE                                         b.fire_risk_zone_cls
                END                          AS fire_zone 
               ,' '                          AS wood_type
               ,' '                          AS dstr_size
               ,' '                          AS dstr_manuf
               ,NVL(UPPER(c.scnrrr_fdr_cat_nam), ' ')         AS fdr_cat
               ,d.event
	       ,NVL(E.suppressed,'N')				AS suppressed	
         FROM  structools.st_bay_equipment     b               LEFT OUTER JOIN
               structools.st_hv_feeder         c
         ON    b.feeder_id = c.hv_fdr_id
                                                             LEFT OUTER JOIN        --pickid init
               structools.st_pickid_committed    d
         ON    b.pick_id = d.pick_id                                                                            
																											LEFT OUTER JOIN
				suppressed_eqp_bays 			E
	   ON b.pick_id = E.pick_id
         )
         ,
         assets_year AS
         (
         SELECT A.*
               ,b.calc_year
         FROM   assets                        A             CROSS JOIN               
                years                         b
--         WHERE A.event IS NULL OR A.event NOT IN ('SUPP', 'PUP')
	 WHERE A.suppressed = 'N'
         )
         ,
         life_expectancy AS
         (
         SELECT DISTINCT parameter_egi
               ,NVL(parameter_wood_type, ' ')            AS parameter_wood_type
               ,NVL(parameter_dstr_size, ' ')            AS parameter_dstr_size
               ,NVL(parameter_dstr_manuf, ' ')           AS parameter_dstr_manuf
               ,NVL(parameter_pwod_reinf_ind, ' ')       AS parameter_pwod_reinf_ind
               ,NVL(parameter_pwod_pre1960_ind, ' ')     AS parameter_pwod_pre1960_ind
               ,parameter_val       AS life_exp
         FROM structools.st_parameter_scenario
         WHERE parameter_id = 'LIFE_EXPECTANCY'
         AND scenario_id = v_parm_scen_id_life_exp
         )
         ,
         assets2 AS
         (
         SELECT A.pick_id
               ,A.equip_grp_id
               ,A.egi
               ,A.fire_zone
               ,A.wood_type
               ,A.dstr_size
               ,A.dstr_manuf
               ,A.calc_year
               ,A.instln_year
               ,A.bay_len_m
               ,A.fdr_cat
               ,b.del_year
               ,b.event
               ,LAG(b.event IGNORE NULLS, 1, NULL) OVER (PARTITION BY A.pick_id ORDER BY A.pick_id, A.calc_year) AS event2
               ,LAG(b.del_year IGNORE NULLS, 1, NULL) OVER (PARTITION BY A.pick_id ORDER BY A.pick_id, A.calc_year) AS del_year2
         FROM assets_year        A  LEFT OUTER JOIN
              del_prog           b  --replaced_assets    b
         ON A.pick_id = b.pick_id
         AND A.calc_year = b.del_year
         )
         ,
         assets3 AS
         (
         SELECT A.pick_id
               ,A.equip_grp_id
                ,CASE
                  WHEN (A.del_year IS NOT NULL OR A.del_year2 IS NOT NULL) AND (A.event = 'REPLACE' OR A.event2 = 'REPLACE') AND egi = 'PWODHNY' THEN  'PWODHNN'
                  WHEN (A.del_year IS NOT NULL OR A.del_year2 IS NOT NULL) AND (A.event = 'REPLACE' OR A.event2 = 'REPLACE') AND egi = 'PWODHYY' THEN  'PWODHNN'
                  WHEN (A.del_year IS NOT NULL OR A.del_year2 IS NOT NULL) AND (A.event = 'REPLACE' OR A.event2 = 'REPLACE') AND egi = 'PWODHYN' THEN  'PWODHNN'
                  WHEN (A.del_year IS NOT NULL OR A.del_year2 IS NOT NULL) AND (A.event = 'REPLACE' OR A.event2 = 'REPLACE') AND egi = 'PWODHNN' THEN  'PWODHNN'  -- was good and still ok
                  WHEN (A.del_year IS NOT NULL OR A.del_year2 IS NOT NULL) AND (A.event = 'REINFORCE' OR A.event2 = 'REINFORCE') AND egi = 'PWODHNY' THEN 'PWODHYY'
                  WHEN (A.del_year IS NOT NULL OR A.del_year2 IS NOT NULL) AND (A.event = 'REINFORCE' OR A.event2 = 'REINFORCE') AND egi = 'PWODHYY' THEN 'PWODHYY' -- was good and still ok
                  WHEN (A.del_year IS NOT NULL OR A.del_year2 IS NOT NULL) AND (A.event = 'REINFORCE' OR A.event2 = 'REINFORCE') AND egi = 'PWODHYN' THEN 'PWODHYN' -- was good and still ok
                  WHEN (A.del_year IS NOT NULL OR A.del_year2 IS NOT NULL) AND (A.event = 'REINFORCE' OR A.event2 = 'REINFORCE') AND egi = 'PWODHNN' THEN 'PWODHYN'
                  ELSE                                                                                           A.egi 
                END                 AS egi  
                ,A.wood_type
                ,A.dstr_size
                ,A.dstr_manuf
                ,A.calc_year
                ,A.fire_zone
                ,A.instln_year
                ,A.bay_len_m
                ,A.fdr_cat
                ,CASE
                  WHEN A.del_year IS NOT NULL AND A.event = 'REPLACE'   THEN    A.calc_year - A.del_year
                  WHEN A.del_year2 IS NOT NULL AND A.event2 = 'REPLACE' THEN    A.calc_year - A.del_year2
                  ELSE                                 A.calc_year - A.instln_year
                END                       AS age
         FROM assets2   A
         )
         ,
         assets3a AS
         (
         SELECT A.pick_id
               ,A.equip_grp_id
               ,A.egi
                ,A.wood_type
                ,CASE
                  WHEN SUBSTR(egi,1,6) = 'PWODHY' THEN   'Y'
                  WHEN SUBSTR(egi,1,6) = 'PWODHN' THEN   'N'
                  ELSE                                   ' '
                END                    AS reinf_ind
                ,CASE
                  WHEN equip_grp_id = 'PWOD' AND SUBSTR(egi,7,1) = 'Y' THEN   'Y'
                  WHEN equip_grp_id = 'PWOD' AND SUBSTR(egi,7,1) = 'N' THEN   'N'
                  ELSE                                   ' '
                END                    AS pre1960_ind
                ,A.dstr_size
                ,A.dstr_manuf
                ,A.calc_year
                ,A.fire_zone
                ,A.instln_year
                ,A.bay_len_m
                ,A.fdr_cat
                ,A.age
         FROM assets3   A
         )
         ,
         assets4 AS
         (
         SELECT pick_id
               ,egi
               ,calc_year
               ,age
               ,bay_len_m
               ,ROUND((age/c.life_exp), 2) * 100 AS pcnt_des_lfe
               ,fire_zone
               ,fdr_cat
               ,dstr_manuf
         FROM assets3a               A INNER JOIN
              life_expectancy       c
         ON   A.equip_grp_id     = c.parameter_egi
         AND  A.wood_type        = c.parameter_wood_type         
         AND  A.reinf_ind        = c.parameter_pwod_reinf_ind
         AND  A.pre1960_ind      = c.parameter_pwod_pre1960_ind
         AND  A.dstr_size        = c.parameter_dstr_size
         AND  A.dstr_manuf       = c.parameter_dstr_manuf
         )
         ,
         assets5 AS
         (
         SELECT egi
               ,calc_year
               ,age
               ,pcnt_des_lfe
               ,fire_zone
               ,fdr_cat
               ,NVL(COUNT(*), 0)       AS cnt
         FROM assets4
         GROUP BY egi
                 ,calc_year
                 ,age
                 ,pcnt_des_lfe
                 ,fire_zone    
                 ,fdr_cat
         )
         ,
         bay_len   AS
         (
         SELECT calc_year
               ,age
               ,fire_zone
               ,fdr_cat
               ,SUM(bay_len_m)         AS bay_len_m
         FROM assets4
         WHERE egi = 'BAY'
         GROUP BY calc_year
               ,age
               ,fire_zone    
               ,fdr_cat
         )
--         ,
--         stats AS
--         (
         SELECT i_program_id
               ,i_base_run_id
               ,calc_year
               ,age
               ,fire_zone
               ,NULL                         AS pwod_h_cnt
               ,NULL                         AS pwod_h_pcnt_des_lfe
               ,NVL(pwod_s_cnt, 0) AS pwod_s_cnt
               ,NVL(pwod_s_pcnt_des_lfe, 0) AS pwod_s_pcnt_des_lfe
               ,NULL                         AS dstr_cnt
               ,NULL                         AS dstr_pcnt_des_lfe
               ,NVL(dof_cnt, 0)  AS dof_cnt
               ,NVL(dof_pcnt_des_lfe, 0) AS dof_pcnt_des_lfe
               ,NVL(ptsd_cnt, 0) AS ptsd_cnt
               ,NVL(ptsd_pcnt_des_lfe, 0) AS ptsd_pcnt_des_lfe
               ,NVL(recl_cnt, 0) AS recl_cnt
               ,NVL(recl_pcnt_des_lfe, 0) AS recl_pcnt_des_lfe
               ,NVL(sect_cnt, 0) AS sect_cnt
               ,NVL(sect_pcnt_des_lfe, 0) AS sect_pcnt_des_lfe
               ,NVL(lbs_cnt, 0)  AS lbs_cnt
               ,NVL(lbs_pcnt_des_lfe, 0) AS lbs_pcnt_des_lfe
               ,NVL(capb_cnt, 0) AS capb_cnt
               ,NVL(capb_pcnt_des_lfe, 0) AS capb_pcnt_des_lfe
               ,NVL(rgtr_cnt, 0) AS rgtr_cnt
               ,NVL(rgtr_pcnt_des_lfe, 0) AS rgtr_pcnt_des_lfe
               ,NVL(bay_cnt, 0)  AS bay_cnt
               ,NVL(bay_pcnt_des_lfe, 0) AS bay_pcnt_des_lfe
               ,NVL(reac_cnt, 0) AS reac_cnt
               ,NVL(reac_pcnt_des_lfe, 0) AS reac_pcnt_des_lfe
               ,fdr_cat
               ,NULL                      AS bay_len_m_dnu
               ,NVL(dstr_small_abb_cnt, 0) AS dstr_small_abb_cnt
               ,NVL(dstr_small_abb_pcnt_des_lfe, 0) AS dstr_small_abb_pcnt_des_lfe
               ,NVL(dstr_small_n_abb_cnt, 0) AS dstr_small_n_abb_cnt
               ,NVL(dstr_small_n_abb_pcnt_des_lfe, 0) AS dstr_small_n_abb_pcnt_des_lfe
               ,NVL(dstr_medium_abb_cnt, 0)              AS dstr_medium_abb_cnt
               ,NVL(dstr_medium_abb_pcnt_des_lfe, 0)    AS dstr_medium_abb_pcnt_des_lfe
               ,NVL(dstr_medium_n_abb_cnt, 0)            AS dstr_medium_n_abb_cnt
               ,NVL(dstr_medium_n_abb_pcnt_des_lfe, 0)  AS dstr_medium_n_abb_pcnt_des_lfe
               ,NVL(dstr_large_abb_cnt, 0)              AS dstr_large_abb_cnt
               ,NVL(dstr_large_abb_pcnt_des_lfe, 0)    AS dstr_large_abb_pcnt_des_lfe
               ,NVL(dstr_large_n_abb_cnt, 0)            AS dstr_large_n_abb_cnt
               ,NVL(dstr_large_n_abb_pcnt_des_lfe, 0)  AS dstr_large_n_abb_pcnt_des_lfe
               ,NVL(pwod_h_y_y_cnt, 0)                AS pwod_h_r_pre60_cnt
               ,NVL(pwod_h_y_y_pcnt_des_lfe, 0)       AS pwod_h_r_pre60_pcnt_des_lfe
               ,NVL(pwod_h_y_n_cnt, 0)                AS pwod_h_r_post60_cnt
               ,NVL(pwod_h_y_n_pcnt_des_lfe, 0)       AS pwod_h_r_post60_pcnt_des_lfe
               ,NVL(pwod_h_n_y_cnt, 0)                AS pwod_h_nr_pre60_cnt
               ,NVL(pwod_h_n_y_pcnt_des_lfe, 0)       AS pwod_h_nr_pre60_pcnt_des_lfe
               ,NVL(pwod_h_n_n_cnt, 0)                AS pwod_h_nr_post60_cnt
               ,NVL(pwod_h_n_n_pcnt_des_lfe, 0)       AS pwod_h_nr_post60_pcnt_des_lfe
               ,bay_len_m
               ,NULL                      AS hvxm_cnt
               ,NULL                      AS hvxm_pcnt_des_lfe
               ,NULL                      AS lvxm_cnt
               ,NULL                      AS lvxm_pcnt_des_lfe
               ,NULL                      AS astay_cnt
               ,NULL                      AS astay_pcnt_des_lfe
               ,NULL                      AS gstay_cnt
               ,NULL                      AS gstay_pcnt_des_lfe
               ,NULL                      AS ostay_cnt
               ,NULL                      AS ostay_pcnt_des_lfe
               ,NULL                      AS agost_cnt
               ,NULL                      AS agost_pcnt_des_lfe
         FROM 
         (
         SELECT A.calc_year
               ,A.age
               ,A.fire_zone
               ,A.cnt
               ,A.egi
               ,A.pcnt_des_lfe
               ,A.fdr_cat
               ,NVL(b.bay_len_m, 0)   AS bay_len_m
         FROM  assets5        A                   LEFT OUTER JOIN 
               bay_len        b  
         ON    A.calc_year = b.calc_year
         AND   A.age         = b.age
         AND   A.fire_zone = b.fire_zone
         AND   A.fdr_cat   = b.fdr_cat              
         )
         PIVOT (MAX(cnt) AS cnt,
               MAX(pcnt_des_lfe) AS pcnt_des_lfe FOR egi IN ('PWODHYY' pwod_h_y_y, 'PWODHYN' pwod_h_y_n, 'PWODHNY' pwod_h_n_y, 'PWODHNN' pwod_h_n_n
                                                   , 'PWODS' pwod_s
                                                   , 'DSTRSMALL ABB' dstr_small_abb, 'DSTRSMALL  ' dstr_small_n_abb
                                                   , 'DSTRMEDIUMABB' dstr_medium_abb, 'DSTRMEDIUM ' dstr_medium_n_abb
                                                   , 'DSTRLARGE ABB' dstr_large_abb, 'DSTRLARGE  ' dstr_large_n_abb
                                                   , 'DOF' dof, 'PTSD' ptsd, 'RECL' recl
                                                   , 'SECT' sect, 'LBS' lbs, 'CAPB' capb, 'RGTR' rgtr, 'BAY' bay, 'REAC' reac ))
         ORDER BY calc_year, age, fire_zone, fdr_cat
         ;
      END IF;
      
      COMMIT;
      
      dbms_output.put_line('generate_age_stats END '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));
      
   EXCEPTION
      WHEN v_prog_excp THEN
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END generate_age_stats;        


   PROCEDURE generate_bays_seg_delta(i_base_run_id IN PLS_INTEGER
                                    ,i_program_id  IN PLS_INTEGER
                                    ,i_delete_flg  IN VARCHAR2    DEFAULT 'N') IS
      
      v_prog_excp             EXCEPTION;
      v_run_years             PLS_INTEGER;
      v_first_year            PLS_INTEGER;
      v_existing_record_cnt PLS_INTEGER;

      CURSOR no_rb_targeted_bays_csr (i_yr IN PLS_INTEGER) IS
         WITH del_prog_mtzn AS
         (
         SELECT                  
         	b.mtzn
           ,A.del_year
         FROM structools.st_del_program_mtzn_optimised		A	INNER JOIN
         			structools.st_mtzn														b
			ON A.mtzn = b.mtzn_opt                       
         WHERE program_id = i_program_id 
         AND   base_run_id = i_base_run_id  
         AND   del_year > 0 AND del_year < i_yr 
         )
         ,                           
         del_prog_seg AS
         (
         SELECT cons_segment_id                  
           ,del_year
         FROM structools.st_del_program_seg_optimised     
         WHERE program_id = i_program_id 
         AND   base_run_id = i_base_run_id  
         AND   del_year > 0 AND del_year < i_yr 
         )     
         ,
         del_prog_bay_pickids AS
         (
         SELECT A.pick_id
               ,i_yr          AS prior_to_cal_yr 
         FROM structools.st_temp_bay_strategy_run          A    INNER JOIN
              del_prog_mtzn b
         ON A.mtzn_grp = B.MTZN
         AND A.calendar_year = B.DEL_YEAR
         WHERE A.event > ' ' 
         AND A.calendar_year < i_yr 
         --AND A.scheduled = 'N'          --pickid init
         AND segment_rebuild_yr IS NOT NULL
         AND A.pickid_suppressed = 'N'
         UNION
         SELECT A.pick_id
               ,i_yr          AS prior_to_cal_yr 
         FROM structools.st_temp_bay_strategy_run          A    INNER JOIN
              del_prog_seg b
         ON A.cons_segment_id = B.cons_segment_id
         AND A.calendar_year = B.DEL_YEAR
         WHERE A.event > ' ' 
         AND A.calendar_year < i_yr
         AND A.pickid_suppressed = 'N' 
         --AND A.scheduled = 'N'          --pickid init
--         AND segment_rebuild_yr IS NOT NULL
         )
         SELECT i_base_run_id
               ,i_program_id
               ,span_id 
               ,pick_id
               ,cond_eqp_typ
               ,mtzn
               ,age
               ,calendar_year
               ,event
               ,event_reason
               ,capex
               ,risk_reduction
               ,risk_reduction_npv15
               --,risk_reduction_npv15_sif
               ,rr_npv15_sif_fdr_cat_adj
               ,cons_segment_id
               ,segment_rebuild_yr 
         FROM
         (
         SELECT i_base_run_id             
               ,i_program_id              
               ,A.vic_id                  AS span_id 
               ,A.pick_id
               ,A.cond_eqp_typ
               ,A.mtzn_grp                AS mtzn
               ,A.age
               ,A.calendar_year
               ,A.event
               ,A.event_reason
               ,A.capex
               ,A.risk_reduction
               ,A.risk_reduction_npv15
               --,A.risk_reduction_npv15_sif
               ,A.rr_npv15_sif_fdr_cat_adj
               ,A.cons_segment_id
               ,A.segment_rebuild_yr
               ,b.pick_id                 AS del_pick_id
         FROM structools.st_temp_bay_strategy_run   A                   LEFT OUTER JOIN
              del_prog_bay_pickids        b
         ON A.pick_id = b.pick_id
         AND A.calendar_year = b.prior_to_cal_yr           
         WHERE event > ' '
         AND A.calendar_year = i_yr 
         --AND A.scheduled = 'N'       --pickid init
         AND segment_rebuild_yr IS NULL
         AND A.pickid_suppressed = 'N'
         ) aa
         WHERE aa.del_pick_id IS NULL
      ;
      
      TYPE no_rb_targeted_bays_tab_typ    IS TABLE OF no_rb_targeted_bays_csr%ROWTYPE;
      v_no_rb_targeted_bays_tab           no_rb_targeted_bays_tab_typ := no_rb_targeted_bays_tab_typ();

      CURSOR rb_not_targeted_bays_csr IS
         /* get null-event bays that belong to segment selected for a rebuild and included in a del program */
         WITH rb_null_event AS
         (
         SELECT DISTINCT pick_id
               ,cond_eqp_typ
               ,vic_id        AS span_id
               ,mtzn_grp      AS mtzn
               ,age
               ,cons_segment_id
               ,segment_rebuild_yr
         FROM structools.st_temp_bay_strategy_run      
         WHERE segment_rebuild_yr IS NOT NULL
         AND calendar_year = segment_rebuild_yr
         AND event IS NULL
         AND pickid_suppressed = 'N'
         )
         ,
         del_segs AS
         (
         SELECT A.cons_segment_id
               ,MIN(A.del_year) AS min_del_yr
         FROM structools.st_del_program_pickid A               
         WHERE A.program_id = i_program_id
         AND equip_grp_id = 'BAY'
         AND A.base_run_id = i_base_run_id
         GROUP BY A.cons_segment_id
         )
         SELECT i_base_run_id
               ,i_program_id 
               ,b.span_id
               ,b.pick_id
               ,b.cond_eqp_typ
               ,b.mtzn
               ,b.age
               ,A.min_del_yr
               ,NULL       --event
               ,NULL       --event_reason
               ,0          --capex
               ,0          --risk_reduction
               ,0          --risk_reduction_npr
               ,0          --risk_reduction_npr_sif
               ,A.cons_segment_id
               ,b.segment_rebuild_yr
         FROM del_segs      A  INNER JOIN
              rb_null_event b
         ON A.cons_segment_id = b.cons_segment_id             
         ORDER BY cons_segment_id, pick_id
      ;

      TYPE rb_not_targeted_bays_tab_typ    IS TABLE OF rb_not_targeted_bays_csr%ROWTYPE;
      v_rb_not_targeted_bays_tab           rb_not_targeted_bays_tab_typ := rb_not_targeted_bays_tab_typ();

   BEGIN
   
      IF i_program_id IS NULL THEN
         SELECT COUNT(*)
         INTO   v_existing_record_cnt
         FROM   structools.ST_DEL_PROGRAM_seg_delta
         WHERE  program_id IS NULL
         AND    base_run_id = i_base_run_id;
      ELSE
         SELECT COUNT(*)
         INTO   v_existing_record_cnt
         FROM   structools.ST_DEL_PROGRAM_seg_delta
         WHERE  program_id = i_program_id
         AND    base_run_id = i_base_run_id;
      END IF;

      IF i_delete_flg = 'Y' THEN
         dbms_output.put_line('Deleting existing results');
         
         IF i_program_id IS NULL THEN
            DELETE FROM structools.ST_DEL_PROGRAM_seg_delta
            WHERE program_id IS NULL
            AND   base_run_id = i_base_run_id;
         ELSE
            DELETE FROM structools.ST_DEL_PROGRAM_seg_delta
            WHERE program_id = i_program_id
            AND   base_run_id = i_base_run_id;
         END IF;
      ELSE
         dbms_output.put_line('Existing results NOT deleted');
         
         IF v_existing_record_cnt > 0 THEN
            dbms_output.put_line('EXCEPTION Existing results NOT deleted');
            RAISE v_prog_excp;
         END IF;
      END IF;

      SELECT first_year
            ,run_years
      INTO   v_first_year
            ,v_run_years
      FROM structools.st_run_request
      WHERE run_id = i_base_run_id;

      IF v_first_year > v_this_year + 1 THEN
         v_year_0 := v_first_year - 1;
      ELSE
         v_year_0 := v_this_year;
      END IF;

      FOR i IN v_year_0 + 1 .. (v_year_0 + v_run_years) LOOP
         v_no_rb_targeted_bays_tab.DELETE;
         OPEN  no_rb_targeted_bays_csr(i);
         
         FETCH no_rb_targeted_bays_csr BULK COLLECT INTO v_no_rb_targeted_bays_tab;
         FORALL j IN 1 .. v_no_rb_targeted_bays_tab.COUNT 
            INSERT INTO structools.st_del_program_seg_delta VALUES v_no_rb_targeted_bays_tab(j)
         ;
         CLOSE no_rb_targeted_bays_csr;
      END LOOP;

      OPEN  rb_not_targeted_bays_csr;
         
      FETCH rb_not_targeted_bays_csr BULK COLLECT INTO v_rb_not_targeted_bays_tab;
      FORALL j IN 1 .. v_rb_not_targeted_bays_tab.COUNT 
         INSERT INTO structools.st_del_program_seg_delta VALUES v_rb_not_targeted_bays_tab(j)
      ;
      CLOSE rb_not_targeted_bays_csr;
      
      COMMIT;
      
   EXCEPTION
      WHEN v_prog_excp THEN
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
   END generate_bays_seg_delta;

   PROCEDURE calc_st_seg_maint_yr_cost(i_yr_maint_parm_scenario_id IN PLS_INTEGER DEFAULT 1
                                      ,i_segment_type IN PLS_INTEGER 
                                       ) IS
   
      v_apf_pwod_yr           NUMBER;
      v_fault_pwod_yr         NUMBER;
      
      v_k4kz_pwod_maint_yr    NUMBER;
      v_k3n9_pwod_maint_yr    NUMBER;
      v_k2ke_pwod_maint_yr    NUMBER;
      v_k2kk_pwod_maint_yr    NUMBER;
      
      v_k4kz_bay_maint_yr     NUMBER;
      v_k3ks_bay_maint_yr     NUMBER;
      v_k3n8_bay_maint_yr     NUMBER;
      v_k2ke_bay_maint_yr     NUMBER;
      v_k4kz_bay_fail_yr      NUMBER;
      
      v_k2ke_dstr_maint_yr    NUMBER;
      v_k2kk_dstr_maint_yr    NUMBER;
      v_k2kj_dstr_maint_yr    NUMBER;
      v_k2kh_dstr_maint_yr    NUMBER;
      v_k2ki_dstr_maint_yr    NUMBER;
      
      v_sql                            VARCHAR2(1000);
      
      
   BEGIN

      /* Get the yearly maintenance cost for varous maintenance types, for the whole network */
      SELECT parameter_val
      INTO   v_k2ke_bay_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'BAY'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K2KE_MAINT'
      ;
      SELECT parameter_val
      INTO   v_k3n8_bay_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'BAY'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K3N8_MAINT'
      ;
      SELECT parameter_val
      INTO   v_k3ks_bay_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'BAY'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K3KS_MAINT'
      ;
      SELECT parameter_val
      INTO   v_k4kz_bay_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'BAY'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K4KZ_MAINT'
      ;
      SELECT parameter_val
      INTO   v_k4kz_bay_fail_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'BAY'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K4KZ_FAIL'
      ;
      SELECT parameter_val
      INTO   v_k2kh_dstr_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'DSTR'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K2KH_MAINT'
      ;
      SELECT parameter_val
      INTO   v_k2ki_dstr_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'DSTR'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K2KI_MAINT'
      ;
      SELECT parameter_val
      INTO   v_k2kj_dstr_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'DSTR'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K2KJ_MAINT'
      ;
      SELECT parameter_val
      INTO   v_k2kk_dstr_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'DSTR'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K2KK_MAINT'
      ;
      SELECT parameter_val
      INTO   v_k2ke_dstr_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'DSTR'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K2KE_MAINT'
      ;
      SELECT parameter_val
      INTO   v_apf_pwod_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'PWOD'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'APF'
      ;
      SELECT parameter_val
      INTO   v_k4kz_pwod_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'PWOD'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K4KZ_MAINT'
      ;
      SELECT parameter_val
      INTO   v_fault_pwod_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'PWOD'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'FAULT'
      ;
      SELECT parameter_val
      INTO   v_k3n9_pwod_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'PWOD'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K3N9_MAINT'
      ;
      SELECT parameter_val
      INTO   v_k2ke_pwod_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'PWOD'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K2KE_MAINT'
      ;
      SELECT parameter_val
      INTO   v_k2kk_pwod_maint_yr
      FROM structools.st_parameter_scenario
      WHERE parameter_id = 'YR_MAINT_COST'
      AND   parameter_egi = 'PWOD'
      AND   scenario_id = i_yr_maint_parm_scenario_id
      AND   parameter_maint_cost_type = 'K2KK_MAINT'
      ;
      
      structools.schema_utils.truncate_table('ST_SEG_MAINT_YR_COST');

      INSERT /*+ APPEND */ INTO structools.st_seg_maint_yr_cost
      WITH all_bays_cnt AS
      (
      SELECT COUNT(*)   AS bays_cnt
      FROM structools.st_bay_equipment
      )
      ,
      lv_seg_bays_cnt_tmp AS
      (
      SELECT ntwk_id
            ,COUNT(*) AS seg_bay_cnt
      FROM structools.st_bay_equipment
      WHERE swsect_net_typ = 'LV'
      GROUP BY ntwk_id
      )
      ,
      lv_seg_bays_cnt AS
      (
      SELECT 'N'||A.net_nid     AS segment_identifier_id
            ,'LV'               AS ntwk_type
            ,NVL(seg_bay_cnt, 0)  AS seg_bay_cnt
      FROM   structoolsw.ntwk    A   LEFT OUTER JOIN
             lv_seg_bays_cnt_tmp    b
      ON A.net_nid = b.ntwk_id
      WHERE A.net_typ = 'LV'
      )
      ,
      hv_seg_bays_cnt_tmp AS
      (   
      SELECT A.segment_id
            ,COUNT(*) AS hv_seg_bay_cnt
      FROM structools.nss_segment         A LEFT OUTER JOIN
           structools.nss_segment_asset   b
      ON A.segment_id = b.segment_id        
      WHERE pick_id LIKE 'B%'
      AND A.segment_type_id = i_segment_type
      AND A.end_dt          = v_null_end_dt
      GROUP BY A.segment_id  -- output 0 for segmnets with no bays ?
      )             
      ,
      hv_seg_bays_cnt AS
      (   
      SELECT A.segment_identifier_id
            ,'HV'          AS ntwk_type
            ,NVL(b.hv_seg_bay_cnt, 0)  AS seg_bay_cnt
      FROM   structools.nss_segment    A   LEFT OUTER JOIN
             hv_seg_bays_cnt_tmp    b
      ON A.segment_id = b.segment_id
      WHERE A.segment_type_id = i_segment_type
      AND A.end_dt            = v_null_end_dt
      )
      ,
      seg_bays_cnt AS
      (
      SELECT *
      FROM lv_seg_bays_cnt
      UNION ALL
      SELECT *
      FROM hv_seg_bays_cnt
      )
      ,
      all_segments AS
      (
      SELECT *
      FROM seg_bays_cnt    CROSS JOIN
           all_bays_cnt
      )
      ,
      PWOD_seg AS
      (
      SELECT * FROM
      (
      SELECT segment_identifier_id AS segment_id
            ,ntwk_type
            ,seg_bay_cnt
            ,bays_cnt
            ,(seg_bay_cnt / bays_cnt)  AS seg_bays_pcnt
            ,'PWOD'                 AS EGI
            ,(seg_bay_cnt / bays_cnt) * v_apf_pwod_yr          AS pwod_apf_yr       
            ,(seg_bay_cnt / bays_cnt) * v_fault_pwod_yr        AS pwod_fault_yr     
            ,(seg_bay_cnt / bays_cnt) * v_k4kz_pwod_maint_yr   AS k4kz_pwod_maint_yr  
            ,(seg_bay_cnt / bays_cnt) * v_k3n9_pwod_maint_yr   AS k3n9_pwod_maint_yr    
            ,(seg_bay_cnt / bays_cnt) * v_k2ke_pwod_maint_yr   AS k2ke_pwod_maint_yr
            ,(seg_bay_cnt / bays_cnt) * v_k2kk_pwod_maint_yr   AS k2kk_pwod_maint_yr
      FROM all_segments
      )  
      UNPIVOT (maint_val FOR maint_type IN (pwod_apf_yr        AS 'APF' 
                                          , pwod_fault_yr      AS 'FAULT'
                                          , k4kz_pwod_maint_yr AS 'K4KZ_MAINT'
                                          , k3n9_pwod_maint_yr AS 'K3N9_MAINT'
                                          , k2ke_pwod_maint_yr AS 'K2KE_MAINT'
                                          , k2kk_pwod_maint_yr AS 'K2KK_MAINT'
                                          ))
      )
      ,
      BAY_SEG AS
      (                                       
      SELECT * FROM
      (
      SELECT segment_identifier_id AS segment_id
            ,ntwk_type
            ,seg_bay_cnt
            ,bays_cnt
            ,(seg_bay_cnt / bays_cnt)  AS seg_bays_pcnt
            ,'BAY'                 AS EGI
            ,(seg_bay_cnt / bays_cnt) * v_k4kz_bay_fail_yr     AS k4kz_bay_fail_yr
            ,(seg_bay_cnt / bays_cnt) * v_k4kz_bay_maint_yr    AS k4kz_bay_maint_yr
            ,(seg_bay_cnt / bays_cnt) * v_k3ks_bay_maint_yr    AS k3ks_bay_maint_yr
            ,(seg_bay_cnt / bays_cnt) * v_k3n8_bay_maint_yr    AS k3n8_bay_maint_yr
            ,(seg_bay_cnt / bays_cnt) * v_k2ke_bay_maint_yr    AS k2ke_bay_maint_yr
      FROM all_segments
      )
      UNPIVOT (maint_val FOR maint_type IN (k4kz_bay_fail_yr   AS 'K4KZ_FAIL' 
                                          , k4kz_bay_maint_yr  AS 'K4KZ_MAINT'
                                          , k3ks_bay_maint_yr  AS 'K3KS_MAINT'
                                          , k3n8_bay_maint_yr  AS 'K3N8_MAINT'
                                          , k2ke_bay_maint_yr  AS 'K2KE_MAINT'
                                          ))
      )
      ,
      DSTR_seg AS
      (                                       
      SELECT * FROM
      (
      SELECT segment_identifier_id AS segment_id
            ,ntwk_type
            ,seg_bay_cnt
            ,bays_cnt
            ,(seg_bay_cnt / bays_cnt)  AS seg_bays_pcnt
            ,'DSTR'                 AS EGI
            ,(seg_bay_cnt / bays_cnt) * v_k2ke_dstr_maint_yr      AS k2ke_dstr_maint_yr
            ,(seg_bay_cnt / bays_cnt) * v_k2kk_dstr_maint_yr      AS k2kk_dstr_maint_yr
            ,(seg_bay_cnt / bays_cnt) * v_k2kj_dstr_maint_yr      AS k2kj_dstr_maint_yr
            ,(seg_bay_cnt / bays_cnt) * v_k2kh_dstr_maint_yr      AS k2kh_dstr_maint_yr
            ,(seg_bay_cnt / bays_cnt) * v_k2ki_dstr_maint_yr      AS k2ki_dstr_maint_yr
      FROM all_segments
      )
      UNPIVOT (maint_val FOR maint_type IN (k2ke_dstr_maint_yr  AS 'K2KE_MAINT' 
                                          , k2kk_dstr_maint_yr  AS 'K2KK_MAINT'
                                          , k2kj_dstr_maint_yr  AS 'K2KJ_MAINT'
                                          , k2kh_dstr_maint_yr  AS 'K2KH_MAINT'
                                          , k2ki_dstr_maint_yr  AS 'K2KI_MAINT'
                                          ))
      )
      SELECT segment_id
            ,egi
            ,maint_type
            ,maint_val
      FROM PWOD_seg
      UNION ALL
      SELECT segment_id
            ,egi
            ,maint_type
            ,maint_val
      FROM BAY_seg
      UNION ALL
      SELECT segment_id
            ,egi
            ,maint_type
            ,maint_val
      FROM DSTR_seg
      ;
      
      COMMIT;

   END calc_st_seg_maint_yr_cost;

   PROCEDURE generate_user_prog_risk_prof(i_user_id  IN VARCHAR2
                                           ,i_user_version   IN PLS_INTEGER
                                           ,i_years        IN PLS_INTEGER
                                           ,i_delete_flg   IN VARCHAR2       DEFAULT 'N') IS

	/* Variables and cursors for procedure generate_user_prog_risk_prof */
   v_cnt										PLS_INTEGER;
   v_table_name					VARCHAR2(40);
   v_column_name					VARCHAR2(40);
   v_sql										VARCHAR2(1000);
   v_dummy              CHAR(1);
   
   CURSOR actioned_pwod_eqp_csr IS
      SELECT    pick_id
               ,equip_grp_id
               ,del_year
               ,event
      FROM structools.st_user_program_pickid
      WHERE user_id = i_user_id AND user_version = i_user_version
      AND equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
      ORDER BY pick_id, del_year
   ;
         
   CURSOR   risk_pwod_eqp_csr IS

      WITH risk_pwod_eqp_noaction AS
      (
      SELECT  'S'||A.pick_id        AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_pwod_NOACTION  A
      UNION ALL
      SELECT   'N'||A.pick_id       AS pick_id_typ
              ,A.*
      FROM structools.st_risk_dstr_NOACTION  A
      UNION ALL
      SELECT 'N'||A.pick_id       AS pick_id_typ
             ,A.* 
      FROM structools.st_risk_ptsd_NOACTION  A
      UNION ALL
      SELECT 'N'||A.pick_id       AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_sect_NOACTION  A
      )
      ,
      risk_pwod_eqp_noaction_npv15 AS
      (
      SELECT  'S'||A.pick_id        AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_pwod_NOACTION_npv15  A
      UNION ALL 
      SELECT   'N'||A.pick_id       AS pick_id_typ
              ,A.*
      FROM structools.st_risk_dstr_NOACTION_npv15  A
      UNION ALL 
      SELECT 'N'||A.pick_id       AS pick_id_typ
             ,A.* 
      FROM structools.st_risk_ptsd_NOACTION_npv15  A
      UNION ALL 
      SELECT 'N'||A.pick_id       AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_sect_NOACTION_npv15  A
      )
      ,
      risk_pwod_eqp_replace AS
      (
      SELECT  'S'||A.pick_id        AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_pwod_REPLACE  A
      UNION ALL 
      SELECT   'N'||A.pick_id       AS pick_id_typ
              ,A.*
      FROM structools.st_risk_dstr_REPLACE  A
      UNION ALL 
      SELECT 'N'||A.pick_id       AS pick_id_typ
             ,A.* 
      FROM structools.st_risk_ptsd_REPLACE  A
      UNION ALL 
      SELECT 'N'||A.pick_id       AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_sect_REPLACE  A
      )
      ,
      risk_pwod_eqp_replace_npv15 AS
      (
      SELECT  'S'||A.pick_id        AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_pwod_REPLACE_npv15  A
      UNION ALL 
      SELECT   'N'||A.pick_id       AS pick_id_typ
              ,A.*
      FROM structools.st_risk_dstr_REPLACE_npv15  A
      UNION ALL 
      SELECT 'N'||A.pick_id       AS pick_id_typ
             ,A.* 
      FROM structools.st_risk_ptsd_REPLACE_npv15  A
      UNION ALL
      SELECT 'N'||A.pick_id       AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_sect_REPLACE_npv15  A
      )
      ,
      risk_pwod_reinforce AS
      (
      SELECT  'S'||A.pick_id        AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_pwod_REINF  A
      )
      ,
      risk_pwod_reinforce_npv15 AS
      (
      SELECT  'S'||A.pick_id        AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_pwod_REINF_npv15  A
      )
      ,
      suppressed_eqp AS
      (
      SELECT pick_id
             ,'Y'					AS suppressed
      FROM structools.st_pickid_suppressed_init
      WHERE equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
--      UNION
--      SELECT pick_id
--            ,'P'						AS suppressed				
--      FROM structools.st_pickid_suppressed
--      WHERE equip_grp_id IN ('PWOD','DSTR','PTSD','SECT')
--      AND scenario_id = v_suppress_pickids_ver
      )
      ,
      pickid_pwod_eqp AS
      (
      SELECT A.pick_id
                 ,A.equip_grp_id
		          ,NVL(b.suppressed,'N')		AS suppressed
      FROM structools.st_equipment	A			LEFT OUTER JOIN
	   suppressed_eqp	b
      ON A.pick_id = b.pick_id	
      WHERE A.equip_grp_id IN  ('PWOD','DSTR','PTSD','SECT')
      AND part_cde = ' '
      )
      ,
      risk AS
      (
      SELECT A.pick_id
            ,A.equip_grp_id
               ,c.risk_indx_val_Y0  AS risk_na_y0
               ,c.risk_indx_val_Y1  AS risk_na_y1
               ,c.risk_indx_val_Y2  AS risk_na_y2
               ,c.risk_indx_val_y3  AS risk_na_y3
               ,c.risk_indx_val_y4  AS risk_na_y4
               ,c.risk_indx_val_y5  AS risk_na_y5
               ,c.risk_indx_val_y6  AS risk_na_y6
               ,c.risk_indx_val_y7  AS risk_na_y7
               ,c.risk_indx_val_y8  AS risk_na_y8
               ,c.risk_indx_val_y9  AS risk_na_y9
               ,c.risk_indx_val_y10  AS risk_na_y10      /* 10 years + 0-year plus 2 years to allow to past replaces (via pickid_committed list) */
               ,c.risk_indx_val_y11  AS risk_na_y11      /* If the run is for more than 10 years, this needs to be changed to extract more risk-years */
               ,c.risk_indx_val_y12  AS risk_na_y12
               ,d.risk_indx_val_Y0  AS risk_npv15_na_y0
               ,d.risk_indx_val_Y1  AS risk_npv15_na_y1
               ,d.risk_indx_val_Y2  AS risk_npv15_na_y2
               ,d.risk_indx_val_y3  AS risk_npv15_na_y3
               ,d.risk_indx_val_y4  AS risk_npv15_na_y4
               ,d.risk_indx_val_y5  AS risk_npv15_na_y5
               ,d.risk_indx_val_y6  AS risk_npv15_na_y6
               ,d.risk_indx_val_y7  AS risk_npv15_na_y7
               ,d.risk_indx_val_y8  AS risk_npv15_na_y8
               ,d.risk_indx_val_y9  AS risk_npv15_na_y9
               ,d.risk_indx_val_y10  AS risk_npv15_na_y10
               ,d.risk_indx_val_y11  AS risk_npv15_na_y11
               ,d.risk_indx_val_y12  AS risk_npv15_na_y12
               ,E.risk_indx_val_Y0  AS risk_repl_y0
               ,E.risk_indx_val_Y1  AS risk_repl_y1
               ,E.risk_indx_val_Y2  AS risk_repl_y2
               ,E.risk_indx_val_y3  AS risk_repl_y3
               ,E.risk_indx_val_y4  AS risk_repl_y4
               ,E.risk_indx_val_y5  AS risk_repl_y5
               ,E.risk_indx_val_y6  AS risk_repl_y6
               ,E.risk_indx_val_y7  AS risk_repl_y7
               ,E.risk_indx_val_y8  AS risk_repl_y8
               ,E.risk_indx_val_y9  AS risk_repl_y9
               ,E.risk_indx_val_y10  AS risk_repl_y10
               ,E.risk_indx_val_y11  AS risk_repl_y11
               ,E.risk_indx_val_y12  AS risk_repl_y12
               ,f.risk_indx_val_Y0  AS risk_npv15_repl_y0
               ,f.risk_indx_val_Y1  AS risk_npv15_repl_y1
               ,f.risk_indx_val_Y2  AS risk_npv15_repl_y2
               ,f.risk_indx_val_y3  AS risk_npv15_repl_y3
               ,f.risk_indx_val_y4  AS risk_npv15_repl_y4
               ,f.risk_indx_val_y5  AS risk_npv15_repl_y5
               ,f.risk_indx_val_y6  AS risk_npv15_repl_y6
               ,f.risk_indx_val_y7  AS risk_npv15_repl_y7
               ,f.risk_indx_val_y8  AS risk_npv15_repl_y8
               ,f.risk_indx_val_y9  AS risk_npv15_repl_y9
               ,f.risk_indx_val_y10  AS risk_npv15_repl_y10
               ,f.risk_indx_val_y11  AS risk_npv15_repl_y11
               ,f.risk_indx_val_y12  AS risk_npv15_repl_y12
               ,G.risk_indx_val_Y0  AS risk_reinf_y0
               ,G.risk_indx_val_Y1  AS risk_reinf_y1
               ,G.risk_indx_val_Y2  AS risk_reinf_y2
               ,G.risk_indx_val_y3  AS risk_reinf_y3
               ,G.risk_indx_val_y4  AS risk_reinf_y4
               ,G.risk_indx_val_y5  AS risk_reinf_y5
               ,G.risk_indx_val_y6  AS risk_reinf_y6
               ,G.risk_indx_val_y7  AS risk_reinf_y7
               ,G.risk_indx_val_y8  AS risk_reinf_y8
               ,G.risk_indx_val_y9  AS risk_reinf_y9
               ,G.risk_indx_val_y10  AS risk_reinf_y10
               ,G.risk_indx_val_y11  AS risk_reinf_y11
               ,G.risk_indx_val_y12  AS risk_reinf_y12
               ,h.risk_indx_val_Y0  AS risk_npv15_reinf_y0
               ,h.risk_indx_val_Y1  AS risk_npv15_reinf_y1
               ,h.risk_indx_val_Y2  AS risk_npv15_reinf_y2
               ,h.risk_indx_val_y3  AS risk_npv15_reinf_y3
               ,h.risk_indx_val_y4  AS risk_npv15_reinf_y4
               ,h.risk_indx_val_y5  AS risk_npv15_reinf_y5
               ,h.risk_indx_val_y6  AS risk_npv15_reinf_y6
               ,h.risk_indx_val_y7  AS risk_npv15_reinf_y7
               ,h.risk_indx_val_y8  AS risk_npv15_reinf_y8
               ,h.risk_indx_val_y9  AS risk_npv15_reinf_y9
               ,h.risk_indx_val_y10  AS risk_npv15_reinf_y10
               ,h.risk_indx_val_y11  AS risk_npv15_reinf_y11
               ,h.risk_indx_val_y12  AS risk_npv15_reinf_y12
      FROM  pickid_pwod_eqp                A  INNER JOIN
            risk_pwod_eqp_noaction c                                          
      ON A.pick_id = c.pick_id_typ
                                    INNER JOIN
            risk_pwod_eqp_noaction_npv15 d                                          
      ON A.pick_id = d.pick_id_typ
                                    INNER JOIN
            risk_pwod_eqp_replace E                                          
      ON A.pick_id = E.pick_id_typ
                                    INNER JOIN
            risk_pwod_eqp_replace_npv15 f                                          
      ON A.pick_id = f.pick_id_typ
                                    LEFT OUTER JOIN
            risk_pwod_reinforce G                                          
      ON A.pick_id = G.pick_id_typ
                                    LEFT OUTER JOIN
            risk_pwod_reinforce_npv15 h                                          
      ON A.pick_id = h.pick_id_typ
      WHERE A.suppressed = 'N'
      )
      SELECT /*+ parallel (risk,8) */
             pick_id
            ,equip_grp_id
            ,v_this_year + yr AS calendar_year
            ,risk_na
            ,risk_npv15_na
            ,risk_repl
            ,risk_npv15_repl
            ,risk_reinf       AS risk_reinf_repair             /* renamed so that resultant columns are identical between the cursor for polles+eqp */
            ,risk_npv15_reinf AS risk_npv15_reinf_repair       /* and bays, and have a single table based on the cursor definition. */
      FROM  risk
      UNPIVOT ((risk_na, risk_npv15_na, risk_repl, risk_npv15_repl, risk_reinf, risk_npv15_reinf) 
                                    FOR yr IN 
                                    ((risk_na_y0,risk_npv15_na_y0,risk_repl_y0,risk_npv15_repl_y0,risk_reinf_y0,risk_npv15_reinf_y0) AS '00'
                                    ,(risk_na_y1,risk_npv15_na_y1,risk_repl_y1,risk_npv15_repl_y1,risk_reinf_y1,risk_npv15_reinf_y1) AS '01'
                                    ,(risk_na_y2,risk_npv15_na_y2,risk_repl_y2,risk_npv15_repl_y2,risk_reinf_y2,risk_npv15_reinf_y2) AS '02'
                                    ,(risk_na_y3,risk_npv15_na_y3,risk_repl_y3,risk_npv15_repl_y3,risk_reinf_y3,risk_npv15_reinf_y3) AS '03'
                                    ,(risk_na_y4,risk_npv15_na_y4,risk_repl_y4,risk_npv15_repl_y4,risk_reinf_y4,risk_npv15_reinf_y4) AS '04'
                                    ,(risk_na_y5,risk_npv15_na_y5,risk_repl_y5,risk_npv15_repl_y5,risk_reinf_y5,risk_npv15_reinf_y5) AS '05'
                                    ,(risk_na_y6,risk_npv15_na_y6,risk_repl_y6,risk_npv15_repl_y6,risk_reinf_y6,risk_npv15_reinf_y6) AS '06'
                                    ,(risk_na_y7,risk_npv15_na_y7,risk_repl_y7,risk_npv15_repl_y7,risk_reinf_y7,risk_npv15_reinf_y7) AS '07'
                                    ,(risk_na_y8,risk_npv15_na_y8,risk_repl_y8,risk_npv15_repl_y8,risk_reinf_y8,risk_npv15_reinf_y8) AS '08'
                                    ,(risk_na_y9,risk_npv15_na_y9,risk_repl_y9,risk_npv15_repl_y9,risk_reinf_y9,risk_npv15_reinf_y9) AS '09'
                                    ,(risk_na_y10,risk_npv15_na_y10,risk_repl_y10,risk_npv15_repl_y10,risk_reinf_y10,risk_npv15_reinf_y10) AS '10'
                                    ,(risk_na_y11,risk_npv15_na_y11,risk_repl_y11,risk_npv15_repl_y11,risk_reinf_y11,risk_npv15_reinf_y11) AS '11'
                                    ,(risk_na_y12,risk_npv15_na_y12,risk_repl_y12,risk_npv15_repl_y12,risk_reinf_y12,risk_npv15_reinf_y12) AS '12'
                                    ))
      WHERE yr < i_years + 3
      ORDER BY pick_id, calendar_year
   ;

   CURSOR actioned_bay_csr IS

      SELECT    pick_id
               ,equip_grp_id
               ,del_year
               ,event
      FROM structools.st_user_program_pickid
      WHERE user_id = i_user_id AND user_version = i_user_version
      AND equip_grp_id IN ('BAY')
      ORDER BY pick_id, del_year
   ;
         
   CURSOR   risk_bay_csr IS

      WITH risk_bay_noaction AS
      (
      SELECT  'B'||A.pick_id        AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_bay_NOACTION  A
      )
      ,
      risk_bay_noaction_npv15 AS
      (
      SELECT  'B'||A.pick_id        AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_bay_NOACTION_npv15  A
      )
      ,
      risk_bay_replace AS
      (
      SELECT  'B'||A.pick_id        AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_bay_REPLACE  A
      )
      ,
      risk_bay_replace_npv15 AS
      (
      SELECT  'B'||A.pick_id        AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_bay_REPLACE_npv15  A
      )
      ,
      risk_bay_repair AS
      (
      SELECT  'B'||A.pick_id        AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_bay_repair  A
      )
      ,
      risk_bay_repair_npv15 AS
      (
      SELECT  'B'||A.pick_id        AS pick_id_typ
              ,A.* 
      FROM structools.st_risk_bay_repair_npv15  A
      )
      ,
      suppressed_bays AS
      (
      SELECT pick_id
            ,'Y'					AS suppressed
      FROM structools.st_pickid_suppressed_init
      WHERE equip_grp_id = 'BAY'
--      UNION
--      SELECT pick_id
--            ,'P'						AS suppressed				
--      FROM structools.st_pickid_suppressed
--      WHERE equip_grp_id = 'BAY'
--      AND scenario_id = v_suppress_pickids_ver
      )
      ,
      pickid_bay AS
      (
      SELECT A.pick_id
             ,'BAY'        AS         equip_grp_id
	     ,nvl(b.suppressed,'N')	AS suppressed
      FROM structools.st_bay_equipment	A	LEFT OUTER JOIN
	   suppressed_bays		b
      ON A.pick_id = b.pick_id	
      )
      ,
      risk AS
      (
      SELECT A.pick_id
            ,A.equip_grp_id
               ,c.risk_indx_val_Y0  AS risk_na_y0
               ,c.risk_indx_val_Y1  AS risk_na_y1
               ,c.risk_indx_val_Y2  AS risk_na_y2
               ,c.risk_indx_val_y3  AS risk_na_y3
               ,c.risk_indx_val_y4  AS risk_na_y4
               ,c.risk_indx_val_y5  AS risk_na_y5
               ,c.risk_indx_val_y6  AS risk_na_y6
               ,c.risk_indx_val_y7  AS risk_na_y7
               ,c.risk_indx_val_y8  AS risk_na_y8
               ,c.risk_indx_val_y9  AS risk_na_y9
               ,c.risk_indx_val_y10  AS risk_na_y10      /* 10 years + 0-year plus 2 years to allow to past replaces (via pickid_committed list) */
               ,c.risk_indx_val_y11  AS risk_na_y11      /* If the run is for more than 10 years, this needs to be changed to extract more risk-years */
               ,c.risk_indx_val_y12  AS risk_na_y12
               ,d.risk_indx_val_Y0  AS risk_npv15_na_y0
               ,d.risk_indx_val_Y1  AS risk_npv15_na_y1
               ,d.risk_indx_val_Y2  AS risk_npv15_na_y2
               ,d.risk_indx_val_y3  AS risk_npv15_na_y3
               ,d.risk_indx_val_y4  AS risk_npv15_na_y4
               ,d.risk_indx_val_y5  AS risk_npv15_na_y5
               ,d.risk_indx_val_y6  AS risk_npv15_na_y6
               ,d.risk_indx_val_y7  AS risk_npv15_na_y7
               ,d.risk_indx_val_y8  AS risk_npv15_na_y8
               ,d.risk_indx_val_y9  AS risk_npv15_na_y9
               ,d.risk_indx_val_y10  AS risk_npv15_na_y10
               ,d.risk_indx_val_y11  AS risk_npv15_na_y11
               ,d.risk_indx_val_y12  AS risk_npv15_na_y12
               ,E.risk_indx_val_Y0  AS risk_repl_y0
               ,E.risk_indx_val_Y1  AS risk_repl_y1
               ,E.risk_indx_val_Y2  AS risk_repl_y2
               ,E.risk_indx_val_y3  AS risk_repl_y3
               ,E.risk_indx_val_y4  AS risk_repl_y4
               ,E.risk_indx_val_y5  AS risk_repl_y5
               ,E.risk_indx_val_y6  AS risk_repl_y6
               ,E.risk_indx_val_y7  AS risk_repl_y7
               ,E.risk_indx_val_y8  AS risk_repl_y8
               ,E.risk_indx_val_y9  AS risk_repl_y9
               ,E.risk_indx_val_y10  AS risk_repl_y10
               ,E.risk_indx_val_y11  AS risk_repl_y11
               ,E.risk_indx_val_y12  AS risk_repl_y12
               ,f.risk_indx_val_Y0  AS risk_npv15_repl_y0
               ,f.risk_indx_val_Y1  AS risk_npv15_repl_y1
               ,f.risk_indx_val_Y2  AS risk_npv15_repl_y2
               ,f.risk_indx_val_y3  AS risk_npv15_repl_y3
               ,f.risk_indx_val_y4  AS risk_npv15_repl_y4
               ,f.risk_indx_val_y5  AS risk_npv15_repl_y5
               ,f.risk_indx_val_y6  AS risk_npv15_repl_y6
               ,f.risk_indx_val_y7  AS risk_npv15_repl_y7
               ,f.risk_indx_val_y8  AS risk_npv15_repl_y8
               ,f.risk_indx_val_y9  AS risk_npv15_repl_y9
               ,f.risk_indx_val_y10  AS risk_npv15_repl_y10
               ,f.risk_indx_val_y11  AS risk_npv15_repl_y11
               ,f.risk_indx_val_y12  AS risk_npv15_repl_y12
               ,G.risk_indx_val_Y0  AS risk_repair_y0
               ,G.risk_indx_val_Y1  AS risk_repair_y1
               ,G.risk_indx_val_Y2  AS risk_repair_y2
               ,G.risk_indx_val_y3  AS risk_repair_y3
               ,G.risk_indx_val_y4  AS risk_repair_y4
               ,G.risk_indx_val_y5  AS risk_repair_y5
               ,G.risk_indx_val_y6  AS risk_repair_y6
               ,G.risk_indx_val_y7  AS risk_repair_y7
               ,G.risk_indx_val_y8  AS risk_repair_y8
               ,G.risk_indx_val_y9  AS risk_repair_y9
               ,G.risk_indx_val_y10  AS risk_repair_y10
               ,G.risk_indx_val_y11  AS risk_repair_y11
               ,G.risk_indx_val_y12  AS risk_repair_y12
               ,h.risk_indx_val_Y0  AS risk_npv15_repair_y0
               ,h.risk_indx_val_Y1  AS risk_npv15_repair_y1
               ,h.risk_indx_val_Y2  AS risk_npv15_repair_y2
               ,h.risk_indx_val_y3  AS risk_npv15_repair_y3
               ,h.risk_indx_val_y4  AS risk_npv15_repair_y4
               ,h.risk_indx_val_y5  AS risk_npv15_repair_y5
               ,h.risk_indx_val_y6  AS risk_npv15_repair_y6
               ,h.risk_indx_val_y7  AS risk_npv15_repair_y7
               ,h.risk_indx_val_y8  AS risk_npv15_repair_y8
               ,h.risk_indx_val_y9  AS risk_npv15_repair_y9
               ,h.risk_indx_val_y10  AS risk_npv15_repair_y10
               ,h.risk_indx_val_y11  AS risk_npv15_repair_y11
               ,h.risk_indx_val_y12  AS risk_npv15_repair_y12
      FROM  pickid_bay                A  INNER JOIN
            risk_bay_noaction c                                          
      ON A.pick_id = c.pick_id_typ
                                    INNER JOIN
            risk_bay_noaction_npv15 d                                          
      ON A.pick_id = d.pick_id_typ
                                    INNER JOIN
            risk_bay_replace E                                          
      ON A.pick_id = E.pick_id_typ
                                    INNER JOIN
            risk_bay_replace_npv15 f                                          
      ON A.pick_id = f.pick_id_typ
                                    INNER JOIN
            risk_bay_repair         G                                          
      ON A.pick_id = G.pick_id_typ
                                    INNER JOIN
            risk_bay_repair_npv15 h                                          
      ON A.pick_id = h.pick_id_typ
      WHERE A.suppressed = 'N'	
      )
      SELECT /*+ parallel (risk,8) */
             pick_id
            ,equip_grp_id
            ,v_this_year + yr AS calendar_year
            ,risk_na
            ,risk_npv15_na
            ,risk_repl
            ,risk_npv15_repl
            ,risk_repair         AS risk_reinf_repair          /* renamed so that resultant columns are identical between the cursor for poles+eqp */
            ,risk_npv15_repair   AS risk_npv15_reinf_repair    /* and bays, and have a single table based on the cursor definition. */
      FROM  risk
      UNPIVOT ((risk_na, risk_npv15_na, risk_repl, risk_npv15_repl, risk_repair, risk_npv15_repair) 
                                    FOR yr IN 
                                    ((risk_na_y0,risk_npv15_na_y0,risk_repl_y0,risk_npv15_repl_y0,risk_repair_y0,risk_npv15_repair_y0) AS '00'
                                    ,(risk_na_y1,risk_npv15_na_y1,risk_repl_y1,risk_npv15_repl_y1,risk_repair_y1,risk_npv15_repair_y1) AS '01'
                                    ,(risk_na_y2,risk_npv15_na_y2,risk_repl_y2,risk_npv15_repl_y2,risk_repair_y2,risk_npv15_repair_y2) AS '02'
                                    ,(risk_na_y3,risk_npv15_na_y3,risk_repl_y3,risk_npv15_repl_y3,risk_repair_y3,risk_npv15_repair_y3) AS '03'
                                    ,(risk_na_y4,risk_npv15_na_y4,risk_repl_y4,risk_npv15_repl_y4,risk_repair_y4,risk_npv15_repair_y4) AS '04'
                                    ,(risk_na_y5,risk_npv15_na_y5,risk_repl_y5,risk_npv15_repl_y5,risk_repair_y5,risk_npv15_repair_y5) AS '05'
                                    ,(risk_na_y6,risk_npv15_na_y6,risk_repl_y6,risk_npv15_repl_y6,risk_repair_y6,risk_npv15_repair_y6) AS '06'
                                    ,(risk_na_y7,risk_npv15_na_y7,risk_repl_y7,risk_npv15_repl_y7,risk_repair_y7,risk_npv15_repair_y7) AS '07'
                                    ,(risk_na_y8,risk_npv15_na_y8,risk_repl_y8,risk_npv15_repl_y8,risk_repair_y8,risk_npv15_repair_y8) AS '08'
                                    ,(risk_na_y9,risk_npv15_na_y9,risk_repl_y9,risk_npv15_repl_y9,risk_repair_y9,risk_npv15_repair_y9) AS '09'
                                    ,(risk_na_y10,risk_npv15_na_y10,risk_repl_y10,risk_npv15_repl_y10,risk_repair_y10,risk_npv15_repair_y10) AS '10'
                                    ,(risk_na_y11,risk_npv15_na_y11,risk_repl_y11,risk_npv15_repl_y11,risk_repair_y11,risk_npv15_repair_y11) AS '11'
                                    ,(risk_na_y12,risk_npv15_na_y12,risk_repl_y12,risk_npv15_repl_y12,risk_repair_y12,risk_npv15_repair_y12) AS '12'
                                    ))
      WHERE yr < i_years + 3
      ORDER BY pick_id, calendar_year
   ;
   CURSOR   risk_fractions_csr IS

      SELECT pick_id
            ,(fire_frac + eshock_frac + physimp_frac) AS pubsafety_frac
            ,wforce_frac
            ,reliability_frac
            ,env_frac
            ,fin_frac
      FROM structools.st_risk_fractions
      WHERE MODEL_NAME IN ('DSTR', 'PTSD', 'PWOD',  'SECT', 'BAY')
   ;

   TYPE actioned_eqp_tab_typ    IS TABLE OF actioned_pwod_eqp_csr%ROWTYPE;
   v_actioned_eqp_tab           actioned_eqp_tab_typ := actioned_eqp_tab_typ();

   TYPE actioned_rec_typ IS RECORD
   (event                        VARCHAR2(15)
   ,del_year                     PLS_INTEGER
   );
   TYPE action_tab_typ           IS TABLE OF actioned_rec_typ;
   v_action_tab                  action_tab_typ := action_tab_typ();
         
   TYPE actioned_arr_typ IS TABLE OF action_tab_typ INDEX BY VARCHAR2(20);
   v_actioned_arr        actioned_arr_typ;
            
   TYPE risk_tab_typ    IS TABLE OF risk_pwod_eqp_csr%ROWTYPE;
   v_risk_tab           risk_tab_typ := risk_tab_typ();

   TYPE risk_fractions_tab_typ   IS TABLE OF risk_fractions_csr%ROWTYPE;
   v_risk_fractions_tab          risk_fractions_tab_typ := risk_fractions_tab_typ();

   TYPE risk_fractions_rec_typ IS RECORD
   (pubsafety_frac               float(126)
   ,wforce_frac                  float(126)
   ,reliability_frac             float(126)
   ,env_frac                     float(126)
   ,fin_frac                     float(126)
   );
         
   TYPE risk_fractions_arr_typ IS TABLE OF risk_fractions_rec_typ INDEX BY VARCHAR2(20);
   v_risk_fractions_arr          risk_fractions_arr_typ;

   v_pick_id                     varchar2(10);
   v_egi                         varchar2(4);
   v_event                       VARCHAR2(15);
   v_del_year                    PLS_INTEGER;
   v_calendar_year               PLS_INTEGER;
   v_risk                        FLOAT(126);
   v_risk_npv15                  FLOAT(126);
   j                             PLS_INTEGER;
   v_risk_pubsafety              FLOAT(126);
   v_risk_wforce                 FLOAT(126);
   v_risk_reliability            FLOAT(126);
   v_risk_env                    FLOAT(126);
   v_risk_fin                    FLOAT(126);
   v_risk_npv15_pubsafety        FLOAT(126);
   v_risk_npv15_wforce           FLOAT(126);
   v_risk_npv15_reliability      FLOAT(126);
   v_risk_npv15_env              FLOAT(126);
   v_risk_npv15_fin              FLOAT(126);
   v_year                        PLS_INTEGER;
   v_action_cnt                  PLS_INTEGER;
   v_max_event_del_year          char(13);
   v_event_del_year              char(13);
	 
	/* Private procedures for procedure generate_user_prog_risk_prof */   
   PROCEDURE  validate_user_prog_input IS
   
   BEGIN
      /* Validate input parameters */
      IF i_user_id IS NULL THEN
         dbms_output.put_line ('i_user_id parameter must NOT be NULL'); 
         RAISE v_prog_excp;
      END IF;

      IF i_user_version IS NULL THEN
         dbms_output.put_line ('i_user_version parameter must NOT be NULL'); 
         RAISE v_prog_excp;
      END IF;

      IF i_years IS NULL THEN
         dbms_output.put_line ('i_years parameter must NOT be NULL'); 
         RAISE v_prog_excp;
      END IF;

      IF i_years > 10 THEN 
         dbms_output.put_line ('maximum OF 10 years VALUES can be GENERATED'); 
         RAISE v_prog_excp;
      END IF;
            

      /* ensure the user table exists */
      BEGIN
         SELECT table_name
         INTO v_table_name
         FROM all_tab_columns
         WHERE owner = upper(i_user_id)
         AND table_name = 'ST_USER_PROGRAM_PICKID_user'
         AND ROWNUM = 1;
      EXCEPTION
         WHEN no_data_found THEN
            dbms_output.put_line ('User table '||i_user_id||' does not exist or egszadk have no access to it'); 
            RAISE v_prog_excp;
      END;

      /* ensure the user table contains all columns we need */
      BEGIN
         SELECT column_name
         INTO v_column_name
         FROM all_tab_columns
         WHERE owner = UPPER(i_user_id)
         AND table_name = 'ST_USER_PROGRAM_PICKID_user'
         AND column_name IN ('USER_VERSION');
      EXCEPTION
         WHEN no_data_found THEN
            dbms_output.put_line ('User table must have column USER_VERSION'); 
            RAISE v_prog_excp;
      END;

      BEGIN
         SELECT column_name
         INTO v_column_name
         FROM all_tab_columns
         WHERE owner = UPPER(i_user_id)
         AND table_name = 'ST_USER_PROGRAM_PICKID_user'
         AND column_name IN ('PICK_ID');
      EXCEPTION
         WHEN no_data_found THEN
            dbms_output.put_line ('User table must have column PICK_ID'); 
            RAISE v_prog_excp;
      END;

      BEGIN
         SELECT column_name
         INTO v_column_name
         FROM all_tab_columns
         WHERE owner = UPPER(i_user_id)
         AND table_name = 'ST_USER_PROGRAM_PICKID_user'
         AND column_name IN ('EQUIP_GRP_ID');
      EXCEPTION
         WHEN no_data_found THEN
            dbms_output.put_line ('User table must have column EQUIP_GRP_ID'); 
            RAISE v_prog_excp;
      END;

      BEGIN
         SELECT column_name
         INTO v_column_name
         FROM all_tab_columns
         WHERE owner = UPPER(i_user_id)
         AND table_name = 'ST_USER_PROGRAM_PICKID_user'
         AND column_name IN ('DEL_YEAR');
      EXCEPTION
         WHEN no_data_found THEN
            dbms_output.put_line ('User table must have column DEL_YEAR'); 
            RAISE v_prog_excp;
      END;

      BEGIN
         SELECT column_name
         INTO v_column_name
         FROM all_tab_columns
         WHERE owner = UPPER(i_user_id)
         AND table_name = 'ST_USER_PROGRAM_PICKID_user'
         AND column_name IN ('EVENT');
      EXCEPTION
         WHEN no_data_found THEN
            dbms_output.put_line ('User table must have column EVENT'); 
            RAISE v_prog_excp;
      END;

      /* Ensure the user table contains data for the specified version */
      v_sql := 'SELECT count(*) FROM '||i_user_id||'.st_user_program_pickid_user WHERE user_version = '||i_user_version;
      execute immediate(v_sql) INTO v_cnt;
      IF v_cnt = 0 THEN
         dbms_output.put_line ('User TABLE does NOT contain ANY DATA FOR THE specified VERSION'); 
         RAISE v_prog_excp;
      END IF;      

      /* Ensure pickids are of only of pre-defined egi types */
      v_sql :=  'SELECT count(*) FROM '||i_user_id||'.st_user_program_pickid_user WHERE user_version = '||i_user_version
                  ||' AND equip_grp_id IS NULL';
      execute immediate(v_sql) INTO v_cnt;
      IF v_cnt > 0 THEN
         dbms_output.put_line ('user DATA contains pickids with NULL equip_grp_id'); 
         RAISE v_prog_excp;
      END IF;
      v_sql :=  'SELECT count(*) FROM '||i_user_id||'.st_user_program_pickid_user WHERE user_version = '||i_user_version
                  ||' AND equip_grp_id NOT IN (''BAY'',''PWOD'',''DSTR'',''PTSD'',''SECT'')';
      execute immediate(v_sql) INTO v_cnt;
      IF v_cnt > 0 THEN
         dbms_output.put_line ('user DATA contains pickids with invalid EGIs (only BAY,PWOD,DSTR,PTSD,SECT allowed)'); 
         RAISE v_prog_excp;
      END IF;

      /* Ensure all pickids have a valid event */
      v_sql := 'SELECT count(*) FROM '||i_user_id||'.st_user_program_pickid_user WHERE user_version = '||i_user_version
                  ||' AND event is NULL';
      execute immediate(v_sql) INTO v_cnt;
      IF v_cnt > 0 THEN
         dbms_output.put_line ('user DATA contains NULL EVENTS'); 
         RAISE v_prog_excp;
      END IF;

      v_sql := 'SELECT count(*) FROM '||i_user_id||'.st_user_program_pickid_user WHERE user_version = '||i_user_version
                  ||' AND event NOT IN (''REPLACE'',''REINFORCE'',''REPAIR'',''REMOVE'')';
      execute immediate(v_sql) INTO v_cnt;
      IF v_cnt > 0 THEN
         dbms_output.put_line ('user DATA contains invalid EVENTS'); 
         RAISE v_prog_excp;
      END IF;

      /* Ensure pickids are unique for user+version*/
      v_sql :=  'SELECT count(*) FROM (SELECT pick_id, count(*) AS cnt FROM '||i_user_id||'.st_user_program_pickid_user WHERE user_version = '||i_user_version||'  GROUP BY pick_id) aa'
                  ||'  WHERE aa.cnt > 1';
      execute immediate(v_sql) INTO v_cnt;
      IF v_cnt > 0 THEN
         dbms_output.put_line ('user DATA contains duplicate pickids for version '||i_user_version); 
         RAISE v_prog_excp;
      END IF;
         
      /* Ensure del_year is between this_year+1 and this_year+ 10*/
      v_sql :=  'SELECT count(*) FROM '||i_user_id||'.st_user_program_pickid_user WHERE user_version = '||i_user_version
               ||'  AND del_year is null';
      execute immediate(v_sql) INTO v_cnt;
      IF v_cnt > 0 THEN
         dbms_output.put_line ('user DATA contains NULLs in DEL_YEAR'); 
         RAISE v_prog_excp;
      END IF;
         
      v_sql :=  'SELECT count(*) FROM '||i_user_id||'.st_user_program_pickid_user WHERE user_version = '||i_user_version
               ||'  AND (del_year <= '||v_this_year||' OR del_year > ('||v_this_year||' + 10))';
      execute immediate(v_sql) INTO v_cnt;
      IF v_cnt > 0 THEN
         dbms_output.put_line ('user DATA contains invalid values in DEL_YEAR'); 
         RAISE v_prog_excp;
      END IF;
         
      IF i_delete_flg = 'Y' THEN
         dbms_output.put_line('Deleting existing results fopr user '||i_user_id||' AND VERSION '||i_user_version);
         DELETE FROM structools.st_user_program_pickid
         WHERE user_id = i_user_id
         AND   user_version = i_user_version;
         DELETE FROM structools.st_user_program_risk
         WHERE user_id = i_user_id
         AND   user_version = i_user_version;
      ELSE
         BEGIN
            SELECT NULL
            INTO     v_dummy                        
            FROM structools.st_user_program_risk
            WHERE user_id = i_user_id
            AND   user_version = i_user_version
            AND ROWNUM = 1;
            dbms_output.put_line ('st_user_program_risk already contains DATA FOR user_id '||i_user_id 
                                    ||' AND user_version '||i_user_version);
            RAISE v_prog_excp;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN /*delete data from the structools input table if no risk data exists for it */
               DELETE FROM structools.st_user_program_pickid
               WHERE user_id = i_user_id
               AND   user_version = i_user_version;
         END;
      END IF;                     
         
      /* Insert data from user owned table to equivalend structools input table */
      v_sql := 'INSERT INTO structools.ST_USER_PROGRAM_PICKID (user_id,user_version,pick_id,equip_grp_id,del_year,event)'
               ||' SELECT '''||i_user_id||''' AS user_id, user_version,pick_id,equip_grp_id,del_year,event'
               ||' FROM '||i_user_id||'.st_user_program_pickid_user WHERE user_version = '||i_user_version;             
      --dbms_output.put_line(v_sql);              
      execute immediate(v_sql);  /* This commits data */

   EXCEPTION
      WHEN v_prog_excp THEN
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END validate_user_prog_input;
               
/* Mainline of  generate_user_prog_risk_prof */          
BEGIN
   dbms_output.put_line('generate_user_prog_risk_prof Start '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

	/* Validate input parameters and input data */
   validate_user_prog_input;
   
   /************************************************/
   /**** Process poles and equipment on poles *****/
   /************************************************/
         
   /* dump all actioned eqp into a nested table */
   OPEN actioned_pwod_eqp_csr;
   FETCH actioned_pwod_eqp_csr BULK COLLECT INTO v_actioned_eqp_tab;
   CLOSE actioned_pwod_eqp_csr;
         
   /*reload them into an associative array, indexed by pick_id. Fill in missing years. */
   v_pick_id := v_actioned_eqp_tab(1).pick_id;
   j := 1;
   WHILE j <= v_actioned_eqp_tab.COUNT LOOP
      v_year := v_this_year + 1;
      v_action_cnt := 0;
      v_action_tab.DELETE;
      v_max_event_del_year := ' ';

      WHILE v_year <= v_this_year + i_years LOOP --2026
         v_del_year := v_actioned_eqp_tab(j).del_year;
         v_event := v_actioned_eqp_tab(j).event;
            
         IF v_year < v_del_year THEN
            v_action_tab.EXTEND;
            v_action_cnt := v_action_cnt + 1;
            IF v_max_event_del_year = ' ' THEN
               v_action_tab(v_action_cnt).event    := NULL;
               v_action_tab(v_action_cnt).del_year := NULL;
            ELSE
               v_action_tab(v_action_cnt).event    := RTRIM(SUBSTR(v_max_event_del_year,1,9));
               v_action_tab(v_action_cnt).del_year := SUBSTR(v_max_event_del_year,10,4);
            END IF;
         ELSIF v_year = v_del_year THEN
            v_action_tab.EXTEND;
            v_action_cnt := v_action_cnt + 1;
            v_event_del_year := RPAD(v_actioned_eqp_tab(j).event,9)||v_actioned_eqp_tab(j).del_year;
            IF v_event_del_year > v_max_event_del_year THEN
               v_max_event_del_year := v_event_del_year;
            END IF;
            v_action_tab(v_action_cnt).event    := RTRIM(SUBSTR(v_max_event_del_year,1,9));
            v_action_tab(v_action_cnt).del_year := SUBSTR(v_max_event_del_year,10,4);

            j := j + 1;
            IF j <= v_actioned_eqp_tab.COUNT AND v_actioned_eqp_tab(j).pick_id = v_pick_id THEN
               NULL;
            ELSE
               IF v_year < v_this_year + i_years THEN
                  WHILE v_year < v_this_year + i_years LOOP
                     v_year := v_year + 1;
                     v_action_tab.EXTEND;
                     v_action_cnt := v_action_cnt + 1;
                     v_action_tab(v_action_cnt).event    := RTRIM(SUBSTR(v_max_event_del_year,1,9));
                     v_action_tab(v_action_cnt).del_year := SUBSTR(v_max_event_del_year,10,4);
                  END LOOP;
               END IF;
            END IF;
         END IF;
         v_year := v_year + 1;
      END LOOP;        
      v_actioned_arr(v_pick_id) := v_action_tab;

      IF j <= v_actioned_eqp_tab.COUNT THEN
         v_pick_id := v_actioned_eqp_tab(j).pick_id;
      END IF;
            
   END LOOP;

   v_actioned_eqp_tab.DELETE;
   v_action_tab.DELETE;

   /* Fetch risk fractions for all pickids, all years, and store in a nested table */
   OPEN risk_fractions_csr;
   FETCH risk_fractions_csr BULK COLLECT INTO v_risk_fractions_tab;
   CLOSE risk_fractions_csr;

   /* Reload into an associative array, indexed by pick_id */
   FOR i IN 1 .. v_risk_fractions_tab.COUNT LOOP
      v_pick_id := v_risk_fractions_tab(i).pick_id;
      v_risk_fractions_arr(v_pick_id).pubsafety_frac    := v_risk_fractions_tab(i).pubsafety_frac;
      v_risk_fractions_arr(v_pick_id).wforce_frac       := v_risk_fractions_tab(i).wforce_frac;
      v_risk_fractions_arr(v_pick_id).reliability_frac := v_risk_fractions_tab(i).reliability_frac;
      v_risk_fractions_arr(v_pick_id).env_frac         := v_risk_fractions_tab(i).env_frac;
      v_risk_fractions_arr(v_pick_id).fin_frac         := v_risk_fractions_tab(i).fin_frac;
   END LOOP;
         
   v_risk_fractions_tab.DELETE;

   /* Process risk data, matching with the actioned table where exists. Insert into st_del_program_risk table */
   OPEN risk_pwod_eqp_csr;

   LOOP 

      /* Fetch risk data for a single pickid, all years, and store in a nested table */
      FETCH risk_pwod_eqp_csr BULK COLLECT INTO v_risk_tab LIMIT i_years + 3;
      EXIT WHEN risk_pwod_eqp_csr%NOTFOUND;
               
      v_pick_id            := v_risk_tab(1).pick_id;
      v_action_tab.DELETE;
      IF v_actioned_arr.EXISTS(v_pick_id) THEN
         v_action_tab         := v_actioned_arr(v_pick_id);
      END IF;
      v_egi                := v_risk_tab(1).equip_grp_id;
      FOR i IN 2 .. (v_risk_tab.COUNT - 2) LOOP          /* only need to process i_years, starting from v_this_year + 1. */
                                                         /* The rest is to access risk for replace/reinforce for committed pickids.*/
         IF v_action_tab.COUNT > 0 THEN
            v_event     := v_action_tab(i - 1).event;
            v_del_year  := v_action_tab(i - 1).del_year;
         ELSE
            v_event     := NULL;
            v_del_year  := NULL;
         END IF;     
         v_calendar_year := v_risk_tab(i).calendar_year;
         IF v_event IS NULL THEN                         
            v_risk := v_risk_tab(i).risk_na;
            v_risk_npv15 := v_risk_tab(i).risk_npv15_na;
         ELSIF v_event = 'REINFORCE' THEN
            v_risk := v_risk_tab(i).risk_reinf_repair;
            v_risk_npv15 := v_risk_tab(i).risk_npv15_reinf_repair;
         ELSIF v_event = 'REPLACE' THEN
            j := v_calendar_year - v_del_year + 1;
            v_risk := v_risk_tab(j).risk_repl;
            v_risk_npv15 := v_risk_tab(j).risk_npv15_repl;
         ELSIF v_event = 'REMOVE' THEN
            v_risk := 0;
            v_risk_npv15 := 0;
         END IF;
         v_risk_pubsafety     := v_risk * v_risk_fractions_arr(v_pick_id).pubsafety_frac;
         v_risk_wforce        := v_risk * v_risk_fractions_arr(v_pick_id).wforce_frac;
         v_risk_reliability   := v_risk * v_risk_fractions_arr(v_pick_id).reliability_frac;
         v_risk_env           := v_risk * v_risk_fractions_arr(v_pick_id).env_frac;
         v_risk_fin           := v_risk * v_risk_fractions_arr(v_pick_id).fin_frac;
         v_risk_npv15_pubsafety     := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).pubsafety_frac;
         v_risk_npv15_wforce        := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).wforce_frac;
         v_risk_npv15_reliability   := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).reliability_frac;
         v_risk_npv15_env           := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).env_frac;
         v_risk_npv15_fin           := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).fin_frac;
               
         INSERT INTO structools.st_user_program_risk
            VALUES   (i_user_id
                     ,i_user_version
                     ,v_pick_id
                     ,v_egi
                     ,v_calendar_year
                     ,v_event
                     ,v_risk
                     ,v_risk_pubsafety
                     ,v_risk_wforce
                     ,v_risk_reliability
                     ,v_risk_env
                     ,v_risk_npv15
                     ,v_risk_npv15_pubsafety
                     ,v_risk_npv15_wforce
                     ,v_risk_npv15_reliability
                     ,v_risk_npv15_env
                     ,v_risk_fin
                     ,v_risk_npv15_fin
                      );
      END LOOP;
   END LOOP;
   CLOSE risk_pwod_eqp_csr;
         
   v_actioned_arr.DELETE;
         
   /***********************/
   /**** Process bays        *****/
   /**********************/
         
   /* dump all actioned eqp into a nested table */
   OPEN actioned_bay_csr;
   FETCH actioned_bay_csr BULK COLLECT INTO v_actioned_eqp_tab;
   CLOSE actioned_bay_csr;
         
   /*reload them into an associative array, indexed by pick_id. Fill in missing years. */
   v_pick_id := v_actioned_eqp_tab(1).pick_id;
   j := 1;
   WHILE j <= v_actioned_eqp_tab.COUNT LOOP
      v_year := v_this_year + 1;
      v_action_cnt := 0;
      v_action_tab.DELETE;
      v_max_event_del_year := ' ';

      WHILE v_year <= v_this_year + i_years LOOP

         v_del_year := v_actioned_eqp_tab(j).del_year;
         v_event := v_actioned_eqp_tab(j).event;
            
         IF v_year < v_del_year THEN
            v_action_tab.EXTEND;
            v_action_cnt := v_action_cnt + 1;
            IF v_max_event_del_year = ' ' THEN
               v_action_tab(v_action_cnt).event    := NULL;
               v_action_tab(v_action_cnt).del_year := NULL;
            ELSE
               v_action_tab(v_action_cnt).event    := RTRIM(SUBSTR(v_max_event_del_year,1,9));
               v_action_tab(v_action_cnt).del_year := SUBSTR(v_max_event_del_year,10,4);
            END IF;
         ELSIF v_year = v_del_year THEN
            v_action_tab.EXTEND;
            v_action_cnt := v_action_cnt + 1;
            v_event_del_year := RPAD(v_actioned_eqp_tab(j).event,9)||v_actioned_eqp_tab(j).del_year;
            IF v_event_del_year > v_max_event_del_year THEN
               v_max_event_del_year := v_event_del_year;
            END IF;
            v_action_tab(v_action_cnt).event    := RTRIM(SUBSTR(v_max_event_del_year,1,9));
            v_action_tab(v_action_cnt).del_year := SUBSTR(v_max_event_del_year,10,4);

            j := j + 1;
            IF j <= v_actioned_eqp_tab.COUNT AND v_actioned_eqp_tab(j).pick_id = v_pick_id THEN
               NULL;
            ELSE
               IF v_year < v_this_year + i_years THEN
                  WHILE v_year < v_this_year + i_years LOOP
                     v_year := v_year + 1;
                     v_action_tab.EXTEND;
                     v_action_cnt := v_action_cnt + 1;
                     v_action_tab(v_action_cnt).event    := RTRIM(SUBSTR(v_max_event_del_year,1,9));
                     v_action_tab(v_action_cnt).del_year := SUBSTR(v_max_event_del_year,10,4);
                  END LOOP;
               END IF;
            END IF;
         END IF;
         v_year := v_year + 1;
      END LOOP;        
      v_actioned_arr(v_pick_id) := v_action_tab;

      IF j <= v_actioned_eqp_tab.COUNT THEN
         v_pick_id := v_actioned_eqp_tab(j).pick_id;
      END IF;
            
   END LOOP;

   v_actioned_eqp_tab.DELETE;
   v_action_tab.DELETE;

   /* Process risk data, matching with the actioned table where exists. Insert into st_del_program_risk table */
   OPEN risk_bay_csr;

   LOOP 

      /* Fetch risk data for a single pickid, all years, and store in a nested table */
      FETCH risk_bay_csr BULK COLLECT INTO v_risk_tab LIMIT i_years + 3;
      EXIT WHEN risk_bay_csr%NOTFOUND;
               
      v_pick_id            := v_risk_tab(1).pick_id;
      v_action_tab.DELETE;
      IF v_actioned_arr.EXISTS(v_pick_id) THEN
         v_action_tab         := v_actioned_arr(v_pick_id);
      END IF;
      v_egi                := v_risk_tab(1).equip_grp_id;
      FOR i IN 2 .. (v_risk_tab.COUNT - 2) LOOP          /* only need to process i_years, starting from v_this_year + 1. */
                                                         /* The rest is to access risk for replace/reinforce for committed pickids.*/
         IF v_action_tab.COUNT > 0 THEN
            v_event     := v_action_tab(i - 1).event;
            v_del_year  := v_action_tab(i - 1).del_year;
         ELSE
            v_event     := NULL;
            v_del_year  := NULL;
         END IF;     
         v_calendar_year := v_risk_tab(i).calendar_year;
         IF v_event IS NULL THEN                         
            v_risk := v_risk_tab(i).risk_na;
            v_risk_npv15 := v_risk_tab(i).risk_npv15_na;
         ELSIF v_event = 'REPAIR' THEN
            v_risk := v_risk_tab(i).risk_reinf_repair;
            v_risk_npv15 := v_risk_tab(i).risk_npv15_reinf_repair;
         ELSIF v_event = 'REPLACE' THEN
            j := v_calendar_year - v_del_year + 1;
            v_risk := v_risk_tab(j).risk_repl;
            v_risk_npv15 := v_risk_tab(j).risk_npv15_repl;
         ELSIF v_event = 'REMOVE' THEN
            v_risk := 0;
            v_risk_npv15 := 0;
         END IF;
         v_risk_pubsafety     := v_risk * v_risk_fractions_arr(v_pick_id).pubsafety_frac;
         v_risk_wforce        := v_risk * v_risk_fractions_arr(v_pick_id).wforce_frac;
         v_risk_reliability   := v_risk * v_risk_fractions_arr(v_pick_id).reliability_frac;
         v_risk_env           := v_risk * v_risk_fractions_arr(v_pick_id).env_frac;
         v_risk_fin           := v_risk * v_risk_fractions_arr(v_pick_id).fin_frac;
         v_risk_npv15_pubsafety     := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).pubsafety_frac;
         v_risk_npv15_wforce        := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).wforce_frac;
         v_risk_npv15_reliability   := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).reliability_frac;
         v_risk_npv15_env           := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).env_frac;
         v_risk_npv15_fin           := v_risk_npv15 * v_risk_fractions_arr(v_pick_id).fin_frac;
         INSERT INTO structools.st_user_program_risk
            VALUES   (i_user_id
                     ,i_user_version
                     ,v_pick_id
                     ,v_egi
                     ,v_calendar_year
                     ,v_event
                     ,v_risk
                     ,v_risk_pubsafety
                     ,v_risk_wforce
                     ,v_risk_reliability
                     ,v_risk_env
                     ,v_risk_npv15
                     ,v_risk_npv15_pubsafety
                     ,v_risk_npv15_wforce
                     ,v_risk_npv15_reliability
                     ,v_risk_npv15_env
                     ,v_risk_fin
                     ,v_risk_npv15_fin
                     );
      END LOOP;
   END LOOP;
   CLOSE risk_bay_csr;
   
   COMMIT;            

   dbms_output.put_line('generate_user_prog_risk_prof End '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

EXCEPTION
   WHEN v_prog_excp THEN
      RAISE;
   WHEN OTHERS THEN
      dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
      dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
      RAISE;

END generate_user_prog_risk_prof;        

PROCEDURE get_zbam_seg_reinf_counts(i_prog_type	IN VARCHAR2
														,i_base_run_id	IN PLS_INTEGER
                                          ,i_program_id	IN PLS_INTEGER
                                          ,i_del_year			IN PLS_INTEGER
                                          ,o_p1_summ		OUT PLS_INTEGER
                                          ,o_p2_summ		OUT PLS_INTEGER
                                          ,o_p3_summ		OUT PLS_INTEGER
                                          ,o_p4_summ		OUT PLS_INTEGER
                                          ,o_p5_summ		OUT PLS_INTEGER
                                          ,o_p6_summ		OUT PLS_INTEGER
                                          ,o_p7_summ		OUT PLS_INTEGER
                                          ,o_p8_summ		OUT PLS_INTEGER
                                          ,o_p9_summ		OUT PLS_INTEGER) IS
--                                          ,o_reinf_counts_csr	OUT sys_refcursor) IS

BEGIN

	IF i_prog_type = 'ZBAM' THEN
      WITH ZBAM AS
      (
      SELECT b.mtzn 
      FROM structools.st_del_program_mtzn_optimised A	LEFT OUTER JOIN 
               structools.st_mtzn b
      ON A.mtzn = b.mtzn_opt
      WHERE A.del_year = i_del_year 
      AND 	A.base_run_id = i_base_run_id 
      AND   A.program_id = i_program_id
      )
      ,
      zbam_rf_selection AS
      (
      SELECT P1_SUMM, P2_SUMM, P3_SUMM, P4_SUMM, P5_SUMM, P6_SUMM, P7_SUMM, P8_SUMM, P9_SUMM
      FROM 	zbam 																			A		LEFT OUTER JOIN 
               structools.st_del_program_zbam_reinf_summ 	b
      ON A.mtzn = b.mtzn
      WHERE b.del_year = i_del_year
      AND 	b.base_run_id = i_base_run_id
      AND 	b.program_id = i_program_id
      )
		SELECT nvl(sum(p1_summ), 0) 	 AS p1_summ
      				,nvl(sum(p2_summ), 0)	AS p2_summ
                  ,nvl(sum(p3_summ), 0)	AS p3_summ
                  ,nvl(sum(p4_summ), 0)	AS p4_summ
                  ,nvl(sum(p5_summ), 0)	AS p5_summ
                  ,nvl(sum(p6_summ), 0)	AS p6_summ
                  ,nvl(sum(p7_summ), 0)	AS p7_summ
                  ,nvl(sum(p8_summ), 0)	AS p8_summ
                  ,nvl(sum(p9_summ), 0) 	AS p9_summ
		INTO     o_p1_summ
      			  ,o_p2_summ	 
                 ,o_p3_summ
                 ,o_p4_summ
                 ,o_p5_summ
                 ,o_p6_summ
                 ,o_p7_summ
                 ,o_p8_summ
                 ,o_p9_summ
      FROM zbam_rf_selection;
      
	ELSIF i_prog_type = 'SEGMENT' THEN   	
      WITH segments AS
      (
      SELECT cons_segment_id 
      FROM structools.st_del_program_seg_optimised
      WHERE del_year = i_del_year
      AND 	  base_run_id = i_base_run_id
      AND    program_id = i_program_id
      ),
      seg_rf_selection AS
      (
      SELECT P1_SUMM, P2_SUMM, P3_SUMM, P4_SUMM, P5_SUMM, P6_SUMM, P7_SUMM, P8_SUMM, P9_SUMM
      FROM segments      														A			LEFT OUTER JOIN 
               structools.st_del_program_seg_reinf_summ b
      ON A.cons_segment_id = b.cons_segment_id
      WHERE b.del_year = i_del_year 
      AND 	b.base_run_id = i_base_run_id
      AND 	b.program_id = i_program_id
      ) 
		SELECT nvl(sum(p1_summ), 0) 	 AS p1_summ
      				,nvl(sum(p2_summ), 0)	AS p2_summ
                  ,nvl(sum(p3_summ), 0)	AS p3_summ
                  ,nvl(sum(p4_summ), 0)	AS p4_summ
                  ,nvl(sum(p5_summ), 0)	AS p5_summ
                  ,nvl(sum(p6_summ), 0)	AS p6_summ
                  ,nvl(sum(p7_summ), 0)	AS p7_summ
                  ,nvl(sum(p8_summ), 0)	AS p8_summ
                  ,nvl(sum(p9_summ), 0) 	AS p9_summ
		INTO     o_p1_summ
      			  ,o_p2_summ	 
                 ,o_p3_summ
                 ,o_p4_summ
                 ,o_p5_summ
                 ,o_p6_summ
                 ,o_p7_summ
                 ,o_p8_summ
                 ,o_p9_summ
      FROM seg_rf_selection;
	END IF;      
   
EXCEPTION
   WHEN v_prog_excp THEN
      RAISE;
   WHEN OTHERS THEN
      dbms_output.put_line('Exception '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
      dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
      RAISE;
      
END get_zbam_seg_reinf_counts;	   /*****************************************************************/
/* Package initialisation                                        */
/*****************************************************************/

BEGIN

   NULL;
   
END reporting;
/
