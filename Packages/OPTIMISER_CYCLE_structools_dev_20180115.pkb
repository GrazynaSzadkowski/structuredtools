CREATE OR REPLACE PACKAGE BODY structoolsapp.optimiser_cycle
AS

   /*****************************************************************************************************************************************/
   /* H6 Changes:                                                                                                                                                                                                                                                          */
   /*    28/11/17 Grazyna Szadkowski   Made changes to the zbam summary procedure to also produce a table of zbam reinforcements and their count by reinforce event_reason.							*/
   /*																	The table is st_del_program_zbam_reinf_summ.                                                                                                                                                       */
   /*    27/11/17 Grazyna Szadkowski   Made changes to cater for pickid suppression.                                                                                                                                                          */
   /*    20/11/17 Grazyna Szadkowski   Created procedure run_summary_for_mtzn_reinf.                                                                                                                                                    */
   /*                                                  Modified procedure run_summary_for_opensolver to cater for reinforcements done in prior years as part of zonal reinforcment program.                        */
   /*                                                  Modified procedure run_summary_for_segments to cater for reinforcements done in prior years as part of zonal reinforcment program.                         */
   /*                                                  Modified procedure create_sniper_program to only pick up REPLACE events.                                                                                                              */
   /*                                                  Modified procedure create_reac_maint_program to cater for reinforcements done in prior years as part of zonal reinforcment program.                          */
   /*    14/08/17 Grazyna Szadkowski   Created by extracting optimiser-cycle related procedures from structured_tools.pkb.                                                                                                    */
   /*****************************************************************************************************************************************/
   

   v_this_year                   PLS_INTEGER := EXTRACT (YEAR FROM SYSDATE);
   v_year_0                      PLS_INTEGER := EXTRACT (YEAR FROM SYSDATE);
   v_null_end_dt                     CONSTANT DATE := TO_DATE('01-01-3000','dd-mm-yyyy');
   v_prog_excp                   EXCEPTION;
   
   v_request_not_valid     EXCEPTION;

   v_parm_scen_id_life_exp          PLS_INTEGER;
   v_parm_scen_id_seg_min_len          PLS_INTEGER;
   v_parm_scen_id_seg_cons_pcnt     PLS_INTEGER;
   v_parm_scen_id_sif_weighting     PLS_INTEGER;
   v_parm_scen_id_seg_rr_doll            PLS_INTEGER;        -- single scenario id applies to the whole set, ie all years
   v_parm_scen_id_eqp_rr_doll            PLS_INTEGER;        -- single scenario id applies to the whole set, ie all years
   v_parm_scen_id_fdr_cat_adj       PLS_INTEGER;
   
   v_cost_key                 VARCHAR2(60);
   TYPE cost_rec_typ          IS RECORD
   (labour_hrs               NUMBER
   ,total_cost                NUMBER
   );
   v_cost_rec              cost_rec_typ;
      
   TYPE cost_tab_typ     IS TABLE OF cost_rec_typ INDEX BY VARCHAR2(60);
   /* table of costs, indexed by a concatenation of various factors, depending on eqp type */   
   v_cost_tab            cost_tab_typ;
   
   TYPE risk_rec_typ IS RECORD (
    pick_id        VARCHAR2(10)
   ,yr0            FLOAT(126)
   ,yr1            FLOAT(126)
   ,yr2            FLOAT(126)
   ,yr3            FLOAT(126)
   ,yr4            FLOAT(126)
   ,yr5            FLOAT(126)
   ,yr6            FLOAT(126)
   ,yr7            FLOAT(126)
   ,yr8            FLOAT(126)
   ,yr9            FLOAT(126)
   ,yr10            FLOAT(126)
   ,yr11            FLOAT(126)
   ,yr12            FLOAT(126)
   ,yr13            FLOAT(126)
   ,yr14            FLOAT(126)
   ,yr15            FLOAT(126)
   ,yr16            FLOAT(126)
   ,yr17            FLOAT(126)
   ,yr18            FLOAT(126)
   ,yr19            FLOAT(126)
   ,yr20            FLOAT(126)
   ,yr21            FLOAT(126)
   ,yr22            FLOAT(126)
   ,yr23            FLOAT(126)
   ,yr24            FLOAT(126)
   ,yr25            FLOAT(126)
   ,yr26            FLOAT(126)
   ,yr27            FLOAT(126)
   ,yr28            FLOAT(126)
   ,yr29            FLOAT(126)
   ,yr30            FLOAT(126)
   ,yr31            FLOAT(126)
   ,yr32            FLOAT(126)
   ,yr33            FLOAT(126)
   ,yr34            FLOAT(126)
   ,yr35            FLOAT(126)
   ,yr36            FLOAT(126)
   ,yr37            FLOAT(126)
   ,yr38            FLOAT(126)
   ,yr39            FLOAT(126)
   ,yr40            FLOAT(126)
   ,yr41            FLOAT(126)
   ,yr42            FLOAT(126)
   ,yr43            FLOAT(126)
   ,yr44            FLOAT(126)
   ,yr45            FLOAT(126)
   ,yr46            FLOAT(126)
   ,yr47            FLOAT(126)
   ,yr48            FLOAT(126)
   ,yr49            FLOAT(126)
   ,yr50            FLOAT(126)
   ,fire_frac       FLOAT(126) 
   ,eshock_frac     FLOAT(126)
   ,wforce_frac     FLOAT(126)
   ,reliability_frac FLOAT(126)
   ,physimp_frac     FLOAT(126)
   ,env_frac         FLOAT(126)
   ,fin_frac         FLOAT(126)
   );
      
   TYPE risk_tab_typ             IS TABLE OF FLOAT(126);

   
   v_risk_pubsafety_sif        FLOAT(126);
   v_risk_wforce_sif           FLOAT(126);
   v_risk_reliability_sif      FLOAT(126);
   v_risk_env_sif              FLOAT(126);
   v_risk_fin_sif              FLOAT(126);

   v_fdr_cat_sif_tab                   structoolsapp.common_procs.fdr_cat_sif_tab_typ;


   PROCEDURE reload_temp_strategy_tables (i_base_run_id IN PLS_INTEGER
                                         ,i_strategy_run_stage IN PLS_INTEGER) IS

      v_sql             VARCHAR2(1000);

   BEGIN   
        
      structoolsapp.common_procs.get_parameter_scenarios(i_base_run_id, v_parm_scen_id_life_exp, v_parm_scen_id_seg_min_len,  v_parm_scen_id_seg_cons_pcnt, v_parm_scen_id_sif_weighting
                                                               , v_parm_scen_id_seg_rr_doll,  v_parm_scen_id_eqp_rr_doll, v_parm_scen_id_fdr_cat_adj);   
                                                               
      structools.schema_utils.truncate_table('ST_TEMP_STRATEGY_RUN');
      structools.schema_utils.truncate_table('ST_TEMP_BAY_STRATEGY_RUN');
      
      IF i_strategy_run_stage = 1 THEN
         INSERT /*+ APPEND */ 
         INTO structools.st_temp_strategy_run
                 (RUN_ID                    
                 ,VIC_ID                    
                 ,PICK_ID                   
                 ,EQUIP_GRP_ID              
                 ,EVENT                     
                 ,RISK                      
                 ,RISK_NOACTION             
--                 ,REMAINING_VAL             
                 ,labour_hrs              
                 ,CAPEX                     
--                 ,OPEX                      
                 ,AGE                       
                 ,CALENDAR_YEAR             
--              ,VARIATION                 
                 ,EVENT_REASON              
                 ,RISK_REDUCTION            
                 ,MAINT_ZONE_NAM            
--                 ,CONDUCTOR_STRATEGY        
                 ,RISK_NPV15                
                 ,RISK_NOACTION_NPV15       
                 ,RISK_REDUCTION_NPV15      
                 ,RISK_NPV15_SIF            
                 ,RISK_NOACTION_NPV15_SIF   
                 ,RISK_REDUCTION_NPV15_SIF  
                 ,FAILURE_PROB              
                 ,SCHEDULED         
                 ,cons_Segment_id
                 ,mtzn_grp
                 ,segment_rebuild_yr
                 ,fire_risk_zone_cls
                 ,wood_type
                 ,fdr_cat
                 ,rr_npv15_sif_fdr_cat_adj
                 ,priority_ind
                 ,part_cde
                 ,part_cde2
                 ,pickid_suppressed
                  )
            WITH pole_repl AS
            (
            SELECT pick_id
                  ,calendar_year
            FROM structools.st_strategy_run
            WHERE run_id = i_base_run_id
            AND event = 'REPLACE'
            AND equip_grp_id = 'PWOD'
            AND part_cde = ' '
            )
            ,
            part_repl AS
            (      
            SELECT DISTINCT pick_id
                  ,calendar_year
            FROM structools.st_strategy_run
            WHERE run_id = i_base_run_id
            AND event = 'REPLACE'
            AND equip_grp_id IN ('PWOD','PAUS')
            AND part_cde > ' '
            MINUS
            SELECT * FROM pole_repl
            )
            ,
            all_pickids AS
            (
            SELECT *
            FROM structools.st_strategy_run
            WHERE run_id = i_base_run_id
            AND part_cde = ' '
            UNION
            SELECT A.*
            FROM structools.st_strategy_run       A  INNER JOIN
                 part_repl             b
            ON A.pick_id = b.pick_id
            AND A.calendar_year = b.calendar_year
            WHERE run_id = i_base_run_id
            )
            SELECT  RUN_ID                    
                 ,VIC_ID                    
                 ,A.PICK_ID                   
                 ,A.EQUIP_GRP_ID              
                 ,EVENT                     
                 ,RISK                      
                 ,RISK_NOACTION             
--                 ,REMAINING_VAL             
                 ,A.labour_hrs              
                 ,CAPEX                     
--                 ,OPEX                      
                 ,AGE                       
                 ,CALENDAR_YEAR             
--              ,VARIATION                 
                 ,EVENT_REASON              
                 ,RISK_REDUCTION            
                 ,A.MAINT_ZONE_NAM            
--                 ,CONDUCTOR_STRATEGY        
                 ,RISK_NPV15                
                 ,RISK_NOACTION_NPV15       
                 ,RISK_REDUCTION_NPV15      
                    ,RISK_NPV15_SIF            
                    ,RISK_NOACTION_NPV15_SIF   
                    ,RISK_REDUCTION_NPV15_SIF  
                    ,FAILURE_PROB              
                    ,SCHEDULED         
                    ,NULL                                    -- cons_segment_id
                    ,A.maint_zone_nam      AS         mtzn_grp
                    ,NULL                                    --segment_rebuild_yr
                    ,CASE
                        WHEN b.fire_risk_zone_cls = 'UNKNOWN'  THEN ' '
                        WHEN b.fire_risk_zone_cls IS NULL      THEN ' '
                        ELSE                                         b.fire_risk_zone_cls
                    END                      AS fire_risk_zone_cls 
                    ,b.wood_type
                    ,CASE 
                            WHEN UPPER(c.scnrrr_fdr_cat_nam) = 'none'            THEN 'SHORTRURAL'
                        WHEN c.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                        ELSE                                                            UPPER(c.scnrrr_fdr_cat_nam)
                    END                                                            AS fdr_cat
                    ,A.rr_npv15_sif_fdr_cat_adj
                    ,A.priority_ind
                    ,A.part_cde
                    ,CASE
                     WHEN A.part_cde = 'ASTAY' OR A.part_cde = 'GSTAY' OR A.part_cde = 'OSTAY' OR A.part_cde = 'AGOST'  THEN  'STAY'
                     ELSE                                                                       A.part_cde
                    END             AS part_cde2
                    ,A.pickid_suppressed
             FROM all_pickids                   A INNER JOIN
                  structools.st_equipment          b
             ON A.pick_id = b.pick_id      
             AND A.part_cde = b.part_cde      
                                                   LEFT OUTER JOIN
                  structools.st_hv_feeder          c
             ON b.feeder_id =        c.hv_fdr_id                                                                         
          ;            
          INSERT /*+ APPEND */
          INTO structools.st_temp_bay_strategy_run
             		 (RUN_ID                    
                    ,VIC_ID                    
                    ,PICK_ID                   
                    ,EQUIP_GRP_ID              
                    ,EVENT                     
                    ,RISK                      
                    ,RISK_NOACTION             
--                    ,REMAINING_VAL             
                    ,LABOUR_HRS              
                    ,CAPEX                     
--                    ,OPEX                      
                    ,AGE                       
                    ,CALENDAR_YEAR             
                    ,EVENT_REASON              
                    ,RISK_REDUCTION            
--                    ,PROT_ZONE_ID              
--                    ,PROT_ZONE_LEN             
                    ,SPAN_LEN                  
                    ,HVCO_CNT                  
                    ,LVCO_CNT                  
                    ,HVSP_CNT                  
                    ,MAINT_ZONE_NAM            
                    ,RISK_NPV15                
                    ,RISK_NOACTION_NPV15       
                    ,RISK_REDUCTION_NPV15      
                    ,RISK_NPV15_SIF            
                    ,RISK_NOACTION_NPV15_SIF   
                    ,RISK_REDUCTION_NPV15_SIF  
                    ,SEGMENT_ID                
                    ,COND_EQP_TYP              
                    ,CONS_SEGMENT_ID           
                    ,SCHEDULED                 
                    ,mtzn_grp               
                    ,segment_rebuild_yr
                    ,fire_risk_zone_cls
                    ,fdr_cat
                    ,rr_npv15_sif_fdr_cat_adj
                    ,priority_ind
                    ,pickid_Suppressed
						)               
             SELECT  RUN_ID                    
                    ,VIC_ID                    
                    ,A.PICK_ID                   
                    ,EQUIP_GRP_ID              
                    ,EVENT                     
                    ,RISK                      
                    ,RISK_NOACTION             
--                    ,REMAINING_VAL             
                    ,LABOUR_HRS              
                    ,CAPEX                     
--                    ,OPEX                      
                    ,AGE                       
                    ,CALENDAR_YEAR             
                    ,EVENT_REASON              
                    ,RISK_REDUCTION            
--                    ,A.PROT_ZONE_ID              
--                    ,PROT_ZONE_LEN             
                    ,SPAN_LEN                  
                    ,HVCO_CNT                  
                    ,LVCO_CNT                  
                    ,HVSP_CNT                  
                    ,MAINT_ZONE_NAM            
                    ,RISK_NPV15                
                    ,RISK_NOACTION_NPV15       
                    ,RISK_REDUCTION_NPV15      
                    ,RISK_NPV15_SIF            
                    ,RISK_NOACTION_NPV15_SIF   
                    ,RISK_REDUCTION_NPV15_SIF  
                    ,A.SEGMENT_ID                
                    ,COND_EQP_TYP              
                    ,CONS_SEGMENT_ID           
                    ,SCHEDULED                 
                    ,maint_zone_nam      AS mtzn_grp               
                    ,NULL                        -- segmnet_rebuild_yr
                    ,CASE
                        WHEN b.fire_risk_zone_cls = 'UNKNOWN'  THEN ' '
                        WHEN b.fire_risk_zone_cls IS NULL      THEN ' '
                        ELSE                                         b.fire_risk_zone_cls
                    END                      AS fire_risk_zone_cls 
                    ,CASE 
                            WHEN UPPER(c.scnrrr_fdr_cat_nam) = 'NONE'            THEN 'SHORTRURAL'
                        WHEN c.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                        ELSE                                                            UPPER(c.scnrrr_fdr_cat_nam)
                    END                                                            AS fdr_cat
                    ,A.rr_npv15_sif_fdr_cat_adj
                    ,A.priority_ind
                    ,A.pickid_Suppressed
             FROM structools.st_bay_strategy_run  A INNER JOIN
                  structools.st_bay_equipment         b
             ON A.pick_id = b.pick_id   
                                                   LEFT OUTER JOIN
                  structools.st_hv_feeder          c
             ON b.feeder_id =        c.hv_fdr_id                                                                         
             WHERE run_id = i_base_run_id
          ;            
      ELSIF i_strategy_run_stage = 2 THEN
         INSERT /*+ APPEND */
         INTO structools.st_temp_strategy_run
                 (RUN_ID                    
                 ,VIC_ID                    
                 ,PICK_ID                   
                 ,EQUIP_GRP_ID              
                 ,EVENT                     
                 ,RISK                      
                 ,RISK_NOACTION             
--                 ,REMAINING_VAL             
                 ,labour_hrs              
                 ,CAPEX                     
--                 ,OPEX                      
                 ,AGE                       
                 ,CALENDAR_YEAR             
--              ,VARIATION                 
                 ,EVENT_REASON              
                 ,RISK_REDUCTION            
                 ,MAINT_ZONE_NAM            
--                 ,CONDUCTOR_STRATEGY        
                 ,RISK_NPV15                
                 ,RISK_NOACTION_NPV15       
                 ,RISK_REDUCTION_NPV15      
                 ,RISK_NPV15_SIF            
                 ,RISK_NOACTION_NPV15_SIF   
                 ,RISK_REDUCTION_NPV15_SIF  
                 ,FAILURE_PROB              
                 ,SCHEDULED         
                 ,cons_Segment_id
                 ,mtzn_grp
                 ,segment_rebuild_yr
                 ,fire_risk_zone_cls
                 ,wood_type
                 ,fdr_cat
                 ,rr_npv15_sif_fdr_cat_adj
                 ,priority_ind
                 ,part_cde
                 ,part_cde2
                 ,pickid_suppressed
                  )
            WITH pole_repl AS
            (
            SELECT pick_id
                  ,calendar_year
            FROM structools.st_strategy_run_rb_tmp
            WHERE run_id = i_base_run_id
            AND event = 'REPLACE'
            AND equip_grp_id = 'PWOD'
            AND part_cde = ' '
            )
            ,
            part_repl AS
            (      
            SELECT DISTINCT pick_id
                  ,calendar_year
            FROM structools.st_strategy_run_rb_tmp
            WHERE run_id = i_base_run_id
            AND event = 'REPLACE'
            AND equip_grp_id IN ('PWOD','PAUS')
            AND part_cde > ' '
            MINUS
            SELECT * FROM pole_repl
            )
            ,
            all_pickids AS
            (
            SELECT *
            FROM structools.st_strategy_run_rb_tmp
            WHERE run_id = i_base_run_id
            AND part_cde = ' '
            UNION
            SELECT A.*
            FROM structools.st_strategy_run_rb_tmp       A  INNER JOIN
                 part_repl             b
            ON A.pick_id = b.pick_id
            AND A.calendar_year = b.calendar_year
            WHERE run_id = i_base_run_id
            )
            SELECT  RUN_ID                    
                 ,VIC_ID                    
                 ,A.PICK_ID                   
                 ,A.EQUIP_GRP_ID              
                 ,EVENT                     
                 ,RISK                      
                 ,RISK_NOACTION             
--                 ,REMAINING_VAL             
                 ,A.labour_hrs              
                 ,CAPEX                     
--                 ,OPEX                      
                 ,AGE                       
                 ,CALENDAR_YEAR             
--                 ,VARIATION                 
                 ,EVENT_REASON              
                 ,RISK_REDUCTION            
                 ,A.MAINT_ZONE_NAM            
--                 ,CONDUCTOR_STRATEGY        
                 ,RISK_NPV15                
                 ,RISK_NOACTION_NPV15       
                 ,RISK_REDUCTION_NPV15      
                 ,RISK_NPV15_SIF            
                 ,RISK_NOACTION_NPV15_SIF   
                 ,RISK_REDUCTION_NPV15_SIF  
                 ,FAILURE_PROB              
                 ,SCHEDULED                 
                 ,CONS_SEGMENT_ID           
                 ,CASE 
                   WHEN cons_segment_mtzn IS NULL            THEN  A.maint_zone_nam
                   ELSE                                            cons_segment_mtzn
                  END                    AS mtzn_grp
                 ,SEGMENT_REBUILD_YR        
                 ,CASE
                     WHEN b.fire_risk_zone_cls = 'UNKNOWN'  THEN ' '
                     WHEN b.fire_risk_zone_cls IS NULL      THEN ' '
                     ELSE                                         b.fire_risk_zone_cls
                 END                      AS fire_risk_zone_cls 
                 ,b.wood_type
                 ,CASE
                     WHEN UPPER(c.scnrrr_fdr_cat_nam) = 'NONE'            THEN 'SHORTRURAL'
                     WHEN c.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                     ELSE                                                            UPPER(c.scnrrr_fdr_cat_nam)
                 END                                                            AS fdr_cat
                 ,A.rr_npv15_sif_fdr_cat_adj
                 ,A.priority_ind
                 ,A.part_cde
                 ,CASE
                  WHEN A.part_cde = 'ASTAY' OR A.part_cde = 'GSTAY' OR A.part_cde = 'OSTAY' OR A.part_cde = 'AGOST'  THEN  'STAY'
                  ELSE                                                                       A.part_cde
                 END             AS part_cde2
                 ,A.pickid_suppressed
            FROM all_pickids              A INNER JOIN
               structools.st_equipment       b
            ON A.pick_id = b.pick_id
            AND A.part_cde = b.part_cde            
                                                LEFT OUTER JOIN
               structools.st_hv_feeder          c
            ON b.feeder_id =        c.hv_fdr_id                                                                         
         ;
         INSERT /*+ APPEND */
         INTO structools.st_temp_bay_strategy_run
             		 (RUN_ID                    
                    ,VIC_ID                    
                    ,PICK_ID                   
                    ,EQUIP_GRP_ID              
                    ,EVENT                     
                    ,RISK                      
                    ,RISK_NOACTION             
--                    ,REMAINING_VAL             
                    ,LABOUR_HRS              
                    ,CAPEX                     
--                    ,OPEX                      
                    ,AGE                       
                    ,CALENDAR_YEAR             
                    ,EVENT_REASON              
                    ,RISK_REDUCTION            
--                    ,PROT_ZONE_ID              
--                    ,PROT_ZONE_LEN             
                    ,SPAN_LEN                  
                    ,HVCO_CNT                  
                    ,LVCO_CNT                  
                    ,HVSP_CNT                  
                    ,MAINT_ZONE_NAM            
                    ,RISK_NPV15                
                    ,RISK_NOACTION_NPV15       
                    ,RISK_REDUCTION_NPV15      
                    ,RISK_NPV15_SIF            
                    ,RISK_NOACTION_NPV15_SIF   
                    ,RISK_REDUCTION_NPV15_SIF  
                    ,SEGMENT_ID                
                    ,COND_EQP_TYP              
                    ,CONS_SEGMENT_ID           
                    ,SCHEDULED                 
                    ,mtzn_grp               
                    ,segment_rebuild_yr
                    ,fire_risk_zone_cls
                    ,fdr_cat
                    ,rr_npv15_sif_fdr_cat_adj
                    ,priority_ind
                    ,pickid_Suppressed
						)               
            SELECT  RUN_ID                    
                 ,VIC_ID                    
                 ,A.PICK_ID                   
                 ,EQUIP_GRP_ID              
                 ,EVENT                     
                 ,RISK                      
                 ,RISK_NOACTION             
--                 ,REMAINING_VAL             
                 ,LABOUR_HRS              
                 ,CAPEX                     
--                 ,OPEX                      
                 ,AGE                       
                 ,CALENDAR_YEAR             
                 ,EVENT_REASON              
                 ,RISK_REDUCTION            
--                 ,A.PROT_ZONE_ID              
--                 ,PROT_ZONE_LEN             
                 ,SPAN_LEN                  
                 ,HVCO_CNT                  
                 ,LVCO_CNT                  
                 ,HVSP_CNT                  
                 ,MAINT_ZONE_NAM            
                 ,RISK_NPV15                
                 ,RISK_NOACTION_NPV15       
                 ,RISK_REDUCTION_NPV15      
                 ,RISK_NPV15_SIF            
                 ,RISK_NOACTION_NPV15_SIF   
                 ,RISK_REDUCTION_NPV15_SIF  
                 ,A.SEGMENT_ID                
                 ,COND_EQP_TYP              
                 ,CONS_SEGMENT_ID           
                 ,SCHEDULED                 
                 ,CASE 
                   WHEN cons_segment_mtzn IS NULL            THEN  maint_zone_nam
                   ELSE                                            cons_segment_mtzn
                  END                      AS mtzn_grp
                 ,SEGMENT_REBUILD_YR       
                 ,CASE
                     WHEN b.fire_risk_zone_cls = 'UNKNOWN'  THEN ' '
                     WHEN b.fire_risk_zone_cls IS NULL      THEN ' '
                     ELSE                                         b.fire_risk_zone_cls
                 END                      AS fire_risk_zone_cls 
                 ,CASE
                         WHEN UPPER(c.scnrrr_fdr_cat_nam) = 'NONE'            THEN 'SHORTRURAL'
                     WHEN c.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                     ELSE                                                            UPPER(c.scnrrr_fdr_cat_nam)
                 END                                                            AS fdr_cat
                 ,A.rr_npv15_sif_fdr_cat_adj
                 ,A.priority_ind
                 ,A.pickid_suppressed
            FROM structools.st_bay_strategy_run_rb_tmp  A INNER JOIN
               structools.st_bay_equipment         b
            ON A.pick_id = b.pick_id   
                                                LEFT OUTER JOIN
               structools.st_hv_feeder          c
            ON b.feeder_id =        c.hv_fdr_id                                                                         
            WHERE run_id = i_base_run_id
         ;            
      ELSIF i_strategy_run_stage = 3 THEN
         INSERT /*+ APPEND */
         INTO structools.st_temp_strategy_run
                 (RUN_ID                    
                 ,VIC_ID                    
                 ,PICK_ID                   
                 ,EQUIP_GRP_ID              
                 ,EVENT                     
                 ,RISK                      
                 ,RISK_NOACTION             
--                 ,REMAINING_VAL             
                 ,labour_hrs              
                 ,CAPEX                     
--                 ,OPEX                      
                 ,AGE                       
                 ,CALENDAR_YEAR             
--              ,VARIATION                 
                 ,EVENT_REASON              
                 ,RISK_REDUCTION            
                 ,MAINT_ZONE_NAM            
--                 ,CONDUCTOR_STRATEGY        
                 ,RISK_NPV15                
                 ,RISK_NOACTION_NPV15       
                 ,RISK_REDUCTION_NPV15      
                 ,RISK_NPV15_SIF            
                 ,RISK_NOACTION_NPV15_SIF   
                 ,RISK_REDUCTION_NPV15_SIF  
                 ,FAILURE_PROB              
                 ,SCHEDULED         
                 ,cons_Segment_id
                 ,mtzn_grp
                 ,segment_rebuild_yr
                 ,fire_risk_zone_cls
                 ,wood_type
                 ,fdr_cat
                 ,rr_npv15_sif_fdr_cat_adj
                 ,priority_ind
                 ,part_cde
                 ,part_cde2
                 ,pickid_suppressed
                  )
            WITH pole_repl AS
            (
            SELECT pick_id
                  ,calendar_year
            FROM structools.st_strategy_run_rb
            WHERE run_id = i_base_run_id
            AND event = 'REPLACE'
            AND equip_grp_id = 'PWOD'
            AND part_cde = ' '
            )
            ,
            part_repl AS
            (      
            SELECT DISTINCT pick_id
                  ,calendar_year
            FROM structools.st_strategy_run_rb
            WHERE run_id = i_base_run_id
            AND event = 'REPLACE'
            AND equip_grp_id IN ('PWOD','PAUS')
            AND part_cde > ' '
            MINUS
            SELECT * FROM pole_repl
            )
            ,
            all_pickids AS
            (
            SELECT *
            FROM structools.st_strategy_run_rb
            WHERE run_id = i_base_run_id
            AND part_cde = ' '
            UNION
            SELECT A.*
            FROM structools.st_strategy_run_rb       A  INNER JOIN
                 part_repl             b
            ON A.pick_id = b.pick_id
            AND A.calendar_year = b.calendar_year
            WHERE run_id = i_base_run_id
            )
            SELECT  RUN_ID                    
                 ,VIC_ID                    
                 ,A.PICK_ID                   
                 ,A.EQUIP_GRP_ID              
                 ,EVENT                     
                 ,RISK                      
                 ,RISK_NOACTION             
--                 ,REMAINING_VAL             
                 ,A.labour_hrs              
                 ,CAPEX                     
--                 ,OPEX                      
                 ,AGE                       
                 ,CALENDAR_YEAR             
--                 ,VARIATION                 
                 ,EVENT_REASON              
                 ,RISK_REDUCTION            
                 ,A.MAINT_ZONE_NAM            
--                 ,CONDUCTOR_STRATEGY        
                 ,RISK_NPV15                
                 ,RISK_NOACTION_NPV15       
                 ,RISK_REDUCTION_NPV15      
                 ,RISK_NPV15_SIF            
                 ,RISK_NOACTION_NPV15_SIF   
                 ,RISK_REDUCTION_NPV15_SIF  
                 ,FAILURE_PROB              
                 ,SCHEDULED                 
                 ,CONS_SEGMENT_ID           
                 ,CASE 
                   WHEN cons_segment_mtzn IS NULL            THEN  A.maint_zone_nam
                   ELSE                                            cons_segment_mtzn
                  END                    AS mtzn_grp
                 ,SEGMENT_REBUILD_YR        
                 ,CASE
                     WHEN b.fire_risk_zone_cls = 'UNKNOWN'  THEN ' '
                     WHEN b.fire_risk_zone_cls IS NULL      THEN ' '
                     ELSE                                         b.fire_risk_zone_cls
                 END                      AS fire_risk_zone_cls 
                 ,b.wood_type
                 ,CASE
                     WHEN UPPER(c.scnrrr_fdr_cat_nam) = 'NONE'            THEN 'SHORTRURAL'
                     WHEN c.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                     ELSE                                                            UPPER(c.scnrrr_fdr_cat_nam)
                 END                                                            AS fdr_cat
                 ,A.rr_npv15_sif_fdr_cat_adj
                 ,A.priority_ind
                 ,A.part_cde
                 ,CASE
                     WHEN A.part_cde = 'ASTAY' OR A.part_cde = 'GSTAY' OR A.part_cde = 'OSTAY' OR A.part_cde = 'AGOST'  THEN  'STAY'
                     ELSE                                                                       A.part_cde
                 END             AS part_cde2
                 ,A.pickid_suppressed
            FROM all_pickids                A INNER JOIN
               structools.st_equipment       b
            ON A.pick_id = b.pick_id       
            AND A.part_cde = b.part_cde     
                                                LEFT OUTER JOIN
               structools.st_hv_feeder          c
            ON b.feeder_id =        c.hv_fdr_id                                                                         
         ;
         INSERT /*+ APPEND */
         INTO structools.st_temp_bay_strategy_run
             		 (RUN_ID                    
                    ,VIC_ID                    
                    ,PICK_ID                   
                    ,EQUIP_GRP_ID              
                    ,EVENT                     
                    ,RISK                      
                    ,RISK_NOACTION             
--                    ,REMAINING_VAL             
                    ,LABOUR_HRS              
                    ,CAPEX                     
--                    ,OPEX                      
                    ,AGE                       
                    ,CALENDAR_YEAR             
                    ,EVENT_REASON              
                    ,RISK_REDUCTION            
--                    ,PROT_ZONE_ID              
--                    ,PROT_ZONE_LEN             
                    ,SPAN_LEN                  
                    ,HVCO_CNT                  
                    ,LVCO_CNT                  
                    ,HVSP_CNT                  
                    ,MAINT_ZONE_NAM            
                    ,RISK_NPV15                
                    ,RISK_NOACTION_NPV15       
                    ,RISK_REDUCTION_NPV15      
                    ,RISK_NPV15_SIF            
                    ,RISK_NOACTION_NPV15_SIF   
                    ,RISK_REDUCTION_NPV15_SIF  
                    ,SEGMENT_ID                
                    ,COND_EQP_TYP              
                    ,CONS_SEGMENT_ID           
                    ,SCHEDULED                 
                    ,mtzn_grp               
                    ,segment_rebuild_yr
                    ,fire_risk_zone_cls
                    ,fdr_cat
                    ,rr_npv15_sif_fdr_cat_adj
                    ,priority_ind
                    ,pickid_Suppressed
						)               
            SELECT  RUN_ID                    
                 ,VIC_ID                    
                 ,A.PICK_ID                   
                 ,EQUIP_GRP_ID              
                 ,EVENT                     
                 ,RISK                      
                 ,RISK_NOACTION             
--                 ,REMAINING_VAL             
                 ,LABOUR_HRS              
                 ,CAPEX                     
--                 ,OPEX                      
                 ,AGE                       
                 ,CALENDAR_YEAR             
                 ,EVENT_REASON              
                 ,RISK_REDUCTION            
--                 ,A.PROT_ZONE_ID              
--                 ,PROT_ZONE_LEN             
                 ,SPAN_LEN                  
                 ,HVCO_CNT                  
                 ,LVCO_CNT                  
                 ,HVSP_CNT                  
                 ,MAINT_ZONE_NAM            
                 ,RISK_NPV15                
                 ,RISK_NOACTION_NPV15       
                 ,RISK_REDUCTION_NPV15      
                 ,RISK_NPV15_SIF            
                 ,RISK_NOACTION_NPV15_SIF   
                 ,RISK_REDUCTION_NPV15_SIF  
                 ,A.SEGMENT_ID                
                 ,COND_EQP_TYP              
                 ,CONS_SEGMENT_ID           
                 ,SCHEDULED                 
                 ,CASE 
                   WHEN cons_segment_mtzn IS NULL            THEN  maint_zone_nam
                   ELSE                                            cons_segment_mtzn
                  END                      AS mtzn_grp
                 ,SEGMENT_REBUILD_YR       
                 ,CASE
                     WHEN b.fire_risk_zone_cls = 'UNKNOWN'  THEN ' '
                     WHEN b.fire_risk_zone_cls IS NULL      THEN ' '
                     ELSE                                         b.fire_risk_zone_cls
                 END                      AS fire_risk_zone_cls 
                 ,CASE
                         WHEN UPPER(c.scnrrr_fdr_cat_nam) = 'NONE'            THEN 'SHORTRURAL'
                     WHEN c.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                     ELSE                                                            UPPER(c.scnrrr_fdr_cat_nam)
                 END                                                            AS fdr_cat
                 ,A.rr_npv15_sif_fdr_cat_adj
                 ,A.priority_ind
                 ,A.pickid_suppressed
            FROM structools.st_bay_strategy_run_rb  A INNER JOIN
               structools.st_bay_equipment         b
            ON A.pick_id = b.pick_id   
                                                LEFT OUTER JOIN
               structools.st_hv_feeder          c
            ON b.feeder_id =        c.hv_fdr_id                                                                         
            WHERE run_id = i_base_run_id
         ;            
      ELSIF i_strategy_run_stage = 4 THEN
         INSERT /*+ APPEND */  
         INTO structools.st_temp_strategy_run
                 (RUN_ID                    
                 ,VIC_ID                    
                 ,PICK_ID                   
                 ,EQUIP_GRP_ID              
                 ,EVENT                     
                 ,RISK                      
                 ,RISK_NOACTION             
--                 ,REMAINING_VAL             
                 ,labour_hrs              
                 ,CAPEX                     
--                 ,OPEX                      
                 ,AGE                       
                 ,CALENDAR_YEAR             
--              ,VARIATION                 
                 ,EVENT_REASON              
                 ,RISK_REDUCTION            
                 ,MAINT_ZONE_NAM            
--                 ,CONDUCTOR_STRATEGY        
                 ,RISK_NPV15                
                 ,RISK_NOACTION_NPV15       
                 ,RISK_REDUCTION_NPV15      
                 ,RISK_NPV15_SIF            
                 ,RISK_NOACTION_NPV15_SIF   
                 ,RISK_REDUCTION_NPV15_SIF  
                 ,FAILURE_PROB              
                 ,SCHEDULED         
                 ,cons_Segment_id
                 ,mtzn_grp
                 ,segment_rebuild_yr
                 ,fire_risk_zone_cls
                 ,wood_type
                 ,fdr_cat
                 ,rr_npv15_sif_fdr_cat_adj
                 ,priority_ind
                 ,part_cde
                 ,part_cde2
                 ,pickid_suppressed
                  )
            WITH pole_repl AS
            (
            SELECT pick_id
                  ,calendar_year
            FROM structools.st_strategy_run_rb2
            WHERE run_id = i_base_run_id
            AND event = 'REPLACE'
            AND equip_grp_id = 'PWOD'
            AND part_cde = ' '
            )
            ,
            part_repl AS
            (      
            SELECT DISTINCT pick_id
                  ,calendar_year
            FROM structools.st_strategy_run_rb2
            WHERE run_id = i_base_run_id
            AND event = 'REPLACE'
            AND equip_grp_id IN ('PWOD','PAUS')
            AND part_cde > ' '
            MINUS
            SELECT * FROM pole_repl
            )
            ,
            all_pickids AS
            (
            SELECT *
            FROM structools.st_strategy_run_rb2
            WHERE run_id = i_base_run_id
            AND part_cde = ' '
            UNION
            SELECT A.*
            FROM structools.st_strategy_run_rb2       A  INNER JOIN
                 part_repl             b
            ON A.pick_id = b.pick_id
            AND A.calendar_year = b.calendar_year
            WHERE run_id = i_base_run_id
            )
            SELECT  RUN_ID                    
                 ,VIC_ID                    
                 ,A.PICK_ID                   
                 ,A.EQUIP_GRP_ID              
                 ,EVENT                     
                 ,RISK                      
                 ,RISK_NOACTION             
--                 ,REMAINING_VAL             
                 ,A.labour_hrs              
                 ,CAPEX                     
--                 ,OPEX                      
                 ,AGE                       
                 ,CALENDAR_YEAR             
--                 ,VARIATION                 
                 ,EVENT_REASON              
                 ,RISK_REDUCTION            
                 ,A.MAINT_ZONE_NAM            
--                 ,CONDUCTOR_STRATEGY        
                 ,RISK_NPV15                
                 ,RISK_NOACTION_NPV15       
                 ,RISK_REDUCTION_NPV15      
                 ,RISK_NPV15_SIF            
                 ,RISK_NOACTION_NPV15_SIF   
                 ,RISK_REDUCTION_NPV15_SIF  
                 ,FAILURE_PROB              
                 ,SCHEDULED                 
                 ,CONS_SEGMENT_ID           
                 ,CASE 
                   WHEN cons_segment_mtzn IS NULL            THEN  A.maint_zone_nam
                   ELSE                                            cons_segment_mtzn
                  END                    AS mtzn_grp
                 ,SEGMENT_REBUILD_YR        
                 ,CASE
                     WHEN b.fire_risk_zone_cls = 'UNKNOWN'  THEN ' '
                     WHEN b.fire_risk_zone_cls IS NULL      THEN ' '
                     ELSE                                         b.fire_risk_zone_cls
                 END                      AS fire_risk_zone_cls 
                 ,b.wood_type
                 ,CASE
                     WHEN UPPER(c.scnrrr_fdr_cat_nam) = 'NONE'            THEN 'SHORTRURAL'
                     WHEN c.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                     ELSE                                                            UPPER(c.scnrrr_fdr_cat_nam)
                 END                                                            AS fdr_cat
                 ,A.rr_npv15_sif_fdr_cat_adj
                 ,A.priority_ind
                 ,A.part_cde
                 ,CASE
                     WHEN A.part_cde = 'ASTAY' OR A.part_cde = 'GSTAY' OR A.part_cde = 'OSTAY' OR A.part_cde = 'AGOST'  THEN  'STAY'
                     ELSE                                                                       A.part_cde
                 END             AS part_cde2
                 ,A.pickid_suppressed
            FROM all_pickids                A INNER JOIN
               structools.st_equipment       b
            ON A.pick_id = b.pick_id       
            AND A.part_cde = b.part_cde     
                                                LEFT OUTER JOIN
               structools.st_hv_feeder          c
            ON b.feeder_id =        c.hv_fdr_id                                                                         
         ;
         INSERT /*+ APPEND */
         INTO structools.st_temp_bay_strategy_run
             		 (RUN_ID                    
                    ,VIC_ID                    
                    ,PICK_ID                   
                    ,EQUIP_GRP_ID              
                    ,EVENT                     
                    ,RISK                      
                    ,RISK_NOACTION             
--                    ,REMAINING_VAL             
                    ,LABOUR_HRS              
                    ,CAPEX                     
--                    ,OPEX                      
                    ,AGE                       
                    ,CALENDAR_YEAR             
                    ,EVENT_REASON              
                    ,RISK_REDUCTION            
--                    ,PROT_ZONE_ID              
--                    ,PROT_ZONE_LEN             
                    ,SPAN_LEN                  
                    ,HVCO_CNT                  
                    ,LVCO_CNT                  
                    ,HVSP_CNT                  
                    ,MAINT_ZONE_NAM            
                    ,RISK_NPV15                
                    ,RISK_NOACTION_NPV15       
                    ,RISK_REDUCTION_NPV15      
                    ,RISK_NPV15_SIF            
                    ,RISK_NOACTION_NPV15_SIF   
                    ,RISK_REDUCTION_NPV15_SIF  
                    ,SEGMENT_ID                
                    ,COND_EQP_TYP              
                    ,CONS_SEGMENT_ID           
                    ,SCHEDULED                 
                    ,mtzn_grp               
                    ,segment_rebuild_yr
                    ,fire_risk_zone_cls
                    ,fdr_cat
                    ,rr_npv15_sif_fdr_cat_adj
                    ,priority_ind
                    ,pickid_Suppressed
						)               
            SELECT  RUN_ID                    
                 ,VIC_ID                    
                 ,A.PICK_ID                   
                 ,EQUIP_GRP_ID              
                 ,EVENT                     
                 ,RISK                      
                 ,RISK_NOACTION             
--                 ,REMAINING_VAL             
                 ,LABOUR_HRS              
                 ,CAPEX                     
--                 ,OPEX                      
                 ,AGE                       
                 ,CALENDAR_YEAR             
                 ,EVENT_REASON              
                 ,RISK_REDUCTION            
--                 ,A.PROT_ZONE_ID              
--                 ,PROT_ZONE_LEN             
                 ,SPAN_LEN                  
                 ,HVCO_CNT                  
                 ,LVCO_CNT                  
                 ,HVSP_CNT                  
                 ,MAINT_ZONE_NAM            
                 ,RISK_NPV15                
                 ,RISK_NOACTION_NPV15       
                 ,RISK_REDUCTION_NPV15      
                 ,RISK_NPV15_SIF            
                 ,RISK_NOACTION_NPV15_SIF   
                 ,RISK_REDUCTION_NPV15_SIF  
                 ,A.SEGMENT_ID                
                 ,COND_EQP_TYP              
                 ,CONS_SEGMENT_ID           
                 ,SCHEDULED                 
                 ,CASE 
                   WHEN cons_segment_mtzn IS NULL            THEN  maint_zone_nam
                   ELSE                                            cons_segment_mtzn
                  END                      AS mtzn_grp
                 ,SEGMENT_REBUILD_YR       
                 ,CASE
                     WHEN b.fire_risk_zone_cls = 'UNKNOWN'  THEN ' '
                     WHEN b.fire_risk_zone_cls IS NULL      THEN ' '
                     ELSE                                         b.fire_risk_zone_cls
                 END                      AS fire_risk_zone_cls 
                 ,CASE
                         WHEN UPPER(c.scnrrr_fdr_cat_nam) = 'NONE'            THEN 'SHORTRURAL'
                     WHEN c.scnrrr_fdr_cat_nam IS NULL            THEN 'SHORTRURAL'
                     ELSE                                                            UPPER(c.scnrrr_fdr_cat_nam)
                 END                                                            AS fdr_cat
                 ,A.rr_npv15_sif_fdr_cat_adj
                 ,A.priority_ind
                 ,A.pickid_suppressed
            FROM structools.st_bay_strategy_run_rb  A INNER JOIN
               structools.st_bay_equipment         b
            ON A.pick_id = b.pick_id   
                                                LEFT OUTER JOIN
               structools.st_hv_feeder          c
            ON b.feeder_id =        c.hv_fdr_id                                                                         
            WHERE run_id = i_base_run_id
         ;            
      END IF;

      COMMIT;
      
   EXCEPTION
      WHEN v_prog_excp THEN
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('Exception '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END reload_temp_strategy_tables;      

-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
   /**************************************************************************************************************/   
   /* Populates tables st_strategy_run_summary and st_del_program_zbam_reinf_summ, which are interrogated by an external optimser script                 */
   /**************************************************************************************************************/   
   PROCEDURE run_summary_for_opensolver(i_base_run_id    IN PLS_INTEGER
                                       ,i_year           IN PLS_INTEGER
                                       ,i_program_id     IN PLS_INTEGER 
                                       ,i_version_id     IN PLS_INTEGER
                                       ,i_mtzn_exclude_yrs      IN PLS_INTEGER DEFAULT 0) IS

      /*****************************************************************/
      /* Declarations for run_summary_for_opensolver procedure         */
      /*****************************************************************/
      v_sql             VARCHAR2(1000);
      v_dummy1          PLS_INTEGER;
      v_dummy2          PLS_INTEGER;
      v_prog_excp       EXCEPTION;
      
      v_reload                      BOOLEAN;
      v_run_id                      PLS_INTEGER;
      v_run_years                     PLS_INTEGER;
      v_mtzn_committed_version_id   PLS_INTEGER;
      v_first_year                PLS_INTEGER;

   BEGIN

      BEGIN
      
         dbms_output.put_line('run_summary_for_opensolver Start '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

         IF i_base_run_id IS NULL OR i_year IS NULL OR i_program_id IS NULL OR i_mtzn_exclude_yrs IS NULL THEN
            dbms_output.put_line ('input parameters cannot be null');            
            RAISE v_prog_excp;
         END IF;
              
         SELECT first_year
         INTO   v_first_year
         FROM structools.st_run_request
         WHERE run_id = i_base_run_id;

         IF v_first_year > v_this_year + 1 THEN
            v_year_0 := v_first_year - 1;
         ELSE
            v_year_0 := v_this_year;
         END IF;
   
         SELECT DISTINCT run_id
                        ,YEAR
         INTO            v_dummy1
                        ,v_dummy2                     
         FROM structools.st_strategy_run_summary
         WHERE  run_id = i_base_run_id
         AND program_id = i_program_id
         AND YEAR = i_year;
         dbms_output.put_line ('run id '||i_base_run_id||' program_id '||i_program_id||' year '||i_year||' already in the summary table. Delete earlier results first');            
         RAISE v_prog_excp;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
      
      BEGIN
         SELECT /*+ parallel */ DISTINCT run_id
         INTO            v_run_id
         FROM structools.st_temp_bay_strategy_run;
         IF v_run_id != i_base_run_id THEN
            dbms_output.put_line ('st_temp_bay_strategy_run does not contain data for run '||i_base_run_id);
            RAISE v_prog_excp;
         END IF;
         SELECT /*+ parallel */ DISTINCT run_id
         INTO            v_run_id
         FROM structools.st_temp_strategy_run;
         IF v_run_id != i_base_run_id THEN
            dbms_output.put_line ('st_temp_strategy_run does not contain data for run '||i_base_run_id);
            RAISE v_prog_excp;
         END IF;
      EXCEPTION
         WHEN TOO_MANY_ROWS THEN
            dbms_output.put_line ('one of the st_temp strategy tables contains multiple runs');
            RAISE v_prog_excp;
      END;
      
      SELECT     run_years
                ,mtzn_committed_version_id
      INTO       v_run_years
                ,v_mtzn_committed_version_id
      FROM       structools.st_run_request    
      WHERE      run_id = i_base_run_id 
      ;
      
      structoolsapp.common_procs.get_parameter_scenarios(i_base_run_id, v_parm_scen_id_life_exp, v_parm_scen_id_seg_min_len,  v_parm_scen_id_seg_cons_pcnt, v_parm_scen_id_sif_weighting
                                                               , v_parm_scen_id_seg_rr_doll,  v_parm_scen_id_eqp_rr_doll, v_parm_scen_id_fdr_cat_adj);   
      
      
      structoolsapp.common_procs.get_sif_weightings(v_parm_scen_id_sif_weighting, v_parm_scen_id_fdr_cat_adj
                                                      ,v_fdr_cat_sif_tab);      
      
      DELETE FROM structools.st_gtt_actioned_segments;
      INSERT INTO structools.st_gtt_actioned_segments  -- = seg_repl
         WITH del_prog_mtzn AS
               (
               SELECT                  
                  b.mtzn
                 ,A.del_year
                 ,A.base_run_id
               FROM structools.st_del_program_mtzn_optimised   	A	INNER JOIN
               			structools.st_mtzn														b
					ON A.mtzn = b.mtzn_opt                        
               WHERE program_id = i_program_id
               AND   base_run_id = i_base_run_id
               AND   del_year > 0 AND del_year < i_year                       
               )                                                              
               ,
               seg_min_repl_yr AS
               (
               SELECT /*+ parallel */ cons_segment_id
                     ,mtzn_grp
                     ,MIN(segment_rebuild_yr) AS min_seg_rb_yr
               FROM structools.st_temp_bay_strategy_run
               WHERE segment_rebuild_yr < i_year            
               GROUP BY cons_segment_id
                       ,mtzn_grp
               )
               SELECT /*+ ORDERED */ DISTINCT A.cons_segment_id
               FROM   del_prog_mtzn          b  INNER JOIN
                      seg_min_repl_yr        A
               ON A.mtzn_grp = b.mtzn
               WHERE b.del_year >= A.min_seg_rb_yr
               AND b.base_run_id = i_base_run_id
               UNION
               SELECT DISTINCT cons_segment_id
               FROM  structools.st_del_program_seg_optimised
               --WHERE (base_run_id BETWEEN v_parent_run_id_first AND v_parent_run_id_last OR base_run_id = i_base_run_id)
               WHERE base_run_id = i_base_run_id AND program_id = i_program_id
               AND   del_year > 0 AND del_year < i_year
      ;         

      DELETE FROM structools.st_gtt_actioned_bay_pickids;
      INSERT INTO structools.st_gtt_actioned_bay_pickids  -- = del_prog_bay_pickids
         WITH del_prog_mtzn AS
               (
               SELECT                  
                  b.mtzn
                 ,A.del_year
                 ,A.base_run_id
               FROM structools.st_del_program_mtzn_optimised   	A	INNER JOIN
               		   structools.st_mtzn														b
					ON A.mtzn = b.mtzn_opt                        
               WHERE program_id = i_program_id
               AND   base_run_id = i_base_run_id
               AND   del_year > 0 AND del_year < i_year
               )                           
               SELECT /*+ parallel */ DISTINCT A.pick_id
               FROM structools.st_temp_bay_strategy_run          A    INNER JOIN
                    del_prog_mtzn b
               ON A.mtzn_grp = B.MTZN
               AND A.calendar_year = B.DEL_YEAR
               AND A.run_id = b.base_run_id        
               WHERE  A.event > ' ' --IS NOT NULL
               AND A.pickid_suppressed = 'N'
               AND A.calendar_year < i_year
               --AND A.scheduled = 'N'             --pickid init
               AND segment_rebuild_yr IS NOT NULL
               AND b.base_run_id = i_base_run_id
               UNION
               SELECT /*+ ORDERED */ DISTINCT b.pick_id
               FROM structools.st_del_program_seg_optimised A  INNER JOIN
                    structools.st_temp_bay_strategy_run     b
               ON A.cons_segment_id = b.cons_segment_id       
               AND A.del_year  = B.calendar_year      
               WHERE A.base_run_id  = i_base_run_id
               AND A.program_id = i_program_id 
               AND A.del_year > 0 AND A.del_year < i_year
               AND b.event > ' '
               AND b.pickid_suppressed = 'N'
      ;

      DELETE FROM structools.st_gtt_actioned_pickids;
      INSERT INTO structools.st_gtt_actioned_pickids     -- = del_prog_pickids
         WITH del_prog_mtzn AS
               (
               SELECT b.mtzn                 
                 ,A.del_year
                 ,A.base_run_id
               FROM structools.st_del_program_mtzn_optimised	A	INNER JOIN
               			structools.st_mtzn													b
					ON A.mtzn = b.mtzn_opt                           
               WHERE program_id = i_program_id
               AND   base_run_id = i_base_run_id
               AND   del_year > 0 AND del_year < i_year
               )
               ,
         		del_prog_mtzn_reinf AS
               (
               SELECT b.mtzn
                 ,A.del_year
                 ,A.base_run_id
               FROM structools.st_del_program_reinf_opt   A	INNER JOIN
               			structools.st_mtzn													b
					ON A.mtzn = b.mtzn_opt                        
               WHERE program_id = i_program_id
               AND   base_run_id = i_base_run_id
               AND   del_year > 0 AND del_year < i_year
               )                           
               ,
               actioned_pickids AS
               (
               SELECT /*+ parallel */ --DISTINCT A.pick_id||','||A.part_cde      AS pick_id_part_cde
               				DISTINCT A.pick_id
                           ,A.part_cde			
               				,A.event
               FROM structools.st_temp_strategy_run          A    INNER JOIN
                    del_prog_mtzn b
               ON A.mtzn_grp = B.MTZN
               AND A.calendar_year = B.DEL_YEAR
               AND A.run_id = b.base_run_id        
               WHERE  A.event > ' ' --IS NOT NULL
               AND A.calendar_year < i_year
               AND (A.pickid_suppressed = 'N' OR A.pickid_suppressed = 'P' AND A.event = 'REINFORCE')
               AND b.base_run_id = i_base_run_id
               UNION
               SELECT /*+ parallel */ --DISTINCT A.pick_id||','||A.part_cde      AS pick_id_part_cde
               				DISTINCT A.pick_id
                           ,A.part_cde			
               				,A.event
               FROM structools.st_temp_strategy_run          A    INNER JOIN
                    del_prog_mtzn_reinf b
               ON A.mtzn_grp = B.MTZN
               AND A.calendar_year = B.DEL_YEAR
               AND A.run_id = b.base_run_id        
               WHERE  A.event = 'REINFORCE'
               AND A.pickid_suppressed IN ('N','P')
               AND A.calendar_year < i_year
               AND b.base_run_id = i_base_run_id
               UNION
               SELECT --DISTINCT pick_id ||','||part_cde                   AS pick_id_and_part
               				DISTINCT pick_id
                           ,part_cde			
               			   ,event
               FROM   structools.st_del_program_sniper
               WHERE  base_run_id = i_base_run_id
               AND    program_id = i_program_id
               AND    del_year < i_year
               UNION
               SELECT /*+ ORDERED */ --DISTINCT b.pick_id||','||b.part_cde      AS pick_id_part_cde
               				DISTINCT b.pick_id
                           ,b.part_cde			
               				,b.event
               FROM structools.st_del_program_seg_optimised A  INNER JOIN
                    structools.st_temp_strategy_run     b
               ON A.cons_segment_id = b.cons_segment_id       
               AND A.del_year  = B.calendar_year      
               WHERE A.base_run_id  = i_base_run_id
               AND A.program_id = i_program_id 
               AND A.del_year > 0 AND A.del_year < i_year
               AND b.event > ' ' --IS NOT NULL
               AND (b.pickid_suppressed = 'N' OR b.pickid_suppressed = 'P' AND b.event = 'REINFORCE') 
               )
               ,
               replaced_poles AS
               (
               SELECT pick_id
               FROM	actioned_pickids
               WHERE pick_id LIKE 'S%' AND part_cde = ' '
               AND event = 'REPLACE'	
               )
               ,
               xarms_stays_on_replaced_poles AS
               (
               SELECT A.pick_id
               			,b.part_cde
					FROM replaced_poles				A		INNER JOIN
               			structools.st_equipment	b
					ON A.pick_id = b.pick_id
               WHERE b.part_cde != ' '                                                
               )
               SELECT DISTINCT pick_id||','||part_cde      AS pick_id_part_cde
               FROM actioned_pickids		
               UNION	
               SELECT pick_id||','||part_cde      AS pick_id_part_cde
               FROM xarms_stays_on_replaced_poles
      ;         


      INSERT INTO structools.st_strategy_run_summary 
         (
         RUN_ID, 
         YEAR, 
         REGION_NAM, 
         MTZN, 
         POLE_REPL_CNT, 
         POLE_REINF_CNT, 
         EQP_REPL_CNT, 
         SPAN_REPL_LEN_KM, 
         SUM_RISK_REDUCTION, 
         SUM_CAPEX, 
         POLE_REPL_CAPEX, 
         POLE_REINF_CAPEX, 
         EQP_REPL_CAPEX, 
         COND_CAPEX, 
         POLE_REPL_RISK_RED, 
         POLE_REINF_RISK_RED, 
         EQP_REPL_RISK_RED, 
         COND_REPL_RISK_RED, 
         SUM_RISK_RED_NPV, 
         POLE_REPL_RISK_RED_NPV, 
         POLE_REINF_RISK_RED_NPV, 
         EQP_REPL_RISK_RED_NPV, 
         COND_REPL_RISK_RED_NPV, 
         SUM_RISK_RED_SIF, 
         POLE_REPL_RISK_RED_SIF, 
         POLE_REINF_RISK_RED_SIF, 
         EQP_REPL_RISK_RED_SIF, 
         COND_REPL_RISK_RED_SIF, 
         SUM_RISK_RED_PUBSAF_SIF, 
         POLE_REPL_RISK_RED_PUBSAF_SIF, 
         POLE_REINF_RISK_RED_PUBSAF_SIF, 
         EQP_REPL_RISK_RED_PUBSAF_SIF, 
         COND_REPL_RISK_RED_PUBSAF_SIF, 
         SUM_RISK_RED_WFORCE_SIF, 
         POLE_REPL_RISK_RED_WFORCE_SIF, 
         POLE_REINF_RISK_RED_WFORCE_SIF, 
         EQP_REPL_RISK_RED_WFORCE_SIF, 
         COND_REPL_RISK_RED_WFORCE_SIF, 
         SUM_RISK_RED_RELIAB_SIF, 
         POLE_REPL_RISK_RED_RELIAB_SIF, 
         POLE_REINF_RISK_RED_RELIAB_SIF, 
         EQP_REPL_RISK_RED_RELIAB_SIF, 
         COND_REPL_RISK_RED_RELIAB_SIF, 
         SUM_RISK_RED_ENV_SIF, 
         POLE_REPL_RISK_RED_ENV_SIF, 
         POLE_REINF_RISK_RED_ENV_SIF, 
         EQP_REPL_RISK_RED_ENV_SIF, 
         COND_REPL_RISK_RED_ENV_SIF, 
         SUM_RISK_RED_NPV_PUBSAF, 
         POLE_REPL_RISK_RED_NPV_PUBSAF, 
         POLE_REINF_RISK_RED_NPV_PUBSAF, 
         EQP_REPL_RISK_RED_NPV_PUBSAF, 
         COND_REPL_RISK_RED_NPV_PUBSAF, 
         SUM_RISK_RED_NPV_WFORCE, 
         POLE_REPL_RISK_RED_NPV_WFORCE, 
         POLE_REINF_RISK_RED_NPV_WFORCE, 
         EQP_REPL_RISK_RED_NPV_WFORCE, 
         COND_REPL_RISK_RED_NPV_WFORCE, 
         SUM_RISK_RED_NPV_RELIAB, 
         POLE_REPL_RISK_RED_NPV_RELIAB, 
         POLE_REINF_RISK_RED_NPV_RELIAB, 
         EQP_REPL_RISK_RED_NPV_RELIAB, 
         COND_REPL_RISK_RED_NPV_RELIAB, 
         SUM_RISK_RED_NPV_ENV, 
         POLE_REPL_RISK_RED_NPV_ENV, 
         POLE_REINF_RISK_RED_NPV_ENV, 
         EQP_REPL_RISK_RED_NPV_ENV, 
         COND_REPL_RISK_RED_NPV_ENV, 
         PROGRAM_ID, 
         AREA_NAM, 
         MTZN_EXCLUDE, 
         S_POLE_REPL_CNT, 
         S_POLE_REINF_CNT, 
         S_EQP_REPL_CNT, 
         S_SPAN_REPL_LEN_KM, 
         S_SUM_RISK_RED, 
         S_SUM_CAPEX, 
         S_POLE_REPL_CAPEX, 
         S_POLE_REINF_CAPEX, 
         S_EQP_REPL_CAPEX, 
         S_CON_CAPEX, 
         S_POLE_REPL_RISK_RED, 
         S_POLE_REINF_RISK_RED, 
         S_EQP_REPL_RISK_RED, 
         S_CON_REPL_RISK_RED, 
         S_SUM_RISK_RED_NPV, 
         S_POLE_REPL_RISK_RED_NPV, 
         S_POLE_REINF_RISK_RED_NPV, 
         S_EQP_REPL_RISK_RED_NPV, 
         S_CON_REPL_RISK_RED_NPV, 
         S_SUM_RISK_RED_SF, 
         S_POLE_REPL_RISK_RED_SF, 
         S_POLE_REINF_RISK_RED_SF, 
         S_EQP_REPL_RISK_RED_SF, 
         S_CON_REPL_RISK_RED_SF, 
         S_SUM_RISK_RED_PSAF_SF, 
         S_POLE_REPL_RISK_RED_PSAF_SF, 
         S_POLE_REIN_RISK_RED_PSAF_SF, 
         S_EQP_REPL_RISK_RED_PSAF_SF, 
         S_CON_REPL_RISK_RED_PSAF_SF, 
         S_SUM_RISK_RED_WF_SF, 
         S_POLE_REPL_RISK_RED_WF_SF, 
         S_POLE_REIN_RISK_RED_WF_SF, 
         S_EQP_REPL_RISK_RED_WF_SF, 
         S_CON_REPL_RISK_RED_WF_SF, 
         S_SUM_RISK_RED_REL_SIF, 
         S_POLE_REPL_RISK_RED_REL_SF, 
         S_POLE_REIN_RISK_RED_REL_SF, 
         S_EQP_REPL_RISK_RED_REL_SF, 
         S_CON_REPL_RISK_RED_REL_SF, 
         S_SUM_RISK_RED_ENV_SF, 
         S_POLE_REPL_RISK_RED_ENV_SF, 
         S_POLE_REIN_RISK_RED_ENV_SF, 
         S_EQP_REPL_RISK_RED_ENV_SF, 
         S_CON_REPL_RISK_RED_ENV_SF, 
         S_SUM_RISK_RED_NPV_PSAF, 
         S_POLE_REPL_RISK_RED_NPV_PBSAF, 
         S_POLE_REIN_RISK_RED_NPV_PSAF, 
         S_EQP_REPL_RISK_RED_NPV_PSAF, 
         S_CON_REPL_RISK_RED_NPV_PSAF, 
         S_SUM_RISK_RED_NPV_WF, 
         S_POLE_REPL_RISK_RED_NPV_WF, 
         S_POLE_REIN_RISK_RED_NPV_WF, 
         S_EQP_REPL_RISK_RED_NPV_WF, 
         S_COND_REPL_RISK_RED_NPV_WF, 
         S_SUM_RISK_RED_NPV_REL, 
         S_POLE_REPL_RISK_RED_NPV_REL, 
         S_POLE_REIN_RISK_RED_NPV_REL, 
         S_EQP_REPL_RISK_RED_NPV_REL, 
         S_COND_REPL_RISK_RED_NPV_REL, 
         S_SUM_RISK_RED_NPV_ENV, 
         S_POLE_REPL_RISK_RED_NPV_ENV, 
         S_POLE_REIN_RISK_RED_NPV_ENV, 
         S_EQP_REPL_RISK_RED_NPV_ENV, 
         S_COND_REPL_RISK_RED_NPV_ENV, 
         DEPOT_NAM, 
         COND_REPL_LEN_KM, 
         S_COND_REPL_LEN_KM, 
         POLE_REPL_PRIOR_CNT, 
         POLE_REINF_PRIOR_CNT, 
         EQP_REPL_PRIOR_CNT, 
         COND_REPL_PRIOR_LEN_KM, 
         S_POLE_REPL_PRIOR_CNT, 
         S_POLE_REINF_PRIOR_CNT, 
         S_EQP_REPL_PRIOR_CNT, 
         S_COND_REPL_PRIOR_LEN_KM, 
         XSTAY_REPL_CNT, 
         XSTAY_REPL_CAPEX, 
         XSTAY_REPL_RISK_RED, 
         XSTAY_REPL_RISK_RED_NPV, 
         XSTAY_REPL_RISK_RED_SIF, 
         XSTAY_REPL_RISK_RED_PUBSAF_SIF, 
         XSTAY_REPL_RISK_RED_WFORCE_SIF, 
         XSTAY_REPL_RISK_RED_RELIAB_SIF, 
         XSTAY_REPL_RISK_RED_ENV_SIF, 
         XSTAY_REPL_RISK_RED_NPV_PUBSAF, 
         XSTAY_REPL_RISK_RED_NPV_WFORCE, 
         XSTAY_REPL_RISK_RED_NPV_RELIAB, 
         XSTAY_REPL_RISK_RED_NPV_ENV, 
         S_XSTAY_REPL_CNT, 
         S_XSTAY_REPL_CAPEX,
         S_XSTAY_REPL_RISK_RED, 
         S_XSTAY_REPL_RISK_RED_NPV, 
         S_XSTAY_REPL_RISK_RED_SF, 
         S_XSTAY_REPL_RISK_RED_PSAF_SF, 
         S_XSTAY_REPL_RISK_RED_WF_SF, 
         S_XSTAY_REPL_RISK_RED_REL_SF, 
         S_XSTAY_REPL_RISK_RED_ENV_SF, 
         S_XSTAY_REPL_RISK_RED_NPV_PSAF, 
         S_XSTAY_REPL_RISK_RED_NPV_WF, 
         S_XSTAY_REPL_RISK_RED_NPV_REL, 
         S_XSTAY_REPL_RISK_RED_NPV_ENV, 
         XSTAY_REPL_PRIORITY_CNT, 
         S_XSTAY_REPL_PRIORITY_CNT, 
         SUM_RISK_RED_FIN_SIF, 
         POLE_REPL_RISK_RED_FIN_SIF, 
         POLE_REINF_RISK_RED_FIN_SIF, 
         EQP_REPL_RISK_RED_FIN_SIF, 
         COND_REPL_RISK_RED_FIN_SIF, 
         SUM_RISK_RED_NPV_FIN, 
         POLE_REPL_RISK_RED_NPV_FIN, 
         POLE_REINF_RISK_RED_NPV_FIN, 
         EQP_REPL_RISK_RED_NPV_FIN, 
         COND_REPL_RISK_RED_NPV_FIN, 
         S_SUM_RISK_RED_FIN_SF, 
         S_POLE_REPL_RISK_RED_FIN_SF, 
         S_POLE_REIN_RISK_RED_FIN_SF, 
         S_EQP_REPL_RISK_RED_FIN_SF, 
         S_CON_REPL_RISK_RED_FIN_SF, 
         S_SUM_RISK_RED_NPV_FIN, 
         S_POLE_REPL_RISK_RED_NPV_FIN, 
         S_POLE_REIN_RISK_RED_NPV_FIN, 
         S_EQP_REPL_RISK_RED_NPV_FIN, 
         S_COND_REPL_RISK_RED_NPV_FIN, 
         XSTAY_REPL_RISK_RED_FIN_SIF, 
         XSTAY_REPL_RISK_RED_NPV_FIN,         
         S_XSTAY_REPL_RISK_RED_FIN_SF, 
         S_XSTAY_REPL_RISK_RED_NPV_FIN,
         pole_repl_labour_hrs,
         s_pole_repl_labour_hrs,
         cond_labour_hrs,
         s_cond_labour_hrs
         )
         WITH nrmt_frac AS
         (
         SELECT pick_id
               ,CASE
                  WHEN model_name = 'XARMHVI'       THEN  'HVXM'
                  WHEN model_name = 'XARMLVI'       THEN  'LVXM'
                  WHEN model_name = 'STAY'         THEN  'STAY'
                  ELSE                                   ' '
               END                     AS part_cde
               ,(fire_frac + eshock_frac + physimp_frac) AS pubsafety_frac
               ,wforce_frac
               ,reliability_frac
               ,env_frac
               ,fin_frac
         FROM structools.st_risk_fractions
         WHERE MODEL_NAME IN ('BAY', 'DOF','DSTR', 'PTSD', 'PWOD',  'RECL', 'RGTR', 'SECT','XARMHVI','XARMLVI','STAY')
         )
         ,
         bay_strategy_curr_year AS
         (
         SELECT /*+ parallel */ 
                A.*
               ,A.span_len * A.hvco_cnt + A.span_len * A.lvco_cnt + A.span_len * A.hvsp_cnt AS cond_len
               ,CASE
                  WHEN priority_ind = 1 THEN
                     A.span_len
                  ELSE
                     0
               END                                                                         AS cond_priority_len
               ,CASE
                  WHEN A.cons_segment_id IS NOT NULL AND segment_rebuild_yr IS NOT NULL AND d.segment_id IS NULL THEN 'S'    
                  ELSE                                                                                                 'P'
               END                                                                     AS selection_type
               ,A.risk_noaction_npv15 * b.pubsafety_frac   * E.sif_weight_pubsafety    AS risk_noaction_pubsaf_sif
               ,A.risk_noaction_npv15 * b.wforce_frac   * E.sif_weight_wforce           AS risk_noaction_wforce_sif
               ,A.risk_noaction_npv15 * b.reliability_frac   * E.sif_weight_reliability AS risk_noaction_reliab_sif
               ,A.risk_noaction_npv15 * b.env_frac   * E.sif_weight_env                AS risk_noaction_env_sif
               ,A.risk_noaction_npv15 * b.fin_frac   * E.sif_weight_fin                AS risk_noaction_fin_sif
               ,A.risk_npv15          * b.pubsafety_frac   * E.sif_weight_pubsafety      AS risk_pubsaf_sif
               ,A.risk_npv15          * b.wforce_frac      * E.sif_weight_wforce         AS risk_wforce_sif
               ,A.risk_npv15          * b.reliability_frac * E.sif_weight_reliability    AS risk_reliab_sif
               ,A.risk_npv15          * b.env_frac         * E.sif_weight_env            AS risk_env_sif
               ,A.risk_npv15          * b.fin_frac         * E.sif_weight_fin            AS risk_fin_sif
               ,A.risk_noaction_npv15 * b.pubsafety_frac                               AS risk_noaction_npv_pubsaf
               ,A.risk_noaction_npv15 * b.wforce_frac                                  AS risk_noaction_npv_wforce
               ,A.risk_noaction_npv15 * b.reliability_frac                             AS risk_noaction_npv_reliab
               ,A.risk_noaction_npv15 * b.env_frac                                     AS risk_noaction_npv_env
               ,A.risk_noaction_npv15 * b.fin_frac                                     AS risk_noaction_npv_fin
               ,A.risk_npv15          * b.pubsafety_frac                               AS risk_npv_pubsaf
               ,A.risk_npv15          * b.wforce_frac                                  AS risk_npv_wforce
               ,A.risk_npv15          * b.reliability_frac                             AS risk_npv_reliab
               ,A.risk_npv15          * b.env_frac                                     AS risk_npv_env
               ,A.risk_npv15          * b.fin_frac                                     AS risk_npv_fin
               ,NVL(E.adj_factor, 1)                                                   AS fdr_cat_adj_factor
         FROM structools.st_temp_bay_strategy_run   A  LEFT OUTER JOIN
              nrmt_frac         b
         ON A.pick_id = b.pick_id        
                                                    LEFT OUTER JOIN
              structools.st_gtt_actioned_segments              d
         ON A.cons_segment_id = d.segment_id
                                                    LEFT OUTER JOIN
              structools.st_gtt_fdr_cat_adj   E
         ON A.fdr_cat = E.fdr_cat
         WHERE event > ' ' --IS NOT NULL
         AND calendar_year = i_year
         AND pickid_suppressed = 'N'
         --AND A.scheduled = 'N'       --pickid init
         )
         ,
         bay_strategy_filtered AS
         (
         SELECT A.*
               ,CASE
                  WHEN risk_pubsaf_sif  < risk_noaction_pubsaf_sif     THEN  (risk_noaction_pubsaf_sif - risk_pubsaf_sif) * fdr_cat_adj_factor
                  ELSE                                                     0
               END                        AS risk_red_pubsaf_sif
               ,CASE
                  WHEN risk_wforce_sif     < risk_noaction_wforce_sif        THEN  (risk_noaction_wforce_sif - risk_wforce_sif) * fdr_cat_adj_factor
                  ELSE                                                     0
               END                        AS risk_red_wforce_sif
               ,CASE
                  WHEN risk_reliab_sif < risk_noaction_reliab_sif  THEN  (risk_noaction_reliab_sif - risk_reliab_sif) * fdr_cat_adj_factor
                  ELSE                                                     0
               END                        AS risk_red_reliab_sif
               ,CASE
                  WHEN risk_env_sif         < risk_noaction_env_sif          THEN  (risk_noaction_env_sif - risk_env_sif) * fdr_cat_adj_factor
                  ELSE                                                     0
               END                        AS risk_red_env_sif
               ,CASE
                  WHEN risk_fin_sif         < risk_noaction_fin_sif          THEN  (risk_noaction_fin_sif - risk_fin_sif) * fdr_cat_adj_factor
                  ELSE                                                     0
               END                        AS risk_red_fin_sif
               ,CASE
                  WHEN risk_npv_pubsaf  < risk_noaction_npv_pubsaf     THEN  risk_noaction_npv_pubsaf - risk_npv_pubsaf
                  ELSE                                                     0
               END                        AS risk_red_npv_pubsaf
               ,CASE
                  WHEN risk_npv_wforce     < risk_noaction_npv_wforce        THEN  risk_noaction_npv_wforce - risk_npv_wforce
                  ELSE                                                     0
               END                        AS risk_red_npv_wforce
               ,CASE
                  WHEN risk_npv_reliab < risk_noaction_npv_reliab  THEN  risk_noaction_npv_reliab - risk_npv_reliab
                  ELSE                                                     0
               END                        AS risk_red_npv_reliab
               ,CASE
                  WHEN risk_npv_env         < risk_noaction_npv_env          THEN  risk_noaction_npv_env - risk_npv_env
                  ELSE                                                     0
               END                        AS risk_red_npv_env
               ,CASE
                  WHEN risk_npv_fin         < risk_noaction_npv_fin          THEN  risk_noaction_npv_fin - risk_npv_fin
                  ELSE                                                     0
               END                        AS risk_red_npv_fin
         FROM bay_strategy_curr_year   A
         WHERE pick_id NOT IN (SELECT pick_id FROM structools.st_gtt_actioned_bay_pickids)
         )
         ,
         bay1 AS
         (
         SELECT mtzn 
               ,selection_type
               ,ROUND(SUM(span_len) / 1000, 1) AS span_len_km
               ,ROUND(SUM(cond_len) / 1000, 1) AS cond_len_km
               --,ROUND(SUM(cond_priority_len) / 1000, 1) AS cond_priority_len_km
               ,SUM(capex) AS cond_capex
               ,SUM(labour_hrs) AS cond_labour_hrs
         FROM      
         (
         SELECT DISTINCT mtzn_grp   AS mtzn
               ,selection_type
               ,vic_id
               ,span_len
               ,cond_len
               ,capex
               ,labour_hrs
         FROM bay_strategy_filtered   A
         )
         GROUP BY mtzn, selection_type
         )
         ,
         bay2 AS
         (
         SELECT mtzn_grp                                 AS mtzn
               ,selection_type
               ,ROUND(SUM(risk_reduction), 2)            AS cond_repl_risk_red
               ,ROUND(SUM(risk_reduction_npv15), 2)      AS cond_repl_risk_red_npv
               ,ROUND(SUM(risk_red_npv_pubsaf), 2)           AS cond_repl_risk_red_npv_pubsaf
               ,ROUND(SUM(risk_red_npv_wforce), 2)           AS cond_repl_risk_red_npv_wforce
               ,ROUND(SUM(risk_red_npv_reliab), 2)           AS cond_repl_risk_red_npv_reliab
               ,ROUND(SUM(risk_red_npv_env), 2)              AS cond_repl_risk_red_npv_env
               ,ROUND(SUM(risk_red_npv_fin), 2)              AS cond_repl_risk_red_npv_fin
               ,ROUND(SUM(rr_npv15_sif_fdr_cat_adj), 2)  AS cond_repl_risk_red_sif
               ,ROUND(SUM(risk_red_pubsaf_sif), 2)           AS cond_repl_risk_red_pubsaf_sif
               ,ROUND(SUM(risk_red_wforce_sif), 2)           AS cond_repl_risk_red_wforce_sif
               ,ROUND(SUM(risk_red_reliab_sif), 2)           AS cond_repl_risk_red_reliab_sif
               ,ROUND(SUM(risk_red_env_sif), 2)              AS cond_repl_risk_red_env_sif
               ,ROUND(SUM(risk_red_fin_sif), 2)              AS cond_repl_risk_red_fin_sif
               ,ROUND(SUM(cond_priority_len) / 1000, 1)  AS cond_priority_len_km
         FROM bay_strategy_filtered   
         GROUP BY mtzn_grp, selection_type
         )
         ,
         bays AS
         (
         SELECT A.mtzn
               ,A.span_len_km AS span_repl_len_km
               ,A.cond_len_km AS cond_repl_len_km
               ,b.cond_priority_len_km AS cond_repl_priority_len_km
               ,A.cond_capex
               ,A.cond_labour_hrs
               ,b.selection_type
               ,b.cond_repl_risk_red
               ,b.cond_repl_risk_red_npv
               ,b.cond_repl_risk_red_npv_pubsaf
               ,b.cond_repl_risk_red_npv_wforce
               ,b.cond_repl_risk_red_npv_reliab
               ,b.cond_repl_risk_red_npv_env
               ,b.cond_repl_risk_red_npv_fin
               ,b.cond_repl_risk_red_sif
               ,b.cond_repl_risk_red_pubsaf_sif
               ,b.cond_repl_risk_red_wforce_sif
               ,b.cond_repl_risk_red_reliab_sif
               ,b.cond_repl_risk_red_env_sif
               ,b.cond_repl_risk_red_fin_sif
         FROM  bay1        A  INNER JOIN
               bay2        b
         ON A.mtzn = b.mtzn
         AND A.selection_type = b.selection_type
         )
         ,
         strategy_curr_year AS
         (
         SELECT A.*
               ,A.pick_id||','||A.part_cde   AS pick_id_and_part
               ,A.risk_noaction_npv15 * b.pubsafety_frac    * E.sif_weight_pubsafety     AS risk_noaction_pubsaf_sif
               ,A.risk_noaction_npv15 * b.wforce_frac       * E.sif_weight_wforce        AS risk_noaction_wforce_sif
               ,A.risk_noaction_npv15 * b.reliability_frac  * E.sif_weight_reliability   AS risk_noaction_reliab_sif
               ,A.risk_noaction_npv15 * b.env_frac          * E.sif_weight_env           AS risk_noaction_env_sif
               ,A.risk_noaction_npv15 * b.fin_frac          * E.sif_weight_fin           AS risk_noaction_fin_sif
               ,A.risk_npv15          * b.pubsafety_frac    * E.sif_weight_pubsafety     AS risk_pubsaf_sif
               ,A.risk_npv15          * b.wforce_frac       * E.sif_weight_wforce        AS risk_wforce_sif
               ,A.risk_npv15          * b.reliability_frac  * E.sif_weight_reliability   AS risk_reliab_sif
               ,A.risk_npv15          * b.env_frac          * E.sif_weight_env           AS risk_env_sif
               ,A.risk_npv15          * b.fin_frac          * E.sif_weight_fin           AS risk_fin_sif
               ,A.risk_noaction_npv15 * b.pubsafety_frac                               AS risk_noaction_npv_pubsaf
               ,A.risk_noaction_npv15 * b.wforce_frac                                  AS risk_noaction_npv_wforce
               ,A.risk_noaction_npv15 * b.reliability_frac                             AS risk_noaction_npv_reliab
               ,A.risk_noaction_npv15 * b.env_frac                                     AS risk_noaction_npv_env
               ,A.risk_noaction_npv15 * b.fin_frac                                     AS risk_noaction_npv_fin
               ,A.risk_npv15          * b.pubsafety_frac                               AS risk_npv_pubsaf
               ,A.risk_npv15          * b.wforce_frac                                  AS risk_npv_wforce
               ,A.risk_npv15          * b.reliability_frac                             AS risk_npv_reliab
               ,A.risk_npv15          * b.env_frac                                     AS risk_npv_env
               ,A.risk_npv15          * b.fin_frac                                     AS risk_npv_fin
               ,NVL(E.adj_factor, 1)                                                   AS fdr_cat_adj_factor
         FROM structools.st_temp_strategy_run       A  LEFT OUTER JOIN
              nrmt_frac         b
         ON A.pick_id = b.pick_id
         AND A.part_cde2 = b.part_cde
                                                   LEFT OUTER JOIN
              structools.st_gtt_fdr_cat_adj        E
         ON A.fdr_cat = E.fdr_cat
         WHERE event > ' ' --IS NOT NULL
         AND calendar_year = i_year
         AND (pickid_suppressed = 'N' OR pickid_suppressed ='P' AND event = 'REINFORCE')
         )
         ,
         strategy_run_filtered AS
         (
         SELECT A.*
               ,CASE
                  WHEN A.cons_segment_id IS NOT NULL AND segment_rebuild_yr IS NOT NULL AND d.segment_id IS NULL THEN 'S'
                  ELSE                                                                                                'P'
               END                                                                                          AS selection_type                 
               ,CASE
                  WHEN risk_pubsaf_sif  < risk_noaction_pubsaf_sif THEN   (risk_noaction_pubsaf_sif - risk_pubsaf_sif) * fdr_cat_adj_factor
                  ELSE                                                  0
               END                        AS risk_red_pubsaf_sif
               ,CASE
                  WHEN risk_wforce_sif     < risk_noaction_wforce_sif    THEN   (risk_noaction_wforce_sif - risk_wforce_sif) * fdr_cat_adj_factor
                  ELSE                                                  0
               END                        AS risk_red_wforce_sif
               ,CASE
                  WHEN risk_reliab_sif < risk_noaction_reliab_sif THEN   (risk_noaction_reliab_sif - risk_reliab_sif) * fdr_cat_adj_factor
                  ELSE                                                  0
               END                        AS risk_red_reliab_sif
               ,CASE
                  WHEN risk_env_sif         < risk_noaction_env_sif         THEN   (risk_noaction_env_sif - risk_env_sif) * fdr_cat_adj_factor
                  ELSE                                                  0
               END                        AS risk_red_env_sif
               ,CASE
                  WHEN risk_fin_sif         < risk_noaction_fin_sif         THEN   (risk_noaction_fin_sif - risk_fin_sif) * fdr_cat_adj_factor
                  ELSE                                                  0
               END                        AS risk_red_fin_sif
               ,CASE
                  WHEN risk_npv_pubsaf  < risk_noaction_npv_pubsaf THEN   risk_noaction_npv_pubsaf - risk_npv_pubsaf
                  ELSE                                                  0
               END                        AS risk_red_npv_pubsaf
               ,CASE
                  WHEN risk_npv_wforce     < risk_noaction_npv_wforce    THEN   risk_noaction_npv_wforce - risk_npv_wforce
                  ELSE                                                  0
               END                        AS risk_red_npv_wforce
               ,CASE
                  WHEN risk_npv_reliab < risk_noaction_npv_reliab THEN   risk_noaction_npv_reliab - risk_npv_reliab
                  ELSE                                                  0
               END                        AS risk_red_npv_reliab
               ,CASE
                  WHEN risk_npv_env         < risk_noaction_npv_env         THEN   risk_noaction_npv_env - risk_npv_env
                  ELSE                                                  0
               END                        AS risk_red_npv_env
               ,CASE
                  WHEN risk_npv_fin         < risk_noaction_npv_fin         THEN   risk_noaction_npv_fin - risk_npv_fin
                  ELSE                                                  0
               END                        AS risk_red_npv_fin
         FROM strategy_curr_year                A  LEFT OUTER JOIN        
              structools.st_gtt_actioned_segments  d
         ON A.cons_segment_id = d.segment_id
         WHERE A.pick_id_and_part NOT IN (SELECT pick_id_part_cde FROM structools.st_gtt_actioned_pickids)
         --WHERE A.pick_id NOT IN (SELECT pick_id FROM del_prog_pickids)
         )
         ,
         EVENTS AS
         (
         SELECT A.mtzn_grp AS mtzn
               ,selection_type
               ,equip_grp_id
               ,CASE 
                  WHEN equip_grp_id = 'PWOD' AND part_cde = ' '   THEN  'POLE'
                  WHEN equip_grp_id = 'PAUS' AND part_cde = ' '   THEN  'POLE'
                  WHEN part_cde = ' '                             THEN  'EQP'
                  WHEN part_cde != ' '                            THEN  'XARMSTAY'
               END                     AS eqp_typ
               ,CASE event
                  WHEN 'REPLACE'    THEN 'REPL'
                  WHEN 'REINFORCE'  THEN 'REINF'
               END                     AS event
               ,risk_reduction                  AS risk_red
               ,risk_reduction_npv15            AS risk_red_npv
               ,risk_red_npv_pubsaf
               ,risk_red_npv_wforce
               ,risk_red_npv_reliab
               ,risk_red_npv_env
               ,risk_red_npv_fin
               ,rr_npv15_sif_fdr_cat_adj        AS risk_red_sif
               ,risk_red_pubsaf_sif
               ,risk_red_wforce_sif
               ,risk_red_reliab_sif
               ,risk_red_env_sif
               ,risk_red_fin_sif
               ,capex
               ,labour_hrs
               ,priority_ind
         FROM strategy_run_filtered A
         )
         ,
         events2 AS
         (
         SELECT mtzn
               ,eqp_typ
               ,selection_type         
               ,eqp_typ||'_'||event    AS eqp_event
               ,COUNT(*)               AS event_cnt 
               ,COUNT(priority_ind)    AS event_priority_cnt
               ,ROUND(SUM(risk_red),2)             AS risk_red
               ,ROUND(SUM(risk_red_npv),2)         AS risk_red_npv
               ,ROUND(SUM(risk_red_npv_pubsaf),2)      AS risk_red_npv_pubsaf
               ,ROUND(SUM(risk_red_npv_wforce),2)      AS risk_red_npv_wforce
               ,ROUND(SUM(risk_red_npv_reliab),2)      AS risk_red_npv_reliab
               ,ROUND(SUM(risk_red_npv_env),2)         AS risk_red_npv_env
               ,ROUND(SUM(risk_red_npv_fin),2)         AS risk_red_npv_fin
               ,ROUND(SUM(risk_red_sif),2)         AS risk_red_sif
               ,ROUND(SUM(risk_red_pubsaf_sif),2)      AS risk_red_pubsaf_sif
               ,ROUND(SUM(risk_red_wforce_sif),2)      AS risk_red_wforce_sif
               ,ROUND(SUM(risk_red_reliab_sif),2)      AS risk_red_reliab_sif
               ,ROUND(SUM(risk_red_env_sif),2)         AS risk_red_env_sif
               ,ROUND(SUM(risk_red_fin_sif),2)         AS risk_red_fin_sif
               ,SUM(capex)             			AS capex
               ,SUM(labour_hrs)              AS labour_hrs
         FROM EVENTS
         GROUP BY mtzn, eqp_typ, selection_type, event         
         )
         ,
         poles AS
         (
         SELECT *
         FROM
         (
         SELECT mtzn
               ,selection_type
               ,eqp_event     
               ,event_cnt     
               ,event_priority_cnt     
               ,risk_red
               ,risk_red_npv  
               ,risk_red_npv_pubsaf
               ,risk_red_npv_wforce
               ,risk_red_npv_reliab
               ,risk_red_npv_env
               ,risk_red_npv_fin
               ,risk_red_sif
               ,risk_red_pubsaf_sif
               ,risk_red_wforce_sif
               ,risk_red_reliab_sif
               ,risk_red_env_sif
               ,risk_red_fin_sif
               ,capex          
               ,labour_hrs
         FROM events2
         )
         PIVOT    (MAX(event_cnt) AS cnt
                  ,MAX(event_priority_cnt) AS priority_cnt
                  ,MAX(risk_red) AS risk_red
                  ,MAX(risk_red_npv) AS risk_red_npv 
                  ,MAX(risk_red_npv_pubsaf) AS risk_red_npv_pubsaf
                  ,MAX(risk_red_npv_wforce) AS risk_red_npv_wforce
                  ,MAX(risk_red_npv_reliab) AS risk_red_npv_reliab
                  ,MAX(risk_red_npv_env)    AS risk_red_npv_env
                  ,MAX(risk_red_npv_fin)    AS risk_red_npv_fin
                  ,MAX(risk_red_sif) AS risk_red_sif
                  ,MAX(risk_red_pubsaf_sif) AS risk_red_pubsaf_sif
                  ,MAX(risk_red_wforce_sif) AS risk_red_wforce_sif
                  ,MAX(risk_red_reliab_sif) AS risk_red_reliab_sif
                  ,MAX(risk_red_env_sif) AS risk_red_env_sif
                  ,MAX(risk_red_fin_sif) AS risk_red_fin_sif
                  ,MAX(capex) AS capex
                  ,MAX(labour_hrs) AS labour_hrs
                  FOR eqp_event IN ('POLE_REPL' pole_repl,'POLE_REINF' pole_reinf,'EQP_REPL' eqp_repl, 'XARMSTAY_REPL' xstay_repl ) )
         )  
         ,
         poles_and_bays AS
         (
         SELECT NVL(A.mtzn, b.mtzn)                      AS mtzn
               ,NVL(A.selection_type, b.selection_type)  AS selection_type
               ,NVL(A.pole_repl_cnt, 0)                  AS pole_repl_cnt
               ,NVL(A.pole_repl_priority_cnt, 0)         AS pole_repl_priority_cnt
               ,NVL(A.pole_reinf_cnt, 0)                 AS pole_reinf_cnt
               ,NVL(A.pole_reinf_priority_cnt, 0)        AS pole_reinf_priority_cnt
               ,NVL(A.eqp_repl_cnt, 0)                   AS eqp_repl_cnt
               ,NVL(A.eqp_repl_priority_cnt, 0)          AS eqp_repl_priority_cnt
               ,NVL(b.span_repl_len_km, 0)               AS span_replace_len_km
               ,NVL(b.cond_repl_len_km, 0)               AS cond_replace_len_km
               ,NVL(b.cond_repl_priority_len_km, 0)      AS cond_replace_priority_len_km
               ,NVL(A.xstay_repl_cnt, 0)               AS xstay_repl_cnt
               ,NVL(A.xstay_repl_priority_cnt, 0)      AS xstay_repl_priority_cnt
               ,NVL(A.pole_repl_capex, 0)                AS pole_repl_capex
               ,NVL(A.pole_repl_labour_hrs, 0)                AS pole_repl_labour_hrs
               ,NVL(A.pole_reinf_capex, 0)               AS pole_reinf_capex
               ,NVL(A.eqp_repl_capex, 0)                 AS eqp_repl_capex
               ,NVL(A.xstay_repl_capex, 0)             AS xstay_repl_capex
               ,NVL(b.cond_capex, 0)                     AS cond_capex
               ,NVL(b.cond_labour_hrs, 0)                     AS cond_labour_hrs
               ,NVL(A.pole_repl_risk_red, 0)             AS pole_repl_risk_red
               ,NVL(A.pole_repl_risk_red_npv, 0)         AS pole_repl_risk_red_npv
               ,NVL(A.pole_repl_risk_red_npv_pubsaf, 0)  AS pole_repl_risk_red_npv_pubsaf
               ,NVL(A.pole_repl_risk_red_npv_wforce, 0)  AS pole_repl_risk_red_npv_wforce
               ,NVL(A.pole_repl_risk_red_npv_reliab, 0)  AS pole_repl_risk_red_npv_reliab
               ,NVL(A.pole_repl_risk_red_npv_env, 0)     AS pole_repl_risk_red_npv_env
               ,NVL(A.pole_repl_risk_red_npv_fin, 0)     AS pole_repl_risk_red_npv_fin
               ,NVL(A.pole_repl_risk_red_sif, 0)         AS pole_repl_risk_red_sif
               ,NVL(A.pole_repl_risk_red_pubsaf_sif, 0)  AS pole_repl_risk_red_pubsaf_sif
               ,NVL(A.pole_repl_risk_red_wforce_sif, 0)  AS pole_repl_risk_red_wforce_sif
               ,NVL(A.pole_repl_risk_red_reliab_sif, 0)  AS pole_repl_risk_red_reliab_sif
               ,NVL(A.pole_repl_risk_red_env_sif, 0)     AS pole_repl_risk_red_env_sif
               ,NVL(A.pole_repl_risk_red_fin_sif, 0)     AS pole_repl_risk_red_fin_sif
               ,NVL(A.pole_reinf_risk_red, 0)            AS pole_reinf_risk_red
               ,NVL(A.pole_reinf_risk_red_npv, 0)        AS pole_reinf_risk_red_npv
               ,NVL(A.pole_reinf_risk_red_npv_pubsaf, 0) AS pole_reinf_risk_red_npv_pubsaf
               ,NVL(A.pole_reinf_risk_red_npv_wforce, 0) AS pole_reinf_risk_red_npv_wforce
               ,NVL(A.pole_reinf_risk_red_npv_reliab, 0) AS pole_reinf_risk_red_npv_reliab
               ,NVL(A.pole_reinf_risk_red_npv_env, 0)    AS pole_reinf_risk_red_npv_env
               ,NVL(A.pole_reinf_risk_red_npv_fin, 0)    AS pole_reinf_risk_red_npv_fin
               ,NVL(A.pole_reinf_risk_red_sif, 0)        AS pole_reinf_risk_red_sif
               ,NVL(A.pole_reinf_risk_red_pubsaf_sif, 0) AS pole_reinf_risk_red_pubsaf_sif
               ,NVL(A.pole_reinf_risk_red_wforce_sif, 0) AS pole_reinf_risk_red_wforce_sif
               ,NVL(A.pole_reinf_risk_red_reliab_sif, 0) AS pole_reinf_risk_red_reliab_sif
               ,NVL(A.pole_reinf_risk_red_env_sif, 0)    AS pole_reinf_risk_red_env_sif
               ,NVL(A.pole_reinf_risk_red_fin_sif, 0)    AS pole_reinf_risk_red_fin_sif
               ,NVL(A.eqp_repl_risk_red, 0)              AS eqp_repl_risk_red
               ,NVL(A.eqp_repl_risk_red_npv, 0)          AS eqp_repl_risk_red_npv
               ,NVL(A.eqp_repl_risk_red_npv_pubsaf, 0)   AS eqp_repl_risk_red_npv_pubsaf
               ,NVL(A.eqp_repl_risk_red_npv_wforce, 0)   AS eqp_repl_risk_red_npv_wforce
               ,NVL(A.eqp_repl_risk_red_npv_reliab, 0)   AS eqp_repl_risk_red_npv_reliab
               ,NVL(A.eqp_repl_risk_red_npv_env, 0)      AS eqp_repl_risk_red_npv_env
               ,NVL(A.eqp_repl_risk_red_npv_fin, 0)      AS eqp_repl_risk_red_npv_fin
               ,NVL(A.eqp_repl_risk_red_sif, 0)          AS eqp_repl_risk_red_sif
               ,NVL(A.eqp_repl_risk_red_pubsaf_sif, 0)   AS eqp_repl_risk_red_pubsaf_sif
               ,NVL(A.eqp_repl_risk_red_wforce_sif, 0)   AS eqp_repl_risk_red_wforce_sif
               ,NVL(A.eqp_repl_risk_red_reliab_sif, 0)   AS eqp_repl_risk_red_reliab_sif
               ,NVL(A.eqp_repl_risk_red_env_sif, 0)      AS eqp_repl_risk_red_env_sif
               ,NVL(A.eqp_repl_risk_red_fin_sif, 0)      AS eqp_repl_risk_red_fin_sif
               ,NVL(A.xstay_repl_risk_red, 0)          AS xstay_repl_risk_red
               ,NVL(A.xstay_repl_risk_red_npv, 0)      AS xstay_repl_risk_red_npv
               ,NVL(A.xstay_repl_risk_red_npv_pubsaf, 0)  AS xstay_repl_risk_red_npv_pubsaf
               ,NVL(A.xstay_repl_risk_red_npv_wforce, 0)  AS xstay_repl_risk_red_npv_wforce
               ,NVL(A.xstay_repl_risk_red_npv_reliab, 0)  AS xstay_repl_risk_red_npv_reliab
               ,NVL(A.xstay_repl_risk_red_npv_env, 0)     AS xstay_repl_risk_red_npv_env
               ,NVL(A.xstay_repl_risk_red_npv_fin, 0)     AS xstay_repl_risk_red_npv_fin
               ,NVL(A.xstay_repl_risk_red_sif, 0)         AS xstay_repl_risk_red_sif
               ,NVL(A.xstay_repl_risk_red_pubsaf_sif, 0)  AS xstay_repl_risk_red_pubsaf_sif
               ,NVL(A.xstay_repl_risk_red_wforce_sif, 0)  AS xstay_repl_risk_red_wforce_sif
               ,NVL(A.xstay_repl_risk_red_reliab_sif, 0)  AS xstay_repl_risk_red_reliab_sif
               ,NVL(A.xstay_repl_risk_red_env_sif, 0)     AS xstay_repl_risk_red_env_sif
               ,NVL(A.xstay_repl_risk_red_fin_sif, 0)     AS xstay_repl_risk_red_fin_sif
               ,NVL(b.cond_repl_risk_red, 0)                AS cond_repl_risk_red
               ,NVL(b.cond_repl_risk_red_npv, 0)            AS cond_repl_risk_red_npv
               ,NVL(b.cond_repl_risk_red_npv_pubsaf, 0)     AS cond_repl_risk_red_npv_pubsaf
               ,NVL(b.cond_repl_risk_red_npv_wforce, 0)     AS cond_repl_risk_red_npv_wforce
               ,NVL(b.cond_repl_risk_red_npv_reliab, 0)     AS cond_repl_risk_red_npv_reliab
               ,NVL(b.cond_repl_risk_red_npv_env, 0)        AS cond_repl_risk_red_npv_env
               ,NVL(b.cond_repl_risk_red_npv_fin, 0)        AS cond_repl_risk_red_npv_fin
               ,NVL(b.cond_repl_risk_red_sif, 0)            AS cond_repl_risk_red_sif
               ,NVL(b.cond_repl_risk_red_pubsaf_sif, 0)     AS cond_repl_risk_red_pubsaf_sif
               ,NVL(b.cond_repl_risk_red_wforce_sif, 0)     AS cond_repl_risk_red_wforce_sif
               ,NVL(b.cond_repl_risk_red_reliab_sif, 0)     AS cond_repl_risk_red_reliab_sif
               ,NVL(b.cond_repl_risk_red_env_sif, 0)        AS cond_repl_risk_red_env_sif
               ,NVL(b.cond_repl_risk_red_fin_sif, 0)        AS cond_repl_risk_red_fin_sif
         FROM poles  A FULL OUTER JOIN
              bays   b
         ON A.mtzn = b.mtzn     
         AND A.selection_type = b.selection_type
         )
         ,
         mtzn_committed AS
         (
         SELECT mtzn
               ,commit_yr
         FROM   structools.st_mtzn_committed
         WHERE mtzn_committed_version_id = v_mtzn_committed_version_id
         )
         ,
         mtzn_tmp AS
         (
         SELECT A.mtzn  AS mtzn
               ,A.area_nam
               ,A.region_nam
               ,A.depot_nam
               ,b.mtzn  AS mtzn_committed
               ,b.commit_yr
         FROM structools.st_mtzn         A     LEFT OUTER JOIN
              mtzn_committed b
         ON A.mtzn = b.mtzn
         WHERE A.current_ind = 'Y'
         )
         ,
         del_prog_mtzn AS
         (
         SELECT                  
            b.mtzn
           ,A.del_year
           ,A.base_run_id
         FROM structools.st_del_program_mtzn_optimised   	A	INNER JOIN
         			structools.st_mtzn														b
			ON A.mtzn = b.mtzn_opt                  
         WHERE program_id = i_program_id
         --AND   (base_run_id BETWEEN v_parent_run_id_first AND v_parent_run_id_last OR base_run_id = i_base_run_id)
         AND   base_run_id = i_base_run_id
         AND   del_year > 0 AND del_year < i_year
         )                           
         ,
         mtzn_treated AS
         (
         SELECT mtzn          AS mtzn_treated
               ,MAX(del_year) AS treated_yr
         FROM del_prog_mtzn
         GROUP BY mtzn
         )
         ,
         final_p AS
         (                 
         SELECT i_base_run_id
               ,i_year
               ,b.region_nam 
               ,A.mtzn
               ,pole_repl_cnt
               ,pole_repl_priority_cnt
               ,pole_reinf_cnt
               ,pole_reinf_priority_cnt
               ,eqp_repl_cnt
               ,eqp_repl_priority_cnt
               ,xstay_repl_cnt
               ,xstay_repl_priority_cnt
               ,span_replace_len_km
               ,cond_replace_len_km
               ,cond_replace_priority_len_km
               ,(pole_repl_risk_red + pole_reinf_risk_red + eqp_repl_risk_red + + xstay_repl_risk_red + cond_repl_risk_red)   AS sum_risk_red
               ,(pole_repl_capex + pole_reinf_capex + eqp_repl_capex + xstay_repl_capex + cond_capex)                    AS sum_capex
               ,pole_repl_capex
               ,pole_repl_labour_hrs
               ,pole_reinf_capex
               ,eqp_repl_capex
               ,xstay_repl_capex
               ,cond_capex
               ,cond_labour_hrs
               ,pole_repl_risk_red
               ,pole_reinf_risk_red
               ,eqp_repl_risk_red
               ,xstay_repl_risk_red
               ,cond_repl_risk_red
               ,(pole_repl_risk_red_npv + pole_reinf_risk_red_npv + eqp_repl_risk_red_npv + + xstay_repl_risk_red_npv + cond_repl_risk_red_npv) 
                                                                                                         AS sum_risk_red_npv
               ,pole_repl_risk_red_npv
               ,pole_reinf_risk_red_npv
               ,eqp_repl_risk_red_npv
               ,xstay_repl_risk_red_npv
               ,cond_repl_risk_red_npv
               ,(pole_repl_risk_red_sif + pole_reinf_risk_red_sif + eqp_repl_risk_red_sif + xstay_repl_risk_red_sif + cond_repl_risk_red_sif) 
                                                                                                         AS sum_risk_red_sif
               ,pole_repl_risk_red_sif
               ,pole_reinf_risk_red_sif
               ,eqp_repl_risk_red_sif
               ,xstay_repl_risk_red_sif
               ,cond_repl_risk_red_sif
               ,(pole_repl_risk_red_pubsaf_sif + pole_reinf_risk_red_pubsaf_sif + eqp_repl_risk_red_pubsaf_sif + xstay_repl_risk_red_pubsaf_sif + cond_repl_risk_red_pubsaf_sif) 
                                                                                                         AS sum_risk_red_pubsaf_sif
               ,pole_repl_risk_red_pubsaf_sif
               ,pole_reinf_risk_red_pubsaf_sif
               ,eqp_repl_risk_red_pubsaf_sif
               ,xstay_repl_risk_red_pubsaf_sif
               ,cond_repl_risk_red_pubsaf_sif
               ,(pole_repl_risk_red_wforce_sif + pole_reinf_risk_red_wforce_sif + eqp_repl_risk_red_wforce_sif + xstay_repl_risk_red_wforce_sif + cond_repl_risk_red_wforce_sif) 
                                                                                                         AS sum_risk_red_wforce_sif
               ,pole_repl_risk_red_wforce_sif
               ,pole_reinf_risk_red_wforce_sif
               ,eqp_repl_risk_red_wforce_sif
               ,xstay_repl_risk_red_wforce_sif
               ,cond_repl_risk_red_wforce_sif
               ,(pole_repl_risk_red_reliab_sif + pole_reinf_risk_red_reliab_sif + eqp_repl_risk_red_reliab_sif + xstay_repl_risk_red_reliab_sif + cond_repl_risk_red_reliab_sif) 
                                                                                                         AS sum_risk_red_reliab_sif
               ,pole_repl_risk_red_reliab_sif
               ,pole_reinf_risk_red_reliab_sif
               ,eqp_repl_risk_red_reliab_sif
               ,xstay_repl_risk_red_reliab_sif
               ,cond_repl_risk_red_reliab_sif
               ,(pole_repl_risk_red_env_sif + pole_reinf_risk_red_env_sif + eqp_repl_risk_red_env_sif + xstay_repl_risk_red_env_sif + cond_repl_risk_red_env_sif) 
                                                                                                         AS sum_risk_red_env_sif
               ,(pole_repl_risk_red_fin_sif + pole_reinf_risk_red_fin_sif + eqp_repl_risk_red_fin_sif + xstay_repl_risk_red_fin_sif + cond_repl_risk_red_fin_sif) 
                                                                                                         AS sum_risk_red_fin_sif
               ,pole_repl_risk_red_env_sif
               ,pole_repl_risk_red_fin_sif
               ,pole_reinf_risk_red_env_sif
               ,pole_reinf_risk_red_fin_sif
               ,eqp_repl_risk_red_env_sif
               ,eqp_repl_risk_red_fin_sif
               ,xstay_repl_risk_red_env_sif
               ,xstay_repl_risk_red_fin_sif
               ,cond_repl_risk_red_env_sif
               ,cond_repl_risk_red_fin_sif
               ,(pole_repl_risk_red_npv_pubsaf + pole_reinf_risk_red_npv_pubsaf + eqp_repl_risk_red_npv_pubsaf + xstay_repl_risk_red_npv_pubsaf + cond_repl_risk_red_npv_pubsaf) 
                                                                                                         AS sum_risk_red_npv_pubsaf
               ,pole_repl_risk_red_npv_pubsaf
               ,pole_reinf_risk_red_npv_pubsaf
               ,eqp_repl_risk_red_npv_pubsaf
               ,xstay_repl_risk_red_npv_pubsaf
               ,cond_repl_risk_red_npv_pubsaf
               ,(pole_repl_risk_red_npv_wforce + pole_reinf_risk_red_npv_wforce + eqp_repl_risk_red_npv_wforce + xstay_repl_risk_red_npv_wforce + cond_repl_risk_red_npv_wforce) 
                                                                                                         AS sum_risk_red_npv_wforce
               ,pole_repl_risk_red_npv_wforce
               ,pole_reinf_risk_red_npv_wforce
               ,eqp_repl_risk_red_npv_wforce
               ,xstay_repl_risk_red_npv_wforce
               ,cond_repl_risk_red_npv_wforce
               ,(pole_repl_risk_red_npv_reliab + pole_reinf_risk_red_npv_reliab + eqp_repl_risk_red_npv_reliab + xstay_repl_risk_red_npv_reliab + cond_repl_risk_red_npv_reliab) 
                                                                                                         AS sum_risk_red_npv_reliab
               ,pole_repl_risk_red_npv_reliab
               ,pole_reinf_risk_red_npv_reliab
               ,eqp_repl_risk_red_npv_reliab
               ,xstay_repl_risk_red_npv_reliab
               ,cond_repl_risk_red_npv_reliab
               ,(pole_repl_risk_red_npv_env + pole_reinf_risk_red_npv_env + eqp_repl_risk_red_npv_env + xstay_repl_risk_red_npv_env + cond_repl_risk_red_npv_env) 
                                                                                                         AS sum_risk_red_npv_env
               ,(pole_repl_risk_red_npv_fin + pole_reinf_risk_red_npv_fin + eqp_repl_risk_red_npv_fin + xstay_repl_risk_red_npv_fin + cond_repl_risk_red_npv_fin) 
                                                                                                         AS sum_risk_red_npv_fin
               ,pole_repl_risk_red_npv_env
               ,pole_repl_risk_red_npv_fin
               ,pole_reinf_risk_red_npv_env
               ,pole_reinf_risk_red_npv_fin
               ,eqp_repl_risk_red_npv_env
               ,eqp_repl_risk_red_npv_fin
               ,xstay_repl_risk_red_npv_env
               ,xstay_repl_risk_red_npv_fin
               ,cond_repl_risk_red_npv_env
               ,cond_repl_risk_red_npv_fin
               ,i_program_id
               ,b.area_nam
               ,CASE
                  WHEN b.mtzn_committed > ' ' AND ((i_year - b.commit_yr) <= i_mtzn_exclude_yrs) THEN 'Y'
                  WHEN c.mtzn_treated IS NOT NULL AND ((i_year - treated_yr) <= i_mtzn_exclude_yrs) THEN 'Y'
                  WHEN d.reason_desc IS NOT NULL AND ((i_year - d.calendar_yr) <= 	i_mtzn_exclude_yrs)	THEN 'Y' 
                  ELSE                                     'N'
               END               AS mtzn_exclude
               ,b.depot_nam
           FROM poles_and_bays     A  
          INNER JOIN mtzn_tmp            b
             ON A.mtzn = b.mtzn
           LEFT OUTER JOIN mtzn_treated      c
             ON A.mtzn = c.mtzn_treated                             
           LEFT OUTER JOIN structools.st_mtzn_excl d
             ON d.mtzn = A.mtzn           
            AND d.calendar_yr = i_year
            AND d.prog_typ = 'ZBAM'
            AND d.version_id = i_version_id                            
         WHERE A.selection_type = 'P'
         )
         ,
         final_s AS
         (                 
         SELECT i_base_run_id
               ,i_year
               ,b.region_nam 
               ,A.mtzn
               ,pole_repl_cnt    AS s_pole_repl_cnt
               ,pole_repl_priority_cnt    AS s_pole_repl_priority_cnt
               ,pole_reinf_cnt   AS s_pole_reinf_cnt
               ,pole_reinf_priority_cnt   AS s_pole_reinf_priority_cnt
               ,eqp_repl_cnt     AS s_eqp_repl_cnt
               ,eqp_repl_priority_cnt     AS s_eqp_repl_priority_cnt
               ,xstay_repl_cnt     AS s_xstay_repl_cnt
               ,xstay_repl_priority_cnt     AS s_xstay_repl_priority_cnt
               ,span_replace_len_km AS s_span_replace_len_km
               ,cond_replace_len_km AS s_cond_replace_len_km
               ,cond_replace_priority_len_km AS s_cond_replace_priority_len_km
               ,(pole_repl_risk_red + pole_reinf_risk_red + eqp_repl_risk_red + xstay_repl_risk_red + cond_repl_risk_red)   AS s_sum_risk_red
               ,(pole_repl_capex + pole_reinf_capex + eqp_repl_capex + xstay_repl_capex + cond_capex)                    AS s_sum_capex
               ,pole_repl_capex  AS s_pole_repl_capex
               ,pole_repl_labour_hrs  AS s_pole_repl_labour_hrs
               ,pole_reinf_capex AS s_pole_reinf_capex
               ,eqp_repl_capex   AS s_eqp_repl_capex
               ,xstay_repl_capex   AS s_xstay_repl_capex
               ,cond_capex       AS s_con_capex
               ,cond_labour_hrs       AS s_cond_labour_hrs
               ,pole_repl_risk_red  AS s_pole_repl_risk_red
               ,pole_reinf_risk_red AS s_pole_reinf_risk_red
               ,eqp_repl_risk_red   AS s_eqp_repl_risk_red
               ,xstay_repl_risk_red   AS s_xstay_repl_risk_red
               ,cond_repl_risk_red  AS s_con_repl_risk_red
               ,(pole_repl_risk_red_npv + pole_reinf_risk_red_npv + eqp_repl_risk_red_npv + xstay_repl_risk_red_npv + cond_repl_risk_red_npv) 
                                                                                                         AS s_sum_risk_red_npv
               ,pole_repl_risk_red_npv    AS s_pole_repl_risk_red_npv
               ,pole_reinf_risk_red_npv   AS s_pole_reinf_risk_red_npv
               ,eqp_repl_risk_red_npv     AS s_eqp_repl_risk_red_npv
               ,xstay_repl_risk_red_npv     AS s_xstay_repl_risk_red_npv
               ,cond_repl_risk_red_npv    AS s_con_repl_risk_red_npv
               ,(pole_repl_risk_red_sif + pole_reinf_risk_red_sif + eqp_repl_risk_red_sif + xstay_repl_risk_red_sif + cond_repl_risk_red_sif) 
                                                                                                         AS s_sum_risk_red_sf
               ,pole_repl_risk_red_sif    AS s_pole_repl_risk_red_sf
               ,pole_reinf_risk_red_sif   AS s_pole_reinf_risk_red_sf
               ,eqp_repl_risk_red_sif     AS s_eqp_repl_risk_red_sf
               ,xstay_repl_risk_red_sif     AS s_xstay_repl_risk_red_sf
               ,cond_repl_risk_red_sif    AS s_con_repl_risk_red_sf
               ,(pole_repl_risk_red_pubsaf_sif + pole_reinf_risk_red_pubsaf_sif + eqp_repl_risk_red_pubsaf_sif + xstay_repl_risk_red_pubsaf_sif + cond_repl_risk_red_pubsaf_sif) 
                                                                                                         AS s_sum_risk_red_psaf_sf
               ,pole_repl_risk_red_pubsaf_sif      AS s_pole_repl_risk_red_psaf_sf
               ,pole_reinf_risk_red_pubsaf_sif     AS s_pole_rein_risk_red_psaf_sf
               ,eqp_repl_risk_red_pubsaf_sif       AS s_eqp_repl_risk_red_psaf_sf
               ,xstay_repl_risk_red_pubsaf_sif       AS s_xstay_repl_risk_red_psaf_sf
               ,cond_repl_risk_red_pubsaf_sif      AS s_con_repl_risk_red_psaf_sf
               ,(pole_repl_risk_red_wforce_sif + pole_reinf_risk_red_wforce_sif + eqp_repl_risk_red_wforce_sif + xstay_repl_risk_red_wforce_sif + cond_repl_risk_red_wforce_sif) 
                                                                                                         AS s_sum_risk_red_wf_sf
               ,pole_repl_risk_red_wforce_sif      AS s_pole_repl_risk_red_wf_sf
               ,pole_reinf_risk_red_wforce_sif     AS s_pole_rein_risk_red_wf_sf
               ,eqp_repl_risk_red_wforce_sif       AS s_eqp_repl_risk_red_wf_sf
               ,xstay_repl_risk_red_wforce_sif       AS s_xstay_repl_risk_red_wf_sf
               ,cond_repl_risk_red_wforce_sif      AS s_con_repl_risk_red_wf_sf
               ,(pole_repl_risk_red_reliab_sif + pole_reinf_risk_red_reliab_sif + eqp_repl_risk_red_reliab_sif + xstay_repl_risk_red_reliab_sif + cond_repl_risk_red_reliab_sif) 
                                                                                                         AS s_sum_risk_red_rel_sif
               ,pole_repl_risk_red_reliab_sif         AS s_pole_repl_risk_red_rel_sf
               ,pole_reinf_risk_red_reliab_sif        AS s_pole_rein_risk_red_rel_sf
               ,eqp_repl_risk_red_reliab_sif          AS s_eqp_repl_risk_red_rel_sf
               ,xstay_repl_risk_red_reliab_sif          AS s_xstay_repl_risk_red_rel_sf
               ,cond_repl_risk_red_reliab_sif         AS s_con_repl_risk_red_rel_sf
               ,(pole_repl_risk_red_env_sif + pole_reinf_risk_red_env_sif + eqp_repl_risk_red_env_sif + xstay_repl_risk_red_env_sif + cond_repl_risk_red_env_sif) 
                                                                                                         AS s_sum_risk_red_env_sf
               ,pole_repl_risk_red_env_sif         AS s_pole_repl_risk_red_env_sf
               ,pole_reinf_risk_red_env_sif        AS s_pole_rein_risk_red_env_sf
               ,eqp_repl_risk_red_env_sif          AS s_eqp_repl_risk_red_env_sf
               ,xstay_repl_risk_red_env_sif          AS s_xstay_repl_risk_red_env_sf
               ,cond_repl_risk_red_env_sif         AS s_con_repl_risk_red_env_sf
               ,(pole_repl_risk_red_fin_sif + pole_reinf_risk_red_fin_sif + eqp_repl_risk_red_fin_sif + xstay_repl_risk_red_fin_sif + cond_repl_risk_red_fin_sif) 
                                                                                                         AS s_sum_risk_red_fin_sf
               ,pole_repl_risk_red_fin_sif         AS s_pole_repl_risk_red_fin_sf
               ,pole_reinf_risk_red_fin_sif        AS s_pole_rein_risk_red_fin_sf
               ,eqp_repl_risk_red_fin_sif          AS s_eqp_repl_risk_red_fin_sf
               ,xstay_repl_risk_red_fin_sif          AS s_xstay_repl_risk_red_fin_sf
               ,cond_repl_risk_red_fin_sif         AS s_con_repl_risk_red_fin_sf
               ,(pole_repl_risk_red_npv_pubsaf + pole_reinf_risk_red_npv_pubsaf + eqp_repl_risk_red_npv_pubsaf + xstay_repl_risk_red_npv_pubsaf + cond_repl_risk_red_npv_pubsaf) 
                                                                                                         AS s_sum_risk_red_npv_psaf
               ,pole_repl_risk_red_npv_pubsaf      AS s_pole_repl_risk_red_npv_pbsaf
               ,pole_reinf_risk_red_npv_pubsaf     AS s_pole_rein_risk_red_npv_psaf
               ,eqp_repl_risk_red_npv_pubsaf       AS s_eqp_repl_risk_red_npv_psaf
               ,xstay_repl_risk_red_npv_pubsaf       AS s_xstay_repl_risk_red_npv_psaf
               ,cond_repl_risk_red_npv_pubsaf      AS s_con_repl_risk_red_npv_psaf
               ,(pole_repl_risk_red_npv_wforce + pole_reinf_risk_red_npv_wforce + eqp_repl_risk_red_npv_wforce + xstay_repl_risk_red_npv_wforce + cond_repl_risk_red_npv_wforce) 
                                                                                                         AS s_sum_risk_red_npv_wf
               ,pole_repl_risk_red_npv_wforce      AS s_pole_repl_risk_red_npv_wf
               ,pole_reinf_risk_red_npv_wforce     AS s_pole_rein_risk_red_npv_wf
               ,eqp_repl_risk_red_npv_wforce       AS s_eqp_repl_risk_red_npv_wf
               ,xstay_repl_risk_red_npv_wforce       AS s_xstay_repl_risk_red_npv_wf
               ,cond_repl_risk_red_npv_wforce      AS s_cond_repl_risk_red_npv_wf
               ,(pole_repl_risk_red_npv_reliab + pole_reinf_risk_red_npv_reliab + eqp_repl_risk_red_npv_reliab + xstay_repl_risk_red_npv_reliab + cond_repl_risk_red_npv_reliab) 
                                                                                                         AS s_sum_risk_red_npv_rel
               ,pole_repl_risk_red_npv_reliab      AS s_pole_repl_risk_red_npv_rel
               ,pole_reinf_risk_red_npv_reliab     AS s_pole_rein_risk_red_npv_rel
               ,eqp_repl_risk_red_npv_reliab       AS s_eqp_repl_risk_red_npv_rel
               ,xstay_repl_risk_red_npv_reliab       AS s_xstay_repl_risk_red_npv_rel
               ,cond_repl_risk_red_npv_reliab      AS s_cond_repl_risk_red_npv_rel
               ,(pole_repl_risk_red_npv_env + pole_reinf_risk_red_npv_env + eqp_repl_risk_red_npv_env + xstay_repl_risk_red_npv_env + cond_repl_risk_red_npv_env) 
                                                                                                         AS s_sum_risk_red_npv_env
               ,pole_repl_risk_red_npv_env         AS s_pole_repl_risk_red_npv_env
               ,pole_reinf_risk_red_npv_env        AS s_pole_rein_risk_red_npv_env
               ,eqp_repl_risk_red_npv_env          AS s_eqp_repl_risk_red_npv_env
               ,xstay_repl_risk_red_npv_env      AS s_xstay_repl_risk_red_npv_env
               ,cond_repl_risk_red_npv_env         AS s_cond_repl_risk_red_npv_env
               ,(pole_repl_risk_red_npv_fin + pole_reinf_risk_red_npv_fin + eqp_repl_risk_red_npv_fin + xstay_repl_risk_red_npv_fin + cond_repl_risk_red_npv_fin) 
                                                                                                         AS s_sum_risk_red_npv_fin
               ,pole_repl_risk_red_npv_fin         AS s_pole_repl_risk_red_npv_fin
               ,pole_reinf_risk_red_npv_fin        AS s_pole_rein_risk_red_npv_fin
               ,eqp_repl_risk_red_npv_fin          AS s_eqp_repl_risk_red_npv_fin
               ,xstay_repl_risk_red_npv_fin      AS s_xstay_repl_risk_red_npv_fin
               ,cond_repl_risk_red_npv_fin         AS s_cond_repl_risk_red_npv_fin
               ,i_program_id
               ,b.area_nam 
               ,CASE
                  WHEN b.mtzn_committed > ' ' AND ((i_year - b.commit_yr) <= i_mtzn_exclude_yrs) THEN 'Y'
                  WHEN c.mtzn_treated IS NOT NULL AND ((i_year - treated_yr) <= i_mtzn_exclude_yrs) THEN 'Y'
                  WHEN d.reason_desc IS NOT NULL AND ((i_year - d.calendar_yr) <= 	i_mtzn_exclude_yrs)	THEN 'Y'
                  ELSE                                     'N'
               END               AS mtzn_exclude
               ,b.depot_nam
           FROM poles_and_bays     A  
          INNER JOIN mtzn_tmp            b
             ON A.mtzn = b.mtzn
           LEFT OUTER JOIN mtzn_treated      c
             ON A.mtzn = c.mtzn_treated            
           LEFT OUTER JOIN structools.st_mtzn_excl d
             ON d.mtzn = A.mtzn           
            AND d.calendar_yr = i_year
            AND d.prog_typ = 'ZBAM'
            AND d.version_id = i_version_id
          WHERE A.selection_type = 'S'
         )
         SELECT NVL(A.i_base_run_id, b.i_base_run_id)
               ,NVL(A.i_year, b.i_year)
               ,NVL(A.region_nam, b.region_nam) 
               ,NVL(A.mtzn, b.mtzn)
               ,NVL(A.pole_repl_cnt,0)
               ,NVL(A.pole_reinf_cnt,0)
               ,NVL(A.eqp_repl_cnt,0)
               ,NVL(A.span_replace_len_km,0)
               ,NVL(A.sum_risk_red,0)
               ,NVL(A.sum_capex,0)
               ,NVL(A.pole_repl_capex,0)
               ,NVL(A.pole_reinf_capex,0)
               ,NVL(A.eqp_repl_capex,0)
               ,NVL(A.cond_capex,0)
               ,NVL(A.pole_repl_risk_red,0)
               ,NVL(A.pole_reinf_risk_red,0)
               ,NVL(A.eqp_repl_risk_red,0)
               ,NVL(A.cond_repl_risk_red,0)
               ,NVL(A.sum_risk_red_npv,0)
               ,NVL(A.pole_repl_risk_red_npv,0)
               ,NVL(A.pole_reinf_risk_red_npv,0)
               ,NVL(A.eqp_repl_risk_red_npv,0)
               ,NVL(A.cond_repl_risk_red_npv,0)
               ,NVL(A.sum_risk_red_sif,0)
               ,NVL(A.pole_repl_risk_red_sif,0)
               ,NVL(A.pole_reinf_risk_red_sif,0)
               ,NVL(A.eqp_repl_risk_red_sif,0)
               ,NVL(A.cond_repl_risk_red_sif,0)
               ,NVL(A.sum_risk_red_pubsaf_sif,0)
               ,NVL(A.pole_repl_risk_red_pubsaf_sif,0)
               ,NVL(A.pole_reinf_risk_red_pubsaf_sif,0)
               ,NVL(A.eqp_repl_risk_red_pubsaf_sif,0)
               ,NVL(A.cond_repl_risk_red_pubsaf_sif,0)
               ,NVL(A.sum_risk_red_wforce_sif,0)
               ,NVL(A.pole_repl_risk_red_wforce_sif,0)
               ,NVL(A.pole_reinf_risk_red_wforce_sif,0)
               ,NVL(A.eqp_repl_risk_red_wforce_sif,0)
               ,NVL(A.cond_repl_risk_red_wforce_sif,0)
               ,NVL(A.sum_risk_red_reliab_sif,0)
               ,NVL(A.pole_repl_risk_red_reliab_sif,0)
               ,NVL(A.pole_reinf_risk_red_reliab_sif,0)
               ,NVL(A.eqp_repl_risk_red_reliab_sif,0)
               ,NVL(A.cond_repl_risk_red_reliab_sif,0)
               ,NVL(A.sum_risk_red_env_sif,0)
               ,NVL(A.pole_repl_risk_red_env_sif,0)
               ,NVL(A.pole_reinf_risk_red_env_sif,0)
               ,NVL(A.eqp_repl_risk_red_env_sif,0)
               ,NVL(A.cond_repl_risk_red_env_sif,0)
               ,NVL(A.sum_risk_red_npv_pubsaf,0)
               ,NVL(A.pole_repl_risk_red_npv_pubsaf,0)
               ,NVL(A.pole_reinf_risk_red_npv_pubsaf,0)
               ,NVL(A.eqp_repl_risk_red_npv_pubsaf,0)
               ,NVL(A.cond_repl_risk_red_npv_pubsaf,0)
               ,NVL(A.sum_risk_red_npv_wforce,0)
               ,NVL(A.pole_repl_risk_red_npv_wforce,0)
               ,NVL(A.pole_reinf_risk_red_npv_wforce,0)
               ,NVL(A.eqp_repl_risk_red_npv_wforce,0)
               ,NVL(A.cond_repl_risk_red_npv_wforce,0)
               ,NVL(A.sum_risk_red_npv_reliab,0)
               ,NVL(A.pole_repl_risk_red_npv_reliab,0)
               ,NVL(A.pole_reinf_risk_red_npv_reliab,0)
               ,NVL(A.eqp_repl_risk_red_npv_reliab,0)
               ,NVL(A.cond_repl_risk_red_npv_reliab,0)
               ,NVL(A.sum_risk_red_npv_env,0)
               ,NVL(A.pole_repl_risk_red_npv_env,0)
               ,NVL(A.pole_reinf_risk_red_npv_env,0)
               ,NVL(A.eqp_repl_risk_red_npv_env,0)
               ,NVL(A.cond_repl_risk_red_npv_env,0)
               ,NVL(A.i_program_id, b.i_program_id)
               ,NVL(A.area_nam, b.area_nam)
               ,NVL(A.mtzn_exclude, b.mtzn_exclude)
               ,NVL(b.s_pole_repl_cnt,0)
               ,NVL(b.s_pole_reinf_cnt,0)
               ,NVL(b.s_eqp_repl_cnt,0)
               ,NVL(b.s_span_replace_len_km,0)
               ,NVL(b.s_sum_risk_red,0)
               ,NVL(b.s_sum_capex,0)
               ,NVL(b.s_pole_repl_capex,0)
               ,NVL(b.s_pole_reinf_capex,0)
               ,NVL(b.s_eqp_repl_capex,0)
               ,NVL(b.s_con_capex,0)
               ,NVL(b.s_pole_repl_risk_red,0)
               ,NVL(b.s_pole_reinf_risk_red,0)
               ,NVL(b.s_eqp_repl_risk_red,0)
               ,NVL(b.s_con_repl_risk_red,0)
               ,NVL(b.s_sum_risk_red_npv,0)
               ,NVL(b.s_pole_repl_risk_red_npv,0)
               ,NVL(b.s_pole_reinf_risk_red_npv,0)
               ,NVL(b.s_eqp_repl_risk_red_npv,0)
               ,NVL(b.s_con_repl_risk_red_npv,0)
               ,NVL(b.s_sum_risk_red_sf,0)
               ,NVL(b.s_pole_repl_risk_red_sf,0)
               ,NVL(b.s_pole_reinf_risk_red_sf,0)
               ,NVL(b.s_eqp_repl_risk_red_sf,0)
               ,NVL(b.s_con_repl_risk_red_sf,0)
               ,NVL(b.s_sum_risk_red_psaf_sf,0)
               ,NVL(b.s_pole_repl_risk_red_psaf_sf,0)
               ,NVL(b.s_pole_rein_risk_red_psaf_sf,0)
               ,NVL(b.s_eqp_repl_risk_red_psaf_sf,0)
               ,NVL(b.s_con_repl_risk_red_psaf_sf,0)
               ,NVL(b.s_sum_risk_red_wf_sf,0)
               ,NVL(b.s_pole_repl_risk_red_wf_sf,0)
               ,NVL(b.s_pole_rein_risk_red_wf_sf,0)
               ,NVL(b.s_eqp_repl_risk_red_wf_sf,0)
               ,NVL(b.s_con_repl_risk_red_wf_sf,0)
               ,NVL(b.s_sum_risk_red_rel_sif,0)
               ,NVL(b.s_pole_repl_risk_red_rel_sf,0)
               ,NVL(b.s_pole_rein_risk_red_rel_sf,0)
               ,NVL(b.s_eqp_repl_risk_red_rel_sf,0)
               ,NVL(b.s_con_repl_risk_red_rel_sf,0)
               ,NVL(b.s_sum_risk_red_env_sf,0)
               ,NVL(b.s_pole_repl_risk_red_env_sf,0)
               ,NVL(b.s_pole_rein_risk_red_env_sf,0)
               ,NVL(b.s_eqp_repl_risk_red_env_sf,0)
               ,NVL(b.s_con_repl_risk_red_env_sf,0)
               ,NVL(b.s_sum_risk_red_npv_psaf,0)
               ,NVL(b.s_pole_repl_risk_red_npv_pbsaf,0)
               ,NVL(b.s_pole_rein_risk_red_npv_psaf,0)
               ,NVL(b.s_eqp_repl_risk_red_npv_psaf,0)
               ,NVL(b.s_con_repl_risk_red_npv_psaf,0)
               ,NVL(b.s_sum_risk_red_npv_wf,0)
               ,NVL(b.s_pole_repl_risk_red_npv_wf,0)
               ,NVL(b.s_pole_rein_risk_red_npv_wf,0)
               ,NVL(b.s_eqp_repl_risk_red_npv_wf,0)
               ,NVL(b.s_cond_repl_risk_red_npv_wf,0)
               ,NVL(b.s_sum_risk_red_npv_rel,0)
               ,NVL(b.s_pole_repl_risk_red_npv_rel,0)
               ,NVL(b.s_pole_rein_risk_red_npv_rel,0)
               ,NVL(b.s_eqp_repl_risk_red_npv_rel,0)
               ,NVL(b.s_cond_repl_risk_red_npv_rel,0)
               ,NVL(b.s_sum_risk_red_npv_env,0)
               ,NVL(b.s_pole_repl_risk_red_npv_env,0)
               ,NVL(b.s_pole_rein_risk_red_npv_env,0)
               ,NVL(b.s_eqp_repl_risk_red_npv_env,0)
               ,NVL(b.s_cond_repl_risk_red_npv_env,0)
               ,NVL(A.depot_nam,b.depot_nam)
               ,NVL(A.cond_replace_len_km,0)
               ,NVL(b.s_cond_replace_len_km,0)
               ,NVL(A.pole_repl_priority_cnt,0)
               ,NVL(A.pole_reinf_priority_cnt,0)
               ,NVL(A.eqp_repl_priority_cnt,0)
               ,NVL(A.cond_replace_priority_len_km,0)
               ,NVL(b.s_pole_repl_priority_cnt,0)
               ,NVL(b.s_pole_reinf_priority_cnt,0)
               ,NVL(b.s_eqp_repl_priority_cnt,0)
               ,NVL(b.s_cond_replace_priority_len_km,0)
               ,NVL(A.xstay_repl_cnt,0)
               ,NVL(A.xstay_repl_capex,0)
               ,NVL(A.xstay_repl_risk_red,0)
               ,NVL(A.xstay_repl_risk_red_npv,0)
               ,NVL(A.xstay_repl_risk_red_sif,0)
               ,NVL(A.xstay_repl_risk_red_pubsaf_sif,0)
               ,NVL(A.xstay_repl_risk_red_wforce_sif,0)
               ,NVL(A.xstay_repl_risk_red_reliab_sif,0)
               ,NVL(A.xstay_repl_risk_red_env_sif,0)
               ,NVL(A.xstay_repl_risk_red_npv_pubsaf,0)
               ,NVL(A.xstay_repl_risk_red_npv_wforce,0)
               ,NVL(A.xstay_repl_risk_red_npv_reliab,0)
               ,NVL(A.xstay_repl_risk_red_npv_env,0)
               ,NVL(b.s_xstay_repl_cnt,0)
               ,NVL(b.s_xstay_repl_capex,0)
               ,NVL(b.s_xstay_repl_risk_red,0)
               ,NVL(b.s_xstay_repl_risk_red_npv,0)
               ,NVL(b.s_xstay_repl_risk_red_sf,0)
               ,NVL(b.s_xstay_repl_risk_red_psaf_sf,0)
               ,NVL(b.s_xstay_repl_risk_red_wf_sf,0)
               ,NVL(b.s_xstay_repl_risk_red_rel_sf,0)
               ,NVL(b.s_xstay_repl_risk_red_env_sf,0)
               ,NVL(b.s_xstay_repl_risk_red_npv_psaf,0)
               ,NVL(b.s_xstay_repl_risk_red_npv_wf,0)
               ,NVL(b.s_xstay_repl_risk_red_npv_rel,0)
               ,NVL(b.s_xstay_repl_risk_red_npv_env,0)
               ,NVL(A.xstay_repl_priority_cnt,0)
               ,NVL(b.s_xstay_repl_priority_cnt,0)
               ,NVL(A.sum_risk_red_fin_sif,0)
               ,NVL(A.pole_repl_risk_red_fin_sif,0)
               ,NVL(A.pole_reinf_risk_red_fin_sif,0)
               ,NVL(A.eqp_repl_risk_red_fin_sif,0)
               ,NVL(A.cond_repl_risk_red_fin_sif,0)
               ,NVL(A.sum_risk_red_npv_fin,0)
               ,NVL(A.pole_repl_risk_red_npv_fin,0)
               ,NVL(A.pole_reinf_risk_red_npv_fin,0)
               ,NVL(A.eqp_repl_risk_red_npv_fin,0)
               ,NVL(A.cond_repl_risk_red_npv_fin,0)
               ,NVL(b.s_sum_risk_red_fin_sf,0)
               ,NVL(b.s_pole_repl_risk_red_fin_sf,0)
               ,NVL(b.s_pole_rein_risk_red_fin_sf,0)
               ,NVL(b.s_eqp_repl_risk_red_fin_sf,0)
               ,NVL(b.s_con_repl_risk_red_fin_sf,0)
               ,NVL(b.s_sum_risk_red_npv_fin,0)
               ,NVL(b.s_pole_repl_risk_red_npv_fin,0)
               ,NVL(b.s_pole_rein_risk_red_npv_fin,0)
               ,NVL(b.s_eqp_repl_risk_red_npv_fin,0)
               ,NVL(b.s_cond_repl_risk_red_npv_fin,0)
               ,NVL(A.xstay_repl_risk_red_fin_sif,0)
               ,NVL(A.xstay_repl_risk_red_npv_fin,0)
               ,NVL(b.s_xstay_repl_risk_red_fin_sf,0)
               ,NVL(b.s_xstay_repl_risk_red_npv_fin,0)
               ,NVL(A.pole_repl_labour_hrs,0)
               ,NVL(b.s_pole_repl_labour_hrs,0)
               ,NVL(A.cond_labour_hrs,0)
               ,NVL(b.s_cond_labour_hrs,0)
         FROM  final_p      A  
         FULL OUTER JOIN
               final_s      b
         ON   A.i_base_run_id = b.i_base_run_id
         AND A.i_year = b.i_year
         AND A.mtzn = b.mtzn                
         AND A.i_program_id  = b.i_program_id
         ;
         
      /* Populate zbam reinforcements counts, for each event_reason */
      DELETE FROM structools.st_gtt_actioned_poles;
      
      INSERT INTO structools.st_gtt_actioned_poles     -- = del_prog_pickids
         WITH del_prog_mtzn AS
               (
               SELECT b.mtzn                 
                 ,A.del_year
                 ,A.base_run_id
               FROM structools.st_del_program_mtzn_optimised	A	INNER JOIN
               			 structools.st_mtzn													b
					ON A.mtzn = b.mtzn_opt                            
               WHERE program_id = i_program_id
               AND   base_run_id = i_base_run_id
               AND   del_year > 0 AND del_year < i_year
               )
               ,
         		del_prog_mtzn_reinf AS
               (
               SELECT b.mtzn
                 ,A.del_year
                 ,A.base_run_id
               FROM structools.st_del_program_reinf_opt   	A	INNER JOIN
               			structools.st_mtzn													b
					ON A.mtzn = b.mtzn_opt                        	
               WHERE program_id = i_program_id
               AND   base_run_id = i_base_run_id
               AND   del_year > 0 AND del_year < i_year
               )                           
               SELECT /*+ parallel */ 
               				DISTINCT A.pick_id
               FROM structools.st_temp_strategy_run          A    INNER JOIN
                    del_prog_mtzn b
               ON A.mtzn_grp = B.MTZN
               AND A.calendar_year = B.DEL_YEAR
               WHERE A.equip_grp_id = 'PWOD'
               AND A.part_cde = ' '
               AND A.event > ' ' 
               AND A.calendar_year < i_year
               AND (A.pickid_suppressed = 'N' OR A.pickid_suppressed = 'P' AND A.event = 'REINFORCE')
               UNION
               SELECT /*+ parallel */ 
               				DISTINCT A.pick_id
               FROM structools.st_temp_strategy_run          A    INNER JOIN
                    del_prog_mtzn_reinf b
               ON A.mtzn_grp = B.MTZN
               AND A.calendar_year = B.DEL_YEAR
               WHERE A.equip_grp_id = 'PWOD'
               AND A.part_cde = ' '
               AND A.event = 'REINFORCE'
               AND A.pickid_suppressed IN ('N','P')
               AND A.calendar_year < i_year
               UNION
               SELECT DISTINCT pick_id
               FROM   structools.st_del_program_sniper
               WHERE  base_run_id = i_base_run_id
               AND    program_id = i_program_id
               AND    del_year < i_year
               AND 	equip_typ = 'PWOD'
               AND part_cde = ' '
               UNION
               SELECT /*+ ORDERED */ 
               				DISTINCT b.pick_id
               FROM structools.st_del_program_seg_optimised A  INNER JOIN
                    structools.st_temp_strategy_run     b
               ON A.cons_segment_id = b.cons_segment_id       
               AND A.del_year  = B.calendar_year      
               WHERE A.base_run_id  = i_base_run_id
               AND A.program_id = i_program_id
               AND A.del_year > 0 AND A.del_year < i_year
               AND b.equip_grp_id = 'PWOD'
               AND b.part_cde = ' '
               AND b.event > ' ' --IS NOT NULL
               AND (b.pickid_suppressed = 'N' OR b.pickid_suppressed = 'P' AND event = 'REINFORCE')
      ;         

		INSERT INTO structools.st_del_program_zbam_reinf_summ
      	(
          base_run_id
         ,program_id
         ,del_year
         ,mtzn
         ,p1_summ
      	,p2_summ
         ,p3_summ
      	,p4_summ
         ,p5_summ
      	,p6_summ
         ,p7_summ
      	,p8_summ
         ,p9_summ
         )
         WITH strategy_reinf_curr_year AS
         (
         SELECT pick_id
         			 ,mtzn_grp AS mtzn
                   ,CASE 
                     WHEN event_reason LIKE 'P1%'			THEN 'P1'
                     WHEN event_reason LIKE 'P2%'			THEN 'P2'
                     WHEN event_reason LIKE 'P3%'			THEN 'P3'
                     WHEN event_reason LIKE 'P4%'			THEN 'P4'
                     WHEN event_reason LIKE 'P5%'			THEN 'P5'
                     WHEN event_reason LIKE 'P6%'			THEN 'P6'
                     WHEN event_reason LIKE 'P7%'			THEN 'P7'
                     WHEN event_reason LIKE 'P8%'			THEN 'P8'
                     WHEN event_reason LIKE 'P9%'			THEN 'P9'    
                     ELSE																		  event_reason           				
                   END									AS event_reason
         FROM structools.st_temp_strategy_run       
         WHERE equip_grp_id = 'PWOD' 
         AND part_cde = ' '
         AND calendar_year = i_year
         AND event = 'REINFORCE'
         AND pickid_suppressed IN ('N','P')
         AND pick_id NOT IN (SELECT pick_id FROM structools.st_gtt_actioned_poles)
         )
         ,
         poles_reinf_summ AS
         (
         SELECT mtzn
                  	,event_reason
               	  ,count(*) AS reinf_cnt
         FROM strategy_reinf_curr_year A
         GROUP BY mtzn, event_reason
         )
         ,
         event_counts AS
         (
         SELECT * FROM
         (
         SELECT  mtzn
                     ,reinf_cnt
                     ,event_reason
         FROM poles_reinf_summ
         )
         PIVOT    (max(reinf_cnt) AS reinf_cnt
                  FOR event_reason IN ('P1' p1,'P2' p2,'P3' p3, 'P4' p4,'P5' p5,'P6' p6,'P7' p7, 'P8' p8 , 'P9' p9   ) )
         )
         SELECT i_base_run_id
                     ,i_program_id
                     ,i_year
                     ,mtzn
                     ,nvl(p1_reinf_cnt,0) AS p1_reinf_cnt
                     ,nvl(p2_reinf_cnt,0) AS p2_reinf_cnt
                     ,nvl(p3_reinf_cnt,0) AS p3_reinf_cnt
                     ,nvl(p4_reinf_cnt,0) AS p4_reinf_cnt
                     ,nvl(p5_reinf_cnt,0) AS p5_reinf_cnt
                     ,nvl(p6_reinf_cnt,0) AS p6_reinf_cnt
                     ,nvl(p7_reinf_cnt,0) AS p7_reinf_cnt
                     ,nvl(p8_reinf_cnt,0) AS p8_reinf_cnt
                     ,nvl(p9_reinf_cnt,0) AS p9_reinf_cnt
            FROM    event_counts
         ;
       
         
      COMMIT;
      
      dbms_output.put_line('run_summary_for_opensolver End '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

   EXCEPTION
      WHEN v_prog_excp THEN
         dbms_output.put_line('Exception '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('Exception '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END run_summary_for_opensolver;        
      
   /*******************************************************************************************************************************************/
   /* Populate tables st_del_program_seg_summary and  st_del_program_seg_reinf_summ.                                                                                                                                                      */
   /********************************************************************************************************************************************/
   PROCEDURE run_summary_for_segments  (i_base_run_id    IN PLS_INTEGER
                                       ,i_year           IN PLS_INTEGER
                                       ,i_program_id     IN PLS_INTEGER) IS 

      v_run_id          PLS_INTEGER;
      v_dummy1          PLS_INTEGER;
      v_dummy2          PLS_INTEGER;
      
   BEGIN
   
      BEGIN
         SELECT DISTINCT base_run_id
                        ,del_year
         INTO            v_dummy1
                        ,v_dummy2                     
         FROM structools.st_del_program_seg_summary
         WHERE  base_run_id = i_base_run_id
         AND program_id = i_program_id
         AND del_year = i_year;
         dbms_output.put_line ('run id '||i_base_run_id||' program_id '||i_program_id||' del_year '||i_year
         ||' already in the segment summary table. Delete earlier results first');            
         RAISE v_prog_excp;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
      
      BEGIN
         SELECT /*+ parallel */ DISTINCT run_id
         INTO            v_run_id
         FROM structools.st_temp_bay_strategy_run;
         IF v_run_id != i_base_run_id THEN
            dbms_output.put_line ('st_temp_bay_strategy_run does not contain data for run '||i_base_run_id);
            RAISE v_prog_excp;
         END IF;
         SELECT /*+ parallel */ DISTINCT run_id
         INTO            v_run_id
         FROM structools.st_temp_strategy_run;
         IF v_run_id != i_base_run_id THEN
            dbms_output.put_line ('st_temp_strategy_run does not contain data for run '||i_base_run_id);
            RAISE v_prog_excp;
         END IF;
      EXCEPTION
         WHEN TOO_MANY_ROWS THEN
            dbms_output.put_line ('one of the st_temp strategy tables contains multiple runs');
            RAISE v_prog_excp;
      END;

      INSERT INTO structools.st_del_program_seg_summary
      (base_run_id
      ,program_id
      ,del_year
      ,cons_segment_id
      ,segment_cost
      ,cct_len_m
      ,cond_risk_red_npv15
      ,cost_saving_pole_repl
      ,pole_repl_cnt
      ,pole_repl_risk_red_npv15
      ,cost_saving_pole_reinf
      ,pole_reinf_cnt
      ,pole_reinf_risk_red_npv15
      ,cost_saving_eqp_replace
      ,eqp_repl_cnt
      ,eqp_replace_risk_red_npv
      ,cost_saving_xstay_replace
      ,xstay_repl_cnt
      ,xstay_replace_risk_red_npv
      ,labour_saving_pole_repl
      ,segment_labour_hrs
      )
      WITH treated_zones_to_date AS
      ( 
      SELECT DISTINCT b.mtzn
      FROM   structools.st_del_program_mtzn_optimised A  INNER JOIN
      				structools.st_mtzn												b
		ON A.mtzn = b.mtzn_opt                  
      WHERE program_id = i_program_id
      AND   base_run_id = i_base_run_id
      AND   del_year > 0 AND del_year <= i_year
      )
      ,
      treated_zones_reinf_to_date AS
      ( 
      SELECT DISTINCT b.mtzn
      FROM   structools.st_del_program_reinf_opt A	INNER JOIN
      			   structools.st_mtzn												b
		ON A.mtzn = b.mtzn_opt                    
      WHERE program_id = i_program_id
      AND   base_run_id = i_base_run_id
      AND   del_year > 0 AND del_year < i_year
      )
      ,
      treated_segments_to_date AS
      ( 
      SELECT DISTINCT cons_segment_id
      FROM   structools.st_del_program_seg_optimised A  
      WHERE program_id = i_program_id
      AND   base_run_id = i_base_run_id
      AND   del_year > 0 AND del_year < i_year
      )
      ,
      sniped_pickids_to_date AS
      ( 
      SELECT DISTINCT pick_id
      FROM   structools.st_del_program_sniper A  
      WHERE program_id = i_program_id
      AND   base_run_id = i_base_run_id
      AND   del_year < i_year
      )
      ,
      segment_bays AS
      (
      SELECT /*+ parallel */
             A.pick_id
            --,A.calendar_year
            ,A.vic_id
            ,A.capex / (A.lvco_cnt + A.hvco_cnt + A.hvsp_cnt) AS bay_capex
            ,A.labour_hrs / (A.lvco_cnt + A.hvco_cnt + A.hvsp_cnt) AS bay_labour_hrs
            ,'BAY'
            ,A.span_len             AS cct_len_m
            ,A.risk_reduction_npv15
            ,A.cons_segment_id
            ,A.mtzn_grp   AS mtzn
            ,c.mtzn                                      AS treated_mtzn
            ,d.cons_segment_id                           AS treated_seg_id
      FROM structools.st_temp_bay_strategy_run     A  LEFT OUTER JOIN
           treated_zones_to_date                c
      ON A.mtzn_grp = c.mtzn     
                                                   LEFT OUTER JOIN
           treated_segments_to_date d
      ON A.cons_segment_id = d.cons_Segment_id   
      WHERE A.event = 'REPLACE'
      AND A.segment_rebuild_yr <= i_year
      AND A.calendar_year = i_year
      AND A.pickid_suppressed = 'N'
      --AND cons_segment_id = 'N2801711'
      )
      ,
      segment_bays_summ AS
      (
      SELECT cons_segment_id
            ,ROUND(SUM(bay_capex))                           AS bays_capex
            ,ROUND(SUM(bay_labour_hrs))                    AS bays_labour_hrs
            ,ROUND(SUM(cct_len_m))                           AS cct_len_m
            ,ROUND(SUM(risk_reduction_npv15),2)              AS risk_red_npv15
      FROM segment_bays
      WHERE treated_mtzn IS NULL
      AND   treated_seg_id IS NULL
      GROUP BY cons_segment_id
      )
      ,
      segment_poles_eqp AS
      (
      SELECT /*+ parallel */
             A.pick_id
            ,A.event
            ,A.capex
            ,A.labour_hrs
            ,A.equip_grp_id
            ,CASE 
               WHEN equip_grp_id = 'PWOD' AND part_cde = ' '   THEN  'POLE'
               WHEN equip_grp_id = 'PAUS' AND part_cde = ' '   THEN  'POLE'
               WHEN part_cde = ' '                             THEN  'EQP'
               WHEN part_cde != ' '                            THEN  'XARMSTAY'
            END                     AS eqp_typ
            ,CASE A.event
               WHEN 'REPLACE'    THEN 'REPL'
               WHEN 'REINFORCE'  THEN 'REINF'
            END                     AS event_short
            ,A.risk_reduction_npv15
            ,A.cons_segment_id
            ,A.mtzn_grp   AS mtzn
            ,c.mtzn                                      AS treated_mtzn
            ,d.cons_segment_id                    AS treated_seg_id
            ,E.pick_id                                   AS treated_pick_id
            ,f.mtzn                                      AS treated_mtzn_reinf
      FROM structools.st_temp_strategy_run     A  LEFT OUTER JOIN
           treated_zones_to_date            c
      ON A.mtzn_grp = c.mtzn     
                                                   LEFT OUTER JOIN
           treated_segments_to_date d
      ON A.cons_segment_id = d.cons_Segment_id   
                                                   LEFT OUTER JOIN
           sniped_pickids_to_date E
      ON A.pick_id = E.pick_id   
      															LEFT OUTER JOIN
           treated_zones_reinf_to_date   	f
      ON A.mtzn_grp = f.mtzn     
      WHERE A.event > ' ' --REPLACE or REINFORCE
      AND A.segment_rebuild_yr <= i_year
      AND A.calendar_year = i_year
      AND (A.pickid_suppressed = 'N' OR A.pickid_suppressed = 'P'  AND A.event = 'REINFORCE')
      )
      ,
      segment_poles_eqp_summ_tmp AS
      (
      SELECT cons_segment_id
            ,eqp_typ||'_'||event_short             AS eqp_event
            ,ROUND(SUM(capex))                     AS capex
            ,ROUND(SUM(labour_hrs))              AS labour_hrs
            ,COUNT(*)                              AS cnt
            ,ROUND(SUM(risk_reduction_npv15),2)    AS risk_red_npv15
      FROM segment_poles_eqp
      WHERE treated_mtzn IS NULL
      AND   treated_seg_id IS NULL
      AND   treated_pick_id IS NULL
      AND (treated_mtzn_reinf IS NULL OR event_short != 'REINF')
      GROUP BY cons_segment_id
              ,eqp_typ||'_'||event_short
      )        
      ,
      segment_poles_eqp_summ AS
      (
      SELECT * FROM
      (
      SELECT cons_segment_id
            ,eqp_event
            ,capex
            ,labour_hrs
            ,cnt
            ,risk_red_npv15
      FROM segment_poles_eqp_summ_tmp
      )
      PIVOT    (MAX(capex) AS capex
      			,MAX(labour_hrs) AS labour_hrs
               ,MAX(cnt) AS cnt
               ,MAX(risk_red_npv15) AS risk_red_npv15
               FOR eqp_event IN ('POLE_REPL' pole_repl,'POLE_REINF' pole_reinf,'EQP_REPL' eqp_repl, 'XARMSTAY_REPL' xstay_repl ) )
      )        
      SELECT i_base_run_id
            ,i_program_id
            ,i_year
            ,A.cons_segment_id
            ,A.bays_capex
            ,A.cct_len_m
            ,A.risk_red_npv15
            ,NVL(b.pole_repl_capex,0)              AS pole_repl_capex
            ,NVL(b.pole_repl_cnt,0)                AS pole_repl_cnt
            ,NVL(b.pole_repl_risk_red_npv15,0)     AS pole_repl_risk_red_npv15
            ,NVL(b.pole_reinf_capex,0)             AS pole_reinf_capex
            ,NVL(b.pole_reinf_cnt,0)               AS pole_reinf_cnt
            ,NVL(b.pole_reinf_risk_red_npv15,0)    AS pole_reinf_risk_red_npv15
            ,NVL(b.eqp_repl_capex,0)              AS eqp_repl_capex
            ,NVL(b.eqp_repl_cnt,0)                AS eqp_repl_cnt
            ,NVL(b.eqp_repl_risk_red_npv15,0)     AS eqp_repl_risk_red_npv15
            ,NVL(b.xstay_repl_capex,0)              AS xstay_repl_capex
            ,NVL(b.xstay_repl_cnt,0)                AS xstay_repl_cnt
            ,NVL(b.xstay_repl_risk_red_npv15,0)     AS xstay_repl_risk_red_npv15
            ,NVL(b.pole_repl_labour_hrs,0)              AS pole_repl_labour_hrs
            ,A.bays_labour_hrs
      FROM  segment_bays_summ       A        LEFT OUTER JOIN
            segment_poles_eqp_summ  B
      ON A.cons_segment_id = b.cons_segment_id
      ORDER BY A.cons_segment_id      
      ;
            

		/* Create summary of reinforcements in various P* categories, per segment, and write results to ST_DEL_PROGRAM_SEG_REINF_SUMM (equivalent of st_del_program_zbam_reinf_summ)   */
      INSERT INTO structools.st_del_program_seg_reinf_summ
      	(
          base_run_id
         ,program_id
         ,del_year
         ,cons_segment_id
         ,p1_summ
      	,p2_summ
         ,p3_summ
         ,p4_summ
      	,p5_summ
         ,p6_summ
         ,p7_summ
      	,p8_summ
         ,p9_summ
      )
      WITH treated_zones_to_date AS
      ( 
      SELECT DISTINCT b.mtzn
      FROM   structools.st_del_program_mtzn_optimised A  INNER JOIN
      				structools.st_mtzn												b
		ON A.mtzn = b.mtzn_opt                  
      WHERE program_id = i_program_id
      AND   base_run_id = i_base_run_id
      AND   del_year > 0 AND del_year <= i_year
      )
      ,
      treated_zones_reinf_to_date AS
      ( 
      SELECT DISTINCT b.mtzn
      FROM   structools.st_del_program_reinf_opt A	INNER JOIN
      			   structools.st_mtzn												b
		ON A.mtzn = b.mtzn_opt                    
      WHERE program_id = i_program_id
      AND   base_run_id = i_base_run_id
      AND   del_year > 0 AND del_year < i_year
      )
      ,
      treated_segments_to_date AS
      ( 
      SELECT DISTINCT cons_segment_id
      FROM   structools.st_del_program_seg_optimised A              
      WHERE program_id = i_program_id
      AND   base_run_id = i_base_run_id
      AND   del_year > 0 AND del_year < i_year
      )
      ,
      sniped_pickids_to_date AS
      ( 
      SELECT DISTINCT pick_id
      FROM   structools.st_del_program_sniper A  
      WHERE program_id = i_program_id
      AND   base_run_id = i_base_run_id
      AND   del_year < i_year
      )
      ,
      strategy_reinf_curr_year AS
      (
      SELECT /*+ parallel */
             A.pick_id
             ,A.cons_segment_id
             ,CASE 
               WHEN event_reason LIKE 'P1%'			THEN 'P1'
               WHEN event_reason LIKE 'P2%'			THEN 'P2'
               WHEN event_reason LIKE 'P3%'			THEN 'P3'
               WHEN event_reason LIKE 'P4%'			THEN 'P4'
               WHEN event_reason LIKE 'P5%'			THEN 'P5'
               WHEN event_reason LIKE 'P6%'			THEN 'P6'
               WHEN event_reason LIKE 'P7%'			THEN 'P7'
               WHEN event_reason LIKE 'P8%'			THEN 'P8'
               WHEN event_reason LIKE 'P9%'			THEN 'P9'    
               ELSE																		  event_reason           				
             END									AS event_reason
            ,c.mtzn                                      AS treated_mtzn
            ,d.cons_segment_id                    AS treated_seg_id
            ,E.pick_id                                   AS sniped_pick_id
            ,f.mtzn                                      AS treated_mtzn_reinf
      FROM structools.st_temp_strategy_run     A  LEFT OUTER JOIN
           treated_zones_to_date            c
      ON A.mtzn_grp = c.mtzn     
                                                   LEFT OUTER JOIN
           treated_segments_to_date d
      ON A.cons_segment_id = d.cons_Segment_id   
                                                   LEFT OUTER JOIN
           sniped_pickids_to_date E
      ON A.pick_id = E.pick_id   
      															LEFT OUTER JOIN
           treated_zones_reinf_to_date   	f
      ON A.mtzn_grp = f.mtzn     
      WHERE A.event = 'REINFORCE' AND A.pickid_suppressed IN ('N','P')
      AND equip_grp_id = 'PWOD' AND part_cde = ' '
      AND A.segment_rebuild_yr <= i_year
      AND A.calendar_year = i_year
      )
      ,
      poles_reinf_summ AS
      (
      SELECT cons_segment_id
                  ,event_reason
                 ,count(*) AS reinf_cnt
      FROM strategy_reinf_curr_year A
      WHERE treated_mtzn IS NULL
      AND   treated_seg_id IS NULL
      AND   sniped_pick_id IS NULL
      AND 	treated_mtzn_reinf IS NULL
      GROUP BY cons_segment_id, event_reason
      )      
      ,
      event_counts AS
      (
      SELECT * FROM
      (
      SELECT  cons_segment_id
                  ,reinf_cnt
                  ,event_reason
      FROM poles_reinf_summ
      )
      PIVOT    (max(reinf_cnt) AS reinf_cnt
               FOR event_reason IN ('P1' p1,'P2' p2,'P3' p3, 'P4' p4,'P5' p5,'P6' p6,'P7' p7, 'P8' p8 , 'P9' p9   ) )
      )
      SELECT i_base_run_id
                  ,i_program_id
                  ,i_year
                  ,cons_segment_id
                  ,nvl(p1_reinf_cnt,0) AS p1_reinf_cnt
                  ,nvl(p2_reinf_cnt,0) AS p2_reinf_cnt
                  ,nvl(p3_reinf_cnt,0) AS p3_reinf_cnt
                  ,nvl(p4_reinf_cnt,0) AS p4_reinf_cnt
                  ,nvl(p5_reinf_cnt,0) AS p5_reinf_cnt
                  ,nvl(p6_reinf_cnt,0) AS p6_reinf_cnt
                  ,nvl(p7_reinf_cnt,0) AS p7_reinf_cnt
                  ,nvl(p8_reinf_cnt,0) AS p8_reinf_cnt
                  ,nvl(p9_reinf_cnt,0) AS p9_reinf_cnt
            FROM    event_counts
      ;
            
      
   EXCEPTION
      WHEN v_prog_excp THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END run_summary_for_segments;

   PROCEDURE run_summary_for_mtzn_reinf(i_base_run_id    IN PLS_INTEGER
                                                          ,i_year                IN PLS_INTEGER
                                                          ,i_program_id      IN PLS_INTEGER) IS 


		v_dummy1				PLS_INTEGER;
      v_dummy2				PLS_INTEGER;
      v_run_id					PLS_INTEGER;
      v_first_year   		PLS_INTEGER;
      v_run_years   		PLS_INTEGER;
      
	BEGIN

   	dbms_output.put_line('run_mtzn_reinf_summ Start '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

      IF i_base_run_id IS NULL OR i_year IS NULL OR i_program_id IS NULL THEN
         dbms_output.put_line ('input PARAMETERS cannot be NULL');            
         RAISE v_prog_excp;
      END IF;

		BEGIN              
         SELECT base_run_id
                     ,del_year
         INTO      v_dummy1
                     ,v_dummy2                     
         FROM structools.st_del_program_reinf_summ
         WHERE  base_run_id = i_base_run_id
         AND program_id = i_program_id
         AND del_YEAR = i_year
         AND ROWNUM = 1;
         dbms_output.put_line ('run ID '||i_base_run_id||' program_id '||i_program_id||' YEAR '||i_year||' already IN st_del_program_reinf_summ. DELETE earlier results first');            
         RAISE v_prog_excp;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
      
      BEGIN
         SELECT /*+ parallel */ DISTINCT run_id
         INTO            v_run_id
         FROM structools.st_temp_strategy_run;
         IF v_run_id != i_base_run_id THEN
            dbms_output.put_line ('st_temp_strategy_run does NOT contain DATA FOR run '||i_base_run_id);
            RAISE v_prog_excp;
         END IF;
      EXCEPTION
         WHEN TOO_MANY_ROWS THEN
            dbms_output.put_line ('st_temp strategy_run TABLE contains multiple runs');
            RAISE v_prog_excp;
      END;
      
      SELECT     first_year
      					,run_years
      INTO       v_first_year
      					,v_run_years
      FROM       structools.st_run_request    
      WHERE      run_id = i_base_run_id 
      ;
      
      IF i_year < v_first_year OR i_year > (v_first_year + v_run_years - 1) THEN
         dbms_output.put_line ('st_temp_strategy_run does NOT contain DATA FOR run '||i_base_run_id);
         RAISE v_prog_excp;
      END IF;
   
      INSERT INTO structools.st_del_program_reinf_summ
      (base_run_id
      ,program_id
      ,del_year
      ,mtzn
      ,p1_summ
      ,p2_summ
      ,p3_summ
      ,p4_summ
      ,p5_summ
      ,p6_summ
      ,p7_summ
      ,p8_summ
      ,p9_summ
      ,concentration_ratio
      )
		WITH reinforcements AS
		(
		SELECT /*+ parallel */ 
      				pick_id
      				,mtzn_grp
                  ,cons_segment_id
                  ,min(calendar_year) AS min_reinf_year
                  ,max(calendar_year) AS max_reinf_year
		FROM structools.st_temp_strategy_run
      WHERE equip_grp_id = 'PWOD' AND part_cde = ' '
      AND event = 'REINFORCE'
      AND calendar_year <=  i_year
      AND pickid_suppressed IN ('N','P')
      GROUP BY  pick_id
      				,mtzn_grp
                  ,cons_segment_id
      )
      ,                         
      treated_mtzn_to_date AS
      ( 
      SELECT DISTINCT b.mtzn
      									,A.del_year
                                 ,A.base_run_id
      FROM   structools.st_del_program_mtzn_optimised A  INNER JOIN
      			   structools.st_mtzn												b
		ON A.mtzn = b.mtzn_opt                  
      WHERE program_id = i_program_id
      AND   base_run_id = i_base_run_id
      AND   del_year > 0 AND del_year <= i_year
      UNION
      SELECT DISTINCT b.mtzn
      									,A.del_year
                                 ,A.base_run_id
      FROM   structools.st_del_program_reinf_opt A		INNER JOIN
      				structools.st_mtzn												b
		ON A.mtzn = b.mtzn_opt                    
      WHERE program_id =  i_program_id
      AND   base_run_id = i_base_run_id
      AND   del_year > 0 AND del_year < i_year
      )
      ,
      treated_segments_to_date AS
      ( 
      SELECT DISTINCT cons_segment_id
      									,del_year
                                 ,base_run_id
      FROM   structools.st_del_program_seg_optimised A  
      WHERE program_id = i_program_id
      AND   base_run_id = i_base_run_id
      AND   del_year > 0 AND del_year <= i_year
      )
      ,
      treated_pickids_to_date AS
      (
      SELECT  DISTINCT
      				A.pick_id				
      				,'MTZN'					AS del_type
                  ,b.del_year			 AS del_year      		
      FROM reinforcements			A			INNER JOIN
      		   treated_mtzn_to_date			  b
		ON A.mtzn_grp = b.mtzn
      WHERE b.del_year BETWEEN min_reinf_year AND max_reinf_year
      UNION  
      SELECT  DISTINCT
      				A.pick_id
      				,'SEG' 					AS del_type
                  ,b.del_year			 AS del_year      		
      FROM reinforcements			A			INNER JOIN
      		   treated_segments_to_date			  b
		ON A.cons_segment_id = b.cons_segment_id
      WHERE b.del_year BETWEEN min_reinf_year AND max_reinf_year
      )
      ,
      pickids AS
      (
      SELECT /*+ parallel */
             A.pick_id					
             ,CASE 
             	WHEN event_reason LIKE 'P1%'			THEN 'P1'
               WHEN event_reason LIKE 'P2%'			THEN 'P2'
               WHEN event_reason LIKE 'P3%'			THEN 'P3'
               WHEN event_reason LIKE 'P4%'			THEN 'P4'
               WHEN event_reason LIKE 'P5%'			THEN 'P5'
               WHEN event_reason LIKE 'P6%'			THEN 'P6'
               WHEN event_reason LIKE 'P7%'			THEN 'P7'
					WHEN event_reason LIKE 'P8%'			THEN 'P8'
               WHEN event_reason LIKE 'P9%'			THEN 'P9'    
               ELSE																		  event_reason           				
             END									AS event_reason
            ,A.cons_segment_id
            ,A.mtzn_grp   AS mtzn
            ,b.pick_id														AS treated_pick_id
            ,b.del_type
            ,b.del_year
      FROM structools.st_temp_strategy_run	A			LEFT OUTER JOIN
      			treated_pickids_to_date	b
		ON A.pick_id = b.pick_id
      WHERE  A.event = 'REINFORCE'
      AND equip_grp_id = 'PWOD' AND part_cde = ' '
      AND A.calendar_year = i_year       
      AND A.pickid_suppressed IN ('N','P')
      )
      ,
      poles_reinf_summ_tmp AS
      (
      SELECT mtzn
      			   ,event_reason
                  ,count(*)			AS reinf_cnt
		FROM pickids
      WHERE   treated_pick_id IS NULL
      GROUP BY mtzn, event_reason
      ORDER BY mtzn, event_reason
      )             
      ,
      event_counts AS
      (
      SELECT * FROM
      (
      SELECT mtzn
      				,reinf_cnt	 AS reinf_cnt
                  ,event_reason
      FROM poles_reinf_summ_tmp
      )
      PIVOT    (max(reinf_cnt) AS reinf_cnt
               FOR event_reason IN ('P1' p1,'P2' p2,'P3' p3, 'P4' p4,'P5' p5,'P6' p6,'P7' p7, 'P8' p8 , 'P9' p9   ) )
 		)
      SELECT i_base_run_id
      				,i_program_id
                  ,i_year
                  ,mtzn
      				,nvl(p1_reinf_cnt,0) AS p1_reinf_cnt
                  ,nvl(p2_reinf_cnt,0) AS p2_reinf_cnt
                  ,nvl(p3_reinf_cnt,0) AS p3_reinf_cnt
                  ,nvl(p4_reinf_cnt,0) AS p4_reinf_cnt
                  ,nvl(p5_reinf_cnt,0) AS p5_reinf_cnt
                  ,nvl(p6_reinf_cnt,0) AS p6_reinf_cnt
                  ,nvl(p7_reinf_cnt,0) AS p7_reinf_cnt
                  ,nvl(p8_reinf_cnt,0) AS p8_reinf_cnt
                  ,nvl(p9_reinf_cnt,0) AS p9_reinf_cnt
                  ,CASE
                  	WHEN (nvl(p1_reinf_cnt,0) + nvl(p2_reinf_cnt,0) + nvl(p3_reinf_cnt,0) + nvl(p4_reinf_cnt,0) + nvl(p5_reinf_cnt,0) + nvl(p6_reinf_cnt,0) + nvl(p7_reinf_cnt,0) + nvl(p8_reinf_cnt,0) + nvl(p9_reinf_cnt,0)) != 0 THEN
                        ROUND(
                        (nvl(p1_reinf_cnt,0) + nvl(p2_reinf_cnt,0) + nvl(p3_reinf_cnt,0) + nvl(p4_reinf_cnt,0) + nvl(p5_reinf_cnt,0) + nvl(p6_reinf_cnt,0)) 
                        / (nvl(p1_reinf_cnt,0) + nvl(p2_reinf_cnt,0) + nvl(p3_reinf_cnt,0) + nvl(p4_reinf_cnt,0) + nvl(p5_reinf_cnt,0) + nvl(p6_reinf_cnt,0) + nvl(p7_reinf_cnt,0) + nvl(p8_reinf_cnt,0) + nvl(p9_reinf_cnt,0))
                        , 2)								
                  	ELSE										NULL
                  END				AS concentration_ratio
         FROM    event_counts A    
         ORDER BY mtzn       
		;
      
      COMMIT;
      
      dbms_output.put_line('run_mtzn_reinf_summ END '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));
            
   EXCEPTION
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      
   END run_summary_for_mtzn_reinf;        
      
   /**************************************************************************************************************/
   /* This procedure creates entries in st_del_program_sniper table for individual asstes selected for treatment,*/
   /* outside of MTZNs as selected by the external optimser, according to requested strategy (currently only     */
   /* risk reduction per dollar).                                                                                */
   /* This procedure creates entries in st_del_program_sniper table for individual asstes selected for treatment,*/ 
   PROCEDURE create_sniper_program(i_program_id                   IN PLS_INTEGER
                                  ,i_base_run_id                  IN PLS_INTEGER 
                                  ,i_del_year                     IN PLS_INTEGER
                                  ,i_del_strategy_id              IN PLS_INTEGER
                                  ,i_del_strategy_scenario_id     IN PLS_INTEGER) IS
                                    
      /*****************************************************************/
      /* Declarations for create_sniper_program procedure            */
      /*****************************************************************/
      v_mtzn_budget              PLS_INTEGER;
      v_sniper_budget            PLS_INTEGER;
      v_tot_standalone_cost      PLS_INTEGER;
      v_base_run_id              PLS_INTEGER;
      v_first_year               PLS_INTEGER;
      v_run_years                  PLS_INTEGER;

      
      CURSOR cost_csr IS
      
         SELECT EQUIP_GRP_ID
               ,ACTION           
               ,JOB_COMPLEXITY   
               ,PHASES           
               ,labour_hrs
               ,TOTAL_COST
               ,region
         FROM structools.st_eqp_type_action_cost
         WHERE treatment_type = 'STANDALONE'
         AND   bundled_ind = 'N'
         ORDER BY equip_grp_id
                  ,ACTION
                  ,job_complexity
                  ,phases
      ;               

      TYPE cost_rec_typ          IS RECORD
      (labour_hrs           NUMBER
      ,total_cost            NUMBER
      );
      v_cost_rec              cost_rec_typ;
      
      TYPE cost_tab_typ     IS TABLE OF cost_rec_typ INDEX BY VARCHAR2(40);

      /* table of costs, indexed by a concatenation of various factors, depending on eqp type */   
      v_cost_tab            cost_tab_typ;

      /* select highest risks individual equipment, not covered by zone program, up to the sniper budget */
      /* using the cost from the base run id */
      
      
      CURSOR sniper_cand_csr IS
      
         WITH past_years_mtzn_prog AS
         ( 
            SELECT DISTINCT 
            	b.mtzn
              ,A.del_year
            FROM   structools.st_del_program_mtzn_optimised A  	INNER JOIN
            			  structools.st_mtzn													b
				ON A.mtzn = b.mtzn_opt                       
            WHERE program_id = i_program_id 
            AND   base_run_id = i_base_run_id
            AND   del_year > 0 AND del_year <= i_del_year
         )
--         ,
--         past_years_mtzn_reinf_prog AS
--         (
--            SELECT DISTINCT  b.mtzn
--							              ,A.del_year
--            FROM   structools.st_del_program_reinf_opt A	INNER JOIN
--            				structools.st_mtzn												b
--				ON A.mtzn = b.mtzn_opt                          
--            WHERE program_id = i_program_id 
--            AND   base_run_id = i_base_run_id
--            AND   del_year > 0 AND del_year <= i_del_year
--         )
         ,
         past_mtzn_pickids AS       
         ( 
         SELECT b.pick_id
               	,b.part_cde
                  ,b.event
         FROM   past_years_mtzn_prog      A  INNER JOIN
                structools.st_temp_strategy_run   b
         ON  A.mtzn        = b.mtzn_grp --b.maint_zone_nam
         AND A.del_year = b.calendar_year                  
         WHERE b.event = 'REPLACE'  
         AND b.pickid_suppressed = 'N'          
         AND   b.part_cde NOT IN ('ASTAY','GSTAY','OSTAY','AGOST') -- exclude stays, but cater for xarms
--         UNION
--         SELECT b.pick_id
--                  ,b.part_cde
--                  ,b.event
--         FROM   past_years_mtzn_reinf_prog      A  INNER JOIN
--                structools.st_temp_strategy_run   b
--         ON  A.mtzn        = b.mtzn_grp --b.maint_zone_nam
--         AND A.del_year = b.calendar_year                  
--         WHERE b.event = 'REINFORCE'
--         AND   b.part_cde = ' '
--         AND (b.pickid_suppressed = 'N' OR b.pickid_suppressed = 'Y' AND b.event = 'REINFORCE')
         )
         ,
         past_seg_pickids AS
         (
         SELECT b.pick_id
               	,b.part_cde
                  ,b.event
         FROM   structools.st_del_program_seg_optimised      A  INNER JOIN
                structools.st_temp_strategy_run   b
         ON  A.cons_segment_id        = b.cons_segment_id
         AND A.del_year = b.calendar_year                  
         WHERE A.program_id = i_program_id 
         AND   A.base_run_id = i_base_run_id
         AND   A.del_year > 0 AND A.del_year <= i_del_year
         AND	b.event = 'REPLACE' 
         AND 	b.pickid_suppressed = 'N'
--         AND 	(b.pickid_suppressed = 'N' OR b.pickid_suppressed = 'Y' AND b.event = 'REINFORCE')
         AND   b.part_cde NOT IN ('ASTAY','GSTAY','OSTAY','AGOST') -- exclude stays, but cater for xarms
         )
         ,
         pickids_to_exclude_tmp AS
         (
         SELECT pick_id
               	,part_cde
                  ,event
         FROM   structools.st_del_program_sniper
         WHERE program_id = i_program_id 
         AND   base_run_id = i_base_run_id
         AND   del_year   < i_del_year
         UNION
         SELECT pick_id
               ,part_cde
               ,event
         FROM   past_mtzn_pickids
         UNION
         SELECT pick_id
               ,part_cde
               ,event
         FROM   past_seg_pickids
         )            
         ,
         poles_to_exclude AS 
         (
         SELECT DISTINCT pick_id
         									,event
         FROM pickids_to_exclude_tmp
         WHERE pick_id LIKE 'S%' AND part_cde= ' '
         )
         ,
         pickids_to_exclude AS
         (
         SELECT pick_id
               ,part_cde
         FROM   pickids_to_exclude_tmp
         UNION
         SELECT A.pick_id
               ,b.part_cde
         FROM   poles_to_exclude          A     INNER JOIN
                structools.st_equipment      b
         ON A.pick_id = b.pick_id                
         WHERE b.part_cde IN ('HVXM','LVXM')
         AND A.event != 'REINFORCE'
         )
         ,
         sniper AS
         (
         SELECT A.pick_id
               ,A.part_cde
               --,mtzn_grp --maint_zone_nam
               ,maint_zone_nam
               ,vic_id
               ,equip_grp_id   
               ,event
               ,risk_reduction_npv15
               ,capex                         --as base_year_capex
               ,risk_reduction_npv15 / capex    AS risk_red_per_dollar
               ,b.pick_id     AS pick_id_exclude
               ,b.part_cde    AS part_cde_exclude
         FROM structools.st_temp_strategy_run A    LEFT OUTER JOIN
              pickids_to_exclude           b
         ON A.pick_id = b.pick_id 
         AND A.part_cde = b.part_cde            
         WHERE calendar_year = i_del_year
         AND event = 'REPLACE' --IN ('REPLACE','REINFORCE')  -- H6 change - only pick REPLACES for sniper action
         AND risk_reduction_npv15 > 0
         AND A.part_cde IN (' ', 'HVXM', 'LVXM') --NOT IN ('ASTAY','GSTAY','OSTAY','AGOST')
         --AND A.pickid_suppressed = 'N'	--suppressed pickids can be subject to sniper
         )
         SELECT /*+ ORDERED */
                i_program_id
               ,i_base_run_id
               ,A.pick_id
               --,A.mtzn_grp --maint_zone_nam
               ,A.maint_zone_nam
               ,A.vic_id
               ,A.equip_grp_id
               ,A.event
               ,A.risk_reduction_npv15
               ,A.risk_red_per_dollar
               ,b.pole_complexity
               ,b.dstr_size
               ,b.recl_phases
               ,CASE b.region_nam
                  WHEN 'METRO'            THEN  'METRO'
                  ELSE                          'COUNTRY'
               END                        AS region
               ,A.part_cde
               ,b.labour_hrs
         FROM sniper                A     INNER JOIN
              structools.st_equipment  b
         ON A.pick_id = b.pick_id    
         AND A.part_cde = b.part_cde     
         WHERE A.pick_id_exclude IS NULL AND A.part_cde_exclude IS NULL     
         --ORDER BY risk_red_per_dollar_sif desc, A.pick_id         --where base_year_running_capex <= v_sniper_budget -- don'T LIMIT it here, becasue UNDER sniper treatment, different  prices APPLY
         ORDER BY risk_red_per_dollar desc, A.pick_id, A.part_cde   
      ;
      

      TYPE sniper_cand_tab_typ            IS TABLE OF sniper_cand_csr%ROWTYPE;
      v_sniper_cand_tab                   sniper_cand_tab_typ := sniper_cand_tab_typ();
      

      /********************************************************************/
      /* Private procs and functions of create_sniper_program procedure   */
      /********************************************************************/

      PROCEDURE process_del_strategy_01_02  IS
      
      BEGIN
      
         /* Validate */ 
         IF i_base_run_id > 0 AND i_del_year >=( v_year_0 + 1) AND i_del_strategy_id > 0 AND i_del_strategy_scenario_id > 0 THEN
            NULL;
         ELSE
            dbms_output.put_line('Validation error - input data not valid ');
            RAISE v_prog_excp;
         END IF;
         
         SELECT del_strategy_parm_val
         INTO   v_sniper_budget
         FROM   structools.st_del_strategy_scenario
         WHERE  del_strategy_id = i_del_strategy_id
         AND    del_strategy_scenario_id = i_del_strategy_scenario_id
         AND    del_strategy_parm_nam = 'SNIPER_EQP_BUDGET'
         ;

         dbms_output.put_line('Sniper budget = '||v_sniper_budget);
                                                                 
         /* Run the sniper component of the strategy (assets with highest risk, not in the above maint zones, up to the budget. */
         /* This should be a subset of the list selected by the cursor, as we use STANDALONE cost here. */
         
         v_tot_standalone_cost := 0;
         
         IF i_del_strategy_id = 1 OR i_del_strategy_id = 2 THEN
            OPEN sniper_cand_csr;
            FETCH sniper_cand_csr  BULK COLLECT INTO v_sniper_cand_tab;
            CLOSE sniper_cand_csr;
         END IF;
         
         
         FOR i IN 1 .. v_sniper_cand_tab.COUNT LOOP
          
            IF (v_sniper_cand_tab(i).equip_grp_id = 'PWOD' OR v_sniper_cand_tab(i).equip_grp_id = 'PAUS') AND v_sniper_cand_tab(i).part_cde = ' ' THEN
               IF v_sniper_cand_tab(i).event = 'REPLACE' THEN
                  v_cost_key := TRIM(v_sniper_cand_tab(i).equip_grp_id)||'REPLACE'||TRIM(v_sniper_cand_tab(i).pole_complexity)||TRIM(v_sniper_cand_tab(i).region);
               ELSE
                  v_cost_key := TRIM(v_sniper_cand_tab(i).equip_grp_id)||'REINFORCE'||TRIM(v_sniper_cand_tab(i).region);
               END IF;
            ELSIF v_sniper_cand_tab(i).equip_grp_id = 'DSTR' THEN         
               v_cost_key := TRIM(v_sniper_cand_tab(i).equip_grp_id)||TRIM(v_sniper_cand_tab(i).dstr_size)||TRIM(v_sniper_cand_tab(i).region);
            ELSIF v_sniper_cand_tab(i).equip_grp_id = 'RECL' THEN
               v_cost_key := TRIM(v_sniper_cand_tab(i).equip_grp_id)||TRIM(v_sniper_cand_tab(i).recl_phases)||TRIM(v_sniper_cand_tab(i).region);
            ELSIF v_sniper_cand_tab(i).part_cde = 'HVXM' OR v_sniper_cand_tab(i).part_cde = 'LVXM' THEN
               v_cost_key := TRIM(v_sniper_cand_tab(i).part_cde);
            ELSE
               v_cost_key := TRIM(v_sniper_cand_tab(i).equip_grp_id)||TRIM(v_sniper_cand_tab(i).region);
            END IF;
            v_tot_standalone_cost := v_tot_standalone_cost + v_cost_tab(v_cost_key).total_cost;
            IF v_tot_standalone_cost >= v_sniper_budget THEN
               EXIT;
            ELSE
               INSERT
               INTO structools.st_del_program_sniper
                  VALUES  (i_program_id
                          ,i_base_run_id
                          ,i_del_year
                          ,i_del_strategy_id 
                          ,i_del_strategy_scenario_id
                          ,v_sniper_cand_tab(i).pick_id
                          --,v_sniper_cand_tab(i).mtzn_grp --maint_zone_nam
                          ,v_sniper_cand_tab(i).maint_zone_nam
                          ,v_sniper_cand_tab(i).vic_id
                          ,v_sniper_cand_tab(i).equip_grp_id
                          ,v_sniper_cand_tab(i).event
                          ,v_sniper_cand_tab(i).risk_reduction_npv15
                          --,v_sniper_cand_tab(i).rr_npv15_sif_fdr_cat_adj
                          --,v_sniper_cand_tab(i).risk_red_per_dollar_sif
                          ,v_sniper_cand_tab(i).risk_red_per_dollar
                          ,v_cost_tab(v_cost_key).total_cost 
                          ,NULL
                          ,NULL
                          ,v_tot_standalone_cost
                          ,v_sniper_cand_tab(i).part_cde 
                          ,v_sniper_cand_tab(i).labour_hrs
                          )
               ;
            END IF;
         END LOOP;


      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line('Exception '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;

      END process_del_strategy_01_02;
   
   /*****************************************************************/
   /* Mainline of create_sniper_program procedure                    */
   /*****************************************************************/
   
   BEGIN

      dbms_output.put_line('create_sniper_program Start '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

      SELECT first_year
            ,run_years
      INTO   v_first_year
            ,v_run_years
      FROM structools.st_run_request
      WHERE run_id = i_base_run_id;
      
--      get_parameter_scenarios(i_base_run_id);
      structoolsapp.common_procs.get_parameter_scenarios(i_base_run_id, v_parm_scen_id_life_exp, v_parm_scen_id_seg_min_len,  v_parm_scen_id_seg_cons_pcnt, v_parm_scen_id_sif_weighting
                                                               , v_parm_scen_id_seg_rr_doll,  v_parm_scen_id_eqp_rr_doll, v_parm_scen_id_fdr_cat_adj);   

      IF v_first_year > v_this_year + 1 THEN
         v_year_0 := v_first_year - 1;
      ELSE
         v_year_0 := v_this_year;
      END IF;
      
      /* get cost of actions for standalone treatment into w-s table (for costing sniper treatment) */
      FOR c IN cost_csr LOOP
      
         v_cost_rec.labour_hrs := c.labour_hrs;
         v_cost_rec.total_cost := c.total_cost;
            
         IF c.equip_grp_id = 'PWOD' OR c.equip_grp_id = 'PAUS' THEN
            v_cost_key := TRIM(c.equip_grp_id)||TRIM(c.action)||TRIM(c.job_complexity)||TRIM(c.region);
            v_cost_tab(v_cost_key) :=  v_cost_rec;
         ELSIF c.equip_grp_id = 'DSTR' THEN
            v_cost_key := TRIM(c.equip_grp_id)||TRIM(c.job_complexity)||TRIM(c.region);
            v_cost_tab(v_cost_key) :=  v_cost_rec;
         ELSIF c.equip_grp_id = 'RECL' THEN
            v_cost_key := TRIM(c.equip_grp_id)||TRIM(c.phases)||TRIM(c.region);
            v_cost_tab(v_cost_key) :=  v_cost_rec;
         ELSIF c.equip_grp_id = 'HVXM' OR c.equip_grp_id = 'LVXM' THEN
            v_cost_key := TRIM(c.equip_grp_id);
            v_cost_tab(v_cost_key) :=  v_cost_rec;
         ELSE
            v_cost_key := TRIM(c.equip_grp_id)||TRIM(c.region);
            v_cost_tab(v_cost_key) :=  v_cost_rec;
         END IF;
      END LOOP;
      
      IF i_del_strategy_id = 1 OR i_del_strategy_id = 2 THEN
         process_del_strategy_01_02;
      ELSE
         dbms_output.put_line('Delivery strategy '||i_del_strategy_id||' not defined');
         RAISE v_prog_excp;
      END IF;

      COMMIT;
      
      dbms_output.put_line('create_sniper_program End '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

   EXCEPTION
      WHEN v_prog_excp THEN
         RAISE;    
      WHEN OTHERS THEN
         dbms_output.put_line('Exception '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END create_sniper_program;
   
   /**************************************************************************************************************/
   /* This procedure creates entries in st_del_program_reac_maint table for individual assets sampled for        */
   /* reactive treatment based on calculated number of anticipated failures following applied actions.           */
   /**************************************************************************************************************/
   PROCEDURE create_reac_maint_program(i_program_id                   IN PLS_INTEGER
                                  ,i_base_run_id                  IN PLS_INTEGER 
                                  ,i_del_year                     IN PLS_INTEGER
                                  ,i_del_strategy_id              IN PLS_INTEGER
                                  ,i_del_strategy_scenario_id     IN PLS_INTEGER) IS
                                    
      /*****************************************************************/
      /* Declarations for create_reac_maint_program procedure            */
      /*****************************************************************/
      v_run_years                  PLS_INTEGER;
      v_seed                     NUMBER;
      v_first_year               PLS_INTEGER;

      v_fire_frac             FLOAT(126);        
      v_eshock_frac           FLOAT(126);     
      v_wforce_frac           FLOAT(126);
      v_reliability_frac      FLOAT(126);
      v_physimp_frac          FLOAT(126);
      v_env_frac              FLOAT(126);
      v_fin_frac              FLOAT(126);
      v_pubsafety_frac        FLOAT(126);
      v_bay_repair_cost           NUMBER;
      v_pwod_reac_replace_cost     NUMBER;
      v_risk_noaction         FLOAT(126);
      v_risk_repair           FLOAT(126);
      v_risk_replace          FLOAT(126);
      v_risk_red              FLOAT(126);
      v_risk_noaction_npv     FLOAT(126);
      v_risk_repair_npv       FLOAT(126);
      v_risk_replace_npv      FLOAT(126);
      v_risk_red_npv          FLOAT(126);
      v_risk_noaction_npv_sif FLOAT(126);
      v_risk_repair_npv_sif   FLOAT(126);
      v_risk_replace_npv_sif  FLOAT(126);
      v_risk_red_npv_sif      FLOAT(126);
      v_event                 VARCHAR2(20);
      v_pick_id               VARCHAR2(15);
      v_part_cde              VARCHAR2(10);
      v_suppress_pickids_ver		VARCHAR2(10);

   
      CURSOR reactive_maint_csr IS
         WITH mtzn_program AS
         (
         SELECT A.del_year
                     ,b.mtzn
                     ,A.program_id
         FROM structools.st_del_program_mtzn_optimised	A	INNER JOIN
         			 structools.st_mtzn													b
			ON A.mtzn = b.mtzn_opt                          
         WHERE base_run_id = i_base_run_id
         AND program_id = i_program_id
         AND del_year > 0 AND del_year <= i_del_year    
         )
         ,
         mtzn_program_reinf AS
         (
         SELECT A.del_year
                     ,b.mtzn
                     ,A.program_id
         FROM structools.st_del_program_reinf_opt		A	INNER JOIN
         			 structools.st_mtzn													b
			ON A.mtzn = b.mtzn_opt                   	       
         WHERE base_run_id = i_base_run_id
         AND program_id = i_program_id
         AND del_year > 0 AND del_year <= i_del_year    
         )         
         ,
         strategy_run_poles AS   -- all poles flagged for action by the strategy run
         (
         SELECT /*+ parallel */ --/*+ full(a) parallel (a 8)*/
                pick_id
               ,equip_grp_id
               ,CASE event
                  WHEN 'REPLACE'    THEN  '2'
                  WHEN 'REINFORCE'  THEN  '1'
               END               AS event_seq
               --,NVL(cons_segment_mtzn, maint_zone_nam) AS mtzn
               ,mtzn_grp                  AS mtzn
               ,cons_segment_id
               ,calendar_year
               ,run_id
         FROM structools.st_temp_strategy_run  A
         WHERE EVENT > ' ' --IS NOT NULL
         AND equip_grp_id = 'PWOD'
         AND part_cde = ' '
         AND (pickid_suppressed = 'N' OR pickid_suppressed = 'P' AND event = 'REINFORCE')
         )
         ,
         pickids_poles_tmp AS          -- all poles actually treated by zonal program, sniper and reac maint (to date)
         (
         SELECT b.pick_id
               ,b.equip_grp_id
               ,b.event_seq
               ,A.del_year
         FROM mtzn_program                         A  INNER JOIN
              strategy_run_poles                   b
         ON   A.MTZN = b.mtzn
         AND A.del_year = B.CALENDAR_YEAR
			UNION
         SELECT b.pick_id
               ,b.equip_grp_id
               ,b.event_seq
               ,A.del_year
         FROM mtzn_program_reinf             A  INNER JOIN
              strategy_run_poles                   b
         ON   A.MTZN = b.mtzn
         AND A.del_year = B.CALENDAR_YEAR
         WHERE b.event_seq = '1' --'REINFORCE'
         UNION
         SELECT b.pick_id
               ,b.equip_grp_id
               ,b.event_seq
               ,A.del_year
         FROM structools.st_del_program_seg_optimised     A  INNER JOIN
              strategy_run_poles                   b
         ON   A.cons_segment_id = b.cons_segment_id
         AND A.del_year = b.calendar_year
         AND A.base_run_id = b.run_id
         WHERE A.base_run_id = i_base_run_id
         AND A.program_id = i_program_id
         AND A.del_year > 0 AND A.del_year <= i_del_year
         UNION
         SELECT A.pick_id
               ,A.equip_typ   AS equip_grp_id
               ,'2'           AS event_seq
               ,A.del_year
         FROM structools.st_del_program_sniper    A  
         WHERE A.base_run_id = i_base_run_id
         AND A.program_id = i_program_id
         AND A.del_year <= i_del_year
         AND A.equip_typ = 'PWOD'
         AND A.part_cde = ' '
         UNION
         SELECT A.pick_id
               ,A.equip_typ  AS equip_grp_id
               ,'3'           AS event_seq
               ,A.del_year
         FROM structools.st_del_program_reac_maint   A       
         WHERE A.base_run_id = i_base_run_id
         AND A.program_id = i_program_id
         AND A.del_year < i_del_year
         AND A.equip_typ = 'PWOD'
         AND part_cde = ' '
         )
         ,
         pickids_poles AS
         (
         SELECT pick_id
               ,equip_grp_id
               ,MAX(event_seq||del_year)                AS max_event_seq   -- take the latest reactive replacement;
         FROM pickids_poles_tmp                                            -- otherwise the latest zonal OR sniper replacement; 
         GROUP BY pick_id, equip_grp_id                          -- otherwise the latest reinforcement
         )                                                                 
         ,
         actioned_pickids_poles AS
         (
         SELECT pick_id
               ,equip_grp_id
               ,SUBSTR(max_event_seq,1,1)                         AS event_seq   
               ,CASE
                  WHEN SUBSTR(max_event_seq,1,1) = '3'          THEN 'REPLACE'
                  WHEN SUBSTR(max_event_seq,1,1) = '2'          THEN 'REPLACE'
                  WHEN SUBSTR(max_event_seq,1,1) = '1'          THEN 'REINFORCE'
               END                                    AS event
               ,SUBSTR(max_event_seq,2,4)             AS del_year
         FROM pickids_poles
         )
         ,
         bay_strategy_run AS
         (
         SELECT /*+ parallel */ --/*+ full(a) parallel (a 8)*/
                pick_id
               ,equip_grp_id
               ,'2' AS event_seq
               --,NVL(cons_segment_mtzn, maint_zone_nam)  AS mtzn
               ,mtzn_grp                              AS mtzn
               ,calendar_year
               ,run_id
               ,cons_segment_id
         FROM structools.st_temp_bay_strategy_run A
         WHERE EVENT > ' '
         AND segment_rebuild_yr IS NOT NULL
         AND pickid_suppressed = 'N'
         )
         ,
         pickids_bays_tmp AS
         (
         SELECT b.pick_id
               ,b.equip_grp_id
               ,b.event_seq                              -- always REPLACE for bays
               ,A.del_year
         FROM mtzn_program                         A  INNER JOIN
              bay_strategy_run               b
         ON   A.MTZN = b.mtzn
         AND A.del_year = B.CALENDAR_YEAR
         UNION
         SELECT b.pick_id
               ,b.equip_grp_id
               ,b.event_seq
               ,A.del_year
         FROM structools.st_del_program_seg_optimised     A  INNER JOIN
              bay_strategy_run                   b
         ON   A.cons_segment_id = b.cons_segment_id
         AND A.del_year = b.calendar_year
         AND A.base_run_id = b.run_id
         WHERE A.base_run_id = i_base_run_id
         AND A.program_id = i_program_id
         AND A.del_year > 0 AND A.del_year <= i_del_year
         UNION
         SELECT A.pick_id
               ,A.equip_typ  AS equip_grp_id
               ,'1'              AS event_seq    -- always REPAIR for bays
               ,A.del_year
         FROM structools.st_del_program_reac_maint   A       
         WHERE A.base_run_id = i_base_run_id
         AND A.program_id = i_program_id
         AND A.del_year < i_del_year
         AND A.equip_typ = 'BAY'
         )
         ,
         pickids_bays AS
         (
         SELECT pick_id
               ,equip_grp_id
               ,MAX(event_seq||del_year)                AS max_event_seq  -- take the latest zonal eplacement; otherwise the latest reactive REPAIR.
         FROM pickids_bays_tmp
         GROUP BY pick_id, equip_grp_id
         )
         ,
         actioned_pickids_bays AS
         (
         SELECT pick_id
               ,equip_grp_id
               ,CASE
                  WHEN SUBSTR(max_event_seq,1,1) = '2'          THEN 'REPLACE'
                  WHEN SUBSTR(max_event_seq,1,1) = '1'          THEN 'REPAIR'
               END                                    AS event
               ,SUBSTR(max_event_seq,2,4)             AS del_year
         FROM pickids_bays 
         ) 
         ,
         suppressed_poles_bays AS
         (
			SELECT pick_id
         				,'Y'					AS suppressed
         FROM structools.st_pickid_suppressed_init
         UNION
         SELECT pick_id
         			,'P'						AS suppressed				
         FROM structools.st_pickid_suppressed
         WHERE scenario_id = v_suppress_pickids_ver
         )
         ,
         failures AS
         (
         SELECT SUBSTR(A.pick_id,2,7) AS pick_id
               ,A.equip_grp_id
               ,b.del_year
               ,b.event
               ,A.maint_zone_nam       AS mtzn
               ,A.prnt_plnt_no         AS vic_id
               ,0                      AS span_len_m
               ,CASE 
                   WHEN UPPER(d.scnrrr_fdr_cat_nam) = 'NONE'         THEN 'SHORTRURAL'
                   WHEN d.scnrrr_fdr_cat_nam IS NULL         THEN 'SHORTRURAL'
                   ELSE                                             UPPER(d.scnrrr_fdr_cat_nam)
               END                                             AS fdr_cat
               ,CASE (i_del_year - v_this_year)
                  WHEN 1                  THEN FAIL_PROB_Y1
                  WHEN 2                  THEN FAIL_PROB_Y2
                  WHEN 3                  THEN FAIL_PROB_Y3
                  WHEN 4                  THEN FAIL_PROB_Y4
                  WHEN 5                  THEN FAIL_PROB_Y5
                  WHEN 6                  THEN FAIL_PROB_Y6
                  WHEN 7                  THEN FAIL_PROB_Y7
                  WHEN 8                  THEN FAIL_PROB_Y8
                  WHEN 9                  THEN FAIL_PROB_Y9
                  WHEN 10                 THEN FAIL_PROB_Y10
                  WHEN 11                 THEN FAIL_PROB_Y11
                  WHEN 12                 THEN FAIL_PROB_Y12
                  WHEN 13                 THEN FAIL_PROB_Y13
                  WHEN 14                 THEN FAIL_PROB_Y14
                  WHEN 15                 THEN FAIL_PROB_Y15
                  WHEN 16                 THEN FAIL_PROB_Y16
                  WHEN 17                 THEN FAIL_PROB_Y17
                  WHEN 18                 THEN FAIL_PROB_Y18
                  WHEN 19                 THEN FAIL_PROB_Y19
                  WHEN 20                 THEN FAIL_PROB_Y20
                  WHEN 21                 THEN FAIL_PROB_Y21
                  WHEN 22                 THEN FAIL_PROB_Y22
                  WHEN 23                 THEN FAIL_PROB_Y23
                  WHEN 24                 THEN FAIL_PROB_Y24
                  WHEN 25                 THEN FAIL_PROB_Y25
                  WHEN 26                 THEN FAIL_PROB_Y26
                  WHEN 27                 THEN FAIL_PROB_Y27
                  WHEN 28                 THEN FAIL_PROB_Y28
                  WHEN 29                 THEN FAIL_PROB_Y29
                  WHEN 30                 THEN FAIL_PROB_Y30
                  WHEN 31                 THEN FAIL_PROB_Y31
                  WHEN 32                 THEN FAIL_PROB_Y32
                  WHEN 33                 THEN FAIL_PROB_Y33
                  WHEN 34                 THEN FAIL_PROB_Y34
                  WHEN 35                 THEN FAIL_PROB_Y35
                  WHEN 36                 THEN FAIL_PROB_Y36
                  WHEN 37                 THEN FAIL_PROB_Y37
                  WHEN 38                 THEN FAIL_PROB_Y38
                  WHEN 39                 THEN FAIL_PROB_Y39
                  WHEN 40                 THEN FAIL_PROB_Y40
                  WHEN 41                 THEN FAIL_PROB_Y41
                  WHEN 42                 THEN FAIL_PROB_Y42
                  WHEN 43                 THEN FAIL_PROB_Y43
                  WHEN 44                 THEN FAIL_PROB_Y44
                  WHEN 45                 THEN FAIL_PROB_Y45
                  WHEN 46                 THEN FAIL_PROB_Y46
                  WHEN 47                 THEN FAIL_PROB_Y47
                  WHEN 48                 THEN FAIL_PROB_Y48
                  WHEN 49                 THEN FAIL_PROB_Y49
                  WHEN 50                 THEN FAIL_PROB_Y50
               END                     AS P_FAIL
               ,E.event                AS event_committed
               ,A.labour_hrs
               ,nvl(f.suppressed, 'N')		AS suppressed
         FROM structools.st_equipment             A     LEFT OUTER JOIN
              actioned_pickids_poles   b
         ON A.pick_id = b.pick_id
                                                    LEFT OUTER JOIN
              structools.st_risk_pole_NOACTION_fail  c
         ON SUBSTR(A.pick_id,2,7) = c.pick_id
                                                   LEFT OUTER JOIN
                  structools.st_hv_feeder          d
             ON A.feeder_id =        d.hv_fdr_id                                                                         
                                                   LEFT OUTER JOIN
            structools.st_pickid_committed         E
         ON A.pick_id = E.pick_id
         AND A.part_cde = E.part_cde                                                                                                                       
                                                   LEFT OUTER JOIN
            suppressed_poles_bays         f
         ON A.pick_id = f.pick_id
         WHERE B.EVENT IS NULL
         AND A.equip_grp_id = 'PWOD'
         AND A.part_cde = ' '
         UNION ALL
         SELECT SUBSTR(A.pick_id,2,7) AS pick_id
               ,A.equip_grp_id
               ,b.del_year
               ,b.event
               ,A.maint_zone_nam       AS mtzn
               ,A.prnt_plnt_no         AS vic_id
               ,0                         AS span_len_m
               ,CASE 
                   WHEN UPPER(d.scnrrr_fdr_cat_nam) = 'NONE'         THEN 'SHORTRURAL'
                   WHEN d.scnrrr_fdr_cat_nam IS NULL         THEN 'SHORTRURAL'
                   ELSE                                             UPPER(d.scnrrr_fdr_cat_nam)
               END                                             AS fdr_cat
               ,CASE (i_del_year - b.del_year)
                  WHEN 0                  THEN FAIL_PROB_Y0
                  WHEN 1                  THEN FAIL_PROB_Y1
                  WHEN 2                  THEN FAIL_PROB_Y2
                  WHEN 3                  THEN FAIL_PROB_Y3
                  WHEN 4                  THEN FAIL_PROB_Y4
                  WHEN 5                  THEN FAIL_PROB_Y5
                  WHEN 6                  THEN FAIL_PROB_Y6
                  WHEN 7                  THEN FAIL_PROB_Y7
                  WHEN 8                  THEN FAIL_PROB_Y8
                  WHEN 9                  THEN FAIL_PROB_Y9
                  WHEN 10                 THEN FAIL_PROB_Y10
                  WHEN 11                 THEN FAIL_PROB_Y11
                  WHEN 12                 THEN FAIL_PROB_Y12
                  WHEN 13                 THEN FAIL_PROB_Y13
                  WHEN 14                 THEN FAIL_PROB_Y14
                  WHEN 15                 THEN FAIL_PROB_Y15
                  WHEN 16                 THEN FAIL_PROB_Y16
                  WHEN 17                 THEN FAIL_PROB_Y17
                  WHEN 18                 THEN FAIL_PROB_Y18
                  WHEN 19                 THEN FAIL_PROB_Y19
                  WHEN 20                 THEN FAIL_PROB_Y20
                  WHEN 21                 THEN FAIL_PROB_Y21
                  WHEN 22                 THEN FAIL_PROB_Y22
                  WHEN 23                 THEN FAIL_PROB_Y23
                  WHEN 24                 THEN FAIL_PROB_Y24
                  WHEN 25                 THEN FAIL_PROB_Y25
                  WHEN 26                 THEN FAIL_PROB_Y26
                  WHEN 27                 THEN FAIL_PROB_Y27
                  WHEN 28                 THEN FAIL_PROB_Y28
                  WHEN 29                 THEN FAIL_PROB_Y29
                  WHEN 30                 THEN FAIL_PROB_Y30
                  WHEN 31                 THEN FAIL_PROB_Y31
                  WHEN 32                 THEN FAIL_PROB_Y32
                  WHEN 33                 THEN FAIL_PROB_Y33
                  WHEN 34                 THEN FAIL_PROB_Y34
                  WHEN 35                 THEN FAIL_PROB_Y35
                  WHEN 36                 THEN FAIL_PROB_Y36
                  WHEN 37                 THEN FAIL_PROB_Y37
                  WHEN 38                 THEN FAIL_PROB_Y38
                  WHEN 39                 THEN FAIL_PROB_Y39
                  WHEN 40                 THEN FAIL_PROB_Y40
                  WHEN 41                 THEN FAIL_PROB_Y41
                  WHEN 42                 THEN FAIL_PROB_Y42
                  WHEN 43                 THEN FAIL_PROB_Y43
                  WHEN 44                 THEN FAIL_PROB_Y44
                  WHEN 45                 THEN FAIL_PROB_Y45
                  WHEN 46                 THEN FAIL_PROB_Y46
                  WHEN 47                 THEN FAIL_PROB_Y47
                  WHEN 48                 THEN FAIL_PROB_Y48
                  WHEN 49                 THEN FAIL_PROB_Y49
                  WHEN 50                 THEN FAIL_PROB_Y50
               END                     AS P_FAIL
               ,E.event                AS event_committed
               ,A.labour_hrs
               ,nvl(f.suppressed, 'N')		AS suppressed
         FROM structools.st_equipment             A  LEFT OUTER JOIN
              actioned_pickids_poles   b
         ON A.pick_id = b.pick_id
                                              LEFT OUTER JOIN
              structools.st_risk_pole_replace_fail  c
         ON SUBSTR(A.pick_id,2,7) = c.pick_id
                                                LEFT OUTER JOIN
              structools.st_hv_feeder          d
         ON A.feeder_id =        d.hv_fdr_id                                                                         
                                                   LEFT OUTER JOIN
            structools.st_pickid_committed         E
            ON A.pick_id = E.pick_id                                                                                                                       
            AND A.part_cde = E.part_cde                                                                                                                       
                                                   LEFT OUTER JOIN
            suppressed_poles_bays         f
         ON A.pick_id = f.pick_id
         WHERE B.EVENT = 'REPLACE'
         AND A.equip_grp_id = 'PWOD'
         AND A.part_cde = ' '
         UNION ALL
         SELECT SUBSTR(A.pick_id,2,7) AS pick_id
               ,A.equip_grp_id
               ,b.del_year
               ,b.event
               ,A.maint_zone_nam       AS mtzn
               ,A.prnt_plnt_no         AS vic_id
               ,0                         AS span_len_m
               ,CASE 
                   WHEN UPPER(d.scnrrr_fdr_cat_nam) = 'NONE'         THEN 'SHORTRURAL'
                   WHEN d.scnrrr_fdr_cat_nam IS NULL         THEN 'SHORTRURAL'
                   ELSE                                             UPPER(d.scnrrr_fdr_cat_nam)
               END                                             AS fdr_cat
               ,CASE (i_del_year - v_this_year)
                  WHEN 1                  THEN FAIL_PROB_Y1
                  WHEN 2                  THEN FAIL_PROB_Y2
                  WHEN 3                  THEN FAIL_PROB_Y3
                  WHEN 4                  THEN FAIL_PROB_Y4
                  WHEN 5                  THEN FAIL_PROB_Y5
                  WHEN 6                  THEN FAIL_PROB_Y6
                  WHEN 7                  THEN FAIL_PROB_Y7
                  WHEN 8                  THEN FAIL_PROB_Y8
                  WHEN 9                  THEN FAIL_PROB_Y9
                  WHEN 10                 THEN FAIL_PROB_Y10
                  WHEN 11                 THEN FAIL_PROB_Y11
                  WHEN 12                 THEN FAIL_PROB_Y12
                  WHEN 13                 THEN FAIL_PROB_Y13
                  WHEN 14                 THEN FAIL_PROB_Y14
                  WHEN 15                 THEN FAIL_PROB_Y15
                  WHEN 16                 THEN FAIL_PROB_Y16
                  WHEN 17                 THEN FAIL_PROB_Y17
                  WHEN 18                 THEN FAIL_PROB_Y18
                  WHEN 19                 THEN FAIL_PROB_Y19
                  WHEN 20                 THEN FAIL_PROB_Y20
                  WHEN 21                 THEN FAIL_PROB_Y21
                  WHEN 22                 THEN FAIL_PROB_Y22
                  WHEN 23                 THEN FAIL_PROB_Y23
                  WHEN 24                 THEN FAIL_PROB_Y24
                  WHEN 25                 THEN FAIL_PROB_Y25
                  WHEN 26                 THEN FAIL_PROB_Y26
                  WHEN 27                 THEN FAIL_PROB_Y27
                  WHEN 28                 THEN FAIL_PROB_Y28
                  WHEN 29                 THEN FAIL_PROB_Y29
                  WHEN 30                 THEN FAIL_PROB_Y30
                  WHEN 31                 THEN FAIL_PROB_Y31
                  WHEN 32                 THEN FAIL_PROB_Y32
                  WHEN 33                 THEN FAIL_PROB_Y33
                  WHEN 34                 THEN FAIL_PROB_Y34
                  WHEN 35                 THEN FAIL_PROB_Y35
                  WHEN 36                 THEN FAIL_PROB_Y36
                  WHEN 37                 THEN FAIL_PROB_Y37
                  WHEN 38                 THEN FAIL_PROB_Y38
                  WHEN 39                 THEN FAIL_PROB_Y39
                  WHEN 40                 THEN FAIL_PROB_Y40
                  WHEN 41                 THEN FAIL_PROB_Y41
                  WHEN 42                 THEN FAIL_PROB_Y42
                  WHEN 43                 THEN FAIL_PROB_Y43
                  WHEN 44                 THEN FAIL_PROB_Y44
                  WHEN 45                 THEN FAIL_PROB_Y45
                  WHEN 46                 THEN FAIL_PROB_Y46
                  WHEN 47                 THEN FAIL_PROB_Y47
                  WHEN 48                 THEN FAIL_PROB_Y48
                  WHEN 49                 THEN FAIL_PROB_Y49
                  WHEN 50                 THEN FAIL_PROB_Y50
               END                     AS P_FAIL
               ,E.event                AS event_committed
               ,A.labour_hrs
               ,nvl(f.suppressed, 'N')		AS suppressed
         FROM structools.st_equipment             A  LEFT OUTER JOIN
              actioned_pickids_poles   b
         ON A.pick_id = b.pick_id
                                              LEFT OUTER JOIN
              structools.st_risk_pole_reinf_fail  c
         ON SUBSTR(A.pick_id,2,7) = c.pick_id
                                                LEFT OUTER JOIN
              structools.st_hv_feeder          d
         ON A.feeder_id =        d.hv_fdr_id                                                                         
                                                   LEFT OUTER JOIN
            structools.st_pickid_committed         E
         ON A.pick_id = E.pick_id                                                                                                                       
         AND A.part_cde = E.part_cde                                                                                                                       
                                                   LEFT OUTER JOIN
            suppressed_poles_bays         f
         ON A.pick_id = f.pick_id
         WHERE B.EVENT = 'REINFORCE'
         AND A.equip_grp_id = 'PWOD'
         AND A.part_cde = ' '
         UNION ALL
         SELECT SUBSTR(A.pick_id,2,7) AS pick_id
               ,'BAY' AS equip_grp_id
               ,b.del_year
               ,b.event
               ,A.mtzn
               ,A.stc1||A.stc2    AS vic_id
               ,A.bay_len_m                         AS span_len_m
               ,CASE 
                   WHEN UPPER(d.scnrrr_fdr_cat_nam) = 'NONE'         THEN 'SHORTRURAL'
                   WHEN d.scnrrr_fdr_cat_nam IS NULL         THEN 'SHORTRURAL'
                   ELSE                                             UPPER(d.scnrrr_fdr_cat_nam)
               END                                             AS fdr_cat
               ,CASE (i_del_year - v_this_year)
                  WHEN 1                  THEN FAIL_PROB_Y1
                  WHEN 2                  THEN FAIL_PROB_Y2
                  WHEN 3                  THEN FAIL_PROB_Y3
                  WHEN 4                  THEN FAIL_PROB_Y4
                  WHEN 5                  THEN FAIL_PROB_Y5
                  WHEN 6                  THEN FAIL_PROB_Y6
                  WHEN 7                  THEN FAIL_PROB_Y7
                  WHEN 8                  THEN FAIL_PROB_Y8
                  WHEN 9                  THEN FAIL_PROB_Y9
                  WHEN 10                 THEN FAIL_PROB_Y10
                  WHEN 11                 THEN FAIL_PROB_Y11
                  WHEN 12                 THEN FAIL_PROB_Y12
                  WHEN 13                 THEN FAIL_PROB_Y13
                  WHEN 14                 THEN FAIL_PROB_Y14
                  WHEN 15                 THEN FAIL_PROB_Y15
                  WHEN 16                 THEN FAIL_PROB_Y16
                  WHEN 17                 THEN FAIL_PROB_Y17
                  WHEN 18                 THEN FAIL_PROB_Y18
                  WHEN 19                 THEN FAIL_PROB_Y19
                  WHEN 20                 THEN FAIL_PROB_Y20
                  WHEN 21                 THEN FAIL_PROB_Y21
                  WHEN 22                 THEN FAIL_PROB_Y22
                  WHEN 23                 THEN FAIL_PROB_Y23
                  WHEN 24                 THEN FAIL_PROB_Y24
                  WHEN 25                 THEN FAIL_PROB_Y25
                  WHEN 26                 THEN FAIL_PROB_Y26
                  WHEN 27                 THEN FAIL_PROB_Y27
                  WHEN 28                 THEN FAIL_PROB_Y28
                  WHEN 29                 THEN FAIL_PROB_Y29
                  WHEN 30                 THEN FAIL_PROB_Y30
                  WHEN 31                 THEN FAIL_PROB_Y31
                  WHEN 32                 THEN FAIL_PROB_Y32
                  WHEN 33                 THEN FAIL_PROB_Y33
                  WHEN 34                 THEN FAIL_PROB_Y34
                  WHEN 35                 THEN FAIL_PROB_Y35
                  WHEN 36                 THEN FAIL_PROB_Y36
                  WHEN 37                 THEN FAIL_PROB_Y37
                  WHEN 38                 THEN FAIL_PROB_Y38
                  WHEN 39                 THEN FAIL_PROB_Y39
                  WHEN 40                 THEN FAIL_PROB_Y40
                  WHEN 41                 THEN FAIL_PROB_Y41
                  WHEN 42                 THEN FAIL_PROB_Y42
                  WHEN 43                 THEN FAIL_PROB_Y43
                  WHEN 44                 THEN FAIL_PROB_Y44
                  WHEN 45                 THEN FAIL_PROB_Y45
                  WHEN 46                 THEN FAIL_PROB_Y46
                  WHEN 47                 THEN FAIL_PROB_Y47
                  WHEN 48                 THEN FAIL_PROB_Y48
                  WHEN 49                 THEN FAIL_PROB_Y49
                  WHEN 50                 THEN FAIL_PROB_Y50
               END                     AS P_FAIL
               ,E.event                AS event_committed
               ,NULL							AS labour_hrs
               ,nvl(f.suppressed, 'N')		AS suppressed
         FROM structools.st_bay_equipment             A  LEFT OUTER JOIN
              actioned_pickids_bays   b
         ON A.pick_id = b.pick_id
                                              LEFT OUTER JOIN
              structools.st_risk_cond_NOACTION_fail  c
         ON SUBSTR(A.pick_id,2,7) = c.pick_id
                                                LEFT OUTER JOIN
              structools.st_hv_feeder          d
         ON A.feeder_id =        d.hv_fdr_id                                                                         
                                                   LEFT OUTER JOIN
            structools.st_pickid_committed         E
         ON A.pick_id = E.pick_id                                                                                                                       
                                                   LEFT OUTER JOIN
           suppressed_poles_bays         f
         ON A.pick_id = f.pick_id
         WHERE B.EVENT IS NULL
         UNION ALL
         SELECT SUBSTR(A.pick_id,2,7) AS pick_id
               ,'BAY' AS equip_grp_id
               ,b.del_year
               ,b.event
               ,A.mtzn
               ,A.stc1||A.stc2    AS vic_id
               ,A.bay_len_m                         AS span_len_m
               ,CASE 
                   WHEN UPPER(d.scnrrr_fdr_cat_nam) = 'NONE'         THEN 'SHORTRURAL'
                   WHEN d.scnrrr_fdr_cat_nam IS NULL         THEN 'SHORTRURAL'
                   ELSE                                             UPPER(d.scnrrr_fdr_cat_nam)
               END                                             AS fdr_cat
               ,CASE (i_del_year - b.del_year)
                  WHEN 0                  THEN FAIL_PROB_Y0
                  WHEN 1                  THEN FAIL_PROB_Y1
                  WHEN 2                  THEN FAIL_PROB_Y2
                  WHEN 3                  THEN FAIL_PROB_Y3
                  WHEN 4                  THEN FAIL_PROB_Y4
                  WHEN 5                  THEN FAIL_PROB_Y5
                  WHEN 6                  THEN FAIL_PROB_Y6
                  WHEN 7                  THEN FAIL_PROB_Y7
                  WHEN 8                  THEN FAIL_PROB_Y8
                  WHEN 9                  THEN FAIL_PROB_Y9
                  WHEN 10                 THEN FAIL_PROB_Y10
                  WHEN 11                 THEN FAIL_PROB_Y11
                  WHEN 12                 THEN FAIL_PROB_Y12
                  WHEN 13                 THEN FAIL_PROB_Y13
                  WHEN 14                 THEN FAIL_PROB_Y14
                  WHEN 15                 THEN FAIL_PROB_Y15
                  WHEN 16                 THEN FAIL_PROB_Y16
                  WHEN 17                 THEN FAIL_PROB_Y17
                  WHEN 18                 THEN FAIL_PROB_Y18
                  WHEN 19                 THEN FAIL_PROB_Y19
                  WHEN 20                 THEN FAIL_PROB_Y20
                  WHEN 21                 THEN FAIL_PROB_Y21
                  WHEN 22                 THEN FAIL_PROB_Y22
                  WHEN 23                 THEN FAIL_PROB_Y23
                  WHEN 24                 THEN FAIL_PROB_Y24
                  WHEN 25                 THEN FAIL_PROB_Y25
                  WHEN 26                 THEN FAIL_PROB_Y26
                  WHEN 27                 THEN FAIL_PROB_Y27
                  WHEN 28                 THEN FAIL_PROB_Y28
                  WHEN 29                 THEN FAIL_PROB_Y29
                  WHEN 30                 THEN FAIL_PROB_Y30
                  WHEN 31                 THEN FAIL_PROB_Y31
                  WHEN 32                 THEN FAIL_PROB_Y32
                  WHEN 33                 THEN FAIL_PROB_Y33
                  WHEN 34                 THEN FAIL_PROB_Y34
                  WHEN 35                 THEN FAIL_PROB_Y35
                  WHEN 36                 THEN FAIL_PROB_Y36
                  WHEN 37                 THEN FAIL_PROB_Y37
                  WHEN 38                 THEN FAIL_PROB_Y38
                  WHEN 39                 THEN FAIL_PROB_Y39
                  WHEN 40                 THEN FAIL_PROB_Y40
                  WHEN 41                 THEN FAIL_PROB_Y41
                  WHEN 42                 THEN FAIL_PROB_Y42
                  WHEN 43                 THEN FAIL_PROB_Y43
                  WHEN 44                 THEN FAIL_PROB_Y44
                  WHEN 45                 THEN FAIL_PROB_Y45
                  WHEN 46                 THEN FAIL_PROB_Y46
                  WHEN 47                 THEN FAIL_PROB_Y47
                  WHEN 48                 THEN FAIL_PROB_Y48
                  WHEN 49                 THEN FAIL_PROB_Y49
                  WHEN 50                 THEN FAIL_PROB_Y50
               END                     AS P_FAIL
               ,E.event                AS event_committed
               ,NULL							AS labour_hrs
               ,nvl(f.suppressed, 'N')		AS suppressed
         FROM structools.st_bay_equipment             A  LEFT OUTER JOIN
              actioned_pickids_bays   b
         ON A.pick_id = b.pick_id
                                              LEFT OUTER JOIN
              structools.st_risk_cond_replace_fail  c
         ON SUBSTR(A.pick_id,2,7) = c.pick_id
                                                LEFT OUTER JOIN
              structools.st_hv_feeder          d
         ON A.feeder_id =        d.hv_fdr_id                                                                         
                                                   LEFT OUTER JOIN
            structools.st_pickid_committed         E
         ON A.pick_id = E.pick_id                                                                                                                       
                                                   LEFT OUTER JOIN
           suppressed_poles_bays         f
         ON A.pick_id = f.pick_id
         WHERE B.EVENT = 'REPLACE' 
         UNION ALL
         SELECT SUBSTR(A.pick_id,2,7) AS pick_id
               ,'BAY' AS equip_grp_id
               ,b.del_year
               ,b.event
               ,A.mtzn
               ,A.stc1||A.stc2    AS vic_id
               ,A.bay_len_m                         AS span_len_m
               ,CASE 
                   WHEN UPPER(d.scnrrr_fdr_cat_nam) = 'NONE'         THEN 'SHORTRURAL'
                   WHEN d.scnrrr_fdr_cat_nam IS NULL         THEN 'SHORTRURAL'
                   ELSE                                             UPPER(d.scnrrr_fdr_cat_nam)
               END                                             AS fdr_cat
               ,CASE (i_del_year - v_this_year)
                  WHEN 0                  THEN FAIL_PROB_Y0
                  WHEN 1                  THEN FAIL_PROB_Y1
                  WHEN 2                  THEN FAIL_PROB_Y2
                  WHEN 3                  THEN FAIL_PROB_Y3
                  WHEN 4                  THEN FAIL_PROB_Y4
                  WHEN 5                  THEN FAIL_PROB_Y5
                  WHEN 6                  THEN FAIL_PROB_Y6
                  WHEN 7                  THEN FAIL_PROB_Y7
                  WHEN 8                  THEN FAIL_PROB_Y8
                  WHEN 9                  THEN FAIL_PROB_Y9
                  WHEN 10                 THEN FAIL_PROB_Y10
                  WHEN 11                 THEN FAIL_PROB_Y11
                  WHEN 12                 THEN FAIL_PROB_Y12
                  WHEN 13                 THEN FAIL_PROB_Y13
                  WHEN 14                 THEN FAIL_PROB_Y14
                  WHEN 15                 THEN FAIL_PROB_Y15
                  WHEN 16                 THEN FAIL_PROB_Y16
                  WHEN 17                 THEN FAIL_PROB_Y17
                  WHEN 18                 THEN FAIL_PROB_Y18
                  WHEN 19                 THEN FAIL_PROB_Y19
                  WHEN 20                 THEN FAIL_PROB_Y20
                  WHEN 21                 THEN FAIL_PROB_Y21
                  WHEN 22                 THEN FAIL_PROB_Y22
                  WHEN 23                 THEN FAIL_PROB_Y23
                  WHEN 24                 THEN FAIL_PROB_Y24
                  WHEN 25                 THEN FAIL_PROB_Y25
                  WHEN 26                 THEN FAIL_PROB_Y26
                  WHEN 27                 THEN FAIL_PROB_Y27
                  WHEN 28                 THEN FAIL_PROB_Y28
                  WHEN 29                 THEN FAIL_PROB_Y29
                  WHEN 30                 THEN FAIL_PROB_Y30
                  WHEN 31                 THEN FAIL_PROB_Y31
                  WHEN 32                 THEN FAIL_PROB_Y32
                  WHEN 33                 THEN FAIL_PROB_Y33
                  WHEN 34                 THEN FAIL_PROB_Y34
                  WHEN 35                 THEN FAIL_PROB_Y35
                  WHEN 36                 THEN FAIL_PROB_Y36
                  WHEN 37                 THEN FAIL_PROB_Y37
                  WHEN 38                 THEN FAIL_PROB_Y38
                  WHEN 39                 THEN FAIL_PROB_Y39
                  WHEN 40                 THEN FAIL_PROB_Y40
                  WHEN 41                 THEN FAIL_PROB_Y41
                  WHEN 42                 THEN FAIL_PROB_Y42
                  WHEN 43                 THEN FAIL_PROB_Y43
                  WHEN 44                 THEN FAIL_PROB_Y44
                  WHEN 45                 THEN FAIL_PROB_Y45
                  WHEN 46                 THEN FAIL_PROB_Y46
                  WHEN 47                 THEN FAIL_PROB_Y47
                  WHEN 48                 THEN FAIL_PROB_Y48
                  WHEN 49                 THEN FAIL_PROB_Y49
                  WHEN 50                 THEN FAIL_PROB_Y50
               END                     AS P_FAIL
               ,E.event                AS event_committed
               ,NULL							AS labour_hrs
               ,nvl(f.suppressed, 'N')		AS suppressed
         FROM structools.st_bay_equipment             A  LEFT OUTER JOIN
              actioned_pickids_bays   b
         ON A.pick_id = b.pick_id
                                              LEFT OUTER JOIN
              structools.st_risk_cond_repair_fail  c
         ON SUBSTR(A.pick_id,2,7) = c.pick_id
                                                LEFT OUTER JOIN
              structools.st_hv_feeder          d
         ON A.feeder_id =        d.hv_fdr_id                                                                         
                                                   LEFT OUTER JOIN
            structools.st_pickid_committed         E
         ON A.pick_id = E.pick_id                                                                                                                       
                                                   LEFT OUTER JOIN
           suppressed_poles_bays         f
         ON A.pick_id = f.pick_id
         WHERE B.EVENT = 'REPAIR'
         )
         ,
         failures_random AS
         (
         SELECT pick_id
               ,equip_grp_id
               ,del_year
               ,event
               ,mtzn
               ,vic_id
               ,span_len_m
               ,fdr_cat
               ,p_fail
               ,CASE
                  WHEN v_seed IS NULL       THEN  p_fail
                  ELSE p_fail * dbms_random.value(0,1) 
               END               AS p_fail_random
               ,labour_hrs
--               ,suppressed
         FROM failures
         WHERE event_committed IS NULL
         AND (suppressed = 'N' OR suppressed = 'P' AND event = 'REINFORCE')
         )
         ,
         total_failures_pwod AS
         (
         SELECT  CEIL(SUM(p_fail))   AS p_fail_tot
         FROM   failures_random
         WHERE equip_grp_id = 'PWOD'
         )
         ,
         total_failures_bay AS
         (
         SELECT  CEIL(SUM(p_fail))   AS p_fail_tot
         FROM   failures_random
         WHERE equip_grp_id = 'BAY'
         )
--         ,
--         selected_for_reac_maint AS
--         (
         SELECT *
         FROM
         (
         SELECT pick_id
               ,equip_grp_id
               ,del_year
               ,event
               ,mtzn
               ,vic_id
               ,span_len_m
               ,fdr_cat
               ,p_fail
               ,p_fail_random
               ,labour_hrs
         FROM failures_random
         WHERE p_fail IS NOT NULL
         AND (del_year IS NULL OR del_year !=  i_del_year)
         AND equip_grp_id = 'PWOD'
         ORDER BY p_fail_random desc, pick_id
         ) WHERE ROWNUM <= (SELECT p_fail_tot FROM total_failures_PWOD)
         UNION ALL
         SELECT *
         FROM
         (
         SELECT pick_id
               ,equip_grp_id
               ,del_year
               ,event
               ,mtzn
               ,vic_id
               ,span_len_m
               ,fdr_cat
               ,p_fail
               ,p_fail_random
               ,labour_hrs
         FROM failures_random
         WHERE p_fail IS NOT NULL
         AND (del_year IS NULL OR del_year !=  i_del_year)
         AND equip_grp_id = 'BAY'
         ORDER BY p_fail_random desc, pick_id
         ) WHERE ROWNUM <= (SELECT p_fail_tot FROM total_failures_BAY)
--         )
--         SELECT *
--         FROM selected_for_reac_maint
--         ORDER BY equip_grp_id, p_fail_random desc
      ;
      
      TYPE reac_maint_tab_typ          IS TABLE OF reactive_maint_csr%ROWTYPE;
      v_reac_maint_tab                 reac_maint_tab_typ := reac_maint_tab_typ();

      /********************************************************************/
      /* Private procs and functions of create_reac_maint_program procedure   */
      /********************************************************************/

      FUNCTION get_risk(i_egi IN VARCHAR2
                       ,i_pick_id IN VARCHAR2
                       ,i_event IN VARCHAR2
                       ,i_idx IN PLS_INTEGER) RETURN FLOAT IS

         o_risk            FLOAT(126);
         v_risk_rec        risk_rec_typ;
         v_risk_tab        risk_tab_typ := risk_tab_typ();
         
      BEGIN
      
         IF i_event = 'NOACTION' THEN
            IF i_egi = 'BAY' THEN
               SELECT   A.*
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
               INTO   v_risk_rec
               FROM   structools.st_risk_bay_noaction A
               WHERE  pick_id = i_pick_id;
            ELSIF i_egi = 'PWOD' THEN
               SELECT   A.*
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
               INTO   v_risk_rec
               FROM   structools.st_risk_pwod_noaction A
               WHERE  pick_id = i_pick_id;
            END IF;
         ELSIF i_event = 'REPAIR' THEN           
            SELECT A.*
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
            INTO   v_risk_rec
            FROM   structools.st_risk_bay_repair A     
            WHERE  pick_id = i_pick_id;
         ELSIF i_event = 'REPLACE' THEN
            IF i_egi = 'BAY' THEN
               SELECT A.*
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
                  ,NULL
               INTO   v_risk_rec
               FROM   structools.st_risk_bay_replace A
               WHERE  pick_id = i_pick_id;
            ELSIF i_egi = 'PWOD' THEN
               SELECT A.*
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
               INTO   v_risk_rec
               FROM   structools.st_risk_pwod_replace A
               WHERE  pick_id = i_pick_id;
            END IF;
         ELSIF i_event = 'REINFORCE' THEN
            SELECT A.*
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
            INTO   v_risk_rec
            FROM   structools.st_risk_pwod_reinf A
            WHERE  pick_id = i_pick_id;
         END IF;

         v_risk_tab.EXTEND(51);
         v_risk_tab(1) := v_risk_rec.yr0; 
         v_risk_tab(2) := v_risk_rec.yr1; 
         v_risk_tab(3) := v_risk_rec.yr2; 
         v_risk_tab(4) := v_risk_rec.yr3;
         v_risk_tab(5) := v_risk_rec.yr4; 
         v_risk_tab(6) := v_risk_rec.yr5; 
         v_risk_tab(7) := v_risk_rec.yr6; 
         v_risk_tab(8) := v_risk_rec.yr7;
         v_risk_tab(9) := v_risk_rec.yr8; 
         v_risk_tab(10) := v_risk_rec.yr9; 
         v_risk_tab(11) := v_risk_rec.yr10; 
         v_risk_tab(12) := v_risk_rec.yr11;
         v_risk_tab(13) := v_risk_rec.yr12; 
         v_risk_tab(14) := v_risk_rec.yr13; 
         v_risk_tab(15) := v_risk_rec.yr14; 
         v_risk_tab(16) := v_risk_rec.yr15;
         v_risk_tab(17) := v_risk_rec.yr16; 
         v_risk_tab(18) := v_risk_rec.yr17; 
         v_risk_tab(19) := v_risk_rec.yr18; 
         v_risk_tab(20) := v_risk_rec.yr19;
         v_risk_tab(21) := v_risk_rec.yr20; 
         v_risk_tab(22) := v_risk_rec.yr21; 
         v_risk_tab(23) := v_risk_rec.yr22; 
         v_risk_tab(24) := v_risk_rec.yr23;
         v_risk_tab(25) := v_risk_rec.yr24; 
         v_risk_tab(26) := v_risk_rec.yr25; 
         v_risk_tab(27) := v_risk_rec.yr26; 
         v_risk_tab(28) := v_risk_rec.yr27;
         v_risk_tab(29) := v_risk_rec.yr28; 
         v_risk_tab(30) := v_risk_rec.yr29; 
         v_risk_tab(31) := v_risk_rec.yr30; 
         v_risk_tab(32) := v_risk_rec.yr31;
         v_risk_tab(33) := v_risk_rec.yr32; 
         v_risk_tab(34) := v_risk_rec.yr33; 
         v_risk_tab(35) := v_risk_rec.yr34; 
         v_risk_tab(36) := v_risk_rec.yr35;
         v_risk_tab(37) := v_risk_rec.yr36; 
         v_risk_tab(38) := v_risk_rec.yr37; 
         v_risk_tab(39) := v_risk_rec.yr38; 
         v_risk_tab(40) := v_risk_rec.yr39;
         v_risk_tab(41) := v_risk_rec.yr40; 
         v_risk_tab(42) := v_risk_rec.yr41; 
         v_risk_tab(43) := v_risk_rec.yr42; 
         v_risk_tab(44) := v_risk_rec.yr43;
         v_risk_tab(45) := v_risk_rec.yr44; 
         v_risk_tab(46) := v_risk_rec.yr45; 
         v_risk_tab(47) := v_risk_rec.yr46; 
         v_risk_tab(48) := v_risk_rec.yr47;
         v_risk_tab(49) := v_risk_rec.yr48; 
         v_risk_tab(50) := v_risk_rec.yr49;
         v_risk_tab(51) := v_risk_rec.yr50;

         o_risk := v_risk_tab(i_idx);

         RETURN o_risk;
            
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RETURN NULL;
         WHEN OTHERS THEN
            dbms_output.put_line('i_idx = '||i_idx||' i_egi = '||i_egi||' i_pick_id = '||i_pick_id||' i_event = '||i_event);
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);         
            RAISE;
                           
      END get_risk;   


      FUNCTION get_risk_npv(i_egi IN VARCHAR2
                           ,i_pick_id IN VARCHAR2
                           ,i_event IN VARCHAR2
                           ,i_idx IN PLS_INTEGER
                           ,i_sif_ind IN CHAR
                           ,i_fdr_cat   IN VARCHAR2) RETURN FLOAT IS

         o_risk            FLOAT(126);
         v_risk_rec        risk_rec_typ;
         v_risk_tab        risk_tab_typ := risk_tab_typ();
         v_adj_factor      NUMBER;
         
      BEGIN

         v_risk_pubsafety_sif   := v_fdr_cat_sif_tab(i_fdr_cat).pubsafety_sif;
         v_risk_wforce_sif      := v_fdr_cat_sif_tab(i_fdr_cat).wforce_sif;
         v_risk_reliability_sif := v_fdr_cat_sif_tab(i_fdr_cat).reliability_sif;
         v_risk_env_sif         := v_fdr_cat_sif_tab(i_fdr_cat).env_sif;   
         v_risk_fin_sif         := v_fdr_cat_sif_tab(i_fdr_cat).fin_sif;
         v_adj_factor           := v_fdr_cat_sif_tab(i_fdr_cat).adj_factor;         
         
         IF i_event = 'NOACTION' THEN
            IF i_egi = 'BAY' THEN
               SELECT   *
               INTO   v_risk_rec
               FROM   structools.st_risk_bay_noaction_npv15
               WHERE  pick_id = i_pick_id;
            ELSIF i_egi = 'PWOD' THEN
               SELECT   *
               INTO   v_risk_rec
               FROM   structools.st_risk_pwod_noaction_npv15
               WHERE  pick_id = i_pick_id;
            END IF;
            IF i_sif_ind = 'Y' THEN
               v_fire_frac             := v_risk_rec.fire_frac;        
               v_eshock_frac           := v_risk_rec.eshock_frac;     
               v_wforce_frac           := v_risk_rec.wforce_frac;
               v_reliability_frac      := v_risk_rec.reliability_frac;
               v_physimp_frac          := v_risk_rec.physimp_frac;
               v_env_frac              := v_risk_rec.env_frac;
               v_fin_frac              := v_risk_rec.fin_frac;
            END IF;   
         ELSIF i_event = 'REPAIR' THEN           
            SELECT A.*
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
            INTO   v_risk_rec
            FROM   structools.st_risk_bay_repair_npv15 A
            WHERE  pick_id = i_pick_id;
         ELSIF i_event = 'REPLACE' THEN
            IF i_egi = 'BAY' THEN
               SELECT A.*
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
                  ,NULL
               INTO   v_risk_rec
               FROM   structools.st_risk_bay_replace_npv15 A
               WHERE  pick_id = i_pick_id;
            ELSIF i_egi = 'PWOD' THEN
               SELECT A.*
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
                     ,NULL
                  ,NULL
               INTO   v_risk_rec
               FROM   structools.st_risk_pwod_replace_npv15 A
               WHERE  pick_id = i_pick_id;
            END IF;
         ELSIF i_event = 'REINFORCE' THEN
            SELECT A.*
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
                  ,NULL
            INTO   v_risk_rec
            FROM   structools.st_risk_pwod_reinf_npv15 A
            WHERE  pick_id = i_pick_id;
         END IF;

         v_risk_tab.EXTEND(51);
         v_risk_tab(1) := v_risk_rec.yr0; 
         v_risk_tab(2) := v_risk_rec.yr1; 
         v_risk_tab(3) := v_risk_rec.yr2; 
         v_risk_tab(4) := v_risk_rec.yr3;
         v_risk_tab(5) := v_risk_rec.yr4; 
         v_risk_tab(6) := v_risk_rec.yr5; 
         v_risk_tab(7) := v_risk_rec.yr6; 
         v_risk_tab(8) := v_risk_rec.yr7;
         v_risk_tab(9) := v_risk_rec.yr8; 
         v_risk_tab(10) := v_risk_rec.yr9; 
         v_risk_tab(11) := v_risk_rec.yr10; 
         v_risk_tab(12) := v_risk_rec.yr11;
         v_risk_tab(13) := v_risk_rec.yr12; 
         v_risk_tab(14) := v_risk_rec.yr13; 
         v_risk_tab(15) := v_risk_rec.yr14; 
         v_risk_tab(16) := v_risk_rec.yr15;
         v_risk_tab(17) := v_risk_rec.yr16; 
         v_risk_tab(18) := v_risk_rec.yr17; 
         v_risk_tab(19) := v_risk_rec.yr18; 
         v_risk_tab(20) := v_risk_rec.yr19;
         v_risk_tab(21) := v_risk_rec.yr20; 
         v_risk_tab(22) := v_risk_rec.yr21; 
         v_risk_tab(23) := v_risk_rec.yr22; 
         v_risk_tab(24) := v_risk_rec.yr23;
         v_risk_tab(25) := v_risk_rec.yr24; 
         v_risk_tab(26) := v_risk_rec.yr25; 
         v_risk_tab(27) := v_risk_rec.yr26; 
         v_risk_tab(28) := v_risk_rec.yr27;
         v_risk_tab(29) := v_risk_rec.yr28; 
         v_risk_tab(30) := v_risk_rec.yr29; 
         v_risk_tab(31) := v_risk_rec.yr30; 
         v_risk_tab(32) := v_risk_rec.yr31;
         v_risk_tab(33) := v_risk_rec.yr32; 
         v_risk_tab(34) := v_risk_rec.yr33; 
         v_risk_tab(35) := v_risk_rec.yr34; 
         v_risk_tab(36) := v_risk_rec.yr35;
         v_risk_tab(37) := v_risk_rec.yr36; 
         v_risk_tab(38) := v_risk_rec.yr37; 
         v_risk_tab(39) := v_risk_rec.yr38; 
         v_risk_tab(40) := v_risk_rec.yr39;
         v_risk_tab(41) := v_risk_rec.yr40; 
         v_risk_tab(42) := v_risk_rec.yr41; 
         v_risk_tab(43) := v_risk_rec.yr42; 
         v_risk_tab(44) := v_risk_rec.yr43;
         v_risk_tab(45) := v_risk_rec.yr44; 
         v_risk_tab(46) := v_risk_rec.yr45; 
         v_risk_tab(47) := v_risk_rec.yr46; 
         v_risk_tab(48) := v_risk_rec.yr47;
         v_risk_tab(49) := v_risk_rec.yr48; 
         v_risk_tab(50) := v_risk_rec.yr49;
         v_risk_tab(51) := v_risk_rec.yr50;

         IF i_sif_ind = 'Y' THEN
            o_risk := v_risk_tab(i_idx) * (v_fire_frac + v_eshock_frac + v_physimp_frac)  * v_risk_pubsafety_sif
                    + v_risk_tab(i_idx) * v_wforce_frac                                   * v_risk_wforce_sif
                    + v_risk_tab(i_idx) * v_reliability_frac                              * v_risk_reliability_sif
                    + v_risk_tab(i_idx) * v_env_frac                                      * v_risk_env_sif
                    + v_risk_tab(i_idx) * v_fin_frac                                      * v_risk_fin_sif;
            o_risk := o_risk * v_adj_factor;
         ELSE
            o_risk := v_risk_tab(i_idx);
         END IF;
         
         RETURN o_risk;
            
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RETURN NULL;
         WHEN OTHERS THEN
            dbms_output.put_line('i_idx = '||i_idx||' i_egi = '||i_egi||' i_pick_id = '||i_pick_id||' i_event = '||i_event);
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);         
            RAISE;
                           
      END get_risk_npv;   

      PROCEDURE process_del_strategy_01 IS
      
      BEGIN
      
         /* Get the seed value */
         SELECT del_strategy_parm_val
         INTO   v_seed
         FROM   structools.st_del_strategy_scenario
         WHERE  del_strategy_id = i_del_strategy_id
         AND    del_strategy_scenario_id = i_del_strategy_scenario_id
         AND    del_strategy_parm_nam = 'FAIL_FCAST_RANDOM_SEED'
         ;
         
         /* get sif weightings */
--         get_sif_weightings(v_parm_scen_id_sif_weighting, v_parm_scen_id_fdr_cat_adj);
         structoolsapp.common_procs.get_sif_weightings(v_parm_scen_id_sif_weighting, v_parm_scen_id_fdr_cat_adj
                                                         ,v_fdr_cat_sif_tab);      
         

         /* Get bay repair cost value */
         SELECT total_cost
         INTO   v_bay_repair_cost
         FROM   structools.st_eqp_type_action_cost
         WHERE  equip_grp_id = 'BAY'
         AND    action = 'REPAIR'
         ;
         
         /* Get pole replace cost for reactive maint */
         SELECT total_cost
         INTO   v_pwod_reac_replace_cost
         FROM   structools.st_eqp_type_action_cost
         WHERE  equip_grp_id = 'PWOD'
         AND    action = 'REAC_REPLACE'
         ;
         
         dbms_random.initialize(v_seed);
         v_reac_maint_tab.DELETE;
         OPEN reactive_maint_csr;
         FETCH reactive_maint_csr BULK COLLECT INTO v_reac_maint_tab;
         CLOSE reactive_maint_csr;
         


         FOR i IN 1 .. v_reac_maint_tab.COUNT LOOP
--            IF v_reac_maint_tab(i).pick_id = '1001533' THEN
--               dbms_output.put_line(v_reac_maint_tab(i).pick_id||', '||v_reac_maint_tab(i).equip_grp_id||', '||v_reac_maint_tab(i).del_year
--                           ||', '||v_reac_maint_tab(i).event||', '||v_reac_maint_tab(i).p_fail||', '||v_reac_maint_tab(i).p_fail_random); --graz
--            END IF;
            /* relies on the risk for NOACTION always being obtained first, so that the sif fractions are known to the subsequent invocation of get_risk proc*/
            IF v_reac_maint_tab(i).equip_grp_id = 'BAY' THEN
               IF v_reac_maint_tab(i).event IS NULL THEN  --ie no action took place against this bay in the past, and it has come up in the sample for a REPAIR this year
                  /* Calculate risk reduction */
                  v_risk_noaction := get_risk('BAY', v_reac_maint_tab(i).pick_id, 'NOACTION', i_del_year - v_this_year + 1);
                  IF v_risk_noaction IS NOT NULL THEN
                     v_risk_repair   := get_risk('BAY', v_reac_maint_tab(i).pick_id, 'REPAIR', i_del_year - v_this_year + 1);  
                     IF v_risk_noaction > v_risk_repair THEN
                        v_risk_red := v_risk_noaction - v_risk_repair;
                     ELSE
                        v_risk_red := 0;
                     END IF;
                  ELSE
                     v_risk_red := 0;
                  END IF;
                  /* Calculate risk reduction npv*/
                  v_risk_noaction_npv := get_risk_npv('BAY', v_reac_maint_tab(i).pick_id, 'NOACTION', i_del_year - v_this_year + 1,'N', v_reac_maint_tab(i).fdr_cat);
                  IF v_risk_noaction_npv IS NOT NULL THEN
                     v_risk_repair_npv   := get_risk_npv('BAY', v_reac_maint_tab(i).pick_id, 'REPAIR', i_del_year - v_this_year + 1, 'N', v_reac_maint_tab(i).fdr_cat);
                     IF v_risk_noaction_npv > v_risk_repair_npv THEN
                        v_risk_red_npv := v_risk_noaction_npv - v_risk_repair_npv;
                     ELSE
                        v_risk_red_npv := 0;
                     END IF;
                  ELSE
                     v_risk_red_npv := 0;
                  END IF;
                  /* Calculate risk reduction npv sif modified */
                  v_risk_noaction_npv_sif := get_risk_npv('BAY', v_reac_maint_tab(i).pick_id, 'NOACTION', i_del_year - v_this_year + 1, 'Y', v_reac_maint_tab(i).fdr_cat);
                  IF v_risk_noaction_npv_sif IS NOT NULL THEN
                     v_risk_repair_npv_sif   := get_risk_npv('BAY', v_reac_maint_tab(i).pick_id, 'REPAIR', i_del_year - v_this_year + 1, 'Y', v_reac_maint_tab(i).fdr_cat);
                     IF v_risk_noaction_npv_sif > v_risk_repair_npv_sif THEN
                        v_risk_red_npv_sif := v_risk_noaction_npv_sif - v_risk_repair_npv_sif;
                     ELSE
                        v_risk_red_npv_sif := 0;
                     END IF;
                  ELSE
                     v_risk_red_npv_sif := 0;
                  END IF;
               ELSE -- REPLACE or REPAIR in the past   
                  /* Calculate risk reduction */
                  IF v_reac_maint_tab(i).event = 'REPLACE' THEN
                     v_risk_noaction := get_risk('BAY', v_reac_maint_tab(i).pick_id, v_reac_maint_tab(i).event, i_del_year - v_reac_maint_tab(i).del_year + 1);
                  ELSE
                     v_risk_noaction := get_risk('BAY', v_reac_maint_tab(i).pick_id, v_reac_maint_tab(i).event, i_del_year - v_this_year + 1);
                  END IF;
                  IF v_risk_noaction IS NOT NULL THEN
                     v_risk_repair   := get_risk('BAY', v_reac_maint_tab(i).pick_id, 'REPAIR', i_del_year - v_this_year + 1);
                     IF v_risk_noaction > v_risk_repair THEN
                        v_risk_red := v_risk_noaction - v_risk_repair;
                     ELSE
                        v_risk_red := 0;
                     END IF;
                  ELSE
                     v_risk_red := 0;
                  END IF;
                  /* Calculate risk reduction npv*/
                  v_risk_noaction_npv := get_risk_npv('BAY', v_reac_maint_tab(i).pick_id, v_reac_maint_tab(i).event, i_del_year - v_reac_maint_tab(i).del_year + 1, 'N', v_reac_maint_tab(i).fdr_cat);
                  IF v_risk_noaction_npv IS NOT NULL THEN
                     v_risk_repair_npv   := get_risk_npv('BAY', v_reac_maint_tab(i).pick_id, 'REPAIR', i_del_year - v_this_year + 1, 'N', v_reac_maint_tab(i).fdr_cat);
                     IF v_risk_noaction_npv > v_risk_repair_npv THEN
                        v_risk_red_npv := v_risk_noaction_npv - v_risk_repair_npv;
                     ELSE
                        v_risk_red_npv := 0;
                     END IF;
                  ELSE
                     v_risk_red_npv := 0;
                  END IF;
                  /* Calculate risk reduction npv sif modified */
                  v_risk_noaction_npv_sif := get_risk_npv('BAY', v_reac_maint_tab(i).pick_id, v_reac_maint_tab(i).event, i_del_year - v_reac_maint_tab(i).del_year + 1, 'Y', v_reac_maint_tab(i).fdr_cat);
                  IF v_risk_noaction_npv_sif IS NOT NULL THEN
                     v_risk_repair_npv_sif   := get_risk_npv('BAY' ,v_reac_maint_tab(i).pick_id, 'REPAIR', i_del_year - v_this_year + 1, 'Y', v_reac_maint_tab(i).fdr_cat);
                     IF v_risk_noaction_npv_sif > v_risk_repair_npv_sif THEN
                        v_risk_red_npv_sif := v_risk_noaction_npv_sif - v_risk_repair_npv_sif;
                     ELSE
                        v_risk_red_npv_sif := 0;
                     END IF;
                  ELSE
                     v_risk_red_npv_sif := 0;
                  END IF;
               END IF;
            ELSIF v_reac_maint_tab(i).equip_grp_id = 'PWOD' THEN 
               IF v_reac_maint_tab(i).event IS NULL THEN   --ie no action took place against this pole in the past, and it has come up in the sample for a REPLACE this year
                  /* Calculate risk reduction */
                  v_risk_noaction := get_risk('PWOD', v_reac_maint_tab(i).pick_id, 'NOACTION', i_del_year - v_this_year + 1);
                  IF v_risk_noaction IS NOT NULL THEN
                     v_risk_replace   := get_risk('PWOD', v_reac_maint_tab(i).pick_id, 'REPLACE', 1);
                     IF v_risk_noaction > v_risk_replace THEN
                        v_risk_red := v_risk_noaction - v_risk_replace;
                     ELSE
                        v_risk_red := 0;
                     END IF;
                  ELSE
                     v_risk_red := 0;
                  END IF;
                  /* Calculate risk reduction npv*/
                  v_risk_noaction_npv := get_risk_npv('PWOD', v_reac_maint_tab(i).pick_id, 'NOACTION', i_del_year - v_this_year + 1, 'N', v_reac_maint_tab(i).fdr_cat);
                  IF v_risk_noaction_npv IS NOT NULL THEN
                     v_risk_replace_npv   := get_risk_npv('PWOD', v_reac_maint_tab(i).pick_id, 'REPLACE', 1, 'N', v_reac_maint_tab(i).fdr_cat);
                     IF v_risk_noaction_npv > v_risk_replace_npv THEN
                        v_risk_red_npv := v_risk_noaction_npv - v_risk_replace_npv;
                     ELSE
                        v_risk_red_npv := 0;
                     END IF;
                  ELSE
                     v_risk_red_npv := 0;
                  END IF;
                  /* Calculate risk reduction npv sif modified */
                  v_risk_noaction_npv_sif := get_risk_npv('PWOD', v_reac_maint_tab(i).pick_id, 'NOACTION', i_del_year - v_this_year + 1, 'Y', v_reac_maint_tab(i).fdr_cat);
                  IF v_risk_noaction_npv_sif IS NOT NULL THEN
                     v_risk_replace_npv_sif  := get_risk_npv('PWOD', v_reac_maint_tab(i).pick_id, 'REPLACE', 1, 'Y', v_reac_maint_tab(i).fdr_cat);
                     IF v_risk_noaction_npv_sif > v_risk_replace_npv_sif THEN
                        v_risk_red_npv_sif := v_risk_noaction_npv_sif - v_risk_replace_npv_sif;
                     ELSE
                        v_risk_red_npv_sif := 0;
                     END IF;
                  ELSE
                     v_risk_red_npv_sif := 0;
                  END IF;
               ELSIF v_reac_maint_tab(i).event = 'REINFORCE' THEN  -- reinforce in the past
                  /* Calculate risk reduction */
                  v_risk_noaction := get_risk('PWOD', v_reac_maint_tab(i).pick_id, 'REINFORCE', i_del_year - v_this_year + 1);
                  IF v_risk_noaction IS NOT NULL THEN
                     v_risk_replace   := get_risk('PWOD', v_reac_maint_tab(i).pick_id, 'REPLACE', 1);  --repair index - check and correct if required
                     IF v_risk_noaction > v_risk_replace THEN
                        v_risk_red := v_risk_noaction - v_risk_replace;
                     ELSE
                        v_risk_red := 0;
                     END IF;
                  ELSE
                     v_risk_red := 0;
                  END IF;
                  /* Calculate risk reduction npv*/
                  v_risk_noaction_npv := get_risk_npv('PWOD', v_reac_maint_tab(i).pick_id, 'REINFORCE', i_del_year - v_this_year + 1, 'N', v_reac_maint_tab(i).fdr_cat);
                  IF v_risk_noaction_npv IS NOT NULL THEN
                     v_risk_replace_npv   := get_risk_npv('PWOD', v_reac_maint_tab(i).pick_id, 'REPLACE', 1, 'N', v_reac_maint_tab(i).fdr_cat);
                     IF v_risk_noaction_npv > v_risk_replace_npv THEN
                        v_risk_red_npv := v_risk_noaction_npv - v_risk_replace_npv;
                     ELSE
                        v_risk_red_npv := 0;
                     END IF;
                  ELSE
                     v_risk_red_npv := 0;
                  END IF;
                  /* Calculate risk reduction npv sif modified */
                  v_risk_noaction_npv_sif := get_risk_npv('PWOD', v_reac_maint_tab(i).pick_id, 'REINFORCE', i_del_year - v_this_year + 1, 'Y', v_reac_maint_tab(i).fdr_cat);
                  IF v_risk_noaction_npv_sif IS NOT NULL THEN
                     v_risk_replace_npv_sif  := get_risk_npv('PWOD', v_reac_maint_tab(i).pick_id, 'REPLACE', 1, 'Y', v_reac_maint_tab(i).fdr_cat);
                     IF v_risk_noaction_npv_sif > v_risk_replace_npv_sif THEN
                        v_risk_red_npv_sif := v_risk_noaction_npv_sif - v_risk_replace_npv_sif;
                     ELSE
                        v_risk_red_npv_sif := 0;
                     END IF;
                  ELSE
                     v_risk_red_npv_sif := 0;
                  END IF;
               ELSE -- REPLACE in the past   
                  /* Calculate risk reduction */
                  v_risk_noaction := get_risk('PWOD', v_reac_maint_tab(i).pick_id, 'REPLACE', i_del_year - v_reac_maint_tab(i).del_year + 1);
                  IF v_risk_noaction IS NOT NULL THEN
                     v_risk_replace   := get_risk('PWOD', v_reac_maint_tab(i).pick_id, 'REPLACE', 1);  --repair index - check and correct if required
                     IF v_risk_noaction > v_risk_replace THEN
                        v_risk_red := v_risk_noaction - v_risk_replace;
                     ELSE
                        v_risk_red := 0;
                     END IF;
                  ELSE
                     v_risk_red := 0;
                  END IF;
                  /* Calculate risk reduction npv*/
                  v_risk_noaction_npv := get_risk_npv('PWOD', v_reac_maint_tab(i).pick_id, 'REPLACE', i_del_year - v_reac_maint_tab(i).del_year + 1, 'N', v_reac_maint_tab(i).fdr_cat);
                  IF v_risk_noaction_npv IS NOT NULL THEN
                     v_risk_replace_npv   := get_risk_npv('PWOD', v_reac_maint_tab(i).pick_id, 'REPLACE', 1, 'N', v_reac_maint_tab(i).fdr_cat);
                     IF v_risk_noaction_npv > v_risk_replace_npv THEN
                        v_risk_red_npv := v_risk_noaction_npv - v_risk_replace_npv;
                     ELSE
                        v_risk_red_npv := 0;
                     END IF;
                  ELSE
                     v_risk_red_npv := 0;
                  END IF;
                  /* Calculate risk reduction npv sif modified */
                  v_risk_noaction_npv_sif := get_risk_npv('PWOD', v_reac_maint_tab(i).pick_id, 'REPLACE', i_del_year - v_reac_maint_tab(i).del_year + 1, 'Y', v_reac_maint_tab(i).fdr_cat);
                  IF v_risk_noaction_npv_sif IS NOT NULL THEN
                     v_risk_replace_npv_sif  := get_risk_npv('PWOD', v_reac_maint_tab(i).pick_id, 'REPLACE', 1, 'Y', v_reac_maint_tab(i).fdr_cat);
                     IF v_risk_noaction_npv_sif > v_risk_replace_npv_sif THEN
                        v_risk_red_npv_sif := v_risk_noaction_npv_sif - v_risk_replace_npv_sif;
                     ELSE
                        v_risk_red_npv_sif := 0;
                     END IF;
                  ELSE
                     v_risk_red_npv_sif := 0;
                  END IF;
               END IF;
            END IF;   
            IF v_reac_maint_tab(i).equip_grp_id = 'BAY' THEN
               v_event := 'REPAIR';
               v_pick_id := 'B'||v_reac_maint_tab(i).pick_id;
            --ELSIF v_reac_maint_tab(i).equip_grp_id = 'PWOD' THEN
            ELSE
               v_event := 'REPLACE';
               v_pick_id := 'S'||v_reac_maint_tab(i).pick_id;
            END IF;
            
            INSERT INTO structools.st_del_program_reac_maint
               VALUES (i_program_id
                      ,i_base_run_id
                      ,i_del_year
                      ,i_del_strategy_id
                      ,i_del_strategy_scenario_id
                      ,v_pick_id
                      ,v_reac_maint_tab(i).mtzn
                      ,v_reac_maint_tab(i).vic_id
                      ,v_reac_maint_tab(i).equip_grp_id
                      ,v_event
                      ,v_risk_red
                      ,v_risk_red_npv
                      ,v_risk_red_npv_sif
                      ,CASE v_reac_maint_tab(i).equip_grp_id
                        WHEN 'BAY'  THEN  v_risk_red_npv / v_bay_repair_cost
                        WHEN 'PWOD' THEN  v_risk_red_npv / v_pwod_reac_replace_cost
                        ELSE              0
                      END
                      ,CASE v_reac_maint_tab(i).equip_grp_id
                        WHEN 'BAY'  THEN  v_bay_repair_cost
                        WHEN 'PWOD' THEN  v_pwod_reac_replace_cost
                        ELSE              0
                      END
                      ,v_reac_maint_tab(i).span_len_m
                      ,' '      --v_reac_maint_tab(i).part_cde
                      ,CASE
                      	WHEN v_reac_maint_tab(i).equip_grp_id = 'PWOD' AND v_event = 'REPLACE' THEN v_reac_maint_tab(i).labour_hrs
                        ELSE																																						NULL
                      END					--AS labour_hrs
                      );   
         END LOOP;
         
         COMMIT;

      EXCEPTION
         WHEN OTHERS THEN
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;

      END process_del_strategy_01;
   
   /*****************************************************************/
   /* Mainline of create_reac_maint_program procedure                    */
   /*****************************************************************/
   
   BEGIN

      dbms_output.put_line('create_reac_maint_program Start '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

      BEGIN
         SELECT first_year
               ,run_years
               ,suppress_pickids_ver
--               ,parent_run_id
         INTO   v_first_year
               ,v_run_years
               ,v_suppress_pickids_ver
--               ,v_parent_run_id
         FROM structools.st_run_request
         WHERE run_id = i_base_run_id;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            NULL;
      END;
      
      IF v_first_year IS NULL THEN
         v_first_year := v_this_year + 1;
      END IF;
      
--      IF v_parent_run_id IS NULL THEN
--         v_parent_run_id_first := -1;
--         v_parent_run_id_last := -1;
--      ELSE
--         v_parent_run_id_first := v_parent_run_id;
--         v_parent_run_id_last  := i_base_run_id - 1; 
--      END IF;

--      get_parameter_scenarios(i_base_run_id);
      structoolsapp.common_procs.get_parameter_scenarios(i_base_run_id, v_parm_scen_id_life_exp, v_parm_scen_id_seg_min_len,  v_parm_scen_id_seg_cons_pcnt, v_parm_scen_id_sif_weighting
                                                               , v_parm_scen_id_seg_rr_doll,  v_parm_scen_id_eqp_rr_doll, v_parm_scen_id_fdr_cat_adj);   
      


      IF v_first_year > v_this_year + 1 THEN
         v_year_0 := v_first_year - 1;
      ELSE
         v_year_0 := v_this_year;
      END IF;

      /* Validate */ 
      IF i_base_run_id >= 0 AND i_del_year >=( v_year_0 + 1) AND i_del_strategy_id > 0 AND i_del_strategy_scenario_id > 0 THEN
         NULL;
      ELSE
         dbms_output.put_line('VALIDATION ERROR - input DATA NOT valid ');
         RAISE v_prog_excp;
      END IF;
       
      
      IF i_del_strategy_id = 1 OR i_del_strategy_id = 2 THEN
         process_del_strategy_01;
      ELSE
         dbms_output.put_line('Delivery strategy '||i_del_strategy_id||' NOT DEFINED');
         RAISE v_prog_excp;
      END IF;

      COMMIT;
      
      dbms_output.put_line('create_reac_maint_program END '||TO_CHAR(SYSDATE,'dd-mm-yyyy hh24:mi:ss'));

   EXCEPTION
      WHEN v_prog_excp THEN
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;

   END create_reac_maint_program;
   

/*****************************************************************/
/* Package initialisation                                        */
/*****************************************************************/

BEGIN

   NULL;
   
END optimiser_cycle;
/
