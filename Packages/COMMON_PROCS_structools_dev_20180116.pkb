CREATE OR REPLACE PACKAGE BODY STRUCTOOLSAPP.common_procs
AS

   /*****************************************************************************************************************************************/
   /* H6 Changes:                                                                                                                                                                                                                                                          */
   /*    14/08/17 Grazyna Szadkowski   Created by extracting common procs from structured_tools.pkb.                                                                                                                                */
   /*****************************************************************************************************************************************/
   

   v_this_year                   PLS_INTEGER := EXTRACT (YEAR FROM SYSDATE);
   v_year_0                      PLS_INTEGER := EXTRACT (YEAR FROM SYSDATE);
   v_null_end_dt                     CONSTANT DATE := TO_DATE('01-01-3000','dd-mm-yyyy');
   v_prog_excp                   EXCEPTION;
   
   v_request_not_valid     EXCEPTION;

   v_cost_key                 VARCHAR2(60);
   TYPE cost_rec_typ          IS RECORD
   (labour_hrs               NUMBER
   ,total_cost                NUMBER
   );
   v_cost_rec              cost_rec_typ;
      
   TYPE cost_tab_typ     IS TABLE OF cost_rec_typ INDEX BY VARCHAR2(60);
   /* table of costs, indexed by a concatenation of various factors, depending on eqp type */   
   v_cost_tab            cost_tab_typ;
   

   PROCEDURE get_parameter_scenarios(i_run_id                        IN PLS_INTEGER
                                                         ,o_parm_scen_id_life_exp               OUT   PLS_INTEGER
                                                         ,o_parm_scen_id_seg_min_len        OUT   PLS_INTEGER
                                                         ,o_parm_scen_id_seg_cons_pcnt     OUT   PLS_INTEGER
                                                         ,o_parm_scen_id_sif_weighting       OUT   PLS_INTEGER
                                                         ,o_parm_scen_id_seg_rr_doll          OUT   PLS_INTEGER
                                                         ,o_parm_scen_id_eqp_rr_doll          OUT   PLS_INTEGER
                                                         ,o_parm_scen_id_fdr_cat_adj         OUT   PLS_INTEGER)   IS
         
   BEGIN
         
      BEGIN
         SELECT parameter_scenario_id
         INTO   o_parm_scen_id_life_exp
         FROM  structools.st_run_request_parameter     
         WHERE run_id = i_run_id
         AND   parameter_id = 'LIFE_EXPECTANCY'
         ;  
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            o_parm_scen_id_life_exp := 1;
      END;
            
      BEGIN
         SELECT parameter_scenario_id
         INTO   o_parm_scen_id_seg_cons_pcnt
         FROM  structools.st_run_request_parameter     
         WHERE run_id = i_run_id
         AND   parameter_id = 'SEG_CONS_PCNT'
         ;  
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            o_parm_scen_id_seg_cons_pcnt := 1;
      END;
            
      BEGIN
         SELECT parameter_scenario_id
         INTO   o_parm_scen_id_seg_min_len
         FROM  structools.st_run_request_parameter     
         WHERE run_id = i_run_id
         AND   parameter_id = 'SEG_CONS_MIN_LEN'
         ;  
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            o_parm_scen_id_seg_min_len := 1;
      END;
            
      BEGIN
         SELECT parameter_scenario_id
         INTO   o_parm_scen_id_sif_weighting
         FROM  structools.st_run_request_parameter     
         WHERE run_id = i_run_id
         AND   parameter_id = 'SIF_WEIGHTING'
         ;  
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            o_parm_scen_id_sif_weighting := 1;
      END;

      BEGIN
         SELECT parameter_scenario_id
         INTO   o_parm_scen_id_seg_rr_doll
         FROM  structools.st_run_request_parameter     
         WHERE run_id = i_run_id
         AND   parameter_id = 'SEG_RR_DOLL_YR'
      ;  
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            o_parm_scen_id_seg_rr_doll := 1;
      END;

      BEGIN
         SELECT parameter_scenario_id
         INTO   o_parm_scen_id_eqp_rr_doll
         FROM  structools.st_run_request_parameter     
         WHERE run_id = i_run_id
         AND   parameter_id = 'EQP_RR_DOLL_YR'
      ;  
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            o_parm_scen_id_eqp_rr_doll := 1;
      END;

      BEGIN
         SELECT parameter_scenario_id
         INTO   o_parm_scen_id_fdr_cat_adj
         FROM  structools.st_run_request_parameter     
         WHERE run_id = i_run_id
         AND   parameter_id = 'FDR_CAT_ADJ'
      ;  
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            o_parm_scen_id_fdr_cat_adj := 1;
      END;

   END get_parameter_scenarios;

   /* This procedure obtains sif weightings for all the sif categories for feeder category for parameter scenario.                   */
   /* It populates global package table (assoc array) v_fdr_cat_sif_tab and also a global temp table st_gtt_fdr_cat_adj.             */
   /* The gtt table is only used in create_summary_for_opensolver procedure; other procs use the associative array to obtain values  */
   /* for feeder category, for efficiency reasons.                                                                                   */
   /* Both are populated here to make maintenance easier if a change is required (as it should apply to both, as both should contain */
   /* th same data.                                                                                                                  */
   PROCEDURE get_sif_weightings(i_parm_scen_id_sif_weighting  IN PLS_INTEGER, i_parm_scen_id_fdr_cat_adj  IN PLS_INTEGER
                                                      ,o_fdr_cat_sif_tab OUT   fdr_cat_sif_tab_typ) IS

      v_fdr_cat_sif_tab                   fdr_cat_sif_tab_typ;
   
      v_risk_pubsafety_sif        FLOAT(126);
      v_risk_wforce_sif           FLOAT(126);
      v_risk_reliability_sif      FLOAT(126);
      v_risk_env_sif              FLOAT(126);
      v_risk_fin_sif              FLOAT(126);
         
      v_risk_pubsafety_sif_pcnt     FLOAT(126);
      v_risk_wforce_sif_pcnt        FLOAT(126); 
      v_risk_reliability_sif_pcnt   FLOAT(126);
      v_risk_env_sif_pcnt           FLOAT(126); 
      v_risk_fin_sif_pcnt           FLOAT(126);
      v_sif_tot_pcnt                FLOAT(126);
      v_parameter_fdr_cat           VARCHAR2(35);
      v_fdr_cat_prev                VARCHAR2(35);
      v_fdr_cat                     VARCHAR2(35);
      v_fdr_cat_adj_factor          NUMBER;
      v_sql                         VARCHAR2(1000);

        CURSOR fdr_cat_sif_and_adj_csr IS
         WITH fdr_cat_sif AS
         (
          SELECT parameter_risk_comp
                 ,TO_NUMBER(parameter_val)    AS fdr_cat_sif_pcnt
               ,parameter_fdr_cat
         FROM structools.st_parameter_scenario
         WHERE PARAMETER_ID = 'SIF_WEIGHTING'
         AND   scenario_id = i_parm_scen_id_sif_weighting
         )
         ,
         fdr_cat_adj AS
         (
         SELECT TO_NUMBER(parameter_val)   AS fdr_cat_adj_factor
                 ,parameter_fdr_cat
         FROM structools.st_parameter_scenario
         WHERE PARAMETER_ID = 'FDR_CAT_ADJ'
         AND   scenario_id = i_parm_scen_id_fdr_cat_adj
         )
         SELECT A.parameter_fdr_cat
               ,A.fdr_cat_adj_factor
               ,b.parameter_risk_comp
               ,b.fdr_cat_sif_pcnt
         FROM  fdr_cat_adj          A  INNER JOIN
               fdr_cat_sif          b
         ON A.parameter_fdr_cat = b.parameter_fdr_cat
         ORDER BY parameter_fdr_cat
                        
      ;    
         
   BEGIN         
   
      DELETE FROM structools.st_gtt_fdr_cat_adj;
      v_fdr_cat_sif_tab.DELETE;
                              
      v_fdr_cat_prev := ' ';
      FOR c IN fdr_cat_sif_and_adj_csr LOOP
         IF c.parameter_fdr_cat != v_fdr_cat_prev AND v_fdr_cat_prev != ' ' THEN
            v_sif_tot_pcnt := v_risk_pubsafety_sif_pcnt + v_risk_wforce_sif_pcnt + v_risk_reliability_sif_pcnt + v_risk_env_sif_pcnt --; --not always 100
                           + v_risk_fin_sif_pcnt;
                             
            v_risk_pubsafety_sif       := 5 * v_risk_pubsafety_sif_pcnt   / v_sif_tot_pcnt;
            v_risk_wforce_sif          := 5 * v_risk_wforce_sif_pcnt      / v_sif_tot_pcnt;
            v_risk_reliability_sif     := 5 * v_risk_reliability_sif_pcnt / v_sif_tot_pcnt;
            v_risk_env_sif             := 5 * v_risk_env_sif_pcnt         / v_sif_tot_pcnt; 
            v_risk_fin_sif             := 5 * v_risk_fin_sif_pcnt         / v_sif_tot_pcnt;

            v_fdr_cat_sif_tab(v_fdr_cat_prev).pubsafety_sif          := v_risk_pubsafety_sif;
            v_fdr_cat_sif_tab(v_fdr_cat_prev).wforce_sif             := v_risk_wforce_sif;
            v_fdr_cat_sif_tab(v_fdr_cat_prev).reliability_sif        := v_risk_reliability_sif;
            v_fdr_cat_sif_tab(v_fdr_cat_prev).env_sif                := v_risk_env_sif;
            v_fdr_cat_sif_tab(v_fdr_cat_prev).fin_sif                := v_risk_fin_sif;
            v_fdr_cat_sif_tab(v_fdr_cat_prev).adj_factor             := v_fdr_cat_adj_factor;          
                        
            INSERT INTO structools.st_gtt_fdr_cat_adj
                VALUES(v_fdr_cat_prev
                       ,v_risk_pubsafety_sif 
                     ,v_risk_wforce_sif  
                     ,v_risk_reliability_sif
                     ,v_risk_env_sif
                     ,v_fdr_cat_adj_factor
                     ,v_risk_fin_sif
                     )
            ;    
         END IF;
         IF c.parameter_risk_comp = 'PUBSAFETY' THEN
             v_risk_pubsafety_sif_pcnt    := c.fdr_cat_sif_pcnt;
         ELSIF c.parameter_risk_comp = 'WFORCE' THEN
             v_risk_wforce_sif_pcnt    := c.fdr_cat_sif_pcnt;
         ELSIF c.parameter_risk_comp = 'RELIABILITY' THEN
             v_risk_reliability_sif_pcnt    := c.fdr_cat_sif_pcnt;
         ELSIF c.parameter_risk_comp = 'ENV' THEN
             v_risk_env_sif_pcnt    := c.fdr_cat_sif_pcnt;
         ELSIF c.parameter_risk_comp = 'FIN' THEN
             v_risk_fin_sif_pcnt    := c.fdr_cat_sif_pcnt;
         END IF;
         v_fdr_cat_adj_factor := c.fdr_cat_adj_factor;
         v_fdr_cat_prev := c.parameter_fdr_cat;
         
      END LOOP;

      /* Process last fdr cat */
      v_sif_tot_pcnt := v_risk_pubsafety_sif_pcnt + v_risk_wforce_sif_pcnt + v_risk_reliability_sif_pcnt + v_risk_env_sif_pcnt --; --not always 100
                     + v_risk_fin_sif_pcnt;
                                                                                                                           
      v_risk_pubsafety_sif       := 5 * v_risk_pubsafety_sif_pcnt   / v_sif_tot_pcnt;
      v_risk_wforce_sif          := 5 * v_risk_wforce_sif_pcnt      / v_sif_tot_pcnt;
      v_risk_reliability_sif     := 5 * v_risk_reliability_sif_pcnt / v_sif_tot_pcnt;
      v_risk_env_sif             := 5 * v_risk_env_sif_pcnt         / v_sif_tot_pcnt; 
      v_risk_fin_sif             := 5 * v_risk_fin_sif_pcnt         / v_sif_tot_pcnt;

      v_fdr_cat_sif_tab(v_fdr_cat_prev).pubsafety_sif          := v_risk_pubsafety_sif;
      v_fdr_cat_sif_tab(v_fdr_cat_prev).wforce_sif             := v_risk_wforce_sif;
      v_fdr_cat_sif_tab(v_fdr_cat_prev).reliability_sif        := v_risk_reliability_sif;
      v_fdr_cat_sif_tab(v_fdr_cat_prev).env_sif                := v_risk_env_sif;
      v_fdr_cat_sif_tab(v_fdr_cat_prev).fin_sif                := v_risk_fin_sif;
      v_fdr_cat_sif_tab(v_fdr_cat_prev).adj_factor             := v_fdr_cat_adj_factor;          

      INSERT INTO structools.st_gtt_fdr_cat_adj
         VALUES(v_fdr_cat_prev
               ,v_risk_pubsafety_sif 
               ,v_risk_wforce_sif  
               ,v_risk_reliability_sif
               ,v_risk_env_sif
               ,v_fdr_cat_adj_factor
               ,v_risk_fin_sif
               )
      ;    
      
      o_fdr_cat_sif_tab := v_fdr_cat_sif_tab;

--      dbms_output.put_line('st_gtt_fdr_cat_adj TABLE dump');   
--        FOR c IN (SELECT * FROM structools.st_gtt_fdr_cat_adj ORDER BY fdr_cat) LOOP
--          dbms_output.put_line(c.fdr_cat||' '||c.sif_weight_pubsafety||' '||c.sif_weight_wforce||' '||c.sif_weight_reliability
--                                 ||' '||c.sif_weight_env||' '||c.sif_weight_fin||' '||c.adj_factor);
--      END LOOP;            
--      
--      dbms_output.put_line('v_fdr_cat_sif_tab assoc. array dump');   
--      v_fdr_cat_prev := v_fdr_cat_sif_tab.FIRST;
--      WHILE v_fdr_cat_prev IS NOT NULL LOOP
--          dbms_output.put_line(v_fdr_cat_prev||' '||v_fdr_cat_sif_tab(v_fdr_cat_prev).pubsafety_sif
--                                                ||' '||v_fdr_cat_sif_tab(v_fdr_cat_prev).wforce_sif
--                                                ||' '||v_fdr_cat_sif_tab(v_fdr_cat_prev).reliability_sif
--                                                ||' '||v_fdr_cat_sif_tab(v_fdr_cat_prev).env_sif
--                                                ||' '||v_fdr_cat_sif_tab(v_fdr_cat_prev).fin_sif
--                                                ||' '||v_fdr_cat_sif_tab(v_fdr_cat_prev).adj_factor);
--         v_fdr_cat_prev := v_fdr_cat_sif_tab.NEXT(v_fdr_cat_prev);
--      END LOOP;   

   END get_sif_weightings;


/*****************************************************************/
/* Package initialisation                                                                                               */
/*****************************************************************/

BEGIN

   NULL;
   
END common_procs;
/

