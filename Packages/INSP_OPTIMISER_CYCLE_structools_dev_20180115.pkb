CREATE OR REPLACE PACKAGE BODY structoolsapp.insp_optimiser_cycle
AS

   /*****************************************************************************************************************************************/
   /* 05/09/17 Grazyna Szadkowski   Created.                                                                                                                                                                                                                   */
   /*****************************************************************************************************************************************/
	/* global package variables */
   v_this_year                   PLS_INTEGER := EXTRACT (YEAR FROM SYSDATE);
   v_prog_excp                EXCEPTION;
   v_request_not_valid     EXCEPTION;   
   v_year							 PLS_INTEGER;
   v_first_year    				PLS_INTEGER;
   v_run_years   				 PLS_INTEGER;
	/**************************************************************************************************************/
   /* Produce counts of the different inspection counts for the year, for each maint zone.                                                                                                 */
   /**************************************************************************************************************/
   PROCEDURE summarise_mtzn_insp(i_run_id	IN PLS_INTEGER, i_program_id IN PLS_INTEGER, i_yr IN   PLS_INTEGER, i_smoothing_ind IN CHAR, i_st_run_id IN   PLS_INTEGER, i_st_program_id IN   PLS_INTEGER
   																	,i_insp_mtzn_committed_ver_id IN PLS_INTEGER
                                                      ,i_force_zone_cutoff_scen_id		IN PLS_INTEGER				/* determines cutoff value for force_selected_zone in insp_mtzn_summary */
                                                      ) IS
   
   	v_last_full_insp_yr		PLS_INTEGER;
      v_insp_typ					  VARCHAR2(10);
      v_insp_scope  	   		VARCHAR2(10);
      v_insp_gtt_mtzn_cnt	PLS_INTEGER;
		v_yr_idx							PLS_INTEGER;
      v_force_zone_cutoff	NUMBER;

		CURSOR insp_mtzn_summary_csr(i_run_id IN PLS_INTEGER, i_program_id IN PLS_INTEGER, i_yr IN PLS_INTEGER, i_smoothing_ind IN CHAR, i_yr_idx IN PLS_INTEGER, i_force_zone_cutoff IN NUMBER) IS 
   
         WITH insp AS
         (
         SELECT CASE	i_smoothing_ind
         					WHEN 'Y'											THEN	b.pick_id
                        ELSE													 			A.pick_id
         				END												AS pick_id
                    ,CASE	i_smoothing_ind
                    		WHEN 'Y'											THEN b.maint_zone_nam
                        ELSE															  A.maint_zone_nam
                    END													AS maint_zone_nam
                    ,CASE	i_smoothing_ind
                    		WHEN 'Y'											THEN b.insp_type
                        ELSE															  A.insp_type
                    END													AS insp_type
         FROM	structools.insp_plan_run					A  LEFT OUTER JOIN
         			structools.insp_plan_run_sm			 b
			ON A.run_id = b.run_id
         AND A.pick_id = b.pick_id
         AND A.calendar_year = b.calendar_year                  
         WHERE A.run_id = i_run_id
         AND A.calendar_year = i_yr
         )
         ,
         planned_insp AS
         (
         SELECT pick_id
         				,maint_zone_nam
                     ,insp_type
			FROM 		insp
         WHERE insp_type IS NOT NULL                     
         )
			,
         mod_insp AS
         (
         SELECT pick_id
                    ,maint_zone_nam
                    ,insp_type
         FROM	structools.insp_plan_mod_run
         WHERE run_id = i_run_id
         AND program_id = i_program_id
         AND calendar_year = i_yr
         AND insp_type IS NOT NULL
         )
         ,
         combined AS
         (
         SELECT A.pick_id							AS orig_pick_id
                    ,A.maint_zone_nam		AS orig_mtzn
                    ,A.insp_type						AS orig_insp_typ
                    ,b.pick_id						 AS mod_pick_id
                    ,b.maint_zone_nam	  AS mod_mtzn
                    ,b.insp_type 					AS mod_insp_typ
         FROM planned_insp				A		FULL OUTER JOIN
                   mod_insp					b
         ON A.pick_id = b.pick_id                
         )
         ,
         final_pickid_list AS
         (
         SELECT
            CASE
               WHEN mod_pick_id IS NOT NULL		THEN	mod_pick_id
               ELSE																       orig_pick_id
            END					AS pick_id
            ,CASE
               WHEN mod_pick_id IS NOT NULL			THEN	mod_mtzn
               ELSE																          orig_mtzn
            END					AS mtzn
            ,CASE
               WHEN mod_pick_id IS NOT NULL			THEN	mod_insp_typ
               ELSE																	       orig_insp_typ
            END					AS insp_type
         FROM	combined					A			
         )
         ,
         risk AS
         (
         SELECT 'S'||pick_id		AS pick_id
                     ,CASE i_yr_idx
                        WHEN 1				THEN 	risk_indx_val_y1
                        WHEN 2				THEN 	risk_indx_val_y2
                        WHEN 3				THEN 	risk_indx_val_y3
                        WHEN 4				THEN 	risk_indx_val_y4
                        WHEN 5				THEN 	risk_indx_val_y5
                        WHEN 6				THEN 	risk_indx_val_y6
                        WHEN 7				THEN 	risk_indx_val_y7
                        WHEN 8				THEN 	risk_indx_val_y8
                        WHEN 9				THEN 	risk_indx_val_y9
                        WHEN 10				THEN 	risk_indx_val_y10
                     END				AS risk_na
			FROM structools.st_risk_pwod_noaction         
         )
         ,
         mtzn_egi_cnt AS
         (
         SELECT mtzn
         				,pwod_cnt
                     ,pcon_cnt
                     ,pmet_cnt
                     ,paus_cnt
                     ,(nvl(pwod_risk_na,0) + nvl(pcon_risk_na,0) + nvl(pmet_risk_na,0) + nvl(paus_risk_na,0)) AS risk_na
         FROM
         (
         SELECT A.maint_zone_nam		AS mtzn
         				,A.equip_grp_id				
         			--	,pick_id
                     ,b.risk_na
			FROM structools.insp_plan_run				A		LEFT OUTER JOIN
         			risk													b
			ON A.pick_id = b.pick_id                  
         WHERE run_id = i_run_id
         AND calendar_year =  v_first_year     
         )
         PIVOT (
         			count(*) AS cnt
                  ,sum(risk_na) AS risk_na
                  FOR equip_grp_id IN ('PWOD' pwod,'PCON' pcon,'PMET' pmet, 'PAUS' paus )
                  )
         ORDER BY mtzn
         )
         ,
         mtzn_summ_tmp AS
         (
         SELECT *
         FROM
         (
         SELECT A.mtzn
               		,A.insp_type          
                     ,b.pwod_cnt
                     ,b.pcon_cnt
                     ,b.pmet_cnt
                     ,b.paus_cnt
                     ,B.risk_na
         FROM final_pickid_list		A	INNER JOIN
         			mtzn_egi_cnt			b
			ON A.mtzn = b.mtzn                  
         )
         PIVOT    (count(*) AS insp_cnt
                  FOR insp_type IN ('FULL' FULL,'VIS' vis,'VEG' veg ) )
         )
         ,
         mtzn_summ AS
         (
         SELECT A.*
                     ,CASE
                        WHEN b.mtzn IS NULL				THEN 				'N'
                        ELSE																		 'Y'
                     END						AS zbam_selected_ind
                     ,c.fire_risk_zone_cls
                     ,c.pub_safety_zone_cls
         FROM mtzn_summ_tmp									A	LEFT OUTER JOIN
                  structools.insp_gtt_mtzn_committed			b
         ON A.mtzn = b.mtzn                   
         																			LEFT OUTER JOIN
						structools.st_mtzn						c
			ON A.mtzn = c.mtzn 
         )
         SELECT A.*
         			,CASE
                  		WHEN zbam_selected_ind = 'Y' AND (((full_insp_cnt + vis_insp_cnt) / pwod_cnt) <= i_force_zone_cutoff) THEN 'Y'
                        ELSE																																										 'N'                                                                                   
						END				AS force_selected_zones
			FROM mtzn_summ			A            
      ;
      
      v_cnt							PLS_INTEGER;

   BEGIN                                                   
   
   	dbms_output.put_line('summarise_mtzn_insp. i_run_id = '||i_run_id||' i_program_id = '||i_program_id||' i_yr = '||i_yr||' i_st_run_id = '||i_st_run_id||' i_st_program_id = '
      											||i_st_program_id||' i_insp_mtzn_committed_ver_id = '||i_insp_mtzn_committed_ver_id);
                                       
   	/* Load a gtt table of committed maint. zones. If i_st_run_id and i_st_program_id are both 0, use insp_mtzn_committed. Otherwise, use st_del_program_mtzn_optimised. */
      
      /* Validate first */
      IF i_run_id IS NULL OR i_program_id IS NULL OR i_yr IS NULL OR i_st_run_id IS NULL OR i_st_program_id IS NULL THEN
			dbms_output.put_line ('NULL VALUES FOUND IN input PARAMETERS');            
         RAISE v_prog_excp;
      END IF;
      
      IF i_st_run_id = 0 AND i_st_program_id = 0 THEN
      	IF i_insp_mtzn_committed_ver_id IS NULL THEN
				dbms_output.put_line ('i_insp_mtzn_committed_version must NOT be NULL FOR i_st_run_id = 0 AND i_st_program_id = 0');            
         	RAISE v_prog_excp;
         END IF;
      END IF;

      IF i_force_zone_cutoff_scen_id IS NULL THEN 
         dbms_output.put_line ('i_force_zone_cutoff_scen_id must NOT be NUL');            
         RAISE v_prog_excp;
      END IF;

      SELECT first_year
      				,run_years
      	INTO v_first_year
         		 ,v_run_years
      FROM structools.insp_run_request
      WHERE run_id = i_run_id;

		IF i_yr = v_first_year THEN
         SELECT count(*)
         INTO v_cnt
         FROM structools.insp_mtzn_summary
         WHERE run_id = i_run_id AND program_id = i_program_id;
         IF v_cnt > 0 THEN
            dbms_output.put_line ('Delete existing optimisation results for run_id '||i_run_id||' and program_id '||i_program_id);            
            RAISE v_prog_excp;
         END IF;
         SELECT count(*)
         INTO v_cnt
         FROM structools.insp_mtzn_optimised
         WHERE run_id = i_run_id AND program_id = i_program_id;
         IF v_cnt > 0 THEN
            dbms_output.put_line ('Delete existing optimisation results for run_id '||i_run_id||' and program_id '||i_program_id);            
            RAISE v_prog_excp;
         END IF;
	END IF;         
      
      SELECT first_year
      				,run_years
      	INTO v_first_year
         		 ,v_run_years
      FROM structools.insp_run_request
      WHERE run_id = i_run_id;

      DELETE FROM structools.insp_gtt_mtzn_committed;
      
      IF i_st_run_id = 0 AND i_st_program_id = 0 THEN
      	INSERT INTO structools.insp_gtt_mtzn_committed
         SELECT mtzn
         FROM structools.insp_mtzn_committed
         WHERE commit_yr = v_first_year --i_yr
         AND mtzn_committed_version_id = i_insp_mtzn_committed_ver_id;
		ELSE         
      	INSERT INTO structools.insp_gtt_mtzn_committed
         SELECT REPLACE(mtzn, '_', '/') AS mtzn
         FROM structools.st_del_program_mtzn_optimised
         WHERE del_year = v_first_year --i_yr
         AND base_run_id = i_st_run_id AND program_id = i_st_program_id;
      END IF;
      
      v_yr_idx := i_yr - v_this_year;
      
      SELECT to_number(parameter_scenario_val)
      INTO v_force_zone_cutoff
      FROM structools.insp_parameter_scenario
      WHERE parameter_id = 'FORCE_ZONE_CUTOFF'
      AND parameter_scenario_id = i_force_zone_cutoff_scen_id;

      
		/* produce summary for each mtzn, with the insp_plan_mod_run overwriting insp_plan_run, where appropriate */
		FOR c IN insp_mtzn_summary_csr(i_run_id, i_program_id, i_yr, i_smoothing_ind, v_yr_idx, v_force_zone_cutoff) LOOP
      	INSERT INTO structools.insp_mtzn_summary
         	(
            run_id
            ,program_id
            ,maint_zone_nam
            ,calendar_year
            ,full_insp_cnt
            ,vis_insp_cnt
            ,veg_insp_cnt
            ,st_run_id
            ,st_program_id
            ,zbam_selected_ind
            ,pwod_cnt
            ,pcon_cnt
            ,pmet_cnt
            ,paus_cnt
            ,fire_risk_zone_cls
            ,pub_safety_zone_cls
            ,risk_na
            ,force_selected_zones
            )
            VALUES
            (
            i_run_id
            ,i_program_id
            ,c.mtzn
            ,i_yr
            ,c.full_insp_cnt
            ,c.vis_insp_cnt
            ,c.veg_insp_cnt
            ,i_st_run_id
            ,i_st_program_id
            ,c.zbam_selected_ind
            ,c.pwod_cnt
            ,c.pcon_cnt
            ,c.pmet_cnt
            ,c.paus_cnt
            ,c.fire_risk_zone_cls
            ,c.pub_safety_zone_cls
            ,c.risk_na
            ,c.force_selected_zones
            );
            
      END LOOP;                                
   
		COMMIT;
	
      EXCEPTION
         WHEN v_prog_excp THEN
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;
         WHEN OTHERS THEN
            dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
            dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            RAISE;

   END summarise_mtzn_insp;
        
    PROCEDURE update_insp_plan(i_run_id IN PLS_INTEGER, i_program_id IN PLS_INTEGER, i_year IN PLS_INTEGER, i_smoothing_ind IN CHAR) IS

		/* the 2 cursors below are identical excep one accesses insp_plan_run and the other insp_plan_run_sm */ 
		CURSOR pickids_full_not_treated_csr1 IS
         WITH mtzns_treated AS
         (
            SELECT                  
                  REPLACE(maint_zone_nam, '_', '/') AS maint_zone_nam
            FROM structools.insp_mtzn_optimised
            WHERE run_id = i_run_id AND program_id = i_program_id
            AND insp_yr = i_year
         )
         ,
         pickids_full AS
         (
         SELECT pick_id
                     ,maint_zone_nam
                     ,1								AS pickid_source
         FROM structools.insp_plan_mod_run
         WHERE run_id = i_run_id
         AND program_id = i_program_id
         AND calendar_year = i_year
         AND insp_type = 'FULL'
         UNION ALL
         SELECT pick_id
                     ,maint_zone_nam
                     ,2								AS pickid_source
         FROM structools.insp_plan_run
         WHERE run_id = i_run_id
         AND calendar_year = i_year
         AND insp_type = 'FULL'
         --ORDER BY pick_id, pickid_source
         )
         ,
         pickids_full_unique AS
         (
         SELECT pick_id
                     ,maint_zone_nam
                     ,min(pickid_source) AS pickid_source
         FROM pickids_full
         GROUP BY pick_id, maint_zone_nam            
         )
         --,
         --pickids_full_not_treated 	AS
         --(
         SELECT pick_id
            			,pickid_source
         FROM
         (
         SELECT A.pick_id
                     ,A.maint_zone_nam
                     ,A.pickid_source
                     ,b.maint_zone_nam			AS mtzn_treated
         FROM pickids_full_unique			A	LEFT OUTER JOIN           
                  mtzns_treated						b
         ON A.maint_zone_nam = b.maint_zone_nam    
         ) aa
         WHERE  mtzn_treated IS NULL
         ORDER BY pick_id
		;

		CURSOR pickids_full_not_treated_csr2 IS
         WITH mtzns_treated AS
         (
            SELECT REPLACE(maint_zone_nam, '_', '/') AS maint_zone_nam
            FROM structools.insp_mtzn_optimised
            WHERE run_id = i_run_id AND program_id = i_program_id
            AND insp_yr = i_year
         )
         ,
         pickids_full AS
         (
         SELECT pick_id
                     ,maint_zone_nam
                     ,1								AS pickid_source
         FROM structools.insp_plan_mod_run
         WHERE run_id = i_run_id
         AND program_id = i_program_id
         AND calendar_year = i_year
         AND insp_type = 'FULL'
         UNION ALL
         SELECT pick_id
                     ,maint_zone_nam
                     ,2								AS pickid_source
         FROM structools.insp_plan_run_sm
         WHERE run_id = i_run_id
         AND calendar_year = i_year
         AND insp_type = 'FULL'
         --ORDER BY pick_id, pickid_source
         )
         ,
         pickids_full_unique AS
         (
         SELECT pick_id
                     ,maint_zone_nam
                     ,min(pickid_source) AS pickid_source
         FROM pickids_full
         GROUP BY pick_id, maint_zone_nam            
         )
         --,
         --pickids_full_not_treated 	AS
         --(
         SELECT pick_id
            			,pickid_source
         FROM
         (
         SELECT A.pick_id
                     ,A.maint_zone_nam
                     ,A.pickid_source
                     ,b.maint_zone_nam			AS mtzn_treated
         FROM pickids_full_unique			A	LEFT OUTER JOIN           
                  mtzns_treated						b
         ON A.maint_zone_nam = b.maint_zone_nam    
         ) aa
         WHERE  mtzn_treated IS NULL
         ORDER BY pick_id
		;
      
      TYPE pickids_not_treated_tab_typ				IS TABLE OF pickids_full_not_treated_csr1%ROWTYPE;
      v_pickids_not_treated_tab					pickids_not_treated_tab_typ := pickids_not_treated_tab_typ();
      
      CURSOR pickid_mod_run_csr(i_pick_id IN varchar2) IS
      	SELECT *
         FROM structools.insp_plan_mod_run
         WHERE	run_id = i_run_id
         AND program_id = i_program_id 
         AND pick_id = i_pick_id
         AND calendar_year >= i_year
         ORDER BY calendar_year
      ;
      TYPE pickid_mod_run_tab_typ			IS TABLE OF pickid_mod_run_csr%ROWTYPE;
      v_pickid_mod_run_tab						pickid_mod_run_tab_typ := pickid_mod_run_tab_typ();

      TYPE pickid_mod_arr_typ					IS TABLE OF pickid_mod_run_tab_typ INDEX BY VARCHAR2(10);
      v_pickid_mod_run_arr						 pickid_mod_arr_typ;

      CURSOR pickid_orig_run_csr1(i_pick_id IN varchar2) IS
      	SELECT *
         FROM structools.insp_plan_run
         WHERE	run_id = i_run_id
         AND pick_id = i_pick_id
         ORDER BY calendar_year
      ;

      CURSOR pickid_orig_run_csr2(i_pick_id IN varchar2) IS
      	SELECT *
         FROM structools.insp_plan_run_sm
         WHERE	run_id = i_run_id
         AND pick_id = i_pick_id
         ORDER BY calendar_year
      ;
      
      TYPE pickid_orig_run_tab_typ			IS TABLE OF pickid_orig_run_csr1%ROWTYPE;
      v_pickid_orig_run_tab						pickid_orig_run_tab_typ := pickid_orig_run_tab_typ();

      j																PLS_INTEGER;
      v_pickid													VARCHAR2(10);
      
      TYPE pickid_orig_arr_typ					IS TABLE OF pickid_orig_run_tab_typ INDEX BY VARCHAR2(10);
      v_pickid_orig_run_arr						 pickid_orig_arr_typ;
      
   BEGIN
   
   	dbms_output.put_line('update_insp_plan. i_run_id = '||i_run_id||' i_program_id = '||i_program_id||'  i_year = '||i_year||' i_smoothing_ind = '||i_smoothing_ind);
      
      /* For each pickid with FULL inspection planned for the year, but not inspected because their mtzns were not selected, */
      /* update their inspection cycle for this year and following years. */
      
      IF i_smoothing_ind != 'Y' AND i_smoothing_ind != 'N' THEN
			dbms_output.put_line ('i_smoothing_ind is not valid '||i_smoothing_ind);            
         RAISE v_prog_excp;
      END IF;
      	
      IF i_smoothing_ind = 'N' THEN
         OPEN pickids_full_not_treated_csr1;
         FETCH pickids_full_not_treated_csr1 BULK COLLECT INTO v_pickids_not_treated_tab;
         CLOSE pickids_full_not_treated_csr1;
      ELSE
         OPEN pickids_full_not_treated_csr2;
         FETCH pickids_full_not_treated_csr2 BULK COLLECT INTO v_pickids_not_treated_tab;
         CLOSE pickids_full_not_treated_csr2;
      END IF;
      
      FOR i IN 1 .. v_pickids_not_treated_tab.count LOOP
      	IF v_pickids_not_treated_tab(i).pickid_source = 1 THEN
				--v_pickid_mod_run_tab.DELETE;
            OPEN pickid_mod_run_csr(v_pickids_not_treated_tab(i).pick_id);
            FETCH pickid_mod_run_csr BULK COLLECT INTO v_pickid_mod_run_tab;
            CLOSE pickid_mod_run_csr;
            /* Copy to an associative array, indexed by pick_id. */
            v_pickid_mod_run_arr(v_pickids_not_treated_tab(i).pick_id) := v_pickid_mod_run_tab;
            DELETE FROM structools.insp_plan_mod_run
            WHERE run_id = i_run_id AND program_id = i_program_id
            AND pick_id = v_pickids_not_treated_tab(i).pick_id
            AND calendar_year >= i_year;
			ELSE
				--v_pickid_orig_run_tab.DELETE;
             IF i_smoothing_ind = 'N' THEN
               OPEN pickid_orig_run_csr1(v_pickids_not_treated_tab(i).pick_id);
               FETCH pickid_orig_run_csr1 BULK COLLECT INTO v_pickid_orig_run_tab;
               CLOSE pickid_orig_run_csr1;
				ELSE               
               OPEN pickid_orig_run_csr2(v_pickids_not_treated_tab(i).pick_id);
               FETCH pickid_orig_run_csr2 BULK COLLECT INTO v_pickid_orig_run_tab;
               CLOSE pickid_orig_run_csr2;
            END IF;
            /* Copy to an associative array, indexed by pick_id. */
            v_pickid_orig_run_arr(v_pickids_not_treated_tab(i).pick_id) := v_pickid_orig_run_tab;
--            dbms_output.put_line('v_pickid_orig_run_tab.count = '||v_pickid_orig_run_tab.count);
			END IF;
		END LOOP;   
            
      /* Disable indexes for faster inserts */
      structools.schema_utils.disable_table_indexes('INSP_PLAN_MOD_RUN');
      
      /* Go through the saved arrays and populate data in insp_mod_run table. */ 
      v_pickid := v_pickid_mod_run_arr.first;
      WHILE v_pickid IS NOT NULL LOOP
      	v_pickid_mod_run_tab := v_pickid_mod_run_arr(v_pickid);
         FOR j IN 1 .. v_pickid_mod_run_tab.count - 1 LOOP
            IF j = 1 THEN  /* calendar_year = i_year. Nullify inspection data for this year */
               INSERT INTO structools.insp_plan_mod_run
                  (run_id
                  ,program_id
                  ,pick_id
                  ,maint_zone_nam
                  ,calendar_year
                  ,insp_type
                  ,insp_scope)
               VALUES
                  (i_run_id
                  ,i_program_id
                  ,v_pickid_mod_run_tab(j).pick_id
                  ,v_pickid_mod_run_tab(j).maint_zone_nam
                  ,v_pickid_mod_run_tab(j).calendar_year
                  ,NULL --insp_type
                  ,NULL --insp_scope
                  );
            END IF; 
            INSERT INTO structools.insp_plan_mod_run  /* Shift inspection data by 1 year */
               (run_id
               ,program_id
               ,pick_id
               ,maint_zone_nam
               ,calendar_year
               ,insp_type
               ,insp_scope)
            VALUES
               (i_run_id
               ,i_program_id
               ,v_pickid_mod_run_tab(j).pick_id
               ,v_pickid_mod_run_tab(j).maint_zone_nam
               ,v_pickid_mod_run_tab(j).calendar_year + 1
               ,v_pickid_mod_run_tab(j).insp_type
               ,v_pickid_mod_run_tab(j).insp_scope)
            ;
         END LOOP;

      	v_pickid := v_pickid_mod_run_arr.NEXT(v_pickid);
      END LOOP;
      
      v_pickid := v_pickid_orig_run_arr.first;
      WHILE v_pickid IS NOT NULL LOOP
      	v_pickid_orig_run_tab := v_pickid_orig_run_arr(v_pickid);
         FOR j IN 1 .. v_pickid_orig_run_tab.count LOOP
            IF  v_pickid_orig_run_tab(j).calendar_year < i_year THEN
               /* write out to the mod_run table, as is in the insp_plan_run table */
               INSERT INTO structools.insp_plan_mod_run
                  (run_id
                  ,program_id
                  ,pick_id
                  ,maint_zone_nam
                  ,calendar_year
                  ,insp_type
                  ,insp_scope)
               VALUES
                  (i_run_id
                  ,i_program_id
                  ,v_pickid_orig_run_tab(j).pick_id
                  ,v_pickid_orig_run_tab(j).maint_zone_nam
                  ,v_pickid_orig_run_tab(j).calendar_year
                  ,v_pickid_orig_run_tab(j).insp_type
                  ,v_pickid_orig_run_tab(j).insp_scope)
               ;
            ELSIF  v_pickid_orig_run_tab(j).calendar_year = i_year THEN
               /* nullify this year */
               INSERT INTO structools.insp_plan_mod_run
                  (run_id
                  ,program_id
                  ,pick_id
                  ,maint_zone_nam
                  ,calendar_year
                  ,insp_type
                  ,insp_scope)
               VALUES
                  (i_run_id
                  ,i_program_id
                  ,v_pickid_orig_run_tab(j).pick_id
                  ,v_pickid_orig_run_tab(j).maint_zone_nam
                  ,v_pickid_orig_run_tab(j).calendar_year
                  ,NULL
                  ,NULL);
            ELSE
               INSERT INTO structools.insp_plan_mod_run
                  (run_id
                  ,program_id
                  ,pick_id
                  ,maint_zone_nam
                  ,calendar_year
                  ,insp_type
                  ,insp_scope)
               VALUES
                  (i_run_id
                  ,i_program_id
                  ,v_pickid_orig_run_tab(j).pick_id
                  ,v_pickid_orig_run_tab(j).maint_zone_nam
                  ,v_pickid_orig_run_tab(j).calendar_year
                  ,v_pickid_orig_run_tab(j - 1).insp_type
                  ,v_pickid_orig_run_tab(j - 1).insp_scope)
               ;
            END IF;
         END LOOP;

      	v_pickid := v_pickid_orig_run_arr.NEXT(v_pickid);
      END LOOP;

      /* Enable indexes */
      structools.schema_utils.enable_table_indexes('INSP_PLAN_MOD_RUN');
			
		COMMIT;
            
   EXCEPTION
      WHEN v_prog_excp THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
      WHEN OTHERS THEN
         dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
         dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         RAISE;
         
   END update_insp_plan;

/*****************************************************************/
/* Package initialisation                                                                                               */
/*****************************************************************/

BEGIN

   NULL;
   
END insp_optimiser_cycle;
/
