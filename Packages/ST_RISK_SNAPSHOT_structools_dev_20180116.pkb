CREATE OR REPLACE PACKAGE BODY ST_RISK_SNAPSHOT
AS
 pvg_pkname_vc VARCHAR2 (40) := 'STRUCTOOLSAPP.ST_RISK_SNAPSHOT';
   TYPE reac_cost_tab_typ        IS TABLE OF number INDEX BY VARCHAR2(20);
   v_reac_cost_tab               reac_cost_tab_typ;

   PROCEDURE PR_LOAD_ALL_RISK_TABLES (p_yr_mth_no IN CHAR)
   AS
   
      CURSOR get_reactv_cost_cur
      IS
         SELECT TRIM(mdl_bus_cde) AS mdl_bus_cde
               ,trmnt_reactv_cst_amt
         --FROM structoolsw.treatment_reactive_cost
         FROM structools.st_treatment_reactive_cost
      ;
   BEGIN
      DBMS_OUTPUT.put_line (
         'Starting risk load for financial year month - ' || p_yr_mth_no);
      DBMS_OUTPUT.put_line (
         '      Loading risk tables for poles and its components...  ');
      DBMS_OUTPUT.put_line (
         '      Start time - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         
      /* load reactive cost for all  models to an associativee array, indexed by the model name */
      FOR c IN get_reactv_cost_cur LOOP
         v_reac_cost_tab(c.mdl_bus_cde) := c.trmnt_reactv_cst_amt;
      END LOOP;   
      
      PR_LOAD_POLES_AND_EQP(p_yr_mth_no);
      
      PR_LOAD_CONDUCTORS(p_yr_mth_no);
      
   END PR_LOAD_ALL_RISK_TABLES;      

   PROCEDURE PR_LOAD_POLES_AND_EQP (p_yr_mth_no IN CHAR)
   AS
   
      CURSOR get_reactv_cost_cur
      IS
         SELECT TRIM(mdl_bus_cde) AS mdl_bus_cde
               ,trmnt_reactv_cst_amt
         --FROM structoolsw.treatment_reactive_cost
         FROM structools.st_treatment_reactive_cost
      ;

   BEGIN

      /* load reactive cost of all  models to an associtaive array, indexed by the model name */
      /* This only needs to be done if this procedure is invoked on its own and not as part of PR_LOAD_ALL_RISK_TABLES procedure */
      IF v_reac_cost_tab.COUNT = 0 THEN
         FOR c IN get_reactv_cost_cur LOOP
            v_reac_cost_tab(c.mdl_bus_cde) := c.trmnt_reactv_cst_amt;
--            dbms_output.put_line('v_reac_cost_tab. mdl_bus_cde = '||c.mdl_bus_cde
--                                    ||' cost = '||c.trmnt_reactv_cst_amt);
         END LOOP;   
      END IF;
      
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'PWOD', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'PWOD', 'MAINTAIN');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'PWOD', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'XARMLV', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'XARMLV', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'XARMHV', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'XARMHV', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'STAY', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'STAY', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'INSUL', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'INSUL', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'NWPOLE', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'NWPOLE', 'REPLACE');
      PR_LOAD_RISK_PWOD (p_yr_mth_no); 
      PR_LOAD_RISK_XARMHVI (p_yr_mth_no);
      PR_LOAD_RISK_XARMLVI (p_yr_mth_no);

      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'DOF', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'DOF', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'DSTRPM', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'DSTRPM', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'PTSD', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'PTSD', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'RCLBS', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'RCLBS', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'VREG', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'VREG', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'SECT', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'SECT', 'REPLACE');  
      

   END PR_LOAD_POLES_AND_EQP;
   
   PROCEDURE PR_LOAD_CONDUCTORS (p_yr_mth_no IN CHAR)
   AS
   
      CURSOR get_reactv_cost_cur
      IS
         SELECT TRIM(mdl_bus_cde) AS mdl_bus_cde
               ,trmnt_reactv_cst_amt
         --FROM structoolsw.treatment_reactive_cost
         FROM structools.st_treatment_reactive_cost
      ;

   BEGIN

      /* load reactive cost of all  models to an associtaive array, indexed by the model name */
      /* This only needs to be done if this procedure is invoked on its own and not as part of PR_LOAD_ALL_RISK_TABLES procedure */
      IF v_reac_cost_tab.COUNT = 0 THEN
         FOR c IN get_reactv_cost_cur LOOP
            v_reac_cost_tab(c.mdl_bus_cde) := c.trmnt_reactv_cst_amt;
--            dbms_output.put_line('v_reac_cost_tab. mdl_bus_cde = '||c.mdl_bus_cde
--                                    ||' cost = '||c.trmnt_reactv_cst_amt);
         END LOOP;   
      END IF;
      
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'CONDFAIL', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'CONDFAIL', 'MAINTAIN');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'CONDFAIL', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'CONDVCA', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'CONDVCA', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'CONDCA', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'CONDCA', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'CONDGC', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'CONDGC', 'REPLACE');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'CONDEARTH', 'DONOTHING');
      PR_LOAD_RISK_MODEL (p_yr_mth_no, 'CONDEARTH', 'REPLACE');
      PR_LOAD_RISK_BAY ( p_yr_mth_no); 


   END PR_LOAD_CONDUCTORS;

   PROCEDURE PR_LOAD_RISK_MODEL (p_yr_mth_no    IN CHAR,
                                 p_risk_model   IN VARCHAR2,
                                 p_action       IN VARCHAR2)
   AS

      CURSOR get_reactv_cost_cur
      IS
         SELECT TRIM(mdl_bus_cde) AS mdl_bus_cde
               ,trmnt_reactv_cst_amt
         --FROM structoolsw.treatment_reactive_cost
         FROM structools.st_treatment_reactive_cost
      ;


      CURSOR get_nrmt_risk_cur (
         c_yr_mth_no    IN CHAR,
         c_risk_model   IN VARCHAR2,
         c_action       IN VARCHAR2)
      IS
         SELECT * FROM
         (
         SELECT (CASE
                    WHEN instrc (ed.plnt_no,
                                 ' ',
                                 1,
                                 1) = 0
                    THEN
                       SUBSTR (ed.plnt_no, -1 * (LENGTH (ed.plnt_no) - 1))
                    ELSE
                       SUBSTR (ed.plnt_no,
                               2,
                               instrc (ed.plnt_no,
                                       ' ',
                                       1,
                                       1) - 2)
                 END)
                   pick_id,
                ED.DFIS_LYR_TYP,
                -- ed.equip_no,
                nrdmf.hzrd_fn_0_1 FAIL_PROB_Y0,
                nrdmf.hzrd_fn_1_2 FAIL_PROB_Y1,
                nrdmf.hzrd_fn_2_3 FAIL_PROB_Y2,
                nrdmf.hzrd_fn_3_4 FAIL_PROB_Y3,
                nrdmf.hzrd_fn_4_5 FAIL_PROB_Y4,
                nrdmf.hzrd_fn_5_6 FAIL_PROB_Y5,
                  nrdmf.hzrd_fn_5_6
                + (nrdmf.hzrd_fn_10_11 - nrdmf.hzrd_fn_5_6) * 1 / 5
                   FAIL_PROB_Y6,
                  nrdmf.hzrd_fn_5_6
                + (nrdmf.hzrd_fn_10_11 - nrdmf.hzrd_fn_5_6) * 2 / 5
                   FAIL_PROB_Y7,
                  nrdmf.hzrd_fn_5_6
                + (nrdmf.hzrd_fn_10_11 - nrdmf.hzrd_fn_5_6) * 3 / 5
                   FAIL_PROB_Y8,
                  nrdmf.hzrd_fn_5_6
                + (nrdmf.hzrd_fn_10_11 - nrdmf.hzrd_fn_5_6) * 4 / 5
                   FAIL_PROB_Y9,
                nrdmf.hzrd_fn_10_11 FAIL_PROB_Y10,
                  nrdmf.hzrd_fn_10_11
                + (nrdmf.hzrd_fn_20_21 - nrdmf.hzrd_fn_10_11) * 1 / 10
                   FAIL_PROB_Y11,
                  nrdmf.hzrd_fn_10_11
                + (nrdmf.hzrd_fn_20_21 - nrdmf.hzrd_fn_10_11) * 2 / 10
                   FAIL_PROB_Y12,
                  nrdmf.hzrd_fn_10_11
                + (nrdmf.hzrd_fn_20_21 - nrdmf.hzrd_fn_10_11) * 3 / 10
                   FAIL_PROB_Y13,
                  nrdmf.hzrd_fn_10_11
                + (nrdmf.hzrd_fn_20_21 - nrdmf.hzrd_fn_10_11) * 4 / 10
                   FAIL_PROB_Y14,
                  nrdmf.hzrd_fn_10_11
                + (nrdmf.hzrd_fn_20_21 - nrdmf.hzrd_fn_10_11) * 5 / 10
                   FAIL_PROB_Y15,
                  nrdmf.hzrd_fn_10_11
                + (nrdmf.hzrd_fn_20_21 - nrdmf.hzrd_fn_10_11) * 6 / 10
                   FAIL_PROB_Y16,
                  nrdmf.hzrd_fn_10_11
                + (nrdmf.hzrd_fn_20_21 - nrdmf.hzrd_fn_10_11) * 7 / 10
                   FAIL_PROB_Y17,
                  nrdmf.hzrd_fn_10_11
                + (nrdmf.hzrd_fn_20_21 - nrdmf.hzrd_fn_10_11) * 8 / 10
                   FAIL_PROB_Y18,
                  nrdmf.hzrd_fn_10_11
                + (nrdmf.hzrd_fn_20_21 - nrdmf.hzrd_fn_10_11) * 9 / 10
                   FAIL_PROB_Y19,
                nrdmf.hzrd_fn_20_21 FAIL_PROB_Y20,
                  nrdmf.hzrd_fn_20_21
                + (nrdmf.hzrd_fn_30_31 - nrdmf.hzrd_fn_20_21) * 1 / 10
                   FAIL_PROB_Y21,
                  nrdmf.hzrd_fn_20_21
                + (nrdmf.hzrd_fn_30_31 - nrdmf.hzrd_fn_20_21) * 2 / 10
                   FAIL_PROB_Y22,
                  nrdmf.hzrd_fn_20_21
                + (nrdmf.hzrd_fn_30_31 - nrdmf.hzrd_fn_20_21) * 3 / 10
                   FAIL_PROB_Y23,
                  nrdmf.hzrd_fn_20_21
                + (nrdmf.hzrd_fn_30_31 - nrdmf.hzrd_fn_20_21) * 4 / 10
                   FAIL_PROB_Y24,
                  nrdmf.hzrd_fn_20_21
                + (nrdmf.hzrd_fn_30_31 - nrdmf.hzrd_fn_20_21) * 5 / 10
                   FAIL_PROB_Y25,
                  nrdmf.hzrd_fn_20_21
                + (nrdmf.hzrd_fn_30_31 - nrdmf.hzrd_fn_20_21) * 6 / 10
                   FAIL_PROB_Y26,
                  nrdmf.hzrd_fn_20_21
                + (nrdmf.hzrd_fn_30_31 - nrdmf.hzrd_fn_20_21) * 7 / 10
                   FAIL_PROB_Y27,
                  nrdmf.hzrd_fn_20_21
                + (nrdmf.hzrd_fn_30_31 - nrdmf.hzrd_fn_20_21) * 8 / 10
                   FAIL_PROB_Y28,
                  nrdmf.hzrd_fn_20_21
                + (nrdmf.hzrd_fn_30_31 - nrdmf.hzrd_fn_20_21) * 9 / 10
                   FAIL_PROB_Y29,
                nrdmf.hzrd_fn_30_31 FAIL_PROB_Y30,
                  nrdmf.hzrd_fn_30_31
                + (nrdmf.hzrd_fn_40_41 - nrdmf.hzrd_fn_30_31) * 1 / 10
                   FAIL_PROB_Y31,
                  nrdmf.hzrd_fn_30_31
                + (nrdmf.hzrd_fn_40_41 - nrdmf.hzrd_fn_30_31) * 2 / 10
                   FAIL_PROB_Y32,
                  nrdmf.hzrd_fn_30_31
                + (nrdmf.hzrd_fn_40_41 - nrdmf.hzrd_fn_30_31) * 3 / 10
                   FAIL_PROB_Y33,
                  nrdmf.hzrd_fn_30_31
                + (nrdmf.hzrd_fn_40_41 - nrdmf.hzrd_fn_30_31) * 4 / 10
                   FAIL_PROB_Y34,
                  nrdmf.hzrd_fn_30_31
                + (nrdmf.hzrd_fn_40_41 - nrdmf.hzrd_fn_30_31) * 5 / 10
                   FAIL_PROB_Y35,
                  nrdmf.hzrd_fn_30_31
                + (nrdmf.hzrd_fn_40_41 - nrdmf.hzrd_fn_30_31) * 6 / 10
                   FAIL_PROB_Y36,
                  nrdmf.hzrd_fn_30_31
                + (nrdmf.hzrd_fn_40_41 - nrdmf.hzrd_fn_30_31) * 7 / 10
                   FAIL_PROB_Y37,
                  nrdmf.hzrd_fn_30_31
                + (nrdmf.hzrd_fn_40_41 - nrdmf.hzrd_fn_30_31) * 8 / 10
                   FAIL_PROB_Y38,
                  nrdmf.hzrd_fn_30_31
                + (nrdmf.hzrd_fn_40_41 - nrdmf.hzrd_fn_30_31) * 9 / 10
                   FAIL_PROB_Y39,
                nrdmf.hzrd_fn_40_41 FAIL_PROB_Y40,
                  nrdmf.hzrd_fn_40_41
                + (nrdmf.hzrd_fn_50_51 - nrdmf.hzrd_fn_40_41) * 1 / 10
                   FAIL_PROB_Y41,
                  nrdmf.hzrd_fn_40_41
                + (nrdmf.hzrd_fn_50_51 - nrdmf.hzrd_fn_40_41) * 2 / 10
                   FAIL_PROB_Y42,
                  nrdmf.hzrd_fn_40_41
                + (nrdmf.hzrd_fn_50_51 - nrdmf.hzrd_fn_40_41) * 3 / 10
                   FAIL_PROB_Y43,
                  nrdmf.hzrd_fn_40_41
                + (nrdmf.hzrd_fn_50_51 - nrdmf.hzrd_fn_40_41) * 4 / 10
                   FAIL_PROB_Y44,
                  nrdmf.hzrd_fn_40_41
                + (nrdmf.hzrd_fn_50_51 - nrdmf.hzrd_fn_40_41) * 5 / 10
                   FAIL_PROB_Y45,
                  nrdmf.hzrd_fn_40_41
                + (nrdmf.hzrd_fn_50_51 - nrdmf.hzrd_fn_40_41) * 6 / 10
                   FAIL_PROB_Y46,
                  nrdmf.hzrd_fn_40_41
                + (nrdmf.hzrd_fn_50_51 - nrdmf.hzrd_fn_40_41) * 7 / 10
                   FAIL_PROB_Y47,
                  nrdmf.hzrd_fn_40_41
                + (nrdmf.hzrd_fn_50_51 - nrdmf.hzrd_fn_40_41) * 8 / 10
                   FAIL_PROB_Y48,
                  nrdmf.hzrd_fn_40_41
                + (nrdmf.hzrd_fn_50_51 - nrdmf.hzrd_fn_40_41) * 9 / 10
                   FAIL_PROB_Y49,
                nrdmf.hzrd_fn_50_51 FAIL_PROB_Y50,
                nrdmf.elec_shck_cnsqnc_amt,
                nrdmf.env_cnsqnc_amt,
                nrdmf.fire_cnsqnc_amt,
                nrdmf.phys_impct_cnsqnc_amt,
                --nrdmf.prob_env_incd,
                nrdmf.reliability_cnsqnc_amt,
                nrdmf.wrk_frce_cnsqnc_amt,
                (  FIRE_CNSQNC_AMT
                 + elec_shck_cnsqnc_amt
                 + phys_impct_cnsqnc_amt
                 + reliability_cnsqnc_amt
                 + wrk_frce_cnsqnc_amt
                 + env_cnsqnc_amt)
                   AS cnsqnc_amt,
                (  nrdmf.prob_fire_cnsqnc_evnt_1
                 + nrdmf.prob_fire_cnsqnc_evnt_2)
                   FIRE_INCD,
                (  nrdmf.prob_elec_shck_cnsqnc_evnt_1
                 + nrdmf.prob_elec_shck_cnsqnc_evnt_2
                 + nrdmf.prob_elec_shck_cnsqnc_evnt_3
                 + nrdmf.prob_elec_shck_cnsqnc_evnt_4)
                   ELEC_SHOCK_INCD,
                (  nrdmf.prob_reliability_cnsqnc_evnt_2
                 + nrdmf.prob_reliability_cnsqnc_evnt_3)
                   LRGE_RLBLTY_INCD,
                nrdmf.prob_fire_cnsqnc_evnt_1 FIRE_FATALITY_INCD,
                nrdmf.prob_elec_shck_cnsqnc_evnt_1 ELEC_SHOCK_FATALITY_INCD,
                0                                  trmnt_reactv_cst_amt,  --placeholder in rec type; populated in the code further down  
               lag(ed.pick_id,1,0) OVER (ORDER BY pick_id) AS prev_pick_id           --purely to easily identify duplicate pickids in the result list
         FROM structoolsw.mthly_equip_risk_dm_fact nrdmf                             --due to data quality issues
                JOIN structoolsw.nrm_asset_type_model_dim atm
                   ON atm.nrm_asset_typ_mdl_sk = nrdmf.nrm_asset_typ_mdl_sk
                JOIN structoolsw.treatment_dim td
                   ON td.trmnt_sk = nrdmf.trmnt_sk
                JOIN structoolsw.equipment_nvgtn_dim en
                   ON en.equip_sk = nrdmf.equip_sk
                JOIN structoolsw.equipment_dim ed
                   ON ed.equip_nvgtn_sk = en.equip_nvgtn_sk
          WHERE     nrdmf.yr_mth_no = c_yr_mth_no --(select max(yr_mth_no) from NDW.MONTH_DIM where NRM_MTH_CMPL_CDE = 'Y')
                AND td.trmnt_nam = c_action
                AND TRIM (atm.mdl_bus_cde) = c_risk_model
          )
          WHERE prev_pick_id != 'S'||pick_id                                        --purely to easily identify duplicate pickids in the result list
                                                                                    --due to data quality issues
                --AND ed.pick_id IN ('B1459363','B721020')
                ;
                --AND ed.pick_id = 'B305847';

      -- AND ed.equip_no in ('000010390153', '000001137671', '000001696986', '000020132622', '000001824246', '000021807039','000020082619', '000020246314', '000001791921', '000001792115',
      -- '000021924570', '000020504143');

      v_risk_fail_rt               structools.ST_RISK_POLE_NOACTION_FAIL%ROWTYPE;
      v_risk_npv_rt                structools.ST_RISK_POLE_NOACTION_NPV15%ROWTYPE;
      v_forecast_x_mean   CONSTANT FLOAT := 46.5; /*the average of (44, 45,46,47,48,49)*/
      v_forecast_y_mean            FLOAT;
      v_forecast_const_a           FLOAT;
      v_forecast_const_b           FLOAT;
      v_record_cnt                 NUMBER := 0;
      v_cmmtd_rec_cnt              NUMBER := 0;
      v_model_type                 VARCHAR2 (30);

   BEGIN
      DBMS_OUTPUT.put_line (
            'Risk MODEL - '
         || p_risk_model
         || '_'
         || p_action
         || ' Start TIME - '
         || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));

      DBMS_OUTPUT.put_line ('Truncating DATA FOR MODEL');

      IF p_risk_model = 'PWOD' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_POLE_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_POLE_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_POLE_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_POLE_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_POLE_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_POLE_NOACTION_NPV15');
         v_model_type := 'POLE';
      ELSIF p_risk_model = 'PWOD' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_POLE_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_POLE_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_POLE_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_POLE_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_POLE_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_POLE_REPLACE_NPV15');
      ELSIF p_risk_model = 'PWOD' AND p_action = 'MAINTAIN' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_POLE_REINF');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_POLE_REINF_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_POLE_REINF_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_POLE_REINF');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_POLE_REINF_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_POLE_REINF_NPV15');
      ELSIF p_risk_model = 'XARMLV' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMLV_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMLV_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMLV_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMLV_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMLV_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMLV_NOACTION_NPV15');
         v_model_type := 'XARMLV';
      ELSIF p_risk_model = 'XARMLV' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMLV_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMLV_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMLV_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMLV_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMLV_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMLV_REPLACE_NPV15');
      ELSIF p_risk_model = 'XARMHV' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMHV_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMHV_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMHV_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMHV_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMHV_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMHV_NOACTION_NPV15');
         v_model_type := 'XARMHV';
      ELSIF p_risk_model = 'XARMHV' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMHV_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMHV_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMHV_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMHV_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMHV_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMHV_REPLACE_NPV15');
      ELSIF p_risk_model = 'STAY' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_STAY_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_STAY_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_STAY_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_STAY_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_STAY_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_STAY_NOACTION_NPV15');
         v_model_type := 'STAY';
      ELSIF p_risk_model = 'STAY' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_STAY_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_STAY_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_STAY_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_STAY_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_STAY_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_STAY_REPLACE_NPV15');
      ELSIF p_risk_model = 'INSUL' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_INSUL_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_INSUL_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_INSUL_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_INSUL_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_INSUL_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_INSUL_NOACTION_NPV15');
         v_model_type := 'INSUL';
      ELSIF p_risk_model = 'INSUL' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_INSUL_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_INSUL_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_INSUL_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_INSUL_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_INSUL_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_INSUL_REPLACE_NPV15');
      ELSIF p_risk_model = 'NWPOLE' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_NWPOLE_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_NWPOLE_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_NWPOLE_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_NWPOLE_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_NWPOLE_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_NWPOLE_NOACTION_NPV15');
         v_model_type := 'NWPOLE';
      ELSIF p_risk_model = 'NWPOLE' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_NWPOLE_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_NWPOLE_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_NWPOLE_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_NWPOLE_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_NWPOLE_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_NWPOLE_REPLACE_NPV15');
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_COND_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_COND_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_COND_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_COND_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_COND_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_COND_NOACTION_NPV15');
         v_model_type := 'COND';
         --DBMS_OUTPUT.put_line ('Truncated ST_RISK_COND_NOACTION, ST_RISK_COND_NOACTION_FAIL, ST_RISK_COND_NOACTION_NPV15');
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_COND_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_COND_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_COND_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_COND_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_COND_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_COND_REPLACE_NPV15');
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'MAINTAIN' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_COND_REPAIR');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_COND_REPAIR_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_COND_REPAIR_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_COND_REPAIR');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_COND_REPAIR_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_COND_REPAIR_NPV15');
      ELSIF p_risk_model = 'CONDGC' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDGCA_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDGCA_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDGCA_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDGCA_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDGCA_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDGCA_NOACTION_NPV15');
         v_model_type := 'CONDGCA';
      ELSIF p_risk_model = 'CONDGC' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDGCA_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDGCA_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDGCA_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDGCA_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDGCA_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDGCA_REPLACE_NPV15');
      ELSIF p_risk_model = 'CONDCA' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDHCA_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDHCA_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDHCA_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDHCA_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDHCA_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDHCA_NOACTION_NPV15');
         v_model_type := 'CONDHCA';
      ELSIF p_risk_model = 'CONDCA' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDHCA_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDHCA_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDHCA_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDHCA_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDHCA_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDHCA_REPLACE_NPV15');
      ELSIF p_risk_model = 'CONDVCA' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDVCA_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDVCA_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDVCA_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDVCA_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDVCA_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDVCA_NOACTION_NPV15');
         v_model_type := 'CONDVCA';
      ELSIF p_risk_model = 'CONDVCA' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDVCA_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDVCA_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDVCA_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDVCA_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDVCA_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDVCA_REPLACE_NPV15');
      ELSIF p_risk_model = 'CONDEARTH' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDEAR_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDEAR_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDEAR_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDEAR_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDEAR_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDEAR_NOACTION_NPV15');
         v_model_type := 'CONDEAR';
      ELSIF p_risk_model = 'CONDEARTH' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDEAR_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDEAR_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_CONDEAR_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDEAR_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDEAR_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_CONDEAR_REPLACE_NPV15');
      ELSIF p_risk_model = 'DOF' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_DOF_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_DOF_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_DOF_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_DOF_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_DOF_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_DOF_NOACTION_NPV15');
         v_model_type := 'DOF';
      ELSIF p_risk_model = 'DOF' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_DOF_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_DOF_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_DOF_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_DOF_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_DOF_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_DOF_REPLACE_NPV15');
      ELSIF p_risk_model = 'DSTRPM' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_DSTR_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_DSTR_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_DSTR_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_DSTR_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_DSTR_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_DSTR_NOACTION_NPV15');
         v_model_type := 'DSTR';
      ELSIF p_risk_model = 'DSTRPM' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_DSTR_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_DSTR_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_DSTR_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_DSTR_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_DSTR_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_DSTR_REPLACE_NPV15');
      ELSIF p_risk_model = 'PTSD' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_PTSD_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_PTSD_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_PTSD_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_PTSD_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_PTSD_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_PTSD_NOACTION_NPV15');
         v_model_type := 'PTSD';
      ELSIF p_risk_model = 'PTSD' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_PTSD_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_PTSD_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_PTSD_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_PTSD_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_PTSD_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_PTSD_REPLACE_NPV15');
      ELSIF p_risk_model = 'RCLBS' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_RECL_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_RECL_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_RECL_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_RECL_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_RECL_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_RECL_NOACTION_NPV15');
         v_model_type := 'RECL';
      ELSIF p_risk_model = 'RCLBS' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_RECL_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_RECL_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_RECL_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_RECL_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_RECL_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_RECL_REPLACE_NPV15');
      ELSIF p_risk_model = 'VREG' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_RGTR_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_RGTR_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_RGTR_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_RGTR_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_RGTR_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_RGTR_NOACTION_NPV15');
         v_model_type := 'RGTR';
      ELSIF p_risk_model = 'VREG' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_RGTR_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_RGTR_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_RGTR_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_RGTR_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_RGTR_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_RGTR_REPLACE_NPV15');
      ELSIF p_risk_model = 'SECT' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_SECT_NOACTION');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_SECT_NOACTION_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_SECT_NOACTION_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_SECT_NOACTION');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_SECT_NOACTION_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_SECT_NOACTION_NPV15');
         v_model_type := 'SECT';
      ELSIF p_risk_model = 'SECT' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_SECT_REPLACE');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_SECT_REPLACE_FAIL');
         structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_SECT_REPLACE_NPV15');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_SECT_REPLACE');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_SECT_REPLACE_FAIL');
         structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_SECT_REPLACE_NPV15');
      END IF;

      IF p_action = 'DONOTHING'
      THEN
         DELETE FROM structools.ST_RISK_FRACTIONS frac
               WHERE FRAC.MODEL_NAME = v_model_type;
      END IF;

      COMMIT;

      dbms_output.put_line('starting actual load - '||TO_CHAR(SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));

      /* load reactive cost of all  models to an associtaive array, indexed by the model name */
      /* This only needs to be done if this procedure is invoked on its own and not as part of PR_LOAD_ALL_RISK_TABLES procedure */
      IF v_reac_cost_tab.COUNT = 0 THEN
         FOR c IN get_reactv_cost_cur LOOP
            v_reac_cost_tab(c.mdl_bus_cde) := c.trmnt_reactv_cst_amt;
--            dbms_output.put_line('v_reac_cost_tab. mdl_bus_cde = '||c.mdl_bus_cde
--                                    ||' cost = '||c.trmnt_reactv_cst_amt);
         END LOOP;   
      END IF;
      
--      dbms_output.put_line('get_nrmt_risk_cur loop. p_yr_mth_no = '||p_yr_mth_no||' p_risk_model = '||p_risk_model||' p_action = '||p_action);
      FOR v_get_nrmt_risk_rec
         IN get_nrmt_risk_cur (p_yr_mth_no, p_risk_model, p_action)
      LOOP
         BEGIN
            v_get_nrmt_risk_rec.trmnt_reactv_cst_amt := v_reac_cost_tab(p_risk_model);        -- set reactive cost for the model in the record
            v_get_nrmt_risk_rec.cnsqnc_amt := v_get_nrmt_risk_rec.cnsqnc_amt + v_reac_cost_tab(p_risk_model); -- add reactive cost for the model to the total consqnce amt
--            dbms_output.put_line('consequence amt = '||v_get_nrmt_risk_rec.cnsqnc_amt);
            v_risk_fail_rt.PICK_ID := v_get_nrmt_risk_rec.PICK_ID;
            v_risk_fail_rt.FAIL_PROB_Y0 := v_get_nrmt_risk_rec.FAIL_PROB_Y0;
            v_risk_fail_rt.FAIL_PROB_Y1 := v_get_nrmt_risk_rec.FAIL_PROB_Y1;
            v_risk_fail_rt.FAIL_PROB_Y2 := v_get_nrmt_risk_rec.FAIL_PROB_Y2;
            v_risk_fail_rt.FAIL_PROB_Y3 := v_get_nrmt_risk_rec.FAIL_PROB_Y3;
            v_risk_fail_rt.FAIL_PROB_Y4 := v_get_nrmt_risk_rec.FAIL_PROB_Y4;
            v_risk_fail_rt.FAIL_PROB_Y5 := v_get_nrmt_risk_rec.FAIL_PROB_Y5;
            v_risk_fail_rt.FAIL_PROB_Y6 := v_get_nrmt_risk_rec.FAIL_PROB_Y6;
            v_risk_fail_rt.FAIL_PROB_Y7 := v_get_nrmt_risk_rec.FAIL_PROB_Y7;
            v_risk_fail_rt.FAIL_PROB_Y8 := v_get_nrmt_risk_rec.FAIL_PROB_Y8;
            v_risk_fail_rt.FAIL_PROB_Y9 := v_get_nrmt_risk_rec.FAIL_PROB_Y9;
            v_risk_fail_rt.FAIL_PROB_Y10 := v_get_nrmt_risk_rec.FAIL_PROB_Y10;
            v_risk_fail_rt.FAIL_PROB_Y11 := v_get_nrmt_risk_rec.FAIL_PROB_Y11;
            v_risk_fail_rt.FAIL_PROB_Y12 := v_get_nrmt_risk_rec.FAIL_PROB_Y12;
            v_risk_fail_rt.FAIL_PROB_Y13 := v_get_nrmt_risk_rec.FAIL_PROB_Y13;
            v_risk_fail_rt.FAIL_PROB_Y14 := v_get_nrmt_risk_rec.FAIL_PROB_Y14;
            v_risk_fail_rt.FAIL_PROB_Y15 := v_get_nrmt_risk_rec.FAIL_PROB_Y15;
            v_risk_fail_rt.FAIL_PROB_Y16 := v_get_nrmt_risk_rec.FAIL_PROB_Y16;
            v_risk_fail_rt.FAIL_PROB_Y17 := v_get_nrmt_risk_rec.FAIL_PROB_Y17;
            v_risk_fail_rt.FAIL_PROB_Y18 := v_get_nrmt_risk_rec.FAIL_PROB_Y18;
            v_risk_fail_rt.FAIL_PROB_Y19 := v_get_nrmt_risk_rec.FAIL_PROB_Y19;
            v_risk_fail_rt.FAIL_PROB_Y20 := v_get_nrmt_risk_rec.FAIL_PROB_Y20;
            v_risk_fail_rt.FAIL_PROB_Y21 := v_get_nrmt_risk_rec.FAIL_PROB_Y21;
            v_risk_fail_rt.FAIL_PROB_Y22 := v_get_nrmt_risk_rec.FAIL_PROB_Y22;
            v_risk_fail_rt.FAIL_PROB_Y23 := v_get_nrmt_risk_rec.FAIL_PROB_Y23;
            v_risk_fail_rt.FAIL_PROB_Y24 := v_get_nrmt_risk_rec.FAIL_PROB_Y24;
            v_risk_fail_rt.FAIL_PROB_Y25 := v_get_nrmt_risk_rec.FAIL_PROB_Y25;
            v_risk_fail_rt.FAIL_PROB_Y26 := v_get_nrmt_risk_rec.FAIL_PROB_Y26;
            v_risk_fail_rt.FAIL_PROB_Y27 := v_get_nrmt_risk_rec.FAIL_PROB_Y27;
            v_risk_fail_rt.FAIL_PROB_Y28 := v_get_nrmt_risk_rec.FAIL_PROB_Y28;
            v_risk_fail_rt.FAIL_PROB_Y29 := v_get_nrmt_risk_rec.FAIL_PROB_Y29;
            v_risk_fail_rt.FAIL_PROB_Y30 := v_get_nrmt_risk_rec.FAIL_PROB_Y30;
            v_risk_fail_rt.FAIL_PROB_Y31 := v_get_nrmt_risk_rec.FAIL_PROB_Y31;
            v_risk_fail_rt.FAIL_PROB_Y32 := v_get_nrmt_risk_rec.FAIL_PROB_Y32;
            v_risk_fail_rt.FAIL_PROB_Y33 := v_get_nrmt_risk_rec.FAIL_PROB_Y33;
            v_risk_fail_rt.FAIL_PROB_Y34 := v_get_nrmt_risk_rec.FAIL_PROB_Y34;
            v_risk_fail_rt.FAIL_PROB_Y35 := v_get_nrmt_risk_rec.FAIL_PROB_Y35;
            v_risk_fail_rt.FAIL_PROB_Y36 := v_get_nrmt_risk_rec.FAIL_PROB_Y36;
            v_risk_fail_rt.FAIL_PROB_Y37 := v_get_nrmt_risk_rec.FAIL_PROB_Y37;
            v_risk_fail_rt.FAIL_PROB_Y38 := v_get_nrmt_risk_rec.FAIL_PROB_Y38;
            v_risk_fail_rt.FAIL_PROB_Y39 := v_get_nrmt_risk_rec.FAIL_PROB_Y39;
            v_risk_fail_rt.FAIL_PROB_Y40 := v_get_nrmt_risk_rec.FAIL_PROB_Y40;
            v_risk_fail_rt.FAIL_PROB_Y41 := v_get_nrmt_risk_rec.FAIL_PROB_Y41;
            v_risk_fail_rt.FAIL_PROB_Y42 := v_get_nrmt_risk_rec.FAIL_PROB_Y42;
            v_risk_fail_rt.FAIL_PROB_Y43 := v_get_nrmt_risk_rec.FAIL_PROB_Y43;
            v_risk_fail_rt.FAIL_PROB_Y44 := v_get_nrmt_risk_rec.FAIL_PROB_Y44;
            v_risk_fail_rt.FAIL_PROB_Y45 := v_get_nrmt_risk_rec.FAIL_PROB_Y45;
            v_risk_fail_rt.FAIL_PROB_Y46 := v_get_nrmt_risk_rec.FAIL_PROB_Y46;
            v_risk_fail_rt.FAIL_PROB_Y47 := v_get_nrmt_risk_rec.FAIL_PROB_Y47;
            v_risk_fail_rt.FAIL_PROB_Y48 := v_get_nrmt_risk_rec.FAIL_PROB_Y48;
            v_risk_fail_rt.FAIL_PROB_Y49 := v_get_nrmt_risk_rec.FAIL_PROB_Y49;
            v_risk_fail_rt.FAIL_PROB_Y50 := v_get_nrmt_risk_rec.FAIL_PROB_Y50;
            v_risk_fail_rt.FIRE_INCD := v_get_nrmt_risk_rec.FIRE_INCD;
            v_risk_fail_rt.ELEC_SHOCK_INCD :=
            v_get_nrmt_risk_rec.ELEC_SHOCK_INCD;
            v_risk_fail_rt.LRGE_RLBLTY_INCD         := v_get_nrmt_risk_rec.LRGE_RLBLTY_INCD;
            --v_risk_fail_rt.PROB_ENV_INCD = v_get_nrmt_risk_rec.PROB_ENV_INCD;
            v_risk_fail_rt.FIRE_FATALITY_INCD       := v_get_nrmt_risk_rec.FIRE_FATALITY_INCD;
            v_risk_fail_rt.ELEC_SHOCK_FATALITY_INCD := v_get_nrmt_risk_rec.ELEC_SHOCK_FATALITY_INCD;

            pr_insert_risk_fail (v_risk_fail_rt, p_risk_model, p_action);

            pr_insert_risk (v_risk_fail_rt,
                            p_risk_model,
                            p_action,
                            v_get_nrmt_risk_rec.cnsqnc_amt);



            /* NPV Calculation */

            v_forecast_y_mean :=
                 (  v_get_nrmt_risk_rec.FAIL_PROB_Y44
                  + v_get_nrmt_risk_rec.FAIL_PROB_Y45
                  + v_get_nrmt_risk_rec.FAIL_PROB_Y46
                  + v_get_nrmt_risk_rec.FAIL_PROB_Y47
                  + v_get_nrmt_risk_rec.FAIL_PROB_Y48
                  + v_get_nrmt_risk_rec.FAIL_PROB_Y49)
               / 6;

            v_forecast_const_b :=
                 (    (44 - v_forecast_x_mean)
                    * (v_get_nrmt_risk_rec.FAIL_PROB_Y44 - v_forecast_y_mean)
                  +   (45 - v_forecast_x_mean)
                    * (v_get_nrmt_risk_rec.FAIL_PROB_Y45 - v_forecast_y_mean)
                  +   (46 - v_forecast_x_mean)
                    * (v_get_nrmt_risk_rec.FAIL_PROB_Y46 - v_forecast_y_mean)
                  +   (47 - v_forecast_x_mean)
                    * (v_get_nrmt_risk_rec.FAIL_PROB_Y47 - v_forecast_y_mean)
                  +   (48 - v_forecast_x_mean)
                    * (v_get_nrmt_risk_rec.FAIL_PROB_Y48 - v_forecast_y_mean)
                  +   (49 - v_forecast_x_mean)
                    * (v_get_nrmt_risk_rec.FAIL_PROB_Y49 - v_forecast_y_mean))
               / 17.5;

            v_forecast_const_a :=
               LEAST(v_forecast_y_mean - v_forecast_const_b * v_forecast_x_mean, 1);

            v_risk_npv_rt.PICK_ID := v_get_nrmt_risk_rec.PICK_ID;
            v_risk_npv_rt.RISK_INDX_VAL_Y0 :=
                 fn_get_npv15_val (0,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y1 :=
                 fn_get_npv15_val (1,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y2 :=
                 fn_get_npv15_val (2,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y3 :=
                 fn_get_npv15_val (3,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y4 :=
                 fn_get_npv15_val (4,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y5 :=
                 fn_get_npv15_val (5,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y6 :=
                 fn_get_npv15_val (6,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y7 :=
                 fn_get_npv15_val (7,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y8 :=
                 fn_get_npv15_val (8,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y9 :=
                 fn_get_npv15_val (9,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y10 :=
                 fn_get_npv15_val (10,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y11 :=
                 fn_get_npv15_val (11,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y12 :=
                 fn_get_npv15_val (12,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y13 :=
                 fn_get_npv15_val (13,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y14 :=
                 fn_get_npv15_val (14,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y15 :=
                 fn_get_npv15_val (15,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y16 :=
                 fn_get_npv15_val (16,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y17 :=
                 fn_get_npv15_val (17,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y18 :=
                 fn_get_npv15_val (18,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y19 :=
                 fn_get_npv15_val (19,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y20 :=
                 fn_get_npv15_val (20,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y21 :=
                 fn_get_npv15_val (21,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y22 :=
                 fn_get_npv15_val (22,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y23 :=
                 fn_get_npv15_val (23,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y24 :=
                 fn_get_npv15_val (24,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y25 :=
                 fn_get_npv15_val (25,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y26 :=
                 fn_get_npv15_val (26,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y27 :=
                 fn_get_npv15_val (27,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y28 :=
                 fn_get_npv15_val (28,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y29 :=
                 fn_get_npv15_val (29,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y30 :=
                 fn_get_npv15_val (30,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y31 :=
                 fn_get_npv15_val (31,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y32 :=
                 fn_get_npv15_val (32,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y33 :=
                 fn_get_npv15_val (33,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y34 :=
                 fn_get_npv15_val (34,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y35 :=
                 fn_get_npv15_val (35,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y36 :=
                 fn_get_npv15_val (36,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y37 :=
                 fn_get_npv15_val (37,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y38 :=
                 fn_get_npv15_val (38,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y39 :=
                 fn_get_npv15_val (39,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y40 :=
                 fn_get_npv15_val (40,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y41 :=
                 fn_get_npv15_val (41,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y42 :=
                 fn_get_npv15_val (42,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y43 :=
                 fn_get_npv15_val (43,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y44 :=
                 fn_get_npv15_val (44,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y45 :=
                 fn_get_npv15_val (45,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y46 :=
                 fn_get_npv15_val (46,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y47 :=
                 fn_get_npv15_val (47,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y48 :=
                 fn_get_npv15_val (48,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y49 :=
                 fn_get_npv15_val (49,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RISK_INDX_VAL_Y50 :=
                 fn_get_npv15_val (50,
                                   v_risk_fail_rt,
                                   v_forecast_const_a,
                                   v_forecast_const_b)
               * v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.FIRE_FRAC :=
                 v_get_nrmt_risk_rec.fire_cnsqnc_amt
               / v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.ESHOCK_FRAC :=
                 v_get_nrmt_risk_rec.elec_shck_cnsqnc_amt
               / v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.WFORCE_FRAC :=
                 v_get_nrmt_risk_rec.wrk_frce_cnsqnc_amt
               / v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.RELIABILITY_FRAC :=
                 v_get_nrmt_risk_rec.reliability_cnsqnc_amt
               / v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.PHYSIMP_FRAC :=
                 v_get_nrmt_risk_rec.phys_impct_cnsqnc_amt
               / v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.ENV_FRAC :=
                 v_get_nrmt_risk_rec.env_cnsqnc_amt
               / v_get_nrmt_risk_rec.cnsqnc_amt;
            v_risk_npv_rt.FIN_FRAC :=
                 v_get_nrmt_risk_rec.trmnt_reactv_cst_amt
               / v_get_nrmt_risk_rec.cnsqnc_amt;

            /*  DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y0 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y0);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y1 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y1);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y2 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y2);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y3 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y3);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y4 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y4);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y5 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y5);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y6 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y6);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y7 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y7);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y8 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y8);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y9 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y9);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y10 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y10);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y11 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y11);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y12 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y12);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y13 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y13);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y14 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y14);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y15 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y15);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y16 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y16);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y17 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y17);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y18 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y18);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y19 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y19);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y20 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y20);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y21 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y21);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y22 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y22);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y23 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y23);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y24 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y24);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y25 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y25);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y26 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y26);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y27 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y27);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y28 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y28);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y29 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y29);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y30 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y30);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y31 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y31);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y32 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y32);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y33 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y33);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y34 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y34);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y35 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y35);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y36 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y36);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y37 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y37);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y38 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y38);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y39 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y39);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y40 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y40);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y41 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y41);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y42 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y42);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y43 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y43);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y44 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y44);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y45 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y45);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y46 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y46);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y47 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y47);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y48 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y48);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y49 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y49);
              DBMS_OUTPUT.put_line (
                 'RISK_INDX_VAL_Y50 - ' || v_risk_npv_rt.RISK_INDX_VAL_Y50);
              DBMS_OUTPUT.put_line ('FIRE_FRAC - ' || v_risk_npv_rt.FIRE_FRAC);
              DBMS_OUTPUT.put_line (
                 'ESHOCK_FRAC - ' || v_risk_npv_rt.ESHOCK_FRAC);
              DBMS_OUTPUT.put_line (
                 'WFORCE_FRAC - ' || v_risk_npv_rt.WFORCE_FRAC);
              DBMS_OUTPUT.put_line (
                 'RELIABILITY_FRAC - ' || v_risk_npv_rt.RELIABILITY_FRAC);
              DBMS_OUTPUT.put_line (
                 'PHYSIMP_FRAC - ' || v_risk_npv_rt.PHYSIMP_FRAC);
              DBMS_OUTPUT.put_line ('ENV_FRAC - ' || v_risk_npv_rt.ENV_FRAC); */

            PR_INSERT_RISK_NPV (v_risk_npv_rt, p_risk_model, p_action);

            IF p_action = 'DONOTHING'
            THEN
               INSERT INTO structools.ST_RISK_FRACTIONS
                    VALUES (
                                 v_get_nrmt_risk_rec.dfis_lyr_typ
                              || v_get_nrmt_risk_rec.pick_id,
                              v_model_type,
                              v_get_nrmt_risk_rec.FIRE_INCD,
                              v_get_nrmt_risk_rec.ELEC_SHOCK_INCD,
                              v_get_nrmt_risk_rec.LRGE_RLBLTY_INCD,
                              v_get_nrmt_risk_rec.ELEC_SHOCK_FATALITY_INCD,
                              v_get_nrmt_risk_rec.FIRE_FATALITY_INCD,
                                v_get_nrmt_risk_rec.fire_cnsqnc_amt
                              / v_get_nrmt_risk_rec.cnsqnc_amt,
                                v_get_nrmt_risk_rec.elec_shck_cnsqnc_amt
                              / v_get_nrmt_risk_rec.cnsqnc_amt,
                                v_get_nrmt_risk_rec.wrk_frce_cnsqnc_amt
                              / v_get_nrmt_risk_rec.cnsqnc_amt,
                                v_get_nrmt_risk_rec.reliability_cnsqnc_amt
                              / v_get_nrmt_risk_rec.cnsqnc_amt,
                                v_get_nrmt_risk_rec.phys_impct_cnsqnc_amt
                              / v_get_nrmt_risk_rec.cnsqnc_amt,
                                v_get_nrmt_risk_rec.env_cnsqnc_amt
                              / v_get_nrmt_risk_rec.cnsqnc_amt,
                                v_get_nrmt_risk_rec.trmnt_reactv_cst_amt
                              / v_get_nrmt_risk_rec.cnsqnc_amt);
            END IF;


            v_record_cnt := v_record_cnt + 1;

            IF MOD (V_RECORD_CNT, 10000) = 0
            THEN
               COMMIT;
               v_cmmtd_rec_cnt := V_RECORD_CNT;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.PUT_LINE (
                     'ERROR IN LOOP FOR PICK_ID - '
                  || v_get_nrmt_risk_rec.PICK_ID);
               DBMS_OUTPUT.PUT_LINE ('RECORDS LOADED - ' || v_cmmtd_rec_cnt);
         END;
      END LOOP;

      COMMIT;

      IF p_risk_model = 'PWOD' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_POLE_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_POLE_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_POLE_NOACTION_NPV15');
      ELSIF p_risk_model = 'PWOD' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_POLE_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_POLE_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_POLE_REPLACE_NPV15');
      ELSIF p_risk_model = 'PWOD' AND p_action = 'MAINTAIN' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_POLE_REINF');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_POLE_REINF_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_POLE_REINF_NPV15');
      ELSIF p_risk_model = 'XARMLV' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMLV_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMLV_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMLV_NOACTION_NPV15');
      ELSIF p_risk_model = 'XARMLV' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMLV_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMLV_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMLV_REPLACE_NPV15');
      ELSIF p_risk_model = 'XARMHV' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMHV_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMHV_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMHV_NOACTION_NPV15');
      ELSIF p_risk_model = 'XARMHV' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMHV_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMHV_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMHV_REPLACE_NPV15');
      ELSIF p_risk_model = 'STAY' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_STAY_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_STAY_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_STAY_NOACTION_NPV15');
      ELSIF p_risk_model = 'STAY' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_STAY_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_STAY_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_STAY_REPLACE_NPV15');
      ELSIF p_risk_model = 'INSUL' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_INSUL_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_INSUL_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_INSUL_NOACTION_NPV15');
      ELSIF p_risk_model = 'INSUL' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_INSUL_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_INSUL_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_INSUL_REPLACE_NPV15');
      ELSIF p_risk_model = 'NWPOLE' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_NWPOLE_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_NWPOLE_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_NWPOLE_NOACTION_NPV15');
      ELSIF p_risk_model = 'NWPOLE' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_NWPOLE_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_NWPOLE_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_NWPOLE_REPLACE_NPV15');
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_COND_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_COND_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_COND_NOACTION_NPV15');
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_COND_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_COND_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_COND_REPLACE_NPV15');
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'MAINTAIN' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_COND_REPAIR');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_COND_REPAIR_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_COND_REPAIR_NPV15');
      ELSIF p_risk_model = 'CONDGC' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDGCA_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDGCA_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDGCA_NOACTION_NPV15');
      ELSIF p_risk_model = 'CONDGC' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDGCA_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDGCA_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDGCA_REPLACE_NPV15');
      ELSIF p_risk_model = 'CONDCA' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDHCA_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDHCA_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDHCA_NOACTION_NPV15');
      ELSIF p_risk_model = 'CONDCA' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDHCA_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDHCA_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDHCA_REPLACE_NPV15');
      ELSIF p_risk_model = 'CONDVCA' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDVCA_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDVCA_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDVCA_NOACTION_NPV15');
      ELSIF p_risk_model = 'CONDVCA' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDVCA_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDVCA_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDVCA_REPLACE_NPV15');
      ELSIF p_risk_model = 'CONDEARTH' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDEAR_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDEAR_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDEAR_NOACTION_NPV15');
      ELSIF p_risk_model = 'CONDEARTH' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDEAR_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDEAR_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_CONDEAR_REPLACE_NPV15');
      ELSIF p_risk_model = 'DOF' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_DOF_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_DOF_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_DOF_NOACTION_NPV15');
      ELSIF p_risk_model = 'DOF' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_DOF_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_DOF_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_DOF_REPLACE_NPV15');
      ELSIF p_risk_model = 'DSTRPM' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_DSTR_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_DSTR_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_DSTR_NOACTION_NPV15');
      ELSIF p_risk_model = 'DSTRPM' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_DSTR_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_DSTR_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_DSTR_REPLACE_NPV15');
      ELSIF p_risk_model = 'PTSD' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_PTSD_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_PTSD_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_PTSD_NOACTION_NPV15');
      ELSIF p_risk_model = 'PTSD' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_PTSD_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_PTSD_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_PTSD_REPLACE_NPV15');
      ELSIF p_risk_model = 'RCLBS' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_RECL_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_RECL_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_RECL_NOACTION_NPV15');
      ELSIF p_risk_model = 'RCLBS' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_RECL_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_RECL_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_RECL_REPLACE_NPV15');
      ELSIF p_risk_model = 'VREG' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_RGTR_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_RGTR_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_RGTR_NOACTION_NPV15');
      ELSIF p_risk_model = 'VREG' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_RGTR_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_RGTR_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_RGTR_REPLACE_NPV15');
      ELSIF p_risk_model = 'SECT' AND p_action = 'DONOTHING' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_SECT_NOACTION');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_SECT_NOACTION_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_SECT_NOACTION_NPV15');
      ELSIF p_risk_model = 'SECT' AND p_action = 'REPLACE' THEN
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_SECT_REPLACE');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_SECT_REPLACE_FAIL');
         structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_SECT_REPLACE_NPV15');
      END IF;

      DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - '
         || p_risk_model
         || '_'
         || p_action);
      DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
      DBMS_OUTPUT.PUT_LINE (
         'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
      DBMS_OUTPUT.PUT_LINE (
         '***************************************************************************************************');
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END PR_LOAD_RISK_MODEL;

   PROCEDURE PR_LOAD_RISK_PWOD
   (p_yr_mth_no  IN    CHAR )
   AS
      v_record_cnt   NUMBER := 0;

      v_pwod_fin_conseq_amt      NUMBER;
      v_xarmhv_fin_conseq_amt    NUMBER;    
      v_xarmlv_fin_conseq_amt    NUMBER;
      v_insul_fin_conseq_amt     NUMBER;
      v_nwpole_fin_conseq_amt    NUMBER;
      v_stay_fin_conseq_amt      NUMBER;

      CURSOR get_reactv_cost_cur
      IS
         SELECT TRIM(mdl_bus_cde) AS mdl_bus_cde
               ,trmnt_reactv_cst_amt
         --FROM structoolsw.treatment_reactive_cost
         FROM structools.st_treatment_reactive_cost
      ;

   BEGIN
      DBMS_OUTPUT.put_line (
            'Risk MODEL - POLE (Combined)_DONOTHING Start TIME -'
         || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));

      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_PWOD_NOACTION');
      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_PWOD_NOACTION_NPV15');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_PWOD_NOACTION');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_PWOD_NOACTION_NPV15');

      DELETE FROM structools.ST_RISK_FRACTIONS frac
            WHERE FRAC.MODEL_NAME = 'PWOD';

      COMMIT;

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_PWOD_NOACTION
            (SELECT POLE.PICK_ID,
                    (  POLE.RISK_INDX_VAL_Y0
                     + NVL (XARMHV.RISK_INDX_VAL_Y0, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y0, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y0, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y0, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y0, 0)),
                    (  POLE.RISK_INDX_VAL_Y1
                     + NVL (XARMHV.RISK_INDX_VAL_Y1, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y1, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y1, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y1, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y1, 0)),
                    (  POLE.RISK_INDX_VAL_Y2
                     + NVL (XARMHV.RISK_INDX_VAL_Y2, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y2, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y2, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y2, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y2, 0)),
                    (  POLE.RISK_INDX_VAL_Y3
                     + NVL (XARMHV.RISK_INDX_VAL_Y3, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y3, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y3, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y3, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y3, 0)),
                    (  POLE.RISK_INDX_VAL_Y4
                     + NVL (XARMHV.RISK_INDX_VAL_Y4, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y4, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y4, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y4, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y4, 0)),
                    (  POLE.RISK_INDX_VAL_Y5
                     + NVL (XARMHV.RISK_INDX_VAL_Y5, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y5, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y5, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y5, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y5, 0)),
                    (  POLE.RISK_INDX_VAL_Y6
                     + NVL (XARMHV.RISK_INDX_VAL_Y6, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y6, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y6, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y6, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y6, 0)),
                    (  POLE.RISK_INDX_VAL_Y7
                     + NVL (XARMHV.RISK_INDX_VAL_Y7, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y7, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y7, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y7, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y7, 0)),
                    (  POLE.RISK_INDX_VAL_Y8
                     + NVL (XARMHV.RISK_INDX_VAL_Y8, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y8, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y8, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y8, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y8, 0)),
                    (  POLE.RISK_INDX_VAL_Y9
                     + NVL (XARMHV.RISK_INDX_VAL_Y9, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y9, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y9, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y9, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y9, 0)),
                    (  POLE.RISK_INDX_VAL_Y10
                     + NVL (XARMHV.RISK_INDX_VAL_Y10, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y10, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y10, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y10, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y10, 0)),
                    (  POLE.RISK_INDX_VAL_Y11
                     + NVL (XARMHV.RISK_INDX_VAL_Y11, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y11, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y11, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y11, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y11, 0)),
                    (  POLE.RISK_INDX_VAL_Y12
                     + NVL (XARMHV.RISK_INDX_VAL_Y12, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y12, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y12, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y12, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y12, 0)),
                    (  POLE.RISK_INDX_VAL_Y13
                     + NVL (XARMHV.RISK_INDX_VAL_Y13, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y13, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y13, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y13, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y13, 0)),
                    (  POLE.RISK_INDX_VAL_Y14
                     + NVL (XARMHV.RISK_INDX_VAL_Y14, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y14, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y14, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y14, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y14, 0)),
                    (  POLE.RISK_INDX_VAL_Y15
                     + NVL (XARMHV.RISK_INDX_VAL_Y15, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y15, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y15, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y15, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y15, 0)),
                    (  POLE.RISK_INDX_VAL_Y16
                     + NVL (XARMHV.RISK_INDX_VAL_Y16, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y16, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y16, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y16, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y16, 0)),
                    (  POLE.RISK_INDX_VAL_Y17
                     + NVL (XARMHV.RISK_INDX_VAL_Y17, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y17, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y17, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y17, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y17, 0)),
                    (  POLE.RISK_INDX_VAL_Y18
                     + NVL (XARMHV.RISK_INDX_VAL_Y18, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y18, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y18, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y18, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y18, 0)),
                    (  POLE.RISK_INDX_VAL_Y19
                     + NVL (XARMHV.RISK_INDX_VAL_Y19, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y19, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y19, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y19, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y19, 0)),
                    (  POLE.RISK_INDX_VAL_Y20
                     + NVL (XARMHV.RISK_INDX_VAL_Y20, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y20, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y20, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y20, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y20, 0)),
                    (  POLE.RISK_INDX_VAL_Y21
                     + NVL (XARMHV.RISK_INDX_VAL_Y21, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y21, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y21, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y21, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y21, 0)),
                    (  POLE.RISK_INDX_VAL_Y22
                     + NVL (XARMHV.RISK_INDX_VAL_Y22, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y22, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y22, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y22, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y22, 0)),
                    (  POLE.RISK_INDX_VAL_Y23
                     + NVL (XARMHV.RISK_INDX_VAL_Y23, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y23, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y23, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y23, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y23, 0)),
                    (  POLE.RISK_INDX_VAL_Y24
                     + NVL (XARMHV.RISK_INDX_VAL_Y24, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y24, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y24, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y24, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y24, 0)),
                    (  POLE.RISK_INDX_VAL_Y25
                     + NVL (XARMHV.RISK_INDX_VAL_Y25, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y25, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y25, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y25, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y25, 0)),
                    (  POLE.RISK_INDX_VAL_Y26
                     + NVL (XARMHV.RISK_INDX_VAL_Y26, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y26, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y26, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y26, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y26, 0)),
                    (  POLE.RISK_INDX_VAL_Y27
                     + NVL (XARMHV.RISK_INDX_VAL_Y27, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y27, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y27, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y27, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y27, 0)),
                    (  POLE.RISK_INDX_VAL_Y28
                     + NVL (XARMHV.RISK_INDX_VAL_Y28, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y28, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y28, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y28, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y28, 0)),
                    (  POLE.RISK_INDX_VAL_Y29
                     + NVL (XARMHV.RISK_INDX_VAL_Y29, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y29, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y29, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y29, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y29, 0)),
                    (  POLE.RISK_INDX_VAL_Y30
                     + NVL (XARMHV.RISK_INDX_VAL_Y30, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y30, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y30, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y30, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y30, 0)),
                    (  POLE.RISK_INDX_VAL_Y31
                     + NVL (XARMHV.RISK_INDX_VAL_Y31, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y31, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y31, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y31, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y31, 0)),
                    (  POLE.RISK_INDX_VAL_Y32
                     + NVL (XARMHV.RISK_INDX_VAL_Y32, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y32, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y32, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y32, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y32, 0)),
                    (  POLE.RISK_INDX_VAL_Y33
                     + NVL (XARMHV.RISK_INDX_VAL_Y33, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y33, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y33, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y33, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y33, 0)),
                    (  POLE.RISK_INDX_VAL_Y34
                     + NVL (XARMHV.RISK_INDX_VAL_Y34, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y34, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y34, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y34, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y34, 0)),
                    (  POLE.RISK_INDX_VAL_Y35
                     + NVL (XARMHV.RISK_INDX_VAL_Y35, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y35, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y35, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y35, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y35, 0)),
                    (  POLE.RISK_INDX_VAL_Y36
                     + NVL (XARMHV.RISK_INDX_VAL_Y36, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y36, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y36, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y36, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y36, 0)),
                    (  POLE.RISK_INDX_VAL_Y37
                     + NVL (XARMHV.RISK_INDX_VAL_Y37, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y37, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y37, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y37, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y37, 0)),
                    (  POLE.RISK_INDX_VAL_Y38
                     + NVL (XARMHV.RISK_INDX_VAL_Y38, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y38, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y38, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y38, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y38, 0)),
                    (  POLE.RISK_INDX_VAL_Y39
                     + NVL (XARMHV.RISK_INDX_VAL_Y39, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y39, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y39, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y39, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y39, 0)),
                    (  POLE.RISK_INDX_VAL_Y40
                     + NVL (XARMHV.RISK_INDX_VAL_Y40, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y40, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y40, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y40, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y40, 0)),
                    (  POLE.RISK_INDX_VAL_Y41
                     + NVL (XARMHV.RISK_INDX_VAL_Y41, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y41, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y41, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y41, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y41, 0)),
                    (  POLE.RISK_INDX_VAL_Y42
                     + NVL (XARMHV.RISK_INDX_VAL_Y42, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y42, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y42, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y42, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y42, 0)),
                    (  POLE.RISK_INDX_VAL_Y43
                     + NVL (XARMHV.RISK_INDX_VAL_Y43, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y43, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y43, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y43, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y43, 0)),
                    (  POLE.RISK_INDX_VAL_Y44
                     + NVL (XARMHV.RISK_INDX_VAL_Y44, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y44, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y44, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y44, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y44, 0)),
                    (  POLE.RISK_INDX_VAL_Y45
                     + NVL (XARMHV.RISK_INDX_VAL_Y45, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y45, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y45, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y45, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y45, 0)),
                    (  POLE.RISK_INDX_VAL_Y46
                     + NVL (XARMHV.RISK_INDX_VAL_Y46, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y46, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y46, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y46, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y46, 0)),
                    (  POLE.RISK_INDX_VAL_Y47
                     + NVL (XARMHV.RISK_INDX_VAL_Y47, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y47, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y47, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y47, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y47, 0)),
                    (  POLE.RISK_INDX_VAL_Y48
                     + NVL (XARMHV.RISK_INDX_VAL_Y48, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y48, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y48, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y48, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y48, 0)),
                    (  POLE.RISK_INDX_VAL_Y49
                     + NVL (XARMHV.RISK_INDX_VAL_Y49, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y49, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y49, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y49, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y49, 0)),
                    (  POLE.RISK_INDX_VAL_Y50
                     + NVL (XARMHV.RISK_INDX_VAL_Y50, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y50, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y50, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y50, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y50, 0))
               FROM structools.ST_RISK_POLE_NOACTION pole,
                    structools.ST_RISK_XARMHV_NOACTION xarmhv,
                    structools.ST_RISK_XARMLV_NOACTION xarmlv,
                    structools.ST_RISK_INSUL_NOACTION insul,
                    structools.ST_RISK_NWPOLE_NOACTION nwpole,
                    structools.ST_RISK_STAY_NOACTION stay
              WHERE     POLE.PICK_ID = XARMHV.PICK_ID(+)
                    AND POLE.PICK_ID = XARMLV.PICK_ID(+)
                    AND POLE.PICK_ID = INSUL.PICK_ID(+)
                    AND POLE.PICK_ID = NWPOLE.PICK_ID(+)
                    AND POLE.PICK_ID = STAY.PICK_ID(+) --AND pole.pick_id IN ('307', '511')
                                                      );

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - PWOD (Combined) DONOTHING');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING risk records IN ST_RISK_PWOD_NOACTION -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      /* load reactive cost of all  models to an associative array, indexed by the model name */
      /* This only needs to be done if this procedure is invoked on its own and not as part of PR_LOAD_ALL_RISK_TABLES procedure */
      IF v_reac_cost_tab.COUNT = 0 THEN
         FOR c IN get_reactv_cost_cur LOOP
            v_reac_cost_tab(c.mdl_bus_cde) := c.trmnt_reactv_cst_amt;
         END LOOP;   
      END IF;

      v_pwod_fin_conseq_amt   := v_reac_cost_tab('PWOD');
      v_xarmhv_fin_conseq_amt := v_reac_cost_tab('XARMHV');
      v_xarmlv_fin_conseq_amt := v_reac_cost_tab('XARMLV');
      v_insul_fin_conseq_amt  := v_reac_cost_tab('INSUL');
      v_nwpole_fin_conseq_amt := v_reac_cost_tab('NWPOLE');
      v_stay_fin_conseq_amt   := v_reac_cost_tab('STAY');

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_PWOD_NOACTION_NPV15
            WITH conseq_calc AS   --fin_frac - copy from here to the end of the sql and replace atm.mdl_bus_cde values
            (
            SELECT 
               (CASE
                   WHEN instrc (ed.plnt_no,
                                ' ',
                                1,
                                1) = 0
                   THEN
                      SUBSTR (ed.plnt_no,
                              -1 * (LENGTH (ed.plnt_no) - 1))
                   ELSE
                      SUBSTR (ed.plnt_no,
                              2,
                              instrc (ed.plnt_no,
                                      ' ',
                                      1,
                                      1) - 2)
                END) AS pick_id
               ,nrdmf.hzrd_fn_1_2
               ,nrdmf.FIRE_CNSQNC_AMT
               ,nrdmf.ELEC_SHCK_CNSQNC_AMT
               ,nrdmf.WRK_FRCE_CNSQNC_AMT
               ,nrdmf.PHYS_IMPCT_CNSQNC_AMT
               ,nrdmf.RELIABILITY_CNSQNC_AMT
               ,nrdmf.ENV_CNSQNC_AMT
               ,CASE
                  WHEN atm.mdl_bus_cde = 'PWOD'          THEN v_pwod_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'XARMHV'        THEN v_xarmhv_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'XARMLV'        THEN v_xarmlv_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'INSUL'         THEN v_insul_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'NWPOLE'        THEN v_nwpole_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'STAY'          THEN v_stay_fin_conseq_amt
               END              AS FIN_CNSQNC_AMT 
               ,CASE                                                                      --fin_fracc - replace
                  WHEN atm.mdl_bus_cde = 'PWOD'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_pwod_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'XARMHV'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_xarmhv_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'XARMLV'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_xarmlv_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'INSUL'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_insul_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'NWPOLE'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_nwpole_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'STAY'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_stay_fin_conseq_amt
               END              AS conseq_amt 
            FROM structoolsw.mthly_equip_risk_dm_fact nrdmf
               JOIN
               structoolsw.nrm_asset_type_model_dim atm
                  ON atm.nrm_asset_typ_mdl_sk =
                        nrdmf.nrm_asset_typ_mdl_sk
               JOIN structoolsw.treatment_dim td
                  ON td.trmnt_sk = nrdmf.trmnt_sk
               JOIN structoolsw.equipment_nvgtn_dim en
                  ON en.equip_sk = nrdmf.equip_sk
               JOIN structoolsw.equipment_dim ed
                  ON ed.equip_nvgtn_sk = en.equip_nvgtn_sk
            WHERE     nrdmf.yr_mth_no = p_yr_mth_no
                  AND td.trmnt_nam = 'DONOTHING'
                  AND atm.mdl_bus_cde IN
                         ('PWOD',
                          'XARMLV',
                          'XARMHV',
                          'STAY',
                          'INSUL',
                          'NWPOLE')
                  --AND ed.pick_id = 'S318'
            )
            ,
            frac AS
            (  
            SELECT   pick_id
                    ,SUM (FIRE_CNSQNC_AMT * hzrd_fn_1_2)          /  SUM (hzrd_fn_1_2 * conseq_amt) AS fire_frac
                    ,SUM (ELEC_SHCK_CNSQNC_AMT * hzrd_fn_1_2)     /  SUM (hzrd_fn_1_2 * conseq_amt) AS ESHOCK_frac
                    ,SUM (WRK_FRCE_CNSQNC_AMT * hzrd_fn_1_2)      /  SUM (hzrd_fn_1_2 * conseq_amt) AS WFORCE_frac
                    ,SUM (RELIABILITY_CNSQNC_AMT * hzrd_fn_1_2)   /  SUM (hzrd_fn_1_2 * conseq_amt) AS RELIABILITY_frac
                    ,SUM (PHYS_IMPCT_CNSQNC_AMT * hzrd_fn_1_2)    /  SUM (hzrd_fn_1_2 * conseq_amt) AS PHYSIMP_frac
                    ,SUM (ENV_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS ENV_frac
                    ,SUM (FIN_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS FIN_frac      
             FROM conseq_calc
            GROUP BY pick_id
            )
            SELECT POLE.PICK_ID,
                    (  POLE.RISK_INDX_VAL_Y0
                     + NVL (XARMHV.RISK_INDX_VAL_Y0, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y0, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y0, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y0, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y0, 0)),
                    (  POLE.RISK_INDX_VAL_Y1
                     + NVL (XARMHV.RISK_INDX_VAL_Y1, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y1, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y1, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y1, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y1, 0)),
                    (  POLE.RISK_INDX_VAL_Y2
                     + NVL (XARMHV.RISK_INDX_VAL_Y2, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y2, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y2, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y2, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y2, 0)),
                    (  POLE.RISK_INDX_VAL_Y3
                     + NVL (XARMHV.RISK_INDX_VAL_Y3, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y3, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y3, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y3, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y3, 0)),
                    (  POLE.RISK_INDX_VAL_Y4
                     + NVL (XARMHV.RISK_INDX_VAL_Y4, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y4, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y4, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y4, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y4, 0)),
                    (  POLE.RISK_INDX_VAL_Y5
                     + NVL (XARMHV.RISK_INDX_VAL_Y5, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y5, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y5, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y5, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y5, 0)),
                    (  POLE.RISK_INDX_VAL_Y6
                     + NVL (XARMHV.RISK_INDX_VAL_Y6, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y6, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y6, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y6, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y6, 0)),
                    (  POLE.RISK_INDX_VAL_Y7
                     + NVL (XARMHV.RISK_INDX_VAL_Y7, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y7, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y7, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y7, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y7, 0)),
                    (  POLE.RISK_INDX_VAL_Y8
                     + NVL (XARMHV.RISK_INDX_VAL_Y8, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y8, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y8, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y8, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y8, 0)),
                    (  POLE.RISK_INDX_VAL_Y9
                     + NVL (XARMHV.RISK_INDX_VAL_Y9, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y9, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y9, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y9, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y9, 0)),
                    (  POLE.RISK_INDX_VAL_Y10
                     + NVL (XARMHV.RISK_INDX_VAL_Y10, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y10, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y10, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y10, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y10, 0)),
                    (  POLE.RISK_INDX_VAL_Y11
                     + NVL (XARMHV.RISK_INDX_VAL_Y11, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y11, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y11, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y11, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y11, 0)),
                    (  POLE.RISK_INDX_VAL_Y12
                     + NVL (XARMHV.RISK_INDX_VAL_Y12, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y12, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y12, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y12, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y12, 0)),
                    (  POLE.RISK_INDX_VAL_Y13
                     + NVL (XARMHV.RISK_INDX_VAL_Y13, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y13, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y13, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y13, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y13, 0)),
                    (  POLE.RISK_INDX_VAL_Y14
                     + NVL (XARMHV.RISK_INDX_VAL_Y14, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y14, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y14, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y14, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y14, 0)),
                    (  POLE.RISK_INDX_VAL_Y15
                     + NVL (XARMHV.RISK_INDX_VAL_Y15, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y15, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y15, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y15, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y15, 0)),
                    (  POLE.RISK_INDX_VAL_Y16
                     + NVL (XARMHV.RISK_INDX_VAL_Y16, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y16, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y16, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y16, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y16, 0)),
                    (  POLE.RISK_INDX_VAL_Y17
                     + NVL (XARMHV.RISK_INDX_VAL_Y17, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y17, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y17, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y17, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y17, 0)),
                    (  POLE.RISK_INDX_VAL_Y18
                     + NVL (XARMHV.RISK_INDX_VAL_Y18, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y18, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y18, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y18, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y18, 0)),
                    (  POLE.RISK_INDX_VAL_Y19
                     + NVL (XARMHV.RISK_INDX_VAL_Y19, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y19, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y19, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y19, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y19, 0)),
                    (  POLE.RISK_INDX_VAL_Y20
                     + NVL (XARMHV.RISK_INDX_VAL_Y20, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y20, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y20, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y20, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y20, 0)),
                    (  POLE.RISK_INDX_VAL_Y21
                     + NVL (XARMHV.RISK_INDX_VAL_Y21, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y21, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y21, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y21, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y21, 0)),
                    (  POLE.RISK_INDX_VAL_Y22
                     + NVL (XARMHV.RISK_INDX_VAL_Y22, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y22, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y22, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y22, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y22, 0)),
                    (  POLE.RISK_INDX_VAL_Y23
                     + NVL (XARMHV.RISK_INDX_VAL_Y23, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y23, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y23, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y23, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y23, 0)),
                    (  POLE.RISK_INDX_VAL_Y24
                     + NVL (XARMHV.RISK_INDX_VAL_Y24, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y24, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y24, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y24, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y24, 0)),
                    (  POLE.RISK_INDX_VAL_Y25
                     + NVL (XARMHV.RISK_INDX_VAL_Y25, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y25, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y25, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y25, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y25, 0)),
                    (  POLE.RISK_INDX_VAL_Y26
                     + NVL (XARMHV.RISK_INDX_VAL_Y26, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y26, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y26, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y26, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y26, 0)),
                    (  POLE.RISK_INDX_VAL_Y27
                     + NVL (XARMHV.RISK_INDX_VAL_Y27, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y27, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y27, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y27, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y27, 0)),
                    (  POLE.RISK_INDX_VAL_Y28
                     + NVL (XARMHV.RISK_INDX_VAL_Y28, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y28, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y28, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y28, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y28, 0)),
                    (  POLE.RISK_INDX_VAL_Y29
                     + NVL (XARMHV.RISK_INDX_VAL_Y29, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y29, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y29, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y29, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y29, 0)),
                    (  POLE.RISK_INDX_VAL_Y30
                     + NVL (XARMHV.RISK_INDX_VAL_Y30, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y30, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y30, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y30, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y30, 0)),
                    (  POLE.RISK_INDX_VAL_Y31
                     + NVL (XARMHV.RISK_INDX_VAL_Y31, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y31, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y31, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y31, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y31, 0)),
                    (  POLE.RISK_INDX_VAL_Y32
                     + NVL (XARMHV.RISK_INDX_VAL_Y32, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y32, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y32, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y32, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y32, 0)),
                    (  POLE.RISK_INDX_VAL_Y33
                     + NVL (XARMHV.RISK_INDX_VAL_Y33, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y33, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y33, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y33, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y33, 0)),
                    (  POLE.RISK_INDX_VAL_Y34
                     + NVL (XARMHV.RISK_INDX_VAL_Y34, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y34, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y34, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y34, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y34, 0)),
                    (  POLE.RISK_INDX_VAL_Y35
                     + NVL (XARMHV.RISK_INDX_VAL_Y35, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y35, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y35, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y35, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y35, 0)),
                    (  POLE.RISK_INDX_VAL_Y36
                     + NVL (XARMHV.RISK_INDX_VAL_Y36, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y36, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y36, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y36, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y36, 0)),
                    (  POLE.RISK_INDX_VAL_Y37
                     + NVL (XARMHV.RISK_INDX_VAL_Y37, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y37, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y37, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y37, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y37, 0)),
                    (  POLE.RISK_INDX_VAL_Y38
                     + NVL (XARMHV.RISK_INDX_VAL_Y38, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y38, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y38, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y38, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y38, 0)),
                    (  POLE.RISK_INDX_VAL_Y39
                     + NVL (XARMHV.RISK_INDX_VAL_Y39, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y39, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y39, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y39, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y39, 0)),
                    (  POLE.RISK_INDX_VAL_Y40
                     + NVL (XARMHV.RISK_INDX_VAL_Y40, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y40, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y40, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y40, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y40, 0)),
                    (  POLE.RISK_INDX_VAL_Y41
                     + NVL (XARMHV.RISK_INDX_VAL_Y41, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y41, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y41, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y41, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y41, 0)),
                    (  POLE.RISK_INDX_VAL_Y42
                     + NVL (XARMHV.RISK_INDX_VAL_Y42, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y42, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y42, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y42, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y42, 0)),
                    (  POLE.RISK_INDX_VAL_Y43
                     + NVL (XARMHV.RISK_INDX_VAL_Y43, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y43, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y43, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y43, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y43, 0)),
                    (  POLE.RISK_INDX_VAL_Y44
                     + NVL (XARMHV.RISK_INDX_VAL_Y44, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y44, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y44, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y44, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y44, 0)),
                    (  POLE.RISK_INDX_VAL_Y45
                     + NVL (XARMHV.RISK_INDX_VAL_Y45, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y45, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y45, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y45, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y45, 0)),
                    (  POLE.RISK_INDX_VAL_Y46
                     + NVL (XARMHV.RISK_INDX_VAL_Y46, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y46, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y46, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y46, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y46, 0)),
                    (  POLE.RISK_INDX_VAL_Y47
                     + NVL (XARMHV.RISK_INDX_VAL_Y47, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y47, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y47, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y47, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y47, 0)),
                    (  POLE.RISK_INDX_VAL_Y48
                     + NVL (XARMHV.RISK_INDX_VAL_Y48, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y48, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y48, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y48, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y48, 0)),
                    (  POLE.RISK_INDX_VAL_Y49
                     + NVL (XARMHV.RISK_INDX_VAL_Y49, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y49, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y49, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y49, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y49, 0)),
                    (  POLE.RISK_INDX_VAL_Y50
                     + NVL (XARMHV.RISK_INDX_VAL_Y50, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y50, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y50, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y50, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y50, 0)),
                    frac.FIRE_FRAC,
                    frac.ESHOCK_FRAC,
                    frac.WFORCE_FRAC,
                    frac.RELIABILITY_FRAC,
                    frac.PHYSIMP_FRAC,
                    frac.ENV_FRAC,
                    frac.FIN_FRAC         --fin_frac
               FROM structools.ST_RISK_POLE_NOACTION_NPV15 pole,
                    structools.ST_RISK_XARMHV_NOACTION_NPV15 xarmhv,
                    structools.ST_RISK_XARMLV_NOACTION_NPV15 xarmlv,
                    structools.ST_RISK_INSUL_NOACTION_NPV15 insul,
                    structools.ST_RISK_NWPOLE_NOACTION_NPV15 nwpole,
                    structools.ST_RISK_STAY_NOACTION_NPV15 stay,
                    frac
              WHERE  
                 POLE.PICK_ID = XARMHV.PICK_ID(+)
                    AND POLE.PICK_ID = XARMLV.PICK_ID(+)
                    AND POLE.PICK_ID = INSUL.PICK_ID(+)
                    AND POLE.PICK_ID = NWPOLE.PICK_ID(+)
                    AND POLE.PICK_ID = STAY.PICK_ID(+)
                    AND POLE.PICK_ID = frac.pick_id(+) --AND pole.pick_id IN ('307', '511')
              ;

         v_record_cnt := SQL%ROWCOUNT;

         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - PWOD (Combined) DONOTHING NPV15');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN NPV TABLE ST_RISK_PWOD_NOACTION_NPV15 -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      BEGIN
         INSERT INTO structools.ST_RISK_FRACTIONS
            WITH conseq_calc AS   --fin_frac - copy from here to the end of the sql and replace atm.mdl_bus_cde values
            (
            SELECT 'S' || (CASE
                      WHEN instrc (ed.plnt_no,
                                   ' ',
                                   1,
                                   1) = 0
                      THEN
                         SUBSTR (ed.plnt_no,
                                 -1 * (LENGTH (ed.plnt_no) - 1))
                      ELSE
                         SUBSTR (ed.plnt_no,
                                 2,
                                 instrc (ed.plnt_no,
                                         ' ',
                                         1,
                                         1) - 2)
                   END) AS pick_id
                  ,nrdmf.hzrd_fn_1_2
                  ,nrdmf.FIRE_CNSQNC_AMT
                  ,nrdmf.ELEC_SHCK_CNSQNC_AMT
                  ,nrdmf.WRK_FRCE_CNSQNC_AMT
                  ,nrdmf.PHYS_IMPCT_CNSQNC_AMT
                  ,nrdmf.RELIABILITY_CNSQNC_AMT
                  ,nrdmf.ENV_CNSQNC_AMT
                  ,CASE
                     WHEN atm.mdl_bus_cde = 'PWOD'          THEN v_pwod_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'XARMHV'        THEN v_xarmhv_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'XARMLV'        THEN v_xarmlv_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'INSUL'         THEN v_insul_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'NWPOLE'         THEN v_nwpole_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'STAY'          THEN v_stay_fin_conseq_amt
                  END              AS FIN_CNSQNC_AMT 
                  ,CASE                                                                      --fin_fracc - replace
                     WHEN atm.mdl_bus_cde = 'PWOD'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_pwod_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'XARMHV'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_xarmhv_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'XARMLV'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_xarmlv_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'INSUL'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_insul_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'NWPOLE'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_nwpole_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'STAY'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_stay_fin_conseq_amt
                  END              AS conseq_amt 
             FROM structoolsw.mthly_equip_risk_dm_fact nrdmf
                  JOIN
                  structoolsw.nrm_asset_type_model_dim atm
                     ON atm.nrm_asset_typ_mdl_sk =
                           nrdmf.nrm_asset_typ_mdl_sk
                  JOIN structoolsw.treatment_dim td
                     ON td.trmnt_sk = nrdmf.trmnt_sk
                  JOIN structoolsw.equipment_nvgtn_dim en
                     ON en.equip_sk = nrdmf.equip_sk
                  JOIN structoolsw.equipment_dim ed
                     ON ed.equip_nvgtn_sk = en.equip_nvgtn_sk
            WHERE     nrdmf.yr_mth_no = p_yr_mth_no
                  AND td.trmnt_nam = 'DONOTHING'
                  AND atm.mdl_bus_cde IN
                         ('PWOD',            --fin_frac - replace
                          'XARMLV',
                          'XARMHV',
                          'STAY',
                          'INSUL',
                          'NWPOLE')
                  --AND ed.pick_id = 'S318'
            )  
            SELECT   pick_id
                     ,'PWOD'                                    -- model_type
                     ,0                                          -- FIRE_INCD
                     ,0                                    -- ELEC_SHOCK_INCD
                     ,0                                    --LRGE_RLBLTY_INCD
                     ,0                            --ELEC_SHOCK_FATALITY_INCD
                     ,0                                 -- FIRE_FATALITY_INCD
                    ,SUM (FIRE_CNSQNC_AMT * hzrd_fn_1_2)          /  SUM (hzrd_fn_1_2 * conseq_amt) AS fire_frac
                    ,SUM (ELEC_SHCK_CNSQNC_AMT * hzrd_fn_1_2)     /  SUM (hzrd_fn_1_2 * conseq_amt) AS ESHOCK_frac
                    ,SUM (WRK_FRCE_CNSQNC_AMT * hzrd_fn_1_2)      /  SUM (hzrd_fn_1_2 * conseq_amt) AS WFORCE_frac
                    ,SUM (RELIABILITY_CNSQNC_AMT * hzrd_fn_1_2)   /  SUM (hzrd_fn_1_2 * conseq_amt) AS RELIABILITY_frac
                    ,SUM (PHYS_IMPCT_CNSQNC_AMT * hzrd_fn_1_2)    /  SUM (hzrd_fn_1_2 * conseq_amt) AS PHYSIMP_frac
                    ,SUM (ENV_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS ENV_frac
                    ,SUM (FIN_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS FIN_frac      
             FROM conseq_calc
             GROUP BY pick_id
             
         ;

         v_record_cnt := SQL%ROWCOUNT;

         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - PWOD (Combined) ST_RISK_FRACTIONS');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN TABLE ST_RISK_FRACTIONS FOR PWOD-'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_PWOD_NOACTION');
      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_PWOD_NOACTION_NPV15');


      DBMS_OUTPUT.put_line (
            'Risk MODEL - POLE (Combined)_REPLACE Start TIME -'
         || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));

      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_PWOD_REPLACE');
      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_PWOD_REPLACE_NPV15');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_PWOD_REPLACE');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_PWOD_REPLACE_NPV15');
      
      COMMIT;

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_PWOD_REPLACE
            (SELECT POLE.PICK_ID,
                    (  POLE.RISK_INDX_VAL_Y0
                     + NVL (XARMHV.RISK_INDX_VAL_Y0, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y0, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y0, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y0, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y0, 0)),
                    (  POLE.RISK_INDX_VAL_Y1
                     + NVL (XARMHV.RISK_INDX_VAL_Y1, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y1, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y1, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y1, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y1, 0)),
                    (  POLE.RISK_INDX_VAL_Y2
                     + NVL (XARMHV.RISK_INDX_VAL_Y2, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y2, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y2, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y2, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y2, 0)),
                    (  POLE.RISK_INDX_VAL_Y3
                     + NVL (XARMHV.RISK_INDX_VAL_Y3, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y3, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y3, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y3, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y3, 0)),
                    (  POLE.RISK_INDX_VAL_Y4
                     + NVL (XARMHV.RISK_INDX_VAL_Y4, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y4, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y4, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y4, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y4, 0)),
                    (  POLE.RISK_INDX_VAL_Y5
                     + NVL (XARMHV.RISK_INDX_VAL_Y5, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y5, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y5, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y5, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y5, 0)),
                    (  POLE.RISK_INDX_VAL_Y6
                     + NVL (XARMHV.RISK_INDX_VAL_Y6, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y6, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y6, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y6, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y6, 0)),
                    (  POLE.RISK_INDX_VAL_Y7
                     + NVL (XARMHV.RISK_INDX_VAL_Y7, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y7, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y7, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y7, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y7, 0)),
                    (  POLE.RISK_INDX_VAL_Y8
                     + NVL (XARMHV.RISK_INDX_VAL_Y8, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y8, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y8, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y8, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y8, 0)),
                    (  POLE.RISK_INDX_VAL_Y9
                     + NVL (XARMHV.RISK_INDX_VAL_Y9, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y9, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y9, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y9, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y9, 0)),
                    (  POLE.RISK_INDX_VAL_Y10
                     + NVL (XARMHV.RISK_INDX_VAL_Y10, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y10, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y10, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y10, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y10, 0)),
                    (  POLE.RISK_INDX_VAL_Y11
                     + NVL (XARMHV.RISK_INDX_VAL_Y11, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y11, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y11, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y11, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y11, 0)),
                    (  POLE.RISK_INDX_VAL_Y12
                     + NVL (XARMHV.RISK_INDX_VAL_Y12, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y12, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y12, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y12, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y12, 0)),
                    (  POLE.RISK_INDX_VAL_Y13
                     + NVL (XARMHV.RISK_INDX_VAL_Y13, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y13, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y13, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y13, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y13, 0)),
                    (  POLE.RISK_INDX_VAL_Y14
                     + NVL (XARMHV.RISK_INDX_VAL_Y14, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y14, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y14, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y14, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y14, 0)),
                    (  POLE.RISK_INDX_VAL_Y15
                     + NVL (XARMHV.RISK_INDX_VAL_Y15, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y15, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y15, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y15, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y15, 0)),
                    (  POLE.RISK_INDX_VAL_Y16
                     + NVL (XARMHV.RISK_INDX_VAL_Y16, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y16, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y16, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y16, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y16, 0)),
                    (  POLE.RISK_INDX_VAL_Y17
                     + NVL (XARMHV.RISK_INDX_VAL_Y17, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y17, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y17, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y17, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y17, 0)),
                    (  POLE.RISK_INDX_VAL_Y18
                     + NVL (XARMHV.RISK_INDX_VAL_Y18, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y18, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y18, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y18, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y18, 0)),
                    (  POLE.RISK_INDX_VAL_Y19
                     + NVL (XARMHV.RISK_INDX_VAL_Y19, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y19, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y19, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y19, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y19, 0)),
                    (  POLE.RISK_INDX_VAL_Y20
                     + NVL (XARMHV.RISK_INDX_VAL_Y20, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y20, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y20, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y20, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y20, 0)),
                    (  POLE.RISK_INDX_VAL_Y21
                     + NVL (XARMHV.RISK_INDX_VAL_Y21, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y21, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y21, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y21, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y21, 0)),
                    (  POLE.RISK_INDX_VAL_Y22
                     + NVL (XARMHV.RISK_INDX_VAL_Y22, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y22, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y22, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y22, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y22, 0)),
                    (  POLE.RISK_INDX_VAL_Y23
                     + NVL (XARMHV.RISK_INDX_VAL_Y23, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y23, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y23, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y23, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y23, 0)),
                    (  POLE.RISK_INDX_VAL_Y24
                     + NVL (XARMHV.RISK_INDX_VAL_Y24, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y24, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y24, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y24, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y24, 0)),
                    (  POLE.RISK_INDX_VAL_Y25
                     + NVL (XARMHV.RISK_INDX_VAL_Y25, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y25, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y25, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y25, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y25, 0)),
                    (  POLE.RISK_INDX_VAL_Y26
                     + NVL (XARMHV.RISK_INDX_VAL_Y26, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y26, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y26, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y26, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y26, 0)),
                    (  POLE.RISK_INDX_VAL_Y27
                     + NVL (XARMHV.RISK_INDX_VAL_Y27, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y27, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y27, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y27, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y27, 0)),
                    (  POLE.RISK_INDX_VAL_Y28
                     + NVL (XARMHV.RISK_INDX_VAL_Y28, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y28, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y28, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y28, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y28, 0)),
                    (  POLE.RISK_INDX_VAL_Y29
                     + NVL (XARMHV.RISK_INDX_VAL_Y29, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y29, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y29, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y29, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y29, 0)),
                    (  POLE.RISK_INDX_VAL_Y30
                     + NVL (XARMHV.RISK_INDX_VAL_Y30, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y30, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y30, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y30, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y30, 0)),
                    (  POLE.RISK_INDX_VAL_Y31
                     + NVL (XARMHV.RISK_INDX_VAL_Y31, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y31, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y31, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y31, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y31, 0)),
                    (  POLE.RISK_INDX_VAL_Y32
                     + NVL (XARMHV.RISK_INDX_VAL_Y32, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y32, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y32, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y32, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y32, 0)),
                    (  POLE.RISK_INDX_VAL_Y33
                     + NVL (XARMHV.RISK_INDX_VAL_Y33, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y33, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y33, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y33, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y33, 0)),
                    (  POLE.RISK_INDX_VAL_Y34
                     + NVL (XARMHV.RISK_INDX_VAL_Y34, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y34, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y34, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y34, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y34, 0)),
                    (  POLE.RISK_INDX_VAL_Y35
                     + NVL (XARMHV.RISK_INDX_VAL_Y35, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y35, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y35, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y35, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y35, 0)),
                    (  POLE.RISK_INDX_VAL_Y36
                     + NVL (XARMHV.RISK_INDX_VAL_Y36, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y36, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y36, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y36, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y36, 0)),
                    (  POLE.RISK_INDX_VAL_Y37
                     + NVL (XARMHV.RISK_INDX_VAL_Y37, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y37, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y37, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y37, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y37, 0)),
                    (  POLE.RISK_INDX_VAL_Y38
                     + NVL (XARMHV.RISK_INDX_VAL_Y38, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y38, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y38, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y38, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y38, 0)),
                    (  POLE.RISK_INDX_VAL_Y39
                     + NVL (XARMHV.RISK_INDX_VAL_Y39, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y39, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y39, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y39, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y39, 0)),
                    (  POLE.RISK_INDX_VAL_Y40
                     + NVL (XARMHV.RISK_INDX_VAL_Y40, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y40, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y40, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y40, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y40, 0)),
                    (  POLE.RISK_INDX_VAL_Y41
                     + NVL (XARMHV.RISK_INDX_VAL_Y41, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y41, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y41, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y41, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y41, 0)),
                    (  POLE.RISK_INDX_VAL_Y42
                     + NVL (XARMHV.RISK_INDX_VAL_Y42, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y42, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y42, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y42, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y42, 0)),
                    (  POLE.RISK_INDX_VAL_Y43
                     + NVL (XARMHV.RISK_INDX_VAL_Y43, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y43, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y43, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y43, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y43, 0)),
                    (  POLE.RISK_INDX_VAL_Y44
                     + NVL (XARMHV.RISK_INDX_VAL_Y44, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y44, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y44, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y44, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y44, 0)),
                    (  POLE.RISK_INDX_VAL_Y45
                     + NVL (XARMHV.RISK_INDX_VAL_Y45, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y45, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y45, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y45, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y45, 0)),
                    (  POLE.RISK_INDX_VAL_Y46
                     + NVL (XARMHV.RISK_INDX_VAL_Y46, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y46, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y46, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y46, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y46, 0)),
                    (  POLE.RISK_INDX_VAL_Y47
                     + NVL (XARMHV.RISK_INDX_VAL_Y47, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y47, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y47, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y47, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y47, 0)),
                    (  POLE.RISK_INDX_VAL_Y48
                     + NVL (XARMHV.RISK_INDX_VAL_Y48, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y48, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y48, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y48, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y48, 0)),
                    (  POLE.RISK_INDX_VAL_Y49
                     + NVL (XARMHV.RISK_INDX_VAL_Y49, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y49, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y49, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y49, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y49, 0)),
                    (  POLE.RISK_INDX_VAL_Y50
                     + NVL (XARMHV.RISK_INDX_VAL_Y50, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y50, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y50, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y50, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y50, 0))
               FROM structools.ST_RISK_POLE_REPLACE pole,
                    structools.ST_RISK_XARMHV_REPLACE xarmhv,
                    structools.ST_RISK_XARMLV_REPLACE xarmlv,
                    structools.ST_RISK_INSUL_REPLACE insul,
                    structools.ST_RISK_NWPOLE_REPLACE nwpole,
                    structools.ST_RISK_STAY_REPLACE stay
              WHERE     POLE.PICK_ID = XARMHV.PICK_ID(+)
                    AND POLE.PICK_ID = XARMLV.PICK_ID(+)
                    AND POLE.PICK_ID = INSUL.PICK_ID(+)
                    AND POLE.PICK_ID = NWPOLE.PICK_ID(+)
                    AND POLE.PICK_ID = STAY.PICK_ID(+) --AND pole.pick_id IN ('307', '511')
                                                      );

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - PWOD (Combined) REPLACE');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING risk records IN ST_RISK_PWOD_REPLACE -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;


      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_PWOD_REPLACE_NPV15
            (SELECT POLE.PICK_ID,
                    (  POLE.RISK_INDX_VAL_Y0
                     + NVL (XARMHV.RISK_INDX_VAL_Y0, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y0, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y0, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y0, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y0, 0)),
                    (  POLE.RISK_INDX_VAL_Y1
                     + NVL (XARMHV.RISK_INDX_VAL_Y1, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y1, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y1, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y1, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y1, 0)),
                    (  POLE.RISK_INDX_VAL_Y2
                     + NVL (XARMHV.RISK_INDX_VAL_Y2, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y2, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y2, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y2, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y2, 0)),
                    (  POLE.RISK_INDX_VAL_Y3
                     + NVL (XARMHV.RISK_INDX_VAL_Y3, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y3, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y3, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y3, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y3, 0)),
                    (  POLE.RISK_INDX_VAL_Y4
                     + NVL (XARMHV.RISK_INDX_VAL_Y4, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y4, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y4, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y4, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y4, 0)),
                    (  POLE.RISK_INDX_VAL_Y5
                     + NVL (XARMHV.RISK_INDX_VAL_Y5, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y5, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y5, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y5, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y5, 0)),
                    (  POLE.RISK_INDX_VAL_Y6
                     + NVL (XARMHV.RISK_INDX_VAL_Y6, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y6, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y6, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y6, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y6, 0)),
                    (  POLE.RISK_INDX_VAL_Y7
                     + NVL (XARMHV.RISK_INDX_VAL_Y7, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y7, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y7, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y7, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y7, 0)),
                    (  POLE.RISK_INDX_VAL_Y8
                     + NVL (XARMHV.RISK_INDX_VAL_Y8, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y8, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y8, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y8, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y8, 0)),
                    (  POLE.RISK_INDX_VAL_Y9
                     + NVL (XARMHV.RISK_INDX_VAL_Y9, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y9, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y9, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y9, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y9, 0)),
                    (  POLE.RISK_INDX_VAL_Y10
                     + NVL (XARMHV.RISK_INDX_VAL_Y10, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y10, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y10, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y10, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y10, 0)),
                    (  POLE.RISK_INDX_VAL_Y11
                     + NVL (XARMHV.RISK_INDX_VAL_Y11, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y11, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y11, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y11, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y11, 0)),
                    (  POLE.RISK_INDX_VAL_Y12
                     + NVL (XARMHV.RISK_INDX_VAL_Y12, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y12, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y12, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y12, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y12, 0)),
                    (  POLE.RISK_INDX_VAL_Y13
                     + NVL (XARMHV.RISK_INDX_VAL_Y13, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y13, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y13, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y13, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y13, 0)),
                    (  POLE.RISK_INDX_VAL_Y14
                     + NVL (XARMHV.RISK_INDX_VAL_Y14, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y14, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y14, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y14, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y14, 0)),
                    (  POLE.RISK_INDX_VAL_Y15
                     + NVL (XARMHV.RISK_INDX_VAL_Y15, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y15, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y15, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y15, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y15, 0)),
                    (  POLE.RISK_INDX_VAL_Y16
                     + NVL (XARMHV.RISK_INDX_VAL_Y16, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y16, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y16, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y16, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y16, 0)),
                    (  POLE.RISK_INDX_VAL_Y17
                     + NVL (XARMHV.RISK_INDX_VAL_Y17, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y17, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y17, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y17, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y17, 0)),
                    (  POLE.RISK_INDX_VAL_Y18
                     + NVL (XARMHV.RISK_INDX_VAL_Y18, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y18, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y18, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y18, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y18, 0)),
                    (  POLE.RISK_INDX_VAL_Y19
                     + NVL (XARMHV.RISK_INDX_VAL_Y19, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y19, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y19, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y19, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y19, 0)),
                    (  POLE.RISK_INDX_VAL_Y20
                     + NVL (XARMHV.RISK_INDX_VAL_Y20, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y20, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y20, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y20, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y20, 0)),
                    (  POLE.RISK_INDX_VAL_Y21
                     + NVL (XARMHV.RISK_INDX_VAL_Y21, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y21, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y21, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y21, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y21, 0)),
                    (  POLE.RISK_INDX_VAL_Y22
                     + NVL (XARMHV.RISK_INDX_VAL_Y22, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y22, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y22, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y22, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y22, 0)),
                    (  POLE.RISK_INDX_VAL_Y23
                     + NVL (XARMHV.RISK_INDX_VAL_Y23, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y23, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y23, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y23, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y23, 0)),
                    (  POLE.RISK_INDX_VAL_Y24
                     + NVL (XARMHV.RISK_INDX_VAL_Y24, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y24, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y24, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y24, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y24, 0)),
                    (  POLE.RISK_INDX_VAL_Y25
                     + NVL (XARMHV.RISK_INDX_VAL_Y25, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y25, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y25, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y25, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y25, 0)),
                    (  POLE.RISK_INDX_VAL_Y26
                     + NVL (XARMHV.RISK_INDX_VAL_Y26, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y26, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y26, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y26, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y26, 0)),
                    (  POLE.RISK_INDX_VAL_Y27
                     + NVL (XARMHV.RISK_INDX_VAL_Y27, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y27, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y27, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y27, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y27, 0)),
                    (  POLE.RISK_INDX_VAL_Y28
                     + NVL (XARMHV.RISK_INDX_VAL_Y28, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y28, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y28, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y28, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y28, 0)),
                    (  POLE.RISK_INDX_VAL_Y29
                     + NVL (XARMHV.RISK_INDX_VAL_Y29, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y29, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y29, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y29, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y29, 0)),
                    (  POLE.RISK_INDX_VAL_Y30
                     + NVL (XARMHV.RISK_INDX_VAL_Y30, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y30, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y30, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y30, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y30, 0)),
                    (  POLE.RISK_INDX_VAL_Y31
                     + NVL (XARMHV.RISK_INDX_VAL_Y31, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y31, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y31, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y31, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y31, 0)),
                    (  POLE.RISK_INDX_VAL_Y32
                     + NVL (XARMHV.RISK_INDX_VAL_Y32, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y32, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y32, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y32, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y32, 0)),
                    (  POLE.RISK_INDX_VAL_Y33
                     + NVL (XARMHV.RISK_INDX_VAL_Y33, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y33, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y33, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y33, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y33, 0)),
                    (  POLE.RISK_INDX_VAL_Y34
                     + NVL (XARMHV.RISK_INDX_VAL_Y34, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y34, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y34, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y34, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y34, 0)),
                    (  POLE.RISK_INDX_VAL_Y35
                     + NVL (XARMHV.RISK_INDX_VAL_Y35, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y35, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y35, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y35, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y35, 0)),
                    (  POLE.RISK_INDX_VAL_Y36
                     + NVL (XARMHV.RISK_INDX_VAL_Y36, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y36, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y36, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y36, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y36, 0)),
                    (  POLE.RISK_INDX_VAL_Y37
                     + NVL (XARMHV.RISK_INDX_VAL_Y37, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y37, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y37, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y37, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y37, 0)),
                    (  POLE.RISK_INDX_VAL_Y38
                     + NVL (XARMHV.RISK_INDX_VAL_Y38, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y38, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y38, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y38, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y38, 0)),
                    (  POLE.RISK_INDX_VAL_Y39
                     + NVL (XARMHV.RISK_INDX_VAL_Y39, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y39, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y39, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y39, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y39, 0)),
                    (  POLE.RISK_INDX_VAL_Y40
                     + NVL (XARMHV.RISK_INDX_VAL_Y40, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y40, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y40, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y40, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y40, 0)),
                    (  POLE.RISK_INDX_VAL_Y41
                     + NVL (XARMHV.RISK_INDX_VAL_Y41, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y41, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y41, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y41, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y41, 0)),
                    (  POLE.RISK_INDX_VAL_Y42
                     + NVL (XARMHV.RISK_INDX_VAL_Y42, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y42, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y42, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y42, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y42, 0)),
                    (  POLE.RISK_INDX_VAL_Y43
                     + NVL (XARMHV.RISK_INDX_VAL_Y43, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y43, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y43, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y43, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y43, 0)),
                    (  POLE.RISK_INDX_VAL_Y44
                     + NVL (XARMHV.RISK_INDX_VAL_Y44, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y44, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y44, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y44, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y44, 0)),
                    (  POLE.RISK_INDX_VAL_Y45
                     + NVL (XARMHV.RISK_INDX_VAL_Y45, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y45, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y45, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y45, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y45, 0)),
                    (  POLE.RISK_INDX_VAL_Y46
                     + NVL (XARMHV.RISK_INDX_VAL_Y46, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y46, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y46, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y46, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y46, 0)),
                    (  POLE.RISK_INDX_VAL_Y47
                     + NVL (XARMHV.RISK_INDX_VAL_Y47, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y47, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y47, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y47, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y47, 0)),
                    (  POLE.RISK_INDX_VAL_Y48
                     + NVL (XARMHV.RISK_INDX_VAL_Y48, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y48, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y48, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y48, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y48, 0)),
                    (  POLE.RISK_INDX_VAL_Y49
                     + NVL (XARMHV.RISK_INDX_VAL_Y49, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y49, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y49, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y49, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y49, 0)),
                    (  POLE.RISK_INDX_VAL_Y50
                     + NVL (XARMHV.RISK_INDX_VAL_Y50, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y50, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y50, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y50, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y50, 0))
               FROM structools.ST_RISK_POLE_REPLACE_NPV15 pole,
                    structools.ST_RISK_XARMHV_REPLACE_NPV15 xarmhv,
                    structools.ST_RISK_XARMLV_REPLACE_NPV15 xarmlv,
                    structools.ST_RISK_INSUL_REPLACE_NPV15 insul,
                    structools.ST_RISK_NWPOLE_REPLACE_NPV15 nwpole,
                    structools.ST_RISK_STAY_REPLACE_NPV15 stay
              WHERE     POLE.PICK_ID = XARMHV.PICK_ID(+)
                    AND POLE.PICK_ID = XARMLV.PICK_ID(+)
                    AND POLE.PICK_ID = INSUL.PICK_ID(+)
                    AND POLE.PICK_ID = NWPOLE.PICK_ID(+)
                    AND POLE.PICK_ID = STAY.PICK_ID(+) --AND pole.pick_id IN ('307', '511')
                                                      );

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - PWOD (Combined) REPLACE NPV15');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN NPV TABLE ST_RISK_PWOD_REPLACE_NPV15 -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_PWOD_REPLACE');
      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_PWOD_REPLACE_NPV15');

      DBMS_OUTPUT.put_line (
            'Risk MODEL - POLE (Combined)_REINFORCE Start TIME -'
         || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));

      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_PWOD_REINF');
      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_PWOD_REINF_NPV15');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_PWOD_REINF');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_PWOD_REINF_NPV15');
      
      COMMIT;

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_PWOD_REINF
            (SELECT POLE.PICK_ID,
                    (  POLE.RISK_INDX_VAL_Y0
                     + NVL (XARMHV.RISK_INDX_VAL_Y0, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y0, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y0, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y0, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y0, 0)),
                    (  POLE.RISK_INDX_VAL_Y1
                     + NVL (XARMHV.RISK_INDX_VAL_Y1, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y1, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y1, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y1, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y1, 0)),
                    (  POLE.RISK_INDX_VAL_Y2
                     + NVL (XARMHV.RISK_INDX_VAL_Y2, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y2, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y2, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y2, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y2, 0)),
                    (  POLE.RISK_INDX_VAL_Y3
                     + NVL (XARMHV.RISK_INDX_VAL_Y3, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y3, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y3, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y3, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y3, 0)),
                    (  POLE.RISK_INDX_VAL_Y4
                     + NVL (XARMHV.RISK_INDX_VAL_Y4, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y4, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y4, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y4, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y4, 0)),
                    (  POLE.RISK_INDX_VAL_Y5
                     + NVL (XARMHV.RISK_INDX_VAL_Y5, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y5, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y5, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y5, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y5, 0)),
                    (  POLE.RISK_INDX_VAL_Y6
                     + NVL (XARMHV.RISK_INDX_VAL_Y6, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y6, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y6, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y6, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y6, 0)),
                    (  POLE.RISK_INDX_VAL_Y7
                     + NVL (XARMHV.RISK_INDX_VAL_Y7, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y7, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y7, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y7, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y7, 0)),
                    (  POLE.RISK_INDX_VAL_Y8
                     + NVL (XARMHV.RISK_INDX_VAL_Y8, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y8, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y8, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y8, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y8, 0)),
                    (  POLE.RISK_INDX_VAL_Y9
                     + NVL (XARMHV.RISK_INDX_VAL_Y9, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y9, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y9, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y9, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y9, 0)),
                    (  POLE.RISK_INDX_VAL_Y10
                     + NVL (XARMHV.RISK_INDX_VAL_Y10, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y10, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y10, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y10, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y10, 0)),
                    (  POLE.RISK_INDX_VAL_Y11
                     + NVL (XARMHV.RISK_INDX_VAL_Y11, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y11, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y11, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y11, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y11, 0)),
                    (  POLE.RISK_INDX_VAL_Y12
                     + NVL (XARMHV.RISK_INDX_VAL_Y12, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y12, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y12, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y12, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y12, 0)),
                    (  POLE.RISK_INDX_VAL_Y13
                     + NVL (XARMHV.RISK_INDX_VAL_Y13, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y13, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y13, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y13, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y13, 0)),
                    (  POLE.RISK_INDX_VAL_Y14
                     + NVL (XARMHV.RISK_INDX_VAL_Y14, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y14, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y14, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y14, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y14, 0)),
                    (  POLE.RISK_INDX_VAL_Y15
                     + NVL (XARMHV.RISK_INDX_VAL_Y15, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y15, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y15, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y15, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y15, 0)),
                    (  POLE.RISK_INDX_VAL_Y16
                     + NVL (XARMHV.RISK_INDX_VAL_Y16, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y16, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y16, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y16, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y16, 0)),
                    (  POLE.RISK_INDX_VAL_Y17
                     + NVL (XARMHV.RISK_INDX_VAL_Y17, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y17, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y17, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y17, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y17, 0)),
                    (  POLE.RISK_INDX_VAL_Y18
                     + NVL (XARMHV.RISK_INDX_VAL_Y18, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y18, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y18, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y18, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y18, 0)),
                    (  POLE.RISK_INDX_VAL_Y19
                     + NVL (XARMHV.RISK_INDX_VAL_Y19, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y19, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y19, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y19, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y19, 0)),
                    (  POLE.RISK_INDX_VAL_Y20
                     + NVL (XARMHV.RISK_INDX_VAL_Y20, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y20, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y20, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y20, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y20, 0)),
                    (  POLE.RISK_INDX_VAL_Y21
                     + NVL (XARMHV.RISK_INDX_VAL_Y21, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y21, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y21, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y21, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y21, 0)),
                    (  POLE.RISK_INDX_VAL_Y22
                     + NVL (XARMHV.RISK_INDX_VAL_Y22, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y22, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y22, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y22, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y22, 0)),
                    (  POLE.RISK_INDX_VAL_Y23
                     + NVL (XARMHV.RISK_INDX_VAL_Y23, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y23, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y23, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y23, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y23, 0)),
                    (  POLE.RISK_INDX_VAL_Y24
                     + NVL (XARMHV.RISK_INDX_VAL_Y24, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y24, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y24, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y24, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y24, 0)),
                    (  POLE.RISK_INDX_VAL_Y25
                     + NVL (XARMHV.RISK_INDX_VAL_Y25, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y25, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y25, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y25, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y25, 0)),
                    (  POLE.RISK_INDX_VAL_Y26
                     + NVL (XARMHV.RISK_INDX_VAL_Y26, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y26, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y26, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y26, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y26, 0)),
                    (  POLE.RISK_INDX_VAL_Y27
                     + NVL (XARMHV.RISK_INDX_VAL_Y27, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y27, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y27, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y27, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y27, 0)),
                    (  POLE.RISK_INDX_VAL_Y28
                     + NVL (XARMHV.RISK_INDX_VAL_Y28, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y28, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y28, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y28, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y28, 0)),
                    (  POLE.RISK_INDX_VAL_Y29
                     + NVL (XARMHV.RISK_INDX_VAL_Y29, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y29, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y29, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y29, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y29, 0)),
                    (  POLE.RISK_INDX_VAL_Y30
                     + NVL (XARMHV.RISK_INDX_VAL_Y30, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y30, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y30, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y30, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y30, 0)),
                    (  POLE.RISK_INDX_VAL_Y31
                     + NVL (XARMHV.RISK_INDX_VAL_Y31, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y31, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y31, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y31, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y31, 0)),
                    (  POLE.RISK_INDX_VAL_Y32
                     + NVL (XARMHV.RISK_INDX_VAL_Y32, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y32, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y32, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y32, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y32, 0)),
                    (  POLE.RISK_INDX_VAL_Y33
                     + NVL (XARMHV.RISK_INDX_VAL_Y33, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y33, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y33, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y33, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y33, 0)),
                    (  POLE.RISK_INDX_VAL_Y34
                     + NVL (XARMHV.RISK_INDX_VAL_Y34, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y34, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y34, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y34, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y34, 0)),
                    (  POLE.RISK_INDX_VAL_Y35
                     + NVL (XARMHV.RISK_INDX_VAL_Y35, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y35, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y35, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y35, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y35, 0)),
                    (  POLE.RISK_INDX_VAL_Y36
                     + NVL (XARMHV.RISK_INDX_VAL_Y36, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y36, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y36, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y36, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y36, 0)),
                    (  POLE.RISK_INDX_VAL_Y37
                     + NVL (XARMHV.RISK_INDX_VAL_Y37, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y37, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y37, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y37, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y37, 0)),
                    (  POLE.RISK_INDX_VAL_Y38
                     + NVL (XARMHV.RISK_INDX_VAL_Y38, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y38, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y38, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y38, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y38, 0)),
                    (  POLE.RISK_INDX_VAL_Y39
                     + NVL (XARMHV.RISK_INDX_VAL_Y39, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y39, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y39, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y39, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y39, 0)),
                    (  POLE.RISK_INDX_VAL_Y40
                     + NVL (XARMHV.RISK_INDX_VAL_Y40, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y40, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y40, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y40, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y40, 0)),
                    (  POLE.RISK_INDX_VAL_Y41
                     + NVL (XARMHV.RISK_INDX_VAL_Y41, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y41, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y41, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y41, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y41, 0)),
                    (  POLE.RISK_INDX_VAL_Y42
                     + NVL (XARMHV.RISK_INDX_VAL_Y42, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y42, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y42, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y42, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y42, 0)),
                    (  POLE.RISK_INDX_VAL_Y43
                     + NVL (XARMHV.RISK_INDX_VAL_Y43, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y43, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y43, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y43, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y43, 0)),
                    (  POLE.RISK_INDX_VAL_Y44
                     + NVL (XARMHV.RISK_INDX_VAL_Y44, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y44, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y44, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y44, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y44, 0)),
                    (  POLE.RISK_INDX_VAL_Y45
                     + NVL (XARMHV.RISK_INDX_VAL_Y45, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y45, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y45, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y45, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y45, 0)),
                    (  POLE.RISK_INDX_VAL_Y46
                     + NVL (XARMHV.RISK_INDX_VAL_Y46, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y46, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y46, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y46, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y46, 0)),
                    (  POLE.RISK_INDX_VAL_Y47
                     + NVL (XARMHV.RISK_INDX_VAL_Y47, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y47, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y47, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y47, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y47, 0)),
                    (  POLE.RISK_INDX_VAL_Y48
                     + NVL (XARMHV.RISK_INDX_VAL_Y48, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y48, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y48, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y48, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y48, 0)),
                    (  POLE.RISK_INDX_VAL_Y49
                     + NVL (XARMHV.RISK_INDX_VAL_Y49, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y49, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y49, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y49, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y49, 0)),
                    (  POLE.RISK_INDX_VAL_Y50
                     + NVL (XARMHV.RISK_INDX_VAL_Y50, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y50, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y50, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y50, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y50, 0))
               FROM structools.ST_RISK_POLE_REINF pole,
                    structools.ST_RISK_XARMHV_NOACTION xarmhv,
                    structools.ST_RISK_XARMLV_NOACTION xarmlv,
                    structools.ST_RISK_INSUL_NOACTION insul,
                    structools.ST_RISK_NWPOLE_NOACTION nwpole,
                    structools.ST_RISK_STAY_NOACTION stay
              WHERE     POLE.PICK_ID = XARMHV.PICK_ID(+)
                    AND POLE.PICK_ID = XARMLV.PICK_ID(+)
                    AND POLE.PICK_ID = INSUL.PICK_ID(+)
                    AND POLE.PICK_ID = NWPOLE.PICK_ID(+)
                    AND POLE.PICK_ID = STAY.PICK_ID(+) --AND pole.pick_id IN ('307', '511')
                                                      );

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - PWOD (Combined) REINFORCE');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING risk records IN ST_RISK_PWOD_REINF -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;


      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_PWOD_REINF_NPV15
            (SELECT POLE.PICK_ID,
                    (  POLE.RISK_INDX_VAL_Y0
                     + NVL (XARMHV.RISK_INDX_VAL_Y0, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y0, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y0, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y0, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y0, 0)),
                    (  POLE.RISK_INDX_VAL_Y1
                     + NVL (XARMHV.RISK_INDX_VAL_Y1, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y1, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y1, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y1, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y1, 0)),
                    (  POLE.RISK_INDX_VAL_Y2
                     + NVL (XARMHV.RISK_INDX_VAL_Y2, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y2, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y2, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y2, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y2, 0)),
                    (  POLE.RISK_INDX_VAL_Y3
                     + NVL (XARMHV.RISK_INDX_VAL_Y3, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y3, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y3, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y3, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y3, 0)),
                    (  POLE.RISK_INDX_VAL_Y4
                     + NVL (XARMHV.RISK_INDX_VAL_Y4, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y4, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y4, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y4, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y4, 0)),
                    (  POLE.RISK_INDX_VAL_Y5
                     + NVL (XARMHV.RISK_INDX_VAL_Y5, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y5, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y5, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y5, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y5, 0)),
                    (  POLE.RISK_INDX_VAL_Y6
                     + NVL (XARMHV.RISK_INDX_VAL_Y6, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y6, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y6, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y6, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y6, 0)),
                    (  POLE.RISK_INDX_VAL_Y7
                     + NVL (XARMHV.RISK_INDX_VAL_Y7, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y7, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y7, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y7, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y7, 0)),
                    (  POLE.RISK_INDX_VAL_Y8
                     + NVL (XARMHV.RISK_INDX_VAL_Y8, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y8, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y8, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y8, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y8, 0)),
                    (  POLE.RISK_INDX_VAL_Y9
                     + NVL (XARMHV.RISK_INDX_VAL_Y9, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y9, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y9, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y9, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y9, 0)),
                    (  POLE.RISK_INDX_VAL_Y10
                     + NVL (XARMHV.RISK_INDX_VAL_Y10, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y10, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y10, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y10, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y10, 0)),
                    (  POLE.RISK_INDX_VAL_Y11
                     + NVL (XARMHV.RISK_INDX_VAL_Y11, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y11, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y11, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y11, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y11, 0)),
                    (  POLE.RISK_INDX_VAL_Y12
                     + NVL (XARMHV.RISK_INDX_VAL_Y12, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y12, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y12, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y12, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y12, 0)),
                    (  POLE.RISK_INDX_VAL_Y13
                     + NVL (XARMHV.RISK_INDX_VAL_Y13, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y13, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y13, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y13, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y13, 0)),
                    (  POLE.RISK_INDX_VAL_Y14
                     + NVL (XARMHV.RISK_INDX_VAL_Y14, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y14, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y14, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y14, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y14, 0)),
                    (  POLE.RISK_INDX_VAL_Y15
                     + NVL (XARMHV.RISK_INDX_VAL_Y15, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y15, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y15, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y15, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y15, 0)),
                    (  POLE.RISK_INDX_VAL_Y16
                     + NVL (XARMHV.RISK_INDX_VAL_Y16, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y16, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y16, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y16, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y16, 0)),
                    (  POLE.RISK_INDX_VAL_Y17
                     + NVL (XARMHV.RISK_INDX_VAL_Y17, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y17, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y17, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y17, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y17, 0)),
                    (  POLE.RISK_INDX_VAL_Y18
                     + NVL (XARMHV.RISK_INDX_VAL_Y18, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y18, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y18, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y18, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y18, 0)),
                    (  POLE.RISK_INDX_VAL_Y19
                     + NVL (XARMHV.RISK_INDX_VAL_Y19, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y19, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y19, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y19, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y19, 0)),
                    (  POLE.RISK_INDX_VAL_Y20
                     + NVL (XARMHV.RISK_INDX_VAL_Y20, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y20, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y20, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y20, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y20, 0)),
                    (  POLE.RISK_INDX_VAL_Y21
                     + NVL (XARMHV.RISK_INDX_VAL_Y21, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y21, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y21, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y21, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y21, 0)),
                    (  POLE.RISK_INDX_VAL_Y22
                     + NVL (XARMHV.RISK_INDX_VAL_Y22, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y22, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y22, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y22, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y22, 0)),
                    (  POLE.RISK_INDX_VAL_Y23
                     + NVL (XARMHV.RISK_INDX_VAL_Y23, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y23, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y23, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y23, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y23, 0)),
                    (  POLE.RISK_INDX_VAL_Y24
                     + NVL (XARMHV.RISK_INDX_VAL_Y24, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y24, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y24, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y24, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y24, 0)),
                    (  POLE.RISK_INDX_VAL_Y25
                     + NVL (XARMHV.RISK_INDX_VAL_Y25, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y25, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y25, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y25, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y25, 0)),
                    (  POLE.RISK_INDX_VAL_Y26
                     + NVL (XARMHV.RISK_INDX_VAL_Y26, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y26, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y26, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y26, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y26, 0)),
                    (  POLE.RISK_INDX_VAL_Y27
                     + NVL (XARMHV.RISK_INDX_VAL_Y27, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y27, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y27, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y27, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y27, 0)),
                    (  POLE.RISK_INDX_VAL_Y28
                     + NVL (XARMHV.RISK_INDX_VAL_Y28, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y28, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y28, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y28, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y28, 0)),
                    (  POLE.RISK_INDX_VAL_Y29
                     + NVL (XARMHV.RISK_INDX_VAL_Y29, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y29, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y29, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y29, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y29, 0)),
                    (  POLE.RISK_INDX_VAL_Y30
                     + NVL (XARMHV.RISK_INDX_VAL_Y30, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y30, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y30, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y30, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y30, 0)),
                    (  POLE.RISK_INDX_VAL_Y31
                     + NVL (XARMHV.RISK_INDX_VAL_Y31, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y31, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y31, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y31, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y31, 0)),
                    (  POLE.RISK_INDX_VAL_Y32
                     + NVL (XARMHV.RISK_INDX_VAL_Y32, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y32, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y32, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y32, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y32, 0)),
                    (  POLE.RISK_INDX_VAL_Y33
                     + NVL (XARMHV.RISK_INDX_VAL_Y33, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y33, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y33, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y33, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y33, 0)),
                    (  POLE.RISK_INDX_VAL_Y34
                     + NVL (XARMHV.RISK_INDX_VAL_Y34, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y34, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y34, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y34, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y34, 0)),
                    (  POLE.RISK_INDX_VAL_Y35
                     + NVL (XARMHV.RISK_INDX_VAL_Y35, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y35, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y35, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y35, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y35, 0)),
                    (  POLE.RISK_INDX_VAL_Y36
                     + NVL (XARMHV.RISK_INDX_VAL_Y36, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y36, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y36, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y36, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y36, 0)),
                    (  POLE.RISK_INDX_VAL_Y37
                     + NVL (XARMHV.RISK_INDX_VAL_Y37, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y37, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y37, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y37, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y37, 0)),
                    (  POLE.RISK_INDX_VAL_Y38
                     + NVL (XARMHV.RISK_INDX_VAL_Y38, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y38, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y38, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y38, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y38, 0)),
                    (  POLE.RISK_INDX_VAL_Y39
                     + NVL (XARMHV.RISK_INDX_VAL_Y39, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y39, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y39, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y39, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y39, 0)),
                    (  POLE.RISK_INDX_VAL_Y40
                     + NVL (XARMHV.RISK_INDX_VAL_Y40, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y40, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y40, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y40, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y40, 0)),
                    (  POLE.RISK_INDX_VAL_Y41
                     + NVL (XARMHV.RISK_INDX_VAL_Y41, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y41, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y41, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y41, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y41, 0)),
                    (  POLE.RISK_INDX_VAL_Y42
                     + NVL (XARMHV.RISK_INDX_VAL_Y42, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y42, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y42, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y42, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y42, 0)),
                    (  POLE.RISK_INDX_VAL_Y43
                     + NVL (XARMHV.RISK_INDX_VAL_Y43, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y43, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y43, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y43, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y43, 0)),
                    (  POLE.RISK_INDX_VAL_Y44
                     + NVL (XARMHV.RISK_INDX_VAL_Y44, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y44, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y44, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y44, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y44, 0)),
                    (  POLE.RISK_INDX_VAL_Y45
                     + NVL (XARMHV.RISK_INDX_VAL_Y45, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y45, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y45, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y45, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y45, 0)),
                    (  POLE.RISK_INDX_VAL_Y46
                     + NVL (XARMHV.RISK_INDX_VAL_Y46, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y46, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y46, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y46, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y46, 0)),
                    (  POLE.RISK_INDX_VAL_Y47
                     + NVL (XARMHV.RISK_INDX_VAL_Y47, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y47, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y47, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y47, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y47, 0)),
                    (  POLE.RISK_INDX_VAL_Y48
                     + NVL (XARMHV.RISK_INDX_VAL_Y48, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y48, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y48, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y48, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y48, 0)),
                    (  POLE.RISK_INDX_VAL_Y49
                     + NVL (XARMHV.RISK_INDX_VAL_Y49, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y49, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y49, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y49, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y49, 0)),
                    (  POLE.RISK_INDX_VAL_Y50
                     + NVL (XARMHV.RISK_INDX_VAL_Y50, 0)
                     + NVL (XARMLV.RISK_INDX_VAL_Y50, 0)
                     + NVL (INSUL.RISK_INDX_VAL_Y50, 0)
                     + NVL (NWPOLE.RISK_INDX_VAL_Y50, 0)
                     + NVL (STAY.RISK_INDX_VAL_Y50, 0))
               FROM structools.ST_RISK_POLE_REINF_NPV15 pole,
                    structools.ST_RISK_XARMHV_NOACTION_NPV15 xarmhv,
                    structools.ST_RISK_XARMLV_NOACTION_NPV15 xarmlv,
                    structools.ST_RISK_INSUL_NOACTION_NPV15 insul,
                    structools.ST_RISK_NWPOLE_NOACTION_NPV15 nwpoLe,
                    structools.ST_RISK_STAY_NOACTION_NPV15 stay
              WHERE     POLE.PICK_ID = XARMHV.PICK_ID(+)
                    AND POLE.PICK_ID = XARMLV.PICK_ID(+)
                    AND POLE.PICK_ID = INSUL.PICK_ID(+)
                    AND POLE.PICK_ID = NWPOLE.PICK_ID(+)
                    AND POLE.PICK_ID = STAY.PICK_ID(+) --AND pole.pick_id IN ('307', '511')
                                                      );

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - PWOD (Combined) REINFORCE NPV15');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN NPV TABLE ST_RISK_PWOD_REINF_NPV15 -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_PWOD_REINF');
      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_PWOD_REINF_NPV15');

   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END PR_LOAD_RISK_PWOD;

   PROCEDURE PR_LOAD_RISK_XARMHVI
   (p_yr_mth_no  IN    CHAR )
   AS
      v_record_cnt   NUMBER := 0;
      v_xarmhv_fin_conseq_amt NUMBER;
      v_insul_fin_conseq_amt  NUMBER;

      CURSOR get_reactv_cost_cur
      IS
         SELECT TRIM(mdl_bus_cde) AS mdl_bus_cde
               ,trmnt_reactv_cst_amt
         --FROM structoolsw.treatment_reactive_cost
         FROM structools.st_treatment_reactive_cost
      ;

   BEGIN
      DBMS_OUTPUT.put_line (
            'Risk MODEL - XARMHV (Combined)_DONOTHING Start TIME -'
         || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));

      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMHVI_NOACTION');
      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMHVI_NOACTION_NPV15');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMHVI_NOACTION');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMHVI_NOACTION_NPV15');

      DELETE FROM structools.ST_RISK_FRACTIONS frac
            WHERE FRAC.MODEL_NAME = 'XARMHVI';

      COMMIT;

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMHVI_NOACTION
             SELECT XARMHV.PICK_ID,
                    XARMHV.RISK_INDX_VAL_Y0 + NVL (INSUL.RISK_INDX_VAL_Y0, 0),
                    XARMHV.RISK_INDX_VAL_Y1 + NVL (INSUL.RISK_INDX_VAL_Y1, 0),
                    XARMHV.RISK_INDX_VAL_Y2 + NVL (INSUL.RISK_INDX_VAL_Y2, 0),
                    XARMHV.RISK_INDX_VAL_Y3 + NVL (INSUL.RISK_INDX_VAL_Y3, 0),
                    XARMHV.RISK_INDX_VAL_Y4 + NVL (INSUL.RISK_INDX_VAL_Y4, 0),
                    XARMHV.RISK_INDX_VAL_Y5 + NVL (INSUL.RISK_INDX_VAL_Y5, 0),
                    XARMHV.RISK_INDX_VAL_Y6 + NVL (INSUL.RISK_INDX_VAL_Y6, 0),
                    XARMHV.RISK_INDX_VAL_Y7 + NVL (INSUL.RISK_INDX_VAL_Y7, 0),
                    XARMHV.RISK_INDX_VAL_Y8 + NVL (INSUL.RISK_INDX_VAL_Y8, 0),
                    XARMHV.RISK_INDX_VAL_Y9 + NVL (INSUL.RISK_INDX_VAL_Y9, 0),
                    XARMHV.RISK_INDX_VAL_Y10 + NVL (INSUL.RISK_INDX_VAL_Y10, 0),
                    XARMHV.RISK_INDX_VAL_Y11 + NVL (INSUL.RISK_INDX_VAL_Y11, 0),
                    XARMHV.RISK_INDX_VAL_Y12 + NVL (INSUL.RISK_INDX_VAL_Y12, 0),
                    XARMHV.RISK_INDX_VAL_Y13 + NVL (INSUL.RISK_INDX_VAL_Y13, 0),
                    XARMHV.RISK_INDX_VAL_Y14 + NVL (INSUL.RISK_INDX_VAL_Y14, 0),
                    XARMHV.RISK_INDX_VAL_Y15 + NVL (INSUL.RISK_INDX_VAL_Y15, 0),
                    XARMHV.RISK_INDX_VAL_Y16 + NVL (INSUL.RISK_INDX_VAL_Y16, 0),
                    XARMHV.RISK_INDX_VAL_Y17 + NVL (INSUL.RISK_INDX_VAL_Y17, 0),
                    XARMHV.RISK_INDX_VAL_Y18 + NVL (INSUL.RISK_INDX_VAL_Y18, 0),
                    XARMHV.RISK_INDX_VAL_Y19 + NVL (INSUL.RISK_INDX_VAL_Y19, 0),
                    XARMHV.RISK_INDX_VAL_Y20 + NVL (INSUL.RISK_INDX_VAL_Y20, 0),
                    XARMHV.RISK_INDX_VAL_Y21 + NVL (INSUL.RISK_INDX_VAL_Y21, 0),
                    XARMHV.RISK_INDX_VAL_Y22 + NVL (INSUL.RISK_INDX_VAL_Y22, 0),
                    XARMHV.RISK_INDX_VAL_Y23 + NVL (INSUL.RISK_INDX_VAL_Y23, 0),
                    XARMHV.RISK_INDX_VAL_Y24 + NVL (INSUL.RISK_INDX_VAL_Y24, 0),
                    XARMHV.RISK_INDX_VAL_Y25 + NVL (INSUL.RISK_INDX_VAL_Y25, 0),
                    XARMHV.RISK_INDX_VAL_Y26 + NVL (INSUL.RISK_INDX_VAL_Y26, 0),
                    XARMHV.RISK_INDX_VAL_Y27 + NVL (INSUL.RISK_INDX_VAL_Y27, 0),
                    XARMHV.RISK_INDX_VAL_Y28 + NVL (INSUL.RISK_INDX_VAL_Y28, 0),
                    XARMHV.RISK_INDX_VAL_Y29 + NVL (INSUL.RISK_INDX_VAL_Y29, 0),
                    XARMHV.RISK_INDX_VAL_Y30 + NVL (INSUL.RISK_INDX_VAL_Y30, 0),
                    XARMHV.RISK_INDX_VAL_Y31 + NVL (INSUL.RISK_INDX_VAL_Y31, 0),
                    XARMHV.RISK_INDX_VAL_Y32 + NVL (INSUL.RISK_INDX_VAL_Y32, 0),
                    XARMHV.RISK_INDX_VAL_Y33 + NVL (INSUL.RISK_INDX_VAL_Y33, 0),
                    XARMHV.RISK_INDX_VAL_Y34 + NVL (INSUL.RISK_INDX_VAL_Y34, 0),
                    XARMHV.RISK_INDX_VAL_Y35 + NVL (INSUL.RISK_INDX_VAL_Y35, 0),
                    XARMHV.RISK_INDX_VAL_Y36 + NVL (INSUL.RISK_INDX_VAL_Y36, 0),
                    XARMHV.RISK_INDX_VAL_Y37 + NVL (INSUL.RISK_INDX_VAL_Y37, 0),
                    XARMHV.RISK_INDX_VAL_Y38 + NVL (INSUL.RISK_INDX_VAL_Y38, 0),
                    XARMHV.RISK_INDX_VAL_Y39 + NVL (INSUL.RISK_INDX_VAL_Y39, 0),
                    XARMHV.RISK_INDX_VAL_Y40 + NVL (INSUL.RISK_INDX_VAL_Y40, 0),
                    XARMHV.RISK_INDX_VAL_Y41 + NVL (INSUL.RISK_INDX_VAL_Y41, 0),
                    XARMHV.RISK_INDX_VAL_Y42 + NVL (INSUL.RISK_INDX_VAL_Y42, 0),
                    XARMHV.RISK_INDX_VAL_Y43 + NVL (INSUL.RISK_INDX_VAL_Y43, 0),
                    XARMHV.RISK_INDX_VAL_Y44 + NVL (INSUL.RISK_INDX_VAL_Y44, 0),
                    XARMHV.RISK_INDX_VAL_Y45 + NVL (INSUL.RISK_INDX_VAL_Y45, 0),
                    XARMHV.RISK_INDX_VAL_Y46 + NVL (INSUL.RISK_INDX_VAL_Y46, 0),
                    XARMHV.RISK_INDX_VAL_Y47 + NVL (INSUL.RISK_INDX_VAL_Y47, 0),
                    XARMHV.RISK_INDX_VAL_Y48 + NVL (INSUL.RISK_INDX_VAL_Y48, 0),
                    XARMHV.RISK_INDX_VAL_Y49 + NVL (INSUL.RISK_INDX_VAL_Y49, 0),
                    XARMHV.RISK_INDX_VAL_Y50 + NVL (INSUL.RISK_INDX_VAL_Y50, 0)
               FROM structools.ST_RISK_XARMHV_NOACTION xarmhv  LEFT OUTER JOIN
                    structools.ST_RISK_INSUL_NOACTION insul
               ON xarmhv.pick_id = insul.pick_id
            ;

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - XARMHVI (Combined) DONOTHING');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING risk records IN ST_RISK_XARMHVI_NOACTION -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      /* load reactive cost of all  models to an associative array, indexed by the model name */
      /* This only needs to be done if this procedure is invoked on its own and not as part of PR_LOAD_ALL_RISK_TABLES procedure */
      IF v_reac_cost_tab.COUNT = 0 THEN
         FOR c IN get_reactv_cost_cur LOOP
            v_reac_cost_tab(c.mdl_bus_cde) := c.trmnt_reactv_cst_amt;
         END LOOP;   
      END IF;

      v_xarmhv_fin_conseq_amt := v_reac_cost_tab('XARMHV');
      v_insul_fin_conseq_amt  := v_reac_cost_tab('INSUL');

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMHVI_NOACTION_NPV15
            WITH conseq_calc AS   --fin_frac - copy from here to the end of the sql and replace atm.mdl_bus_cde values
            (
            SELECT 
               (CASE
                   WHEN instrc (ed.plnt_no,
                                ' ',
                                1,
                                1) = 0
                   THEN
                      SUBSTR (ed.plnt_no,
                              -1 * (LENGTH (ed.plnt_no) - 1))
                   ELSE
                      SUBSTR (ed.plnt_no,
                              2,
                              instrc (ed.plnt_no,
                                      ' ',
                                      1,
                                      1) - 2)
                END) AS pick_id
               ,nrdmf.hzrd_fn_1_2
               ,nrdmf.FIRE_CNSQNC_AMT
               ,nrdmf.ELEC_SHCK_CNSQNC_AMT
               ,nrdmf.WRK_FRCE_CNSQNC_AMT
               ,nrdmf.PHYS_IMPCT_CNSQNC_AMT
               ,nrdmf.RELIABILITY_CNSQNC_AMT
               ,nrdmf.ENV_CNSQNC_AMT
               ,CASE
                  WHEN atm.mdl_bus_cde = 'XARMHV'        THEN v_xarmhv_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'INSUL'         THEN v_insul_fin_conseq_amt
               END              AS FIN_CNSQNC_AMT 
               ,CASE                                                                      --fin_fracc - replace
                  WHEN atm.mdl_bus_cde = 'XARMHV'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_xarmhv_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'INSUL'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_insul_fin_conseq_amt
               END              AS conseq_amt 
            FROM structoolsw.mthly_equip_risk_dm_fact nrdmf
               JOIN
               structoolsw.nrm_asset_type_model_dim atm
                  ON atm.nrm_asset_typ_mdl_sk =
                        nrdmf.nrm_asset_typ_mdl_sk
               JOIN structoolsw.treatment_dim td
                  ON td.trmnt_sk = nrdmf.trmnt_sk
               JOIN structoolsw.equipment_nvgtn_dim en
                  ON en.equip_sk = nrdmf.equip_sk
               JOIN structoolsw.equipment_dim ed
                  ON ed.equip_nvgtn_sk = en.equip_nvgtn_sk
            WHERE     nrdmf.yr_mth_no = p_yr_mth_no
                  AND td.trmnt_nam = 'DONOTHING'
                  AND atm.mdl_bus_cde IN
                         (
                          'XARMHV',
                          'INSUL')
                  --AND ed.pick_id = 'S318'
            )
            ,
            frac AS
            (  
            SELECT   pick_id
                    ,SUM (FIRE_CNSQNC_AMT * hzrd_fn_1_2)          /  SUM (hzrd_fn_1_2 * conseq_amt) AS fire_frac
                    ,SUM (ELEC_SHCK_CNSQNC_AMT * hzrd_fn_1_2)     /  SUM (hzrd_fn_1_2 * conseq_amt) AS ESHOCK_frac
                    ,SUM (WRK_FRCE_CNSQNC_AMT * hzrd_fn_1_2)      /  SUM (hzrd_fn_1_2 * conseq_amt) AS WFORCE_frac
                    ,SUM (RELIABILITY_CNSQNC_AMT * hzrd_fn_1_2)   /  SUM (hzrd_fn_1_2 * conseq_amt) AS RELIABILITY_frac
                    ,SUM (PHYS_IMPCT_CNSQNC_AMT * hzrd_fn_1_2)    /  SUM (hzrd_fn_1_2 * conseq_amt) AS PHYSIMP_frac
                    ,SUM (ENV_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS ENV_frac
                    ,SUM (FIN_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS FIN_frac      
             FROM conseq_calc
            GROUP BY pick_id
            )
            SELECT XARMHV.PICK_ID,
                    XARMHV.RISK_INDX_VAL_Y0 + NVL (INSUL.RISK_INDX_VAL_Y0, 0),
                    XARMHV.RISK_INDX_VAL_Y1 + NVL (INSUL.RISK_INDX_VAL_Y1, 0),
                    XARMHV.RISK_INDX_VAL_Y2 + NVL (INSUL.RISK_INDX_VAL_Y2, 0),
                    XARMHV.RISK_INDX_VAL_Y3 + NVL (INSUL.RISK_INDX_VAL_Y3, 0),
                    XARMHV.RISK_INDX_VAL_Y4 + NVL (INSUL.RISK_INDX_VAL_Y4, 0),
                    XARMHV.RISK_INDX_VAL_Y5 + NVL (INSUL.RISK_INDX_VAL_Y5, 0),
                    XARMHV.RISK_INDX_VAL_Y6 + NVL (INSUL.RISK_INDX_VAL_Y6, 0),
                    XARMHV.RISK_INDX_VAL_Y7 + NVL (INSUL.RISK_INDX_VAL_Y7, 0),
                    XARMHV.RISK_INDX_VAL_Y8 + NVL (INSUL.RISK_INDX_VAL_Y8, 0),
                    XARMHV.RISK_INDX_VAL_Y9 + NVL (INSUL.RISK_INDX_VAL_Y9, 0),
                    XARMHV.RISK_INDX_VAL_Y10 + NVL (INSUL.RISK_INDX_VAL_Y10, 0),
                    XARMHV.RISK_INDX_VAL_Y11 + NVL (INSUL.RISK_INDX_VAL_Y11, 0),
                    XARMHV.RISK_INDX_VAL_Y12 + NVL (INSUL.RISK_INDX_VAL_Y12, 0),
                    XARMHV.RISK_INDX_VAL_Y13 + NVL (INSUL.RISK_INDX_VAL_Y13, 0),
                    XARMHV.RISK_INDX_VAL_Y14 + NVL (INSUL.RISK_INDX_VAL_Y14, 0),
                    XARMHV.RISK_INDX_VAL_Y15 + NVL (INSUL.RISK_INDX_VAL_Y15, 0),
                    XARMHV.RISK_INDX_VAL_Y16 + NVL (INSUL.RISK_INDX_VAL_Y16, 0),
                    XARMHV.RISK_INDX_VAL_Y17 + NVL (INSUL.RISK_INDX_VAL_Y17, 0),
                    XARMHV.RISK_INDX_VAL_Y18 + NVL (INSUL.RISK_INDX_VAL_Y18, 0),
                    XARMHV.RISK_INDX_VAL_Y19 + NVL (INSUL.RISK_INDX_VAL_Y19, 0),
                    XARMHV.RISK_INDX_VAL_Y20 + NVL (INSUL.RISK_INDX_VAL_Y20, 0),
                    XARMHV.RISK_INDX_VAL_Y21 + NVL (INSUL.RISK_INDX_VAL_Y21, 0),
                    XARMHV.RISK_INDX_VAL_Y22 + NVL (INSUL.RISK_INDX_VAL_Y22, 0),
                    XARMHV.RISK_INDX_VAL_Y23 + NVL (INSUL.RISK_INDX_VAL_Y23, 0),
                    XARMHV.RISK_INDX_VAL_Y24 + NVL (INSUL.RISK_INDX_VAL_Y24, 0),
                    XARMHV.RISK_INDX_VAL_Y25 + NVL (INSUL.RISK_INDX_VAL_Y25, 0),
                    XARMHV.RISK_INDX_VAL_Y26 + NVL (INSUL.RISK_INDX_VAL_Y26, 0),
                    XARMHV.RISK_INDX_VAL_Y27 + NVL (INSUL.RISK_INDX_VAL_Y27, 0),
                    XARMHV.RISK_INDX_VAL_Y28 + NVL (INSUL.RISK_INDX_VAL_Y28, 0),
                    XARMHV.RISK_INDX_VAL_Y29 + NVL (INSUL.RISK_INDX_VAL_Y29, 0),
                    XARMHV.RISK_INDX_VAL_Y30 + NVL (INSUL.RISK_INDX_VAL_Y30, 0),
                    XARMHV.RISK_INDX_VAL_Y31 + NVL (INSUL.RISK_INDX_VAL_Y31, 0),
                    XARMHV.RISK_INDX_VAL_Y32 + NVL (INSUL.RISK_INDX_VAL_Y32, 0),
                    XARMHV.RISK_INDX_VAL_Y33 + NVL (INSUL.RISK_INDX_VAL_Y33, 0),
                    XARMHV.RISK_INDX_VAL_Y34 + NVL (INSUL.RISK_INDX_VAL_Y34, 0),
                    XARMHV.RISK_INDX_VAL_Y35 + NVL (INSUL.RISK_INDX_VAL_Y35, 0),
                    XARMHV.RISK_INDX_VAL_Y36 + NVL (INSUL.RISK_INDX_VAL_Y36, 0),
                    XARMHV.RISK_INDX_VAL_Y37 + NVL (INSUL.RISK_INDX_VAL_Y37, 0),
                    XARMHV.RISK_INDX_VAL_Y38 + NVL (INSUL.RISK_INDX_VAL_Y38, 0),
                    XARMHV.RISK_INDX_VAL_Y39 + NVL (INSUL.RISK_INDX_VAL_Y39, 0),
                    XARMHV.RISK_INDX_VAL_Y40 + NVL (INSUL.RISK_INDX_VAL_Y40, 0),
                    XARMHV.RISK_INDX_VAL_Y41 + NVL (INSUL.RISK_INDX_VAL_Y41, 0),
                    XARMHV.RISK_INDX_VAL_Y42 + NVL (INSUL.RISK_INDX_VAL_Y42, 0),
                    XARMHV.RISK_INDX_VAL_Y43 + NVL (INSUL.RISK_INDX_VAL_Y43, 0),
                    XARMHV.RISK_INDX_VAL_Y44 + NVL (INSUL.RISK_INDX_VAL_Y44, 0),
                    XARMHV.RISK_INDX_VAL_Y45 + NVL (INSUL.RISK_INDX_VAL_Y45, 0),
                    XARMHV.RISK_INDX_VAL_Y46 + NVL (INSUL.RISK_INDX_VAL_Y46, 0),
                    XARMHV.RISK_INDX_VAL_Y47 + NVL (INSUL.RISK_INDX_VAL_Y47, 0),
                    XARMHV.RISK_INDX_VAL_Y48 + NVL (INSUL.RISK_INDX_VAL_Y48, 0),
                    XARMHV.RISK_INDX_VAL_Y49 + NVL (INSUL.RISK_INDX_VAL_Y49, 0),
                    XARMHV.RISK_INDX_VAL_Y50 + NVL (INSUL.RISK_INDX_VAL_Y50, 0),
                    frac.fire_frac,
                    frac.eshock_frac,
                    frac.wforce_frac,
                    frac.reliability_frac,
                    frac.physimp_frac,
                    frac.env_frac,
                    frac.fin_frac
                     FROM  structools.ST_RISK_XARMHV_NOACTION_NPV15 xarmhv     LEFT OUTER JOIN
                           structools.ST_RISK_INSUL_NOACTION_NPV15 insul
                           ON xarmhv.pick_id = insul.pick_id
                                                                           LEFT OUTER JOIN
                           frac
                           ON xarmhv.pick_id = frac.pick_id
                     --and ed.equip_no = '000001000022'--in ('000020132622', '000001824246')
                     --and ed.plnt_no = 'S307'
                     ;

         v_record_cnt := SQL%ROWCOUNT;

         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - XARMHVI (Combined) DONOTHING NPV15');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN NPV TABLE ST_RISK_XARMHVI_NOACTION_NPV15 -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      BEGIN
         INSERT INTO structools.ST_RISK_FRACTIONS
            WITH conseq_calc AS   --fin_frac - copy from here to the end of the sql and replace atm.mdl_bus_cde values
            (
            SELECT 'S' || (CASE
                      WHEN instrc (ed.plnt_no,
                                   ' ',
                                   1,
                                   1) = 0
                      THEN
                         SUBSTR (ed.plnt_no,
                                 -1 * (LENGTH (ed.plnt_no) - 1))
                      ELSE
                         SUBSTR (ed.plnt_no,
                                 2,
                                 instrc (ed.plnt_no,
                                         ' ',
                                         1,
                                         1) - 2)
                   END) AS pick_id
                  ,nrdmf.hzrd_fn_1_2
                  ,nrdmf.FIRE_CNSQNC_AMT
                  ,nrdmf.ELEC_SHCK_CNSQNC_AMT
                  ,nrdmf.WRK_FRCE_CNSQNC_AMT
                  ,nrdmf.PHYS_IMPCT_CNSQNC_AMT
                  ,nrdmf.RELIABILITY_CNSQNC_AMT
                  ,nrdmf.ENV_CNSQNC_AMT
                  ,CASE
                     WHEN atm.mdl_bus_cde = 'XARMHV'        THEN v_xarmhv_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'INSUL'         THEN v_insul_fin_conseq_amt
                  END              AS FIN_CNSQNC_AMT 
                  ,CASE                                                                      --fin_fracc - replace
                     WHEN atm.mdl_bus_cde = 'XARMHV'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_xarmhv_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'INSUL'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_insul_fin_conseq_amt
                  END              AS conseq_amt 
             FROM structoolsw.mthly_equip_risk_dm_fact nrdmf
                  JOIN
                  structoolsw.nrm_asset_type_model_dim atm
                     ON atm.nrm_asset_typ_mdl_sk =
                           nrdmf.nrm_asset_typ_mdl_sk
                  JOIN structoolsw.treatment_dim td
                     ON td.trmnt_sk = nrdmf.trmnt_sk
                  JOIN structoolsw.equipment_nvgtn_dim en
                     ON en.equip_sk = nrdmf.equip_sk
                  JOIN structoolsw.equipment_dim ed
                     ON ed.equip_nvgtn_sk = en.equip_nvgtn_sk
            WHERE     nrdmf.yr_mth_no = p_yr_mth_no
                  AND td.trmnt_nam = 'DONOTHING'
                  AND atm.mdl_bus_cde IN
                         ('XARMHV',
                          'INSUL')
                  --AND ed.pick_id = 'S318'
            )  
            SELECT   pick_id
                     ,'XARMHVI'                                    -- model_type
                     ,0                                          -- FIRE_INCD
                     ,0                                    -- ELEC_SHOCK_INCD
                     ,0                                    --LRGE_RLBLTY_INCD
                     ,0                            --ELEC_SHOCK_FATALITY_INCD
                     ,0                                 -- FIRE_FATALITY_INCD
                    ,SUM (FIRE_CNSQNC_AMT * hzrd_fn_1_2)          /  SUM (hzrd_fn_1_2 * conseq_amt) AS fire_frac
                    ,SUM (ELEC_SHCK_CNSQNC_AMT * hzrd_fn_1_2)     /  SUM (hzrd_fn_1_2 * conseq_amt) AS ESHOCK_frac
                    ,SUM (WRK_FRCE_CNSQNC_AMT * hzrd_fn_1_2)      /  SUM (hzrd_fn_1_2 * conseq_amt) AS WFORCE_frac
                    ,SUM (RELIABILITY_CNSQNC_AMT * hzrd_fn_1_2)   /  SUM (hzrd_fn_1_2 * conseq_amt) AS RELIABILITY_frac
                    ,SUM (PHYS_IMPCT_CNSQNC_AMT * hzrd_fn_1_2)    /  SUM (hzrd_fn_1_2 * conseq_amt) AS PHYSIMP_frac
                    ,SUM (ENV_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS ENV_frac
                    ,SUM (FIN_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS FIN_frac      
             FROM conseq_calc
             GROUP BY pick_id
            ;

         v_record_cnt := SQL%ROWCOUNT;

         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - XARMHVI (Combined) ST_RISK_FRACTIONS');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN TABLE ST_RISK_FRACTIONS FOR XARMHVI-'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMHVI_NOACTION');
      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMHVI_NOACTION_NPV15');

      DBMS_OUTPUT.put_line (
            'Risk MODEL - XARMHVI (Combined)_REPLACE Start TIME -'
         || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));

      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMHVI_REPLACE');
      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMHVI_REPLACE_NPV15');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMHVI_REPLACE');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMHVI_REPLACE_NPV15');
      
      COMMIT;

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMHVI_REPLACE
            SELECT XARMHV.PICK_ID,
                    XARMHV.RISK_INDX_VAL_Y0 + NVL (INSUL.RISK_INDX_VAL_Y0, 0),
                    XARMHV.RISK_INDX_VAL_Y1 + NVL (INSUL.RISK_INDX_VAL_Y1, 0),
                    XARMHV.RISK_INDX_VAL_Y2 + NVL (INSUL.RISK_INDX_VAL_Y2, 0),
                    XARMHV.RISK_INDX_VAL_Y3 + NVL (INSUL.RISK_INDX_VAL_Y3, 0),
                    XARMHV.RISK_INDX_VAL_Y4 + NVL (INSUL.RISK_INDX_VAL_Y4, 0),
                    XARMHV.RISK_INDX_VAL_Y5 + NVL (INSUL.RISK_INDX_VAL_Y5, 0),
                    XARMHV.RISK_INDX_VAL_Y6 + NVL (INSUL.RISK_INDX_VAL_Y6, 0),
                    XARMHV.RISK_INDX_VAL_Y7 + NVL (INSUL.RISK_INDX_VAL_Y7, 0),
                    XARMHV.RISK_INDX_VAL_Y8 + NVL (INSUL.RISK_INDX_VAL_Y8, 0),
                    XARMHV.RISK_INDX_VAL_Y9 + NVL (INSUL.RISK_INDX_VAL_Y9, 0),
                    XARMHV.RISK_INDX_VAL_Y10 + NVL (INSUL.RISK_INDX_VAL_Y10, 0),
                    XARMHV.RISK_INDX_VAL_Y11 + NVL (INSUL.RISK_INDX_VAL_Y11, 0),
                    XARMHV.RISK_INDX_VAL_Y12 + NVL (INSUL.RISK_INDX_VAL_Y12, 0),
                    XARMHV.RISK_INDX_VAL_Y13 + NVL (INSUL.RISK_INDX_VAL_Y13, 0),
                    XARMHV.RISK_INDX_VAL_Y14 + NVL (INSUL.RISK_INDX_VAL_Y14, 0),
                    XARMHV.RISK_INDX_VAL_Y15 + NVL (INSUL.RISK_INDX_VAL_Y15, 0),
                    XARMHV.RISK_INDX_VAL_Y16 + NVL (INSUL.RISK_INDX_VAL_Y16, 0),
                    XARMHV.RISK_INDX_VAL_Y17 + NVL (INSUL.RISK_INDX_VAL_Y17, 0),
                    XARMHV.RISK_INDX_VAL_Y18 + NVL (INSUL.RISK_INDX_VAL_Y18, 0),
                    XARMHV.RISK_INDX_VAL_Y19 + NVL (INSUL.RISK_INDX_VAL_Y19, 0),
                    XARMHV.RISK_INDX_VAL_Y20 + NVL (INSUL.RISK_INDX_VAL_Y20, 0),
                    XARMHV.RISK_INDX_VAL_Y21 + NVL (INSUL.RISK_INDX_VAL_Y21, 0),
                    XARMHV.RISK_INDX_VAL_Y22 + NVL (INSUL.RISK_INDX_VAL_Y22, 0),
                    XARMHV.RISK_INDX_VAL_Y23 + NVL (INSUL.RISK_INDX_VAL_Y23, 0),
                    XARMHV.RISK_INDX_VAL_Y24 + NVL (INSUL.RISK_INDX_VAL_Y24, 0),
                    XARMHV.RISK_INDX_VAL_Y25 + NVL (INSUL.RISK_INDX_VAL_Y25, 0),
                    XARMHV.RISK_INDX_VAL_Y26 + NVL (INSUL.RISK_INDX_VAL_Y26, 0),
                    XARMHV.RISK_INDX_VAL_Y27 + NVL (INSUL.RISK_INDX_VAL_Y27, 0),
                    XARMHV.RISK_INDX_VAL_Y28 + NVL (INSUL.RISK_INDX_VAL_Y28, 0),
                    XARMHV.RISK_INDX_VAL_Y29 + NVL (INSUL.RISK_INDX_VAL_Y29, 0),
                    XARMHV.RISK_INDX_VAL_Y30 + NVL (INSUL.RISK_INDX_VAL_Y30, 0),
                    XARMHV.RISK_INDX_VAL_Y31 + NVL (INSUL.RISK_INDX_VAL_Y31, 0),
                    XARMHV.RISK_INDX_VAL_Y32 + NVL (INSUL.RISK_INDX_VAL_Y32, 0),
                    XARMHV.RISK_INDX_VAL_Y33 + NVL (INSUL.RISK_INDX_VAL_Y33, 0),
                    XARMHV.RISK_INDX_VAL_Y34 + NVL (INSUL.RISK_INDX_VAL_Y34, 0),
                    XARMHV.RISK_INDX_VAL_Y35 + NVL (INSUL.RISK_INDX_VAL_Y35, 0),
                    XARMHV.RISK_INDX_VAL_Y36 + NVL (INSUL.RISK_INDX_VAL_Y36, 0),
                    XARMHV.RISK_INDX_VAL_Y37 + NVL (INSUL.RISK_INDX_VAL_Y37, 0),
                    XARMHV.RISK_INDX_VAL_Y38 + NVL (INSUL.RISK_INDX_VAL_Y38, 0),
                    XARMHV.RISK_INDX_VAL_Y39 + NVL (INSUL.RISK_INDX_VAL_Y39, 0),
                    XARMHV.RISK_INDX_VAL_Y40 + NVL (INSUL.RISK_INDX_VAL_Y40, 0),
                    XARMHV.RISK_INDX_VAL_Y41 + NVL (INSUL.RISK_INDX_VAL_Y41, 0),
                    XARMHV.RISK_INDX_VAL_Y42 + NVL (INSUL.RISK_INDX_VAL_Y42, 0),
                    XARMHV.RISK_INDX_VAL_Y43 + NVL (INSUL.RISK_INDX_VAL_Y43, 0),
                    XARMHV.RISK_INDX_VAL_Y44 + NVL (INSUL.RISK_INDX_VAL_Y44, 0),
                    XARMHV.RISK_INDX_VAL_Y45 + NVL (INSUL.RISK_INDX_VAL_Y45, 0),
                    XARMHV.RISK_INDX_VAL_Y46 + NVL (INSUL.RISK_INDX_VAL_Y46, 0),
                    XARMHV.RISK_INDX_VAL_Y47 + NVL (INSUL.RISK_INDX_VAL_Y47, 0),
                    XARMHV.RISK_INDX_VAL_Y48 + NVL (INSUL.RISK_INDX_VAL_Y48, 0),
                    XARMHV.RISK_INDX_VAL_Y49 + NVL (INSUL.RISK_INDX_VAL_Y49, 0),
                    XARMHV.RISK_INDX_VAL_Y50 + NVL (INSUL.RISK_INDX_VAL_Y50, 0)
               FROM structools.ST_RISK_XARMHV_REPLACE xarmhv   LEFT OUTER JOIN
                    structools.ST_RISK_INSUL_REPLACE insul
               ON    xarmhv.pick_id = insul.pick_id
            ;

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - XARMHVI (Combined) REPLACE');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING risk records IN ST_RISK_XARMHVI_REPLACE -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;


      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMHVI_REPLACE_NPV15
            SELECT XARMHV.PICK_ID,
                    XARMHV.RISK_INDX_VAL_Y0 + NVL (INSUL.RISK_INDX_VAL_Y0, 0),
                    XARMHV.RISK_INDX_VAL_Y1 + NVL (INSUL.RISK_INDX_VAL_Y1, 0),
                    XARMHV.RISK_INDX_VAL_Y2 + NVL (INSUL.RISK_INDX_VAL_Y2, 0),
                    XARMHV.RISK_INDX_VAL_Y3 + NVL (INSUL.RISK_INDX_VAL_Y3, 0),
                    XARMHV.RISK_INDX_VAL_Y4 + NVL (INSUL.RISK_INDX_VAL_Y4, 0),
                    XARMHV.RISK_INDX_VAL_Y5 + NVL (INSUL.RISK_INDX_VAL_Y5, 0),
                    XARMHV.RISK_INDX_VAL_Y6 + NVL (INSUL.RISK_INDX_VAL_Y6, 0),
                    XARMHV.RISK_INDX_VAL_Y7 + NVL (INSUL.RISK_INDX_VAL_Y7, 0),
                    XARMHV.RISK_INDX_VAL_Y8 + NVL (INSUL.RISK_INDX_VAL_Y8, 0),
                    XARMHV.RISK_INDX_VAL_Y9 + NVL (INSUL.RISK_INDX_VAL_Y9, 0),
                    XARMHV.RISK_INDX_VAL_Y10 + NVL (INSUL.RISK_INDX_VAL_Y10, 0),
                    XARMHV.RISK_INDX_VAL_Y11 + NVL (INSUL.RISK_INDX_VAL_Y11, 0),
                    XARMHV.RISK_INDX_VAL_Y12 + NVL (INSUL.RISK_INDX_VAL_Y12, 0),
                    XARMHV.RISK_INDX_VAL_Y13 + NVL (INSUL.RISK_INDX_VAL_Y13, 0),
                    XARMHV.RISK_INDX_VAL_Y14 + NVL (INSUL.RISK_INDX_VAL_Y14, 0),
                    XARMHV.RISK_INDX_VAL_Y15 + NVL (INSUL.RISK_INDX_VAL_Y15, 0),
                    XARMHV.RISK_INDX_VAL_Y16 + NVL (INSUL.RISK_INDX_VAL_Y16, 0),
                    XARMHV.RISK_INDX_VAL_Y17 + NVL (INSUL.RISK_INDX_VAL_Y17, 0),
                    XARMHV.RISK_INDX_VAL_Y18 + NVL (INSUL.RISK_INDX_VAL_Y18, 0),
                    XARMHV.RISK_INDX_VAL_Y19 + NVL (INSUL.RISK_INDX_VAL_Y19, 0),
                    XARMHV.RISK_INDX_VAL_Y20 + NVL (INSUL.RISK_INDX_VAL_Y20, 0),
                    XARMHV.RISK_INDX_VAL_Y21 + NVL (INSUL.RISK_INDX_VAL_Y21, 0),
                    XARMHV.RISK_INDX_VAL_Y22 + NVL (INSUL.RISK_INDX_VAL_Y22, 0),
                    XARMHV.RISK_INDX_VAL_Y23 + NVL (INSUL.RISK_INDX_VAL_Y23, 0),
                    XARMHV.RISK_INDX_VAL_Y24 + NVL (INSUL.RISK_INDX_VAL_Y24, 0),
                    XARMHV.RISK_INDX_VAL_Y25 + NVL (INSUL.RISK_INDX_VAL_Y25, 0),
                    XARMHV.RISK_INDX_VAL_Y26 + NVL (INSUL.RISK_INDX_VAL_Y26, 0),
                    XARMHV.RISK_INDX_VAL_Y27 + NVL (INSUL.RISK_INDX_VAL_Y27, 0),
                    XARMHV.RISK_INDX_VAL_Y28 + NVL (INSUL.RISK_INDX_VAL_Y28, 0),
                    XARMHV.RISK_INDX_VAL_Y29 + NVL (INSUL.RISK_INDX_VAL_Y29, 0),
                    XARMHV.RISK_INDX_VAL_Y30 + NVL (INSUL.RISK_INDX_VAL_Y30, 0),
                    XARMHV.RISK_INDX_VAL_Y31 + NVL (INSUL.RISK_INDX_VAL_Y31, 0),
                    XARMHV.RISK_INDX_VAL_Y32 + NVL (INSUL.RISK_INDX_VAL_Y32, 0),
                    XARMHV.RISK_INDX_VAL_Y33 + NVL (INSUL.RISK_INDX_VAL_Y33, 0),
                    XARMHV.RISK_INDX_VAL_Y34 + NVL (INSUL.RISK_INDX_VAL_Y34, 0),
                    XARMHV.RISK_INDX_VAL_Y35 + NVL (INSUL.RISK_INDX_VAL_Y35, 0),
                    XARMHV.RISK_INDX_VAL_Y36 + NVL (INSUL.RISK_INDX_VAL_Y36, 0),
                    XARMHV.RISK_INDX_VAL_Y37 + NVL (INSUL.RISK_INDX_VAL_Y37, 0),
                    XARMHV.RISK_INDX_VAL_Y38 + NVL (INSUL.RISK_INDX_VAL_Y38, 0),
                    XARMHV.RISK_INDX_VAL_Y39 + NVL (INSUL.RISK_INDX_VAL_Y39, 0),
                    XARMHV.RISK_INDX_VAL_Y40 + NVL (INSUL.RISK_INDX_VAL_Y40, 0),
                    XARMHV.RISK_INDX_VAL_Y41 + NVL (INSUL.RISK_INDX_VAL_Y41, 0),
                    XARMHV.RISK_INDX_VAL_Y42 + NVL (INSUL.RISK_INDX_VAL_Y42, 0),
                    XARMHV.RISK_INDX_VAL_Y43 + NVL (INSUL.RISK_INDX_VAL_Y43, 0),
                    XARMHV.RISK_INDX_VAL_Y44 + NVL (INSUL.RISK_INDX_VAL_Y44, 0),
                    XARMHV.RISK_INDX_VAL_Y45 + NVL (INSUL.RISK_INDX_VAL_Y45, 0),
                    XARMHV.RISK_INDX_VAL_Y46 + NVL (INSUL.RISK_INDX_VAL_Y46, 0),
                    XARMHV.RISK_INDX_VAL_Y47 + NVL (INSUL.RISK_INDX_VAL_Y47, 0),
                    XARMHV.RISK_INDX_VAL_Y48 + NVL (INSUL.RISK_INDX_VAL_Y48, 0),
                    XARMHV.RISK_INDX_VAL_Y49 + NVL (INSUL.RISK_INDX_VAL_Y49, 0),
                    XARMHV.RISK_INDX_VAL_Y50 + NVL (INSUL.RISK_INDX_VAL_Y50, 0)
               FROM structools.ST_RISK_XARMHV_REPLACE_NPV15 xarmhv   LEFT OUTER JOIN
                    structools.ST_RISK_INSUL_REPLACE_NPV15 insul
               ON    xarmhv.pick_id = insul.pick_id
            ;

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - XARMHVI (Combined) REPLACE NPV15');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN NPV TABLE ST_RISK_XARMHVI_REPLACE_NPV15 -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMHVI_REPLACE');
      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMHVI_REPLACE_NPV15');

   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END PR_LOAD_RISK_XARMHVI;

   PROCEDURE PR_LOAD_RISK_XARMLVI
   (p_yr_mth_no  IN    CHAR )
   AS
      v_record_cnt   NUMBER := 0;
      v_xarmlv_fin_conseq_amt NUMBER;
      v_insul_fin_conseq_amt  NUMBER;

      CURSOR get_reactv_cost_cur
      IS
         SELECT TRIM(mdl_bus_cde) AS mdl_bus_cde
               ,trmnt_reactv_cst_amt
         --FROM structoolsw.treatment_reactive_cost
         FROM structools.st_treatment_reactive_cost
      ;

   BEGIN
      DBMS_OUTPUT.put_line (
            'Risk MODEL - XARMLVI (Combined)_DONOTHING Start TIME -'
         || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));

      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMLVI_NOACTION');
      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMLVI_NOACTION_NPV15');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMLVI_NOACTION');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMLVI_NOACTION_NPV15');

      DELETE FROM structools.ST_RISK_FRACTIONS frac
            WHERE FRAC.MODEL_NAME = 'XARMLVI';

      COMMIT;

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMLVI_NOACTION
             SELECT XARMLV.PICK_ID,
                    XARMLV.RISK_INDX_VAL_Y0 + NVL (INSUL.RISK_INDX_VAL_Y0, 0),
                    XARMLV.RISK_INDX_VAL_Y1 + NVL (INSUL.RISK_INDX_VAL_Y1, 0),
                    XARMLV.RISK_INDX_VAL_Y2 + NVL (INSUL.RISK_INDX_VAL_Y2, 0),
                    XARMLV.RISK_INDX_VAL_Y3 + NVL (INSUL.RISK_INDX_VAL_Y3, 0),
                    XARMLV.RISK_INDX_VAL_Y4 + NVL (INSUL.RISK_INDX_VAL_Y4, 0),
                    XARMLV.RISK_INDX_VAL_Y5 + NVL (INSUL.RISK_INDX_VAL_Y5, 0),
                    XARMLV.RISK_INDX_VAL_Y6 + NVL (INSUL.RISK_INDX_VAL_Y6, 0),
                    XARMLV.RISK_INDX_VAL_Y7 + NVL (INSUL.RISK_INDX_VAL_Y7, 0),
                    XARMLV.RISK_INDX_VAL_Y8 + NVL (INSUL.RISK_INDX_VAL_Y8, 0),
                    XARMLV.RISK_INDX_VAL_Y9 + NVL (INSUL.RISK_INDX_VAL_Y9, 0),
                    XARMLV.RISK_INDX_VAL_Y10 + NVL (INSUL.RISK_INDX_VAL_Y10, 0),
                    XARMLV.RISK_INDX_VAL_Y11 + NVL (INSUL.RISK_INDX_VAL_Y11, 0),
                    XARMLV.RISK_INDX_VAL_Y12 + NVL (INSUL.RISK_INDX_VAL_Y12, 0),
                    XARMLV.RISK_INDX_VAL_Y13 + NVL (INSUL.RISK_INDX_VAL_Y13, 0),
                    XARMLV.RISK_INDX_VAL_Y14 + NVL (INSUL.RISK_INDX_VAL_Y14, 0),
                    XARMLV.RISK_INDX_VAL_Y15 + NVL (INSUL.RISK_INDX_VAL_Y15, 0),
                    XARMLV.RISK_INDX_VAL_Y16 + NVL (INSUL.RISK_INDX_VAL_Y16, 0),
                    XARMLV.RISK_INDX_VAL_Y17 + NVL (INSUL.RISK_INDX_VAL_Y17, 0),
                    XARMLV.RISK_INDX_VAL_Y18 + NVL (INSUL.RISK_INDX_VAL_Y18, 0),
                    XARMLV.RISK_INDX_VAL_Y19 + NVL (INSUL.RISK_INDX_VAL_Y19, 0),
                    XARMLV.RISK_INDX_VAL_Y20 + NVL (INSUL.RISK_INDX_VAL_Y20, 0),
                    XARMLV.RISK_INDX_VAL_Y21 + NVL (INSUL.RISK_INDX_VAL_Y21, 0),
                    XARMLV.RISK_INDX_VAL_Y22 + NVL (INSUL.RISK_INDX_VAL_Y22, 0),
                    XARMLV.RISK_INDX_VAL_Y23 + NVL (INSUL.RISK_INDX_VAL_Y23, 0),
                    XARMLV.RISK_INDX_VAL_Y24 + NVL (INSUL.RISK_INDX_VAL_Y24, 0),
                    XARMLV.RISK_INDX_VAL_Y25 + NVL (INSUL.RISK_INDX_VAL_Y25, 0),
                    XARMLV.RISK_INDX_VAL_Y26 + NVL (INSUL.RISK_INDX_VAL_Y26, 0),
                    XARMLV.RISK_INDX_VAL_Y27 + NVL (INSUL.RISK_INDX_VAL_Y27, 0),
                    XARMLV.RISK_INDX_VAL_Y28 + NVL (INSUL.RISK_INDX_VAL_Y28, 0),
                    XARMLV.RISK_INDX_VAL_Y29 + NVL (INSUL.RISK_INDX_VAL_Y29, 0),
                    XARMLV.RISK_INDX_VAL_Y30 + NVL (INSUL.RISK_INDX_VAL_Y30, 0),
                    XARMLV.RISK_INDX_VAL_Y31 + NVL (INSUL.RISK_INDX_VAL_Y31, 0),
                    XARMLV.RISK_INDX_VAL_Y32 + NVL (INSUL.RISK_INDX_VAL_Y32, 0),
                    XARMLV.RISK_INDX_VAL_Y33 + NVL (INSUL.RISK_INDX_VAL_Y33, 0),
                    XARMLV.RISK_INDX_VAL_Y34 + NVL (INSUL.RISK_INDX_VAL_Y34, 0),
                    XARMLV.RISK_INDX_VAL_Y35 + NVL (INSUL.RISK_INDX_VAL_Y35, 0),
                    XARMLV.RISK_INDX_VAL_Y36 + NVL (INSUL.RISK_INDX_VAL_Y36, 0),
                    XARMLV.RISK_INDX_VAL_Y37 + NVL (INSUL.RISK_INDX_VAL_Y37, 0),
                    XARMLV.RISK_INDX_VAL_Y38 + NVL (INSUL.RISK_INDX_VAL_Y38, 0),
                    XARMLV.RISK_INDX_VAL_Y39 + NVL (INSUL.RISK_INDX_VAL_Y39, 0),
                    XARMLV.RISK_INDX_VAL_Y40 + NVL (INSUL.RISK_INDX_VAL_Y40, 0),
                    XARMLV.RISK_INDX_VAL_Y41 + NVL (INSUL.RISK_INDX_VAL_Y41, 0),
                    XARMLV.RISK_INDX_VAL_Y42 + NVL (INSUL.RISK_INDX_VAL_Y42, 0),
                    XARMLV.RISK_INDX_VAL_Y43 + NVL (INSUL.RISK_INDX_VAL_Y43, 0),
                    XARMLV.RISK_INDX_VAL_Y44 + NVL (INSUL.RISK_INDX_VAL_Y44, 0),
                    XARMLV.RISK_INDX_VAL_Y45 + NVL (INSUL.RISK_INDX_VAL_Y45, 0),
                    XARMLV.RISK_INDX_VAL_Y46 + NVL (INSUL.RISK_INDX_VAL_Y46, 0),
                    XARMLV.RISK_INDX_VAL_Y47 + NVL (INSUL.RISK_INDX_VAL_Y47, 0),
                    XARMLV.RISK_INDX_VAL_Y48 + NVL (INSUL.RISK_INDX_VAL_Y48, 0),
                    XARMLV.RISK_INDX_VAL_Y49 + NVL (INSUL.RISK_INDX_VAL_Y49, 0),
                    XARMLV.RISK_INDX_VAL_Y50 + NVL (INSUL.RISK_INDX_VAL_Y50, 0)
               FROM structools.ST_RISK_XARMLV_NOACTION xarmlv  LEFT OUTER JOIN
                    structools.ST_RISK_INSUL_NOACTION insul
               ON xarmlv.pick_id = insul.pick_id
            ;

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - XARMLVI (Combined) DONOTHING');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING risk records IN ST_RISK_XARMLVI_NOACTION -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      /* load reactive cost of all  models to an associative array, indexed by the model name */
      /* This only needs to be done if this procedure is invoked on its own and not as part of PR_LOAD_ALL_RISK_TABLES procedure */
      IF v_reac_cost_tab.COUNT = 0 THEN
         FOR c IN get_reactv_cost_cur LOOP
            v_reac_cost_tab(c.mdl_bus_cde) := c.trmnt_reactv_cst_amt;
         END LOOP;   
      END IF;

      v_xarmlv_fin_conseq_amt := v_reac_cost_tab('XARMLV');
      v_insul_fin_conseq_amt  := v_reac_cost_tab('INSUL');

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMLVI_NOACTION_NPV15
            WITH conseq_calc AS   --fin_frac - copy from here to the end of the sql and replace atm.mdl_bus_cde values
            (
            SELECT 
               (CASE
                   WHEN instrc (ed.plnt_no,
                                ' ',
                                1,
                                1) = 0
                   THEN
                      SUBSTR (ed.plnt_no,
                              -1 * (LENGTH (ed.plnt_no) - 1))
                   ELSE
                      SUBSTR (ed.plnt_no,
                              2,
                              instrc (ed.plnt_no,
                                      ' ',
                                      1,
                                      1) - 2)
                END) AS pick_id
               ,nrdmf.hzrd_fn_1_2
               ,nrdmf.FIRE_CNSQNC_AMT
               ,nrdmf.ELEC_SHCK_CNSQNC_AMT
               ,nrdmf.WRK_FRCE_CNSQNC_AMT
               ,nrdmf.PHYS_IMPCT_CNSQNC_AMT
               ,nrdmf.RELIABILITY_CNSQNC_AMT
               ,nrdmf.ENV_CNSQNC_AMT
               ,CASE
                  WHEN atm.mdl_bus_cde = 'XARMLV'        THEN v_xarmlv_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'INSUL'         THEN v_insul_fin_conseq_amt
               END              AS FIN_CNSQNC_AMT 
               ,CASE                                                                      --fin_fracc - replace
                  WHEN atm.mdl_bus_cde = 'XARMLV'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_xarmlv_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'INSUL'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_insul_fin_conseq_amt
               END              AS conseq_amt 
            FROM structoolsw.mthly_equip_risk_dm_fact nrdmf
               JOIN
               structoolsw.nrm_asset_type_model_dim atm
                  ON atm.nrm_asset_typ_mdl_sk =
                        nrdmf.nrm_asset_typ_mdl_sk
               JOIN structoolsw.treatment_dim td
                  ON td.trmnt_sk = nrdmf.trmnt_sk
               JOIN structoolsw.equipment_nvgtn_dim en
                  ON en.equip_sk = nrdmf.equip_sk
               JOIN structoolsw.equipment_dim ed
                  ON ed.equip_nvgtn_sk = en.equip_nvgtn_sk
            WHERE     nrdmf.yr_mth_no = p_yr_mth_no
                  AND td.trmnt_nam = 'DONOTHING'
                  AND atm.mdl_bus_cde IN
                         (
                          'XARMLV',
                          'INSUL')
                  --AND ed.pick_id = 'S318'
            )
            ,
            frac AS
            (  
            SELECT   pick_id
                    ,SUM (FIRE_CNSQNC_AMT * hzrd_fn_1_2)          /  SUM (hzrd_fn_1_2 * conseq_amt) AS fire_frac
                    ,SUM (ELEC_SHCK_CNSQNC_AMT * hzrd_fn_1_2)     /  SUM (hzrd_fn_1_2 * conseq_amt) AS ESHOCK_frac
                    ,SUM (WRK_FRCE_CNSQNC_AMT * hzrd_fn_1_2)      /  SUM (hzrd_fn_1_2 * conseq_amt) AS WFORCE_frac
                    ,SUM (RELIABILITY_CNSQNC_AMT * hzrd_fn_1_2)   /  SUM (hzrd_fn_1_2 * conseq_amt) AS RELIABILITY_frac
                    ,SUM (PHYS_IMPCT_CNSQNC_AMT * hzrd_fn_1_2)    /  SUM (hzrd_fn_1_2 * conseq_amt) AS PHYSIMP_frac
                    ,SUM (ENV_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS ENV_frac
                    ,SUM (FIN_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS FIN_frac      
             FROM conseq_calc
            GROUP BY pick_id
            )
            SELECT XARMLV.PICK_ID,
                    XARMLV.RISK_INDX_VAL_Y0 + NVL (INSUL.RISK_INDX_VAL_Y0, 0),
                    XARMLV.RISK_INDX_VAL_Y1 + NVL (INSUL.RISK_INDX_VAL_Y1, 0),
                    XARMLV.RISK_INDX_VAL_Y2 + NVL (INSUL.RISK_INDX_VAL_Y2, 0),
                    XARMLV.RISK_INDX_VAL_Y3 + NVL (INSUL.RISK_INDX_VAL_Y3, 0),
                    XARMLV.RISK_INDX_VAL_Y4 + NVL (INSUL.RISK_INDX_VAL_Y4, 0),
                    XARMLV.RISK_INDX_VAL_Y5 + NVL (INSUL.RISK_INDX_VAL_Y5, 0),
                    XARMLV.RISK_INDX_VAL_Y6 + NVL (INSUL.RISK_INDX_VAL_Y6, 0),
                    XARMLV.RISK_INDX_VAL_Y7 + NVL (INSUL.RISK_INDX_VAL_Y7, 0),
                    XARMLV.RISK_INDX_VAL_Y8 + NVL (INSUL.RISK_INDX_VAL_Y8, 0),
                    XARMLV.RISK_INDX_VAL_Y9 + NVL (INSUL.RISK_INDX_VAL_Y9, 0),
                    XARMLV.RISK_INDX_VAL_Y10 + NVL (INSUL.RISK_INDX_VAL_Y10, 0),
                    XARMLV.RISK_INDX_VAL_Y11 + NVL (INSUL.RISK_INDX_VAL_Y11, 0),
                    XARMLV.RISK_INDX_VAL_Y12 + NVL (INSUL.RISK_INDX_VAL_Y12, 0),
                    XARMLV.RISK_INDX_VAL_Y13 + NVL (INSUL.RISK_INDX_VAL_Y13, 0),
                    XARMLV.RISK_INDX_VAL_Y14 + NVL (INSUL.RISK_INDX_VAL_Y14, 0),
                    XARMLV.RISK_INDX_VAL_Y15 + NVL (INSUL.RISK_INDX_VAL_Y15, 0),
                    XARMLV.RISK_INDX_VAL_Y16 + NVL (INSUL.RISK_INDX_VAL_Y16, 0),
                    XARMLV.RISK_INDX_VAL_Y17 + NVL (INSUL.RISK_INDX_VAL_Y17, 0),
                    XARMLV.RISK_INDX_VAL_Y18 + NVL (INSUL.RISK_INDX_VAL_Y18, 0),
                    XARMLV.RISK_INDX_VAL_Y19 + NVL (INSUL.RISK_INDX_VAL_Y19, 0),
                    XARMLV.RISK_INDX_VAL_Y20 + NVL (INSUL.RISK_INDX_VAL_Y20, 0),
                    XARMLV.RISK_INDX_VAL_Y21 + NVL (INSUL.RISK_INDX_VAL_Y21, 0),
                    XARMLV.RISK_INDX_VAL_Y22 + NVL (INSUL.RISK_INDX_VAL_Y22, 0),
                    XARMLV.RISK_INDX_VAL_Y23 + NVL (INSUL.RISK_INDX_VAL_Y23, 0),
                    XARMLV.RISK_INDX_VAL_Y24 + NVL (INSUL.RISK_INDX_VAL_Y24, 0),
                    XARMLV.RISK_INDX_VAL_Y25 + NVL (INSUL.RISK_INDX_VAL_Y25, 0),
                    XARMLV.RISK_INDX_VAL_Y26 + NVL (INSUL.RISK_INDX_VAL_Y26, 0),
                    XARMLV.RISK_INDX_VAL_Y27 + NVL (INSUL.RISK_INDX_VAL_Y27, 0),
                    XARMLV.RISK_INDX_VAL_Y28 + NVL (INSUL.RISK_INDX_VAL_Y28, 0),
                    XARMLV.RISK_INDX_VAL_Y29 + NVL (INSUL.RISK_INDX_VAL_Y29, 0),
                    XARMLV.RISK_INDX_VAL_Y30 + NVL (INSUL.RISK_INDX_VAL_Y30, 0),
                    XARMLV.RISK_INDX_VAL_Y31 + NVL (INSUL.RISK_INDX_VAL_Y31, 0),
                    XARMLV.RISK_INDX_VAL_Y32 + NVL (INSUL.RISK_INDX_VAL_Y32, 0),
                    XARMLV.RISK_INDX_VAL_Y33 + NVL (INSUL.RISK_INDX_VAL_Y33, 0),
                    XARMLV.RISK_INDX_VAL_Y34 + NVL (INSUL.RISK_INDX_VAL_Y34, 0),
                    XARMLV.RISK_INDX_VAL_Y35 + NVL (INSUL.RISK_INDX_VAL_Y35, 0),
                    XARMLV.RISK_INDX_VAL_Y36 + NVL (INSUL.RISK_INDX_VAL_Y36, 0),
                    XARMLV.RISK_INDX_VAL_Y37 + NVL (INSUL.RISK_INDX_VAL_Y37, 0),
                    XARMLV.RISK_INDX_VAL_Y38 + NVL (INSUL.RISK_INDX_VAL_Y38, 0),
                    XARMLV.RISK_INDX_VAL_Y39 + NVL (INSUL.RISK_INDX_VAL_Y39, 0),
                    XARMLV.RISK_INDX_VAL_Y40 + NVL (INSUL.RISK_INDX_VAL_Y40, 0),
                    XARMLV.RISK_INDX_VAL_Y41 + NVL (INSUL.RISK_INDX_VAL_Y41, 0),
                    XARMLV.RISK_INDX_VAL_Y42 + NVL (INSUL.RISK_INDX_VAL_Y42, 0),
                    XARMLV.RISK_INDX_VAL_Y43 + NVL (INSUL.RISK_INDX_VAL_Y43, 0),
                    XARMLV.RISK_INDX_VAL_Y44 + NVL (INSUL.RISK_INDX_VAL_Y44, 0),
                    XARMLV.RISK_INDX_VAL_Y45 + NVL (INSUL.RISK_INDX_VAL_Y45, 0),
                    XARMLV.RISK_INDX_VAL_Y46 + NVL (INSUL.RISK_INDX_VAL_Y46, 0),
                    XARMLV.RISK_INDX_VAL_Y47 + NVL (INSUL.RISK_INDX_VAL_Y47, 0),
                    XARMLV.RISK_INDX_VAL_Y48 + NVL (INSUL.RISK_INDX_VAL_Y48, 0),
                    XARMLV.RISK_INDX_VAL_Y49 + NVL (INSUL.RISK_INDX_VAL_Y49, 0),
                    XARMLV.RISK_INDX_VAL_Y50 + NVL (INSUL.RISK_INDX_VAL_Y50, 0),
                    frac.fire_frac,
                    frac.eshock_frac,
                    frac.wforce_frac,
                    frac.reliability_frac,
                    frac.physimp_frac,
                    frac.env_frac,
                    frac.fin_frac
                     FROM  structools.ST_RISK_XARMLV_NOACTION_NPV15 xarmlv     LEFT OUTER JOIN
                           structools.ST_RISK_INSUL_NOACTION_NPV15 insul
                           ON xarmlv.pick_id = insul.pick_id
                                                                           LEFT OUTER JOIN
                           frac
                           ON xarmlv.pick_id = frac.pick_id
                     --and ed.equip_no = '000001000022'--in ('000020132622', '000001824246')
                     --and ed.plnt_no = 'S307'
                     ;

         v_record_cnt := SQL%ROWCOUNT;

         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - XARMLVI (Combined) DONOTHING NPV15');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN NPV TABLE ST_RISK_XARMLVI_NOACTION_NPV15 -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      BEGIN
         INSERT INTO structools.ST_RISK_FRACTIONS
            WITH conseq_calc AS   --fin_frac - copy from here to the end of the sql and replace atm.mdl_bus_cde values
            (
            SELECT 'S' || (CASE
                      WHEN instrc (ed.plnt_no,
                                   ' ',
                                   1,
                                   1) = 0
                      THEN
                         SUBSTR (ed.plnt_no,
                                 -1 * (LENGTH (ed.plnt_no) - 1))
                      ELSE
                         SUBSTR (ed.plnt_no,
                                 2,
                                 instrc (ed.plnt_no,
                                         ' ',
                                         1,
                                         1) - 2)
                   END) AS pick_id
                  ,nrdmf.hzrd_fn_1_2
                  ,nrdmf.FIRE_CNSQNC_AMT
                  ,nrdmf.ELEC_SHCK_CNSQNC_AMT
                  ,nrdmf.WRK_FRCE_CNSQNC_AMT
                  ,nrdmf.PHYS_IMPCT_CNSQNC_AMT
                  ,nrdmf.RELIABILITY_CNSQNC_AMT
                  ,nrdmf.ENV_CNSQNC_AMT
                  ,CASE
                     WHEN atm.mdl_bus_cde = 'XARMLV'        THEN v_xarmlv_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'INSUL'         THEN v_insul_fin_conseq_amt
                  END              AS FIN_CNSQNC_AMT 
                  ,CASE                                                                      --fin_fracc - replace
                     WHEN atm.mdl_bus_cde = 'XARMLV'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_xarmlv_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'INSUL'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_insul_fin_conseq_amt
                  END              AS conseq_amt 
             FROM structoolsw.mthly_equip_risk_dm_fact nrdmf
                  JOIN
                  structoolsw.nrm_asset_type_model_dim atm
                     ON atm.nrm_asset_typ_mdl_sk =
                           nrdmf.nrm_asset_typ_mdl_sk
                  JOIN structoolsw.treatment_dim td
                     ON td.trmnt_sk = nrdmf.trmnt_sk
                  JOIN structoolsw.equipment_nvgtn_dim en
                     ON en.equip_sk = nrdmf.equip_sk
                  JOIN structoolsw.equipment_dim ed
                     ON ed.equip_nvgtn_sk = en.equip_nvgtn_sk
            WHERE     nrdmf.yr_mth_no = p_yr_mth_no
                  AND td.trmnt_nam = 'DONOTHING'
                  AND atm.mdl_bus_cde IN
                         ('XARMLV',
                          'INSUL')
                  --AND ed.pick_id = 'S318'
            )  
            SELECT   pick_id
                     ,'XARMLVI'                                    -- model_type
                     ,0                                          -- FIRE_INCD
                     ,0                                    -- ELEC_SHOCK_INCD
                     ,0                                    --LRGE_RLBLTY_INCD
                     ,0                            --ELEC_SHOCK_FATALITY_INCD
                     ,0                                 -- FIRE_FATALITY_INCD
                    ,SUM (FIRE_CNSQNC_AMT * hzrd_fn_1_2)          /  SUM (hzrd_fn_1_2 * conseq_amt) AS fire_frac
                    ,SUM (ELEC_SHCK_CNSQNC_AMT * hzrd_fn_1_2)     /  SUM (hzrd_fn_1_2 * conseq_amt) AS ESHOCK_frac
                    ,SUM (WRK_FRCE_CNSQNC_AMT * hzrd_fn_1_2)      /  SUM (hzrd_fn_1_2 * conseq_amt) AS WFORCE_frac
                    ,SUM (RELIABILITY_CNSQNC_AMT * hzrd_fn_1_2)   /  SUM (hzrd_fn_1_2 * conseq_amt) AS RELIABILITY_frac
                    ,SUM (PHYS_IMPCT_CNSQNC_AMT * hzrd_fn_1_2)    /  SUM (hzrd_fn_1_2 * conseq_amt) AS PHYSIMP_frac
                    ,SUM (ENV_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS ENV_frac
                    ,SUM (FIN_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS FIN_frac      
             FROM conseq_calc
             GROUP BY pick_id
            ;

         v_record_cnt := SQL%ROWCOUNT;

         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - XARMLVI (Combined) ST_RISK_FRACTIONS');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN TABLE ST_RISK_FRACTIONS FOR XARMLVI-'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMLVI_NOACTION');
      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMLVI_NOACTION_NPV15');

      DBMS_OUTPUT.put_line (
            'Risk MODEL - XARMLVI (Combined)_REPLACE Start TIME -'
         || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));

      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMLVI_REPLACE');
      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_XARMLVI_REPLACE_NPV15');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMLVI_REPLACE');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_XARMLVI_REPLACE_NPV15');
      
      COMMIT;

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMLVI_REPLACE
            SELECT XARMLV.PICK_ID,
                    XARMLV.RISK_INDX_VAL_Y0 + NVL (INSUL.RISK_INDX_VAL_Y0, 0),
                    XARMLV.RISK_INDX_VAL_Y1 + NVL (INSUL.RISK_INDX_VAL_Y1, 0),
                    XARMLV.RISK_INDX_VAL_Y2 + NVL (INSUL.RISK_INDX_VAL_Y2, 0),
                    XARMLV.RISK_INDX_VAL_Y3 + NVL (INSUL.RISK_INDX_VAL_Y3, 0),
                    XARMLV.RISK_INDX_VAL_Y4 + NVL (INSUL.RISK_INDX_VAL_Y4, 0),
                    XARMLV.RISK_INDX_VAL_Y5 + NVL (INSUL.RISK_INDX_VAL_Y5, 0),
                    XARMLV.RISK_INDX_VAL_Y6 + NVL (INSUL.RISK_INDX_VAL_Y6, 0),
                    XARMLV.RISK_INDX_VAL_Y7 + NVL (INSUL.RISK_INDX_VAL_Y7, 0),
                    XARMLV.RISK_INDX_VAL_Y8 + NVL (INSUL.RISK_INDX_VAL_Y8, 0),
                    XARMLV.RISK_INDX_VAL_Y9 + NVL (INSUL.RISK_INDX_VAL_Y9, 0),
                    XARMLV.RISK_INDX_VAL_Y10 + NVL (INSUL.RISK_INDX_VAL_Y10, 0),
                    XARMLV.RISK_INDX_VAL_Y11 + NVL (INSUL.RISK_INDX_VAL_Y11, 0),
                    XARMLV.RISK_INDX_VAL_Y12 + NVL (INSUL.RISK_INDX_VAL_Y12, 0),
                    XARMLV.RISK_INDX_VAL_Y13 + NVL (INSUL.RISK_INDX_VAL_Y13, 0),
                    XARMLV.RISK_INDX_VAL_Y14 + NVL (INSUL.RISK_INDX_VAL_Y14, 0),
                    XARMLV.RISK_INDX_VAL_Y15 + NVL (INSUL.RISK_INDX_VAL_Y15, 0),
                    XARMLV.RISK_INDX_VAL_Y16 + NVL (INSUL.RISK_INDX_VAL_Y16, 0),
                    XARMLV.RISK_INDX_VAL_Y17 + NVL (INSUL.RISK_INDX_VAL_Y17, 0),
                    XARMLV.RISK_INDX_VAL_Y18 + NVL (INSUL.RISK_INDX_VAL_Y18, 0),
                    XARMLV.RISK_INDX_VAL_Y19 + NVL (INSUL.RISK_INDX_VAL_Y19, 0),
                    XARMLV.RISK_INDX_VAL_Y20 + NVL (INSUL.RISK_INDX_VAL_Y20, 0),
                    XARMLV.RISK_INDX_VAL_Y21 + NVL (INSUL.RISK_INDX_VAL_Y21, 0),
                    XARMLV.RISK_INDX_VAL_Y22 + NVL (INSUL.RISK_INDX_VAL_Y22, 0),
                    XARMLV.RISK_INDX_VAL_Y23 + NVL (INSUL.RISK_INDX_VAL_Y23, 0),
                    XARMLV.RISK_INDX_VAL_Y24 + NVL (INSUL.RISK_INDX_VAL_Y24, 0),
                    XARMLV.RISK_INDX_VAL_Y25 + NVL (INSUL.RISK_INDX_VAL_Y25, 0),
                    XARMLV.RISK_INDX_VAL_Y26 + NVL (INSUL.RISK_INDX_VAL_Y26, 0),
                    XARMLV.RISK_INDX_VAL_Y27 + NVL (INSUL.RISK_INDX_VAL_Y27, 0),
                    XARMLV.RISK_INDX_VAL_Y28 + NVL (INSUL.RISK_INDX_VAL_Y28, 0),
                    XARMLV.RISK_INDX_VAL_Y29 + NVL (INSUL.RISK_INDX_VAL_Y29, 0),
                    XARMLV.RISK_INDX_VAL_Y30 + NVL (INSUL.RISK_INDX_VAL_Y30, 0),
                    XARMLV.RISK_INDX_VAL_Y31 + NVL (INSUL.RISK_INDX_VAL_Y31, 0),
                    XARMLV.RISK_INDX_VAL_Y32 + NVL (INSUL.RISK_INDX_VAL_Y32, 0),
                    XARMLV.RISK_INDX_VAL_Y33 + NVL (INSUL.RISK_INDX_VAL_Y33, 0),
                    XARMLV.RISK_INDX_VAL_Y34 + NVL (INSUL.RISK_INDX_VAL_Y34, 0),
                    XARMLV.RISK_INDX_VAL_Y35 + NVL (INSUL.RISK_INDX_VAL_Y35, 0),
                    XARMLV.RISK_INDX_VAL_Y36 + NVL (INSUL.RISK_INDX_VAL_Y36, 0),
                    XARMLV.RISK_INDX_VAL_Y37 + NVL (INSUL.RISK_INDX_VAL_Y37, 0),
                    XARMLV.RISK_INDX_VAL_Y38 + NVL (INSUL.RISK_INDX_VAL_Y38, 0),
                    XARMLV.RISK_INDX_VAL_Y39 + NVL (INSUL.RISK_INDX_VAL_Y39, 0),
                    XARMLV.RISK_INDX_VAL_Y40 + NVL (INSUL.RISK_INDX_VAL_Y40, 0),
                    XARMLV.RISK_INDX_VAL_Y41 + NVL (INSUL.RISK_INDX_VAL_Y41, 0),
                    XARMLV.RISK_INDX_VAL_Y42 + NVL (INSUL.RISK_INDX_VAL_Y42, 0),
                    XARMLV.RISK_INDX_VAL_Y43 + NVL (INSUL.RISK_INDX_VAL_Y43, 0),
                    XARMLV.RISK_INDX_VAL_Y44 + NVL (INSUL.RISK_INDX_VAL_Y44, 0),
                    XARMLV.RISK_INDX_VAL_Y45 + NVL (INSUL.RISK_INDX_VAL_Y45, 0),
                    XARMLV.RISK_INDX_VAL_Y46 + NVL (INSUL.RISK_INDX_VAL_Y46, 0),
                    XARMLV.RISK_INDX_VAL_Y47 + NVL (INSUL.RISK_INDX_VAL_Y47, 0),
                    XARMLV.RISK_INDX_VAL_Y48 + NVL (INSUL.RISK_INDX_VAL_Y48, 0),
                    XARMLV.RISK_INDX_VAL_Y49 + NVL (INSUL.RISK_INDX_VAL_Y49, 0),
                    XARMLV.RISK_INDX_VAL_Y50 + NVL (INSUL.RISK_INDX_VAL_Y50, 0)
               FROM structools.ST_RISK_XARMLV_REPLACE xarmlv   LEFT OUTER JOIN
                    structools.ST_RISK_INSUL_REPLACE insul
               ON    xarmlv.pick_id = insul.pick_id
            ;

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - XARMLVI (Combined) REPLACE');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING risk records IN ST_RISK_XARMLVI_REPLACE -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;


      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMLVI_REPLACE_NPV15
            SELECT XARMLV.PICK_ID,
                    XARMLV.RISK_INDX_VAL_Y0 + NVL (INSUL.RISK_INDX_VAL_Y0, 0),
                    XARMLV.RISK_INDX_VAL_Y1 + NVL (INSUL.RISK_INDX_VAL_Y1, 0),
                    XARMLV.RISK_INDX_VAL_Y2 + NVL (INSUL.RISK_INDX_VAL_Y2, 0),
                    XARMLV.RISK_INDX_VAL_Y3 + NVL (INSUL.RISK_INDX_VAL_Y3, 0),
                    XARMLV.RISK_INDX_VAL_Y4 + NVL (INSUL.RISK_INDX_VAL_Y4, 0),
                    XARMLV.RISK_INDX_VAL_Y5 + NVL (INSUL.RISK_INDX_VAL_Y5, 0),
                    XARMLV.RISK_INDX_VAL_Y6 + NVL (INSUL.RISK_INDX_VAL_Y6, 0),
                    XARMLV.RISK_INDX_VAL_Y7 + NVL (INSUL.RISK_INDX_VAL_Y7, 0),
                    XARMLV.RISK_INDX_VAL_Y8 + NVL (INSUL.RISK_INDX_VAL_Y8, 0),
                    XARMLV.RISK_INDX_VAL_Y9 + NVL (INSUL.RISK_INDX_VAL_Y9, 0),
                    XARMLV.RISK_INDX_VAL_Y10 + NVL (INSUL.RISK_INDX_VAL_Y10, 0),
                    XARMLV.RISK_INDX_VAL_Y11 + NVL (INSUL.RISK_INDX_VAL_Y11, 0),
                    XARMLV.RISK_INDX_VAL_Y12 + NVL (INSUL.RISK_INDX_VAL_Y12, 0),
                    XARMLV.RISK_INDX_VAL_Y13 + NVL (INSUL.RISK_INDX_VAL_Y13, 0),
                    XARMLV.RISK_INDX_VAL_Y14 + NVL (INSUL.RISK_INDX_VAL_Y14, 0),
                    XARMLV.RISK_INDX_VAL_Y15 + NVL (INSUL.RISK_INDX_VAL_Y15, 0),
                    XARMLV.RISK_INDX_VAL_Y16 + NVL (INSUL.RISK_INDX_VAL_Y16, 0),
                    XARMLV.RISK_INDX_VAL_Y17 + NVL (INSUL.RISK_INDX_VAL_Y17, 0),
                    XARMLV.RISK_INDX_VAL_Y18 + NVL (INSUL.RISK_INDX_VAL_Y18, 0),
                    XARMLV.RISK_INDX_VAL_Y19 + NVL (INSUL.RISK_INDX_VAL_Y19, 0),
                    XARMLV.RISK_INDX_VAL_Y20 + NVL (INSUL.RISK_INDX_VAL_Y20, 0),
                    XARMLV.RISK_INDX_VAL_Y21 + NVL (INSUL.RISK_INDX_VAL_Y21, 0),
                    XARMLV.RISK_INDX_VAL_Y22 + NVL (INSUL.RISK_INDX_VAL_Y22, 0),
                    XARMLV.RISK_INDX_VAL_Y23 + NVL (INSUL.RISK_INDX_VAL_Y23, 0),
                    XARMLV.RISK_INDX_VAL_Y24 + NVL (INSUL.RISK_INDX_VAL_Y24, 0),
                    XARMLV.RISK_INDX_VAL_Y25 + NVL (INSUL.RISK_INDX_VAL_Y25, 0),
                    XARMLV.RISK_INDX_VAL_Y26 + NVL (INSUL.RISK_INDX_VAL_Y26, 0),
                    XARMLV.RISK_INDX_VAL_Y27 + NVL (INSUL.RISK_INDX_VAL_Y27, 0),
                    XARMLV.RISK_INDX_VAL_Y28 + NVL (INSUL.RISK_INDX_VAL_Y28, 0),
                    XARMLV.RISK_INDX_VAL_Y29 + NVL (INSUL.RISK_INDX_VAL_Y29, 0),
                    XARMLV.RISK_INDX_VAL_Y30 + NVL (INSUL.RISK_INDX_VAL_Y30, 0),
                    XARMLV.RISK_INDX_VAL_Y31 + NVL (INSUL.RISK_INDX_VAL_Y31, 0),
                    XARMLV.RISK_INDX_VAL_Y32 + NVL (INSUL.RISK_INDX_VAL_Y32, 0),
                    XARMLV.RISK_INDX_VAL_Y33 + NVL (INSUL.RISK_INDX_VAL_Y33, 0),
                    XARMLV.RISK_INDX_VAL_Y34 + NVL (INSUL.RISK_INDX_VAL_Y34, 0),
                    XARMLV.RISK_INDX_VAL_Y35 + NVL (INSUL.RISK_INDX_VAL_Y35, 0),
                    XARMLV.RISK_INDX_VAL_Y36 + NVL (INSUL.RISK_INDX_VAL_Y36, 0),
                    XARMLV.RISK_INDX_VAL_Y37 + NVL (INSUL.RISK_INDX_VAL_Y37, 0),
                    XARMLV.RISK_INDX_VAL_Y38 + NVL (INSUL.RISK_INDX_VAL_Y38, 0),
                    XARMLV.RISK_INDX_VAL_Y39 + NVL (INSUL.RISK_INDX_VAL_Y39, 0),
                    XARMLV.RISK_INDX_VAL_Y40 + NVL (INSUL.RISK_INDX_VAL_Y40, 0),
                    XARMLV.RISK_INDX_VAL_Y41 + NVL (INSUL.RISK_INDX_VAL_Y41, 0),
                    XARMLV.RISK_INDX_VAL_Y42 + NVL (INSUL.RISK_INDX_VAL_Y42, 0),
                    XARMLV.RISK_INDX_VAL_Y43 + NVL (INSUL.RISK_INDX_VAL_Y43, 0),
                    XARMLV.RISK_INDX_VAL_Y44 + NVL (INSUL.RISK_INDX_VAL_Y44, 0),
                    XARMLV.RISK_INDX_VAL_Y45 + NVL (INSUL.RISK_INDX_VAL_Y45, 0),
                    XARMLV.RISK_INDX_VAL_Y46 + NVL (INSUL.RISK_INDX_VAL_Y46, 0),
                    XARMLV.RISK_INDX_VAL_Y47 + NVL (INSUL.RISK_INDX_VAL_Y47, 0),
                    XARMLV.RISK_INDX_VAL_Y48 + NVL (INSUL.RISK_INDX_VAL_Y48, 0),
                    XARMLV.RISK_INDX_VAL_Y49 + NVL (INSUL.RISK_INDX_VAL_Y49, 0),
                    XARMLV.RISK_INDX_VAL_Y50 + NVL (INSUL.RISK_INDX_VAL_Y50, 0)
               FROM structools.ST_RISK_XARMLV_REPLACE_NPV15 xarmlv   LEFT OUTER JOIN
                    structools.ST_RISK_INSUL_REPLACE_NPV15 insul
               ON    xarmlv.pick_id = insul.pick_id
            ;

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - XARMLVI (Combined) REPLACE NPV15');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN NPV TABLE ST_RISK_XARMLVI_REPLACE_NPV15 -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMLVI_REPLACE');
      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_XARMLVI_REPLACE_NPV15');

   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END PR_LOAD_RISK_XARMLVI;

   PROCEDURE PR_LOAD_RISK_BAY
   (p_yr_mth_no  IN    CHAR )
   AS
      v_record_cnt   NUMBER := 0;
      v_condfail_fin_conseq_amt  NUMBER;
      v_condgc_fin_conseq_amt    NUMBER;
      v_condvca_fin_conseq_amt   NUMBER;
      v_condca_fin_conseq_amt    NUMBER;
      v_condearth_fin_conseq_amt NUMBER;

      CURSOR get_reactv_cost_cur
      IS
         SELECT TRIM(mdl_bus_cde) AS mdl_bus_cde
               ,trmnt_reactv_cst_amt
         --FROM structoolsw.treatment_reactive_cost
         FROM structools.st_treatment_reactive_cost
      ;

   BEGIN
      DBMS_OUTPUT.put_line (
            'Risk MODEL - BAY (Combined)_DONOTHING Start TIME -'
         || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));

      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_BAY_NOACTION');
      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_BAY_NOACTION_NPV15');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_BAY_NOACTION');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_BAY_NOACTION_NPV15');

      DELETE FROM structools.ST_RISK_FRACTIONS frac
            WHERE FRAC.MODEL_NAME = 'BAY';

      COMMIT;

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_BAY_NOACTION
            (SELECT cond.PICK_ID,
                    (  COND.RISK_INDX_VAL_Y0
                     + NVL (CONDE.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y0, 0)),
                    (  COND.RISK_INDX_VAL_Y1
                     + NVL (CONDE.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y1, 0)),
                    (  COND.RISK_INDX_VAL_Y2
                     + NVL (CONDE.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y2, 0)),
                    (  COND.RISK_INDX_VAL_Y3
                     + NVL (CONDE.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y3, 0)),
                    (  COND.RISK_INDX_VAL_Y4
                     + NVL (CONDE.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y4, 0)),
                    (  COND.RISK_INDX_VAL_Y5
                     + NVL (CONDE.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y5, 0)),
                    (  COND.RISK_INDX_VAL_Y6
                     + NVL (CONDE.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y6, 0)),
                    (  COND.RISK_INDX_VAL_Y7
                     + NVL (CONDE.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y7, 0)),
                    (  COND.RISK_INDX_VAL_Y8
                     + NVL (CONDE.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y8, 0)),
                    (  COND.RISK_INDX_VAL_Y9
                     + NVL (CONDE.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y9, 0)),
                    (  COND.RISK_INDX_VAL_Y10
                     + NVL (CONDE.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y10, 0)),
                    (  COND.RISK_INDX_VAL_Y11
                     + NVL (CONDE.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y11, 0)),
                    (  COND.RISK_INDX_VAL_Y12
                     + NVL (CONDE.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y12, 0)),
                    (  COND.RISK_INDX_VAL_Y13
                     + NVL (CONDE.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y13, 0)),
                    (  COND.RISK_INDX_VAL_Y14
                     + NVL (CONDE.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y14, 0)),
                    (  COND.RISK_INDX_VAL_Y15
                     + NVL (CONDE.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y15, 0)),
                    (  COND.RISK_INDX_VAL_Y16
                     + NVL (CONDE.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y16, 0)),
                    (  COND.RISK_INDX_VAL_Y17
                     + NVL (CONDE.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y17, 0)),
                    (  COND.RISK_INDX_VAL_Y18
                     + NVL (CONDE.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y18, 0)),
                    (  COND.RISK_INDX_VAL_Y19
                     + NVL (CONDE.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y19, 0)),
                    (  COND.RISK_INDX_VAL_Y20
                     + NVL (CONDE.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y20, 0)),
                    (  COND.RISK_INDX_VAL_Y21
                     + NVL (CONDE.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y21, 0)),
                    (  COND.RISK_INDX_VAL_Y22
                     + NVL (CONDE.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y22, 0)),
                    (  COND.RISK_INDX_VAL_Y23
                     + NVL (CONDE.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y23, 0)),
                    (  COND.RISK_INDX_VAL_Y24
                     + NVL (CONDE.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y24, 0)),
                    (  COND.RISK_INDX_VAL_Y25
                     + NVL (CONDE.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y25, 0)),
                    (  COND.RISK_INDX_VAL_Y26
                     + NVL (CONDE.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y26, 0)),
                    (  COND.RISK_INDX_VAL_Y27
                     + NVL (CONDE.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y27, 0)),
                    (  COND.RISK_INDX_VAL_Y28
                     + NVL (CONDE.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y28, 0)),
                    (  COND.RISK_INDX_VAL_Y29
                     + NVL (CONDE.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y29, 0)),
                    (  COND.RISK_INDX_VAL_Y30
                     + NVL (CONDE.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y30, 0)),
                    (  COND.RISK_INDX_VAL_Y31
                     + NVL (CONDE.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y31, 0)),
                    (  COND.RISK_INDX_VAL_Y32
                     + NVL (CONDE.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y32, 0)),
                    (  COND.RISK_INDX_VAL_Y33
                     + NVL (CONDE.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y33, 0)),
                    (  COND.RISK_INDX_VAL_Y34
                     + NVL (CONDE.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y34, 0)),
                    (  COND.RISK_INDX_VAL_Y35
                     + NVL (CONDE.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y35, 0)),
                    (  COND.RISK_INDX_VAL_Y36
                     + NVL (CONDE.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y36, 0)),
                    (  COND.RISK_INDX_VAL_Y37
                     + NVL (CONDE.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y37, 0)),
                    (  COND.RISK_INDX_VAL_Y38
                     + NVL (CONDE.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y38, 0)),
                    (  COND.RISK_INDX_VAL_Y39
                     + NVL (CONDE.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y39, 0)),
                    (  COND.RISK_INDX_VAL_Y40
                     + NVL (CONDE.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y40, 0)),
                    (  COND.RISK_INDX_VAL_Y41
                     + NVL (CONDE.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y41, 0)),
                    (  COND.RISK_INDX_VAL_Y42
                     + NVL (CONDE.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y42, 0)),
                    (  COND.RISK_INDX_VAL_Y43
                     + NVL (CONDE.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y43, 0)),
                    (  COND.RISK_INDX_VAL_Y44
                     + NVL (CONDE.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y44, 0)),
                    (  COND.RISK_INDX_VAL_Y45
                     + NVL (CONDE.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y45, 0)),
                    (  COND.RISK_INDX_VAL_Y46
                     + NVL (CONDE.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y46, 0)),
                    (  COND.RISK_INDX_VAL_Y47
                     + NVL (CONDE.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y47, 0)),
                    (  COND.RISK_INDX_VAL_Y48
                     + NVL (CONDE.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y48, 0)),
                    (  COND.RISK_INDX_VAL_Y49
                     + NVL (CONDE.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y49, 0)),
                    (  COND.RISK_INDX_VAL_Y50
                     + NVL (CONDE.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y50, 0))
               FROM structools.ST_RISK_COND_NOACTION cond,
                    structools.ST_RISK_CONDEAR_NOACTION conde, 
                    structools.ST_RISK_CONDVCA_NOACTION condv,
                    structools.ST_RISK_CONDHCA_NOACTION condh,
                    structools.ST_RISK_CONDGCA_NOACTION condg
              WHERE     cond.PICK_ID = condv.PICK_ID(+)
                    AND cond.PICK_ID = condh.PICK_ID(+)
                    AND cond.PICK_ID = condg.PICK_ID(+)
                    AND cond.PICK_ID = conde.PICK_ID(+));
                    
                    
                    
--                    INSERT INTO structools.ST_RISK_BAY_NOACTION
--                     (SELECT cond.PICK_ID,
--                    (  COND.RISK_INDX_VAL_Y0 + NVL (CONDV.RISK_INDX_VAL_Y0, 0)),
--(  COND.RISK_INDX_VAL_Y1 + NVL (CONDV.RISK_INDX_VAL_Y1, 0)),
--(  COND.RISK_INDX_VAL_Y2 + NVL (CONDV.RISK_INDX_VAL_Y2, 0)),
--(  COND.RISK_INDX_VAL_Y3 + NVL (CONDV.RISK_INDX_VAL_Y3, 0)),
--(  COND.RISK_INDX_VAL_Y4 + NVL (CONDV.RISK_INDX_VAL_Y4, 0)),
--(  COND.RISK_INDX_VAL_Y5 + NVL (CONDV.RISK_INDX_VAL_Y5, 0)),
--(  COND.RISK_INDX_VAL_Y6 + NVL (CONDV.RISK_INDX_VAL_Y6, 0)),
--(  COND.RISK_INDX_VAL_Y7 + NVL (CONDV.RISK_INDX_VAL_Y7, 0)),
--(  COND.RISK_INDX_VAL_Y8 + NVL (CONDV.RISK_INDX_VAL_Y8, 0)),
--(  COND.RISK_INDX_VAL_Y9 + NVL (CONDV.RISK_INDX_VAL_Y9, 0)),
--(  COND.RISK_INDX_VAL_Y10 + NVL (CONDV.RISK_INDX_VAL_Y10, 0)),
--(  COND.RISK_INDX_VAL_Y11 + NVL (CONDV.RISK_INDX_VAL_Y11, 0)),
--(  COND.RISK_INDX_VAL_Y12 + NVL (CONDV.RISK_INDX_VAL_Y12, 0)),
--(  COND.RISK_INDX_VAL_Y13 + NVL (CONDV.RISK_INDX_VAL_Y13, 0)),
--(  COND.RISK_INDX_VAL_Y14 + NVL (CONDV.RISK_INDX_VAL_Y14, 0)),
--(  COND.RISK_INDX_VAL_Y15 + NVL (CONDV.RISK_INDX_VAL_Y15, 0)),
--(  COND.RISK_INDX_VAL_Y16 + NVL (CONDV.RISK_INDX_VAL_Y16, 0)),
--(  COND.RISK_INDX_VAL_Y17 + NVL (CONDV.RISK_INDX_VAL_Y17, 0)),
--(  COND.RISK_INDX_VAL_Y18 + NVL (CONDV.RISK_INDX_VAL_Y18, 0)),
--(  COND.RISK_INDX_VAL_Y19 + NVL (CONDV.RISK_INDX_VAL_Y19, 0)),
--(  COND.RISK_INDX_VAL_Y20 + NVL (CONDV.RISK_INDX_VAL_Y20, 0)),
--(  COND.RISK_INDX_VAL_Y21 + NVL (CONDV.RISK_INDX_VAL_Y21, 0)),
--(  COND.RISK_INDX_VAL_Y22 + NVL (CONDV.RISK_INDX_VAL_Y22, 0)),
--(  COND.RISK_INDX_VAL_Y23 + NVL (CONDV.RISK_INDX_VAL_Y23, 0)),
--(  COND.RISK_INDX_VAL_Y24 + NVL (CONDV.RISK_INDX_VAL_Y24, 0)),
--(  COND.RISK_INDX_VAL_Y25 + NVL (CONDV.RISK_INDX_VAL_Y25, 0)),
--(  COND.RISK_INDX_VAL_Y26 + NVL (CONDV.RISK_INDX_VAL_Y26, 0)),
--(  COND.RISK_INDX_VAL_Y27 + NVL (CONDV.RISK_INDX_VAL_Y27, 0)),
--(  COND.RISK_INDX_VAL_Y28 + NVL (CONDV.RISK_INDX_VAL_Y28, 0)),
--(  COND.RISK_INDX_VAL_Y29 + NVL (CONDV.RISK_INDX_VAL_Y29, 0)),
--(  COND.RISK_INDX_VAL_Y30 + NVL (CONDV.RISK_INDX_VAL_Y30, 0)),
--(  COND.RISK_INDX_VAL_Y31 + NVL (CONDV.RISK_INDX_VAL_Y31, 0)),
--(  COND.RISK_INDX_VAL_Y32 + NVL (CONDV.RISK_INDX_VAL_Y32, 0)),
--(  COND.RISK_INDX_VAL_Y33 + NVL (CONDV.RISK_INDX_VAL_Y33, 0)),
--(  COND.RISK_INDX_VAL_Y34 + NVL (CONDV.RISK_INDX_VAL_Y34, 0)),
--(  COND.RISK_INDX_VAL_Y35 + NVL (CONDV.RISK_INDX_VAL_Y35, 0)),
--(  COND.RISK_INDX_VAL_Y36 + NVL (CONDV.RISK_INDX_VAL_Y36, 0)),
--(  COND.RISK_INDX_VAL_Y37 + NVL (CONDV.RISK_INDX_VAL_Y37, 0)),
--(  COND.RISK_INDX_VAL_Y38 + NVL (CONDV.RISK_INDX_VAL_Y38, 0)),
--(  COND.RISK_INDX_VAL_Y39 + NVL (CONDV.RISK_INDX_VAL_Y39, 0)),
--(  COND.RISK_INDX_VAL_Y40 + NVL (CONDV.RISK_INDX_VAL_Y40, 0)),
--(  COND.RISK_INDX_VAL_Y41 + NVL (CONDV.RISK_INDX_VAL_Y41, 0)),
--(  COND.RISK_INDX_VAL_Y42 + NVL (CONDV.RISK_INDX_VAL_Y42, 0)),
--(  COND.RISK_INDX_VAL_Y43 + NVL (CONDV.RISK_INDX_VAL_Y43, 0)),
--(  COND.RISK_INDX_VAL_Y44 + NVL (CONDV.RISK_INDX_VAL_Y44, 0)),
--(  COND.RISK_INDX_VAL_Y45 + NVL (CONDV.RISK_INDX_VAL_Y45, 0)),
--(  COND.RISK_INDX_VAL_Y46 + NVL (CONDV.RISK_INDX_VAL_Y46, 0)),
--(  COND.RISK_INDX_VAL_Y47 + NVL (CONDV.RISK_INDX_VAL_Y47, 0)),
--(  COND.RISK_INDX_VAL_Y48 + NVL (CONDV.RISK_INDX_VAL_Y48, 0)),
--(  COND.RISK_INDX_VAL_Y49 + NVL (CONDV.RISK_INDX_VAL_Y49, 0)),
--(  COND.RISK_INDX_VAL_Y50 + NVL (CONDV.RISK_INDX_VAL_Y50, 0))
--               FROM structools.ST_RISK_COND_NOACTION cond,
--                    structools.ST_RISK_CONDVCA_NOACTION condv
--            WHERE     cond.PICK_ID = condv.PICK_ID(+)
--                    );

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - BAY (Combined) DONOTHING');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING risk records IN ST_RISK_BAY_NOACTION -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;


      /* load reactive cost of all  models to an associative array, indexed by the model name */
      /* This only needs to be done if this procedure is invoked on its own and not as part of PR_LOAD_ALL_RISK_TABLES procedure */
      IF v_reac_cost_tab.COUNT = 0 THEN
         FOR c IN get_reactv_cost_cur LOOP
            v_reac_cost_tab(c.mdl_bus_cde) := c.trmnt_reactv_cst_amt;
         END LOOP;   
      END IF;

      v_condfail_fin_conseq_amt  := v_reac_cost_tab('CONDFAIL');
      v_condgc_fin_conseq_amt    := v_reac_cost_tab('CONDGC');
      v_condvca_fin_conseq_amt   := v_reac_cost_tab('CONDVCA');
      v_condca_fin_conseq_amt    := v_reac_cost_tab('CONDCA');
      v_condearth_fin_conseq_amt := v_reac_cost_tab('CONDEARTH');

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_BAY_NOACTION_NPV15
            WITH conseq_calc AS   --fin_frac - copy from here to the end of the sql and replace atm.mdl_bus_cde values
            (
            SELECT 
               (CASE
                   WHEN instrc (ed.plnt_no,
                                ' ',
                                1,
                                1) = 0
                   THEN
                      SUBSTR (ed.plnt_no,
                              -1 * (LENGTH (ed.plnt_no) - 1))
                   ELSE
                      SUBSTR (ed.plnt_no,
                              2,
                              instrc (ed.plnt_no,
                                      ' ',
                                      1,
                                      1) - 2)
                END) AS pick_id
               ,nrdmf.hzrd_fn_1_2
               ,nrdmf.FIRE_CNSQNC_AMT
               ,nrdmf.ELEC_SHCK_CNSQNC_AMT
               ,nrdmf.WRK_FRCE_CNSQNC_AMT
               ,nrdmf.PHYS_IMPCT_CNSQNC_AMT
               ,nrdmf.RELIABILITY_CNSQNC_AMT
               ,nrdmf.ENV_CNSQNC_AMT
               ,CASE
                  WHEN atm.mdl_bus_cde = 'CONDFAIL'      THEN v_condfail_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'CONDGC'        THEN v_condgc_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'CONDVCA'       THEN v_condvca_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'CONDCA'        THEN v_condca_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'CONDEARTH'     THEN v_condearth_fin_conseq_amt
               END              AS FIN_CNSQNC_AMT 
               ,CASE                                                                      --fin_fracc - replace
                  WHEN atm.mdl_bus_cde = 'CONDFAIL'      THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_condfail_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'CONDGC'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_condgc_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'CONDVCA'      THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_condvca_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'CONDCA'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_condca_fin_conseq_amt
                  WHEN atm.mdl_bus_cde = 'CONDEARTH'       THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                            + v_condearth_fin_conseq_amt
               END              AS conseq_amt 
            FROM structoolsw.mthly_equip_risk_dm_fact nrdmf
               JOIN
               structoolsw.nrm_asset_type_model_dim atm
                  ON atm.nrm_asset_typ_mdl_sk =
                        nrdmf.nrm_asset_typ_mdl_sk
               JOIN structoolsw.treatment_dim td
                  ON td.trmnt_sk = nrdmf.trmnt_sk
               JOIN structoolsw.equipment_nvgtn_dim en
                  ON en.equip_sk = nrdmf.equip_sk
               JOIN structoolsw.equipment_dim ed
                  ON ed.equip_nvgtn_sk = en.equip_nvgtn_sk
            WHERE     nrdmf.yr_mth_no = p_yr_mth_no
                  AND td.trmnt_nam = 'DONOTHING'
                  AND atm.mdl_bus_cde IN
                         ('CONDFAIL','CONDGC','CONDVCA','CONDCA','CONDEARTH')
                  --AND ed.pick_id = 'S318'
            )
            ,
            frac AS
            (  
            SELECT   pick_id
                    ,SUM (FIRE_CNSQNC_AMT * hzrd_fn_1_2)          /  SUM (hzrd_fn_1_2 * conseq_amt) AS fire_frac
                    ,SUM (ELEC_SHCK_CNSQNC_AMT * hzrd_fn_1_2)     /  SUM (hzrd_fn_1_2 * conseq_amt) AS ESHOCK_frac
                    ,SUM (WRK_FRCE_CNSQNC_AMT * hzrd_fn_1_2)      /  SUM (hzrd_fn_1_2 * conseq_amt) AS WFORCE_frac
                    ,SUM (RELIABILITY_CNSQNC_AMT * hzrd_fn_1_2)   /  SUM (hzrd_fn_1_2 * conseq_amt) AS RELIABILITY_frac
                    ,SUM (PHYS_IMPCT_CNSQNC_AMT * hzrd_fn_1_2)    /  SUM (hzrd_fn_1_2 * conseq_amt) AS PHYSIMP_frac
                    ,SUM (ENV_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS ENV_frac
                    ,SUM (FIN_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS FIN_frac      
             FROM conseq_calc
            GROUP BY pick_id
            )
            SELECT cond.PICK_ID,
                    (  COND.RISK_INDX_VAL_Y0
                     + NVL (CONDE.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y0, 0)),
                    (  COND.RISK_INDX_VAL_Y1
                     + NVL (CONDE.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y1, 0)),
                    (  COND.RISK_INDX_VAL_Y2
                     + NVL (CONDE.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y2, 0)),
                    (  COND.RISK_INDX_VAL_Y3
                     + NVL (CONDE.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y3, 0)),
                    (  COND.RISK_INDX_VAL_Y4
                     + NVL (CONDE.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y4, 0)),
                    (  COND.RISK_INDX_VAL_Y5
                     + NVL (CONDE.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y5, 0)),
                    (  COND.RISK_INDX_VAL_Y6
                     + NVL (CONDE.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y6, 0)),
                    (  COND.RISK_INDX_VAL_Y7
                     + NVL (CONDE.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y7, 0)),
                    (  COND.RISK_INDX_VAL_Y8
                     + NVL (CONDE.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y8, 0)),
                    (  COND.RISK_INDX_VAL_Y9
                     + NVL (CONDE.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y9, 0)),
                    (  COND.RISK_INDX_VAL_Y10
                     + NVL (CONDE.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y10, 0)),
                    (  COND.RISK_INDX_VAL_Y11
                     + NVL (CONDE.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y11, 0)),
                    (  COND.RISK_INDX_VAL_Y12
                     + NVL (CONDE.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y12, 0)),
                    (  COND.RISK_INDX_VAL_Y13
                     + NVL (CONDE.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y13, 0)),
                    (  COND.RISK_INDX_VAL_Y14
                     + NVL (CONDE.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y14, 0)),
                    (  COND.RISK_INDX_VAL_Y15
                     + NVL (CONDE.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y15, 0)),
                    (  COND.RISK_INDX_VAL_Y16
                     + NVL (CONDE.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y16, 0)),
                    (  COND.RISK_INDX_VAL_Y17
                     + NVL (CONDE.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y17, 0)),
                    (  COND.RISK_INDX_VAL_Y18
                     + NVL (CONDE.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y18, 0)),
                    (  COND.RISK_INDX_VAL_Y19
                     + NVL (CONDE.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y19, 0)),
                    (  COND.RISK_INDX_VAL_Y20
                     + NVL (CONDE.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y20, 0)),
                    (  COND.RISK_INDX_VAL_Y21
                     + NVL (CONDE.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y21, 0)),
                    (  COND.RISK_INDX_VAL_Y22
                     + NVL (CONDE.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y22, 0)),
                    (  COND.RISK_INDX_VAL_Y23
                     + NVL (CONDE.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y23, 0)),
                    (  COND.RISK_INDX_VAL_Y24
                     + NVL (CONDE.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y24, 0)),
                    (  COND.RISK_INDX_VAL_Y25
                     + NVL (CONDE.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y25, 0)),
                    (  COND.RISK_INDX_VAL_Y26
                     + NVL (CONDE.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y26, 0)),
                    (  COND.RISK_INDX_VAL_Y27
                     + NVL (CONDE.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y27, 0)),
                    (  COND.RISK_INDX_VAL_Y28
                     + NVL (CONDE.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y28, 0)),
                    (  COND.RISK_INDX_VAL_Y29
                     + NVL (CONDE.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y29, 0)),
                    (  COND.RISK_INDX_VAL_Y30
                     + NVL (CONDE.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y30, 0)),
                    (  COND.RISK_INDX_VAL_Y31
                     + NVL (CONDE.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y31, 0)),
                    (  COND.RISK_INDX_VAL_Y32
                     + NVL (CONDE.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y32, 0)),
                    (  COND.RISK_INDX_VAL_Y33
                     + NVL (CONDE.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y33, 0)),
                    (  COND.RISK_INDX_VAL_Y34
                     + NVL (CONDE.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y34, 0)),
                    (  COND.RISK_INDX_VAL_Y35
                     + NVL (CONDE.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y35, 0)),
                    (  COND.RISK_INDX_VAL_Y36
                     + NVL (CONDE.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y36, 0)),
                    (  COND.RISK_INDX_VAL_Y37
                     + NVL (CONDE.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y37, 0)),
                    (  COND.RISK_INDX_VAL_Y38
                     + NVL (CONDE.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y38, 0)),
                    (  COND.RISK_INDX_VAL_Y39
                     + NVL (CONDE.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y39, 0)),
                    (  COND.RISK_INDX_VAL_Y40
                     + NVL (CONDE.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y40, 0)),
                    (  COND.RISK_INDX_VAL_Y41
                     + NVL (CONDE.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y41, 0)),
                    (  COND.RISK_INDX_VAL_Y42
                     + NVL (CONDE.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y42, 0)),
                    (  COND.RISK_INDX_VAL_Y43
                     + NVL (CONDE.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y43, 0)),
                    (  COND.RISK_INDX_VAL_Y44
                     + NVL (CONDE.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y44, 0)),
                    (  COND.RISK_INDX_VAL_Y45
                     + NVL (CONDE.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y45, 0)),
                    (  COND.RISK_INDX_VAL_Y46
                     + NVL (CONDE.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y46, 0)),
                    (  COND.RISK_INDX_VAL_Y47
                     + NVL (CONDE.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y47, 0)),
                    (  COND.RISK_INDX_VAL_Y48
                     + NVL (CONDE.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y48, 0)),
                    (  COND.RISK_INDX_VAL_Y49
                     + NVL (CONDE.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y49, 0)),
                    (  COND.RISK_INDX_VAL_Y50
                     + NVL (CONDE.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y50, 0)),
                    frac.FIRE_FRAC,
                    frac.ESHOCK_FRAC,
                    frac.WFORCE_FRAC,
                    frac.RELIABILITY_FRAC,
                    frac.PHYSIMP_FRAC,
                    frac.ENV_FRAC,
                    frac.fin_frac
               FROM  structools.ST_RISK_COND_NOACTION_NPV15 cond     LEFT OUTER JOIN
                     structools.ST_RISK_CONDEAR_NOACTION_NPV15 conde
                     ON cond.pick_id = conde.pick_id
                                                                  LEFT OUTER JOIN
                     structools.ST_RISK_CONDVCA_NOACTION_NPV15 condv
                     ON cond.pick_id = condv.pick_id                                                                     
                                                                  LEFT OUTER JOIN
                     structools.ST_RISK_CONDHCA_NOACTION_NPV15 condh
                     ON cond.pick_id = condh.pick_id                                                                     
                                                                  LEFT OUTER JOIN
                     structools.ST_RISK_CONDGCA_NOACTION_NPV15 condg
                     ON cond.pick_id = condg.pick_id                                                                     
                                                                  LEFT OUTER JOIN
                     frac
                     ON cond.pick_id = frac.pick_id
         ;
                    

--  INSERT INTO structools.ST_RISK_BAY_NOACTION_NPV15
--            (SELECT cond.PICK_ID,
--                    (  COND.RISK_INDX_VAL_Y0 + NVL (CONDV.RISK_INDX_VAL_Y0, 0)),
--(  COND.RISK_INDX_VAL_Y1 + NVL (CONDV.RISK_INDX_VAL_Y1, 0)),
--(  COND.RISK_INDX_VAL_Y2 + NVL (CONDV.RISK_INDX_VAL_Y2, 0)),
--(  COND.RISK_INDX_VAL_Y3 + NVL (CONDV.RISK_INDX_VAL_Y3, 0)),
--(  COND.RISK_INDX_VAL_Y4 + NVL (CONDV.RISK_INDX_VAL_Y4, 0)),
--(  COND.RISK_INDX_VAL_Y5 + NVL (CONDV.RISK_INDX_VAL_Y5, 0)),
--(  COND.RISK_INDX_VAL_Y6 + NVL (CONDV.RISK_INDX_VAL_Y6, 0)),
--(  COND.RISK_INDX_VAL_Y7 + NVL (CONDV.RISK_INDX_VAL_Y7, 0)),
--(  COND.RISK_INDX_VAL_Y8 + NVL (CONDV.RISK_INDX_VAL_Y8, 0)),
--(  COND.RISK_INDX_VAL_Y9 + NVL (CONDV.RISK_INDX_VAL_Y9, 0)),
--(  COND.RISK_INDX_VAL_Y10 + NVL (CONDV.RISK_INDX_VAL_Y10, 0)),
--(  COND.RISK_INDX_VAL_Y11 + NVL (CONDV.RISK_INDX_VAL_Y11, 0)),
--(  COND.RISK_INDX_VAL_Y12 + NVL (CONDV.RISK_INDX_VAL_Y12, 0)),
--(  COND.RISK_INDX_VAL_Y13 + NVL (CONDV.RISK_INDX_VAL_Y13, 0)),
--(  COND.RISK_INDX_VAL_Y14 + NVL (CONDV.RISK_INDX_VAL_Y14, 0)),
--(  COND.RISK_INDX_VAL_Y15 + NVL (CONDV.RISK_INDX_VAL_Y15, 0)),
--(  COND.RISK_INDX_VAL_Y16 + NVL (CONDV.RISK_INDX_VAL_Y16, 0)),
--(  COND.RISK_INDX_VAL_Y17 + NVL (CONDV.RISK_INDX_VAL_Y17, 0)),
--(  COND.RISK_INDX_VAL_Y18 + NVL (CONDV.RISK_INDX_VAL_Y18, 0)),
--(  COND.RISK_INDX_VAL_Y19 + NVL (CONDV.RISK_INDX_VAL_Y19, 0)),
--(  COND.RISK_INDX_VAL_Y20 + NVL (CONDV.RISK_INDX_VAL_Y20, 0)),
--(  COND.RISK_INDX_VAL_Y21 + NVL (CONDV.RISK_INDX_VAL_Y21, 0)),
--(  COND.RISK_INDX_VAL_Y22 + NVL (CONDV.RISK_INDX_VAL_Y22, 0)),
--(  COND.RISK_INDX_VAL_Y23 + NVL (CONDV.RISK_INDX_VAL_Y23, 0)),
--(  COND.RISK_INDX_VAL_Y24 + NVL (CONDV.RISK_INDX_VAL_Y24, 0)),
--(  COND.RISK_INDX_VAL_Y25 + NVL (CONDV.RISK_INDX_VAL_Y25, 0)),
--(  COND.RISK_INDX_VAL_Y26 + NVL (CONDV.RISK_INDX_VAL_Y26, 0)),
--(  COND.RISK_INDX_VAL_Y27 + NVL (CONDV.RISK_INDX_VAL_Y27, 0)),
--(  COND.RISK_INDX_VAL_Y28 + NVL (CONDV.RISK_INDX_VAL_Y28, 0)),
--(  COND.RISK_INDX_VAL_Y29 + NVL (CONDV.RISK_INDX_VAL_Y29, 0)),
--(  COND.RISK_INDX_VAL_Y30 + NVL (CONDV.RISK_INDX_VAL_Y30, 0)),
--(  COND.RISK_INDX_VAL_Y31 + NVL (CONDV.RISK_INDX_VAL_Y31, 0)),
--(  COND.RISK_INDX_VAL_Y32 + NVL (CONDV.RISK_INDX_VAL_Y32, 0)),
--(  COND.RISK_INDX_VAL_Y33 + NVL (CONDV.RISK_INDX_VAL_Y33, 0)),
--(  COND.RISK_INDX_VAL_Y34 + NVL (CONDV.RISK_INDX_VAL_Y34, 0)),
--(  COND.RISK_INDX_VAL_Y35 + NVL (CONDV.RISK_INDX_VAL_Y35, 0)),
--(  COND.RISK_INDX_VAL_Y36 + NVL (CONDV.RISK_INDX_VAL_Y36, 0)),
--(  COND.RISK_INDX_VAL_Y37 + NVL (CONDV.RISK_INDX_VAL_Y37, 0)),
--(  COND.RISK_INDX_VAL_Y38 + NVL (CONDV.RISK_INDX_VAL_Y38, 0)),
--(  COND.RISK_INDX_VAL_Y39 + NVL (CONDV.RISK_INDX_VAL_Y39, 0)),
--(  COND.RISK_INDX_VAL_Y40 + NVL (CONDV.RISK_INDX_VAL_Y40, 0)),
--(  COND.RISK_INDX_VAL_Y41 + NVL (CONDV.RISK_INDX_VAL_Y41, 0)),
--(  COND.RISK_INDX_VAL_Y42 + NVL (CONDV.RISK_INDX_VAL_Y42, 0)),
--(  COND.RISK_INDX_VAL_Y43 + NVL (CONDV.RISK_INDX_VAL_Y43, 0)),
--(  COND.RISK_INDX_VAL_Y44 + NVL (CONDV.RISK_INDX_VAL_Y44, 0)),
--(  COND.RISK_INDX_VAL_Y45 + NVL (CONDV.RISK_INDX_VAL_Y45, 0)),
--(  COND.RISK_INDX_VAL_Y46 + NVL (CONDV.RISK_INDX_VAL_Y46, 0)),
--(  COND.RISK_INDX_VAL_Y47 + NVL (CONDV.RISK_INDX_VAL_Y47, 0)),
--(  COND.RISK_INDX_VAL_Y48 + NVL (CONDV.RISK_INDX_VAL_Y48, 0)),
--(  COND.RISK_INDX_VAL_Y49 + NVL (CONDV.RISK_INDX_VAL_Y49, 0)),
--(  COND.RISK_INDX_VAL_Y50 + NVL (CONDV.RISK_INDX_VAL_Y50, 0)),
--                    frac.FIRE_FRAC,
--                    frac.ESHOCK_FRAC,
--                    frac.WFORCE_FRAC,
--                    frac.RELIABILITY_FRAC,
--                    frac.PHYSIMP_FRAC,
--                    frac.ENV_FRAC
--               FROM structools.ST_RISK_COND_NOACTION_NPV15 cond,
--                    structools.ST_RISK_CONDVCA_NOACTION_NPV15 condv,
--                    (  SELECT (CASE
--                                  WHEN instrc (ed.plnt_no,
--                                               ' ',
--                                               1,
--                                               1) = 0
--                                  THEN
--                                     SUBSTR (ed.plnt_no,
--                                             -1 * (LENGTH (ed.plnt_no) - 1))
--                                  ELSE
--                                     SUBSTR (ed.plnt_no,
--                                             2,
--                                             instrc (ed.plnt_no,
--                                                     ' ',
--                                                     1,
--                                                     1) - 2)
--                               END)
--                                 pick_id,
--                                 SUM (FIRE_CNSQNC_AMT * hzrd_fn_1_2   )
--                              /  SUM (hzrd_fn_1_2   * (FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT ))
--                                 AS fire_frac,
--                                SUM (ELEC_SHCK_CNSQNC_AMT * hzrd_fn_1_2 )
--                              /  SUM (hzrd_fn_1_2   * (FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT ))
--                                 AS ESHOCK_FRAC,
--                                SUM (WRK_FRCE_CNSQNC_AMT * hzrd_fn_1_2 )
--                              / SUM (hzrd_fn_1_2   * (FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT ))
--                                 AS WFORCE_FRAC,
--                                SUM (RELIABILITY_CNSQNC_AMT * hzrd_fn_1_2 )
--                              / SUM (hzrd_fn_1_2   * (FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT ))
--                                 AS RELIABILITY_frac,
--                                SUM (PHYS_IMPCT_CNSQNC_AMT * hzrd_fn_1_2 )
--                              / SUM (hzrd_fn_1_2   * (FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT ))
--                                 AS PHYSIMP_FRAC,
--                                 SUM (ENV_CNSQNC_AMT * hzrd_fn_1_2 )
--                              / SUM (hzrd_fn_1_2   * (FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT ))
--                                 AS ENV_frac
--                         FROM structoolsw.mthly_equip_risk_dm_fact nrdmf
--                              JOIN
--                              structoolsw.nrm_asset_type_model_dim atm
--                                 ON atm.nrm_asset_typ_mdl_sk =
--                                       nrdmf.nrm_asset_typ_mdl_sk
--                              JOIN structoolsw.treatment_dim td
--                                 ON td.trmnt_sk = nrdmf.trmnt_sk
--                              JOIN structoolsw.equipment_nvgtn_dim en
--                                 ON en.equip_sk = nrdmf.equip_sk
--                              JOIN structoolsw.equipment_dim ed
--                                 ON ed.equip_nvgtn_sk = en.equip_nvgtn_sk
--                        WHERE     nrdmf.yr_mth_no = p_yr_mth_no
--                              AND td.trmnt_nam = 'DONOTHING'
--                              AND hzrd_fn_1_2 <> 0
--                              AND atm.mdl_bus_cde IN
--                                     ('CONDFAIL', 'CONDVCA')-- 'CONDGC',  'CONDCA')
--                     --and ed.equip_no = '000001000022'--in ('000020132622', '000001824246')
--                     --and ed.plnt_no = 'S307'
--                     GROUP BY (CASE
--                                  WHEN instrc (ed.plnt_no,
--                                               ' ',
--                                               1,
--                                               1) = 0
--                                  THEN
--                                     SUBSTR (ed.plnt_no,
--                                             -1 * (LENGTH (ed.plnt_no) - 1))
--                                  ELSE
--                                     SUBSTR (ed.plnt_no,
--                                             2,
--                                             instrc (ed.plnt_no,
--                                                     ' ',
--                                                     1,
--                                                     1) - 2)
--                               END)) frac
--              WHERE     cond.PICK_ID = condv.PICK_ID(+)
--                    AND cond.PICK_ID = frac.pick_id(+));


         v_record_cnt := SQL%ROWCOUNT;

         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - BAY (Combined) DONOTHING NPV15');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN NPV TABLE ST_RISK_BAY_NOACTION_NPV15 -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      BEGIN
         INSERT INTO structools.ST_RISK_FRACTIONS
            WITH conseq_calc AS   --fin_frac - copy from here to the end of the sql and replace atm.mdl_bus_cde values
            (
            SELECT 'B' || (CASE
                      WHEN instrc (ed.plnt_no,
                                   ' ',
                                   1,
                                   1) = 0
                      THEN
                         SUBSTR (ed.plnt_no,
                                 -1 * (LENGTH (ed.plnt_no) - 1))
                      ELSE
                         SUBSTR (ed.plnt_no,
                                 2,
                                 instrc (ed.plnt_no,
                                         ' ',
                                         1,
                                         1) - 2)
                   END) AS pick_id
                  ,nrdmf.hzrd_fn_1_2
                  ,nrdmf.FIRE_CNSQNC_AMT
                  ,nrdmf.ELEC_SHCK_CNSQNC_AMT
                  ,nrdmf.WRK_FRCE_CNSQNC_AMT
                  ,nrdmf.PHYS_IMPCT_CNSQNC_AMT
                  ,nrdmf.RELIABILITY_CNSQNC_AMT
                  ,nrdmf.ENV_CNSQNC_AMT
                  ,CASE
                     WHEN atm.mdl_bus_cde = 'CONDFAIL'      THEN v_condfail_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'CONDGC'        THEN v_condgc_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'CONDVCA'       THEN v_condvca_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'CONDCA'        THEN v_condca_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'CONDEARTH'     THEN v_condearth_fin_conseq_amt
                  END              AS FIN_CNSQNC_AMT 
                  ,CASE                                                                      --fin_fracc - replace
                     WHEN atm.mdl_bus_cde = 'CONDFAIL'    THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_condfail_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'CONDGC'      THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_condgc_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'CONDVCA'     THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_condvca_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'CONDCA'      THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_condca_fin_conseq_amt
                     WHEN atm.mdl_bus_cde = 'CONDEARTH'   THEN FIRE_CNSQNC_AMT + ELEC_SHCK_CNSQNC_AMT +  WRK_FRCE_CNSQNC_AMT + PHYS_IMPCT_CNSQNC_AMT + RELIABILITY_CNSQNC_AMT + ENV_CNSQNC_AMT
                                                               + v_condearth_fin_conseq_amt
                  END              AS conseq_amt 
             FROM structoolsw.mthly_equip_risk_dm_fact nrdmf
                  JOIN
                  structoolsw.nrm_asset_type_model_dim atm
                     ON atm.nrm_asset_typ_mdl_sk =
                           nrdmf.nrm_asset_typ_mdl_sk
                  JOIN structoolsw.treatment_dim td
                     ON td.trmnt_sk = nrdmf.trmnt_sk
                  JOIN structoolsw.equipment_nvgtn_dim en
                     ON en.equip_sk = nrdmf.equip_sk
                  JOIN structoolsw.equipment_dim ed
                     ON ed.equip_nvgtn_sk = en.equip_nvgtn_sk
            WHERE     nrdmf.yr_mth_no = p_yr_mth_no
                  AND td.trmnt_nam = 'DONOTHING'
                  AND atm.mdl_bus_cde IN
                         ('CONDFAIL','CONDGC','CONDVCA','CONDCA','CONDEARTH')
                  --AND ed.pick_id = 'S318'
            )  
            SELECT   pick_id
                     ,'BAY'                                    -- model_type
                     ,0                                          -- FIRE_INCD
                     ,0                                    -- ELEC_SHOCK_INCD
                     ,0                                    --LRGE_RLBLTY_INCD
                     ,0                            --ELEC_SHOCK_FATALITY_INCD
                     ,0                                 -- FIRE_FATALITY_INCD
                    ,SUM (FIRE_CNSQNC_AMT * hzrd_fn_1_2)          /  SUM (hzrd_fn_1_2 * conseq_amt) AS fire_frac
                    ,SUM (ELEC_SHCK_CNSQNC_AMT * hzrd_fn_1_2)     /  SUM (hzrd_fn_1_2 * conseq_amt) AS ESHOCK_frac
                    ,SUM (WRK_FRCE_CNSQNC_AMT * hzrd_fn_1_2)      /  SUM (hzrd_fn_1_2 * conseq_amt) AS WFORCE_frac
                    ,SUM (RELIABILITY_CNSQNC_AMT * hzrd_fn_1_2)   /  SUM (hzrd_fn_1_2 * conseq_amt) AS RELIABILITY_frac
                    ,SUM (PHYS_IMPCT_CNSQNC_AMT * hzrd_fn_1_2)    /  SUM (hzrd_fn_1_2 * conseq_amt) AS PHYSIMP_frac
                    ,SUM (ENV_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS ENV_frac
                    ,SUM (FIN_CNSQNC_AMT * hzrd_fn_1_2)           /  SUM (hzrd_fn_1_2 * conseq_amt) AS FIN_frac      
             FROM conseq_calc
             GROUP BY pick_id
             ;

         v_record_cnt := SQL%ROWCOUNT;

         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - BAY (Combined) ST_RISK_FRACTIONS');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN TABLE ST_RISK_FRACTIONS FOR BAY-'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_BAY_NOACTION');
      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_BAY_NOACTION_NPV15');

      DBMS_OUTPUT.put_line (
            'Risk MODEL - BAY (Combined)_REPLACE Start TIME -'
         || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));

      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_BAY_REPLACE');
      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_BAY_REPLACE_NPV15');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_BAY_REPLACE');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_BAY_REPLACE_NPV15');
      
      COMMIT;

      BEGIN
        
       INSERT /*+ APPEND */ INTO structools.ST_RISK_BAY_REPLACE
            (SELECT cond.PICK_ID,
                    (  COND.RISK_INDX_VAL_Y0
                     + NVL (CONDE.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y0, 0)),
                    (  COND.RISK_INDX_VAL_Y1
                     + NVL (CONDE.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y1, 0)),
                    (  COND.RISK_INDX_VAL_Y2
                     + NVL (CONDE.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y2, 0)),
                    (  COND.RISK_INDX_VAL_Y3
                     + NVL (CONDE.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y3, 0)),
                    (  COND.RISK_INDX_VAL_Y4
                     + NVL (CONDE.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y4, 0)),
                    (  COND.RISK_INDX_VAL_Y5
                     + NVL (CONDE.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y5, 0)),
                    (  COND.RISK_INDX_VAL_Y6
                     + NVL (CONDE.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y6, 0)),
                    (  COND.RISK_INDX_VAL_Y7
                     + NVL (CONDE.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y7, 0)),
                    (  COND.RISK_INDX_VAL_Y8
                     + NVL (CONDE.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y8, 0)),
                    (  COND.RISK_INDX_VAL_Y9
                     + NVL (CONDE.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y9, 0)),
                    (  COND.RISK_INDX_VAL_Y10
                     + NVL (CONDE.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y10, 0)),
                    (  COND.RISK_INDX_VAL_Y11
                     + NVL (CONDE.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y11, 0)),
                    (  COND.RISK_INDX_VAL_Y12
                     + NVL (CONDE.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y12, 0)),
                    (  COND.RISK_INDX_VAL_Y13
                     + NVL (CONDE.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y13, 0)),
                    (  COND.RISK_INDX_VAL_Y14
                     + NVL (CONDE.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y14, 0)),
                    (  COND.RISK_INDX_VAL_Y15
                     + NVL (CONDE.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y15, 0)),
                    (  COND.RISK_INDX_VAL_Y16
                     + NVL (CONDE.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y16, 0)),
                    (  COND.RISK_INDX_VAL_Y17
                     + NVL (CONDE.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y17, 0)),
                    (  COND.RISK_INDX_VAL_Y18
                     + NVL (CONDE.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y18, 0)),
                    (  COND.RISK_INDX_VAL_Y19
                     + NVL (CONDE.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y19, 0)),
                    (  COND.RISK_INDX_VAL_Y20
                     + NVL (CONDE.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y20, 0)),
                    (  COND.RISK_INDX_VAL_Y21
                     + NVL (CONDE.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y21, 0)),
                    (  COND.RISK_INDX_VAL_Y22
                     + NVL (CONDE.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y22, 0)),
                    (  COND.RISK_INDX_VAL_Y23
                     + NVL (CONDE.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y23, 0)),
                    (  COND.RISK_INDX_VAL_Y24
                     + NVL (CONDE.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y24, 0)),
                    (  COND.RISK_INDX_VAL_Y25
                     + NVL (CONDE.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y25, 0)),
                    (  COND.RISK_INDX_VAL_Y26
                     + NVL (CONDE.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y26, 0)),
                    (  COND.RISK_INDX_VAL_Y27
                     + NVL (CONDE.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y27, 0)),
                    (  COND.RISK_INDX_VAL_Y28
                     + NVL (CONDE.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y28, 0)),
                    (  COND.RISK_INDX_VAL_Y29
                     + NVL (CONDE.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y29, 0)),
                    (  COND.RISK_INDX_VAL_Y30
                     + NVL (CONDE.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y30, 0)),
                    (  COND.RISK_INDX_VAL_Y31
                     + NVL (CONDE.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y31, 0)),
                    (  COND.RISK_INDX_VAL_Y32
                     + NVL (CONDE.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y32, 0)),
                    (  COND.RISK_INDX_VAL_Y33
                     + NVL (CONDE.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y33, 0)),
                    (  COND.RISK_INDX_VAL_Y34
                     + NVL (CONDE.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y34, 0)),
                    (  COND.RISK_INDX_VAL_Y35
                     + NVL (CONDE.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y35, 0)),
                    (  COND.RISK_INDX_VAL_Y36
                     + NVL (CONDE.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y36, 0)),
                    (  COND.RISK_INDX_VAL_Y37
                     + NVL (CONDE.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y37, 0)),
                    (  COND.RISK_INDX_VAL_Y38
                     + NVL (CONDE.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y38, 0)),
                    (  COND.RISK_INDX_VAL_Y39
                     + NVL (CONDE.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y39, 0)),
                    (  COND.RISK_INDX_VAL_Y40
                     + NVL (CONDE.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y40, 0)),
                    (  COND.RISK_INDX_VAL_Y41
                     + NVL (CONDE.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y41, 0)),
                    (  COND.RISK_INDX_VAL_Y42
                     + NVL (CONDE.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y42, 0)),
                    (  COND.RISK_INDX_VAL_Y43
                     + NVL (CONDE.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y43, 0)),
                    (  COND.RISK_INDX_VAL_Y44
                     + NVL (CONDE.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y44, 0)),
                    (  COND.RISK_INDX_VAL_Y45
                     + NVL (CONDE.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y45, 0)),
                    (  COND.RISK_INDX_VAL_Y46
                     + NVL (CONDE.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y46, 0)),
                    (  COND.RISK_INDX_VAL_Y47
                     + NVL (CONDE.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y47, 0)),
                    (  COND.RISK_INDX_VAL_Y48
                     + NVL (CONDE.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y48, 0)),
                    (  COND.RISK_INDX_VAL_Y49
                     + NVL (CONDE.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y49, 0)),
                    (  COND.RISK_INDX_VAL_Y50
                     + NVL (CONDE.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y50, 0))
               FROM structools.ST_RISK_COND_REPLACE cond,
                    structools.ST_RISK_CONDEAR_REPLACE conde,
                    structools.ST_RISK_CONDVCA_REPLACE condv,
                    structools.ST_RISK_CONDHCA_REPLACE condh,
                    structools.ST_RISK_CONDGCA_REPLACE condg
              WHERE     cond.PICK_ID = condv.PICK_ID(+)
                    AND cond.PICK_ID = conde.PICK_ID(+)
                    AND cond.PICK_ID = condh.PICK_ID(+)
                    AND cond.PICK_ID = condg.PICK_ID(+)); 
                    
                    
--                    INSERT INTO structools.ST_RISK_BAY_REPLACE
--            (SELECT cond.PICK_ID,
--                     (  COND.RISK_INDX_VAL_Y0 + NVL (CONDV.RISK_INDX_VAL_Y0, 0)),
--(  COND.RISK_INDX_VAL_Y1 + NVL (CONDV.RISK_INDX_VAL_Y1, 0)),
--(  COND.RISK_INDX_VAL_Y2 + NVL (CONDV.RISK_INDX_VAL_Y2, 0)),
--(  COND.RISK_INDX_VAL_Y3 + NVL (CONDV.RISK_INDX_VAL_Y3, 0)),
--(  COND.RISK_INDX_VAL_Y4 + NVL (CONDV.RISK_INDX_VAL_Y4, 0)),
--(  COND.RISK_INDX_VAL_Y5 + NVL (CONDV.RISK_INDX_VAL_Y5, 0)),
--(  COND.RISK_INDX_VAL_Y6 + NVL (CONDV.RISK_INDX_VAL_Y6, 0)),
--(  COND.RISK_INDX_VAL_Y7 + NVL (CONDV.RISK_INDX_VAL_Y7, 0)),
--(  COND.RISK_INDX_VAL_Y8 + NVL (CONDV.RISK_INDX_VAL_Y8, 0)),
--(  COND.RISK_INDX_VAL_Y9 + NVL (CONDV.RISK_INDX_VAL_Y9, 0)),
--(  COND.RISK_INDX_VAL_Y10 + NVL (CONDV.RISK_INDX_VAL_Y10, 0)),
--(  COND.RISK_INDX_VAL_Y11 + NVL (CONDV.RISK_INDX_VAL_Y11, 0)),
--(  COND.RISK_INDX_VAL_Y12 + NVL (CONDV.RISK_INDX_VAL_Y12, 0)),
--(  COND.RISK_INDX_VAL_Y13 + NVL (CONDV.RISK_INDX_VAL_Y13, 0)),
--(  COND.RISK_INDX_VAL_Y14 + NVL (CONDV.RISK_INDX_VAL_Y14, 0)),
--(  COND.RISK_INDX_VAL_Y15 + NVL (CONDV.RISK_INDX_VAL_Y15, 0)),
--(  COND.RISK_INDX_VAL_Y16 + NVL (CONDV.RISK_INDX_VAL_Y16, 0)),
--(  COND.RISK_INDX_VAL_Y17 + NVL (CONDV.RISK_INDX_VAL_Y17, 0)),
--(  COND.RISK_INDX_VAL_Y18 + NVL (CONDV.RISK_INDX_VAL_Y18, 0)),
--(  COND.RISK_INDX_VAL_Y19 + NVL (CONDV.RISK_INDX_VAL_Y19, 0)),
--(  COND.RISK_INDX_VAL_Y20 + NVL (CONDV.RISK_INDX_VAL_Y20, 0)),
--(  COND.RISK_INDX_VAL_Y21 + NVL (CONDV.RISK_INDX_VAL_Y21, 0)),
--(  COND.RISK_INDX_VAL_Y22 + NVL (CONDV.RISK_INDX_VAL_Y22, 0)),
--(  COND.RISK_INDX_VAL_Y23 + NVL (CONDV.RISK_INDX_VAL_Y23, 0)),
--(  COND.RISK_INDX_VAL_Y24 + NVL (CONDV.RISK_INDX_VAL_Y24, 0)),
--(  COND.RISK_INDX_VAL_Y25 + NVL (CONDV.RISK_INDX_VAL_Y25, 0)),
--(  COND.RISK_INDX_VAL_Y26 + NVL (CONDV.RISK_INDX_VAL_Y26, 0)),
--(  COND.RISK_INDX_VAL_Y27 + NVL (CONDV.RISK_INDX_VAL_Y27, 0)),
--(  COND.RISK_INDX_VAL_Y28 + NVL (CONDV.RISK_INDX_VAL_Y28, 0)),
--(  COND.RISK_INDX_VAL_Y29 + NVL (CONDV.RISK_INDX_VAL_Y29, 0)),
--(  COND.RISK_INDX_VAL_Y30 + NVL (CONDV.RISK_INDX_VAL_Y30, 0)),
--(  COND.RISK_INDX_VAL_Y31 + NVL (CONDV.RISK_INDX_VAL_Y31, 0)),
--(  COND.RISK_INDX_VAL_Y32 + NVL (CONDV.RISK_INDX_VAL_Y32, 0)),
--(  COND.RISK_INDX_VAL_Y33 + NVL (CONDV.RISK_INDX_VAL_Y33, 0)),
--(  COND.RISK_INDX_VAL_Y34 + NVL (CONDV.RISK_INDX_VAL_Y34, 0)),
--(  COND.RISK_INDX_VAL_Y35 + NVL (CONDV.RISK_INDX_VAL_Y35, 0)),
--(  COND.RISK_INDX_VAL_Y36 + NVL (CONDV.RISK_INDX_VAL_Y36, 0)),
--(  COND.RISK_INDX_VAL_Y37 + NVL (CONDV.RISK_INDX_VAL_Y37, 0)),
--(  COND.RISK_INDX_VAL_Y38 + NVL (CONDV.RISK_INDX_VAL_Y38, 0)),
--(  COND.RISK_INDX_VAL_Y39 + NVL (CONDV.RISK_INDX_VAL_Y39, 0)),
--(  COND.RISK_INDX_VAL_Y40 + NVL (CONDV.RISK_INDX_VAL_Y40, 0)),
--(  COND.RISK_INDX_VAL_Y41 + NVL (CONDV.RISK_INDX_VAL_Y41, 0)),
--(  COND.RISK_INDX_VAL_Y42 + NVL (CONDV.RISK_INDX_VAL_Y42, 0)),
--(  COND.RISK_INDX_VAL_Y43 + NVL (CONDV.RISK_INDX_VAL_Y43, 0)),
--(  COND.RISK_INDX_VAL_Y44 + NVL (CONDV.RISK_INDX_VAL_Y44, 0)),
--(  COND.RISK_INDX_VAL_Y45 + NVL (CONDV.RISK_INDX_VAL_Y45, 0)),
--(  COND.RISK_INDX_VAL_Y46 + NVL (CONDV.RISK_INDX_VAL_Y46, 0)),
--(  COND.RISK_INDX_VAL_Y47 + NVL (CONDV.RISK_INDX_VAL_Y47, 0)),
--(  COND.RISK_INDX_VAL_Y48 + NVL (CONDV.RISK_INDX_VAL_Y48, 0)),
--(  COND.RISK_INDX_VAL_Y49 + NVL (CONDV.RISK_INDX_VAL_Y49, 0)),
--(  COND.RISK_INDX_VAL_Y50 + NVL (CONDV.RISK_INDX_VAL_Y50, 0))
--               FROM structools.ST_RISK_COND_REPLACE cond,
--                    structools.ST_RISK_CONDVCA_REPLACE condv
--                    WHERE     cond.PICK_ID = condv.PICK_ID(+)
--                    ); 
                    

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - BAY (Combined) REPLACE');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING risk records IN ST_RISK_BAY_REPLACE -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;


      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_BAY_REPLACE_NPV15
            SELECT cond.PICK_ID,
                    (  COND.RISK_INDX_VAL_Y0
                     + NVL (CONDE.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y0, 0)),
                    (  COND.RISK_INDX_VAL_Y1
                     + NVL (CONDE.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y1, 0)),
                    (  COND.RISK_INDX_VAL_Y2
                     + NVL (CONDE.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y2, 0)),
                    (  COND.RISK_INDX_VAL_Y3
                     + NVL (CONDE.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y3, 0)),
                    (  COND.RISK_INDX_VAL_Y4
                     + NVL (CONDE.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y4, 0)),
                    (  COND.RISK_INDX_VAL_Y5
                     + NVL (CONDE.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y5, 0)),
                    (  COND.RISK_INDX_VAL_Y6
                     + NVL (CONDE.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y6, 0)),
                    (  COND.RISK_INDX_VAL_Y7
                     + NVL (CONDE.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y7, 0)),
                    (  COND.RISK_INDX_VAL_Y8
                     + NVL (CONDE.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y8, 0)),
                    (  COND.RISK_INDX_VAL_Y9
                     + NVL (CONDE.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y9, 0)),
                    (  COND.RISK_INDX_VAL_Y10
                     + NVL (CONDE.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y10, 0)),
                    (  COND.RISK_INDX_VAL_Y11
                     + NVL (CONDE.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y11, 0)),
                    (  COND.RISK_INDX_VAL_Y12
                     + NVL (CONDE.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y12, 0)),
                    (  COND.RISK_INDX_VAL_Y13
                     + NVL (CONDE.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y13, 0)),
                    (  COND.RISK_INDX_VAL_Y14
                     + NVL (CONDE.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y14, 0)),
                    (  COND.RISK_INDX_VAL_Y15
                     + NVL (CONDE.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y15, 0)),
                    (  COND.RISK_INDX_VAL_Y16
                     + NVL (CONDE.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y16, 0)),
                    (  COND.RISK_INDX_VAL_Y17
                     + NVL (CONDE.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y17, 0)),
                    (  COND.RISK_INDX_VAL_Y18
                     + NVL (CONDE.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y18, 0)),
                    (  COND.RISK_INDX_VAL_Y19
                     + NVL (CONDE.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y19, 0)),
                    (  COND.RISK_INDX_VAL_Y20
                     + NVL (CONDE.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y20, 0)),
                    (  COND.RISK_INDX_VAL_Y21
                     + NVL (CONDE.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y21, 0)),
                    (  COND.RISK_INDX_VAL_Y22
                     + NVL (CONDE.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y22, 0)),
                    (  COND.RISK_INDX_VAL_Y23
                     + NVL (CONDE.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y23, 0)),
                    (  COND.RISK_INDX_VAL_Y24
                     + NVL (CONDE.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y24, 0)),
                    (  COND.RISK_INDX_VAL_Y25
                     + NVL (CONDE.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y25, 0)),
                    (  COND.RISK_INDX_VAL_Y26
                     + NVL (CONDE.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y26, 0)),
                    (  COND.RISK_INDX_VAL_Y27
                     + NVL (CONDE.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y27, 0)),
                    (  COND.RISK_INDX_VAL_Y28
                     + NVL (CONDE.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y28, 0)),
                    (  COND.RISK_INDX_VAL_Y29
                     + NVL (CONDE.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y29, 0)),
                    (  COND.RISK_INDX_VAL_Y30
                     + NVL (CONDE.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y30, 0)),
                    (  COND.RISK_INDX_VAL_Y31
                     + NVL (CONDE.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y31, 0)),
                    (  COND.RISK_INDX_VAL_Y32
                     + NVL (CONDE.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y32, 0)),
                    (  COND.RISK_INDX_VAL_Y33
                     + NVL (CONDE.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y33, 0)),
                    (  COND.RISK_INDX_VAL_Y34
                     + NVL (CONDE.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y34, 0)),
                    (  COND.RISK_INDX_VAL_Y35
                     + NVL (CONDE.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y35, 0)),
                    (  COND.RISK_INDX_VAL_Y36
                     + NVL (CONDE.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y36, 0)),
                    (  COND.RISK_INDX_VAL_Y37
                     + NVL (CONDE.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y37, 0)),
                    (  COND.RISK_INDX_VAL_Y38
                     + NVL (CONDE.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y38, 0)),
                    (  COND.RISK_INDX_VAL_Y39
                     + NVL (CONDE.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y39, 0)),
                    (  COND.RISK_INDX_VAL_Y40
                     + NVL (CONDE.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y40, 0)),
                    (  COND.RISK_INDX_VAL_Y41
                     + NVL (CONDE.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y41, 0)),
                    (  COND.RISK_INDX_VAL_Y42
                     + NVL (CONDE.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y42, 0)),
                    (  COND.RISK_INDX_VAL_Y43
                     + NVL (CONDE.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y43, 0)),
                    (  COND.RISK_INDX_VAL_Y44
                     + NVL (CONDE.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y44, 0)),
                    (  COND.RISK_INDX_VAL_Y45
                     + NVL (CONDE.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y45, 0)),
                    (  COND.RISK_INDX_VAL_Y46
                     + NVL (CONDE.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y46, 0)),
                    (  COND.RISK_INDX_VAL_Y47
                     + NVL (CONDE.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y47, 0)),
                    (  COND.RISK_INDX_VAL_Y48
                     + NVL (CONDE.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y48, 0)),
                    (  COND.RISK_INDX_VAL_Y49
                     + NVL (CONDE.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y49, 0)),
                    (  COND.RISK_INDX_VAL_Y50
                     + NVL (CONDE.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y50, 0))
               FROM structools.ST_RISK_COND_REPLACE_NPV15 cond,
                    structools.ST_RISK_CONDEAR_REPLACE_NPV15 conde,
                    structools.ST_RISK_CONDVCA_REPLACE_NPV15 condv,
                    structools.ST_RISK_CONDHCA_REPLACE_NPV15 condh,
                    structools.ST_RISK_CONDGCA_REPLACE_NPV15 condg
              WHERE     cond.PICK_ID = condv.PICK_ID(+)
                    AND cond.PICK_ID = conde.PICK_ID(+)
                    AND cond.PICK_ID = condh.PICK_ID(+)
                    AND cond.PICK_ID = condg.PICK_ID(+);
                    
                    
--                    INSERT INTO structools.ST_RISK_BAY_REPLACE_NPV15
--            (SELECT cond.PICK_ID,
--                    (  COND.RISK_INDX_VAL_Y0 + NVL (CONDV.RISK_INDX_VAL_Y0, 0)),
--(  COND.RISK_INDX_VAL_Y1 + NVL (CONDV.RISK_INDX_VAL_Y1, 0)),
--(  COND.RISK_INDX_VAL_Y2 + NVL (CONDV.RISK_INDX_VAL_Y2, 0)),
--(  COND.RISK_INDX_VAL_Y3 + NVL (CONDV.RISK_INDX_VAL_Y3, 0)),
--(  COND.RISK_INDX_VAL_Y4 + NVL (CONDV.RISK_INDX_VAL_Y4, 0)),
--(  COND.RISK_INDX_VAL_Y5 + NVL (CONDV.RISK_INDX_VAL_Y5, 0)),
--(  COND.RISK_INDX_VAL_Y6 + NVL (CONDV.RISK_INDX_VAL_Y6, 0)),
--(  COND.RISK_INDX_VAL_Y7 + NVL (CONDV.RISK_INDX_VAL_Y7, 0)),
--(  COND.RISK_INDX_VAL_Y8 + NVL (CONDV.RISK_INDX_VAL_Y8, 0)),
--(  COND.RISK_INDX_VAL_Y9 + NVL (CONDV.RISK_INDX_VAL_Y9, 0)),
--(  COND.RISK_INDX_VAL_Y10 + NVL (CONDV.RISK_INDX_VAL_Y10, 0)),
--(  COND.RISK_INDX_VAL_Y11 + NVL (CONDV.RISK_INDX_VAL_Y11, 0)),
--(  COND.RISK_INDX_VAL_Y12 + NVL (CONDV.RISK_INDX_VAL_Y12, 0)),
--(  COND.RISK_INDX_VAL_Y13 + NVL (CONDV.RISK_INDX_VAL_Y13, 0)),
--(  COND.RISK_INDX_VAL_Y14 + NVL (CONDV.RISK_INDX_VAL_Y14, 0)),
--(  COND.RISK_INDX_VAL_Y15 + NVL (CONDV.RISK_INDX_VAL_Y15, 0)),
--(  COND.RISK_INDX_VAL_Y16 + NVL (CONDV.RISK_INDX_VAL_Y16, 0)),
--(  COND.RISK_INDX_VAL_Y17 + NVL (CONDV.RISK_INDX_VAL_Y17, 0)),
--(  COND.RISK_INDX_VAL_Y18 + NVL (CONDV.RISK_INDX_VAL_Y18, 0)),
--(  COND.RISK_INDX_VAL_Y19 + NVL (CONDV.RISK_INDX_VAL_Y19, 0)),
--(  COND.RISK_INDX_VAL_Y20 + NVL (CONDV.RISK_INDX_VAL_Y20, 0)),
--(  COND.RISK_INDX_VAL_Y21 + NVL (CONDV.RISK_INDX_VAL_Y21, 0)),
--(  COND.RISK_INDX_VAL_Y22 + NVL (CONDV.RISK_INDX_VAL_Y22, 0)),
--(  COND.RISK_INDX_VAL_Y23 + NVL (CONDV.RISK_INDX_VAL_Y23, 0)),
--(  COND.RISK_INDX_VAL_Y24 + NVL (CONDV.RISK_INDX_VAL_Y24, 0)),
--(  COND.RISK_INDX_VAL_Y25 + NVL (CONDV.RISK_INDX_VAL_Y25, 0)),
--(  COND.RISK_INDX_VAL_Y26 + NVL (CONDV.RISK_INDX_VAL_Y26, 0)),
--(  COND.RISK_INDX_VAL_Y27 + NVL (CONDV.RISK_INDX_VAL_Y27, 0)),
--(  COND.RISK_INDX_VAL_Y28 + NVL (CONDV.RISK_INDX_VAL_Y28, 0)),
--(  COND.RISK_INDX_VAL_Y29 + NVL (CONDV.RISK_INDX_VAL_Y29, 0)),
--(  COND.RISK_INDX_VAL_Y30 + NVL (CONDV.RISK_INDX_VAL_Y30, 0)),
--(  COND.RISK_INDX_VAL_Y31 + NVL (CONDV.RISK_INDX_VAL_Y31, 0)),
--(  COND.RISK_INDX_VAL_Y32 + NVL (CONDV.RISK_INDX_VAL_Y32, 0)),
--(  COND.RISK_INDX_VAL_Y33 + NVL (CONDV.RISK_INDX_VAL_Y33, 0)),
--(  COND.RISK_INDX_VAL_Y34 + NVL (CONDV.RISK_INDX_VAL_Y34, 0)),
--(  COND.RISK_INDX_VAL_Y35 + NVL (CONDV.RISK_INDX_VAL_Y35, 0)),
--(  COND.RISK_INDX_VAL_Y36 + NVL (CONDV.RISK_INDX_VAL_Y36, 0)),
--(  COND.RISK_INDX_VAL_Y37 + NVL (CONDV.RISK_INDX_VAL_Y37, 0)),
--(  COND.RISK_INDX_VAL_Y38 + NVL (CONDV.RISK_INDX_VAL_Y38, 0)),
--(  COND.RISK_INDX_VAL_Y39 + NVL (CONDV.RISK_INDX_VAL_Y39, 0)),
--(  COND.RISK_INDX_VAL_Y40 + NVL (CONDV.RISK_INDX_VAL_Y40, 0)),
--(  COND.RISK_INDX_VAL_Y41 + NVL (CONDV.RISK_INDX_VAL_Y41, 0)),
--(  COND.RISK_INDX_VAL_Y42 + NVL (CONDV.RISK_INDX_VAL_Y42, 0)),
--(  COND.RISK_INDX_VAL_Y43 + NVL (CONDV.RISK_INDX_VAL_Y43, 0)),
--(  COND.RISK_INDX_VAL_Y44 + NVL (CONDV.RISK_INDX_VAL_Y44, 0)),
--(  COND.RISK_INDX_VAL_Y45 + NVL (CONDV.RISK_INDX_VAL_Y45, 0)),
--(  COND.RISK_INDX_VAL_Y46 + NVL (CONDV.RISK_INDX_VAL_Y46, 0)),
--(  COND.RISK_INDX_VAL_Y47 + NVL (CONDV.RISK_INDX_VAL_Y47, 0)),
--(  COND.RISK_INDX_VAL_Y48 + NVL (CONDV.RISK_INDX_VAL_Y48, 0)),
--(  COND.RISK_INDX_VAL_Y49 + NVL (CONDV.RISK_INDX_VAL_Y49, 0)),
--(  COND.RISK_INDX_VAL_Y50 + NVL (CONDV.RISK_INDX_VAL_Y50, 0))
--               FROM structools.ST_RISK_COND_REPLACE_NPV15 cond,
--                    structools.ST_RISK_CONDVCA_REPLACE_NPV15 condv
--              WHERE     cond.PICK_ID = condv.PICK_ID(+)
--                    );

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - BAY (Combined) REPLACE NPV15');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN NPV TABLE ST_RISK_BAY_REPLACE_NPV15 -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;

      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_BAY_REPLACE');
      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_BAY_REPLACE_NPV15');

      DBMS_OUTPUT.put_line (
            'Risk MODEL - BAY (Combined)_REPAIR Start TIME -'
         || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));

      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_BAY_REPAIR');
      structools.SCHEMA_UTILS.TRUNCATE_TABLE ('ST_RISK_BAY_REPAIR_NPV15');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_BAY_REPAIR');
      structools.SCHEMA_UTILS.DISABLE_TABLE_INDEXES ('ST_RISK_BAY_REPAIR_NPV15');
      
      COMMIT;

      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_BAY_REPAIR
            (SELECT cond.PICK_ID,
                    (  COND.RISK_INDX_VAL_Y0
                     + NVL (CONDE.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y0, 0)),
                    (  COND.RISK_INDX_VAL_Y1
                     + NVL (CONDE.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y1, 0)),
                    (  COND.RISK_INDX_VAL_Y2
                     + NVL (CONDE.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y2, 0)),
                    (  COND.RISK_INDX_VAL_Y3
                     + NVL (CONDE.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y3, 0)),
                    (  COND.RISK_INDX_VAL_Y4
                     + NVL (CONDE.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y4, 0)),
                    (  COND.RISK_INDX_VAL_Y5
                     + NVL (CONDE.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y5, 0)),
                    (  COND.RISK_INDX_VAL_Y6
                     + NVL (CONDE.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y6, 0)),
                    (  COND.RISK_INDX_VAL_Y7
                     + NVL (CONDE.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y7, 0)),
                    (  COND.RISK_INDX_VAL_Y8
                     + NVL (CONDE.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y8, 0)),
                    (  COND.RISK_INDX_VAL_Y9
                     + NVL (CONDE.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y9, 0)),
                    (  COND.RISK_INDX_VAL_Y10
                     + NVL (CONDE.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y10, 0)),
                    (  COND.RISK_INDX_VAL_Y11
                     + NVL (CONDE.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y11, 0)),
                    (  COND.RISK_INDX_VAL_Y12
                     + NVL (CONDE.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y12, 0)),
                    (  COND.RISK_INDX_VAL_Y13
                     + NVL (CONDE.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y13, 0)),
                    (  COND.RISK_INDX_VAL_Y14
                     + NVL (CONDE.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y14, 0)),
                    (  COND.RISK_INDX_VAL_Y15
                     + NVL (CONDE.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y15, 0)),
                    (  COND.RISK_INDX_VAL_Y16
                     + NVL (CONDE.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y16, 0)),
                    (  COND.RISK_INDX_VAL_Y17
                     + NVL (CONDE.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y17, 0)),
                    (  COND.RISK_INDX_VAL_Y18
                     + NVL (CONDE.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y18, 0)),
                    (  COND.RISK_INDX_VAL_Y19
                     + NVL (CONDE.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y19, 0)),
                    (  COND.RISK_INDX_VAL_Y20
                     + NVL (CONDE.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y20, 0)),
                    (  COND.RISK_INDX_VAL_Y21
                     + NVL (CONDE.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y21, 0)),
                    (  COND.RISK_INDX_VAL_Y22
                     + NVL (CONDE.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y22, 0)),
                    (  COND.RISK_INDX_VAL_Y23
                     + NVL (CONDE.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y23, 0)),
                    (  COND.RISK_INDX_VAL_Y24
                     + NVL (CONDE.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y24, 0)),
                    (  COND.RISK_INDX_VAL_Y25
                     + NVL (CONDE.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y25, 0)),
                    (  COND.RISK_INDX_VAL_Y26
                     + NVL (CONDE.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y26, 0)),
                    (  COND.RISK_INDX_VAL_Y27
                     + NVL (CONDE.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y27, 0)),
                    (  COND.RISK_INDX_VAL_Y28
                     + NVL (CONDE.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y28, 0)),
                    (  COND.RISK_INDX_VAL_Y29
                     + NVL (CONDE.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y29, 0)),
                    (  COND.RISK_INDX_VAL_Y30
                     + NVL (CONDE.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y30, 0)),
                    (  COND.RISK_INDX_VAL_Y31
                     + NVL (CONDE.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y31, 0)),
                    (  COND.RISK_INDX_VAL_Y32
                     + NVL (CONDE.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y32, 0)),
                    (  COND.RISK_INDX_VAL_Y33
                     + NVL (CONDE.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y33, 0)),
                    (  COND.RISK_INDX_VAL_Y34
                     + NVL (CONDE.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y34, 0)),
                    (  COND.RISK_INDX_VAL_Y35
                     + NVL (CONDE.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y35, 0)),
                    (  COND.RISK_INDX_VAL_Y36
                     + NVL (CONDE.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y36, 0)),
                    (  COND.RISK_INDX_VAL_Y37
                     + NVL (CONDE.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y37, 0)),
                    (  COND.RISK_INDX_VAL_Y38
                     + NVL (CONDE.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y38, 0)),
                    (  COND.RISK_INDX_VAL_Y39
                     + NVL (CONDE.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y39, 0)),
                    (  COND.RISK_INDX_VAL_Y40
                     + NVL (CONDE.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y40, 0)),
                    (  COND.RISK_INDX_VAL_Y41
                     + NVL (CONDE.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y41, 0)),
                    (  COND.RISK_INDX_VAL_Y42
                     + NVL (CONDE.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y42, 0)),
                    (  COND.RISK_INDX_VAL_Y43
                     + NVL (CONDE.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y43, 0)),
                    (  COND.RISK_INDX_VAL_Y44
                     + NVL (CONDE.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y44, 0)),
                    (  COND.RISK_INDX_VAL_Y45
                     + NVL (CONDE.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y45, 0)),
                    (  COND.RISK_INDX_VAL_Y46
                     + NVL (CONDE.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y46, 0)),
                    (  COND.RISK_INDX_VAL_Y47
                     + NVL (CONDE.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y47, 0)),
                    (  COND.RISK_INDX_VAL_Y48
                     + NVL (CONDE.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y48, 0)),
                    (  COND.RISK_INDX_VAL_Y49
                     + NVL (CONDE.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y49, 0)),
                    (  COND.RISK_INDX_VAL_Y50
                     + NVL (CONDE.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y50, 0))
               FROM structools.ST_RISK_COND_REPAIR cond,
                    structools.ST_RISK_CONDEAR_NOACTION conde,
                    structools.ST_RISK_CONDVCA_NOACTION condv,
                    structools.ST_RISK_CONDHCA_NOACTION condh,
                    structools.ST_RISK_CONDGCA_NOACTION condg
              WHERE     cond.PICK_ID = condv.PICK_ID(+)
                    AND cond.PICK_ID = conde.PICK_ID(+)
                    AND cond.PICK_ID = condh.PICK_ID(+)
                    AND cond.PICK_ID = condg.PICK_ID(+));
                    
                    
--                    INSERT INTO structools.ST_RISK_BAY_REPAIR
--            (SELECT cond.PICK_ID,
--                    (  COND.RISK_INDX_VAL_Y0 + NVL (CONDV.RISK_INDX_VAL_Y0, 0)),
--(  COND.RISK_INDX_VAL_Y1 + NVL (CONDV.RISK_INDX_VAL_Y1, 0)),
--(  COND.RISK_INDX_VAL_Y2 + NVL (CONDV.RISK_INDX_VAL_Y2, 0)),
--(  COND.RISK_INDX_VAL_Y3 + NVL (CONDV.RISK_INDX_VAL_Y3, 0)),
--(  COND.RISK_INDX_VAL_Y4 + NVL (CONDV.RISK_INDX_VAL_Y4, 0)),
--(  COND.RISK_INDX_VAL_Y5 + NVL (CONDV.RISK_INDX_VAL_Y5, 0)),
--(  COND.RISK_INDX_VAL_Y6 + NVL (CONDV.RISK_INDX_VAL_Y6, 0)),
--(  COND.RISK_INDX_VAL_Y7 + NVL (CONDV.RISK_INDX_VAL_Y7, 0)),
--(  COND.RISK_INDX_VAL_Y8 + NVL (CONDV.RISK_INDX_VAL_Y8, 0)),
--(  COND.RISK_INDX_VAL_Y9 + NVL (CONDV.RISK_INDX_VAL_Y9, 0)),
--(  COND.RISK_INDX_VAL_Y10 + NVL (CONDV.RISK_INDX_VAL_Y10, 0)),
--(  COND.RISK_INDX_VAL_Y11 + NVL (CONDV.RISK_INDX_VAL_Y11, 0)),
--(  COND.RISK_INDX_VAL_Y12 + NVL (CONDV.RISK_INDX_VAL_Y12, 0)),
--(  COND.RISK_INDX_VAL_Y13 + NVL (CONDV.RISK_INDX_VAL_Y13, 0)),
--(  COND.RISK_INDX_VAL_Y14 + NVL (CONDV.RISK_INDX_VAL_Y14, 0)),
--(  COND.RISK_INDX_VAL_Y15 + NVL (CONDV.RISK_INDX_VAL_Y15, 0)),
--(  COND.RISK_INDX_VAL_Y16 + NVL (CONDV.RISK_INDX_VAL_Y16, 0)),
--(  COND.RISK_INDX_VAL_Y17 + NVL (CONDV.RISK_INDX_VAL_Y17, 0)),
--(  COND.RISK_INDX_VAL_Y18 + NVL (CONDV.RISK_INDX_VAL_Y18, 0)),
--(  COND.RISK_INDX_VAL_Y19 + NVL (CONDV.RISK_INDX_VAL_Y19, 0)),
--(  COND.RISK_INDX_VAL_Y20 + NVL (CONDV.RISK_INDX_VAL_Y20, 0)),
--(  COND.RISK_INDX_VAL_Y21 + NVL (CONDV.RISK_INDX_VAL_Y21, 0)),
--(  COND.RISK_INDX_VAL_Y22 + NVL (CONDV.RISK_INDX_VAL_Y22, 0)),
--(  COND.RISK_INDX_VAL_Y23 + NVL (CONDV.RISK_INDX_VAL_Y23, 0)),
--(  COND.RISK_INDX_VAL_Y24 + NVL (CONDV.RISK_INDX_VAL_Y24, 0)),
--(  COND.RISK_INDX_VAL_Y25 + NVL (CONDV.RISK_INDX_VAL_Y25, 0)),
--(  COND.RISK_INDX_VAL_Y26 + NVL (CONDV.RISK_INDX_VAL_Y26, 0)),
--(  COND.RISK_INDX_VAL_Y27 + NVL (CONDV.RISK_INDX_VAL_Y27, 0)),
--(  COND.RISK_INDX_VAL_Y28 + NVL (CONDV.RISK_INDX_VAL_Y28, 0)),
--(  COND.RISK_INDX_VAL_Y29 + NVL (CONDV.RISK_INDX_VAL_Y29, 0)),
--(  COND.RISK_INDX_VAL_Y30 + NVL (CONDV.RISK_INDX_VAL_Y30, 0)),
--(  COND.RISK_INDX_VAL_Y31 + NVL (CONDV.RISK_INDX_VAL_Y31, 0)),
--(  COND.RISK_INDX_VAL_Y32 + NVL (CONDV.RISK_INDX_VAL_Y32, 0)),
--(  COND.RISK_INDX_VAL_Y33 + NVL (CONDV.RISK_INDX_VAL_Y33, 0)),
--(  COND.RISK_INDX_VAL_Y34 + NVL (CONDV.RISK_INDX_VAL_Y34, 0)),
--(  COND.RISK_INDX_VAL_Y35 + NVL (CONDV.RISK_INDX_VAL_Y35, 0)),
--(  COND.RISK_INDX_VAL_Y36 + NVL (CONDV.RISK_INDX_VAL_Y36, 0)),
--(  COND.RISK_INDX_VAL_Y37 + NVL (CONDV.RISK_INDX_VAL_Y37, 0)),
--(  COND.RISK_INDX_VAL_Y38 + NVL (CONDV.RISK_INDX_VAL_Y38, 0)),
--(  COND.RISK_INDX_VAL_Y39 + NVL (CONDV.RISK_INDX_VAL_Y39, 0)),
--(  COND.RISK_INDX_VAL_Y40 + NVL (CONDV.RISK_INDX_VAL_Y40, 0)),
--(  COND.RISK_INDX_VAL_Y41 + NVL (CONDV.RISK_INDX_VAL_Y41, 0)),
--(  COND.RISK_INDX_VAL_Y42 + NVL (CONDV.RISK_INDX_VAL_Y42, 0)),
--(  COND.RISK_INDX_VAL_Y43 + NVL (CONDV.RISK_INDX_VAL_Y43, 0)),
--(  COND.RISK_INDX_VAL_Y44 + NVL (CONDV.RISK_INDX_VAL_Y44, 0)),
--(  COND.RISK_INDX_VAL_Y45 + NVL (CONDV.RISK_INDX_VAL_Y45, 0)),
--(  COND.RISK_INDX_VAL_Y46 + NVL (CONDV.RISK_INDX_VAL_Y46, 0)),
--(  COND.RISK_INDX_VAL_Y47 + NVL (CONDV.RISK_INDX_VAL_Y47, 0)),
--(  COND.RISK_INDX_VAL_Y48 + NVL (CONDV.RISK_INDX_VAL_Y48, 0)),
--(  COND.RISK_INDX_VAL_Y49 + NVL (CONDV.RISK_INDX_VAL_Y49, 0)),
--(  COND.RISK_INDX_VAL_Y50 + NVL (CONDV.RISK_INDX_VAL_Y50, 0))
--               FROM structools.ST_RISK_COND_REPAIR cond,
--                    structools.ST_RISK_CONDVCA_NOACTION condv
--              WHERE     cond.PICK_ID = condv.PICK_ID(+)
--                    );

         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - BAY (Combined) REPAIR');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING risk records IN ST_RISK_BAY_REPAIR -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;


      BEGIN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_BAY_REPAIR_NPV15
            (SELECT cond.PICK_ID,
                    (  COND.RISK_INDX_VAL_Y0
                     + NVL (CONDE.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y0, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y0, 0)),
                    (  COND.RISK_INDX_VAL_Y1
                     + NVL (CONDE.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y1, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y1, 0)),
                    (  COND.RISK_INDX_VAL_Y2
                     + NVL (CONDE.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y2, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y2, 0)),
                    (  COND.RISK_INDX_VAL_Y3
                     + NVL (CONDE.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y3, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y3, 0)),
                    (  COND.RISK_INDX_VAL_Y4
                     + NVL (CONDE.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y4, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y4, 0)),
                    (  COND.RISK_INDX_VAL_Y5
                     + NVL (CONDE.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y5, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y5, 0)),
                    (  COND.RISK_INDX_VAL_Y6
                     + NVL (CONDE.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y6, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y6, 0)),
                    (  COND.RISK_INDX_VAL_Y7
                     + NVL (CONDE.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y7, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y7, 0)),
                    (  COND.RISK_INDX_VAL_Y8
                     + NVL (CONDE.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y8, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y8, 0)),
                    (  COND.RISK_INDX_VAL_Y9
                     + NVL (CONDE.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y9, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y9, 0)),
                    (  COND.RISK_INDX_VAL_Y10
                     + NVL (CONDE.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y10, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y10, 0)),
                    (  COND.RISK_INDX_VAL_Y11
                     + NVL (CONDE.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y11, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y11, 0)),
                    (  COND.RISK_INDX_VAL_Y12
                     + NVL (CONDE.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y12, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y12, 0)),
                    (  COND.RISK_INDX_VAL_Y13
                     + NVL (CONDE.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y13, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y13, 0)),
                    (  COND.RISK_INDX_VAL_Y14
                     + NVL (CONDE.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y14, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y14, 0)),
                    (  COND.RISK_INDX_VAL_Y15
                     + NVL (CONDE.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y15, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y15, 0)),
                    (  COND.RISK_INDX_VAL_Y16
                     + NVL (CONDE.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y16, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y16, 0)),
                    (  COND.RISK_INDX_VAL_Y17
                     + NVL (CONDE.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y17, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y17, 0)),
                    (  COND.RISK_INDX_VAL_Y18
                     + NVL (CONDE.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y18, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y18, 0)),
                    (  COND.RISK_INDX_VAL_Y19
                     + NVL (CONDE.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y19, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y19, 0)),
                    (  COND.RISK_INDX_VAL_Y20
                     + NVL (CONDE.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y20, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y20, 0)),
                    (  COND.RISK_INDX_VAL_Y21
                     + NVL (CONDE.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y21, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y21, 0)),
                    (  COND.RISK_INDX_VAL_Y22
                     + NVL (CONDE.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y22, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y22, 0)),
                    (  COND.RISK_INDX_VAL_Y23
                     + NVL (CONDE.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y23, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y23, 0)),
                    (  COND.RISK_INDX_VAL_Y24
                     + NVL (CONDE.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y24, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y24, 0)),
                    (  COND.RISK_INDX_VAL_Y25
                     + NVL (CONDE.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y25, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y25, 0)),
                    (  COND.RISK_INDX_VAL_Y26
                     + NVL (CONDE.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y26, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y26, 0)),
                    (  COND.RISK_INDX_VAL_Y27
                     + NVL (CONDE.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y27, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y27, 0)),
                    (  COND.RISK_INDX_VAL_Y28
                     + NVL (CONDE.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y28, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y28, 0)),
                    (  COND.RISK_INDX_VAL_Y29
                     + NVL (CONDE.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y29, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y29, 0)),
                    (  COND.RISK_INDX_VAL_Y30
                     + NVL (CONDE.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y30, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y30, 0)),
                    (  COND.RISK_INDX_VAL_Y31
                     + NVL (CONDE.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y31, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y31, 0)),
                    (  COND.RISK_INDX_VAL_Y32
                     + NVL (CONDE.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y32, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y32, 0)),
                    (  COND.RISK_INDX_VAL_Y33
                     + NVL (CONDE.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y33, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y33, 0)),
                    (  COND.RISK_INDX_VAL_Y34
                     + NVL (CONDE.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y34, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y34, 0)),
                    (  COND.RISK_INDX_VAL_Y35
                     + NVL (CONDE.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y35, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y35, 0)),
                    (  COND.RISK_INDX_VAL_Y36
                     + NVL (CONDE.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y36, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y36, 0)),
                    (  COND.RISK_INDX_VAL_Y37
                     + NVL (CONDE.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y37, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y37, 0)),
                    (  COND.RISK_INDX_VAL_Y38
                     + NVL (CONDE.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y38, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y38, 0)),
                    (  COND.RISK_INDX_VAL_Y39
                     + NVL (CONDE.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y39, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y39, 0)),
                    (  COND.RISK_INDX_VAL_Y40
                     + NVL (CONDE.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y40, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y40, 0)),
                    (  COND.RISK_INDX_VAL_Y41
                     + NVL (CONDE.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y41, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y41, 0)),
                    (  COND.RISK_INDX_VAL_Y42
                     + NVL (CONDE.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y42, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y42, 0)),
                    (  COND.RISK_INDX_VAL_Y43
                     + NVL (CONDE.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y43, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y43, 0)),
                    (  COND.RISK_INDX_VAL_Y44
                     + NVL (CONDE.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y44, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y44, 0)),
                    (  COND.RISK_INDX_VAL_Y45
                     + NVL (CONDE.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y45, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y45, 0)),
                    (  COND.RISK_INDX_VAL_Y46
                     + NVL (CONDE.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y46, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y46, 0)),
                    (  COND.RISK_INDX_VAL_Y47
                     + NVL (CONDE.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y47, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y47, 0)),
                    (  COND.RISK_INDX_VAL_Y48
                     + NVL (CONDE.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y48, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y48, 0)),
                    (  COND.RISK_INDX_VAL_Y49
                     + NVL (CONDE.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y49, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y49, 0)),
                    (  COND.RISK_INDX_VAL_Y50
                     + NVL (CONDE.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDV.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDH.RISK_INDX_VAL_Y50, 0)
                     + NVL (CONDG.RISK_INDX_VAL_Y50, 0))
               FROM structools.ST_RISK_COND_REPAIR_NPV15 cond,
                    structools.ST_RISK_CONDEAR_NOACTION_NPV15 conde,
                    structools.ST_RISK_CONDVCA_NOACTION_NPV15 condv,
                    structools.ST_RISK_CONDHCA_NOACTION_NPV15 condh,
                    structools.ST_RISK_CONDGCA_NOACTION_NPV15 condg
              WHERE     cond.PICK_ID = condv.PICK_ID(+)
                    AND cond.PICK_ID = conde.PICK_ID(+)
                    AND cond.PICK_ID = condh.PICK_ID(+)
                    AND cond.PICK_ID = condg.PICK_ID(+));
                    
--                     INSERT INTO structools.ST_RISK_BAY_REPAIR_NPV15
--            (SELECT cond.PICK_ID,
--                      (  COND.RISK_INDX_VAL_Y0 + NVL (CONDV.RISK_INDX_VAL_Y0, 0)),
--(  COND.RISK_INDX_VAL_Y1 + NVL (CONDV.RISK_INDX_VAL_Y1, 0)),
--(  COND.RISK_INDX_VAL_Y2 + NVL (CONDV.RISK_INDX_VAL_Y2, 0)),
--(  COND.RISK_INDX_VAL_Y3 + NVL (CONDV.RISK_INDX_VAL_Y3, 0)),
--(  COND.RISK_INDX_VAL_Y4 + NVL (CONDV.RISK_INDX_VAL_Y4, 0)),
--(  COND.RISK_INDX_VAL_Y5 + NVL (CONDV.RISK_INDX_VAL_Y5, 0)),
--(  COND.RISK_INDX_VAL_Y6 + NVL (CONDV.RISK_INDX_VAL_Y6, 0)),
--(  COND.RISK_INDX_VAL_Y7 + NVL (CONDV.RISK_INDX_VAL_Y7, 0)),
--(  COND.RISK_INDX_VAL_Y8 + NVL (CONDV.RISK_INDX_VAL_Y8, 0)),
--(  COND.RISK_INDX_VAL_Y9 + NVL (CONDV.RISK_INDX_VAL_Y9, 0)),
--(  COND.RISK_INDX_VAL_Y10 + NVL (CONDV.RISK_INDX_VAL_Y10, 0)),
--(  COND.RISK_INDX_VAL_Y11 + NVL (CONDV.RISK_INDX_VAL_Y11, 0)),
--(  COND.RISK_INDX_VAL_Y12 + NVL (CONDV.RISK_INDX_VAL_Y12, 0)),
--(  COND.RISK_INDX_VAL_Y13 + NVL (CONDV.RISK_INDX_VAL_Y13, 0)),
--(  COND.RISK_INDX_VAL_Y14 + NVL (CONDV.RISK_INDX_VAL_Y14, 0)),
--(  COND.RISK_INDX_VAL_Y15 + NVL (CONDV.RISK_INDX_VAL_Y15, 0)),
--(  COND.RISK_INDX_VAL_Y16 + NVL (CONDV.RISK_INDX_VAL_Y16, 0)),
--(  COND.RISK_INDX_VAL_Y17 + NVL (CONDV.RISK_INDX_VAL_Y17, 0)),
--(  COND.RISK_INDX_VAL_Y18 + NVL (CONDV.RISK_INDX_VAL_Y18, 0)),
--(  COND.RISK_INDX_VAL_Y19 + NVL (CONDV.RISK_INDX_VAL_Y19, 0)),
--(  COND.RISK_INDX_VAL_Y20 + NVL (CONDV.RISK_INDX_VAL_Y20, 0)),
--(  COND.RISK_INDX_VAL_Y21 + NVL (CONDV.RISK_INDX_VAL_Y21, 0)),
--(  COND.RISK_INDX_VAL_Y22 + NVL (CONDV.RISK_INDX_VAL_Y22, 0)),
--(  COND.RISK_INDX_VAL_Y23 + NVL (CONDV.RISK_INDX_VAL_Y23, 0)),
--(  COND.RISK_INDX_VAL_Y24 + NVL (CONDV.RISK_INDX_VAL_Y24, 0)),
--(  COND.RISK_INDX_VAL_Y25 + NVL (CONDV.RISK_INDX_VAL_Y25, 0)),
--(  COND.RISK_INDX_VAL_Y26 + NVL (CONDV.RISK_INDX_VAL_Y26, 0)),
--(  COND.RISK_INDX_VAL_Y27 + NVL (CONDV.RISK_INDX_VAL_Y27, 0)),
--(  COND.RISK_INDX_VAL_Y28 + NVL (CONDV.RISK_INDX_VAL_Y28, 0)),
--(  COND.RISK_INDX_VAL_Y29 + NVL (CONDV.RISK_INDX_VAL_Y29, 0)),
--(  COND.RISK_INDX_VAL_Y30 + NVL (CONDV.RISK_INDX_VAL_Y30, 0)),
--(  COND.RISK_INDX_VAL_Y31 + NVL (CONDV.RISK_INDX_VAL_Y31, 0)),
--(  COND.RISK_INDX_VAL_Y32 + NVL (CONDV.RISK_INDX_VAL_Y32, 0)),
--(  COND.RISK_INDX_VAL_Y33 + NVL (CONDV.RISK_INDX_VAL_Y33, 0)),
--(  COND.RISK_INDX_VAL_Y34 + NVL (CONDV.RISK_INDX_VAL_Y34, 0)),
--(  COND.RISK_INDX_VAL_Y35 + NVL (CONDV.RISK_INDX_VAL_Y35, 0)),
--(  COND.RISK_INDX_VAL_Y36 + NVL (CONDV.RISK_INDX_VAL_Y36, 0)),
--(  COND.RISK_INDX_VAL_Y37 + NVL (CONDV.RISK_INDX_VAL_Y37, 0)),
--(  COND.RISK_INDX_VAL_Y38 + NVL (CONDV.RISK_INDX_VAL_Y38, 0)),
--(  COND.RISK_INDX_VAL_Y39 + NVL (CONDV.RISK_INDX_VAL_Y39, 0)),
--(  COND.RISK_INDX_VAL_Y40 + NVL (CONDV.RISK_INDX_VAL_Y40, 0)),
--(  COND.RISK_INDX_VAL_Y41 + NVL (CONDV.RISK_INDX_VAL_Y41, 0)),
--(  COND.RISK_INDX_VAL_Y42 + NVL (CONDV.RISK_INDX_VAL_Y42, 0)),
--(  COND.RISK_INDX_VAL_Y43 + NVL (CONDV.RISK_INDX_VAL_Y43, 0)),
--(  COND.RISK_INDX_VAL_Y44 + NVL (CONDV.RISK_INDX_VAL_Y44, 0)),
--(  COND.RISK_INDX_VAL_Y45 + NVL (CONDV.RISK_INDX_VAL_Y45, 0)),
--(  COND.RISK_INDX_VAL_Y46 + NVL (CONDV.RISK_INDX_VAL_Y46, 0)),
--(  COND.RISK_INDX_VAL_Y47 + NVL (CONDV.RISK_INDX_VAL_Y47, 0)),
--(  COND.RISK_INDX_VAL_Y48 + NVL (CONDV.RISK_INDX_VAL_Y48, 0)),
--(  COND.RISK_INDX_VAL_Y49 + NVL (CONDV.RISK_INDX_VAL_Y49, 0)),
--(  COND.RISK_INDX_VAL_Y50 + NVL (CONDV.RISK_INDX_VAL_Y50, 0))
--               FROM structools.ST_RISK_COND_REPAIR_NPV15 cond,
--                    structools.ST_RISK_CONDVCA_NOACTION_NPV15 condv
--              WHERE     cond.PICK_ID = condv.PICK_ID(+));


         v_record_cnt := SQL%ROWCOUNT;
         COMMIT;

         DBMS_OUTPUT.PUT_LINE (
            'Successfully completed load FOR - BAY (Combined) REPAIR NPV15');
         DBMS_OUTPUT.PUT_LINE ('Total records inserted - ' || V_RECORD_CNT);
         DBMS_OUTPUT.PUT_LINE (
            'END TIME - ' || TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss'));
         DBMS_OUTPUT.PUT_LINE (
            '***************************************************************************************************');
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'ERROR WHILE INSERTING records IN NPV TABLE ST_RISK_BAY_REPAIR_NPV15 -'
               || SUBSTR (SQLERRM, 1, 1000));
            RAISE;
      END;
      
      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_BAY_REPAIR');
      structools.SCHEMA_UTILS.ENABLE_TABLE_INDEXES ('ST_RISK_BAY_REPAIR_NPV15');
      
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END PR_LOAD_RISK_BAY;


   PROCEDURE PR_INSERT_RISK_NPV (
      p_risk_npv_rt   IN structools.ST_RISK_POLE_NOACTION_NPV15%ROWTYPE,
      p_risk_model    IN VARCHAR2,
      p_action        IN VARCHAR2)
   AS
   BEGIN
      IF p_risk_model = 'PWOD' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_POLE_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'PWOD' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_POLE_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'PWOD' AND p_action = 'MAINTAIN'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_POLE_REINF_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'XARMLV' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMLV_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'XARMLV' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMLV_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'XARMHV' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMHV_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'XARMHV' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMHV_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'STAY' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_STAY_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'STAY' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_STAY_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'INSUL' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_INSUL_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'INSUL' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_INSUL_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'NWPOLE' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_NWPOLE_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'NWPOLE' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_NWPOLE_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_COND_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_COND_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'MAINTAIN'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_COND_REPAIR_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'CONDGC' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDGCA_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'CONDGC' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDGCA_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'CONDCA' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDHCA_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'CONDCA' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDHCA_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'CONDVCA' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDVCA_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'CONDVCA' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDVCA_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'CONDEARTH' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDEAR_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'CONDEARTH' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDEAR_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'DOF' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_DOF_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'DOF' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_DOF_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'DSTRPM' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_DSTR_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'DSTRPM' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_DSTR_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'PTSD' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_PTSD_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'PTSD' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_PTSD_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'RCLBS' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_RECL_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'RCLBS' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_RECL_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'VREG' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_RGTR_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'VREG' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_RGTR_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      ELSIF p_risk_model = 'SECT' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_SECT_NOACTION_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50,
                      P_RISK_NPV_RT.FIRE_FRAC,
                      P_RISK_NPV_RT.ESHOCK_FRAC,
                      P_RISK_NPV_RT.WFORCE_FRAC,
                      P_RISK_NPV_RT.RELIABILITY_FRAC,
                      P_RISK_NPV_RT.PHYSIMP_FRAC,
                      P_RISK_NPV_RT.ENV_FRAC,
                      P_RISK_NPV_RT.FIN_FRAC);
      ELSIF p_risk_model = 'SECT' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_SECT_REPLACE_NPV15
              VALUES (P_RISK_NPV_RT.PICK_ID,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y0,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y1,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y2,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y3,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y4,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y5,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y6,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y7,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y8,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y9,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y10,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y11,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y12,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y13,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y14,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y15,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y16,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y17,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y18,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y19,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y20,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y21,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y22,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y23,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y24,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y25,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y26,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y27,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y28,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y29,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y30,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y31,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y32,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y33,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y34,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y35,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y36,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y37,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y38,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y39,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y40,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y41,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y42,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y43,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y44,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y45,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y46,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y47,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y48,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y49,
                      P_RISK_NPV_RT.RISK_INDX_VAL_Y50);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (
               'ERROR WHILE loading NPV TABLE FOR pick_id -'
            || P_RISK_NPV_RT.PICK_ID
            || ' - '
            || SUBSTR (SQLERRM, 1, 1000));
         RAISE;
   END PR_INSERT_RISK_NPV;

   PROCEDURE PR_INSERT_RISK_FAIL (
      p_risk_fail_rt   IN structools.ST_RISK_POLE_NOACTION_FAIL%ROWTYPE,
      p_risk_model     IN VARCHAR2,
      p_action         IN VARCHAR2)
   AS
   BEGIN
      --.put_line('testing');

      IF p_risk_model = 'PWOD' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_POLE_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50,
                      P_RISK_FAIL_RT.FIRE_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_INCD,
                      P_RISK_FAIL_RT.LRGE_RLBLTY_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_FATALITY_INCD,
                      P_RISK_FAIL_RT.FIRE_FATALITY_INCD);
      ELSIF p_risk_model = 'PWOD' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_POLE_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'PWOD' AND p_action = 'MAINTAIN'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_POLE_REINF_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'XARMLV' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMLV_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50,
                      P_RISK_FAIL_RT.FIRE_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_INCD,
                      P_RISK_FAIL_RT.LRGE_RLBLTY_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_FATALITY_INCD,
                      P_RISK_FAIL_RT.FIRE_FATALITY_INCD);
      ELSIF p_risk_model = 'XARMLV' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMLV_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'XARMHV' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMHV_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50,
                      P_RISK_FAIL_RT.FIRE_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_INCD,
                      P_RISK_FAIL_RT.LRGE_RLBLTY_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_FATALITY_INCD,
                      P_RISK_FAIL_RT.FIRE_FATALITY_INCD);
      ELSIF p_risk_model = 'XARMHV' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMHV_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'STAY' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_STAY_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50,
                      P_RISK_FAIL_RT.FIRE_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_INCD,
                      P_RISK_FAIL_RT.LRGE_RLBLTY_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_FATALITY_INCD,
                      P_RISK_FAIL_RT.FIRE_FATALITY_INCD);
      ELSIF p_risk_model = 'STAY' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_STAY_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'INSUL' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_INSUL_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50,
                      P_RISK_FAIL_RT.FIRE_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_INCD,
                      P_RISK_FAIL_RT.LRGE_RLBLTY_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_FATALITY_INCD,
                      P_RISK_FAIL_RT.FIRE_FATALITY_INCD);
      ELSIF p_risk_model = 'INSUL' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_INSUL_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'NWPOLE' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_NWPOLE_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50,
                      P_RISK_FAIL_RT.FIRE_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_INCD,
                      P_RISK_FAIL_RT.LRGE_RLBLTY_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_FATALITY_INCD,
                      P_RISK_FAIL_RT.FIRE_FATALITY_INCD);
      ELSIF p_risk_model = 'NWPOLE' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_NWPOLE_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_COND_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_COND_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'MAINTAIN'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_COND_REPAIR_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'CONDGC' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDGCA_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'CONDGC' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDGCA_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'CONDCA' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDHCA_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'CONDCA' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDHCA_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'CONDVCA' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDVCA_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'CONDVCA' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDVCA_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'CONDEARTH' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDEAR_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'CONDEARTH' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDEAR_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'DOF' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_DOF_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'DOF' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_DOF_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'DSTRPM' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_DSTR_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50,
                      P_RISK_FAIL_RT.FIRE_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_INCD,
                      P_RISK_FAIL_RT.LRGE_RLBLTY_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_FATALITY_INCD,
                      P_RISK_FAIL_RT.FIRE_FATALITY_INCD);
      ELSIF p_risk_model = 'DSTRPM' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_DSTR_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'PTSD' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_PTSD_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50,
                      P_RISK_FAIL_RT.FIRE_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_INCD,
                      P_RISK_FAIL_RT.LRGE_RLBLTY_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_FATALITY_INCD,
                      P_RISK_FAIL_RT.FIRE_FATALITY_INCD);
      ELSIF p_risk_model = 'PTSD' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_PTSD_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'RCLBS' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_RECL_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'RCLBS' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_RECL_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'VREG' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_RGTR_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'VREG' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_RGTR_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSIF p_risk_model = 'SECT' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_SECT_NOACTION_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50,
                      P_RISK_FAIL_RT.FIRE_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_INCD,
                      P_RISK_FAIL_RT.LRGE_RLBLTY_INCD,
                      P_RISK_FAIL_RT.ELEC_SHOCK_FATALITY_INCD,
                      P_RISK_FAIL_RT.FIRE_FATALITY_INCD);
      ELSIF p_risk_model = 'SECT' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_SECT_REPLACE_FAIL
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50);
      ELSE
         DBMS_OUTPUT.put_line ('NO matrch FOUND');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (
               'ERROR WHILE loading risk fail TABLE FOR pick_id '
            || P_RISK_FAIL_RT.PICK_ID
            || '-'
            || SUBSTR (SQLERRM, 1, 1000));
         RAISE;
   END PR_INSERT_RISK_FAIL;

   PROCEDURE PR_INSERT_RISK (
      p_risk_fail_rt   IN structools.ST_RISK_POLE_NOACTION_FAIL%ROWTYPE,
      p_risk_model     IN VARCHAR2,
      p_action         IN VARCHAR2,
      p_cnsqnc_amt     IN NUMBER)
   AS
   BEGIN
      IF p_risk_model = 'PWOD' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_POLE_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'PWOD' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_POLE_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'PWOD' AND p_action = 'MAINTAIN'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_POLE_REINF
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'XARMLV' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMLV_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'XARMLV' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMLV_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'XARMHV' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMHV_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'XARMHV' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_XARMHV_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'STAY' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_STAY_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'STAY' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_STAY_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'INSUL' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_INSUL_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'INSUL' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_INSUL_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'NWPOLE' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_NWPOLE_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'NWPOLE' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_NWPOLE_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_COND_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_COND_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'CONDFAIL' AND p_action = 'MAINTAIN'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_COND_REPAIR
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'CONDGC' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDGCA_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'CONDGC' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDGCA_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'CONDCA' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDHCA_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'CONDCA' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDHCA_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'CONDVCA' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDVCA_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'CONDVCA' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDVCA_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'CONDEARTH' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDEAR_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'CONDEARTH' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_CONDEAR_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'DOF' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_DOF_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'DOF' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_DOF_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'DSTRPM' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_DSTR_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'DSTRPM' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_DSTR_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'PTSD' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_PTSD_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'PTSD' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_PTSD_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'RCLBS' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_RECL_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'RCLBS' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_RECL_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'VREG' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_RGTR_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'VREG' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_RGTR_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'SECT' AND p_action = 'DONOTHING'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_SECT_NOACTION
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      ELSIF p_risk_model = 'SECT' AND p_action = 'REPLACE'
      THEN
         INSERT /*+ APPEND */ INTO structools.ST_RISK_SECT_REPLACE
              VALUES (P_RISK_FAIL_RT.PICK_ID,
                      P_RISK_FAIL_RT.FAIL_PROB_Y0 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y1 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y2 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y3 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y4 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y5 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y6 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y7 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y8 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y9 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y10 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y11 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y12 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y13 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y14 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y15 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y16 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y17 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y18 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y19 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y20 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y21 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y22 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y23 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y24 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y25 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y26 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y27 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y28 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y29 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y30 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y31 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y32 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y33 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y34 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y35 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y36 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y37 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y38 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y39 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y40 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y41 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y42 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y43 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y44 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y45 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y46 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y47 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y48 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y49 * P_CNSQNC_AMT,
                      P_RISK_FAIL_RT.FAIL_PROB_Y50 * P_CNSQNC_AMT);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line (
               'ERROR WHILE loading risk TABLE FOR pick_id '
            || P_RISK_FAIL_RT.PICK_ID
            || ' - '
            || SUBSTR (SQLERRM, 1, 1000));
         RAISE;
   END PR_INSERT_RISK;

   FUNCTION fn_get_npv15_val (
      p_yr_no              IN NUMBER,
      p_risk_fail_rt       IN structools.ST_RISK_POLE_NOACTION_FAIL%ROWTYPE,
      p_forecast_const_a   IN FLOAT,
      p_forecast_const_b   IN FLOAT)
      RETURN FLOAT
   IS
      v_npv_val_y     FLOAT;
      V_PDF_15Y_1     FLOAT;
      V_PDF_15Y_2     FLOAT;
      V_PDF_15Y_3     FLOAT;
      V_PDF_15Y_4     FLOAT;
      V_PDF_15Y_5     FLOAT;
      V_PDF_15Y_6     FLOAT;
      V_PDF_15Y_7     FLOAT;
      V_PDF_15Y_8     FLOAT;
      V_PDF_15Y_9     FLOAT;
      V_PDF_15Y_10    FLOAT;
      V_PDF_15Y_11    FLOAT;
      V_PDF_15Y_12    FLOAT;
      V_PDF_15Y_13    FLOAT;
      V_PDF_15Y_14    FLOAT;
      V_PDF_15Y_15    FLOAT;
      V_CONSTANT      FLOAT := 1.0433;

      TYPE DATA_15Y IS VARRAY (15) OF FLOAT;

      v_pfail_15y     DATA_15Y;
      v_present_val   DATA_15Y;
   BEGIN
      IF p_yr_no = 0
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y0,
                      p_risk_fail_rt.FAIL_PROB_Y1,
                      p_risk_fail_rt.FAIL_PROB_Y2,
                      p_risk_fail_rt.FAIL_PROB_Y3,
                      p_risk_fail_rt.FAIL_PROB_Y4,
                      p_risk_fail_rt.FAIL_PROB_Y5,
                      p_risk_fail_rt.FAIL_PROB_Y6,
                      p_risk_fail_rt.FAIL_PROB_Y7,
                      p_risk_fail_rt.FAIL_PROB_Y8,
                      p_risk_fail_rt.FAIL_PROB_Y9,
                      p_risk_fail_rt.FAIL_PROB_Y10,
                      p_risk_fail_rt.FAIL_PROB_Y11,
                      p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14);
      ELSIF p_yr_no = 1
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y1,
                      p_risk_fail_rt.FAIL_PROB_Y2,
                      p_risk_fail_rt.FAIL_PROB_Y3,
                      p_risk_fail_rt.FAIL_PROB_Y4,
                      p_risk_fail_rt.FAIL_PROB_Y5,
                      p_risk_fail_rt.FAIL_PROB_Y6,
                      p_risk_fail_rt.FAIL_PROB_Y7,
                      p_risk_fail_rt.FAIL_PROB_Y8,
                      p_risk_fail_rt.FAIL_PROB_Y9,
                      p_risk_fail_rt.FAIL_PROB_Y10,
                      p_risk_fail_rt.FAIL_PROB_Y11,
                      p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15);
      ELSIF p_yr_no = 2
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y2,
                      p_risk_fail_rt.FAIL_PROB_Y3,
                      p_risk_fail_rt.FAIL_PROB_Y4,
                      p_risk_fail_rt.FAIL_PROB_Y5,
                      p_risk_fail_rt.FAIL_PROB_Y6,
                      p_risk_fail_rt.FAIL_PROB_Y7,
                      p_risk_fail_rt.FAIL_PROB_Y8,
                      p_risk_fail_rt.FAIL_PROB_Y9,
                      p_risk_fail_rt.FAIL_PROB_Y10,
                      p_risk_fail_rt.FAIL_PROB_Y11,
                      p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16);
      ELSIF p_yr_no = 3
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y3,
                      p_risk_fail_rt.FAIL_PROB_Y4,
                      p_risk_fail_rt.FAIL_PROB_Y5,
                      p_risk_fail_rt.FAIL_PROB_Y6,
                      p_risk_fail_rt.FAIL_PROB_Y7,
                      p_risk_fail_rt.FAIL_PROB_Y8,
                      p_risk_fail_rt.FAIL_PROB_Y9,
                      p_risk_fail_rt.FAIL_PROB_Y10,
                      p_risk_fail_rt.FAIL_PROB_Y11,
                      p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17);
      ELSIF p_yr_no = 4
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y4,
                      p_risk_fail_rt.FAIL_PROB_Y5,
                      p_risk_fail_rt.FAIL_PROB_Y6,
                      p_risk_fail_rt.FAIL_PROB_Y7,
                      p_risk_fail_rt.FAIL_PROB_Y8,
                      p_risk_fail_rt.FAIL_PROB_Y9,
                      p_risk_fail_rt.FAIL_PROB_Y10,
                      p_risk_fail_rt.FAIL_PROB_Y11,
                      p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18);
      ELSIF p_yr_no = 5
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y5,
                      p_risk_fail_rt.FAIL_PROB_Y6,
                      p_risk_fail_rt.FAIL_PROB_Y7,
                      p_risk_fail_rt.FAIL_PROB_Y8,
                      p_risk_fail_rt.FAIL_PROB_Y9,
                      p_risk_fail_rt.FAIL_PROB_Y10,
                      p_risk_fail_rt.FAIL_PROB_Y11,
                      p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19);
      ELSIF p_yr_no = 6
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y6,
                      p_risk_fail_rt.FAIL_PROB_Y7,
                      p_risk_fail_rt.FAIL_PROB_Y8,
                      p_risk_fail_rt.FAIL_PROB_Y9,
                      p_risk_fail_rt.FAIL_PROB_Y10,
                      p_risk_fail_rt.FAIL_PROB_Y11,
                      p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20);
      ELSIF p_yr_no = 7
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y7,
                      p_risk_fail_rt.FAIL_PROB_Y8,
                      p_risk_fail_rt.FAIL_PROB_Y9,
                      p_risk_fail_rt.FAIL_PROB_Y10,
                      p_risk_fail_rt.FAIL_PROB_Y11,
                      p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21);
      ELSIF p_yr_no = 8
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y8,
                      p_risk_fail_rt.FAIL_PROB_Y9,
                      p_risk_fail_rt.FAIL_PROB_Y10,
                      p_risk_fail_rt.FAIL_PROB_Y11,
                      p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22);
      ELSIF p_yr_no = 9
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y9,
                      p_risk_fail_rt.FAIL_PROB_Y10,
                      p_risk_fail_rt.FAIL_PROB_Y11,
                      p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23);
      ELSIF p_yr_no = 10
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y10,
                      p_risk_fail_rt.FAIL_PROB_Y11,
                      p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24);
      ELSIF p_yr_no = 11
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y11,
                      p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25);
      ELSIF p_yr_no = 12
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y12,
                      p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26);
      ELSIF p_yr_no = 13
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y13,
                      p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27);
      ELSIF p_yr_no = 14
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y14,
                      p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28);
      ELSIF p_yr_no = 15
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y15,
                      p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29);
      ELSIF p_yr_no = 16
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y16,
                      p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30);
      ELSIF p_yr_no = 17
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y17,
                      p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31);
      ELSIF p_yr_no = 18
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y18,
                      p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32);
      ELSIF p_yr_no = 19
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y19,
                      p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33);
      ELSIF p_yr_no = 20
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y20,
                      p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34);
      ELSIF p_yr_no = 21
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y21,
                      p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35);
      ELSIF p_yr_no = 22
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y22,
                      p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36);
      ELSIF p_yr_no = 23
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y23,
                      p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37);
      ELSIF p_yr_no = 24
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y24,
                      p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38);
      ELSIF p_yr_no = 25
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y25,
                      p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39);
      ELSIF p_yr_no = 26
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y26,
                      p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40);
      ELSIF p_yr_no = 27
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y27,
                      p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41);
      ELSIF p_yr_no = 28
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y28,
                      p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42);
      ELSIF p_yr_no = 29
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y29,
                      p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43);
      ELSIF p_yr_no = 30
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y30,
                      p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44);
      ELSIF p_yr_no = 31
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y31,
                      p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45);
      ELSIF p_yr_no = 32
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y32,
                      p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46);
      ELSIF p_yr_no = 33
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y33,
                      p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47);
      ELSIF p_yr_no = 34
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y34,
                      p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48);
      ELSIF p_yr_no = 35
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y35,
                      p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49);
      ELSIF p_yr_no = 36
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y36,
                      p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50);
      ELSIF p_yr_no = 37
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y37,
                      p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51));
      ELSIF p_yr_no = 38
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y38,
                      p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52));
      ELSIF p_yr_no = 39
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y39,
                      p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52),
                      (p_forecast_const_a + p_forecast_const_b * 53));
      ELSIF p_yr_no = 40
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y40,
                      p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52),
                      (p_forecast_const_a + p_forecast_const_b * 53),
                      (p_forecast_const_a + p_forecast_const_b * 54));
      ELSIF p_yr_no = 41
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y41,
                      p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52),
                      (p_forecast_const_a + p_forecast_const_b * 53),
                      (p_forecast_const_a + p_forecast_const_b * 54),
                      (p_forecast_const_a + p_forecast_const_b * 55));
      ELSIF p_yr_no = 42
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y42,
                      p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52),
                      (p_forecast_const_a + p_forecast_const_b * 53),
                      (p_forecast_const_a + p_forecast_const_b * 54),
                      (p_forecast_const_a + p_forecast_const_b * 55),
                      (p_forecast_const_a + p_forecast_const_b * 56));
      ELSIF p_yr_no = 43
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y43,
                      p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52),
                      (p_forecast_const_a + p_forecast_const_b * 53),
                      (p_forecast_const_a + p_forecast_const_b * 54),
                      (p_forecast_const_a + p_forecast_const_b * 55),
                      (p_forecast_const_a + p_forecast_const_b * 56),
                      (p_forecast_const_a + p_forecast_const_b * 57));
      ELSIF p_yr_no = 44
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y44,
                      p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52),
                      (p_forecast_const_a + p_forecast_const_b * 53),
                      (p_forecast_const_a + p_forecast_const_b * 54),
                      (p_forecast_const_a + p_forecast_const_b * 55),
                      (p_forecast_const_a + p_forecast_const_b * 56),
                      (p_forecast_const_a + p_forecast_const_b * 57),
                      (p_forecast_const_a + p_forecast_const_b * 58));
      ELSIF p_yr_no = 45
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y45,
                      p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52),
                      (p_forecast_const_a + p_forecast_const_b * 53),
                      (p_forecast_const_a + p_forecast_const_b * 54),
                      (p_forecast_const_a + p_forecast_const_b * 55),
                      (p_forecast_const_a + p_forecast_const_b * 56),
                      (p_forecast_const_a + p_forecast_const_b * 57),
                      (p_forecast_const_a + p_forecast_const_b * 58),
                      (p_forecast_const_a + p_forecast_const_b * 59));
      ELSIF p_yr_no = 46
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y46,
                      p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52),
                      (p_forecast_const_a + p_forecast_const_b * 53),
                      (p_forecast_const_a + p_forecast_const_b * 54),
                      (p_forecast_const_a + p_forecast_const_b * 55),
                      (p_forecast_const_a + p_forecast_const_b * 56),
                      (p_forecast_const_a + p_forecast_const_b * 57),
                      (p_forecast_const_a + p_forecast_const_b * 58),
                      (p_forecast_const_a + p_forecast_const_b * 59),
                      (p_forecast_const_a + p_forecast_const_b * 60));
      ELSIF p_yr_no = 47
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y47,
                      p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52),
                      (p_forecast_const_a + p_forecast_const_b * 53),
                      (p_forecast_const_a + p_forecast_const_b * 54),
                      (p_forecast_const_a + p_forecast_const_b * 55),
                      (p_forecast_const_a + p_forecast_const_b * 56),
                      (p_forecast_const_a + p_forecast_const_b * 57),
                      (p_forecast_const_a + p_forecast_const_b * 58),
                      (p_forecast_const_a + p_forecast_const_b * 59),
                      (p_forecast_const_a + p_forecast_const_b * 60),
                      (p_forecast_const_a + p_forecast_const_b * 61));
      ELSIF p_yr_no = 48
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y48,
                      p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52),
                      (p_forecast_const_a + p_forecast_const_b * 53),
                      (p_forecast_const_a + p_forecast_const_b * 54),
                      (p_forecast_const_a + p_forecast_const_b * 55),
                      (p_forecast_const_a + p_forecast_const_b * 56),
                      (p_forecast_const_a + p_forecast_const_b * 57),
                      (p_forecast_const_a + p_forecast_const_b * 58),
                      (p_forecast_const_a + p_forecast_const_b * 59),
                      (p_forecast_const_a + p_forecast_const_b * 60),
                      (p_forecast_const_a + p_forecast_const_b * 61),
                      (p_forecast_const_a + p_forecast_const_b * 62));
      ELSIF p_yr_no = 49
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y49,
                      p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52),
                      (p_forecast_const_a + p_forecast_const_b * 53),
                      (p_forecast_const_a + p_forecast_const_b * 54),
                      (p_forecast_const_a + p_forecast_const_b * 55),
                      (p_forecast_const_a + p_forecast_const_b * 56),
                      (p_forecast_const_a + p_forecast_const_b * 57),
                      (p_forecast_const_a + p_forecast_const_b * 58),
                      (p_forecast_const_a + p_forecast_const_b * 59),
                      (p_forecast_const_a + p_forecast_const_b * 60),
                      (p_forecast_const_a + p_forecast_const_b * 61),
                      (p_forecast_const_a + p_forecast_const_b * 62),
                      (p_forecast_const_a + p_forecast_const_b * 63));
      ELSIF p_yr_no = 50
      THEN
         v_pfail_15y :=
            DATA_15Y (p_risk_fail_rt.FAIL_PROB_Y50,
                      (p_forecast_const_a + p_forecast_const_b * 51),
                      (p_forecast_const_a + p_forecast_const_b * 52),
                      (p_forecast_const_a + p_forecast_const_b * 53),
                      (p_forecast_const_a + p_forecast_const_b * 54),
                      (p_forecast_const_a + p_forecast_const_b * 55),
                      (p_forecast_const_a + p_forecast_const_b * 56),
                      (p_forecast_const_a + p_forecast_const_b * 57),
                      (p_forecast_const_a + p_forecast_const_b * 58),
                      (p_forecast_const_a + p_forecast_const_b * 59),
                      (p_forecast_const_a + p_forecast_const_b * 60),
                      (p_forecast_const_a + p_forecast_const_b * 61),
                      (p_forecast_const_a + p_forecast_const_b * 62),
                      (p_forecast_const_a + p_forecast_const_b * 63),
                      (p_forecast_const_a + p_forecast_const_b * 64));
      END IF;


      V_PDF_15Y_1 := v_pfail_15y (1);
      V_PDF_15Y_2 := v_pfail_15y (2) * (1 - V_PDF_15Y_1);
      V_PDF_15Y_3 := v_pfail_15y (3) * (1 - V_PDF_15Y_1 - V_PDF_15Y_2);
      V_PDF_15Y_4 :=
         v_pfail_15y (4) * (1 - V_PDF_15Y_1 - V_PDF_15Y_2 - V_PDF_15Y_3);
      V_PDF_15Y_5 :=
           v_pfail_15y (5)
         * (1 - V_PDF_15Y_1 - V_PDF_15Y_2 - V_PDF_15Y_3 - V_PDF_15Y_4);

      V_PDF_15Y_6 :=
           v_pfail_15y (6)
         * (  1
            - V_PDF_15Y_1
            - V_PDF_15Y_2
            - V_PDF_15Y_3
            - V_PDF_15Y_4
            - V_PDF_15Y_5);

      V_PDF_15Y_7 :=
           v_pfail_15y (7)
         * (  1
            - V_PDF_15Y_1
            - V_PDF_15Y_2
            - V_PDF_15Y_3
            - V_PDF_15Y_4
            - V_PDF_15Y_5
            - V_PDF_15Y_6);

      V_PDF_15Y_8 :=
           v_pfail_15y (8)
         * (  1
            - V_PDF_15Y_1
            - V_PDF_15Y_2
            - V_PDF_15Y_3
            - V_PDF_15Y_4
            - V_PDF_15Y_5
            - V_PDF_15Y_6
            - V_PDF_15Y_7);

      V_PDF_15Y_9 :=
           v_pfail_15y (9)
         * (  1
            - V_PDF_15Y_1
            - V_PDF_15Y_2
            - V_PDF_15Y_3
            - V_PDF_15Y_4
            - V_PDF_15Y_5
            - V_PDF_15Y_6
            - V_PDF_15Y_7
            - V_PDF_15Y_8);

      V_PDF_15Y_10 :=
           v_pfail_15y (10)
         * (  1
            - V_PDF_15Y_1
            - V_PDF_15Y_2
            - V_PDF_15Y_3
            - V_PDF_15Y_4
            - V_PDF_15Y_5
            - V_PDF_15Y_6
            - V_PDF_15Y_7
            - V_PDF_15Y_8
            - V_PDF_15Y_9);

      V_PDF_15Y_11 :=
           v_pfail_15y (11)
         * (  1
            - V_PDF_15Y_1
            - V_PDF_15Y_2
            - V_PDF_15Y_3
            - V_PDF_15Y_4
            - V_PDF_15Y_5
            - V_PDF_15Y_6
            - V_PDF_15Y_7
            - V_PDF_15Y_8
            - V_PDF_15Y_9
            - V_PDF_15Y_10);

      V_PDF_15Y_12 :=
           v_pfail_15y (12)
         * (  1
            - V_PDF_15Y_1
            - V_PDF_15Y_2
            - V_PDF_15Y_3
            - V_PDF_15Y_4
            - V_PDF_15Y_5
            - V_PDF_15Y_6
            - V_PDF_15Y_7
            - V_PDF_15Y_8
            - V_PDF_15Y_9
            - V_PDF_15Y_10
            - V_PDF_15Y_11);

      V_PDF_15Y_13 :=
           v_pfail_15y (13)
         * (  1
            - V_PDF_15Y_1
            - V_PDF_15Y_2
            - V_PDF_15Y_3
            - V_PDF_15Y_4
            - V_PDF_15Y_5
            - V_PDF_15Y_6
            - V_PDF_15Y_7
            - V_PDF_15Y_8
            - V_PDF_15Y_9
            - V_PDF_15Y_10
            - V_PDF_15Y_11
            - V_PDF_15Y_12);

      V_PDF_15Y_14 :=
           v_pfail_15y (14)
         * (  1
            - V_PDF_15Y_1
            - V_PDF_15Y_2
            - V_PDF_15Y_3
            - V_PDF_15Y_4
            - V_PDF_15Y_5
            - V_PDF_15Y_6
            - V_PDF_15Y_7
            - V_PDF_15Y_8
            - V_PDF_15Y_9
            - V_PDF_15Y_10
            - V_PDF_15Y_11
            - V_PDF_15Y_12
            - V_PDF_15Y_13);

      V_PDF_15Y_15 :=
           v_pfail_15y (15)
         * (  1
            - V_PDF_15Y_1
            - V_PDF_15Y_2
            - V_PDF_15Y_3
            - V_PDF_15Y_4
            - V_PDF_15Y_5
            - V_PDF_15Y_6
            - V_PDF_15Y_7
            - V_PDF_15Y_8
            - V_PDF_15Y_9
            - V_PDF_15Y_10
            - V_PDF_15Y_11
            - V_PDF_15Y_12
            - V_PDF_15Y_13
            - V_PDF_15Y_14);


      /*  DBMS_OUTPUT.put_line ('*************');
        DBMS_OUTPUT.put_line (
             p_risk_fail_rt.pick_id||'_'|| p_yr_no
           || '  VALUES - '
           || V_PDF_15Y_1
           || '     '
           || V_PDF_15Y_2
           || '     '
           || V_PDF_15Y_3
           || '     '
           || V_PDF_15Y_4
           || '     '
           || V_PDF_15Y_5
           || '     '
           || V_PDF_15Y_6
           || '     '
           || V_PDF_15Y_7
           || '     '
           || V_PDF_15Y_8
           || '     '
           || V_PDF_15Y_9
           || '     '
           || V_PDF_15Y_10
           || '     '
           || V_PDF_15Y_11
           || '     '
           || V_PDF_15Y_12
           || '     '
           || V_PDF_15Y_13
           || '     '
           || V_PDF_15Y_14
           || '     '
           || V_PDF_15Y_15); */

      v_present_val :=
         DATA_15Y (V_PDF_15Y_1,
                   V_PDF_15Y_2 / 1.0433,
                   V_PDF_15Y_3 / POWER (1.0433, 2),
                   V_PDF_15Y_4 / POWER (1.0433, 3),
                   V_PDF_15Y_5 / POWER (1.0433, 4),
                   V_PDF_15Y_6 / POWER (1.0433, 5),
                   V_PDF_15Y_7 / POWER (1.0433, 6),
                   V_PDF_15Y_8 / POWER (1.0433, 7),
                   V_PDF_15Y_9 / POWER (1.0433, 8),
                   V_PDF_15Y_10 / POWER (1.0433, 9),
                   V_PDF_15Y_11 / POWER (1.0433, 10),
                   V_PDF_15Y_12 / POWER (1.0433, 11),
                   V_PDF_15Y_13 / POWER (1.0433, 12),
                   V_PDF_15Y_14 / POWER (1.0433, 13),
                   V_PDF_15Y_15 / POWER (1.0433, 14));
      /*  DBMS_OUTPUT.put_line (
              'PRESENT VALUE -'
           || v_present_val (1)
           || '     '
           || v_present_val (2)
           || '     '
           || v_present_val (3)
           || '     '
           || v_present_val (4)
           || '     '
           || v_present_val (5)
           || '     '
           || v_present_val (6)
           || '     '
           || v_present_val (7)
           || '     '
           || v_present_val (8)
           || '     '
           || v_present_val (9)
           || '     '
           || v_present_val (10)
           || '     '
           || v_present_val (11)
           || '     '
           || v_present_val (12)
           || '     '
           || v_present_val (13)
           || '     '
           || v_present_val (14)
           || '     '
           || v_present_val (15)); */

      v_npv_val_y :=
           v_present_val (1)
         + v_present_val (2)
         + v_present_val (3)
         + v_present_val (4)
         + v_present_val (5)
         + v_present_val (6)
         + v_present_val (7)
         + v_present_val (8)
         + v_present_val (9)
         + v_present_val (10)
         + v_present_val (11)
         + v_present_val (12)
         + v_present_val (13)
         + v_present_val (14)
         + v_present_val (15);

      RETURN v_npv_val_y;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
               'ERROR IN FUNCTION fn_get_npv15_val WHILE trying TO get npv VALUE FOR '
            || p_risk_fail_rt.pick_id
            || ' FOR YEAR '
            || p_yr_no);
         DBMS_OUTPUT.PUT_LINE (SUBSTR (SQLERRM, 1, 1000));
         RETURN 0;
   END fn_get_npv15_val;
END ST_RISK_SNAPSHOT;
/

