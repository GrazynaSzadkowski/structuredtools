DECLARE

--Proc egszadk.STRUCTURED_TOOLS.run_summary_for_opensolver
--populates TABLES:
--		St_strategy_run_summary 
--		st_del_program_zbam_reinf_summ 
--
--EXTERNAL optimizer 
--populates TABLE 
--		st_del_program_mtzn_optimised
--
--Proc egszadk.STRUCTURED_TOOLS.run_summary_for_segments
--Populates TABLES:
--		st_del_program_seg_summary 
--		st_del_program_seg_reinf_summ
--
--EXTERNAL optimizer 
--populates TABLE 
--		st_del_program_seg_optimised
--
--Proc egszadk.STRUCTURED_TOOLS.run_summary_for_mtzn_reinf
--Populates TABLE:
--		st_del_program_reinf_summ
--
--EXTERNAL optimizer 
--populates TABLE:
--		st_del_program_reinf_opt
--
--Proc EGSZADK.STRUCTURED_TOOLS.create_sniper_program
--Populates TABLE:
--		st_del_program_sniper
--            
--EGSZADK.STRUCTURED_TOOLS.create_reac_maint_program
--Populates TABLE:
--		st_del_program_rec_maint
--            


BEGIN

--		populate st_temp_stratgey_run, st_temp_bay_stratgey_run

--   DELETE FROM egszadk.st_strategy_run_summary
--   WHERE run_id = 1 and program_id = 1;  --and year?

--   DELETE FROM egszadk.st_del_program_mtzn_optimised
--   WHERE base_run_id = 1 and program_id = 1;  --and del_year?

--   DELETE FROM egszadk.st_del_program_seg_summary
--   WHERE base_run_id = 1 AND program_id = 1;  --and del_year?
   
--   DELETE FROM egszadk.st_del_program_seg_optimised
--   WHERE base_run_id = 1 AND program_id = 1;  --and del_year?

--   DELETE FROM egszadk.st_del_program_seg_mtzn_reinf_summ
--   WHERE base_run_id = 1 AND program_id = 1;  --and del_year?
   
--   DELETE FROM egszadk.st_del_program_mtzn_reinf_opt
--   WHERE base_run_id = 1 AND program_id = 1;  --and del_year?

--   DELETE FROM egszadk.st_del_program_sniper
--   WHERE base_run_id = 1 AND program_id = 1;  --and del_year?

--   DELETE FROM egszadk.st_del_program_reac_maint
--   WHERE base_run_id = 1 AND program_id = 1;  --and del_year?

--   egszadk.optimiser_cycle.reload_temp_strategy_tables (1002 --run_id
--                                         														,4				--stage
--                                                                                 );

--   COMMIT;

--   dbms_output.put_line('start opt loop '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
--   
--   FOR i_YEAR IN 2018 .. 2019 LOOP
--   
--      dbms_output.put_line('Processing year '||i_year);

		/* St_strategy_run_summary and st_del_program_zbam_reinf_summ */
--      egszadk.STRUCTURED_TOOLS.run_summary_for_opensolver(1           --run_id
--                                                         ,2019  --,i_year       --YEAR
--                                                         ,1          --del PROGRAM ID FROM optimiser(OR NULL OR 0 WHEN run FOR THE FIRST YEAR) 
--																			 ,2          --mtzn_excl_version_id
--                                                         ,0          --mtzn_exclude_yrs (DEFAULT 0)
--                                                          );         
--		COMMIT;                                                          

--      INSERT INTO st_del_program_mtzn_optimised
--      SELECT del_year
--            ,mtzn
--            ,1002
--            ,1
--      FROM st_del_program_mtzn_optimised     
--      WHERE base_run_id = 1000 AND program_id = 2 AND del_year = 2018;
--      
--       --i_year;
--      COMMIT;
--      DELETE FROM egszadk.st_del_program_seg_summary
--      WHERE base_run_id = 1002 AND program_id = 1;
--      COMMIT;
      
		/* st_del_program_seg_summary and st_del_program_seg_reinf_summ */
--      egszadk.STRUCTURED_TOOLS.run_summary_for_segments(1002           --run_id
--                                                         ,2018 --i_year       --YEAR
--                                                         ,1          --del PROGRAM ID FROM optimiser(OR NULL OR 0 WHEN run FOR THE FIRST YEAR) 
--                                                         );
--
--		COMMIT;
      
--      INSERT INTO st_del_program_seg_optimised
--      SELECT del_year
--            ,mtzn
--            ,1002
--            ,1
--      FROM st_del_program_seg_optimised     
--      WHERE base_run_id = 1000 AND program_id = 2 AND del_year = 2018;
--      
		/* st_del_program_reinf_summ */
      egszadk.STRUCTURED_TOOLS.run_summary_for_mtzn_reinf(1           --run_id
                                                         ,2019 --i_year       --YEAR
                                                         ,2          --del PROGRAM ID FROM optimiser(OR NULL OR 0 WHEN run FOR THE FIRST YEAR) 
                                                         );
--		COMMIT;

--      INSERT INTO st_del_program_reinf_opt
--      SELECT del_year
--            ,mtzn
--            ,1002
--            ,1
--      FROM st_del_program_seg_optimised     
--      WHERE base_run_id = 1000 AND program_id = 2 AND del_year = 2018;
--      
		/* st_del_program_sniper */
--      EGSZADK.STRUCTURED_TOOLS.create_sniper_program
--                                 (1                 --v_program_id   
--                                 ,1002             --v_base_run_id   
--                                 ,2018 --i_year               --p_program_year 
--                                 ,2                --p_del_strategy_id 
--                                 ,8                --p_del_strategy_scenario_id
--                                 );
--		COMMIT;                             
--
		/* st_del_program_rec_maint */
--      EGSZADK.STRUCTURED_TOOLS.create_reac_maint_program
--                                 (1                --v_program_id   
--                                 ,1002             --     v_base_run_id   
--                                 ,2018              --  p_program_year 
--                                 ,2                --   p_del_strategy_id 
--                                 ,8                --   p_del_strategy_scenario_id
--                                 );
--   END LOOP;

--   dbms_output.put_line('end opt loop '||TO_CHAR(SYSDATE,'hh24:mi:ss'));
--   
--   

END;