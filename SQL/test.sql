SELECT table_name
				,column_name
            ,data_type
            ,data_scale
FROM sys.all_tab_columns
WHERE owner = 'STRUCTOOLS'
AND (table_name LIKE 'ST%' OR table_name LIKE 'NSS%')
AND data_type = 'NUMBER'
ORDER BY table_name, column_name        