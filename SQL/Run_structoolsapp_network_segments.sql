DECLARE
   v_seg_id_first			PLS_INTEGER;
   v_seg_id_last			PLS_INTEGER;
   v_start_dt_char		VARCHAR2(50);      
   v_start_dt				date;
   v_end_dt					date;    
   v_seg_type				PLS_INTEGER;  

   -- The whole thing takes about 2+ hrs. 
   
BEGIN

--	structoolsapp.network_segments.create_segment_type('Some segment type descr');

--	structoolsapp.network_segments.update_segment_type(1, 'Elec connectivity by mat cde and age, with upper length limit');

   /* Do not run drop_segment_indexes as part of regular segments run */
--	structoolsapp.network_segments.drop_segment_indexes;
--	structoolsapp.network_segments.create_segment_indexes;

   /****************************************************************************************************** */
   /****************************************************************************************************** */
   /* This section does everything it needs to to generate segment data for segment type 1.                */
   /* The following procedures (combine_segments and establish_segment_mtzn), can be run separately if     */
   /* needed.                                                                                              */
   
   v_seg_id_first := 0;
   v_seg_id_last  := 0;
   v_start_dt_char := NULL;
   structoolsapp.network_segments.create_segment_type_1(v_seg_id_first	--OUT	PLS_INTEGER
                                                 ,v_seg_id_last		--OUT	PLS_INTEGER
                                                 ,v_start_dt_char  --OUT	VARCHAR2) IS;
                                                 );      
	dbms_output.put_line (TO_CHAR(SYSDATE, 'dd-mm-yyyy hh24:mi:ss')||' Created segments type 1 '||v_start_dt_char
   							||' seg_id_first = '||v_seg_id_first||' seg_id_last = '||v_seg_id_last);

   /****************************************************************************************************** */
   /****************************************************************************************************** */



--   structoolsapp.network_segments.combine_segments(698983   		--v_seg_id_first --IN PLS_INTEGER   -- this is run as part of create_segment_type_1
--   								  					  ,761327   		--v_seg_id_last  --IN PLS_INTEGER   -- but can be run separately, eg when it falls over
--                                            );                                                  -- in this part, because it does a commit after
                                                                                                --  it writes TO FPASS TABLES  
	
--      structoolsapp.network_segments.establish_segment_mtzn(761328         --v_seg_id_first --IN PLS_INTEGER   -- this is run as part of create_segment_type_1
--                                              ,824124         --v_seg_id_last  --IN PLS_INTEGER   -- but can be run separately, eg when it falls over
--                                            );                                                  -- in this part, because it does a commit after
--                                                                                                --  combine_segments  
   
--   structoolsapp.network_segments.delete_segment_version (1						--i_segment_type	IN	PLS_INTEGER
--   																,TO_DATE('19/01/2016 08:29:04','dd-mm-yyyy hh24:mi_ss')		--start_dt
--                                    					,TO_DATE('01-01-3000','dd-mm-yyyy')								--end_dt
--                                                   );  
                                                   
   --***********************************
   --Recreate table st_bay_equipment
   --***********************************
   
END;