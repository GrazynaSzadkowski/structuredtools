DECLARE
   
   v_run_id                   PLS_INTEGER;
   v_program_id               PLS_INTEGER;
   v_base_run_id              PLS_INTEGER;
   v_year                     PLS_INTEGER;
   v_sql                      VARCHAR2(1000);
   v_run_tab                  structoolsapp.housekeeping.del_run_tab_typ := structoolsapp.housekeeping.del_run_tab_typ();
   v_rr_parm_scen_tab         structoolsapp.housekeeping.rr_parm_scen_tab_typ := structoolsapp.housekeeping.rr_parm_scen_tab_typ();
   v_rr_strat_scen_tab        structoolsapp.housekeeping.rr_strat_scen_tab_typ := structoolsapp.housekeeping.rr_strat_scen_tab_typ();
   v_strategy_egi_tab         structoolsapp.housekeeping.strategy_egi_tab_typ := structoolsapp.housekeeping.strategy_egi_tab_typ();
   v_strategy_condition_tab   structoolsapp.housekeeping.strategy_condition_tab_typ := structoolsapp.housekeeping.strategy_condition_tab_typ();
   v_strategy_scenario_tab    structoolsapp.housekeeping.strategy_scenario_tab_typ := structoolsapp.housekeeping.strategy_scenario_tab_typ();
   v_strategy_id_tab          structoolsapp.housekeeping.strategy_id_tab_typ := structoolsapp.housekeeping.strategy_id_tab_typ();

BEGIN

--   /* These are run in the sequence as set below, after structoolsw schema (snapshot) has been refreshed from DW */
--   /* they have also been covered in Structured Tools Detailed Design, under Other Tables */
--   structoolsapp.housekeeping.create_st_tables('st_carrier_detail', 1);
--   structoolsapp.housekeeping.create_st_tables('st_carr_mdl_material', 1);
--    /* Recreate segments data */
--	structoolsapp.housekeeping.create_st_tables('st_bay_defect', 1);
--   structoolsapp.housekeeping.create_st_tables('st_bay_equipment', 1);  --Dependent on segment tables populated
--   structoolsapp.housekeeping.create_st_tables('st_eqp_defect', 1);
--   structoolsapp.housekeeping.create_st_tables('st_eqp_mtzn', 1);
--	structools.schema_utils.enable_table_indexes('ST_EQUIPMENT');
--   structoolsapp.housekeeping.create_st_tables('st_equipment', 1);
--   structoolsapp.housekeeping.create_st_tables('st_mtzn', 1);
--      structoolsapp.housekeeping.create_st_tables('st_hv_feeder', 1);
--   structoolsapp.housekeeping.create_st_tables('st_span', 1);       --Dependent on st_bay_equipment populated 
 structoolsapp.housekeeping.create_st_tables('st_prot_zone', 1);      --Dependent on st_bay_equipment populated


--   structoolsapp.housekeeping.load_suppressed_pickids(NULL	--i_fg_scenario_id
--   																								,2050		--i_max_cutoff_year
--                                                                           ,12				--i_load_version
--                                                                           ,3				--i_st_suppress_ver
--                                                                           ,'N');  		--i_delete_st_suppress_ver

--   structoolsapp.housekeeping.load_suppressed_pickids_user(2050		--i_max_cutoff_year
--   																												,'PUP'    	--suppression_reason
--                                                                                       ,4				--i_st_suppress_ver
--                                                                                        ,'N');  		--i_delete_st_suppress_ver

--   structoolsapp.housekeeping.load_suppressed_pickids(NULL	--i_fg_scenario_id
--   																								,2050		--i_max_cutoff_year
--                                                                           ,12				--i_load_version
--                                                                           ,4				--i_st_suppress_ver
--                                                                           ,'N');  		--i_delete_st_suppress_ver



--   v_run_tab.DELETE;
--   v_run_tab.EXTEND(1);
--   v_run_tab(1) := 925;
--   v_run_tab(2) := 604;
--   v_run_tab(3) := 814;
--   v_run_tab(4) := 823;
--   v_run_tab(5) := 824;
--   dbms_output.put_line ('Table count = '||v_run_tab.COUNT);
--
--   structoolsapp.housekeeping.delete_run('ALL'            --deletion scope, ie ALL or OPT or TAB
--                                       ,NULL --'ST_STRATEGY_RUN'         --table name, only for scope = TAB
--                                       ,'IN'             --qualifier, one of EQ, GE, IN; for qualifying run ids in the next parameter
--                                       ,v_run_tab        --table of run ids; must contain only one element for qualifier = EQ or GE 
--                                       );

--      structools.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN');   --run this when it fails in execute_run_request, bay processing stage      

--   structoolsapp.housekeeping.delete_run('TAB'            --deletion scope, ie ALL or OPT or TAB
--                                       ,'ST_DEL_PROGRAM_RISK'         --table name, only for scope = TAB
--                                       ,'EQ'             --qualifier, one of EQ, GE, IN; for qualifying run ids in the next parameter
--                                       ,v_run_tab        --table of run ids; must contain only one element for qualifier = EQ or GE 
--                                       );
--   structoolsapp.housekeeping.delete_run('TAB'            --deletion scope, ie ALL or OPT or TAB
--                                       ,'ST_DEL_PROGRAM_REAC_MAINT'         --table name, only for scope = TAB
--                                       ,'EQ'             --qualifier, one of EQ, GE, IN; for qualifying run ids in the next parameter
--                                       ,v_run_tab        --table of run ids; must contain only one element for qualifier = EQ or GE 
--                                       );
--   structoolsapp.housekeeping.delete_run('TAB'            --deletion scope, ie ALL or OPT or TAB
--                                       ,'ST_DEL_PROGRAM_INCD_FORECAST'         --table name, only for scope = TAB
--                                       ,'EQ'             --qualifier, one of EQ, GE, IN; for qualifying run ids in the next parameter
--                                       ,v_run_tab        --table of run ids; must contain only one element for qualifier = EQ or GE 
--                                       );
--   structoolsapp.housekeeping.delete_run('TAB'            --deletion scope, ie ALL or OPT or TAB
--                                       ,'ST_DEL_PROGRAM_AGE_STATS'         --table name, only for scope = TAB
--                                       ,'EQ'             --qualifier, one of EQ, GE, IN; for qualifying run ids in the next parameter
--                                       ,v_run_tab        --table of run ids; must contain only one element for qualifier = EQ or GE 
--                                       );
--   
--   structoolsapp.housekeeping.delete_program_year('ST_DEL_PROGRAM_REAC_MAINT'         --table_name
--                                       ,100                                        --run_id
--                                       ,22                                          --program_id, can = NULL
--                                       ,NULL                                        --year, can = NULL (ie all years)
--                                       ,NULL                                      --del_strategy_id (applicable only to reac_maint and sniper tables 
--                                       ,NULL                                      --del_strategy_scenario_id (applicable only to reac_maint and sniper tables
--                                       );
   
EXCEPTION
   WHEN OTHERS THEN
      dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
      dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);  
      RAISE;                        
END;                                  
