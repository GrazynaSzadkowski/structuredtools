DECLARE
   
   v_run_id                   PLS_INTEGER;
   v_program_id               PLS_INTEGER;
   v_base_run_id              PLS_INTEGER;
   v_year                     PLS_INTEGER;
   v_sql                      VARCHAR2(1000);
   v_run_tab                  egszadk.housekeeping.del_run_tab_typ := egszadk.housekeeping.del_run_tab_typ();
   v_rr_parm_scen_tab         egszadk.housekeeping.rr_parm_scen_tab_typ := egszadk.housekeeping.rr_parm_scen_tab_typ();
   v_rr_strat_scen_tab        egszadk.housekeeping.rr_strat_scen_tab_typ := egszadk.housekeeping.rr_strat_scen_tab_typ();
   v_strategy_egi_tab         egszadk.housekeeping.strategy_egi_tab_typ := egszadk.housekeeping.strategy_egi_tab_typ();
   v_strategy_condition_tab   egszadk.housekeeping.strategy_condition_tab_typ := egszadk.housekeeping.strategy_condition_tab_typ();
   v_strategy_scenario_tab    egszadk.housekeeping.strategy_scenario_tab_typ := egszadk.housekeeping.strategy_scenario_tab_typ();
   v_strategy_id_tab          egszadk.housekeeping.strategy_id_tab_typ := egszadk.housekeeping.strategy_id_tab_typ();

BEGIN


--   /* Do not run drop and create_strategy_run_indexes as part of a regular strategy run. It is handled as part of the code. */
--   /* They are exposed procs which can be used on as needed basis */      
--      egszadk.STRUCTURED_TOOLS.drop_strategy_run_idx;

--      egszadk.schema_utils.disable_table_indexes('ST_BAY_STRATEGY_RUN');
--      egszadk.schema_utils.disable_table_indexes('ST_STRATEGY_RUN');      
--      egszadk.schema_utils.enable_table_indexes('ST_STRATEGY_RUN');   --run this when it fails in execute_run_request, vicinity processing stage
--      egszadk.STRUCTURED_TOOLS.create_strategy_run_rb_tmp_idx;  --run this when it fails in optimise_for_segment_rebuild
--      egszadk.schema_utils.enable_table_indexes('ST_STRATEGY_RUN_RB_TMP');   --run this when it fails in execute_run_request, bay processing stage      


--      dbms_output.put_line ('Dropped indexes');
--      egszadk.STRUCTURED_TOOLS.drop_strategy_run_rb_idx;
--      egszadk.STRUCTURED_TOOLS.create_strategy_run_rb_idx;
--      


--      egszadk.STRUCTURED_TOOLS.create_strategy_run_idx;

--      egszadk.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN');   --run this when it fails in execute_run_request, bay processing stage      
--      egszadk.HOUSEKEEPING2.reset_run_request(1001);  --nullify run_ts
--      egszadk.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN');

--      egszadk.schema_utils.enable_table_indexes('ST_STRATEGY_RUN');
--      egszadk.HOUSEKEEPING.reset_run_request(1005);

      egszadk.STRUCTURED_TOOLS_1.execute_run_request(2);
--      egszadk.STRUCTURED_TOOLS.optimise_original_selection(925);
      
      
--      DELETE FROM st_bay_strategy_run WHERE run_id = 1006; COMMIT;
--      egszadk.STRUCTURED_TOOLS_1.execute_run_request(1006);
      
      /* these next 3 procedures can be run by executing optimise_original_selection(run_id) */
      /* or by running them individually  */
--      egszadk.schema_utils.enable_table_indexes('ST_STRATEGY_RUN_RB_TMP');
--      egszadk.STRUCTURED_TOOLS.optimise_for_segment_rebuild(1002);
--      dbms_output.put_line ('Completed optimise_for_segment_rebuild');
--      
--      egszadk.STRUCTURED_TOOLS_0.adjust_for_partial_treatment(1);
--      dbms_output.put_line ('Completed adjust_for_partial_treatment');
      
--		egszadk.schema_utils.enable_table_indexes('ST_STRATEGY_RUN_rb2');
--      egszadk.STRUCTURED_TOOLS.upgrade_poles_under_cond(1);    
--		dbms_output.put_line ('Completed upgrade_poles_under_cond');                                                                     



      
   
EXCEPTION
   WHEN OTHERS THEN
      dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
      dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);  
      RAISE;                        
END;                                  
