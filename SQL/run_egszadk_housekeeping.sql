DECLARE
   
   v_run_tab                  egszadk.housekeeping.del_run_tab_typ := egszadk.housekeeping.del_run_tab_typ();

BEGIN

--	egszadk.housekeeping.create_st_tables('st_carr_mdl_material', 1);  -- see comments in the code re updating cond diameter.
--	egszadk.housekeeping.create_st_tables('st_carrier_detail', 1);
-- egszadk.housekeeping.create_st_tables('ST_BAY_DEFECT_ACTION',1);
-- egszadk.housekeeping.create_st_tables('ST_BAY_DEFECT',1);
-- egszadk.housekeeping.create_st_tables('ST_BAY_EQUIPMENT',1);
--egszadk.housekeeping.create_st_tables('ST_EQP_DEFECT',1);
--egszadk.housekeeping.create_st_tables('ST_EQP_MTZN',1);
--egszadk.housekeeping.create_st_tables('st_equipment', 1);
--egszadk.housekeeping.create_st_tables('st_mtzn', 1);
--egszadk.housekeeping.create_st_tables('st_hv_feeder', 1);
--egszadk.housekeeping.create_st_tables('st_span', 1);
--egszadk.housekeeping.create_st_tables('st_prot_zone', 1);

  --egszadk.schema_utils.enable_table_indexes('ST_EQUIPMENT');
   --
--   egszadk.housekeeping.register_run_request(1               --i_run_id
--                                                ,10                  --i_run_years
--                                                ,'egszadk'         --i_run_requestor     
--                                                ,'test H6 new reinf strategies = 1'    --i_run_comment
--                                                ,'FULL'            --i_treatment_type (FULL or PARTIAL
--                                                ,5                  --i_mtzn_committed_version_id
--                                                ,2018               --i_first_year
--                                                ,1               --i_parent_run_program_id
--                                                ,800               --i_parent_run_id
--                                                ,5                  --i_strategy_set_id
--                                                ,v_rr_parm_scen_tab         --i_run_request_parm_scen_tab
--                                                ,v_rr_strat_scen_tab         --i_run_request_strat_scen_tab
--                                                );

   v_run_tab.DELETE;
   v_run_tab.EXTEND(1);
   v_run_tab(1) := 1;
--   v_run_tab(2) := 1002;
--   v_run_tab(3) := 800;
--   v_run_tab(4) := 813;
--   v_run_tab(5) := 888;
--   dbms_output.put_line ('Table count = '||v_run_tab.COUNT);
--
   egszadk.housekeeping.delete_run('TAB'            --deletion scope, ie ALL or OPT or TAB
                                       ,'ST_BAY_STRATEGY_RUN'         --table name, only for scope = TAB, otherwise NULL
                                       ,'EQ'             --qualifier, one of EQ, GE, IN; for qualifying run ids in the next parameter
                                       ,v_run_tab        --table of run ids; must contain only one element for qualifier = EQ or GE 
                                       );
   egszadk.housekeeping.delete_run('TAB'            --deletion scope, ie ALL or OPT or TAB
                                       ,'ST_BAY_STRATEGY_RUN_RB_TMP'         --table name, only for scope = TAB, otherwise NULL
                                       ,'EQ'             --qualifier, one of EQ, GE, IN; for qualifying run ids in the next parameter
                                       ,v_run_tab        --table of run ids; must contain only one element for qualifier = EQ or GE 
                                       );
   egszadk.housekeeping.delete_run('TAB'            --deletion scope, ie ALL or OPT or TAB
                                       ,'ST_BAY_STRATEGY_RUN_RB'         --table name, only for scope = TAB, otherwise NULL
                                       ,'EQ'             --qualifier, one of EQ, GE, IN; for qualifying run ids in the next parameter
                                       ,v_run_tab        --table of run ids; must contain only one element for qualifier = EQ or GE 
                                       );
   egszadk.housekeeping.delete_run('TAB'            --deletion scope, ie ALL or OPT or TAB
                                       ,'ST_STRATEGY_RUN'         --table name, only for scope = TAB, otherwise NULL
                                       ,'EQ'             --qualifier, one of EQ, GE, IN; for qualifying run ids in the next parameter
                                       ,v_run_tab        --table of run ids; must contain only one element for qualifier = EQ or GE 
                                       );
   egszadk.housekeeping.delete_run('TAB'            --deletion scope, ie ALL or OPT or TAB
                                       ,'ST_STRATEGY_RUN_RB_TMP'         --table name, only for scope = TAB, otherwise NULL
                                       ,'EQ'             --qualifier, one of EQ, GE, IN; for qualifying run ids in the next parameter
                                       ,v_run_tab        --table of run ids; must contain only one element for qualifier = EQ or GE 
                                       );
   egszadk.housekeeping.delete_run('TAB'            --deletion scope, ie ALL or OPT or TAB
                                       ,'ST_STRATEGY_RUN_RB'         --table name, only for scope = TAB, otherwise NULL
                                       ,'EQ'             --qualifier, one of EQ, GE, IN; for qualifying run ids in the next parameter
                                       ,v_run_tab        --table of run ids; must contain only one element for qualifier = EQ or GE 
                                       );
   egszadk.housekeeping.delete_run('TAB'            --deletion scope, ie ALL or OPT or TAB
                                       ,'ST_STRATEGY_RUN_RB2'         --table name, only for scope = TAB, otherwise NULL
                                       ,'EQ'             --qualifier, one of EQ, GE, IN; for qualifying run ids in the next parameter
                                       ,v_run_tab        --table of run ids; must contain only one element for qualifier = EQ or GE 
                                       );

EXCEPTION
   WHEN OTHERS THEN
      dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
      dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);  
      RAISE;                        
END;                                  
