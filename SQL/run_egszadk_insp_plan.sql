DECLARE
   

BEGIN

--   egszadk.insp_housekeeping.register_insp_run_request(2               --i_run_id
--   															,2018								--i_first_year
--                                                ,10                  --i_run_years
--                                                ,'egszadk'         --i_run_requestor     
--                                                ,'testing restricted smoothing'    --i_run_comment
--                                                ,'Y'						--i_smoothing_ind
--                                                );

--   egszadk.schema_utils.truncate_table('INSP_PLAN_RUN');

--   egszadk.schema_utils.disable_table_indexes('INSP_PLAN_RUN_SM');
--	DELETE FROM egszadk.insp_plan_run_sm
--   WHERE run_id = 3;
--	COMMIT;
--   egszadk.schema_utils.enable_table_indexes('INSP_PLAN_RUN_SM');
   
--   egszadk.insp_housekeeping.reset_insp_run_request(3,'S');              --i_run_id, 'A' or 'S' for reset All or reset Smoothing only timestamps
--	egszadk.schema_utils.enable_table_indexes('INSP_PLAN_RUN_SM');
--	egszadk.insp_plan.execute_run_request(4);            
	egszadk.insp_plan.apply_smoothing(4,3);            --i_run_id, smoothing_ind (0,1,2,3); nothing will be done if it's 0
   
--   egszadk.schema_utils.truncate_table('INSP_MTZN_SUMMARY');
	--DELETE FROM egszadk.insp_plan_mod_run; commit;
	--egszadk.insp_optimiser_cycle.update_insp_plan(1,0,2022);            --i_run_id, program_id, year

--	/* Optimisation cleanup */
--   DELETE FROM egszadk.insp_plan_mod_run
--   WHERE run_id = 1 AND program_id = 0;--   AND calendar_year = 2018;
--   DELETE FROM egszadk.insp_mtzn_summary
--   WHERE run_id = 1 AND program_id = 0; -- AND calendar_year = 2018;
--   DELETE FROM egszadk.insp_mtzn_optimised
--   WHERE run_id = 1 AND program_id = 0; -- AND calendar_year = 2018;
--   COMMIT;

	/* Optimisation loop */
--	egszadk.insp_optimiser_cycle.summarise_mtzn_insp(1,0,2018,'Y',0,0,1,1);            --i_run_id, program_id, year, smoothing_ind, st_run_id, st_program_id, insp_mtzn_committed_ver_id
--	Add MTZNs to egszadk.INSP_MTZN_OPTIMISED   
--   egszadk.insp_optimiser_cycle.update_insp_plan(1,0,2018,'Y');   


END;                                  
