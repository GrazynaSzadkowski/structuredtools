DECLARE
   
   v_run_id                   PLS_INTEGER;
   v_program_id               PLS_INTEGER;
   v_base_run_id              PLS_INTEGER;
   v_year                     PLS_INTEGER;
   v_sql                      VARCHAR2(1000);

BEGIN

   structoolsapp.reporting_2.generate_user_prog_risk_prof('EGSZADK'    --user_id
   																										,2						--user_version
                                                 											,10   				--years
                                                                                 ,'Y'					--delte existing results flag
                                                                                 );					
--   structoolsapp.structured_tools.generate_user_prog_risk_prof('EGSZADK'    --user_id
--   																										,3						--user_version
--                                                 											,3   				--years
--                                                                                 ,'N'					--delete existing results flag
--                                                                                 );					

--   structoolsapp.reporting.generate_opt_eqp_list(2     --p_program_id 
--                                                  ,814      --base_run_id
--                                                  ,'N'
--                                                  );                            

--   structoolsapp.reporting.generate_age_stats(NULL --1000 --NULL --0        --program_id
--                                             ,0  --000 --90       --run_id
--                                             ,10             --years
--                                             ,'N');      

--   structoolsapp.reporting.generate_incd_forecast(0 --9000    --run_id
--                                                 ,NULL --2  -- program id (null = no program) 1000
--                                                 ,10    --years
--                                                 ,'N');  -- delete existing results


--   structoolsapp.reporting.generate_post_del_risk_profile(0 --100    --run_id
--                                                 ,NULL  --2  -- program id (null = no program) 1000
--                                                 ,10);     --years
 

--   structoolsapp.reporting.calc_st_seg_maint_yr_cost(1       --parameter scenario id
--                                                      ,1       -- segment type
--                                                     );


EXCEPTION
   WHEN OTHERS THEN
      dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
      dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);  
      RAISE;                        
END;                                  
