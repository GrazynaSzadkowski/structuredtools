DECLARE
   

BEGIN

--	structoolsapp.insp_housekeeping.register_insp_run_request(i_run_id                        IN PLS_INTEGER
--                                 ,i_first_year                    IN PLS_INTEGER
--                                 ,i_run_years                     IN PLS_INTEGER
--                                 ,i_run_requestor                 IN VARCHAR2    DEFAULT NULL     
--                                 ,i_run_comment                   IN VARCHAR2    DEFAULT NULL
--                                 ,i_smoothing_ind					IN CHAR  -- 0,1,2,3. 0 = no smoothing
                                 
--   structools.schema_utils.truncate_table('INSP_PLAN_RUN');
--   structools.schema_utils.disable_table_indexes('INSP_PLAN_RUN_SM');

--	DELETE FROM structools.insp_plan_run_sm
--   WHERE run_id = 3;
--	COMMIT;
--   structools.schema_utils.enable_table_indexes('INSP_PLAN_RUN_SM');
   
--   structoolsapp.insp_housekeeping.reset_insp_run_request(3,'S');              --i_run_id, 'A' or 'S' for reset All or reset Smoothing only timestamps
--	structools.schema_utils.enable_table_indexes('INSP_PLAN_RUN_SM');
	structoolsapp.insp_plan.execute_run_request(3);          -- 10 minutes   
--	structoolsapp.insp_plan.apply_smoothing(4,3);            --i_run_id, smoothing_ind (0,1,2,3); nothing will be done if it's 0
   
--   structools.schema_utils.truncate_table('INSP_MTZN_SUMMARY');
	--DELETE FROM structools.insp_plan_mod_run; commit;
	--structoolsapp.insp_optimiser_cycle.update_insp_plan(1,0,2022);            --i_run_id, program_id, year

--	/* Optimisation cleanup */
--   DELETE FROM structools.insp_plan_mod_run
--   WHERE run_id = 1 AND program_id = 0;--   AND calendar_year = 2018;
--   DELETE FROM structools.insp_mtzn_summary
--   WHERE run_id = 1 AND program_id = 0; -- AND calendar_year = 2018;
--   DELETE FROM structools.insp_mtzn_optimised
--   WHERE run_id = 1 AND program_id = 0; -- AND calendar_year = 2018;
--   COMMIT;

	/* Optimisation loop */
--	structoolsapp.insp_optimiser_cycle.summarise_mtzn_insp(1,0,2018,'Y',0,0,1,1);            --i_run_id, program_id, year, smoothing_ind, st_run_id, st_program_id, insp_mtzn_committed_ver_id
--	Add MTZNs to structools.INSP_MTZN_OPTIMISED   
--   structoolsapp.insp_optimiser_cycle.update_insp_plan(1,0,2018,'Y');   


END;                                  
