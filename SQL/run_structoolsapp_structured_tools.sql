DECLARE
   
   v_run_id                   PLS_INTEGER;
   v_program_id               PLS_INTEGER;
   v_base_run_id              PLS_INTEGER;
   v_year                     PLS_INTEGER;
   v_sql                      VARCHAR2(1000);
   v_run_tab                  structoolsapp.housekeeping.del_run_tab_typ := structoolsapp.housekeeping.del_run_tab_typ();
   v_rr_parm_scen_tab         structoolsapp.housekeeping.rr_parm_scen_tab_typ := structoolsapp.housekeeping.rr_parm_scen_tab_typ();
   v_rr_strat_scen_tab        structoolsapp.housekeeping.rr_strat_scen_tab_typ := structoolsapp.housekeeping.rr_strat_scen_tab_typ();
   v_strategy_egi_tab         structoolsapp.housekeeping.strategy_egi_tab_typ := structoolsapp.housekeeping.strategy_egi_tab_typ();
   v_strategy_condition_tab   structoolsapp.housekeeping.strategy_condition_tab_typ := structoolsapp.housekeeping.strategy_condition_tab_typ();
   v_strategy_scenario_tab    structoolsapp.housekeeping.strategy_scenario_tab_typ := structoolsapp.housekeeping.strategy_scenario_tab_typ();
   v_strategy_id_tab          structoolsapp.housekeeping.strategy_id_tab_typ := structoolsapp.housekeeping.strategy_id_tab_typ();

BEGIN



--   /* Do not run drop and create_strategy_run_indexes as part of a regular strategy run. It is handled as part of the code. */
--   /* They are exposed procs which can be used on as needed basis */      
--      structoolsapp.STRUCTURED_TOOLS.drop_strategy_run_idx;
--      structools.schema_utils.disable_table_indexes('ST_BAY_STRATEGY_RUN');
--      structools.schema_utils.disable_table_indexes('ST_STRATEGY_RUN');      
--      structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN');   --run this when it fails in execute_run_request, vicinity processing stage
--      structoolsapp.STRUCTURED_TOOLS.create_strategy_run_rb_tmp_idx;  --run this when it fails in optimise_for_segment_rebuild
--      structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN_RB_TMP');   --run this when it fails in execute_run_request, bay processing stage      
--      dbms_output.put_line ('Dropped indexes');
--      structoolsapp.STRUCTURED_TOOLS.drop_strategy_run_rb_idx;
--      structoolsapp.STRUCTURED_TOOLS.create_strategy_run_rb_idx;
--      structoolsapp.STRUCTURED_TOOLS.create_strategy_run_idx;
--      structools.schema_utils.enable_table_indexes('ST_STRATEGY_RUN');   --run this when it fails in execute_run_request, bay processing stage      
--      structoolsapp.HOUSEKEEPING.reset_run_request(1000);  --nullify run_ts
--      structools.schema_utils.enable_table_indexes('ST_BAY_STRATEGY_RUN');vbfgdf

      structoolsapp.STRUCTURED_TOOLS.execute_run_request(10);   --Takes approx 2 hrs 

--      structoolsapp.STRUCTURED_TOOLS.optimise_original_selection(2);
      
      /* these next 3 procedures can be run by executing optimise_original_selection(run_id) */
      /* or by running them individually  */
      structoolsapp.STRUCTURED_TOOLS.optimise_for_segment_rebuild(10);
      structoolsapp.STRUCTURED_TOOLS.adjust_for_partial_treatment(10);
--      structoolsapp.STRUCTURED_TOOLS.upgrade_poles_under_cond(2);                                                                   



      

   
EXCEPTION
   WHEN OTHERS THEN
      dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
      dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);  
      RAISE;                        
END;                                  
