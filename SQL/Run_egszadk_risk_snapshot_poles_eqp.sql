DECLARE
   v_yr_mth                char(6) := 201609;
   

BEGIN

--   egszadk.ST_RISK_SNAPSHOT.PR_LOAD_POLES_AND_EQP (v_yr_mth);
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'XARMHV', 'DONOTHING');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'XARMHV', 'REPLACE');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'STAY', 'DONOTHING');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'STAY', 'REPLACE');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'INSUL', 'DONOTHING');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'INSUL', 'REPLACE');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'NWPOLE', 'DONOTHING');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'NWPOLE', 'REPLACE');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_PWOD (v_yr_mth); 
      egszadk.st_risk_snapshot.PR_LOAD_RISK_XARMHVI (v_yr_mth);
      egszadk.st_risk_snapshot.PR_LOAD_RISK_XARMLVI (v_yr_mth);

      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'DOF', 'DONOTHING');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'DOF', 'REPLACE');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'DSTRPM', 'DONOTHING');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'DSTRPM', 'REPLACE');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'PTSD', 'DONOTHING');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'PTSD', 'REPLACE');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'RCLBS', 'DONOTHING');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'RCLBS', 'REPLACE');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'VREG', 'DONOTHING');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'VREG', 'REPLACE');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'SECT', 'DONOTHING');
      egszadk.st_risk_snapshot.PR_LOAD_RISK_MODEL (v_yr_mth, 'SECT', 'REPLACE');  
   
END;

