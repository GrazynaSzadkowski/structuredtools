DECLARE
   

BEGIN

   egszadk.insp_housekeeping.cleanup_insp_optimisation_data (1, 0) -- (i_run_id IN PLS_INTEGER, i_program_id IN PLS_INTEGER)
   ;
--   egszadk.insp_housekeeping.register_insp_run_request(i_run_id IN PLS_INTEGER, i_first_year IN PLS_INTEGER, i_run_years IN PLS_INTEGER, i_run_requestor IN VARCHAR2 DEFAULT NULL     
--                                                            ,i_run_comment IN VARCHAR2 DEFAULT NULL)
--   ;
--   egszadk.insp_housekeeping.reset_insp_run_request   (i_run_id  IN PLS_INTEGER, i_all_or_smoothing	IN CHAR)
--   ;
--   egszadk.insp_housekeeping.delete_insp_run_request   (i_run_id  IN PLS_INTEGER)
--   ;
--   egszadk.insp_housekeeping.delete_insp_plan   ( i_run_id  IN PLS_INTEGER, i_all_or_smoothing	IN CHAR)

EXCEPTION
   WHEN OTHERS THEN
      dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
      dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);  
      RAISE;                        
END;                                  
