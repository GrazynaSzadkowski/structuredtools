DECLARE
   
   v_run_id                   PLS_INTEGER;
   v_program_id               PLS_INTEGER;
   v_base_run_id              PLS_INTEGER;
   v_year                     PLS_INTEGER;
   v_sql                      VARCHAR2(1000);

BEGIN

--   egszadk.reporting.generate_user_prog_risk_prof('EGSZADK'    --user_id
--   																										,1						--user_version
--                                                 											,10   				--years
--                                                                                 ,'Y'					--delte existing results flag
--                                                                                 );					
--   egszadk.structured_tools.generate_user_prog_risk_prof('EGSZADK'    --user_id
--   																										,3						--user_version
--                                                 											,3   				--years
--                                                                                 ,'N'					--delete existing results flag
--                                                                                 );					

--   egszadk.reporting.generate_opt_eqp_list(1     --p_program_id 
--                                                  ,1      --base_run_id
--                                                  ,'N'
--                                                  );                            

--   egszadk.reporting.generate_age_stats(2 --1000 --NULL --0        --program_id
--                                             ,1000  --000 --90       --run_id
--                                             ,3            --years
--                                             ,'Y');      

--   egszadk.reporting.generate_incd_forecast(9000    --run_id
--                                                 ,2  -- program id (null = no program) 1000
--                                                 ,10    --years
--                                                 ,'N');  -- delete existing results


--   egszadk.reporting.generate_post_del_risk_profile(110 --100    --run_id
--                                                 ,8 --NULL  --2  -- program id (null = no program) 1000
--                                                 ,5);     --years
 

--	egszadk.schema_utils.truncate_table('ST_SEG_MAINT_YR_COST');
   
   egszadk.reporting.calc_st_seg_maint_yr_cost(1       --parameter scenario id
                                                      ,1       -- segment type
                                                     );


EXCEPTION
   WHEN OTHERS THEN
      dbms_output.put_line('EXCEPTION '||SQLCODE||' '||SUBSTR(SQLERRM,1,255));
      dbms_output.put_line(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);  
      RAISE;                        
END;                                  
