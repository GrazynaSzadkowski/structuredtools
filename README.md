### What is this repository for? ###

A set of tools commonly referred to as Structured Tools, used primarily to help with asset investment scenario modelling.
Please refer DM# Nickname 33534340 (Structured Tools Detailed Design).

### Who do I talk to? ###

Grazyna Szadkowski 
grazyna.szadkowski@westernpower.com.au
