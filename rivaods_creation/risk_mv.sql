S1_S2_BAY_MV (ALBANY Data): 
SQL : 
SELECT list.structure_1_pick_id,
(SELECT instln_dt
FROM ndw.dist_ewd_asset_attribute_dim p1a, ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id = list.structure_1_pick_id
AND p1a.curr_flg = 'Y')
AS S1_install_date,
list.structure_2_pick_id,
(SELECT instln_dt
FROM ndw.dist_ewd_asset_attribute_dim p1a, ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id = list.structure_2_pick_id
AND p1a.curr_flg = 'Y')
AS S2_install_date,
bay_subtype_desc,
bay_len_m,
equip_cde,
swsect_net_typ AS Net_type,
comp_asset_id AS bay_id,
(SELECT instln_dt
FROM ndw.dist_ewd_asset_attribute_dim p1a, ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id = TRIM (list.comp_asset_id)
AND p1a.curr_flg = 'Y')
AS bay_install_date,
COUNT (*) OVER (PARTITION BY list.structure_1_pick_id) AS bay_s1_count,
COUNT (*) OVER (PARTITION BY list.structure_2_pick_id) AS bay_s2_count
FROM osds.asset_topology list
WHERE ASST_LYR_TYP = 'B'
AND asst_id_no IN
(SELECT DISTINCT asst_id_no
FROM osds.asset_geography cg
WHERE ASST_LYR_TYP = 'B'
AND STD_DSTRCT_NAM IN ('ALBANY')) ; 
SPAN_MV : 
SQL : 
SELECT list.structure_1_pick_id,
(SELECT instln_dt
FROM ndw.dist_ewd_asset_attribute_dim p1a, ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id = list.structure_1_pick_id
AND p1a.curr_flg = 'Y')
AS S1_install_date,
list.structure_2_pick_id,
(SELECT instln_dt
FROM ndw.dist_ewd_asset_attribute_dim p1a, ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id = list.structure_2_pick_id
AND p1a.curr_flg = 'Y')
AS S2_install_date,
bay_subtype_desc,
bay_len_m,
equip_cde,
swsect_net_typ AS Net_type,
comp_asset_id AS bay_id,
(SELECT instln_dt
FROM ndw.dist_ewd_asset_attribute_dim p1a, ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id = TRIM (cg.comp_asset_id)
AND p1a.curr_flg = 'Y')
AS bay_install_date,
COUNT (*) OVER (PARTITION BY list.structure_1_pick_id) AS bay_count
FROM ( SELECT DISTINCT structure_1_pick_id, structure_2_pick_id, COUNT (*)
FROM osds.asset_topology cg
WHERE ASST_LYR_TYP = 'B'
AND asst_id_no IN
(SELECT DISTINCT asst_id_no
FROM osds.asset_geography cg
WHERE ASST_LYR_TYP = 'B'
AND STD_DSTRCT_NAM IN ('ALBANY'))
GROUP BY structure_1_pick_id, structure_2_pick_id
HAVING COUNT (*) > 1) list,
osds.asset_topology cg
WHERE ASST_LYR_TYP = 'B'
AND list.structure_1_pick_id = cg.structure_1_pick_id
AND list.structure_2_pick_id = cg.structure_2_pick_id 
CONDUCTOR_MV: 
SQL: 
/* Formatted on 3/04/2014 7:55:22 AM (QP5 v5.240.12305.39446) */
SELECT carrbaylst.Carrier_pick_id,
carrbaylst.Carrier_SWSECT_FDR_NAM,
carrbaylst.carrier_SWSECT_FDR_id,
carrbaylst.CARR_MDL_CDE,
carrbaylst.CARR_MDL_DESC,
carrbaylst.SUBTYPECD,
carrbaylst.SUBTYPECD_DESC,
carrbaylst.EQUIP_CODE,
carrbaylst.DISTRN_LVL_DESC,
carrbaylst.PHASEDESIGNATION_DESC,
carrbaylst.OPERATINGVOLTAGE,
carrbaylst.LENGTH_M,
carrbaylst.BAY_LEN_M,
carrbaylst.bay_SWSECT_UPSTRM_PROT_DEV_ID,
carrbaylst.bay_SWSECT_UPSTRM_PROT_DEV_TYP,
carrbaylst.Carrier_Suburb,
carrbaylst.Carrier_District,
carrbaylst.Carrier_Maint_zone,
carrbaylst.Carrier_FIRE_RISK_ZONE_RTG,
carrbaylst.Bay_Suburb,
carrbaylst.Bay_District,
carrbaylst.Bay_Maint_zone,
carrbaylst.Bay_FIRE_RISK_ZONE_RTG,
carrbaylst.Bay_street_nam,
carrbaylst.bay_Address_location,
carrbaylst.bay_pick_id,
carrbaylst.STRUCTURE_1_PICK_ID,
S1_equip_name,
S1_equip_desc,
S1_HV_crossarm_construct_typ,
S1_install_date,
S1_length,
S1_LV_crossarm_len,
carrbaylst.STRUCTURE_2_PICK_ID,
S2_equip_name,
S2_equip_desc,
S2_HV_crossarm_construct_typ,
S2_install_date,
S2_length,
S2_LV_crossarm_len
FROM (SELECT carr.Carrier_pick_id,
carr.Carrier_SWSECT_FDR_NAM,
carr.carrier_SWSECT_FDR_id,
carr.CARR_MDL_CDE,
carr.CARR_MDL_DESC,
carr.SUBTYPECD,
carr.SUBTYPECD_DESC,
carr.EQUIP_CODE,
carr.DISTRN_LVL_DESC,
carr.PHASEDESIGNATION_DESC,
carr.OPERATINGVOLTAGE,
carr.LENGTH_M,
carr.Carrier_Suburb,
carr.Carrier_District,
carr.Carrier_Maint_zone,
carr.Carrier_FIRE_RISK_ZONE_RTG,
gbay.STD_SUBRB_DESC AS Bay_Suburb,
gbay.STD_DSTRCT_NAM AS Bay_District,
gbay.MAINT_ZONE_NAM AS Bay_Maint_zone,
gbay.FIRE_RISK_ZONE_RTG AS Bay_FIRE_RISK_ZONE_RTG,
equipb.LOCN_STR_NAM_DESC AS Bay_street_nam,
equipb.locn_addr_1 AS bay_Address_location,
bay.comp_asset_id AS bay_pick_id,
bay.SWSECT_UPSTRM_PROTCTN_DEV_ID
AS bay_SWSECT_UPSTRM_PROT_DEV_ID,
bay.SWSECT_UPSTRM_PROTCTN_DEV_TYP
AS bay_SWSECT_UPSTRM_PROT_DEV_TYP,
bay.BAY_LEN_M,
bay.STRUCTURE_1_PICK_ID,
(SELECT HV_CRSSARM_CNSTR_TYP
FROM ndw.dist_ewd_asset_attribute_dim p1a,
ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id =
bay.STRUCTURE_1_PICK_ID
AND p1a.curr_flg = 'Y')
AS S1_HV_crossarm_construct_typ,
(SELECT instln_dt
FROM ndw.dist_ewd_asset_attribute_dim p1a,
ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id =
bay.STRUCTURE_1_PICK_ID
AND p1a.curr_flg = 'Y')
AS S1_install_date,
(SELECT LEN_M
FROM ndw.dist_ewd_asset_attribute_dim p1a,
ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id =
bay.STRUCTURE_1_PICK_ID
AND p1a.curr_flg = 'Y')
AS S1_length,
(SELECT LV_CRSSARM_LEN
FROM ndw.dist_ewd_asset_attribute_dim p1a,
ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id =
bay.STRUCTURE_1_PICK_ID
AND p1a.curr_flg = 'Y')
AS S1_LV_crossarm_len,
(SELECT equip_nam_cde
FROM ndw.equipment_dim
WHERE equip_stat = 'AC'
AND pick_id = bay.STRUCTURE_1_PICK_ID)
AS S1_equip_name,
(SELECT equip_desc
FROM ndw.equipment_dim
WHERE equip_stat = 'AC'
AND pick_id = bay.STRUCTURE_1_PICK_ID)
AS S1_equip_desc,
bay.STRUCTURE_2_PICK_ID,
(SELECT equip_nam_cde
FROM ndw.equipment_dim
WHERE equip_stat = 'AC'
AND pick_id = bay.STRUCTURE_2_PICK_ID)
AS S2_equip_name,
(SELECT equip_desc
FROM ndw.equipment_dim
WHERE equip_stat = 'AC'
AND pick_id = bay.STRUCTURE_2_PICK_ID)
AS S2_equip_desc,
(SELECT HV_CRSSARM_CNSTR_TYP
FROM ndw.dist_ewd_asset_attribute_dim p1a,
ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id = bay.structure_2_pick_id
AND p1a.curr_flg = 'Y')
AS S2_HV_crossarm_construct_typ,
(SELECT instln_dt
FROM ndw.dist_ewd_asset_attribute_dim p1a,
ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id = bay.structure_2_pick_id
AND p1a.curr_flg = 'Y')
AS S2_install_date,
(SELECT LEN_M
FROM ndw.dist_ewd_asset_attribute_dim p1a,
ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id = bay.structure_2_pick_id
AND p1a.curr_flg = 'Y')
AS S2_length,
(SELECT LV_CRSSARM_LEN
FROM ndw.dist_ewd_asset_attribute_dim p1a,
ndw.equipment_dim p1
WHERE equip_stat = 'AC'
AND p1a.equip_no = p1.equip_no
AND p1.pick_id = bay.structure_2_pick_id
AND p1a.curr_flg = 'Y')
AS S2_LV_crossarm_len
FROM (SELECT /*
+ NO_QUERY_TRANSFORMATION */
DISTINCT
PICK_ID AS Carrier_pick_id,
ct.asst_id_no AS CONDUCTOR_PICK_ID,
ct.SWSECT_FDR_NAM AS Carrier_SWSECT_FDR_NAM,
ct.SWSECT_FDR_ID AS carrier_SWSECT_FDR_id,
ct.CARR_MDL_CDE,
ct.CARR_MDL_DESC,
SUBTYPECD,
SUBTYPECD_DESC,
EQUIP_CODE,
CASE
WHEN DISTRN_LVL_ID = 3 THEN 'LVOH'
WHEN DISTRN_LVL_ID = 4 THEN 'LVUG'
WHEN DISTRN_LVL_ID = 5 THEN 'HVOH'
WHEN DISTRN_LVL_ID = 6 THEN 'HVUG'
WHEN DISTRN_LVL_ID = 7 THEN 'DTTR'
WHEN DISTRN_LVL_ID = 8 THEN 'SLCT'
WHEN DISTRN_LVL_ID = 9 THEN 'SLPT'
WHEN DISTRN_LVL_ID = 10 THEN 'SLCL'
WHEN DISTRN_LVL_ID = 15 THEN 'ERTH'
WHEN DISTRN_LVL_ID = 16 THEN 'DATA'
WHEN DISTRN_LVL_ID = 17 THEN '66UG'
WHEN DISTRN_LVL_ID = 18 THEN '66OH'
WHEN DISTRN_LVL_ID = 19 THEN '132U'
WHEN DISTRN_LVL_ID = 20 THEN '132O'
WHEN DISTRN_LVL_ID = 21 THEN '220O'
WHEN DISTRN_LVL_ID = 22 THEN '330O'
WHEN DISTRN_LVL_ID = 35 THEN 'PWTR'
WHEN DISTRN_LVL_ID = 49 THEN 'COMU'
WHEN DISTRN_LVL_ID = 51 THEN 'COMO'
WHEN DISTRN_LVL_ID = 54 THEN 'SGOH'
ELSE '????'
END
DISTRN_LVL_DESC,
CASE
WHEN PHASEDESIGNATION = 4 THEN 'A'
WHEN PHASEDESIGNATION = 6 THEN 'AB'
WHEN PHASEDESIGNATION = 5 THEN 'AC'
WHEN PHASEDESIGNATION = 2 THEN 'B'
WHEN PHASEDESIGNATION = 3 THEN 'BC'
WHEN PHASEDESIGNATION = 1 THEN 'C'
WHEN PHASEDESIGNATION = 7 THEN 'ABC'
ELSE '????'
END
PHASEDESIGNATION_DESC,
OPERATINGVOLTAGE,
-- CARRIER_MODEL_CODE,
LENGTH_M,
cg.STD_SUBRB_DESC AS Carrier_Suburb,
cg.STD_DSTRCT_NAM AS Carrier_District,
cg.MAINT_ZONE_NAM AS Carrier_Maint_zone,
cg.FIRE_RISK_ZONE_RTG AS Carrier_FIRE_RISK_ZONE_RTG
FROM osds.carrier_detail_mv c,
osds.asset_topology ct,
osds.asset_geography cg
WHERE c.pick_id = TRIM (ct.comp_asset_id)
AND ct.asst_lyr_typ = 'C'
AND ct.asst_id_no IN
(SELECT DISTINCT asst_id_no
FROM osds.asset_geography cg
WHERE ASST_LYR_TYP = 'C'
AND STD_DSTRCT_NAM IN ('ALBANY'))
AND cg.asst_lyr_typ = 'C'
AND ct.comp_asset_id = cg.comp_asset_id) carr,
osds.asset_topology bay,
osds.asset_geography gbay,
ndw.equipment_dim equipb
WHERE carr.CONDUCTOR_PICK_ID = bay.CONDUCTOR_PICK_ID
AND bay.comp_asset_id = gbay.comp_asset_id
AND equipb.pick_id = TRIM (bay.comp_asset_id)
-- Only Active bays
AND equipb.equip_stat = 'AC') carrbaylst 
RISK_MV 
SQL; 
select ed.PLNT_NO,ed.equip_no,mod.NRM_ASSET_TYP_DESC,
NRM_ASSET_TYP_grp_DESC
,FAIL_LKLHD_VAL
,FIRE_LOSS_CNSQNC_VAL
,CUST_LOSS_CNSQNC_VAL
,SAFETY_LOSS_CNSQNC_VAL
,ENV_LOSS_CNSQNC_VAL
,FIRE_RISK_INDX_VAL
,CUST_RISK_INDX_VAL
,SAFETY_RISK_INDX_VAL
,ENV_RISK_INDX_VAL
,RISK_INDX_VAL
,TRGT_RISK_INDX_VAL
,EQUIP_NRM_ASSET_TYP_CNT
from ndw.MTHLY_EQUIP_NRM_RISK_DM_FACT eipf
inner join ndw.equipment_nvgtn_dim t4 on eipf.equip_sk = t4.equip_sk
inner join ndw.equipment_dim ed on t4.equip_nvgtn_sk = ed.equip_nvgtn_sk
inner join ndw.NRM_ASSET_TYPE_MODEL_DIM mod on mod.NRM_ASSET_TYP_MDL_SK = eipf.NRM_ASSET_TYP_MDL_SK
where eipf.asset_seg_id = 'D'
and eipf.yr_mth_no = '201402'
and ed.equip_stat = 'AC'

